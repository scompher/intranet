
<link rel="stylesheet" type="text/css" href="../styles.css">

<cfparam name="form.employeeID" default="">
<cfparam name="form.startDate" default="">
<cfparam name="form.endDate" default="">
<cfparam name="form.startDateHH" default="">
<cfparam name="form.startDateMM" default="">
<cfparam name="form.endDateHH" default="">
<cfparam name="form.endDateMM" default="">
<cfparam name="form.departmentID" default="">
<cfparam name="form.callType" default="incoming">
<cfparam name="form.err" default="">

<cffunction name="formatTime">
	<cfargument name="timeToFormat" type="numeric" required="yes">
	
		<cfset timeVal = timeToFormat>
		<cfset hours = int(timeval / 3600)>
		<cfif hours gt 0><cfset timeval = timeval - (hours * 3600)></cfif>
		<cfset mins = int(timeval / 60)>
		<cfif mins gt 0><cfset timeval = timeval - (mins * 60)></cfif>
		<cfset seconds = timeval>

		<cfset formattedTime = "#numberformat(hours,"00")#:#numberformat(mins,"00")#:#numberformat(seconds,"00")#">

	<cfreturn formattedTime>
</cffunction>

<cfif isDefined("form.btnRunReport")>
	<cfif trim(form.startDate) is "">
		<cfset err = listappend(err,"The start date is required.")>
	</cfif>
	<cfif trim(form.endDate) is "" and (trim(form.endDateHH) is not "" or trim(form.endDateMM) is not "")>
		<cfset err = listappend(err,"The end date is required if you are specifying an end time.")>
	</cfif>
	<cfif trim(form.endDate) is not "">
		<cfif datecompare(form.startDate,form.endDate) is 1>
			<cfset err = listappend(err,"The end date must be greater than the start date.")>
		</cfif>
	</cfif>
	<cfif trim(form.startDate) is not "">
		<cfif not isDate(form.startDate)>
			<cfset err = listappend(err,"The start date must be a valid date.")>
		</cfif>
	</cfif>
	<cfif trim(form.endDate) is not "">
		<cfif not isDate(form.endDate)>
			<cfset err = listappend(err,"The end date must be a valid date.")>
		</cfif>
	</cfif>
</cfif>

<cfquery name="getDepts" datasource="#ds#">
	select * 
	from smdr9000.dbo.departments 
	where departmentName like 'operations%' or departmentName like 'data%' or departmentName like 'dealer%' or departmentName like 'accounting%' or departmentName like 'ops%' 
	order by departmentName asc
</cfquery>

<br />

<div align="center">

<cfif trim(err) is not "">
	<cfloop list="#err#" index="errorMsg">
	<cfoutput>
	<span class="alert"><b>#errorMsg#</b></span><br />
	</cfoutput>
	</cfloop>
	<br />
</cfif>

<table border="0" cellspacing="0" cellpadding="5">
    <tr>
        <td class="highlightbar"><b>Productivity By Dispatcher </b></td>
    </tr>
    <tr>
        <td class="greyrowbottomnopadding">
            <table width="100%" border="0" cellspacing="0" cellpadding="5">
				<cfform method="post" action="index.cfm">
                <tr>
                    <td width="19%" nowrap>Employee #:</td>
                    <td width="81%" nowrap="nowrap">
                        <cfinput type="text" name="employeeID" style="width:50px;" maxlength="5" value="#form.employeeiD#">
                    </td>
                </tr>
				<tr>
					<td nowrap="nowrap">Department:</td>
					<td nowrap="nowrap">
					<select name="departmentID">
						<cfoutput query="getDepts">
							<option <cfif getDepts.departmentID is form.departmentID>selected</cfif> value="#getDepts.departmentID#">#getDepts.departmentName#</option>
						</cfoutput>
					</select>
					</td>
				</tr>
                <tr>
                    <td nowrap>Start Date/Time: </td>
                    <td nowrap="nowrap">
		            <cfinput type="datefield" name="startDate" style="width:75px; vertical-align:middle;" maxlength="10" value="#form.startDate#">
                    &nbsp;
                    <cfinput type="text" name="startDateHH" style="width:25px; vertical-align:middle;" value="#form.startDateHH#"> : 
                    <cfinput type="text" name="startDateMM" style="width:25px; vertical-align:middle;" value="#form.startDateMM#"> 
					(24 hour time format)
                    </td>
                </tr>
                <tr>
                    <td nowrap>End Date/Time: </td>
                    <td nowrap="nowrap">
		            <cfinput type="datefield" name="endDate" style="width:75px; vertical-align:middle;" maxlength="10" value="#form.endDate#">
                    &nbsp;
                    <cfinput type="text" name="endDateHH" style="width:25px; vertical-align:middle;" value="#form.endDateHH#"> : 
                    <cfinput type="text" name="endDateMM" style="width:25px; vertical-align:middle;" value="#form.endDateMM#"> 
					(24 hour time format)
                    </td>
                </tr>
                <tr>
					<td nowrap="nowrap">Call type:</td>
					<td nowrap="nowrap">
					<select name="callType">
						<option <cfif form.calltype is "incoming">selected</cfif> value="incoming">Incoming</option>
						<option <cfif form.calltype is "outgoing">selected</cfif> value="outgoing">Outgoing</option>
					</select>
					</td>
				</tr>
				<tr>
                    <td colspan="2">
                        <cfinput name="btnRunReport" type="submit" class="sidebar" value="Run Report">
						<input name="btnClear" type="button" class="sidebar" value="New Search" onclick="document.location='index.cfm';">
                    </td>
				</tr>
				</cfform>
            </table>
        </td>
    </tr>

</table>
</div>

<!--- <cfabort> --->

<cfif isdefined("form.btnRunReport") and trim(err) is "">

	<cfset startDateTime = "">
	<cfset startDateTime = startDateTime & "#form.startDate#">
	<cfif trim(form.startDateHH) is not "" and trim(form.startDateMM) is not "">
		<cfset startDateTime = startDateTime & " #form.startDateHH#:#form.startDateMM#">
	</cfif>
	
	<cfif trim(form.endDate) is "">
		<cfset form.endDateTime = now()>
	<cfelse>
		<cfset endDateTime = "">
		<cfset endDateTime = endDateTime & "#form.endDate#">
		<cfif trim(form.endDateHH) is not "" and trim(form.endDateMM) is not "">
			<cfset endDateTime = endDateTime & " #form.endDateHH#:#form.endDateMM#">
		</cfif>
	</cfif>

	<cfif datecompare(startDateTime,endDateTime) is 0>
		<cfset endDateTime = dateadd("d",1,startDateTime)>
	</cfif>

	<cfquery name="getDeptList" datasource="#ds#">
		select * 
		from smdr9000.dbo.department_extension_lookup
		where departmentid = #form.departmentID# 
	</cfquery>
	<cfset extList = quotedvaluelist(getDeptList.extension)>

	<!--- get employee logs for timeframe --->
	<cfquery name="getLoggedInRecords" datasource="intranet">
		select 
			ROW_NUMBER() OVER(ORDER BY employeeID asc, loggedDateTime asc) AS 'RowNumber', 
			intranet.dbo.dispatcherLog.dispatcherLogID, 
			intranet.dbo.dispatcherLog.loggedDateTime, 
			intranet.dbo.dispatcherLog.loggedAction, 
			intranet.dbo.dispatcherLog.employeeID, 
			intranet.dbo.dispatcherLog.port, 
			intranet.dbo.dispatcherLog.station 
		from intranet.dbo.dispatcherLog
		where 
		(loggedDateTime >= #createodbcdatetime(startDateTime)# 
		<cfif trim(endDateTime) is not "">
			and loggedDateTime <= #createodbcdatetime(endDateTime)#
		</cfif>
		) and 
		<cfif trim(form.employeeID) is not "">
			employeeid = '#form.employeeID#' and 
		</cfif>
		<cfif trim(extList) is not "">
			station IN (#preserveSingleQuotes(extList)#) and 
		</cfif>
		1=1
		order by employeeID asc, station asc, loggedDateTime asc 
	</cfquery>
	<cfquery name="getStations" dbtype="query">
		select distinct station 
		from getLoggedInRecords 
		order by station asc 
	</cfquery>
	<cfset stationList = quotedvaluelist(getStations.station)>

	<cfif trim(stationList is "")>
		station list empty
		<cfabort>
	</cfif>

	<!--- get calls for timeframe and stations --->
	<cfquery name="getAllCalls" datasource="#ds#">
		select 
			smdr9000.dbo.smdrData.dateTimeOccurred, 
			smdr9000.dbo.smdrData.dialedNumber, 
			smdr9000.dbo.smdrData.duration, left(smdr9000.dbo.smdrData.duration,1) + ':' + substring(smdr9000.dbo.smdrData.duration,2,2) + ':' + right(smdr9000.dbo.smdrData.duration,2) as callduration, 
			left(smdr9000.dbo.smdrData.duration,1) * 3600 +  substring(smdr9000.dbo.smdrData.duration,2,2) * 60 + right(smdr9000.dbo.smdrData.duration,2) as durationInSeconds, 
			smdr9000.dbo.smdrData.callingNumber 
		from smdr9000.dbo.smdrData with (nolock) 
		where 
		(dateTimeOccurred >= #createodbcdatetime(startDateTime)# and dateTimeOccurred <= #createodbcdatetime(endDateTime)#) and 
		(conditionCode <> 'G' and conditionCode <> 'H' and conditionCode <> 'I') and 
		<cfif form.callType is "incoming">
			smdr9000.dbo.smdrData.dialedNumber IN (#preserveSingleQuotes(stationList)#)
		<cfelseif form.callType is "outgoing">
			smdr9000.dbo.smdrData.callingNumber IN (#preserveSingleQuotes(stationList)#)
		</cfif>
		order by smdr9000.dbo.smdrData.dateTimeOccurred asc
	</cfquery>
	
	<cfset loggedIn = "">
	<cfset loggedOut = "">
	<cfset recordMarker = 0>
	<cfset totalCalls = 0>
	<cfset foundPair = false>
	<cfset totalCallDuration = 0>
	
	<cfset qAllCalls = querynew("dateTimeOccurred,dialedNumber,callDuration,callingNumber,employeeID,station,durationInSeconds")>

	<cfset queryList = "">

	<cfset employeeID = "">
	<cfoutput query="getLoggedInRecords" group="employeeID">
		<cfoutput group="station">
			<cfoutput>
			<!--- #employeeID# #station# #loggedAction# #loggedDateTime#<br /> --->
			<cfif loggedAction is "LOGGED IN">
				<cfset loggedIn = loggedDateTime>
			<cfelseif loggedAction is "LOGGED OUT">
				<cfset loggedOut = loggedDateTime>
				<cfif trim(loggedIn) is ""><cfset loggedIn = startDateTime></cfif>
			</cfif>
			<cfif trim(loggedIn) is not "" and trim(loggedOut) is not "">
				<!--- found pair<br /> --->
				<cfset queryList = listappend(queryList,"#employeeID#~#station#~#loggedIn#~#loggedOut#","|")>
				<cfset loggedIn = "">
				<cfset loggedOut = "">
			</cfif>
			</cfoutput>
			<cfif trim(loggedIn) is not "" and trim(loggedOut) is "">
				<cfset loggedOut = endDateTime>
				<!--- found pair<br /> --->
				<cfset queryList = listappend(queryList,"#employeeID#~#station#~#loggedIn#~#loggedOut#","|")>
				<cfset loggedIn = "">
				<cfset loggedOut = "">
			<cfelseif trim(loggedOut) is not "" and trim(loggedIn) is "">
				<cfset loggedIn = startDateTime>
				<!--- found pair<br /> --->
				<cfset queryList = listappend(queryList,"#employeeID#~#station#~#loggedIn#~#loggedOut#","|")>
				<cfset loggedIn = "">
				<cfset loggedOut = "">
			</cfif>
			<!--- <br /> --->
		</cfoutput>
		<cfif trim(loggedIn) is not "" and trim(loggedOut) is "">
			<cfset loggedOut = dateadd("d",1,startDate)>
			<!--- found pair<br /> --->
			<cfset queryList = listappend(queryList,"#employeeID#~#station#~#loggedIn#~#loggedOut#","|")>
			<cfset loggedIn = "">
			<cfset loggedOut = "">
		<cfelseif trim(loggedOut) is not "" and trim(loggedIn) is "">
			<cfset loggedIn = startDateTime>
			found pair<br />
				<cfset queryList = listappend(queryList,"#employeeID#~#station#~#loggedIn#~#loggedOut#","|")>
			<cfset loggedIn = "">
			<cfset loggedOut = "">
		</cfif>
		<!--- <br /> --->
	</cfoutput>

	<cfloop list="#queryList#" delimiters="|" index="criteria">
		<cfset employeeID = listgetat(criteria,1,"~")>
		<cfset station = listgetat(criteria,2,"~")>
		<cfset start = listgetat(criteria,3,"~")>
		<cfset end = listgetat(criteria,4,"~")>
		<cfquery name="getCalls" dbtype="query">
			select * 
			from getAllCalls 
			where 
			<cfif form.callType is "incoming">
				dialedNumber = '#station#' and 
			<cfelseif form.callType is "outgoing">
				callingNumber = '#station#' and 
			</cfif>
			(dateTimeOccurred >= #createodbcdatetime(start)# and dateTimeOccurred <= #createodbcdatetime(end)#)
		</cfquery>
		<cfif getCalls.recordcount gt 0>
			<cfloop query="getCalls">
				<cfset x = queryAddRow(qAllCalls)>
				<cfset x = querySetCell(qAllCalls,"dateTimeOccurred",getCalls.dateTimeOccurred)>
				<cfset x = querySetCell(qAllCalls,"dialedNumber",getCalls.dialedNumber)>
				<cfset x = querySetCell(qAllCalls,"callDuration",getCalls.callDuration)>
				<cfset x = querySetCell(qAllCalls,"callingNumber",getCalls.callingNumber)>
				<cfset x = querySetCell(qAllCalls,"employeeID",employeeID)>
				<cfif form.callType is "incoming">
					<cfset x = querySetCell(qAllCalls,"station",getCalls.dialedNumber)>
				<cfelseif form.callType is "outgoing">
					<cfset x = querySetCell(qAllCalls,"station",getCalls.callingNumber)>
				<cfelse>
					<cfset x = querySetCell(qAllCalls,"station",station)>
				</cfif>
				<cfset x = querySetCell(qAllCalls,"durationInSeconds",getCalls.durationInSeconds)>
			</cfloop>
		</cfif>
	</cfloop>

	<br />
	<br />
	
	<div align="center" class="normal">
	<table width="600" border="0" cellspacing="0" cellpadding="5">
		<cfset counter = 0>
		<cfoutput query="qAllCalls" group="employeeID">
			<tr>
				<td><b>Call Date/Time</b></td>
				<td><b>Employee</b></td>
				<td><b>Station</b></td>
				<td><b>Call Duration </b></td>
				<cfif form.callType is "incoming">
					<td><b>Calling ## </b></td>
				<cfelseif form.callType is "outgoing">
					<td><b>Dialed ## </b></td>
				</cfif>
			</tr>
			<cfset totalCalls = 0>
			<cfset totalCallDuration = 0>
			<cfoutput group="station">
				<cfoutput>
					<cfset totalCalls = totalCalls + 1>
					<cfset totalCallDuration = totalCallDuration + durationInSeconds>
					<cfif durationInSeconds gte 300>
					<tr style="background-color:##FFBFBF">
					<cfelse>
					<tr>
					</cfif>
						<td>#dateformat(dateTimeOccurred,'mm/dd/yyyy')# #timeformat(dateTimeOccurred,'HH:mm:ss')#</td>
						<td>#employeeID#</td>
						<td>#station#</td>
						<td>#callDuration#</td>
						<cfif form.callType is "incoming">
							<td>#callingNumber#</td>
						<cfelseif form.callType is "outgoing">
							<td>#dialedNumber#</td>
						</cfif>
					</tr>
				</cfoutput>
			</cfoutput>
			<tr><td colspan="5"><hr /></td></tr>
			<cfif totalCalls GT 0>
				<cfset avgCallDuration = int(evaluate(totalCallDuration / totalCalls))>
			<cfelse>
				<cfset avgCallDuration = 0>
			</cfif>
			<tr>
				<td><b>Total Calls:</b></td>
				<td><b>#totalCalls#</b></td>
				<td><b></b></td>
				<td><b>Average call duration:</b></td>
				<td><b>#formatTime(avgCallDuration)#</b></td>
			</tr>
			<tr><td colspan="5"><br /><br /></td></tr>
		</cfoutput>
	</table>
	</div>
</cfif>
