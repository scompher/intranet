<cfif outputFormat is "PDF">

	<cffile action="upload" filefield="txtInputFile" destination="#request.directPath#\convertInvoice\temp\" nameconflict="overwrite">
	<cfset uploadedFile = file.ServerFile>
	<cfset uploadedFileName = replace(uploadedFile,".txt","")>

	<!--- convert invoice to PDF --->
	<cfif fileExists("#request.directpath#\convertInvoice\temp\#uploadedFileName#.txt")>
		<cf_convertToPDF inputFile="#request.directpath#\convertInvoice\temp\#uploadedFileName#.txt" outputFile="#request.directpath#\convertInvoice\temp\#uploadedFileName#.pdf" printOnly="yes">
	</cfif>
	<cfheader name="content-disposition" value="attachment;filename=#uploadedFileName#.pdf"> 
	<cfcontent file="#request.directpath#\convertInvoice\temp\#uploadedFileName#.pdf" type="application/unknown">

<cfelse>

	<cffile action="upload" filefield="cInputFile" destination="#request.directPath#\convertInvoice\temp\" nameconflict="overwrite">
	<cfset cUploadedFile = file.ServerFile>
	
	<cffile action="upload" filefield="mInputFile" destination="#request.directPath#\convertInvoice\temp\" nameconflict="overwrite">
	<cfset mUploadedFile = file.ServerFile>
	
	<cffile action="upload" filefield="nInputFile" destination="#request.directPath#\convertInvoice\temp\" nameconflict="overwrite">
	<cfset nUploadedFile = file.ServerFile>
	
	<cffile action="upload" filefield="rInputFile" destination="#request.directPath#\convertInvoice\temp\" nameconflict="overwrite">
	<cfset rUploadedFile = file.ServerFile>
	
	<cfset uploadedFileName = replace(cUploadedFile, ".C", "", "all")>
	
	<!--- convert invoice parts to multi sheet XLS --->
	<cfset sourceFilesExist = false>
	<cfset sourceFileExt = "C,M,N,R">
	<cfloop list="#sourceFileExt#" index="ext">
		<cfif fileExists("#request.directpath#\convertInvoice\temp\#uploadedFileName#.#ext#")>
			<cfset sourceFilesExist = true>
		</cfif>
	</cfloop>
	<cfif sourceFilesExist>
		<cfx_invoice sourceDirectory="#request.directpath#\convertInvoice\temp\" sourceFileNamePattern="#uploadedFileName#" outputFile="#request.directpath#\convertInvoice\temp\#uploadedFileName#.xls">
	</cfif>
	<cfheader name="content-disposition" value="attachment;filename=#uploadedFileName#.xls"> 
	<cfcontent file="#request.directpath#\convertInvoice\temp\#uploadedFileName#.xls" type="application/unknown">
	
</cfif>

