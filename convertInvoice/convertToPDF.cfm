
<cfparam name="attributes.inputFile" default="">
<cfparam name="attributes.outputFile" default="">
<cfparam name="attributes.printOnly" default="">

<cfset inputFile = attributes.inputFile>
<cfset outputFile = attributes.outputFile>
<cfset printOnly = attributes.printOnly>

<cffile action="read" file="#inputFile#" variable="fileContents">

<cfset esc = chr(27)>
<cfset cr = chr(13) & chr(10)>
<cfset nbsp = "&nbsp;">

<cfif printOnly is "yes">
	<!--- <cfset margintop = "1.75"> --->
	<cfset margintop = "0.50">
<cfelse>
	<cfset margintop = "1.50">
</cfif>

<cfdocument format="pdf" scale="100" overwrite="yes" marginleft=".25" filename="#outputFile#" margintop="#margintop#"> 
<style type="text/css">
	.default {font-family:"Courier New"; font-size:14px;} 
	.largeheading {font-family:"Courier New"; font-size:18px;}
	.detail {font-family:"Courier New"; font-size:11px;}
</style>
<cfset pagecount = 1>
<div class="default">
<cfoutput>
<cfset formFeedCounter = 0>
<cfloop list="#fileContents#" delimiters="#chr(13)#" index="line">
	<cfset output = line>
	<cfset output = replace(output," ",nbsp,"all")>
	<cfset output = replace(output,"[largeheading]","<span class=""largeheading"">", "all")>
	<cfset output = replace(output,"[/largeheading]","</span>", "all")>
	<cfset output = replace(output,"[detail]","<span class=""detail"">", "all")>
	<cfset output = replace(output,"[/detail]","</span>", "all")>
	<cfif printOnly is not "yes">
		<cfif pagecount is 1>
			<cfdocumentitem type="header"><img src="invoiceLogo.gif" /></cfdocumentitem>
		</cfif>
	</cfif>
	<cfif findnocase(chr(12),output) is not 0>
		<cfdocumentitem evalatprint="true" type="pagebreak"></cfdocumentitem>
		<cfif printOnly is not "yes">
			<cfdocumentitem type="header"><img src="invoiceLogo.gif" /></cfdocumentitem>
		</cfif>
		<cfset pagecount = pagecount + 1>
	<cfelseif findnocase("[cuthere]",output) is not 0>
		<cfif printOnly is not "yes">
			<br />
			<center><img width="800" src="cuthere.jpg" /></center>
		</cfif>
	<cfelse>
		<cfif printOnly is "yes">
			<cfif findnocase("S&nbsp;U&nbsp;M&nbsp;M&nbsp;A&nbsp;R&nbsp;Y",output) is not 0>
				<br /><br /><br /><br /><br /><br /><br />#output#<br />
			<cfelse>
				#output#<br />
			</cfif>
		<cfelse>
			#output#<br />
		</cfif>
	</cfif>
</cfloop>
</cfoutput>
</div>
</cfdocument>
