
<link rel="stylesheet" type="text/css" href="/styles.css">

<cfparam name="outputFormat" default="">

<div align="center">
<table width="400" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Manual Invoice Processing</b> </td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<form method="post" action="index.cfm" enctype="multipart/form-data">
				<tr>
					<td>Choose Format: 
						<select onChange="this.form.submit();" name="outputFormat" style="vertical-align:middle;">
							<option value="">Select Format</option>
							<option <cfif outputFormat is "PDF">selected</cfif> value="PDF">PDF Document</option>
							<option <cfif outputFormat is "XLS">selected</cfif> value="XLS">Excel Spreadsheet</option>
						</select>
					</td>
				</tr>
				<cfif outputFormat is "PDF">
				<tr>
					<td>
					Select .TXT Input file: 
					<input type="file" name="txtInputFile" style="vertical-align:middle;">
					</td>
				</tr>
				<cfelseif outputFormat is "XLS">
				<tr>
					<td>
					Select .C Input file: 
					<input type="file" name="cInputFile" style="vertical-align:middle;">
					</td>
				</tr>
				<tr>
					<td>
					Select .M Input file: 
					<input type="file" name="mInputFile" style="vertical-align:middle;">
					</td>
				</tr>
				<tr>
					<td>
					Select .N Input file: 
					<input type="file" name="nInputFile" style="vertical-align:middle;">
					</td>
				</tr>
				<tr>
					<td>
					Select .R Input file: 
					<input type="file" name="rInputFile" style="vertical-align:middle;">
					</td>
				</tr>
				</cfif>
				<cfif outputFormat is not "">
				<tr>
					<td>
					<input onClick="this.form.action='process.cfm';" name="btnSubmit" type="submit" class="sidebar" value="Upload Files and Process">
					</td>
				</tr>
				</cfif>
				</form>
			</table>
		</td>
	</tr>
</table>
<br />
<a class="normal" style="text-decoration:underline;" href="/index.cfm">Return to Intranet</a>
</div>

