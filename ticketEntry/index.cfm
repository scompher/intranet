
<!--- <link rel="stylesheet" type="text/css" href="../styles.css"> --->

<!--- item list 
Item name - URL - Access to URL 
Technology After Hours Call Log - /technologyOnCall/ - /technologyOnCall/
Operations After Hours Call Log - /calllog/index.cfm - /calllog/index.cfm
Create a New Inquiry - /inquiry/createInquiry.cfm - /inquiry/createInquiry.cfm
Equipment Checklist New Ticket - /troubleTickets/newTicket.cfm - /troubleTickets/tickets.cfm
Outage Maintenance New Ticket - /uptime/outtages/add.cfm - /uptime/outtages/
Global Change Request - /globalChange/new.cfm - /globalChange/new.cfm
Make A Suggestion - /misc/suggest.cfm - all have access
--->

<cfset MenuItemList = "">
<cfset MenuItemList = listappend(MenuItemList,"Technology After Hours Call Log - /technologyOnCall/new.cfm")>
<cfset MenuItemList = listappend(MenuItemList,"Operations After Hours Call Log - /calllog/new_incident.cfm")>
<cfset MenuItemList = listappend(MenuItemList,"Create a New Inquiry - /inquiry/createInquiry.cfm")>
<cfset MenuItemList = listappend(MenuItemList,"Equipment Checklist New Ticket - /troubleTickets/newTicket.cfm")>
<cfset MenuItemList = listappend(MenuItemList,"Outage Maintenance New Ticket - /uptime/outtages/add.cfm")>
<cfset MenuItemList = listappend(MenuItemList,"Global Change Request - /globalChange/new.cfm")>
<cfset MenuItemList = listappend(MenuItemList,"New Service Interruption - /serviceInterruption/newIncidentrpt.cfm")>
<cfset MenuItemList = listappend(MenuItemList,"Dealer Message Center New Message - /dealerMessageCenter/newmessage.cfm")>

<cfset AccessItemList = "/technologyOnCall/,/calllog/index.cfm,/inquiry/createInquiry.cfm,/troubleTickets/tickets.cfm,/uptime/outtages/,/globalChange/new.cfm,/serviceInterruption/newIncidentrpt.cfm,/dealerMessageCenter/newmessage.cfm">

<cfif trim(cookie.seclist) is "">
	<cfset cookie.seclist = 0>
</cfif>

<cfquery name="getAccessItems" datasource="#ds#">
	select itemURL  
	from dbo.Admin_Security_Items
	where itemID IN (#cookie.seclist#) 
</cfquery>

<table width="225" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>New Ticket Entry</b></td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td><b>Select Ticket to Create:</b> </td>
				</tr>
				<form method="post" action="index.cfm">
				<tr>
					<td>
						<select name="jumpMenuChoice" style="vertical-align:middle;" onchange="document.location = this.form.jumpMenuChoice.options[this.form.jumpMenuChoice.selectedIndex].value;">
							<option value="">Please Select A Ticket</option>
							<cfoutput>
							<cfloop query="getAccessItems">	
								<cfset searchIndex = listfindnocase(AccessItemList,getAccessItems.itemURL)>
								<cfif searchIndex is not 0>
									<cfset theItem = listgetat(menuItemList,searchIndex)>
									<cfset theItemName = listgetat(theItem,1,"-")>
									<cfset theItemUrl = listgetat(theItem,2,"-")>
									<option value="#trim(theItemUrl)#">#trim(theItemName)#</option>
								</cfif>
							</cfloop>
							<!--- <option value="/misc/suggest.cfm">Make A Suggestion</option> --->
							</cfoutput>
						</select>
					</td>
				</tr>
				</form>
			</table>
		</td>
	</tr>
</table>
<br />
