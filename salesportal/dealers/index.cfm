<link rel="stylesheet" type="text/css" href="../../styles.css">

<cfquery name="getdealers" datasource="#ds#">
	select salesPortal_dealers.*, admin_users.firstname, admin_users.lastname 
	from salesPortal_dealers 
	left join admin_users on salesPortal_dealers.acctmanagerid = admin_users.adminuserid
	<!--- 
	<cfif getsec.seclevelid gt 2>
	where acctmanagerid = #cookie.adminlogin#
	</cfif>
	--->
	order by dealerid asc
</cfquery>

<div align="center" class="normal">
<table border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Sales portal dealer maintenance</b></td>
	</tr>
	<form method="post" action="add.cfm">
	<tr>
		<td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td align="center"><b>Action</b></td>
					<td><b>Dealer Name </b></td>
					<td><b>Passcode</b></td>
					<td align="center"><b>Expires</b></td>
					<td align="center"><b>Account Manager</b> </td>
					<td align="center"><b>Presentation</b></td>
					<td align="center"><b>Quote</b></td>
					<td align="center"><b>Sales Book</b></td>
					<td align="center"><b>Number of Visits</b></td>
				</tr>
				<cfoutput query="getdealers">
				<cfset daysToExpire = datediff("d",dateformat(now(),'mm/dd/yyyy'), expirationDate)>
				<cfif daysToExpire lte 2>
				<tr style="color:##DF262B; font-weight:bold;">
				<cfelseif daysToExpire is 3>
				<tr style="font-weight:bold;">
				<cfelse>
				<tr>
				</cfif>
					<td align="center" class="linedrow" style="color:##000000; font-weight:normal">
					<a href="edit.cfm?dealerid=#dealerid#">edit</a> | 
					<a href="javascript: if (confirm('Are you sure you wish to delete this dealer?')) document.location='delete.cfm?dealerid=#dealerid#';">delete</a>
					</td>
					<td class="linedrow">#dealername#</td>
					<td class="linedrow">#passcode#</td>
					<td align="center" class="linedrow">#dateformat(expirationDate, 'mm/dd/yyyy')# <cfif daysToExpire is 3>!</cfif></td>
					<td align="center" class="linedrow">#firstname# #lastname#</td>
					<td align="center" class="linedrow"><cfif presentation is not ""><a target="_blank" href="#copalinkURL#/sales/files/#presentation#">View</a></cfif>&nbsp;</td>
					<td align="center" class="linedrow"><cfif priceQuote is not ""><a target="_blank" href="#copalinkURL#/sales/files/#priceQuote#">View</a></cfif>&nbsp;</td>
					<td align="center" class="linedrow"><cfif salesbook is not ""><a target="_blank" href="#copalinkURL#/sales/files/#salesbook#">View</a></cfif>&nbsp;</td>
					<td align="center" class="linedrow">#numberOfVisits#</td>
				</tr>
				</cfoutput>
				<tr>
					<td colspan="8">
						<input style="height:20px" name="Submit" type="submit" class="sidebar" value="Add Dealer">
					</td>
				</tr>
			</table>
		</td>
	</tr>
	</form>
</table>
<br />
<a href="/">Return to Intranet</a>
</div>
