
<!--- get filenames --->
<cfquery name="getinfo" datasource="#ds#">
	select * from salesportal_dealers
	where dealerid = #dealerid#
</cfquery>


<!--- delete files --->
<cfset filepath = "e:\websites\copalink\sales\files">

<cfif trim(getinfo.presentation) is not "">
	<cfif fileExists("#filepath#\#getinfo.presentation#")>
		<cffile action="delete" file="#filepath#\#getinfo.presentation#">
	</cfif>
</cfif>

<cfif trim(getinfo.priceQuote) is not "">
	<cfif fileExists("#filepath#\#getinfo.priceQuote#")>
		<cffile action="delete" file="#filepath#\#getinfo.priceQuote#">
	</cfif>
</cfif>

<cfif trim(getinfo.salesbook) is not "">
	<cfif fileExists("#filepath#\#getinfo.salesbook#")>
		<cffile action="delete" file="#filepath#\#getinfo.salesbook#">
	</cfif>
</cfif>

<!--- delete info --->
<cfquery name="deleteInfo" datasource="#ds#">
	delete from salesportal_dealers
	where dealerid = #dealerid#
</cfquery>

<cflocation url="index.cfm">