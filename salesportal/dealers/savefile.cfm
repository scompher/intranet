
<cfset filepath = "e:\websites\copalink\sales\files">

<cfswitch expression="#attributes.filetype#">
	<cfcase value="presentation"><cfset field = "presentation"></cfcase>
	<cfcase value="quote"><cfset field = "priceQuote"></cfcase>
	<cfcase value="salesbook"><cfset field = "salesBook"></cfcase>
</cfswitch>

<!--- save file --->
<cffile action="upload" filefield="#attributes.formfield#" destination="#filepath#\" nameconflict="makeunique">
<cfset savedFile = file.ServerFile>
<cfset uploadedFile = file.ClientFile>
<cfset newName = "#attributes.dealerID#_#savedFile#">
<cffile action="rename" source="#filepath#\#savedFile#" destination="#filepath#\#newName#">

<cfset caller.savedFileName = "#newname#">
