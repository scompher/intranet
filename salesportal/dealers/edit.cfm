<link rel="stylesheet" type="text/css" href="../../styles.css">

<cfsetting requesttimeout="300">

<cfif isdefined("form.updateDealer")>
	
	<cfparam name="form.executed" default="0">
		
	<cfset savedPresentation = "">
	<cfset savedQuote = "">
	<cfset savedSalesBook = "">
	
	<cftry>

	<cfquery name="saveDealerInfo" datasource="#ds#">
		update salesportal_dealers 
		set dealerName = '#form.dealerName#', dealerEmail = '#form.dealerEmail#', passcode = '#form.passcode#', expirationDate = #createodbcdate(form.expirationDate)#, executed = #form.executed#
		where dealerid = #form.dealerid#
	</cfquery>

	<cfif form.presentation is not "">
		<cf_savefile dealerid="#dealerid#" formfield="presentation" filetype="presentation" action="new">
		<cfset savedPresentation = savedFileName>
		<cfquery name="saveDealerInfo" datasource="#ds#">
			update salesportal_dealers 
			set presentation = '#savedPresentation#' 
			where dealerid = #dealerid#
		</cfquery>
	</cfif>
	<cfif form.quote is not "">
		<cf_savefile dealerid="#dealerid#" formfield="quote" filetype="quote" action="new">
		<cfset savedQuote = savedFileName>
		<cfquery name="saveDealerInfo" datasource="#ds#">
			update salesportal_dealers 
			set priceQuote = '#savedQuote#' 
			where dealerid = #dealerid#
		</cfquery>
	</cfif>
	<cfif form.salesbook is not "">
		<cf_savefile dealerid="#dealerid#" formfield="salesbook" filetype="salesbook" action="new">
		<cfset savedSalesbook = savedFileName>
		<cfquery name="saveDealerInfo" datasource="#ds#">
			update salesportal_dealers 
			set salesBook = '#savedSalesBook#'
			where dealerid = #dealerid#
		</cfquery>
	</cfif>

	<cflocation url="index.cfm">
	
	<cfcatch type="any">
		<div align="center">
		<table border="0" cellspacing="0" cellpadding="5" width="500">
			<tr>
				<td class="highlightbar"><b>Edit A Dealer </b></td>
			</tr>
			<tr>
				<td class="greyrowbottomnopadding">
				<table width="100%" border="0" cellspacing="0" cellpadding="5" class="grey">
					<tr>
						<td align="center">
						<br />
						An error has occurred while updating this dealer.
						<br />
						<br />
						<a style="text-decoration:underline;" href="javascript:history.go(-1);">Click here to go back and try again</a>
						<br />
						<br />
						<cfoutput>
						#cfcatch.Message#
						<br />
						<br />
						#cfcatch.Detail#
						<br />
						<br />
						</cfoutput>
						</td>
					</tr>
				</table>
				</td>
			</tr>
		</table>
		</div>
		<cfabort>
	</cfcatch>
	
	</cftry>
	
</cfif>

<script language="javascript" src="../cal2.js">
/*
Xin's Popup calendar script-  Xin Yang (http://www.yxscripts.com/)
Script featured on/available at http://www.dynamicdrive.com/
This notice must stay intact for use
*/
</script>
<script language="javascript" src="../cal_conf2.js"></script>

<cfif not isdefined("form.submitted")>
	<cfquery name="getInfo" datasource="#ds#">
		select * from salesportal_dealers
		where dealerid = #dealerid#
	</cfquery>
	<cfif getinfo.recordcount gt 0>
		<cfset form.acctmanagerid = getinfo.acctmanagerid>
		<cfset form.dealername = getinfo.dealername>
		<cfset form.dealeremail = getinfo.dealeremail>
		<cfset form.passcode = getinfo.passcode>
		<cfset form.expirationdate = dateformat(getinfo.expirationdate,'mm/dd/yyyy')>
		<cfset currentPresentation = getinfo.presentation>
		<cfset currentQuote = getinfo.priceQuote>
		<cfset currentSalesBook = getinfo.salesbook>
		<cfset form.executed = getinfo.executed>
	</cfif>
</cfif>

<cfparam name="form.acctmanagerid" default="0">
<cfparam name="form.dealerName" default="">
<cfparam name="form.dealerEmail" default="">
<cfparam name="form.passcode" default="">
<cfparam name="form.expirationDate" default="">
<cfparam name="form.executed" default="0">

<cfquery name="getAcctManagers" datasource="#ds#">
	select * 
	from admin_users
	inner join admin_users_departments_lookup on admin_users.adminuserid = admin_users_departments_lookup.adminuserid
	inner join admin_users_departments on admin_users_departments_lookup.departmentid = admin_users_departments.departmentid 
	where admin_users_departments.department = 'Sales' and admin_users.active <> 0
</cfquery>

<script type="text/javascript">
	function checkForm(frm) {
		if (frm.dealerName.value == '') {alert('The dealer name is required.'); frm.dealerName.focus(); return false;}
		if (frm.dealerEmail.value == '') {alert('The dealer email is required.'); frm.dealerEmail.focus(); return false;}
		if (frm.passcode.value == '') {alert('The passcode is required.'); frm.passcode.focus(); return false;}
		if (frm.expirationDate.value == '') {alert('The expiration date is required.'); frm.expirationDate.focus(); return false;}
		frm.submit();
	}
</script>

<div align="center">
<br>
<form method="post" action="edit.cfm" name="mainform" enctype="multipart/form-data">
<cfoutput>
<input type="hidden" name="dealerid" value="#dealerid#">
</cfoutput>
<input type="hidden" name="updateDealer" value="1" />
<input type="hidden" name="submitted" value="1">
<table border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Edit A Dealer </b></td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellpadding="5" cellspacing="0" class="grey">
				<tr>
					<td nowrap="nowrap">Account Manager:</td>
					<td>
					<select name="acctmanagerid">
						<option value="0"></option>
						<cfoutput query="getAcctManagers">
							<option <cfif form.acctmanagerid is getAcctManagers.adminuserid>selected</cfif> value="#adminuserid#">#firstname# #lastname#</option>
						</cfoutput>
					</select>
					</td>
				</tr>
				<tr>
					<td nowrap="nowrap">Dealer Name: </td>
					<td>
						<input name="dealerName" type="text" style="width:300px" value="<cfoutput>#form.dealerName#</cfoutput>" />
					</td>
				</tr>
				<tr>
					<td nowrap="nowrap">Dealer Email:</td>
					<td>
						<input name="dealerEmail" type="text" style="width:300px" value="<cfoutput>#form.dealerEmail#</cfoutput>" />
					</td>
				</tr>
				<tr>
					<td nowrap="nowrap">Portal Passcode: </td>
					<td>
						<input name="passcode" type="text" style="width:100px" value="<cfoutput>#form.passcode#</cfoutput>" />
					</td>
				</tr>
				<tr>
					<td nowrap="nowrap">Expiration Date: </td>
					<td class="nopadding">
						<table border="0" cellpadding="5" cellspacing="0" class="grey">
							<tr>
								<td>
									<input name="expirationDate" type="text" id="expirationDate" style="width:75px" value="<cfoutput>#form.expirationDate#</cfoutput>" />
								</td>
								<td align="center"><a style="text-decoration:none;" href="javascript:showCal('ExpirationDate');"> <img src="/images/calicon.gif" alt="Select a Date" align="absmiddle" width="20" height="20" border="0" /> </a></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td nowrap="nowrap">Custom Presentation: </td>
					<td>
						<cfif currentPresentation is not "">
							<cfoutput>
							<a target="_blank" href="#copalinkURL#/sales/files/#currentPresentation#">View</a> | <a href="javascript: if (confirm('Are you sure you wish to delete this presentation?')) document.location='deleteFile.cfm?dealerid=#dealerid#&file=#currentPresentation#&filetype=presentation';">Delete</a>
							</cfoutput>
							<input type="hidden" name="presentation" />
						<cfelse>
							<input name="presentation" type="file" id="presentation" />
						</cfif>
					</td>
				</tr>
				<tr>
					<td nowrap="nowrap">Custom Price Quote: </td>
					<td>
						<cfif currentQuote is not "">
							<cfoutput>
							<a target="_blank" href="#copalinkURL#/sales/files/#currentQuote#">View</a> | <a href="javascript: if (confirm('Are you sure you wish to delete this price quote?')) document.location='deleteFile.cfm?dealerid=#dealerid#&file=#currentQuote#&filetype=quote';">Delete</a>
							</cfoutput>
							<input type="hidden" name="quote" />
						<cfelse>
							<input name="quote" type="file" id="quote" />
						</cfif>
					</td>
				</tr>
				<tr>
					<td nowrap="nowrap">Custom Sales Book: </td>
					<td>
						<cfif currentSalesBook is not "">
							<cfoutput>
							<a target="_blank" href="#copalinkURL#/sales/files/#currentSalesbook#">View</a> | <a href="javascript: if (confirm('Are you sure you wish to delete this sales book?')) document.location='deleteFile.cfm?dealerid=#dealerid#&file=#currentSalesBook#&filetype=salesbook';">Delete</a>
							</cfoutput>
							<input type="hidden" name="salesBook" />
						<cfelse>
							<input name="salesBook" type="file" id="salesBook" />
						</cfif>
					</td>
				</tr>
				<tr>
					<td nowrap="nowrap">Executed:</td>
					<td><input name="executed" type="checkbox" <cfif form.executed is 1>checked</cfif> value="1" /></td>
				</tr>
				<tr>
					<td colspan="2">
						<input style="height:20px" type="button" class="sidebar" value="Update Dealer" onclick="checkForm(this.form);" />
						<input style="height:20px" type="button" class="sidebar" value="Cancel" onclick="document.location='index.cfm';" />
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</form>
<br>
</div>
