
<link rel="stylesheet" type="text/css" href="../styles.css">

<cfparam name="startDate" default="">
<cfparam name="endDate" default="">
<cfparam name="dealerNumber" default="">

<cfif isDefined("form.btnGetReport")>
	<cfquery name="getreport" datasource="copalink">
		select * 
		from AccountDeletions_actions 
		where 
		<cfif trim(dealernumber) is not "">
		dealernumber = '#dealernumber#' and	
		</cfif> 
		<cfif trim(startDate) is not "">
			actionDateTime >= #createodbcdate(startDate)# and 
		</cfif>
		<cfif trim(endDate) is not "">
			actionDateTime <= #createodbcdate(dateadd("d",1,endDate))#  and 
		</cfif>
		1 = 1
		order by actionDateTime ASC 
	</cfquery>
</cfif>

<script language="javascript" src="cal2.js">
/*
Xin's Popup calendar script-  Xin Yang (http://www.yxscripts.com/)
Script featured on/available at http://www.dynamicdrive.com/
This notice must stay intact for use
*/
</script>
<script language="javascript" src="cal_conf2.js"></script>

<div align="center">
<table width="722" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Account Deletion Activity Report</b></td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td align="center" class="nopadding">
						<table border="0" cellspacing="0" cellpadding="5">
							<form method="post" action="index.cfm">
							<tr>
								<td><b>Enter Dealer Number: </b></td>
								<td colspan="2">
									<input style="width:40px;" name="dealernumber" type="text" maxlength="4" value="<cfoutput>#dealerNumber#</cfoutput>">
								</td>
							</tr>
							<tr>
								<td><b>Enter Date Range:  </b></td>
								<td class="nopadding">
									<table width="100%" border="0" cellspacing="0" cellpadding="5">
										<tr>
											<td>Start:</td>
											<td>
												<input style="width:75px" name="startDate" type="text" value="<cfoutput>#startDate#</cfoutput>">
											</td>
											<td><a style="text-decoration:none;" href="javascript:showCal('StartDate');"> <img src="/images/calicon.gif" alt="Select a Date" width="20" height="20" border="0" align="absmiddle" class="lightbox" /> </a></td>
										</tr>
									</table>
								</td>
								<td class="nopadding">
									<table width="100%" border="0" cellspacing="0" cellpadding="5">
										<tr>
											<td>End:</td>
											<td>
												<input style="width:75px" name="endDate" type="text" value="<cfoutput>#endDate#</cfoutput>">
											</td>
											<td><a style="text-decoration:none;" href="javascript:showCal('EndDate');"><img src="/images/calicon.gif" width="20" height="20" border="0" class="lightbox"></a></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td colspan="3">
									<input name="btnGetReport" type="submit" class="sidebar" value="Get Report">
								</td>
							</tr>
							</form>
						</table>
					</td>
				</tr>
				<cfif isDefined("form.btnGetReport")>
				<tr>
					<td class="nopadding">
						<table width="100%" border="0" cellspacing="0" cellpadding="5">
							<tr>
								<td colspan="2" class="highlightbartopbottomborderonly"><b>Report Details</b> </td>
							</tr>
							<tr>
								<td width="25%" align="center" nowrap class="linedrow"><b>Date/Time</b></td>
								<td class="linedrow"><b>Description</b></td>
							</tr>
							<cfoutput query="getReport">
							<tr>
								<td align="center" nowrap class="linedrow">#dateformat(actionDateTime,'mm/dd/yyyy')# #timeformat(actionDateTime,'hh:mm:ss tt')#</td>
								<td class="linedrow">#actionDescription#</td>
							</tr>
							</cfoutput>
						</table>
					</td>
				</tr>
				</cfif>
			</table>
		</td>
	</tr>
</table>
<br />
<a class="normal" style="text-decoration:underline;" href="/index.cfm">Return to Intranet</a>
</div>
