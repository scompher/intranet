
<link rel="stylesheet" type="text/css" href="../styles.css">

<!--- upload files --->
<cfif isDefined("form.btnprocess")>
	<cfset uploadDir = "E:\websites\copalink\scheduled\authorityUpdate">
	
	<!--- upload police file --->	
	<cfif trim(form.Police) is not ""> <cffile action="upload" filefield="Police" destination="#uploadDir#" nameconflict="overwrite"> </cfif>
	<!--- upload fire file --->
	<cfif trim(form.Fire) is not ""> <cffile action="upload" filefield="Fire" destination="#uploadDir#" nameconflict="overwrite"> </cfif>
	<!--- upload medical file --->
	<cfif trim(form.Medical) is not ""> <cffile action="upload" filefield="Medical" destination="#uploadDir#" nameconflict="overwrite"> </cfif>
	<br />
	<br />
	<div align="center" class="normal">
		<b>Files uploaded successfully </b>
		<br />
		<!---<br />
		<a style="text-decoration:underline;" href="http://192.168.1.7/scheduled/authorityUpdate/updateAuthorities.cfm">Click here to begin processing files</a>
		<br />--->
		<br />
	</div>	
	
	<cfabort>
</cfif>

<!---  10/12 SW - New application to load the Authority Information (Police/Fire/Medical) from PICK --->
<div align="center">

<cfform method="post" action="index.cfm" enctype="multipart/form-data">
<table border="0" cellpadding="5" cellspacing="0">
		<tr>
			<td class="highlightbar"><b>COPS Authority Upload</b></td>
		</tr>
		<tr>
		<td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
                	<td nowrap="nowrap"><b>Police File: </b></td>
					<td>
					<cfinput type="file" name="Police" style="width:400px; background-color:##FFFFFF;">
					</td>
                </tr>
				<tr>
                	<td nowrap="nowrap"><b>Fire File: </b></td>
					<td>
					<cfinput type="file" name="Fire" style="width:400px; background-color:##FFFFFF;">
					</td>
                </tr>
				<tr>
                	<td nowrap="nowrap"><b>Medical File: </b></td>
					<td>
					<cfinput type="file" name="Medical" style="width:400px; background-color:##FFFFFF;">
					</td>
                </tr>
				<tr>
					<td colspan="2">
						<cfinput type="submit" name="btnProcess" id="Process" value="Upload Files"> &nbsp;
					</td>
				</tr>
			</table>
		</td>
		</tr>
</table>
</cfform>
<br />
<a class="normal" style="text-decoration:underline;" href="/index.cfm">Return to Intranet</a>
</div>