<style type="text/css">
	TABLE {font-family:Arial, Helvetica, sans-serif, sans-serif;font-size:12px}
	.normal {font-family:Arial, Helvetica, sans-serif, sans-serif;font-size:12px}
	.heading {font-family:Arial, Helvetica, sans-serif;font-size:12px}
	.titlebar {font-family: Arial, Helvetica, sans-serif;	font-size: 12px;font-weight: bold;color: #FFFFFF;}
</style>

<cfquery name="getallstatus" datasource="#ds#">
	select * from tasktracker_status
	order by description asc
</cfquery>

<div align="center">
<table border="0" cellpadding="5" cellspacing="0">
    <tr>
        <td width="250"><b>Item</b></td>
        <td align="center"><b>Action</b></td>
    </tr>
    <tr>
        <td colspan="2"><hr size="1"></td>
    </tr>
	<cfif getallstatus.recordcount GT 0>
	<cfoutput query="getallstatus">
	<cfif getallstatus.currentrow MOD 2 IS 0>
    <tr>
	<cfelse>
	<tr bgcolor="##eeeeee">
	</cfif>
        <td width="250">#getallstatus.description#</td>
        <td align="center"><a href="edit.cfm?id=#getallstatus.statusid#">Edit</a> | <a onClick="return confirm('Are you sure you wish to delete this item?');" href="delete.cfm?id=#getallstatus.statusid#">Delete</a> </td>
    </tr>
	</cfoutput>
	<cfelse>
    <tr>
        <td colspan="2">There are no status items currently on file</td>
    </tr>	
	</cfif>
    <tr>
        <td colspan="2"><hr size="1"></td>
    </tr>
    <tr>
        <td width="250" align="center"><a href="/index.cfm">Return to Administrative Menu</a></td>
        <td align="center"><a href="add.cfm">Add New Status</a></td>
    </tr>
</table>
</div>
