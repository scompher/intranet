
<cfparam name="msg" default="">

<cfif isdefined("form.btnContinue")>
	<cflocation url="add2.cfm?taskid=#taskid#">
	<cfabort>
</cfif>

<cfif isdefined("form.btnUpdateStatus")>
	<cfquery name="updatestatus" datasource="#ds#">
	<cfloop index="status" list="#form.statusid#">
	<cfset sid = listgetat(status,1,":")>
	<cfset uid = listgetat(status,2,":")>
	begin
		update tasktracker_task_user_lookup
		set statusid = #sid#
		where taskid = #taskid# and adminuserid = #uid#
	end
	</cfloop>
	</cfquery>
	<cfset msg = "Status Updated">
</cfif>

<cfquery name="gettask" datasource="#ds#">
	select tasktracker_task_user_lookup.taskid, tasktracker_task_user_lookup.adminuserid, tasktracker_task_user_lookup.statusid, admin_users.firstname, admin_users.lastname, tasktracker_status.description as status
	from tasktracker_task_user_lookup
	inner join admin_users on tasktracker_task_user_lookup.adminuserid = admin_users.adminuserid
	inner join tasktracker_status on tasktracker_task_user_lookup.statusid = tasktracker_status.statusid
	where tasktracker_task_user_lookup.taskid = #taskid#
</cfquery>

<cfquery name="getstatus" datasource="#ds#">
	select * from tasktracker_status order by description asc
</cfquery>

<div align="center">
<form method="post" action="setstatus.cfm">
<cfoutput>
<input type="hidden" name="taskid" value="#taskid#">
</cfoutput>
<table border="1" cellpadding="5" cellspacing="0">
	<tr>
		<td colspan="2" align="center" valign="middle" bgcolor="#dddddd"><b>Assign Initial Status</b></td>
	</tr>
	<cfif trim(msg) IS NOT "">
	<cfoutput>
	<tr>
		<td colspan="2" align="center" valign="middle" bgcolor="##ffffcc">#msg#</td>
	</tr>
	</cfoutput>
	</cfif>
	<cfloop query="gettask">
	<cfset uid = gettask.adminuserid>
	<cfset sid = gettask.statusid>
	<tr>
		<td><cfoutput>#gettask.firstname# #gettask.lastname#</cfoutput></td>
		<td>
		<select name="statusid" class="normal">
		<cfoutput query="getstatus">
			<option value="#getstatus.statusid#:#uid#" <cfif sid IS getstatus.statusid>selected</cfif> >#getstatus.description#</option>
		</cfoutput>
		</select>
		<input name="btnUpdateStatus" type="submit" class="normal" value="Update">
		</td>
	</tr>
	</cfloop>
	<tr>
		<td colspan="2"><input type="submit" name="btnContinue" value="Continue"></td>
	</tr>
</table>
</form>
</div>
