
<cfif isDefined("form.btnFinished") OR isdefined("url.quickentry")>

<cfif isdefined("url.quickentry")>
	<cflocation url="add2_save.cfm?taskid=#taskid#&quickentry=1">
<cfelse>
	<cflocation url="add2_save.cfm?taskid=#taskid#">
</cfif>


<cfelse>

	<cfquery name="getinfo" datasource="#ds#">
		select * from tasktracker_task_user_lookup where taskid = #taskid#
	</cfquery>
	<cfset assignedusers = valuelist(getinfo.adminuserid)>
	
	<cfif assignedusers IS 0 OR (assignedusers IS cookie.adminlogin AND getsec.seclevelid GT 2)>
		<cflocation url="add2_save.cfm?taskid=#taskid#">
	<cfelse>
		<cfinclude template="assignpriorities.cfm">
	</cfif>

</cfif>