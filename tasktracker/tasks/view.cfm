
<link rel="stylesheet" type="text/css" href="../../styles.css">

<cfparam name="form.HTTP_REFERER" default="#HTTP_REFERER#">

<cfif isdefined("form.statusid")>
	<cfset statusupdated = createodbcdatetime(now())>
	<!--- update status --->
	<cfquery name="updateStatus" datasource="#ds#">
		update tasktracker_task_user_lookup
		set statusupdated = #statusupdated#, statusid = #listgetat(form.statusid,2,":")#<cfif (listgetat(form.statusid,2,":") IS 1) OR (listgetat(form.statusid,2,":") IS 5)>, priority = 0</cfif>
		where taskid = #taskid# and adminuserid = #listgetat(form.statusid,1,":")#
	</cfquery>

	<!--- renumber priorities --->
	<cfquery name="getpriorities" datasource="#ds#">
		select taskid, priority, adminuserid 
		from tasktracker_task_user_lookup 
		where adminuserid = #listgetat(form.statusid,1,":")# and statusid NOT IN (1,5)
		order by adminuserid, priority asc
	</cfquery>

	<!--- if statusid = 5 (unassigned) then un-link user --->
	<cfif form.statusid IS 5>
		<cfquery name="unlinkuser" datasource="#ds#">
			update tasktracker_task_user_lookup
			set adminuserid = 0
			where taskid = #taskid#
		</cfquery>
	</cfif>
	
	<cfset p = 1>
	<cfset curruser = 0>
	<cfloop query="getpriorities">
		<cfif curruser IS NOT adminuserid><cfset p = 1><cfset curruser = getpriorities.adminuserid></cfif>
		<cfquery name="updatepriority" datasource="#ds#">
			update tasktracker_task_user_lookup
			set	priority = #p#
			where taskid = #taskid# and adminuserid = #adminuserid#
		</cfquery>
		<cfset p = p + 1>
	</cfloop>

	<!--- send status change notification --->
	<cfquery name="getoriginator" datasource="#ds#">
		select admin_users.email, admin_users.firstname, admin_users.adminuserid
		from admin_users
		inner join tasktracker_tasks on tasktracker_tasks.originatorid = admin_users.adminuserid
		where tasktracker_tasks.taskid = #taskid#
	</cfquery>
	<cfquery name="getusers" datasource="#ds#">
		select admin_users.email, admin_users.firstname
		from admin_users
		inner join tasktracker_task_user_lookup on tasktracker_task_user_lookup.adminuserid = admin_users.adminuserid
		where tasktracker_task_user_lookup.taskid = #taskid# <cfif isdefined("cookie.adminlogin")>and admin_users.adminuserid <> #cookie.adminlogin#</cfif>
	</cfquery>
	<cfset emaillist = valuelist(getusers.email)>
	<cfif cookie.adminlogin IS NOT getoriginator.adminuserid>
		<cfset emaillist = listappend(emaillist, getoriginator.email)>
	</cfif>

<!--- send emails --->
<cfloop index="email" list="#emaillist#">
<cfmail from="pgregory@copsmonitoring.com" to="#email#" subject="Task Manager Notification : Task Status Change" username="copalink@copsmonitoring.com" password="copsmoncal">

The status for the following task has been changed:

#form.title# 
#form.description# 

You may view the details of this task by clicking on the following link:

#request.appurl#tasktracker/tasks/view.cfm?id=#taskid#
</cfmail>
</cfloop>
</cfif>

<cfif NOT isDefined("taskid") AND isDefined("id")>
	<cfparam name="taskid" default="#id#">
</cfif>
<cfquery name="getstatus" datasource="#ds#">
	select * from tasktracker_status
	order by description asc
</cfquery>
<cfquery name="GetBasicInfo" datasource="#ds#">
	select tasktracker_tasks.*, tasktracker_categories.category, admin_users.firstname, admin_users.lastname
	from tasktracker_tasks
	left join admin_users on tasktracker_tasks.originatorID = admin_users.adminuserid
	left join tasktracker_categories on tasktracker_tasks.categoryid = tasktracker_categories.categoryid
	where taskid = #taskid#
</cfquery>
<cfquery name="GetUserInfo" datasource="#ds#">
	select tasktracker_task_user_lookup.*, admin_users.*, tasktracker_status.description as status
	from tasktracker_task_user_lookup, admin_users, tasktracker_status
	where taskid = #taskid# and tasktracker_task_user_lookup.adminuserid = admin_users.adminuserid and tasktracker_task_user_lookup.statusid = tasktracker_status.statusid
</cfquery>
<cfquery name="GetNoteInfo" datasource="#ds#">
	select * 
	from tasktracker_notes, admin_users
	where taskid = #taskid# and tasktracker_notes.creatorid = admin_users.adminuserid
	order by created desc
</cfquery>

<cfoutput>
<script language="JavaScript">
<!--
function addnote() {
	notewin = window.open("addnote.cfm?id=#taskid#","notewin","width=550,height=220");
	notewin.focus();
}
function editnote(notesid) {
	notewin = window.open("editnote.cfm?id=" + notesid,"notewin","width=550,height=220");
	notewin.focus();
}
function notehist(notesid) {
	notewin = window.open("notehist.cfm?id=" + notesid,"notewin","width=600,height=400,resizable=yes,scrollbars=yes");
	notewin.focus();
}
function gettimespent(frm, uid) {
	if (frm.statusid.options[frm.statusid.selectedIndex].text == "Completed") {
		timespentwin = window.open("gettimespent.cfm?id=#taskid#&u=" + uid,"timespentwin","width=550,height=220");
		timespentwin.focus();
	}
	else {
		frm.submit();
	}
}
//-->
</script> 
</cfoutput>

<div align="center">
    <table width="900" border="0" cellpadding="5" cellspacing="0">
		<cfoutput query="getbasicinfo">
        <tr>
            <td class="highlightbar" colspan="3"><b>Project Sponsor : #getbasicinfo.sponsor#</b></td>
        </tr>
        <tr>
            <td class="greyrow" colspan="3">#getbasicinfo.sponsor#&nbsp;</td>
        </tr>
        <tr>
            <td class="highlightbar" colspan="3"><b>Task : #getbasicinfo.title#</b></td>
        </tr>
        <tr>
            <td class="greyrow" colspan="3">#replace(getbasicinfo.description,"#chr(13)#","<br>","all")#</td>
        </tr>
		<tr>
			<td class="greyrow" colspan="3">&nbsp;</td>
		</tr>
		<cfif trim(getbasicinfo.duedate) IS NOT "">
			<tr>
				<td class="highlightbar" colspan="3"><b>Due Date:</b></td>
			</tr>
			<tr>
				<td class="greyrow" colspan="3"><cfif datediff("d",getbasicinfo.duedate,now()) GT 0><span style="color:ff0000;"><cfelse><span></cfif>#dateformat(getbasicinfo.duedate,'mm/dd/yyyy')#</span></td>
			</tr>		
			<tr>
				<td class="greyrow" colspan="3">&nbsp;</td>
			</tr>
		</cfif>
		<tr>
			<td class="highlightbar" colspan="3"><b>Assigned By:</b></td>
		</tr>
		<tr>
			<td class="greyrow" colspan="3">#getbasicinfo.firstname# #getbasicinfo.lastname#</td>
		</tr>
		<tr>
			<td class="greyrow" colspan="3">&nbsp;</td>
		</tr>
		<cfif trim(getbasicinfo.category) IS NOT "">
			<tr>
				<td class="highlightbar" colspan="3"><b>Category:</b></td>
			</tr>
			<tr>
				<td class="greyrow" colspan="3">#getbasicinfo.category#</td>
			</tr>
			<tr>
				<td class="greyrow" colspan="3">&nbsp;</td>
			</tr>
			</cfif>
		</cfoutput>
		<cfif getuserinfo.recordcount GT 0>
        <tr>
            <td class="highlightbar" style="border-right:0px;"><b>Assigned Users:</b></td>
            <td class="highlightbar" style="border-left:0px; border-right:0px;"><b>Current Status:</b></td>
            <td class="highlightbar" style="border-left:0px;"><b>Priority and Tasks:</b></td>
        </tr>
		</cfif>
		<cfloop query="getuserinfo">
		<cfquery name="gettasks" datasource="#ds#">
			select tasktracker_tasks.title, tasktracker_tasks.taskid as tid, tasktracker_task_user_lookup.priority
			from tasktracker_tasks, tasktracker_task_user_lookup
			where tasktracker_tasks.taskid = tasktracker_task_user_lookup.taskid and tasktracker_task_user_lookup.adminuserid = #getuserinfo.adminuserid# and tasktracker_task_user_lookup.statusid NOT IN (1,5)
			order by tasktracker_task_user_lookup.priority asc
		</cfquery>
		<cfset sid = getuserinfo.statusid>
		<cfset userid = getuserinfo.adminuserid>
		<form method="post" name="statusform" action="view.cfm">
        <tr>
            <td  class="greyrow" style="border-right:0px;" valign="top"><cfoutput><a href="viewbyuser.cfm?id=#userid#">#getuserinfo.firstname# #getuserinfo.lastname#</a></cfoutput></td>
            <td class="greyrow" style="border-left:0px; border-right:0px;" valign="top">
			<cfif isDefined("cookie.adminlogin")>
				<cfinclude template="/auth.cfm">
				<cfif cookie.adminlogin IS userid OR (getsec.recordcount gt 0 and getsec.seclevelid LTE 2)>
					<cfoutput>
						<input type="hidden" name="HTTP_REFERER" value="#form.HTTP_REFERER#">
						<input type="hidden" name="taskid" value="#taskid#">
						<input type="hidden" name="title" value="#getbasicinfo.title#">
						<input type="hidden" name="description" value="#getbasicinfo.description#">
					</cfoutput>
					<select name="statusid" class="normal">
					<cfloop query="getstatus">
					<cfoutput>
						<option value="#userid#:#getstatus.statusid#" <cfif sid IS getstatus.statusid>selected</cfif> >#getstatus.description#</option>
					</cfoutput>
					</cfloop>
					</select>
					<cfoutput>
					<!--- <input onClick="gettimespent(this.form,'#userid#');" type="button" value="Update" class="normal"> --->
					<input type="submit" value="Update" class="normal">
					<cfif trim(getuserinfo.timespent) IS NOT "" AND sid IS 1>&nbsp;&nbsp;Time Spent: #getuserinfo.timespent#</cfif></form>
					</cfoutput>
				<cfelse>
					<cfoutput>
						#getuserinfo.status#
					</cfoutput>				
				</cfif>
			<cfelse>
			<cfoutput>
				#getuserinfo.status#
			</cfoutput>
			</cfif>
			</td>
            <td class="greyrow" style="border-left:0px;" valign="top">
			<cfoutput query="gettasks">
				<cfif gettasks.tid IS taskid>
					<b>#gettasks.priority#.&nbsp;&nbsp;#gettasks.title#</b><br>
				<cfelse>
					#gettasks.priority#.&nbsp;&nbsp;<a href="view.cfm?id=#gettasks.tid#">#gettasks.title#</a><br>
				</cfif>
			</cfoutput>&nbsp;
			</td>
        </tr>
		</cfloop>
        <tr>
            <td class="greyrow" colspan="3" valign="top">&nbsp;</td>
        </tr>
        <tr>
            <td class="highlightbar" colspan="3" valign="top"><b>Notes:</b></td>
        </tr>
		<cfoutput query="getnoteinfo">
        <tr>
            <td class="greyrow" colspan="3" valign="top">
			<b>#getnoteinfo.firstname# #getnoteinfo.lastname# : Created #dateformat(created, 'mm/dd/yyyy')# #timeformat(created, 'hh:mm tt')#<cfif trim(getnoteinfo.edited) IS NOT ""> : Edited #dateformat(edited, 'mm/dd/yyyy')# #timeformat(edited, 'hh:mm tt')# : <a href="javascript:notehist(#getnoteinfo.notesid#);">Click to View Edit History</a></cfif></b>&nbsp;
			<cfif isdefined("cookie.adminlogin")><cfif getnoteinfo.adminuserid IS cookie.adminlogin><a href="javascript:editnote(#notesid#);">[Edit Note]</a></cfif></cfif>
			</td>
        </tr>
        <tr>
            <td class="greyrow" colspan="3" valign="top">#replace(content,"#chr(13)#","<br>","all")#</td>
        </tr>
        <tr>
            <td class="greyrow" colspan="3" valign="top">&nbsp;</td>
        </tr>
		</cfoutput>
		<form>
        <tr>
            <td class="greyrowbottom" colspan="3" valign="top">
			<cfif isDefined("cookie.adminlogin")>
			<input onClick="addnote();" type="button" value="Add Note">
			<cfelse>
			<b>You must be logged in to notate or update a task</b>
			</cfif>
			</td>
        </tr>
		</form>
		<cfif isDefined("cookie.adminlogin")>
		<!--- 
		<cfoutput>
        <tr align="center" bgcolor="##dddddd">
            <td colspan="3" valign="top"><a href="#form.HTTP_REFERER#">Return to Previous Page</a> | <a href="add.cfm">Add New Task</a></td>
        </tr>
		</cfoutput>
		--->
		<tr>
			<td colspan="3" align="center"><a href="add.cfm">Add New Task</a></td>
		</tr>
		<cfelse>
		<cfoutput>
        <tr align="center" bgcolor="##dddddd">
            <td colspan="3" valign="top"><a href="/index.cfm">Log In</a></td>
        </tr>
		</cfoutput>
        </cfif>
    </table>
	<p>
	<cfinclude template="footer.cfm">
</div>

