
<cfif isDefined("s")>

	<cfquery name="gettask" datasource="#ds#">
		select tasktracker_tasks.title, tasktracker_tasks.description, tasktracker_task_user_lookup.adminuserid
		from tasktracker_tasks
		inner join tasktracker_task_user_lookup on tasktracker_tasks.taskid = tasktracker_task_user_lookup.taskid
		where tasktracker_tasks.taskid = #taskid#
	</cfquery>
	<cfset assignedusers = valuelist(gettask.adminuserid)>
	
	<cfset s = replace(s,"#chr(19)#","-","all")>
	<cfset s = replace(s,"#chr(20)#","-","all")>

	<cfwddx action="wddx2cfml" input="#s#" output="qSnapshot">
	
	<cfquery name="s2" datasource="#ds#">
		select tasktracker_tasks.title, tasktracker_tasks.taskid, tasktracker_task_user_lookup.priority, tasktracker_task_user_lookup.adminuserid
		from tasktracker_tasks, tasktracker_task_user_lookup
		where tasktracker_tasks.taskid = tasktracker_task_user_lookup.taskid and tasktracker_task_user_lookup.adminuserid IN (#assignedusers#) and tasktracker_task_user_lookup.priority <> 0
		order by adminuserid, priority ASC
	</cfquery>

	<cfloop index="id" list="#assignedusers#">
		<cfquery name="checkpcurrent" dbtype="query">
			select taskid, adminuserid, priority
			from s2
			where adminuserid = #id#
		</cfquery>
		<cfquery name="checkpsnapshot" dbtype="query">
			select taskid, adminuserid, priority
			from qSnapshot
			where adminuserid = #id#
		</cfquery>
		<cfwddx action="cfml2wddx" input="#checkpcurrent#" output="cpc">
		<cfwddx action="cfml2wddx" input="#checkpsnapshot#" output="cps">
		<cfif cpc IS NOT cps>
		<cfquery name="getemail" datasource="#ds#">
			select * from admin_users where adminuserid = #id#
		</cfquery>
<cfmail from="pgregory@copsmonitoring.com" to="#getemail.email#" subject="Task Manager Notification : Task priorities have been changed" username="copalink@copsmonitoring.com" password="copsmoncal">
Your task priorities have changed.  Here are your current priorities:

<cfloop query="s2">
#priority#. #title#

</cfloop>
</cfmail>
		</cfif>
	</cfloop>
</cfif>

<cflocation url="index.cfm">

<!--- 
<div align="center">
<table border="0" cellpadding="0" cellspacing="5">
<tr>
	<td>
	<b>Task Successfully Updated</b>
	<br>
	<br>
	<a href="index.cfm">Return to the Task Menu</a>
	</td>
</tr>
</table>
</div> --->

