<body onBlur="focus();">

<cfquery name="getstatus" datasource="#ds#">
	select * from tasktracker_status
	order by description ASC
</cfquery>

<script language="JavaScript">
<!--
function applyfilter(frm, qString) {

	var qArray = qString.split("&");
	var urlstring = "";

	for (var i=0;i<frm.statusid.length;i++) {
		if (frm.statusid[i].checked) {
			urlstring += frm.statusid[i].value;
			urlstring += ",";
		}
		var output = urlstring.slice(0,urlstring.lastIndexOf(","));
	}
	
	var outputstring = "";
	
	for (var i=0;i<qArray.length;i++) {
		var itemArray = qArray[i].split("=");
		if (itemArray[0] == "p") {
			var page = itemArray[1];			
		} else 	if (itemArray[0] == "f") {
			var f = "f=" + output;
			outputstring += f + "&";
		} else {
			outputstring += qArray[i] + "&";
		}
		var tmp = outputstring.slice(0,outputstring.lastIndexOf("&"));
	}

	if (output != "") {
		self.opener.location = page + "?" + tmp;
		self.close();
	}
}
//-->
</script> 

<form method="post" action="taskfilter.cfm">
<table border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td><b>Show only items with the selected status:</b></td>
	</tr>
	<tr>
	    <td><table border="0" cellpadding="5" cellspacing="0">
            <tr>
				<cfoutput query="getstatus">
                <td>
				<input type="checkbox" name="statusid" value="#getstatus.statusid#" <cfif listfind(f,statusid) IS NOT 0>checked</cfif> >
				&nbsp;#getstatus.description#
				</td>
				</cfoutput>
            </tr>
        </table></td>
    </tr>
	<cfoutput>
	<tr>
		<td>
		<input onClick="applyfilter(this.form, '#cgi.QUERY_STRING#');" type="button" value="Apply Filter" class="normal">
		</td>
	</tr>
	</cfoutput>
</table>
</form>
</body>

