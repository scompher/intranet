
<link rel="stylesheet" type="text/css" href="../../styles.css">

<cfif isDefined("form.btnContinue")>

	<cfinclude template="edit_save.cfm">

<cfelse>
	
	<!--- get task --->
	<cfquery name="gettask" datasource="#ds#">
		select tasktracker_tasks.sponsor, tasktracker_tasks.taskid, tasktracker_tasks.duedate, tasktracker_tasks.title, tasktracker_tasks.description, tasktracker_tasks.categoryid, tasktracker_task_user_lookup.adminuserid, Admin_Users_Departments_Lookup.departmentid
		from tasktracker_tasks
		inner join tasktracker_task_user_lookup on tasktracker_tasks.taskid = tasktracker_task_user_lookup.taskid
		left join Admin_Users_Departments_Lookup on tasktracker_task_user_lookup.adminuserid = Admin_Users_Departments_Lookup.adminuserid
		where tasktracker_tasks.taskid = #id#
	</cfquery>
	<cfset assignedlist = valuelist(gettask.adminuserid)>
	<cfset deptid = gettask.departmentid>
	<cfset catid = gettask.categoryid>
	
	<!--- find what departments user has access to --->
	<cfquery name="getdeptaccess" datasource="#ds#">
		select departmentid
		from admin_users_departments_lookup
		where adminuserid = #cookie.adminlogin#
	</cfquery>
	<cfset deptlist = valuelist(getdeptaccess.departmentid)>

	<cfquery name="getdepts" datasource="#ds#">
		select * from Admin_Users_Departments
		where Admin_Users_Departments.departmentid IN (#deptlist#)
		order by department asc
	</cfquery>

	<!--- get users in departments --->
	<cfquery name="getusers" datasource="#ds#">
		select distinct admin_users.*, admin_users_departments_lookup.departmentid
		from admin_users, admin_users_departments_lookup
		where admin_users.adminuserid = admin_users_departments_lookup.adminuserid and admin_users_departments_lookup.departmentid IN (#deptlist#)
		order by admin_users.lastname asc, admin_users.firstname asc
	</cfquery>
	
	<cfquery name="getcats" datasource="#ds#">
		select * from tasktracker_categories
		order by category asc
	</cfquery>

<cfoutput>
<script language="javascript" src="cal2.js">
/*
Xin's Popup calendar script-  Xin Yang (http://www.yxscripts.com/)
Script featured on/available at http://www.dynamicdrive.com/
This notice must stay intact for use
*/
</script>
<script language="javascript" src="cal_conf2.js"></script>
<script language="JavaScript">
<!--
function filterusers (department,users)
{
	var userArray = new Array(#getusers.recordcount#);
	<cfset count = 0>
	<cfloop query="getusers">
		<cfif listfind(assignedlist,getusers.adminuserid) IS NOT 0><cfset sel = true><cfelse><cfset sel = false></cfif>
		userArray[#count#] = "#getusers.lastname#, #getusers.firstname#:#getusers.adminuserid#:#getusers.departmentid#:#sel#";
		<cfset count = count + 1>
	</cfloop>
	
	var deptid = department.options[department.selectedIndex].value;

	users.options.length = 0;

	var j=0;
	var i = 0;
	while (i < userArray.length) {
		var temparray = userArray[i].split(":");
		if (temparray[2] == deptid) {
			users.options[j] = new Option(temparray[0],temparray[1]);
			if (temparray[3] == "true") {
				users.options[j].selected = true;
			}
			j++
		}
		i++;
	}

}
//-->
</script> 
</cfoutput>

	<div align="center">
	<form method="post" action="edit.cfm">
	<cfoutput>
	<input type="hidden" name="id" value="#id#">
	<input type="hidden" name="taskid" value="#id#">
	<input type="hidden" name="assignedlist" value="#assignedlist#">
	</cfoutput>
	<table border="0" cellpadding="5" cellspacing="0">
		<tr align="center">
			<td class="highlightbar"><b>Edit A Task</b></td>
		</tr>
		<tr>
			<td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td valign="middle">
					<b>Category:</b><br>
					<select name="categoryid" class="normal">
						<option value="0"></option>
					<cfoutput query="getcats">
						<option value="#getcats.categoryid#" <cfif variables.catid IS getcats.categoryid>selected</cfif> >#getcats.category#</option>
					</cfoutput>
					</select>
					</td>
				</tr>
				<cfoutput query="gettask" group="taskid">
				<tr>
					<td valign="middle"><b>Project Sponsor:</b><br>
					<input name="sponsor" type="text" class="normal" size="58" maxlength="100" value="#gettask.sponsor#">
					</td>
				</tr>
				<tr>
					<td valign="top"><b>Title:<br>
						<input name="title" type="text" class="normal" size="58" maxlength="100" value="#gettask.title#">
					</b></td>
				</tr>
				<cfif getsec.seclevelid LTE 3>
				<tr>
					<td valign="middle">
					<b>Due Date:</b> (optional)<br>
					<input onFocus="if (this.value.length > 0) this.select();" name="duedate" type="text" class="normal" size="12" maxlength="10"<cfif trim(gettask.duedate) IS NOT ""> value="#dateformat(gettask.duedate,'mm/dd/yyyy')#"</cfif>>&nbsp;
					<a style="text-decoration:none;" href="javascript:showCal('TaskCalendarDueDate');"><img align="absmiddle" border="0" src="/images/calicon.gif"></a>
					</td>
				</tr>
				</cfif>
				<tr>
					<td valign="top"><b>Description:<br>
						<textarea name="description" cols="57" rows="5" class="normal">#gettask.description#</textarea>
					</b></td>
					</tr>
				</cfoutput>
				<tr>
					<td>
					<b>Select Department:</b><br>
					<select name="departmentid" class="normal" style="width:150px" onChange="filterusers(this.form.departmentid,this.form.assignedUsers);">
						<option value="0">None</option>
					<cfoutput query="getdepts">
						<option value="#getdepts.departmentid#" <cfif variables.deptid IS getdepts.departmentid>selected</cfif> >#department#</option>
					</cfoutput>
					</select>
					</td>
				</tr>
				<tr>
					<td valign="top"><b>Assigned To:</b><br>
						<select name="assignedUsers" size="10" multiple class="normal" style="width:250px">
							<cfoutput query="getusers">
								<cfif getusers.departmentid IS variables.deptid>
								<option value="#getusers.adminuserid#" <cfif listfind(assignedlist, getusers.adminuserid) IS NOT 0>selected</cfif> >#firstname# #lastname#</option>
								</cfif>
							</cfoutput>
						</select>
						<br>
						<span class="small" style="color:FF0000">Hold CTRL or SHIFT to select multiples</span>
					</td>
					</tr>
				<tr>
					<td><input type="submit" name="btnContinue" value="Continue"></td>
				</tr>
			</table>
			</td>
		</tr>
	</table>
	<table border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td><a href="/index.cfm">Return to Administrative Menu</a></td>
	</tr>
	</table>
	</form>
	</div>

</cfif>