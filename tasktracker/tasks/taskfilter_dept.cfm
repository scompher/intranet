<body onBlur="focus();">

<cfquery name="getdepts" datasource="#ds#">
	select * from Admin_Users_Departments
	order by department ASC
</cfquery>

<script language="JavaScript">
<!--
function applyfilter(frm, qString) {

	var qArray = qString.split("&");
	var urlstring = "";

	for (var i=0;i<frm.departmentid.length;i++) {
		if (frm.departmentid[i].checked) {
			urlstring += frm.departmentid[i].value;
			urlstring += ",";
		}
		var output = urlstring.slice(0,urlstring.lastIndexOf(","));
	}
	
	var outputstring = "";
	
	for (var i=0;i<qArray.length;i++) {
		var itemArray = qArray[i].split("=");
		if (itemArray[0] == "p") {
			var page = itemArray[1];			
		} else 	if (itemArray[0] == "d") {
			var d = "d=" + output;
			outputstring += d + "&";
		} else {
			outputstring += qArray[i] + "&";
		}
		var tmp = outputstring.slice(0,outputstring.lastIndexOf("&"));
	}

	if (output != "") {
		self.opener.location = page + "?" + tmp;
		self.close();
	}
}
//-->
</script> 

<form method="post" action="taskfilter_dept.cfm">
<table border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td><b>Show only items from the selected departments:</b></td>
	</tr>
	<tr>
	    <td>
		<table border="0" cellpadding="5" cellspacing="0">
            <tr>
				<cfloop from="1" to="#evaluate(ceiling(getdepts.recordcount / 3))#" index="x">
					<td valign="top">
					<cfoutput query="getdepts" startrow="#evaluate((x * 3) - 2)#" maxrows="3">
						<input type="checkbox" name="departmentid" value="#getdepts.departmentid#" <cfif listfind(d,departmentid) IS NOT 0>checked</cfif> >
						#getdepts.department#<br>					
					</cfoutput>
					</td>
				</cfloop>
				<!--- 
				<cfoutput query="getdepts">
                <td>
				<input type="checkbox" name="departmentid" value="#getdepts.departmentid#" <cfif listfind(d,departmentid) IS NOT 0>checked</cfif> >
				&nbsp;#getdepts.department#
				</td>
				</cfoutput>
				 --->
            </tr>
        </table>
		</td>
    </tr>
	<cfoutput>
	<tr>
		<td>
		<input onClick="applyfilter(this.form, '#cgi.QUERY_STRING#');" type="button" value="Apply Filter" class="normal">
		</td>
	</tr>
	</cfoutput>
</table>
</form>
</body>

