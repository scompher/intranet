
<cfset created = createodbcdatetime(now())>
<cfset statusupdated = createodbcdatetime(now())>

<cfparam name="form.assignedusers" default="0">
<cfparam name="form.categoryid" default="0">
<cfparam name="form.departmentid" default="0">
<cfparam name="form.statusid" default="2">
<cfparam name="form.duedate" default="">

<!--- <cfset form.title = xmlformat(form.title)> --->
<cfset form.title = replace(form.title, "#chr(19)#","-","all")>
<cfset form.title = replace(form.title, "#chr(20)#","-","all")>
<cfset form.title = replace(form.title, "#chr(39)#","","all")>
<cfset form.title = replace(form.title, "#chr(34)#","'","all")>

<!--- <cfset form.description = xmlformat(form.description)> --->
<cfset form.description = replace(form.description,"#chr(19)#","-","all")>
<cfset form.description = replace(form.description,"#chr(20)#","-","all")>
<cfset form.description = replace(form.description,"#chr(39)#","","all")>
<cfset form.description = replace(form.description, "#chr(34)#","'","all")>

<cfif trim(form.duedate) IS NOT "">
	<cfset form.duedate = createodbcdatetime(form.duedate)>
</cfif>

<cftransaction>
<!--- add task --->
<cfquery name="addTask" datasource="#ds#">
	insert into tasktracker_tasks (sponsor, originatorID, created, title, description, categoryid, departmentid<cfif trim(form.duedate) IS NOT "">, duedate</cfif>)
	values ('#form.sponsor#', #cookie.adminlogin#, #created#, '#form.title#', '#form.description#', #form.categoryid#, #form.departmentid#<cfif trim(form.duedate) IS NOT "">, #form.duedate#</cfif>)
</cfquery>

<!--- get ID --->
<cfquery name="getID" datasource="#ds#">
	select max(taskid) as ID from tasktracker_tasks
</cfquery>
<cfset taskid = #getid.id#>

<!--- insert into lookup --->
<cfloop index="adminuserid" list="#form.assignedusers#">
	<cfif adminuserid IS 0><cfset statusid = 5><cfset statusupdated = "NULL"><cfelse><cfset statusid = form.statusid></cfif>
	<cfquery name="getmaxpriority" datasource="#ds#">
		select max(priority) as p from tasktracker_task_user_lookup where adminuserid = #adminuserid# and statusid NOT IN (1,5)
	</cfquery>
	<cfif trim(getmaxpriority.p) IS ""><cfset p = 1><cfelse><cfset p = getmaxpriority.p + 1></cfif>
	<cfif statusid IS 1><cfset p = 0></cfif>
	<cfquery name="insertLooup" datasource="#ds#">
		insert into tasktracker_task_user_lookup (taskid, adminuserid, priority, statusid, statusupdated)
		values (#taskid#, #adminuserid#, #p#, #statusid#, #statusupdated#)
	</cfquery>
</cfloop>
</cftransaction>

<cfif isDefined("quickentry")>
	<cflocation url="add2.cfm?taskid=#taskid#&quickentry=1">
</cfif>

<cfif getsec.seclevelid LTE 2>
	<cflocation url="add2.cfm?taskid=#taskid#">
<cfelse>
	<cfif (cookie.adminlogin IS NOT form.assignedusers) AND (form.assignedusers IS NOT 0)>
		<cflocation url="setstatus.cfm?taskid=#taskid#">
	<cfelse>
		<cflocation url="add2.cfm?taskid=#taskid#">
	</cfif>
</cfif>

