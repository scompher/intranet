
<link rel="stylesheet" type="text/css" href="../../styles.css">

<cfparam name="form.HTTP_REFERER" default="#cgi.HTTP_REFERER#">

<!--- <cfparam name="sort" default="date_asc"> --->
<cfparam name="f" default="2,4,5">
<cfparam name="form.updatepriorities" default="0">

<cfset querystring = "f=#f#&id=#id#">

<cfif isdefined("form.btnStatusUpdate")>
	<cfloop index="status" list="#form.statusid#">
		<cfset statusid = listgetat(status,2,";")>
		<cfset tid = listgetat(status,1,";")>
		<cfset id = form.id>

		<!--- get details --->
		<cfquery name="checkstatus" datasource="#ds#">
			select tasktracker_tasks.title, tasktracker_tasks.description, tasktracker_task_user_lookup .statusid 
			from tasktracker_tasks
			inner join tasktracker_task_user_lookup on tasktracker_tasks.taskid = tasktracker_task_user_lookup.taskid
			where tasktracker_task_user_lookup.taskid = #variables.tid# AND tasktracker_task_user_lookup.adminuserid = #id#
		</cfquery>

		<cfif checkstatus.statusid IS NOT variables.statusid>
		<cfset statusupdated = createodbcdatetime(now())>
			<!--- update status --->
			<cfquery name="updateStatus" datasource="#ds#">
				update tasktracker_task_user_lookup
				set statusupdated = #statusupdated#, statusid = #variables.statusid#<cfif (variables.statusid IS 1) OR (variables.statusid IS 5)>, priority = 0</cfif>
				where taskid = #tid# and adminuserid = #id#
			</cfquery>
			
			<!--- if statusid = 5 (unassigned) then un-link user --->
			<cfif variables.statusid IS 5>
				<cfquery name="unlinkuser" datasource="#ds#">
					update tasktracker_task_user_lookup
					set adminuserid = 0
					where taskid = #tid# and adminuserid=#id#
				</cfquery>
			</cfif>
			
			<!--- renumber priorities --->
			<cfquery name="getpriorities" datasource="#ds#">
				select taskid, priority, adminuserid 
				from tasktracker_task_user_lookup 
				where adminuserid = #id# and statusid NOT IN (1,5)
				order by adminuserid, priority asc
			</cfquery>
			
			<cfset p = 1>
			<cfset curruser = 0>
			<cfloop query="getpriorities">
				<cfif curruser IS NOT adminuserid><cfset p = 1><cfset curruser = getpriorities.adminuserid></cfif>
				<cfquery name="updatepriority" datasource="#ds#">
					update tasktracker_task_user_lookup
					set	priority = #p#
					where taskid = #getpriorities.taskid# and adminuserid = #getpriorities.adminuserid#
				</cfquery>
				<cfset p = p + 1>
			</cfloop>

			<!--- send status change notification --->
			<cfquery name="getoriginator" datasource="#ds#">
				select admin_users.email, admin_users.firstname, admin_users.adminuserid
				from admin_users
				inner join tasktracker_tasks on tasktracker_tasks.originatorid = admin_users.adminuserid
				where tasktracker_tasks.taskid = #tid#
			</cfquery>
			<cfquery name="getusers" datasource="#ds#">
				select admin_users.email, admin_users.firstname
				from admin_users
				inner join tasktracker_task_user_lookup on tasktracker_task_user_lookup.adminuserid = admin_users.adminuserid
				where tasktracker_task_user_lookup.taskid = #tid# and admin_users.adminuserid <> #cookie.adminlogin#
			</cfquery>
			<cfset emaillist = valuelist(getusers.email)>
			<cfif cookie.adminlogin IS NOT getoriginator.adminuserid>
				<cfset emaillist = listappend(emaillist, getoriginator.email)>
			</cfif>

<!--- send emails --->
<cfloop index="email" list="#emaillist#">
<cfmail from="pgregory@copsmonitoring.com" to="#email#" subject="Task Manager Notification : Task Status Change" username="copalink@copsmonitoring.com" password="copsmoncal">

The status for the following task has been changed:

#checkstatus.title# 
#checkstatus.description# 

You may view the details of this task by clicking on the following link:

#request.appurl#tasktracker/tasks/view.cfm?id=#variables.tid#
</cfmail>
</cfloop> 

		</cfif>

	</cfloop>
</cfif>

<cfif form.updatepriorities IS 1>
	<cfset updateflag = false>
	<cfset pri = "priority#id#">
	<cfloop index="item" list="#evaluate(pri)#">
		<cfset adminuserid = id>
		<cfset tid = listgetat(item,1,";")>
		<cfset p = listgetat(item,2,";")>
		<cfquery name="updatepriorities" datasource="#ds#">
			if exists (select * from tasktracker_task_user_lookup where taskid = #tid# and adminuserid = #adminuserid#)
				begin
					update tasktracker_task_user_lookup
					set priority = #p#
					where taskid = #tid# and adminuserid = #adminuserid#
				end
			else
				begin
					insert into tasktracker_task_user_lookup (taskid, adminuserid, priority)
					values (#tid#, #adminuserid#, #p#)
				end		
		</cfquery>
	</cfloop>
	<!--- send email notification that priorities changed --->
	<cfquery name="s2" datasource="#ds#">
		select tasktracker_tasks.title, tasktracker_tasks.taskid, tasktracker_task_user_lookup.priority, tasktracker_task_user_lookup.adminuserid
		from tasktracker_tasks, tasktracker_task_user_lookup
		where tasktracker_tasks.taskid = tasktracker_task_user_lookup.taskid and tasktracker_task_user_lookup.adminuserid IN (#adminuserid#) and tasktracker_task_user_lookup.priority <> 0
		order by adminuserid, priority ASC
	</cfquery>
	<cfquery name="getemail" datasource="#ds#">
		select * from admin_users where adminuserid = #adminuserid#
	</cfquery>
	<cfmail from="pgregory@copsmonitoring.com" to="#getemail.email#" subject="Task Manager Notification : Task priorities have been changed" username="copalink@copsmonitoring.com" password="copsmoncal">
	Your task priorities have changed.  Here are your current priorities:
	
	<cfloop query="s2">
	#priority#. #title#
	
	</cfloop>
	</cfmail>
</cfif>

<script language="JavaScript">
<!--
function openfilter(q) {
	filterwin = window.open("taskfilter.cfm?" + q,"filterwin","width=450,height=125");
	filterwin.focus();
}
function reorder(frm, priorityArray, theItem, oldIndex) {
	var tmp = 0;
	if (oldIndex < theItem.selectedIndex) {
		if (oldIndex != (theItem.selectedIndex - 1)) {
			for (var i=0;i<priorityArray.length;i++) {
				if (priorityArray[i].selectedIndex <= theItem.selectedIndex && theItem != priorityArray[i]) {
					priorityArray[i].selectedIndex -= 1;
				}
			}
		}
		else {
			priorityArray[oldIndex].selectedIndex = theItem.selectedIndex;
			priorityArray[oldIndex + 1].selectedIndex = oldIndex;
		}
	}
	else  {
		if (oldIndex != (theItem.selectedIndex + 1)) {
			for (var i=0;i<priorityArray.length;i++) {
				if (priorityArray[i].selectedIndex >= theItem.selectedIndex && theItem != priorityArray[i]) {
					priorityArray[i].selectedIndex += 1;
				}
			}
		}
		else {
			priorityArray[oldIndex].selectedIndex = theItem.selectedIndex;
			priorityArray[oldIndex - 1].selectedIndex = oldIndex;
		}
	}
	frm.updatepriorities.value = 1;
	frm.submit();
}
function gettimespent(frm, uid, tid) {
	if (frm.statusid[0].length) {
		for (var i=0;i<frm.statusid.length;i++) {
			var v = frm.statusid[i].options[frm.statusid[i].selectedIndex].value;
			var txt = frm.statusid[i].options[frm.statusid[i].selectedIndex].text;
			var t = v.split(";");
			if (t[0] == tid) {
				if (txt == "Completed") {
					timespentwin = window.open("gettimespent.cfm?id=" + tid + "&u=" + uid,"timespentwin","width=550,height=220");
					timespentwin.focus();
				}
				else {
					frm.submit();
				}
			}
		}
	} else { 
		var txt = frm.statusid.options[frm.statusid.selectedIndex].text;
		if (txt == "Completed") {
			timespentwin = window.open("gettimespent.cfm?id=" + tid + "&u=" + uid,"timespentwin","width=550,height=220");
			timespentwin.focus();
		}
		else {
			frm.submit();
		}

	}
}
//-->
</script> 

<cfquery name="getuserinfo" datasource="#ds#">
	select * from admin_users where adminuserid = #id#
</cfquery>
<cfquery name="getstatus" datasource="#ds#">
	select * from tasktracker_status
	order by description asc
</cfquery>
<cfquery name="gettaskinfo" datasource="#ds#">
	select tasktracker_task_user_lookup.priority, tasktracker_tasks.title, tasktracker_tasks.created, tasktracker_tasks.description, tasktracker_tasks.taskid, tasktracker_task_user_lookup.statusid, tasktracker_status.description as status, tasktracker_task_user_lookup.adminuserid, tasktracker_tasks.duedate, tasktracker_task_user_lookup.timespent, tasktracker_task_user_lookup.statusupdated
	from tasktracker_task_user_lookup, tasktracker_tasks, tasktracker_status
	where tasktracker_tasks.active <> 0 and tasktracker_task_user_lookup.taskid = tasktracker_tasks.taskid and tasktracker_task_user_lookup.statusid = tasktracker_status.statusid and tasktracker_task_user_lookup.adminuserid = #id# AND tasktracker_task_user_lookup.statusid IN (#f#)
	order by tasktracker_task_user_lookup.priority asc, tasktracker_task_user_lookup.taskid desc
</cfquery>

<div align="center">
	<cfinclude template="footer.cfm">
	<p>
	
	<table width="955" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td class="highlightbar"><b>View Tasks</b></td>
		</tr>
		<tr>
			<td class="greyrowbottomnopadding">
				<table width="100%" border="0" cellpadding="5" cellspacing="0">
					<cfoutput>
					<tr>
						<td><a href="javascript:openfilter('#querystring#&p=viewbyuser.cfm');"><b>Click to Filter By Status</b></a></td>
					</tr>
					<tr>
						<td align="center"><b>#getuserinfo.firstname# #getuserinfo.lastname#</b></td>
					</tr>
					</cfoutput>
					<tr>
						<td align="center">
						<table border="0" style="border:1px solid #000000;" cellpadding="5" cellspacing="0">
							<tr align="center" class="highlightbar">
								<td class="highlightbar" style="border-right:0px; border-left:0px; border-top:0px;"><b>Priority</b></td>
								<td class="highlightbar" style="border-right:0px; border-left:0px; border-top:0px;"><b>Task</b></td>
								<td class="highlightbar" style="border-right:0px; border-left:0px; border-top:0px;"><b>Status</b></td>
							</tr>
							<form method="post" name="statusform" action="viewbyuser.cfm">
							<input type="hidden" name="updatepriorities" value="0">
							<cfoutput>
							<!--- <input type="hidden" name="HTTP_REFERER" value="#form.HTTP_REFERER#"> --->
							<input type="hidden" name="f" value="#f#">
							<input type="hidden" name="id" value="#id#">
							</cfoutput>
							<cfoutput query="gettaskinfo">
							<cfset sid = gettaskinfo.statusid>
							<cfset tid = gettaskinfo.taskid>
							<tr>
								<td align="center" class="linedrowrightcolumn">
								<cfif (getsec.seclevelid LTE 3)>
									<cfif gettaskinfo.statusid IS NOT 1 AND gettaskinfo.statusid IS NOT 5>
										<select name="priority#id#" class="normal" onChange="reorder(this.form,priority#id#, this, #evaluate(gettaskinfo.priority - 1)#)">
										<cfloop from="1" to="#gettaskinfo.recordcount#" index="x">
											<option value="#variables.tid#;#x#" <cfif gettaskinfo.priority IS x>selected</cfif> >#x#</option>
										</cfloop>
										</select>
									<cfelse>
									&nbsp;
									</cfif>
								<cfelse>
									<cfif gettaskinfo.priority GT 0>#gettaskinfo.priority#<cfelse>&nbsp;</cfif>
								</cfif>
								</td>
								<td class="linedrowrightcolumn">
								<a href="view.cfm?id=#taskid#">#title#</a> 
								<br>Created: #dateformat(gettaskinfo.created, 'mm/dd/yyyy')#
								<cfif sid IS 1>
									<cfif trim(gettaskinfo.timespent) IS NOT ""><br>(Time Spent: #gettaskinfo.timespent#)</cfif>
								</cfif>
								<cfif trim(gettaskinfo.duedate) IS NOT ""><br><cfif datediff("d",gettaskinfo.duedate,now()) GT 0><span style="color:ff0000;"><cfelse><span></cfif>(Due Date: #dateformat(gettaskinfo.duedate,'mm/dd/yyyy')#)</span></cfif>
								</td>
								<td valign="top" nowrap class="linedrow">
								<cfif (cookie.adminlogin IS gettaskinfo.adminuserid) OR (getsec.seclevelid LTE 2)>
									<select name="statusid" class="normal">
									<cfloop query="getstatus">
										<option value="#variables.tid#;#getstatus.statusid#" <cfif sid IS getstatus.statusid>selected</cfif> >#getstatus.description#</option>
									</cfloop>
									</select>
									<input type="hidden" name="btnStatusUpdate" value="Update">
									<!--- <input onClick="gettimespent(this.form,'#id#','#variables.tid#');" type="button" value="Update" class="normal"> --->
									<input type="submit" value="Update" class="normal">
								<cfelse>
									#gettaskinfo.status#
								</cfif>
								<br>Last Updated:  #dateformat(gettaskinfo.statusupdated,'mm/dd/yyyy')#
								</td>
							</tr>
							</cfoutput>
							</form>
							<tr>
								<td colspan="3" align="center"><a href="add.cfm">Add New Task</a></td>
							</tr>
						</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<p>
	<cfinclude template="footer.cfm">
</div>

