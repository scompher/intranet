
<cfquery name="getemails" datasource="#ds#">
	select tasktracker_tasks.originatorID, tasktracker_tasks.title, tasktracker_tasks.description, tasktracker_task_user_lookup.adminuserid, admin_users.firstname, admin_users.lastname, admin_users.email
	from tasktracker_tasks
	inner join tasktracker_task_user_lookup on tasktracker_tasks.taskid = tasktracker_task_user_lookup.taskid
	inner join admin_users on tasktracker_task_user_lookup.adminuserid = admin_users.adminuserid
	where tasktracker_tasks.taskid = #taskid#
</cfquery>

<cfloop query="getemails">
<cfif getemails.originatorID IS NOT getemails.adminuserid>
<cfmail from="pgregory@copsmonitoring.com" to="#getemails.email#" subject="Task Manager Notification : New Task Assigned To You" username="copalink@copsmonitoring.com" password="copsmoncal">
#getemails.firstname#,

You have been assigned a new task: 

#getemails.title# 
#getemails.description# 

You may view the details of this task by clicking on the following link:

#request.appurl#tasktracker/tasks/view.cfm?id=#taskid#
 
</cfmail>
</cfif>
</cfloop>

<cfif isdefined("url.quickentry")>
<div align="center">
<table border="0" cellpadding="0" cellspacing="5">
<tr>
	<td>
	<b>Quick Task Successfully Added</b>
	</td>
</tr>
</table>
</div>
<meta http-equiv="refresh" content="1;url=add.cfm?quickentry=1">
<cfelse>
	<cflocation url="index.cfm">
</cfif>



