
<cfif isDefined("form.timespent")>

<cfquery name="updateTimeSpent" datasource="#ds#">
	update tasktracker_task_user_lookup
	set timespent = '#form.timespent#'
	where taskid = #id# and adminuserid = #u#
</cfquery>

	<script type="text/javascript">
		self.opener.statusform.submit();
		self.close();
	</script>

<cfelse>

<body>
<div align="center">
	<form method="post" action="gettimespent.cfm">
	<cfoutput>
	<input type="hidden" name="u" value="#u#">
	<input type="hidden" name="id" value="#id#">
	</cfoutput>
	<table border="0" cellpadding="3">
		<tr>
			<td><b>Please enter the amount of time spent on this task:</b></td>
		</tr>
		<tr>
			<td><input type="text" name="timespent" maxlength="75" size="20"></td>
		</tr>
		<tr>
			<td>
			<input name="btnSaveTime" type="submit" value="Save">
			</td>
		</tr>
	</table>
	</form>
</div>
</body>

</cfif>
