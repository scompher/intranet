
<cfparam name="form.assignedusers" default="0">
<cfparam name="form.categoryid" default="0">
<cfparam name="form.departmentid" default="0">
<cfparam name="form.duedate" default="">

<!--- <cfset form.title = xmlformat(form.title)> --->
<cfset form.title = replace(form.title, "#chr(19)#","-","all")>
<cfset form.title = replace(form.title, "#chr(20)#","-","all")>
<cfset form.title = replace(form.title, "#chr(39)#","","all")>
<cfset form.title = replace(form.title, "#chr(34)#","'","all")>
<cfset form.title = replace(form.title, "&","and","all")>

<!--- <cfset form.description = xmlformat(form.description)> --->
<cfset form.description = replace(form.description,"#chr(19)#","-","all")>
<cfset form.description = replace(form.description,"#chr(20)#","-","all")>
<cfset form.description = replace(form.description,"#chr(39)#","","all")>
<cfset form.description = replace(form.description, "#chr(34)#","'","all")>

<cfif trim(form.duedate) IS NOT "">
	<cfset form.duedate = createodbcdatetime(form.duedate)>
</cfif>

<cftransaction>
<!--- update task --->
<cfquery name="updateTask" datasource="#ds#">
	update tasktracker_tasks 
	set sponsor = '#form.sponsor#', title = '#form.title#', description = '#form.description#', categoryid = #form.categoryid#, departmentid = #form.departmentid#<cfif trim(form.duedate) IS NOT "">, duedate = #form.duedate#</cfif>
	where taskid = #form.taskid#
</cfquery>

<!--- update assigned users --->
<cfloop index="adminuserid" list="#form.assignedusers#">
<cfif listfind(assignedlist,adminuserid) IS 0><cfset statusid = 2></cfif>
<cfif adminuserid IS 0><cfset statusid = 5></cfif>
<cfquery name="getmaxpriority" datasource="#ds#">
	select max(priority) as p from tasktracker_task_user_lookup where adminuserid = #adminuserid#
</cfquery>
<cfif trim(getmaxpriority.p) IS ""><cfset p = 1><cfelse><cfset p = getmaxpriority.p + 1></cfif>
<cfquery name="insertLooup" datasource="#ds#">
	if NOT exists (select * from tasktracker_task_user_lookup where taskid = #form.taskid# and adminuserid = #adminuserid#)
	begin
		insert into tasktracker_task_user_lookup (taskid, adminuserid, priority<cfif listfind(assignedlist,adminuserid) IS 0>, statusid</cfif>)
		values (#taskid#, #adminuserid#, #p#<cfif listfind(assignedlist,adminuserid) IS 0>, #statusid#</cfif>)
	end
</cfquery>
</cfloop>

<!--- delete unassigned users --->
<cfquery name="delunassigned" datasource="#ds#">
	delete from tasktracker_task_user_lookup
	where taskid = #taskid# and adminuserid NOT IN (#form.assignedusers#)
</cfquery>
</cftransaction>

<cflocation url="edit2.cfm?taskid=#taskid#">
