
<link rel="stylesheet" type="text/css" href="../../styles.css">

<script language="javascript" src="cal2.js">
/*
Xin's Popup calendar script-  Xin Yang (http://www.yxscripts.com/)
Script featured on/available at http://www.dynamicdrive.com/
This notice must stay intact for use
*/
</script>
<script language="javascript" src="cal_conf2.js"></script>
<SCRIPT LANGUAGE="JavaScript">
<!-- Original:  Sandeep Tamhankar (stamhankar@hotmail.com) -->
<!-- Web Site:  http://207.20.242.93 -->

<!-- This script and many more are available free online at -->
<!-- The JavaScript Source!! http://javascript.internet.com -->

<!-- Begin
function IsValidTime(timeStr) {

	// Checks if time is in HH:MM:SS AM/PM format.
	// The seconds and AM/PM are optional.
	
	var timePat = /^(\d{1,2}):(\d{2})(:(\d{2}))?(\s?(AM|am|PM|pm))?$/;
	
	var matchArray = timeStr.match(timePat);
	
	if (matchArray == null) {
		alert("Time is not in a valid format.");
		return false;
	}
	
	hour = matchArray[1];
	minute = matchArray[2];
	second = matchArray[4];
	ampm = matchArray[6];
	
	if (second=="") { second = null; }
	if (ampm=="") { ampm = null }
	
	if (hour < 0  || hour > 23) {
		alert("Hour must be between 1 and 12. (or 0 and 23 for military time)");
		return false;
	}
	if (hour <= 12 && ampm == null) {
		if (confirm("Please indicate which time format you are using.  OK = Standard Time, CANCEL = Military Time")) {
			alert("You must specify AM or PM.");
			return false;
		}
	}
	if  (hour > 12 && ampm != null) {
		alert("You can't specify AM or PM for military time.");
		return false;
	}
	if (minute<0 || minute > 59) {
		alert ("Minute must be between 0 and 59.");
		return false;
	}
	if (second != null && (second < 0 || second > 59)) {
		alert ("Second must be between 0 and 59.");
		return false;
	}

return true;
}
//  End -->
</script>
<script language = "Javascript">
/**
 * DHTML date validation script. Courtesy of SmartWebby.com (http://www.smartwebby.com/dhtml/)
 */
// Declaring valid date character, minimum year and maximum year
var dtCh= "/";
var minYear=1900;
var maxYear=2100;

function isInteger(s){
	var i;
    for (i = 0; i < s.length; i++){   
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}

function stripCharsInBag(s, bag){
	var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.
    for (i = 0; i < s.length; i++){   
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}

function daysInFebruary (year){
	// February has 29 days in any year evenly divisible by four,
    // EXCEPT for centurial years which are not also divisible by 400.
    return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 );
}
function DaysArray(n) {
	for (var i = 1; i <= n; i++) {
		this[i] = 31
		if (i==4 || i==6 || i==9 || i==11) {this[i] = 30}
		if (i==2) {this[i] = 29}
   } 
   return this
}

function isDate(dtStr){
	var daysInMonth = DaysArray(12)
	var pos1=dtStr.indexOf(dtCh)
	var pos2=dtStr.indexOf(dtCh,pos1+1)
	var strMonth=dtStr.substring(0,pos1)
	var strDay=dtStr.substring(pos1+1,pos2)
	var strYear=dtStr.substring(pos2+1)
	strYr=strYear
	if (strDay.charAt(0)=="0" && strDay.length>1) strDay=strDay.substring(1)
	if (strMonth.charAt(0)=="0" && strMonth.length>1) strMonth=strMonth.substring(1)
	for (var i = 1; i <= 3; i++) {
		if (strYr.charAt(0)=="0" && strYr.length>1) strYr=strYr.substring(1)
	}
	month=parseInt(strMonth)
	day=parseInt(strDay)
	year=parseInt(strYr)
	if (pos1==-1 || pos2==-1){
		alert("The date format should be : mm/dd/yyyy")
		return false
	}
	if (strMonth.length<1 || month<1 || month>12){
		alert("Please enter a valid month")
		return false
	}
	if (strDay.length<1 || day<1 || day>31 || (month==2 && day>daysInFebruary(year)) || day > daysInMonth[month]){
		alert("Please enter a valid day")
		return false
	}
	if (strYear.length != 4 || year==0 || year<minYear || year>maxYear){
		alert("Please enter a valid 4 digit year between "+minYear+" and "+maxYear)
		return false
	}
	if (dtStr.indexOf(dtCh,pos2+1)!=-1 || isInteger(stripCharsInBag(dtStr, dtCh))==false){
		alert("Please enter a valid date")
		return false
	}
return true
}
</script>
<script language="JavaScript">
<!--
function checkform(frm) {
	if (!isDate(frm.startdate.value)) {frm.startdate.focus(); return false;}
	if (!IsValidTime(frm.starttime.value + ' ' + frm.starttt.options[frm.starttt.selectedIndex].value)) {frm.starttime.focus(); return false;}
	if (!isDate(frm.enddate.value)) {frm.enddate.focus(); return false;}
	if (!IsValidTime(frm.endtime.value + ' ' + frm.endtt.options[frm.endtt.selectedIndex].value)) {frm.endtime.focus(); return false;}
	frm.submit();
}
function notehist(notesid) {
	notewin = window.open("notehist.cfm?id=" + notesid,"notewin","width=600,height=400,resizable=yes,scrollbars=yes");
	notewin.focus();
}
//-->
</script> 

<cfparam name="orderby" default="tasktracker_notes.created desc">
<cfparam name="sort" default="datetime_desc">
<cfparam name="start" default="00:00AM">
<cfparam name="end" default="11:59PM">
<cfparam name="f" default="2,4,5">
<cfparam name="d" default="#variables.departmentlist#">

<cfif trim(cgi.QUERY_STRING) IS ""><cfset querystring = "f=#f#&sort=#sort#&d=#d#"><cfelse><cfset querystring = cgi.QUERY_STRING></cfif>

<cfoutput>
<script language="JavaScript">
<!--
function openfilter(q) {
	filterwin = window.open("taskfilter.cfm?" + q,"filterwin","width=450,height=125");
	filterwin.focus();
}
function opendeptfilter(q) {
	filterwin = window.open("taskfilter_dept.cfm?" + q,"filterwin","width=450,height=175");
	filterwin.focus();
}
//-->
</script> 
</cfoutput>

<cfif isDefined("form.btnFilter")>
	<cfset s = "#form.startdate# #form.starttime##form.starttt#">
	<cfset e = "#form.enddate# #form.endtime##form.endtt#">
	<cfset start = createodbcdatetime(s)>
	<cfset end = createodbcdatetime(e)>
</cfif>

<cfswitch expression="#sort#">
	<cfcase value="user_asc"><cfset orderby = "admin_users.lastname asc, admin_users.firstname asc"></cfcase>
	<cfcase value="user_desc"><cfset orderby = "admin_users.lastname desc, admin_users.firstname desc"></cfcase>
	<cfcase value="task_asc"><cfset orderby = "tasktracker_tasks.title asc"></cfcase>
	<cfcase value="task_desc"><cfset orderby = "tasktracker_tasks.title desc"></cfcase>
	<cfcase value="datetime_asc"><cfset orderby = "tasktracker_notes.created asc"></cfcase>
	<cfcase value="datetime_desc"><cfset orderby = "tasktracker_notes.created desc"></cfcase>
	<cfcase value="cat_asc"><cfset orderby = "tasktracker_categories.category asc"></cfcase>
	<cfcase value="cat_desc"><cfset orderby = "tasktracker_categories.category desc"></cfcase>
	<cfcase value="dept_asc"><cfset orderby = "Admin_Users_Departments.department asc"></cfcase>
	<cfcase value="dept_desc"><cfset orderby = "Admin_Users_Departments.department desc"></cfcase>
</cfswitch>

<cfquery name="getnotes" datasource="#ds#">
	select tasktracker_notes.*, admin_users.firstname, admin_users.lastname, admin_users.adminuserid, tasktracker_tasks.taskid, tasktracker_tasks.title, tasktracker_categories.category, tasktracker_task_user_lookup.statusid
	from tasktracker_notes
	inner join admin_users on tasktracker_notes.creatorID = admin_users.adminuserid
	inner join tasktracker_tasks on tasktracker_notes.taskid = tasktracker_tasks.taskid
	inner join tasktracker_task_user_lookup on tasktracker_tasks.taskid = tasktracker_task_user_lookup.taskid 
	left join tasktracker_categories on tasktracker_tasks.categoryid = tasktracker_categories.categoryid
	inner join Admin_Users_Departments on tasktracker_tasks.departmentid = Admin_Users_Departments.departmentid
	where tasktracker_tasks.active <> 0 AND tasktracker_task_user_lookup.statusid IN (#f#) AND tasktracker_tasks.departmentid IN (#d#) <cfif trim(start) IS NOT "00:00AM" AND trim(end) IS NOT "11:59PM">and tasktracker_notes.created >= #start# and tasktracker_notes.created <= #end#</cfif>
	order by #orderby#
</cfquery>
<!--- and tasktracker_notes.creatorid = tasktracker_task_user_lookup.adminuserid --->
<div align="center">
<cfinclude template="footer.cfm">
<p>
    <table width="900" border="1" cellpadding="5" cellspacing="0">
		<cfoutput>
		<tr class="highlightbar">
			<td colspan="2">
			<a href="javascript:openfilter('#querystring#&p=noteview.cfm');"><b>Click to Filter By Status</b></a> | 
			<a href="javascript:opendeptfilter('#querystring#&p=noteview.cfm');"><b>Click to Filter By Department</b></a>
			</td>
		</tr>
		</cfoutput>
		<tr>
			<td class="greyrowbottomnopadding">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td colspan="2"><b>View All Task Notes </b></td>
					</tr>
					<cfset urlappend = "&f=#f#&d=#d#">
					<cfif trim(start) IS NOT "" AND trim(end) IS NOT ""><cfset urlappend = "&start=#start#&end=#end#&f=#f#&d=#d#"></cfif>
					<cfoutput>
					<tr align="center">
						<td colspan="2"><b>Sort By :</b> 
						<cfif sort IS "user_asc"><cfset sortlink = "user_desc"><cfelse><cfset sortlink = "user_asc"></cfif>
						<a href="noteview.cfm?sort=#sortlink##urlappend#">User</a> | 
						<cfif sort IS "task_asc"><cfset sortlink = "task_desc"><cfelse><cfset sortlink = "task_asc"></cfif>
						<a href="noteview.cfm?sort=#sortlink##urlappend#">Task</a> | 
						<cfif sort IS "datetime_asc"><cfset sortlink = "datetime_desc"><cfelse><cfset sortlink = "datetime_asc"></cfif>
						<a href="noteview.cfm?sort=#sortlink##urlappend#">Date/Time </a> | 
						<cfif sort IS "cat_asc"><cfset sortlink = "cat_desc"><cfelse><cfset sortlink = "cat_asc"></cfif>
						<a href="noteview.cfm?sort=#sortlink##urlappend#">Category </a> | 
						<cfif sort IS "dept_asc"><cfset sortlink = "dept_desc"><cfelse><cfset sortlink = "dept_asc"></cfif>
						<a href="noteview.cfm?sort=#sortlink##urlappend#">Department </a>
						</td>
					</tr>
					</cfoutput>
					<form method="post" action="noteview.cfm">
					<tr align="center">
						<td colspan="2" valign="middle">
						<table width="100%" border="0" cellpadding="5" cellspacing="0">
						<tr>
							<td>
							<cfoutput>
							Start Date: <input onFocus="if (this.value.length > 0) this.select();" type="text" name="startdate" size="12" maxlength="12" class="small" <cfif trim(start) IS NOT "00:00AM">value="#dateformat(start,'mm/dd/yyyy')#"<cfelse>value="mm/dd/yyyy"</cfif> > <a style="text-decoration:none;" href="javascript:showCal('TaskCalendarStart');"><img align="absmiddle" border="0" src="/images/calicon.gif"></a>&nbsp;&nbsp;&nbsp;&nbsp;
							Time (hh:mm) : <input onFocus="if (this.value.length > 0) this.select();" type="text" name="starttime" size="5" maxlength="5" <cfif trim(start) IS NOT "">value="#timeformat(start,'hh:mm')#"<cfelse>value="00:00"</cfif> class="small">
							<select name="starttt" class="small">
								<option value="AM" <cfif trim(start) IS NOT ""><cfif ucase(timeformat(start,'tt')) IS "AM">selected</cfif></cfif> >AM</option>
								<option value="PM" <cfif trim(start) IS NOT ""><cfif ucase(timeformat(start,'tt')) IS "PM">selected</cfif></cfif> >PM</option>
							</select>
							</td>
							<td>
							End Date: <input onFocus="if (this.value.length > 0) this.select();" type="text" name="enddate" size="12" maxlength="12" class="small" <cfif trim(end) IS NOT "11:59PM">value="#dateformat(end,'mm/dd/yyyy')#"<cfelse>value="mm/dd/yyyy"</cfif> > <a style="text-decoration:none;" href="javascript:showCal('TaskCalendarEnd');"><img align="absmiddle" border="0" src="/images/calicon.gif"></a>&nbsp;&nbsp;&nbsp;&nbsp;
							Time (hh:mm) : <input onFocus="if (this.value.length > 0) this.select();" type="text" name="endtime" size="5" maxlength="5" <cfif trim(end) IS NOT "">value="#timeformat(end,'hh:mm')#"<cfelse>value="00:00"</cfif> class="small">
							<select name="endtt" class="small">
								<option value="AM" <cfif trim(start) IS NOT ""><cfif ucase(timeformat(end,'tt')) IS "AM">selected</cfif></cfif> >AM</option>
								<option value="PM" <cfif trim(start) IS NOT ""><cfif ucase(timeformat(end,'tt')) IS "PM">selected</cfif></cfif> >PM</option>
							</select>
							</td></cfoutput>
							
						</tr>
						</table>
						</td>
					</tr>
					<tr><td colspan="2" align="center">
					<cfoutput>
					<input onClick="return checkform(this.form);" type="button" value="Filter by Date and Time" class="normal">
					<input type="hidden" name="sort" value="#sort#">
					<input type="hidden" name="f" value="#f#">
					</cfoutput>
					</td>
					</tr>
					<tr><td colspan="2" class="linedrow"><br /></td></tr>
					<input type="hidden" name="btnFilter" value="1">
					</form>
					<cfoutput query="getnotes" group="notesid">
					<tr>
						<td valign="top"><b><a href="viewbyuser.cfm?id=#getnotes.adminuserid#">#getnotes.firstname# #getnotes.lastname#</a> : #dateformat(getnotes.created,'mm/dd/yyyy')# #timeformat(getnotes.created,'hh:mm tt')#<cfif trim(edited) IS NOT ""><br />Last Edited #dateformat(getnotes.edited,'mm/dd/yyyy')# #timeformat(getnotes.edited,'hh:mm tt')#</b>&nbsp;&nbsp;<a href="javascript:notehist(#getnotes.notesid#);">View Edit History</a></cfif></td>
						<td valign="top"><b>Task : </b><a href="view.cfm?id=#getnotes.taskid#">#getnotes.title#</a></td>
					</tr>
					<cfif trim(getnotes.category) IS NOT "">
					<tr>
						<td colspan="2"><b>Category:</b> #getnotes.category# </td>
					</tr>
					</cfif>
					<tr>
						<td class="linedrow" colspan="2">#replace(getnotes.content,"#chr(13)#","<br>","all")#</td>
					</tr>
					</cfoutput>
					<tr>
						<td colspan="2">&nbsp;</td>
					</tr>
				</table>
			</td>
		</tr>
    </table>
	<p>
	<cfinclude template="footer.cfm">
</div>
