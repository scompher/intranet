
<cfif isDefined("btnUpdate")>

<cfloop index="user" list="#userid#">
	<cfset pri = "priority#user#">
	<cfloop index="item" list="#evaluate(pri)#">
		<cfset adminuserid = listgetat(item,1,";")>
		<cfset taskid = listgetat(item,2,";")>
		<cfset p = listgetat(item,3,";")>
		<cfquery name="updatepriorities" datasource="#ds#">
			if exists (select * from tasktracker_task_user_lookup where taskid = #taskid# and adminuserid = #adminuserid#)
				begin
					update tasktracker_task_user_lookup
					set priority = #p#
					where taskid = #taskid# and adminuserid = #adminuserid#
				end
			else
				begin
					insert into tasktracker_task_user_lookup (taskid, adminuserid, priority)
					values (#taskid#, #adminuserid#, #p#)
				end		
		</cfquery>
	</cfloop>
</cfloop>
</cfif>

<!--- get users --->
<cfquery name="getusers" datasource="#ds#">
	select * from admin_users
	where adminuserid = #userid#
</cfquery>

<cfif NOT isDefined("qSnapshot")>
	<!--- get snapshot --->
	<cfquery name="getsnapshot" datasource="#ds#">
		select tasktracker_tasks.title, tasktracker_tasks.taskid, tasktracker_task_user_lookup.priority, tasktracker_task_user_lookup.adminuserid
		from tasktracker_tasks, tasktracker_task_user_lookup
		where tasktracker_tasks.taskid = tasktracker_task_user_lookup.taskid and tasktracker_task_user_lookup.adminuserid = #userid#
		order by adminuserid, priority ASC
	</cfquery>
	<cfwddx action="cfml2wddx" input="#getsnapshot#" output="qSnapshot">
</cfif>

<script language="JavaScript">
<!--
function reorder(frm, priorityArray, theItem, oldIndex) {
	var tmp = 0;
	if (oldIndex < theItem.selectedIndex) {
		if (oldIndex != (theItem.selectedIndex - 1)) {
			for (var i=0;i<priorityArray.length;i++) {
				if (priorityArray[i].selectedIndex <= theItem.selectedIndex && theItem != priorityArray[i]) {
					priorityArray[i].selectedIndex -= 1;
				}
			}
		}
		else {
			priorityArray[oldIndex].selectedIndex = theItem.selectedIndex;
			priorityArray[oldIndex + 1].selectedIndex = oldIndex;
		}
	}
	else  {
		if (oldIndex != (theItem.selectedIndex + 1)) {
			for (var i=0;i<priorityArray.length;i++) {
				if (priorityArray[i].selectedIndex >= theItem.selectedIndex && theItem != priorityArray[i]) {
					priorityArray[i].selectedIndex += 1;
				}
			}
		}
		else {
			priorityArray[oldIndex].selectedIndex = theItem.selectedIndex;
			priorityArray[oldIndex - 1].selectedIndex = oldIndex;
		}
	}
	frm.submit();
}
//-->
</script> 

<div align="center">
<cfoutput>
<form method="post" action="#listgetat(path_translated,listlen(path_translated,"\"),"\")#">
<input type="hidden" name="btnUpdate" value="1">
<input type="hidden" name="taskid" value="#taskid#">
<input type="hidden" name="userid" value="#userid#">
<input type="hidden" name="qSnapshot" value="#qSnapshot#">
</cfoutput>
<table border="0" cellpadding="5" cellspacing="0">
	<cfloop query="getusers">
	<cfquery name="gettasks" datasource="#ds#">
		select tasktracker_tasks.title, tasktracker_tasks.taskid, tasktracker_task_user_lookup.priority
		from tasktracker_tasks, tasktracker_task_user_lookup
		where tasktracker_tasks.taskid = tasktracker_task_user_lookup.taskid and tasktracker_task_user_lookup.adminuserid = #getusers.adminuserid#
		order by priority ASC
	</cfquery>
	<cfset userid = getusers.adminuserid>
    <tr>
        <td>
		<table border="0" cellpadding="0" cellspacing="0">
		<cfloop query="gettasks">
            <tr>
                <td>
				<cfoutput>
				<select name="priority#userid#" class="normal" onChange="reorder(this.form,priority#userid#, this, #evaluate(gettasks.priority - 1)#)">
				<cfloop index="x" from="1" to="#evaluate(gettasks.recordcount)#">
					<option value="#userid#;#gettasks.taskid#;#x#" <cfif gettasks.priority IS x>selected</cfif> >#x#</option>
				</cfloop>
				</select>&nbsp;&nbsp;
				</cfoutput>
				</td>
                <td>
				<cfoutput>
				#gettasks.title#
				</cfoutput>
				</td>
            </tr>
		</cfloop>
        </table>
		</td>
    </tr>
	</cfloop>
</table>
</form>
</div>
