
<link rel="stylesheet" type="text/css" href="../../styles.css">

<cfparam name="sort" default="user_asc">
<cfparam name="f" default="2,4,5">
<cfparam name="d" default="#variables.departmentlist#">

<cfif trim(cgi.QUERY_STRING) IS ""><cfset querystring = "f=#f#&sort=#sort#&d=#d#"><cfelse><cfset querystring = cgi.QUERY_STRING></cfif>

<cfoutput>
<script language="JavaScript">
<!--
function openfilter(q) {
	filterwin = window.open("taskfilter.cfm?" + q,"filterwin","width=450,height=125");
	filterwin.focus();
}
function opendeptfilter(q) {
	filterwin = window.open("taskfilter_dept.cfm?" + q,"filterwin","width=450,height=175");
	filterwin.focus();
}
//-->
</script> 
</cfoutput>

<cfswitch expression="#sort#">
	<cfcase value="user_asc"><cfset orderby = "lastname asc, firstname asc, priority asc"></cfcase>
	<cfcase value="user_desc"><cfset orderby = "lastname desc, firstname desc, priority asc"></cfcase>
</cfswitch>

<cfquery name="GetInfo" datasource="#ds#">
	select admin_users.adminuserid, admin_users.firstname, admin_users.lastname, tasktracker_tasks.duedate, tasktracker_tasks.taskid, tasktracker_tasks.title, tasktracker_status.description as status, tasktracker_task_user_lookup.priority, tasktracker_task_user_lookup.statusupdated, tasktracker_task_user_lookup.timespent
	from admin_users
	inner join tasktracker_task_user_lookup on admin_users.adminuserid = tasktracker_task_user_lookup.adminuserid
	inner join tasktracker_tasks on tasktracker_task_user_lookup.taskid = tasktracker_tasks.taskid
	inner join tasktracker_status on tasktracker_task_user_lookup.statusid = tasktracker_status.statusid
	inner join Admin_Users_Departments on tasktracker_tasks.departmentid = Admin_Users_Departments.departmentid
	where tasktracker_tasks.active <> 0 AND tasktracker_task_user_lookup.statusid IN (#f#) AND tasktracker_tasks.departmentid IN (#d#)
	order by #orderby#
</cfquery>
<div align="center">
<cfinclude template="footer.cfm">
<p>
    <table border="1" cellpadding="5" cellspacing="0">
		<cfoutput>
		<tr>
			<td colspan="2" class="highlightbar">
			<a href="javascript:openfilter('#querystring#&p=users.cfm');">Click to Filter By Status</a>
			<cfif getsec.seclevelid LTE 2>
			 | 
			<a href="javascript:opendeptfilter('#querystring#&p=users.cfm');">Click to Filter By Department</a>
			</cfif>
			</td>
		</tr>
		<tr>
			<td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td>
					<cfif sort IS "user_asc">
					    <b>
					    <cfset sortlink = "user_desc">
					    <cfelse>
					    <cfset sortlink = "user_asc">
					    </b>
					</cfif>
					<a href="users.cfm?sort=#sortlink#&f=#f#">User</a></td>
					<td>Assigned Tasks</td>
				</tr>
				</cfoutput>
				<cfoutput query="getinfo" group="adminuserid">
				<tr valign="top">
					<td nowrap>
					<cfif (cookie.adminlogin IS #getinfo.adminuserid#) OR (getsec.seclevelid LTE 2)>
						<a href="viewbyuser.cfm?id=#getinfo.adminuserid#">#getinfo.firstname# #getinfo.lastname#</a>
					<cfelse>
						#getinfo.firstname# #getinfo.lastname#
					</cfif>
					</td>
					<td>
					<table border="1" cellpadding="3" cellspacing="0">
						<tr>
							<td><b>Priority</b></td>
							<td><b>Task</b></td>
							<td><b>Status</b></td>
						</tr>
						<cfoutput>
						<tr>
							<td valign="top"><cfif getinfo.priority GT 0>#getinfo.priority#<cfelse>&nbsp;</cfif></td>
							<td valign="top">
							<a href="view.cfm?id=#getinfo.taskid#">#getinfo.title#</a>
							<cfif trim(getinfo.timespent) IS NOT ""><br>(Time Spent: #getinfo.timespent#)</cfif>
							<cfif trim(getinfo.duedate) IS NOT ""><br><cfif datediff("d",getinfo.duedate,now()) GT 0><span style="color:ff0000;"><cfelse><span></cfif>(Due Date: #dateformat(getinfo.duedate,'mm/dd/yyyy')#)</span></cfif>
							</td>
							<td valign="top">#getinfo.status# <cfif trim(getinfo.statusupdated) IS NOT "">#dateformat(getinfo.statusupdated,'mm/dd/yyyy')# #timeformat(getinfo.statusupdated, 'hh:mm tt')#</cfif></td>
						</tr>
						</cfoutput>
					</table></td>
				</tr>
				</cfoutput>
				<tr valign="top">
					<td colspan="2" align="center"><a href="add.cfm">Add New Task</a></td>
				</tr>
			</table>
			</td>
		</tr>
    </table>
	<p>
	<cfinclude template="footer.cfm">
</div>
