
<cfif isdefined("cookie.adminlogin")>
<cfquery name="getitems" datasource="#request.odbc_datasource#">
	select Admin_Security_Sections.*, Admin_Security_Items.*
	from Admin_Security_Sections
	inner join admin_security_items on admin_security_sections.sectionid = admin_security_items.sectionid
	inner join admin_security_lookup on admin_security_items.itemid = admin_security_lookup.itemid
	where admin_security_lookup.adminuserid = #cookie.adminlogin# and admin_security_sections.sectionid = 4
	order by admin_security_sections.sectionname asc, admin_security_items.itemname asc
</cfquery>

<div align="center" style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:11px">
<a href="/index.cfm">Main Menu</a> | 
<cfoutput query="getitems">
<a href="#itemurl#">#itemname#</a>
<cfif getitems.currentrow mod 5 is 0>
<br />
<cfelse>
<cfif getitems.currentrow IS NOT getitems.recordcount> | </cfif>
</cfif>
</cfoutput>
</div>
</cfif>
