
<cfquery name="getusers" datasource="#ds#">
	select adminuserid from tasktracker_task_user_lookup where taskid = #url.id#
</cfquery>
<cfset userlist = valuelist(getusers.adminuserid)>

<cfquery name="delUserLookup" datasource="#ds#">
	delete from tasktracker_task_user_lookup
	where taskid = #url.id#
</cfquery>

<cfquery name="deleteTask" datasource="#ds#">
	update tasktracker_tasks
	set active = 0
	where taskid = #url.id#
</cfquery>

<cfquery name="getpriorities" datasource="#ds#">
	select taskid, priority, adminuserid 
	from tasktracker_task_user_lookup 
	where adminuserid IN (#userlist#) and statusid NOT IN (1,5)
	order by adminuserid, priority asc
</cfquery>

<cfset p = 1>
<cfset curruser = 0>
<cfloop query="getpriorities">
	<cfif curruser IS NOT adminuserid><cfset p = 1><cfset curruser = getpriorities.adminuserid></cfif>
	<cfquery name="updatepriority" datasource="#ds#">
		update tasktracker_task_user_lookup
		set	priority = #p#
		where taskid = #taskid# and adminuserid = #adminuserid#
	</cfquery>
	<cfset p = p + 1>
</cfloop>

<cflocation url="index.cfm">
