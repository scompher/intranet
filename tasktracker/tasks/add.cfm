<link rel="stylesheet" type="text/css" href="../../styles.css">

<cfif isDefined("form.btnContinue")>

	<cfinclude template="add_save.cfm">

<cfelse>
	
	<cfparam name="form.categoryid" default="0">
	<cfparam name="form.title" default="">
	<cfparam name="form.sponsor" default="">
	<cfparam name="form.description" default="">
		
	<!--- find what departments user has access to --->
	<cfquery name="getdeptaccess" datasource="#ds#">
		select departmentid
		from admin_users_departments_lookup
		where adminuserid = #cookie.adminlogin#
	</cfquery>
	<cfset deptlist = valuelist(getdeptaccess.departmentid)>
	
	<!--- get users in departments --->
	<cfquery name="getusers" datasource="#ds#">
		select distinct admin_users.*, admin_users_departments_lookup.departmentid
		from admin_users, admin_users_departments_lookup
		where admin_users.adminuserid = admin_users_departments_lookup.adminuserid and admin_users_departments_lookup.departmentid IN (#deptlist#)
		order by admin_users.lastname asc, admin_users.firstname asc
	</cfquery>
	
	<cfquery name="getcats" datasource="#ds#">
		select * from tasktracker_categories
		order by category asc
	</cfquery>
	
	<cfquery name="getdepts" datasource="#ds#">
		select * from Admin_Users_Departments
		where Admin_Users_Departments.departmentid IN (#deptlist#)
		order by department asc
	</cfquery>

	<cfquery name="getstatus" datasource="#ds#">
		select * from tasktracker_status order by description asc
	</cfquery>

<cfoutput>
<script language="javascript" src="cal2.js">
/*
Xin's Popup calendar script-  Xin Yang (http://www.yxscripts.com/)
Script featured on/available at http://www.dynamicdrive.com/
This notice must stay intact for use
*/
</script>
<script language="javascript" src="cal_conf2.js"></script>
<script language="JavaScript">
<!--
function filterusers (department,users)
{
	var userArray = new Array(#getusers.recordcount#);
	<cfset count = 0>
	<cfloop query="getusers">
		userArray[#count#] = "#getusers.lastname#, #getusers.firstname#:#getusers.adminuserid#:#getusers.departmentid#";
		<cfset count = count + 1>
	</cfloop>
	
	var deptid = department.options[department.selectedIndex].value;

	users.options.length = 0;

	var j=0;
	var i = 0;
	while (i < userArray.length) {
		var temparray = userArray[i].split(":");
		if (temparray[2] == deptid) {
			users.options[j] = new Option(temparray[0],temparray[1]);
			j++
		}
		i++;
	}
}
function checkform(frm) {
	if (frm.title.value == "") {alert('The task title is required.'); frm.title.focus(); return false;}
	if (frm.departmentid.selectedIndex == 0) {
		alert('You must choose a department.'); 
		frm.departmentid.focus(); 
		return false;
	}
	frm.submit();
}
//-->
</script> 
</cfoutput>
	
<div align="center">
<cfinclude template="footer.cfm">
<p>
	<form method="post" action="add.cfm">
	<cfif isdefined("url.quickentry")>
	<input type="hidden" name="quickentry" value="1">
	</cfif>
	<table border="0" cellpadding="5" cellspacing="0">
		<tr>
			<td class="highlightbar" align="center" valign="middle"><b>Add A New Task </b></td>
		</tr>
		<tr>
			<td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td valign="middle">
					<b>Category:</b><br>
					<select name="categoryid" class="normal" style="width:150px">
						<option value="0">None</option>
					<cfoutput query="getcats">
						<option value="#getcats.categoryid#" <cfif form.categoryid IS getcats.categoryid>selected</cfif> >#getcats.category#</option>
					</cfoutput>
					</select>
					</td>
				</tr>
				<cfoutput>
				<tr>
					<td valign="middle"><b>Project Sponsor:</b><br>
					<input name="sponsor" type="text" class="normal" size="58" maxlength="100" value="#form.sponsor#">
					</td>
				</tr>
				<tr>
					<td valign="middle">
					<b>Title:</b><br>
					<input name="title" type="text" class="normal" size="58" maxlength="100" value="#form.title#">
					</td>
				</tr>
				<cfif getsec.seclevelid LTE 3 AND not isdefined("url.quickentry")>
				<tr>
					<td valign="middle">
					<b>Due Date:</b> (optional)<br>
					<input onFocus="if (this.value.length > 0) this.select();" name="duedate" type="text" class="normal" size="12" maxlength="10" value="">&nbsp;
					<a style="text-decoration:none;" href="javascript:showCal('TaskCalendarDueDate');"><img align="absmiddle" border="0" src="/images/calicon.gif"></a>
					</td>
				</tr>
				</cfif>
				<tr>
					<td valign="top">
					<b>Description:</b><br>
					<textarea name="description" cols="60" rows="5" class="normal">#form.description#</textarea>
					</td>
				</tr>
				</cfoutput>
				<!--- <cfif getsec.seclevelid LTE 2 AND not isdefined("url.quickentry")> --->
					<tr>
						<td>
						<b>Select Department:</b><br>
						<select name="departmentid" class="normal" style="width:150px" onChange="filterusers(this.form.departmentid,this.form.assignedUsers);">
							<option value="0">None</option>
						<cfoutput query="getdepts">
							<option value="#getdepts.departmentid#">#department#</option>
						</cfoutput>
						</select>
						</td>
					</tr>
					<tr>
						<td>
						<b>Assigned To:</b><br>
						<select name="assignedUsers" size="10" multiple class="normal" style="width:250px">
							<option value="0"></option>
						</select>
						<br>
						<span class="small" style="color:FF0000">Hold CTRL or SHIFT to select multiples</span>
						</td>
					</tr>
					<!--- 		
					<cfelse>
					<tr>
						<td>
						<b>Select Department:</b><br>
						<select name="departmentid" class="normal" style="width:150px">
							<option value="0">None</option>
						<cfoutput query="getdepts">
							<option value="#getdepts.departmentid#" <cfif getsec.defaultdeptid IS getdepts.departmentid>selected</cfif> >#department#</option>
						</cfoutput>
						</select>
						</td>
					</tr>
					<cfoutput>
					<!--- <input type="hidden" name="departmentid" value="#deptlist#"> --->
					<tr>
						<td>
						<b>Assigned To:</b><br>
						<select name="assignedUsers" class="normal" style="width:150px">
							<option value="0">Unassigned</option>
							<option value="#cookie.adminlogin#">Myself</option>
						</select>
						</td>
					</tr>
					</cfoutput>
					<tr>
						<td>
						<b>Status:</b><br>
						<select name="statusid" style="width:150px" class="normal">
						<cfoutput query="getstatus">
						<cfif getstatus.description IS NOT "Unassigned">
							<option value="#getstatus.statusid#">#getstatus.description#</option>
						</cfif>
						</cfoutput>
						</select>
						<br>
						<span class="small" style="color:FF0000">If user is unassigned, status will also be unassigned</span>
						</td>
					</tr>
				</cfif>
				--->
				<tr>
					<td>
					<input type="hidden" name="btnContinue" value="1">
					<input onClick="return checkform(this.form);" type="button" value="Continue"><br>
					</td>
				</tr>

			</table>
			</td>
		</tr>
	</table>
	</form>
	</div>
<p>
<cfinclude template="footer.cfm">
</cfif>

