
<cfif isDefined("form.btnAddNote")>

	<cfset edited = createodbcdatetime(now())>

	<!--- save archive--->
	<cfquery name="addarchive" datasource="#ds#">
		insert into tasktracker_notes_archive (notesid, oldcontent, edited)
		values (#form.id#, '#form.oldnote#', #edited#)
	</cfquery>
	
	<!--- save note --->
	<cfquery name="updatenote" datasource="#ds#">
		update tasktracker_notes 
		set content = '#form.content#', edited = #edited#
		where notesid = #form.id#
	</cfquery>
	
	<!--- refresh opener window --->
	<cfoutput>
	<script language="JavaScript">
	<!--
	self.opener.location="view.cfm?id=#form.taskid#";
	self.close();
	//-->
	</script> 
	</cfoutput>
	
	<!--- send new note notification 
	<cfquery name="getoriginator" datasource="#ds#">
		select admin_users.email, admin_users.firstname
		from admin_users
		inner join tasktracker_tasks on tasktracker_tasks.originatorid = admin_users.adminuserid
		where tasktracker_tasks.taskid = #id#
	</cfquery>
	<cfquery name="getusers" datasource="#ds#">
		select admin_users.email, admin_users.firstname
		from admin_users
		inner join tasktracker_task_user_lookup on tasktracker_task_user_lookup.adminuserid = admin_users.adminuserid
		where tasktracker_task_user_lookup.taskid = #id# and admin_users.adminuserid <> #cookie.adminlogin#
	</cfquery>
	<cfset emaillist = valuelist(getusers.email)>	
	<cfset emaillist = listappend(emaillist, getoriginator.email)>

<!--- send users email --->
<cfloop index="email" list="#emaillist#">
<cfmail from="pgregory@copsmonitoring.com" to="#email#" subject="Task Manager Notification : Note Added to Task" username="copalink@copsmonitoring.com" password="copsmoncal">
A note has been added to the following task:

#form.title# 
#form.description# 

You may view the details of this task by clicking on the following link:

#request.appurl#tasktracker/tasks/view.cfm?id=#id#
</cfmail>
</cfloop>
--->
<cfelse>
	
	<cfquery name="getnote" datasource="#ds#">
		select * from tasktracker_notes
		where notesid = #id#
	</cfquery>
	
	<cfquery name="getuser" datasource="#ds#">
		select * from admin_users where adminuserid = #cookie.adminlogin#
	</cfquery>
	
	<body onLoad="document.forms[0].content.focus();">
	<div align="center">
		<form method="post" action="editnote.cfm">
		<cfoutput>
		<input type="hidden" name="taskid" value="#getnote.taskid#">
		<input type="hidden" name="id" value="#id#">
		<input type="hidden" name="oldnote" value="#getnote.content#">
		</cfoutput>
		<table border="0" cellpadding="3">
			<cfoutput>
			<tr>
				<td><b>#getuser.firstname# #getuser.lastname#</b></td>
			</tr>
			</cfoutput>
			<cfoutput query="getnote">
			<tr>
				<td><textarea name="content" cols="70" rows="7" class="normal">#getnote.content#</textarea></td>
			</tr>
			</cfoutput>
			<tr>
				<td>
				<input name="btnAddNote" type="submit" value="Update Note">
				<input onClick="self.close();" type="button" value="Close">
				</td>
			</tr>
		</table>
		</form>
	</div>
	</body>
</cfif>
