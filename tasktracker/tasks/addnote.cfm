
<cfif isDefined("form.btnAddNote")>

	<cfset created = createodbcdatetime(now())>
	
	<!--- save note --->
	<cfquery name="addnote" datasource="#ds#">
		insert into tasktracker_notes (creatorID, taskID, created, content)
		values (#cookie.adminlogin#, #id#, #created#, '#form.content#')
	</cfquery>
	
	<!--- get title and desc --->
	<cfquery name="getinfo" datasource="#ds#">
		select * from tasktracker_tasks
		where taskid = #id#
	</cfquery>
	
	<!--- send new note notification --->
	<cfquery name="getoriginator" datasource="#ds#">
		select admin_users.email, admin_users.firstname, admin_users.adminuserid
		from admin_users
		inner join tasktracker_tasks on tasktracker_tasks.originatorid = admin_users.adminuserid
		where tasktracker_tasks.taskid = #id#
	</cfquery>
	<cfquery name="getusers" datasource="#ds#">
		select admin_users.email, admin_users.firstname
		from admin_users
		inner join tasktracker_task_user_lookup on tasktracker_task_user_lookup.adminuserid = admin_users.adminuserid
		where tasktracker_task_user_lookup.taskid = #id# and admin_users.adminuserid <> #cookie.adminlogin#
	</cfquery>
	<cfset emaillist = valuelist(getusers.email)>
	<cfif cookie.adminlogin IS NOT getoriginator.adminuserid>
		<cfset emaillist = listappend(emaillist, getoriginator.email)>
	</cfif>

<!--- send users email --->
<cfloop index="email" list="#emaillist#">
<cfmail from="pgregory@copsmonitoring.com" to="#email#" subject="Task Manager Notification : Note Added to Task" username="copalink@copsmonitoring.com" password="copsmoncal">
A note has been added to the following task:

#getinfo.title# 
#getinfo.description# 

You may view the details of this task by clicking on the following link:

#request.appurl#tasktracker/tasks/view.cfm?id=#id#
</cfmail>
</cfloop>

<!--- refresh opener window --->
<cfoutput>
<script language="JavaScript">
<!--
self.opener.location="view.cfm?id=#id#";
self.close();
//-->
</script> 
</cfoutput>

<cfelse>
	
	<cfquery name="getuser" datasource="#ds#">
		select * from admin_users where adminuserid = #cookie.adminlogin#
	</cfquery>
	
	<body onLoad="document.forms[0].content.focus();">
	<div align="center">
		<form method="post" action="addnote.cfm">
		<cfoutput>
		<input type="hidden" name="id" value="#id#">
		</cfoutput>
		<table border="0" cellpadding="3">
			<cfoutput>
			<tr>
				<td><b>#getuser.firstname# #getuser.lastname#</b></td>
			</tr>
			</cfoutput>
			<tr>
				<td><textarea name="content" cols="70" rows="7" class="normal"></textarea></td>
			</tr>
			<tr>
				<td>
				<input name="btnAddNote" type="submit" value="Add Note">
				<input onClick="self.close();" type="button" value="Close">
				</td>
			</tr>
		</table>
		</form>
	</div>
	</body>
</cfif>
