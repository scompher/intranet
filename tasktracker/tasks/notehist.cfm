
<link rel="stylesheet" type="text/css" href="../../styles.css">

<cfquery name="getnotearchive" datasource="#ds#">
	select tasktracker_notes_archive.*, tasktracker_notes.notesid
	from tasktracker_notes_archive
	inner join tasktracker_notes on tasktracker_notes_archive.notesid = tasktracker_notes.notesid
	where tasktracker_notes_archive.notesid = #id#
	order by tasktracker_notes_archive.edited DESC
</cfquery>

<cfoutput query="getnotearchive">
<table width="100%" border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td class="highlightbar"><B>#dateformat(edited, 'mm/dd/yyyy')# #timeformat(edited, 'hh:mm tt')#</B></td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td>#oldcontent#</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<br />
</cfoutput>
