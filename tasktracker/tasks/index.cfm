<link rel="stylesheet" type="text/css" href="../../styles.css">

<cfparam name="sort" default="date_asc">
<cfparam name="f" default="2,4,5">
<cfparam name="d" default="#variables.departmentlist#">

<!--- <cfif (trim(cgi.QUERY_STRING) IS "")><cfset querystring = "f=#f#&sort=#sort#&d=#d#"><cfelse><cfset querystring = cgi.QUERY_STRING></cfif> --->
<cfset querystring = "f=#f#&sort=#sort#&d=#d#">

<cfoutput>
<script language="JavaScript">
<!--
function openfilter(q) {
	filterwin = window.open("taskfilter.cfm?" + q,"filterwin","width=450,height=125");
	filterwin.focus();
}
function opendeptfilter(q) {
	filterwin = window.open("taskfilter_dept.cfm?" + q,"filterwin","width=450,height=175");
	filterwin.focus();
}
//-->
</script> 
</cfoutput>

<cfswitch expression="#sort#">
	<cfcase value="date_asc"><cfset orderby = "created asc"></cfcase>
	<cfcase value="date_desc"><cfset orderby = "created desc"></cfcase>
	<cfcase value="task_asc"><cfset orderby = "title asc"></cfcase>
	<cfcase value="task_desc"><cfset orderby = "title desc"></cfcase>
	<cfcase value="user_asc"><cfset orderby = "lastname asc, firstname asc"></cfcase>
	<cfcase value="user_desc"><cfset orderby = "lastname desc, firstname desc"></cfcase>
	<cfcase value="status_asc"><cfset orderby = "status asc"></cfcase>
	<cfcase value="status_desc"><cfset orderby = "status desc"></cfcase>
	<cfcase value="statusdate_asc"><cfset orderby = "statusupdated asc"></cfcase>
	<cfcase value="statusdate_desc"><cfset orderby = "statusupdated desc"></cfcase>
	<cfcase value="cat_asc"><cfset orderby = "category asc"></cfcase>
	<cfcase value="cat_desc"><cfset orderby = "category desc"></cfcase>
	<cfcase value="dept_asc"><cfset orderby = "Admin_Users_Departments.department asc"></cfcase>
	<cfcase value="dept_desc"><cfset orderby = "Admin_Users_Departments.department desc"></cfcase>
</cfswitch>

<cfquery name="getalltask" datasource="#ds#">
	select tasktracker_tasks.taskid, tasktracker_tasks.duedate, tasktracker_tasks.title, tasktracker_tasks.active, tasktracker_tasks.created, tasktracker_status.description as status, admin_users.firstname, admin_users.lastname, admin_users.adminuserid, tasktracker_categories.category, Admin_Users_Departments.department, tasktracker_task_user_lookup.statusupdated, tasktracker_task_user_lookup.timespent, tasktracker_task_user_lookup.statusid as sid
	from tasktracker_tasks
	left join tasktracker_categories on tasktracker_categories.categoryid = tasktracker_tasks.categoryid
	inner join tasktracker_task_user_lookup on tasktracker_tasks.taskid = tasktracker_task_user_lookup.taskid
	inner join tasktracker_status on tasktracker_task_user_lookup.statusid = tasktracker_status.statusid
	inner join Admin_Users_Departments on tasktracker_tasks.departmentid = Admin_Users_Departments.departmentid
	left join admin_users on tasktracker_task_user_lookup.adminuserid = admin_users.adminuserid
	where tasktracker_tasks.active <> 0 AND tasktracker_task_user_lookup.statusid IN (#f#) AND tasktracker_tasks.departmentid IN (#d#)
	order by #orderby#
</cfquery>

<div align="center">
<cfinclude template="footer.cfm">
<p>

<table width="955" border="0" cellspacing="0" cellpadding="5">
    <tr>
        <td class="highlightbar"><b>Tasks</b></td>
    </tr>
    <tr>
        <td class="greyrowbottomnopadding" style="border:none;">
		
<table width="100%" border="1" style="border:1px #000000 solid; border-left:0px; border-top:0px;" cellpadding="5" cellspacing="0">
	<cfoutput>
	<tr>
	    <td colspan="7">
		<a href="javascript:openfilter('#querystring#&p=index.cfm');">Click to Filter By Status</a>
		<cfif getsec.seclevelid LTE 2>
		 | 
		<a href="javascript:opendeptfilter('#querystring#&p=index.cfm');">Click to Filter By Department</a>
		</cfif>
		</td>
    </tr>
	<tr>
        <td align="center" width="50%">
		<cfif sort IS "task_asc"><cfset sortlink = "task_desc"><cfelse><cfset sortlink = "task_asc"></cfif>
		<a href="index.cfm?sort=#sortlink#&f=#f#&d=#d#"><b>Task Name</b></a>
		</td>
        <td align="center">
		<cfif sort IS "dept_asc"><cfset sortlink = "dept_desc"><cfelse><cfset sortlink = "dept_asc"></cfif>
		<a href="index.cfm?sort=#sortlink#&f=#f#&d=#d#"><b>Dept.</b></a>
		</td>
		<td align="center">
		<cfif sort IS "cat_asc"><cfset sortlink = "cat_desc"><cfelse><cfset sortlink = "cat_asc"></cfif>
		<a href="index.cfm?sort=#sortlink#&f=#f#&d=#d#"><b>Category</b></a>
		</td>
		<td align="center">
		<cfif sort IS "date_asc"><cfset sortlink = "date_desc"><cfelse><cfset sortlink = "date_asc"></cfif>
		<a href="index.cfm?sort=#sortlink#&f=#f#&d=#d#"><b>Created</b></a>
		</td>
		<td align="center">
		<cfif sort IS "user_asc"><cfset sortlink = "user_desc"><cfelse><cfset sortlink = "user_asc"></cfif>
		<a href="index.cfm?sort=#sortlink#&f=#f#&d=#d#"><b>Assigned To</b></a>
		</td>
		<td align="center">
		<cfif sort IS "status_asc"><cfset sortlink = "status_desc"><cfelse><cfset sortlink = "status_asc"></cfif>
		<a href="index.cfm?sort=#sortlink#&f=#f#&d=#d#"><b>Status</b></a> | 
		<cfif sort IS "statusdate_desc"><cfset sortlink = "statusdate_asc"><cfelse><cfset sortlink = "statusdate_desc"></cfif>
		<a href="index.cfm?sort=#sortlink#&f=#f#&d=#d#"><b>Date</b></a>
		</td>
        <td align="center"><b>Actions</b></td>
    </tr>
	</cfoutput>
	<cfif getalltask.recordcount GT 0>
		<cfoutput query="getalltask" group="taskid">
			<cfset userlist = "">
			<cfoutput><cfset userlist = listappend(userlist,adminuserid)></cfoutput>
			<cfif listfind(userlist,cookie.adminlogin) IS NOT 0>
			<tr>
			<cfelse>
			<tr>
			</cfif>
				<td valign="top">
				<a href="view.cfm?id=#getalltask.taskid#">#getalltask.title#</a> 
				<cfif trim(getalltask.timespent) IS NOT "" AND getalltask.sid IS 1><br>(Time Spent: #getalltask.timespent#)</cfif>
				<cfif trim(getalltask.duedate) IS NOT ""><br><cfif datediff("d",getalltask.duedate,now()) GT 0><span style="color:ff0000;"><cfelse><span></cfif>(Due Date: #dateformat(getalltask.duedate,'mm/dd/yyyy')#)</span></cfif>
				</td>
				<td valign="top">#getalltask.department#&nbsp;</td>
				<td valign="top">#getalltask.category#&nbsp;</td>
				<td valign="top" align="center" nowrap>#dateformat(getalltask.created,'mm/dd/yyyy')# #timeformat(getalltask.created, 'hh:mm tt')#</td></td>
				<td valign="top" nowrap>
				<cfoutput>
				<a href="viewbyuser.cfm?id=#getalltask.adminuserid#">#getalltask.firstname# #getalltask.lastname#</a><br>
				</cfoutput>
				</td>
				<td valign="top" align="center" nowrap>
				<cfoutput>
				#getalltask.status# <cfif trim(getalltask.statusupdated) IS NOT "">#dateformat(getalltask.statusupdated,'mm/dd/yyyy')# #timeformat(getalltask.statusupdated,'hh:mm tt')#</cfif><br>
				</cfoutput>
				</td>
				<td valign="middle" align="center">
				<cfif getsec.seclevelid LTE 2 or listfind(userlist,cookie.adminlogin) IS NOT 0>
				<a href="edit.cfm?id=#getalltask.taskid#"><img src="/images/edit.gif" border="0" alt="Edit Task"></a>
				</cfif>
				&nbsp;
				<cfif getsec.seclevelid LTE 2>
				<a onClick="return confirm('Are you sure you wish to delete this item?');" href="delete.cfm?id=#getalltask.taskid#"><img src="/images/delete.gif" border="0" alt="Delete Task"></a>
				</cfif>
				&nbsp;
				</td>
			</tr>
		</cfoutput>
	<cfelse>
    <tr>
        <td colspan="7">There are no tasks currently on file</td>
    </tr>	
	</cfif>
    <tr>
        <td colspan="7" align="center"><a href="add.cfm">Add New Task</a></td>
    </tr>
</table>

		</td>
    </tr>
</table>

<p>
<cfinclude template="footer.cfm">
</div>
