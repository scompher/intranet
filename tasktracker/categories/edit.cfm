
<cfif isdefined("form.btnSave")>

	<cfquery name="insertcat" datasource="#ds#">
		update tasktracker_categories 
		set category = '#form.category#'
		where categoryid = #form.categoryid#
	</cfquery>

	<cflocation url="index.cfm">

<cfelse>

<style type="text/css">
	TABLE {font-family:Arial, Helvetica, sans-serif, sans-serif;font-size:12px}
	.normal {font-family:Arial, Helvetica, sans-serif, sans-serif;font-size:12px}
	.heading {font-family:Arial, Helvetica, sans-serif;font-size:12px}
	.titlebar {font-family: Arial, Helvetica, sans-serif;	font-size: 12px;font-weight: bold;color: #FFFFFF;}
	a:link {color: #000000; text-decoration: none;}
	a:visited {text-decoration: none; color: #000000;}
	a:hover {text-decoration: none; color: #0000FF;}
	a:active {text-decoration: none; color: #000000;}
</style>

<cfquery name="getcat" datasource="#ds#">
	select * from tasktracker_categories
	where categoryid = #categoryid#
</cfquery>

<div align="center">
<form method="post" action="edit.cfm">
<cfoutput>
<input type="hidden" name="categoryid" value="#categoryid#">
</cfoutput>
<table width="397" border="1" cellpadding="5" cellspacing="0">
    <tr>
        <td width="383" bgcolor="#dddddd"><b>Edit A Category </b></td>
    </tr>
	<cfoutput query="getcat">
    <tr>
        <td valign="bottom"><b>Category:
                <br>
                <input type="text" name="category" class="normal" size="75" maxlength="255" value="#getcat.category#">
                <br>
                <input type="submit" name="btnSave" value="Update Category" class="normal">
		</td>
	</tr>
	</cfoutput>
</table>
</form>
</div>

</cfif>