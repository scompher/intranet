
<style type="text/css">
	TABLE {font-family:Arial, Helvetica, sans-serif, sans-serif;font-size:12px}
	.normal {font-family:Arial, Helvetica, sans-serif, sans-serif;font-size:12px}
	.heading {font-family:Arial, Helvetica, sans-serif;font-size:12px}
	.titlebar {font-family: Arial, Helvetica, sans-serif;	font-size: 12px;font-weight: bold;color: #FFFFFF;}
	a:link {color: #000000; text-decoration: none;}
	a:visited {text-decoration: none; color: #000000;}
	a:hover {text-decoration: none; color: #0000FF;}
	a:active {text-decoration: none; color: #000000;}
</style>

<cfquery name="getcats" datasource="#ds#">
	select * from tasktracker_categories
	order by category asc
</cfquery>

<div align="center">
    <table width="400" border="1" cellpadding="5" cellspacing="0">
        <tr>
            <td colspan="2" align="center" bgcolor="#dddddd"><b>Category Administration </b></td>
        </tr>
        <tr>
            <td width="60" align="center" bgcolor="#dddddd"><b>Action</b></td>
            <td bgcolor="#dddddd"><b>Category</b></td>
        </tr>
		<cfoutput query="getcats">
        <tr>
            <td align="center">
			<cfif getsec.seclevelid LTE 2>
			<a href="edit.cfm?categoryid=#categoryid#"><img src="/images/edit.gif" border="0" alt="Edit Category"></a>&nbsp;&nbsp;
			</cfif>
			<cfif getsec.seclevelid LTE 1>
			<a onClick="return confirm('Are you sure you wish to delete this category?');" href="delete.cfm?categoryid=#categoryid#"><img src="/images/delete.gif" border="0" alt="Delete Category">
			</cfif>
			</td>
            <td>#category#</td>
        </tr>
		</cfoutput>
        <tr>
            <td align="center"><a href="add.cfm">Add New</a></td>
            <td>&nbsp;</td>
        </tr>
    </table>
	<table border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td><a href="/index.cfm">Return to Administrative Menu</a></td>
	</tr>
	</table>
</div>
