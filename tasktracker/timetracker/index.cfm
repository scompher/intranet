
<cfparam name="url.edit" default="0">
<cfparam name="day" default="#dateformat(now(),'mm/dd/yyyy')#">

<!--- add time item --->
<cfif isdefined("form.btnAddItem")>
	<cfquery name="insertItem" datasource="#ds#">
		insert into timetracker_items (adminuserid, hours, taskid, description, timeday)
		values (#cookie.adminlogin#, '#form.hours#', #form.taskid#, '#form.description#', #form.day#)
	</cfquery>
</cfif>
<!--- add time item --->

<!--- update time item --->
<cfif isdefined("form.btnUpdateItem")><br />
	<cfquery name="updateitem" datasource="#ds#">
		update timetracker_items 
		set hours = '#form.edithours#', taskid = #form.edittaskid#, description = '#form.editdescription#'
		where itemid = #form.edititemid#
	</cfquery>
</cfif>
<!--- update time item --->

<cfif not isdate(day)><cfset day = dateformat(now(),'mm/dd/yyyy')></cfif>
<cfset day = createodbcdate(day)>

<cfquery name="gettasks" datasource="#ds#">
	select tasktracker_tasks.taskid, tasktracker_tasks.title
	from tasktracker_tasks
	left join tasktracker_task_user_lookup on tasktracker_tasks.taskid = tasktracker_task_user_lookup.taskid
	where tasktracker_task_user_lookup.adminuserid = #cookie.adminlogin# and tasktracker_task_user_lookup.statusid <> 1 and tasktracker_tasks.active <> 0
	order by tasktracker_tasks.title asc
</cfquery>
<cfset t = queryaddrow(gettasks)>
<cfset t = querysetcell(gettasks,"taskid",0)>
<cfset t = querysetcell(gettasks,"title","Miscellaneous")>

<cfquery name="gettime" datasource="#ds#">
	select timetracker_items.itemid, timetracker_items.hours, timetracker_items.description, timetracker_items.taskid, tasktracker_tasks.title
	from timetracker_items
	left join tasktracker_tasks on timetracker_items.taskid = tasktracker_tasks.taskid
	where adminuserid = #cookie.adminlogin# and timeday = #day#
</cfquery>

<script language="JavaScript" type="text/javascript">
	function showMonth(urlString) {
		var calendarWin = window.open("showcalendar.cfm?p=index.cfm&" + urlString, "calendarWin", "width=375,height=350");
	}

	function checkform(frm) {
		if (frm.taskid.selectedIndex == 0) {alert('The task is required, or select Miscellaneous.'); frm.taskid.focus(); return false;}
		// if (frm.description.value == "") {alert('The description is required.'); frm.description.focus(); return false;}
		return true;
	}
</script>

<div align="center" class="normal">
<form method="post" action="index.cfm">
<cfoutput>
<input type="hidden" name="day" value="#day#" />
</cfoutput>
<table width="725" border="1" cellpadding="5" cellspacing="0" class="normal">
	<tr>
		<td align="center" class="header"><b>Time Tracker</b></td>
	</tr>
	<tr>
		<td bgcolor="FFFFCC">
		<table width="725" border="0" cellspacing="0" cellpadding="3">
			<cfoutput>
			<tr>
				<td width="445" nowrap="nowrap" bgcolor="FFFFCC"><b>Time for #dateformat(day,'dddd')#, #dateformat(day, 'long')#</b> (click item to edit)</td>
				<td width="268" align="right" bgcolor="FFFFCC">
				<a href="index.cfm?day=#dateformat(now(),'mm/dd/yyyy')#"><b>Today</b></a> | 
				<a href="index.cfm?day=#dateformat(dateadd("d",-1,day),'mm/dd/yyyy')#"><b>Previous Day</b></a> | 
				<a href="index.cfm?day=#dateformat(dateadd("d",1,day),'mm/dd/yyyy')#"><b>Next Day</b></a> | 
				<a href="javascript:showMonth('m=#dateformat(day,'m')#&y=#dateformat(day,'yyyy')#');"><b>Month View</b></a>
				</td>
			</tr>
			</cfoutput>
		</table>
		</td>
	</tr>
	<tr>
		<td><table width="725" border="1" cellspacing="0" cellpadding="3">
			<tr>
				<td width="71" align="center" nowrap="nowrap" bgcolor="DDDDDD"><b>Hours</b></td>
				<td width="215" bgcolor="DDDDDD"><b>Task</b></td>
				<td colspan="2" bgcolor="DDDDDD"><b>Description</b></td>
			</tr>
			<cfset totalhours = 0.00>
			<cfif gettime.recordcount GT 0>
				<cfoutput query="gettime">
						<cfif url.edit is not gettime.itemid>
							<tr>
								<td align="center" valign="top" nowrap="nowrap">#numberformat(gettime.hours,"0.00")#</td>
								<td valign="top"><a href="index.cfm?edit=#itemid#&amp;day=#dateformat(day,'mm/dd/yyyy')#"><cfif gettime.taskid IS 0>Miscellaneous<cfelse>#gettime.title#</cfif></a></td>
								<td width="358" valign="top">#replace(gettime.description, "#chr(13)#", "<br>", "all")#&nbsp;</td>
								<td width="47" valign="top" nowrap="nowrap"><a onclick="return confirm('Are you sure you wish to remove this item?');" href="remove.cfm?id=#gettime.itemid#&amp;day=#dateformat(day,'mm/dd/yyyy')#">[remove]</a></td>
							</tr>
							<cfset totalhours = totalhours + gettime.hours>
						<cfelse>
							<input type="hidden" name="edititemid" value="#gettime.itemid#" />
							<tr>
								<td align="center" valign="top" nowrap="nowrap" bgcolor="DDDDDD">
								<select name="edithours" class="normal">
								<cfloop from="0.00" to="24.00" step="0.25" index="hour">
									<option value="#numberformat(hour,"0.00")#" <cfif gettime.hours is hour>selected</cfif> >#numberformat(hour,"0.00")#</option>
								</cfloop>
								</select>
								</td>
								<td colspan="3" valign="top" bgcolor="DDDDDD">
								<cfset tid = gettime.taskid>
								<cfquery name="checktask" dbtype="query">
									select * from gettasks where taskid = #gettime.taskid#
								</cfquery>
								<cfif checktask.recordcount gt 0>
									<select name="edittaskid" class="normal" style="width:625">
									<cfloop query="gettasks">
										<option value="#taskid#" <cfif gettasks.taskid is tid>selected</cfif> >#title#</option>
									</cfloop>
									</select>
								<cfelse>
									#gettime.title#
								</cfif>
								</td>
							</tr>
							<tr>
								<td colspan="4" align="center" valign="top" nowrap="nowrap" bgcolor="DDDDDD">
								<textarea name="editdescription" rows="3" class="normal" style="width:720">#gettime.description#</textarea>
								</td>
							</tr>
							<tr>
								<td colspan="4" valign="top" nowrap="nowrap" bgcolor="DDDDDD">
								<input type="submit" name="btnUpdateItem" value="Update Item" />
								</td>
							</tr>
						</cfif>
				</cfoutput>
				<cfoutput>
				<tr>
					<td align="center" valign="top" nowrap="nowrap"><b>#numberformat(totalhours,"0.00")#</b></td>
					<td colspan="3" valign="top"><b>Total time for #dateformat(day, 'long')#</b></td>
				</tr>
				</cfoutput>
			</cfif>
		</table>
		</td>
	</tr>
	<tr>
		<td bgcolor="FFFFCC"><b>New Item Entry: </b></td>
	</tr>
	<tr>
		<td>
		<table width="725" border="1" cellspacing="0" cellpadding="3">
			<tr>
				<td width="67" align="center" bgcolor="DDDDDD"><b>Hours</b></td>
				<td width="645" bgcolor="DDDDDD"><b>Task</b> (Add and edit tasks in the task manager)</td>
			</tr>
			<tr>
				<td align="center">
				<select name="hours" class="normal">
				<cfloop from="0.00" to="24.00" step="0.25" index="hour">
				<cfoutput>
					<option value="#numberformat(hour,"0.00")#">#numberformat(hour,"0.00")#</option>
				</cfoutput>
				</cfloop>
				</select>
				</td>
				<td>
				<select name="taskid" class="normal" style="width:625">
					<option></option>
				<cfoutput query="gettasks">
					<option value="#taskid#">#title#</option>
				</cfoutput>
				</select>
				</td>
			</tr>
			<tr>
				<td colspan="2" bgcolor="DDDDDD"><b>Description</b></td>
				</tr>
			<tr>
				<td colspan="2"><textarea name="description" rows="3" class="normal" style="width:720"></textarea></td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td>
		<input type="submit" name="btnAddItem" value="Add Item" onclick="return checkform(this.form);" />
		</td>
	</tr>
</table>
</form>
<cfinclude template="/tasktracker/tasks/footer.cfm">
</div>

