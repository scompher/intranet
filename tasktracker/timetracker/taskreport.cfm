
<cfif getsec.seclevelid LTE 2>
	<cfparam name="uid" default="-1">
<cfelse>
	<cfparam name="uid" default="#cookie.adminlogin#">
</cfif>

<cfparam name="tid" default="-1">

<cfquery name="getusers" datasource="#ds#">
	select distinct admin_users.adminuserid, admin_users.firstname, admin_users.lastname
	from admin_users
	inner join Admin_Users_Departments_Lookup on admin_users.adminuserid = Admin_Users_Departments_Lookup.adminuserid
	where Admin_Users_Departments_Lookup.departmentid in (1,4)
	order by admin_users.lastname asc, admin_users.firstname asc
</cfquery>

<cfquery name="gettasks" datasource="#ds#">
	select distinct tasktracker_tasks.title, timetracker_items.taskid
	from timetracker_items
	left join tasktracker_tasks on timetracker_items.taskid = tasktracker_tasks.taskid
	where (timetracker_items.adminuserid = #uid#) or (timetracker_items.adminuserid = #uid# and timetracker_items.taskid = 0)
	order by title
</cfquery>

<cfquery name="getreport" datasource="#ds#">
	select  sum(hours) as sumhours
	from timetracker_items
	where taskid = #tid#
</cfquery>

<style type="text/css">
	TABLE {font-family:Arial, Helvetica, sans-serif;font-size:12px; background-color:#FFFFFF;}
	.normal {font-family:Arial, Helvetica, sans-serif;font-size:12px}
	.heading {font-family:Arial, Helvetica, sans-serif;font-size:16px}
	.titlebar {font-family: Arial, Helvetica, sans-serif;	font-size: 12px;font-weight: bold;color: #FFFFFF;}
	.small {font-family:Verdana, Arial, Helvetica, sans-serif;font-size:10px}
	a:link {color:#000000; text-decoration: underline;}
	a:visited {text-decoration: underline; color: #000000;}
	a:hover {text-decoration: underline; color: #0000FF;}
	a:active {text-decoration: underline; color: #000000;}
	.box {border: 1px solid #000000;}
	.header {font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #FFFFFF; background-color: #000066;}
</style>

<script language="JavaScript" type="text/javascript">
	function checkform(frm) {
		if (frm.tid.selectedIndex == 0) {alert('Please select a task.'); return false;}
		
		return true;
	}
</script>

<div align="center" class="normal">
<form method="post" action="taskreport.cfm">
<table width="725" border="1" cellspacing="0" cellpadding="5">
	<tr>
		<td colspan="2" align="center" class="header"><b>Time Tracker Task Repor</b></td>
	</tr>
	<cfif getsec.seclevelid LTE 2>
	<tr>
		<td width="80" nowrap><b>Select User:</b></td>
		<td width="619">
		<select name="uid" class="normal">
			<option value="0">Select User</option>
			<cfoutput query="getusers">
				<option <cfif getusers.adminuserid is uid>selected</cfif> value="#getusers.adminuserid#">#firstname# #lastname#</option>
			</cfoutput>
		</select>
		<input type="submit" value="Submit" class="normal">
		</td>
	</tr>
	</cfif>
	<cfif gettasks.recordcount gt 0>
	<tr>
		<td nowrap><b>Select Task:</b></td>
		<td>
		<select name="tid" class="normal">
			<option value="">Select Task</option>
			<cfoutput query="gettasks">
				<option <cfif gettasks.taskid is tid>selected</cfif> value="#gettasks.taskid#"><cfif gettasks.title is "">Miscellenous<cfelse>#gettasks.title#</cfif></option>
			</cfoutput>
		</select>
		<input type="submit" value="Submit" class="normal" onClick="return checkform(this.form);">
		</td>
	</tr>
	</cfif>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<cfif tid is not -1>
	<cfoutput>
	<tr>
		<td colspan="2" align="center" bgcolor="##FFFFCC"><b>You have a total of #numberformat(getreport.sumhours,"0.00")# accumulated hours for this task. </b></td>
	</tr>
	</cfoutput>
	</cfif>
</table>
</form>
<cfinclude template="/tasktracker/tasks/footer.cfm">
</div>

