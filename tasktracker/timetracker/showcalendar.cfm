
<cfparam name="currentmonth" default="#dateformat(now(),'mm')#">
<cfparam name="currentyear" default="#dateformat(now(),'yyyy')#">

<cfif isDefined("url.m")>
	<cfset currentmonth = url.m>
</cfif>
<cfif isDefined("url.y")>
	<cfset currentyear = url.y>
</cfif>

<cfset days = daysinmonth("#currentmonth#/1/#currentyear#")>

<cfset prevmonth = currentmonth - 1>
<cfset prevyear = currentyear>
	<cfif prevmonth IS 0>
		<cfset prevmonth = 12>
		<cfset prevyear = currentyear - 1>
	</cfif>
<cfset nextmonth = currentmonth + 1>
<cfset nextyear = currentyear>
	<cfif nextmonth IS 13>
		<cfset nextmonth = 1>
		<cfset nextyear = currentyear + 1>
	</cfif>

<cfset count = 1>
<table width="350" border="0" cellpadding="2" cellspacing="0">
<tr>
	<td width="100%">
	<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td width="33%" valign="bottom" class="smallnormal" align="center"><cfoutput><a href="showcalendar.cfm?m=#variables.prevmonth#&amp;y=#variables.prevyear#&p=#p#"><b>#monthasstring(variables.prevmonth)#</b></a></cfoutput></td>
		<td width="33%" valign="bottom" class="smallnormal" align="center"><cfoutput><b>#monthasstring(currentmonth)# #currentyear#</b></cfoutput></td>
		<td width="33%" valign="bottom" class="smallnormal" align="center"><cfoutput><a href="showcalendar.cfm?m=#variables.nextmonth#&amp;y=#variables.nextyear#&p=#p#"><b>#monthasstring(variables.nextmonth)#</b></a></cfoutput></td>
	</tr>
	<tr>
		<td class="normal" colspan="3"><hr width="90%" size="1"></td>
	</tr>
	</table>
	<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td width="40" align="center">Sun</td>
		<td width="40" align="center">Mon</td>
		<td width="40" align="center">Tue</td>
		<td width="40" align="center">Wed</td>
		<td width="40" align="center">Thur</td>
		<td width="40" align="center">Fri</td>
		<td width="40" align="center">Sat</td>
	</tr>
	</table>	
	<table width="100%" border="1" cellpadding="2" cellspacing="0">
	<cfloop index="x" from="1" to="6">
	<cfif (X IS 6)>
		<cfif NOT isdate("#currentmonth#/#count#/#currentyear#")>
		<cfbreak>
		</cfif>
	</cfif>
	<tr>
		<cfloop index="y" from="1" to="7">
		<cfset currentDate = "#currentmonth#/#count#/#currentyear#">
		<cfif isdate(#variables.currentDate#)>
			<cfif dayofweek(#variables.currentDate#) IS y>
			<!--- 
			<cfquery name="getevents" datasource="#ds#">
				select itemid 
				from timetracker_items 
				where adminuserid = #cookie.adminlogin# and timeday = #createodbcdate(variables.currentdate)#
			</cfquery>
			<cfif getevents.recordcount GT 0>
				<td class="calendarsmall" align="center" valign="middle" bgcolor="FFFFCC" width="40" height="40">
			<cfelse>
				<td class="calendarsmall" align="center" valign="middle" width="40" height="40">
			</cfif>
			 --->
				<td class="calendarsmall" align="center" valign="middle" width="40" height="40">
				<cfoutput>
				<a style="text-decoration: none;" href="javascript:opener.location='#p#?day=#variables.currentDate#'; self.close();">
				<cfif isDefined("url.showdate")>
					<cfif variables.currentdate IS url.showdate>
						<b style="color:ff0000;">#count#</b>
					<cfelse>
						#count#
					</cfif>
				<cfelse>
					<cfif variables.currentdate IS dateformat(now(),'mm/dd/yyyy')>
						<b style="color:ff0000;">#count#</b>
					<cfelse>
						#count#
					</cfif>
				</cfif>
				</a>
				</cfoutput>
			</td>
			<cfset count = count + 1>
			<cfelse>
			<td class="normal" align="center" valign="middle" width="40" height="40">&nbsp;</td>
			</cfif>
		<cfelse>
			<td class="normal" width="40" height="40" align="right">&nbsp;</td>
		</cfif>
		</cfloop>
	</tr>
	</cfloop>
	</table>
	</td>
</tr>
<cfoutput>
<tr>
	<td align="center">
	<br>
	<a href="showcalendar.cfm?m=#dateformat(now(),'m')#&amp;y=#dateformat(now(),'yyyy')#&p=#p#">Jump to current month</a>
	</td>
</tr>
</cfoutput>
</table>
