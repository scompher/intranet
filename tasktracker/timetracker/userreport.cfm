
<cfparam name="d" default="1,4">
<cfparam name="form.adminuserid" default="0">
<cfparam name="beginning" default="">
<cfparam name="ending" default="">

<cfquery name="getusers" datasource="#ds#">
	select distinct admin_users.adminuserid, admin_users.firstname, admin_users.lastname
	from admin_users
	inner join Admin_Users_Departments_Lookup on admin_users.adminuserid = Admin_Users_Departments_Lookup.adminuserid
	where Admin_Users_Departments_Lookup.departmentid in (1,4)
	order by admin_users.lastname asc, admin_users.firstname asc
</cfquery>

<cfquery name="getuser" datasource="#ds#">
	select * from admin_users
	where adminuserid = #adminuserid#
</cfquery>

<cfquery name="gettime" datasource="#ds#">
	select timetracker_items.adminuserid, timetracker_items.taskid, timetracker_items.hours, timetracker_items.description, timetracker_items.timeday, tasktracker_tasks.title
	from timetracker_items
	left join tasktracker_tasks on timetracker_items.taskid = tasktracker_tasks.taskid
	where timetracker_items.adminuserid = #adminuserid# 
	<cfif trim(beginning) IS NOT "">and timetracker_items.timeday >= #createodbcdate(beginning)# </cfif>
	<cfif trim(ending) IS NOT "">and timetracker_items.timeday <= #createodbcdate(ending)# </cfif>
	order by timetracker_items.timeday asc
</cfquery>

<script language="javascript" src="cal2.js">
/*
Xin's Popup calendar script-  Xin Yang (http://www.yxscripts.com/)
Script featured on/available at http://www.dynamicdrive.com/
This notice must stay intact for use
*/
</script>
<script language="javascript" src="cal_conf2.js"></script>
<div align="center" class="normal">
<form method="post" action="userreport.cfm">
<table width="725" border="1" cellpadding="5" cellspacing="0" class="normal">
	<tr>
		<td class="header"><b>Time Tracker User Report</b> </td>
	</tr>
	<tr>
		<td><table border="0" cellspacing="0" cellpadding="3">
			<tr>
				<td><b>Select User: </b></td>
				<td>
				<select name="adminuserid" class="normal">
					<option value="0"></option>
				<cfoutput query="getusers">
					<option value="#getusers.adminuserid#" <cfif form.adminuserid is getusers.adminuserid>selected</cfif> >#getusers.firstname# #getusers.lastname#</option>
				</cfoutput>
				</select>				</td>
				<td><input name="btnLookUp" type="submit" class="normal" value="Look Up"></td>
			</tr>
		</table></td>
	</tr>
	<tr>
		<td>
		<cfoutput>
		<table border="0" cellspacing="0" cellpadding="3">
			<tr>
				<td nowrap><b>Date Range:</b> </td>
				<td width="5" nowrap>&nbsp;</td>
				<td nowrap>Starting:</td>
				<td nowrap><input name="beginning" type="text" size="10" maxlength="10" class="normal" value="#beginning#">
					&nbsp;<a style="text-decoration:none;" href="javascript:showCal('BeginDate');"><img src="../../images/calicon.gif" width="20" height="20" align="absmiddle" border="0"></a></td>
				<td width="5" nowrap>&nbsp;</td>
				<td nowrap>Ending:</td>
				<td nowrap><input name="ending" type="text" size="10" maxlength="10" class="normal" value="#ending#">
					&nbsp;<a style="text-decoration:none;" href="javascript:showCal('EndDate');"><img src="../../images/calicon.gif" width="20" height="20" align="absmiddle" border="0"></a></td>
				<td nowrap><input name="btnApplyFilter" type="submit" class="normal" value="Apply Filter"></td>
			</tr>
		</table>
		</cfoutput>
		</td>
	</tr>
	<cfif getuser.recordcount gt 0>
	<cfoutput>
	<tr>
		<td bgcolor="FFFFCC">
		<b>
		Time report for #getuser.firstname# #getuser.lastname# 
		<cfif trim(beginning) IS NOT "">from #beginning#</cfif> 
		<cfif trim(ending) IS NOT "">to #ending#</cfif>
		</b>
		</td>
	</tr>
	</cfoutput>
	</cfif>
	<cfoutput query="gettime" group="timeday">
	<tr bgcolor="DDDDDD">
		<td><b>#dateformat(gettime.timeday,'dddd')# #dateformat(gettime.timeday,'mm/dd/yyyy')#</b></td>
	</tr>
	<tr>
		<td><table width="100%" border="1" cellpadding="3" cellspacing="0" class="normal">
			<tr>
				<td width="8%" align="center" nowrap bgcolor="EEEEEE"><b>Hours</b></td>
				<td width="24%" nowrap bgcolor="EEEEEE"><b>Task</b></td>
				<td width="68%" nowrap bgcolor="EEEEEE"><b>Description</b></td>
			</tr>
			<cfset totalhours = 0.00>
			<cfoutput>
			<cfset totalhours = totalhours + gettime.hours>
			<tr>
				<td valign="top" align="center">#numberformat(gettime.hours,"0.00")#</td>
				<td valign="top"><cfif gettime.taskid IS 0>Miscellaneous<cfelse>#gettime.title#</cfif></td>
				<td valign="top">#replace(gettime.description, "#chr(13)#", "<br>", "all")#</td>
			</tr>
			</cfoutput>
			<tr>
				<td align="center"><b>#numberformat(totalhours,"0.00")#</b></td>
				<td colspan="2"><b>Total Hours </b></td>
				</tr>
		</table></td>
	</tr>
	</cfoutput>
</table>
</form>
<cfinclude template="/tasktracker/tasks/footer.cfm">
</div>


