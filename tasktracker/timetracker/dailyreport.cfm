
<cfparam name="day" default="#dateformat(now(),'mm/dd/yyyy')#">

<!--- get users & taks --->
<cfquery name="getitems" datasource="#ds#">
	select admin_users.firstname, admin_users.lastname, admin_users.adminuserid, tasktracker_tasks.title, timetracker_items.hours, timetracker_items.description, timetracker_items.taskid
	from admin_users
	left join timetracker_items on admin_users.adminuserid = timetracker_items.adminuserid
	left join tasktracker_tasks on timetracker_items.taskid = tasktracker_tasks.taskid
	where timeday = #createodbcdate(day)#
	order by admin_users.lastname asc, admin_users.firstname asc
</cfquery>

<script language="JavaScript" type="text/javascript">
	function showMonth(urlString) {
		var calendarWin = window.open("showcalendar.cfm?p=dailyreport.cfm&" + urlString, "calendarWin", "width=375,height=350");
	}

	function checkform(frm) {
		if (frm.taskid.selectedIndex == 0) {alert('The task is required, or select Miscellaneous.'); frm.taskid.focus(); return false;}
		if (frm.description.value == "") {alert('The description is required.'); frm.description.focus(); return false;}
		return true;
	}
</script>

<div align="center" class="normal">
<table width="725" border="1" cellpadding="5" cellspacing="0" class="normal">
	<tr>
		<td class="header"><b>Time Tracker Daily Report </b></td>
	</tr>
	<tr>
		<td bgcolor="FFFFCC">
		<cfoutput>
		<table width="100%" border="0" cellspacing="0" cellpadding="3">
			<tr>
				<!--- <td width="19%" nowrap bgcolor="FFFFCC"><a href="javascript:openfilter('#query_string#&p=dailyreport.cfm');"><b>Click to Filter By Status</b></a></td> --->
				<td align="center" bgcolor="FFFFCC"><b>#dateformat(day,'dddd')#, #dateformat(day, 'long')#</b></td>
				<td width="36%" align="right" nowrap bgcolor="FFFFCC">
				<cfoutput>
				<a href="dailyreport.cfm?day=#dateformat(now(),'mm/dd/yyyy')#"><b>Today</b></a> | 
				<a href="dailyreport.cfm?day=#dateformat(dateadd("d",-1,day),'mm/dd/yyyy')#"><b>Previous Day</b></a> | 
				<a href="dailyreport.cfm?day=#dateformat(dateadd("d",1,day),'mm/dd/yyyy')#"><b>Next Day</b></a> | 
				<a href="javascript:showMonth('m=#dateformat(day,'m')#&y=#dateformat(day,'yyyy')#');"><b>Month View</b></a>
				</cfoutput>
				</td>
			</tr>
		</table>
		</cfoutput>
		</td>
	</tr>
	<cfoutput query="getitems" group="adminuserid">
	<tr>
		<td bgcolor="DDDDDD"><b>#getitems.firstname# #getitems.lastname#</b></td>
	</tr>
	<tr>
		<td>
		<table width="100%" border="1" cellpadding="3" cellspacing="0" class="normal">
			<tr>
				<td width="8%" align="center"><b>Hours</b></td>
				<td width="31%"><b>Task</b></td>
				<td width="61%"><b>Description</b></td>
			</tr>
			<cfset totalhours = 0.00>
			<cfoutput>
			<cfset totalhours = totalhours + getitems.hours>
			<tr>
				<td valign="top" align="center">#numberformat(getitems.hours,"0.00")#</td>
				<td valign="top"><cfif getitems.taskid IS 0>Miscellaneous<cfelse>#getitems.title#</cfif></td>
				<td valign="top">#getitems.description#</td>
			</tr>
			</cfoutput>
			<tr>
				<td align="center"><b>#numberformat(variables.totalhours,"0.00")#</b></td>
				<td colspan="2"><b>Total hours</b></td>
			</tr>
		</table>
		</td>
	</tr>
	</cfoutput>
</table>
<p>
<cfinclude template="/tasktracker/tasks/footer.cfm">
</div>
