<!--- Managers Log Search Form page --->
<link rel="stylesheet" type="text/css" href="/styles.css">

<cfparam name = "form.searchRec" default = "">
<cfparam name = "form.searchLoc" default = "">
<cfparam name = "form.searchKeyword" default="">
<cfparam name = "form.searchSubject" default="">
<cfparam name = "form.searchName" default="">
<cfparam name = "form.searchOrder" default="DESC">
<cfparam name = "form.searchStartDate" default="#dateformat(now(),'mm/dd/yyyy')#">
<cfparam name = "form.searchEndDate" default="#dateformat(now(),'mm/dd/yyyy')#">

<div align="center" class="normal" style="position:relative;z-index:0">
	<cfform method="post" action="index.cfm">
    <table border="0" cellspacing="0" cellpadding="5">
        <tr>
            <td class="highlightbar"><b>Search By: </b></td>
        </tr>
        <tr>
            <td class="greyrowbottomnopadding">
			<table border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td colspan="2">
					<b>Enter Account Number, Keyword, Date Range or any combination.<br />
					The default is current day's log. </b>					</td>
				</tr>
                <tr>
                    <td nowrap="nowrap"><b>Account Number: </b></td>
					<td>
					<cfinput type="text" name="searchRec" id="rec" value="#form.searchRec#" style="width:50px">
                     -
                   	<cfinput type="text" name="searchLoc" id="loc" value="#form.searchLoc#" style="width:50px">
					</td>
                </tr>
				<!---
                <tr>
                    <td nowrap="nowrap"><b>Keyword(s):</b></td>
                    <td><cfinput type="text" name="searchKeyword" id="keyword" value="#form.searchKeyword#" style="width:400px"></td>
                </tr>
				--->
                <tr>
                	<td><b>Subject:</b></td>
                	<td>
                		<cfinput type="text" name="searchSubject" id="subject" value="#form.searchSubject#" style="width:400px">
                	</td>
                </tr>
                <tr>
                	<td>&nbsp;</td>
                	<td class="smallred">Enter entire or part of subject to search</td>
                </tr>
				<tr>
				<td><b>Manager:</b></td>
                	<td>
                		<cfinput type="text" name="searchName" id="name" value="#form.searchName#" style="width:400px">
                	</td>
				</tr>
				<tr>
                	<td>&nbsp;</td>
                	<td class="smallred">Enter entire name or part of the name to search</td>
                </tr>
				<tr>
                    <td colspan="2"><b>Date Range: </b>&nbsp;Please enter in the format mm/dd/yyyy or use the calendar</td>
                </tr>
                <tr>
                    <td><b>Begin Date: </b></td>
					<td style="z-index:3; position:relative;">
					<cfinput type="datefield" name="searchStartDate" value="#form.searchStartDate#" validate="date" message="Please enter a valid beginning date" style="width:75px;">
					</td>
                </tr>
                <tr>
                    <td><b>End Date: </b></td>
					<td style="z-index:2; position:relative;">
					<cfinput type="datefield" name="searchEndDate" value="#form.searchEndDate#" validate="date" message="Please enter a valid ending date" style="width:75px;">
					</td>
                </tr>
                <tr>
                	<td><b>List Results:</b> </td>
                	<td>
                		<select name="searchOrder">
                			<option <cfif form.searchOrder is "ASC">selected</cfif> value="ASC">Oldest First</option>
                			<option <cfif form.searchOrder is "DESC">selected</cfif> value="DESC">Newest First</option>
               			</select>
               		</td>
                </tr>
                <tr>
					<td colspan="2">
						<cfinput type="submit" name="btnSubmit" id="Search Now" value="Search Now"> &nbsp;
						<cfinput type="submit" name="btnReset" id="Reset Search" value="Reset Search">&nbsp;
					</td>
                </tr>
            </table>
			</td>
        </tr>
		<tr>
			<td class="nopadding">
			<br />
			<cfinput type="image" src="images/btnNewEntryPink.gif" name="btnNewEntry" id="Create New Entry" value="Create New Entry">
			</td>
		</tr>
    </table>
	</cfform>
</div>
