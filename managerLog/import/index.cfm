
<cfsetting showdebugoutput="no" requesttimeout="600">

<cfquery name="emptyTable" datasource="#ds#">
	truncate table managerLog_logEntries
</cfquery>

<cfquery name="getRecords" datasource="#ds#">
	select 
	LogDate + ' ' + CONVERT(nvarchar(8),time,108) as dateTimeEntered, 
	LEFT(log,50) + '...' as otherSubject, 
	Receiver, Account, oper, 
	LOG as note 
	from dbo.temp_MgrLog
	where LogDate is not null 
</cfquery>

<cfoutput query="getRecords">
	<cfif listlen(oper) gt 1>
		<cfset firstname = trim("#listgetat(oper,2)#")>
		<cfset lastname = trim("#listgetat(oper,1)#")>
		<cfset firstname = listgetat(firstname,1," ")>
		<cfquery name="getUser" datasource="#ds#">
			select * from admin_users 
			where firstname = '#firstname#' and lastname = '#lastname#' 
		</cfquery>
		<cfif getUser.recordcount gt 0>
			<cfset adminuserid = getUser.adminuserid>
		<cfelse>
			<cfset adminuserid = 0>
		</cfif>
	</cfif>
	<cfset accountNumber = receiver & "-" & account>
	<cfif trim(accountNumber) is "-">
		<cfset accountNumber = "">
	</cfif>
	<cfquery name="insertLogItem" datasource="#ds#">
		insert into managerLog_logEntries (subject, otherSubject, note, accountNumber, dateTimeEntered, adminUserID, receiver, location, firstname, lastname)
		values ('', '#getRecords.otherSubject#', '#getRecords.note#', '#variables.accountNumber#', #createodbcdatetime(getRecords.dateTimeEntered)#, #adminUserID#, '#getRecords.receiver#', '#getRecords.account#', '#firstname#', '#lastname#') 
	</cfquery>
</cfoutput>

complete


