<!--- Managers Log Edit Email page --->
<cfquery name="GetEmail" datasource="#ds#" >
SELECT * 
	FROM managerLog_emailNotifications 
	WHERE notificationID = #url.notificationID#
</cfquery>

<link rel="stylesheet" type="text/css" href="/styles.css">
<!---<cfparam name = "form.name" default = "">
<cfparam name = "form.emailAddress" default = "">--->

<div align="center" class="normal">

<cfform id="form1" name="form1" method="post" action="editEmail_save.cfm">
<table border="0" cellspacing="0" cellpadding="5">
    <tr>
        <td colspan="2" class="highlightbar"><b>Email Maintenance Change Entry </b></td>
    </tr>
	<tr>
	

	<cfinput type = "hidden" name="notificationID" value="#url.notificationID#" />
	<td class="greyrowbottomnopadding">
	<table border="0" cellpadding="5" cellspacing="0">
	<cfoutput query="GetEmail">
		<tr>
			<td width="97" nowrap="nowrap"><b>Name:</b></td>
			<td width="275"><cfinput name="name" type="text" value="#GetEmail.name#" required="Yes" message="Name is required." style="width:300px" maxlength="255"/></td>
		</tr>
		<tr>
			<td nowrap="nowrap"><b>Email Address:</b></td>
			<td width="275"><cfinput name="emailAddress" type="text" value="#GetEmail.emailAddress#" required="Yes" validate="email" message="You must enter a valid e-mail address." style="width:300px" maxlength="255"/></td>
		</tr>
		<tr>
		<td colspan="2">
			<cfinput type="submit" name="btnSave" id="Save" value="Save Change"> &nbsp;
			<cfinput type="button" onClick="document.location='index.cfm';" name="btnCancel" value="Cancel">
		</td> 
		</tr> 
		</cfoutput>
	 </table>
	 </td>
    </tr>
</table>

</cfform>
 <p><br />
        <a href="javascript:history.go(-1);">Return</a> </p>
</div>
