<!--- Managers Log New Email page --->
<link rel="stylesheet" type="text/css" href="/styles.css">
<cfparam name = "form.name" default = "">
<cfparam name = "form.emailAddress" default = "">

<div align="center" class="normal">

<cfif isDefined("form.btnSave")>
	<cfinclude template="newEmail_save.cfm">
</cfif>

<cfform id="form1" name="form1" method="post" action="newEmail.cfm">
<table border="0" cellspacing="0" cellpadding="5">
    <tr>
        <td colspan="2" class="highlightbar"><b>Email Maintenance Add New Entry </b></td>
    </tr>
	<tr>
	
	<td class="greyrowbottomnopadding">
	<table border="0" cellpadding="5" cellspacing="0">
		<tr>
			<td nowrap="nowrap"><b>Name:</b></td>
			<td><cfinput type="text" name="name" value="#form.name#" class="normal" id="name" required="Yes" style="width:300px" maxlength="255" message="Name is required."></td>
		</tr>
		<tr>
			<td nowrap="nowrap"><b>Email Address:</b></td>
			<td><cfinput type="text" name="emailAddress" validate="email" value="#form.emailAddress#" required="Yes" class="normal" id="email" style="width:300px" maxlength="255" message="You must enter a valid e-mail address."></td>
		</tr>
		<tr>
		<td colspan="2">
			<cfinput type="submit" name="btnSave" id="Save" value="Save"> &nbsp;
			<cfinput type="button" onClick="document.location='index.cfm';" name="btnCancel" value="Cancel">
		</td> 
		</tr> 
	 </table>
	 </td>
    </tr>

</table>

</cfform>
 <p><br />
 <a href="index.cfm" style="text-decoration:underline">Return to Previous Page</a> </p>
</div>