<!--- Managers Log Validation of Search page --->

<cfparam name = "form.searchRec" default = "">
<cfparam name = "form.searchLoc" default = "">
<cfparam name = "form.searchKeyword" default="">
<cfparam name = "form.searchStartDate" default="">
<cfparam name = "form.searchEndDate" default="">

<cfset err="">
<cfset comparision ="">

<cfif form.searchRec is "" and form.searchLoc is not "">
	<cfset err = listappend(err,"Receiver is required.", "|")>
</cfif>

<cfif form.searchRec is not "" and form.searchLoc is "">
	<cfset err = listappend(err,"Location is required.", "|")>
</cfif>


<!---  Check if Begin and End Date are filled in --->
<cfif form.searchStartDate is "">
	 <cfif form.searchEndDate is not "">
	  	<cfset err = listappend(err,"Begin Date must be selected.", "|")>
	 </cfif>
</cfif>
	 
<cfif form.searchEndDate is "">
	 <cfif form.searchStartDate is not "">
	  	<cfset err = listappend(err,"End Date must be selected.", "|")>
	 </cfif>
</cfif>

<cfif form.searchStartDate is not "" and form.searchEndDate is not "">
	<cfset comparison = DateCompare(form.searchStartDate, form.searchEndDate)>
	<cfswitch expression = #comparison#>
				<cfcase value = "1">
					<cfset err = listappend(err,"Begin Date must be &lt; End Date.", "|")>
				</cfcase>
	</cfswitch>
</cfif>

<!--- Display error messages --->

<cfloop list = "#err#" index = "message" delimiters = "|"> 
	<cfoutput>
	<br />
	<br />
		<p ><b><li class="alert"> #message# <br> </li></b></p>
	</cfoutput>
</cfloop>


