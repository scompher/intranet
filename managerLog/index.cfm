<!--- Managers Log Index Page --->
<link rel="stylesheet" type="text/css" href="/styles.css">

<br />

<div align="center" class="normal" style="position:relative;z-index:0">

<cfparam name="err" default="">

<!--- pagination variables --->
<cfset maxrows = 50>
<cfparam name="p" default="1">
<cfparam name="pages" default="1">
<!--- pagination variables --->

<!--- cal cstart page --->
<cfset start = (maxrows * p) - (maxrows - 1)>
<!--- cal cstart page --->

<!--- convert URL variables to form variables --->
<cfif isDefined("url.searchRec")>
	<cfset form.searchRec = url.searchRec>
</cfif>
<cfif isDefined("url.searchLoc")>
	<cfset form.searchLoc = url.searchLoc>
</cfif>
<cfif isDefined("url.searchKeyword")>
	<cfset form.searchKeyword = url.searchKeyword>
</cfif>
<cfif isDefined("url.searchSubject")>
	<cfset form.searchSubject = url.searchSubject>
</cfif>
<cfif isDefined("url.searchName")>
	<cfset form.searchName = url.searchName>
</cfif>
<cfif isDefined("url.searchStartDate")>
	<cfset form.searchStartDate = url.searchStartDate>
</cfif>
<cfif isDefined("url.searchEndDate")>
	<cfset form.searchEndDate = url.searchEndDate>
</cfif>

<!--- if Submit button is entered, display data --->
<cfif isDefined("form.btnSubmit")>
	<cfinclude template = "validateSearch.cfm">
</cfif>

<!---if Reset button is entered, erase data --->
<cfif isDefined("form.btnReset")>
	<cfset form.searchStartDate = "">
	<cfset form.searchEndDate = "">
	<cfset form.searchRec = "">
	<cfset form.searchLoc = "">
	<cfset form.searchKeyword = "">
	<cfset form.searchSubject = "">
	<cfset form.searchName = "">
	<cfset form.searchOrder = "DESC">
</cfif>

<cfif isDefined("form.btnNewEntry.x")>
	<cfinclude template = "newEntry.cfm">
	<cfabort>
</cfif>

<cfinclude template = "searchForm.cfm">

<cfif trim(err) is "" and not isDefined("form.btnReset")>
	
	<cfif trim(form.searchKeyword) is not "">
		<cfsearch name = "getResults" collection = "intranet_managerLog" criteria = "#form.searchKeyword#">
		<cfif getResults.recordcount is 0>
			<cfset idlist = 0>
		<cfelse>
			<cfset idlist = valuelist(getResults.key)>
		</cfif>
	</cfif>
	
	<cfquery name="GetMgrlog" datasource="#ds#" >
		SELECT managerLog_logEntries.* 
		FROM managerLog_logEntries 
		where
		<cfif trim(form.searchRec) is not "">
			receiver = '#form.searchRec#' and 
		</cfif>
		<cfif trim(form.searchLoc) is not "">
			location = '#form.searchLoc#' and 
		</cfif>
		<cfif trim(form.searchStartDate) is not "">
			dateTimeEntered >= #createodbcdate(form.searchStartDate)# and 
		</cfif>
		<cfif trim(form.searchEndDate) is not "">
			<cfset form.searchEndDate = dateadd("d",form.searchEndDate,1)>
			dateTimeEntered < #createodbcdate(form.searchEndDate)# and 
		</cfif>
		<cfif trim(form.searchKeyword) is not "">
			logid IN (#idlist#) and 
		</cfif>
		<cfif trim(form.searchSubject) is not "">
			(subject LIKE '%#trim(form.searchSubject)#%' or otherSubject LIKE '%#trim(form.searchSubject)#%') and 
		</cfif>
		<cfif trim(form.searchName) is not "">
			(firstname + lastname like '%#trim(form.searchName)#%' or firstname + ' ' + lastname like '%#trim(form.searchName)#%') and 
		</cfif>
		1=1 
		Order by dateTimeEntered #form.searchOrder# 
	</cfquery>

	<!--- calculate number of pages --->
	<cfif isnumeric(GetMgrlog.recordcount)>
		<cfset totalItems = GetMgrlog.recordcount>
	<cfelse>
		<cfset totalItems = 0>
	</cfif>
	<cfset pages = ceiling(totalItems / maxrows)>
	
	<p></p>
	<cfif (#GetMgrlog.recordcount#) gt 1>
	<h4>
	<cfoutput>
	#numberformat(GetMgrlog.recordcount)# Records Found.	</cfoutput>	</h4>
	<cfelse>
	<h4>
	<cfoutput>
	#GetMgrlog.recordcount# Record Found.	</cfoutput>	</h4>
	</cfif>

	<p></p>

	<cfif getMgrLog.recordcount gt 0>
		<cfset end = start + maxrows - 1>
		<cfif end gt GetMgrlog.recordcount>
			<cfset end = GetMgrlog.recordcount>
		</cfif>
		<cfoutput>
		Displaying items #start# - #end#
		</cfoutput>
		<p></p>
	</cfif>

	<!--- convert search parameters into url variables --->
	<cfif trim(form.searchStartDate) is not "">
		<cfset searchString = "searchRec=#form.searchRec#&searchLoc=#form.searchLoc#&searchKeyword=#form.searchKeyword#&searchSubject=#form.searchSubject#&searchName=#form.searchName#&searchStartDate=#trim(dateformat(form.searchStartDate,'mm/dd/yyyy'))#&searchEndDate=#trim(dateformat(dateadd("d",form.searchEndDate,-1),'mm/dd/yyyy'))#">
	<cfelse>
		<cfset searchString = "searchRec=#form.searchRec#&searchLoc=#form.searchLoc#&searchKeyword=#form.searchKeyword#&searchSubject=#form.searchSubject#&searchName=#form.searchName#&searchStartDate=&searchEndDate=">
	</cfif>
	
	<table width="725" border="0" cellpadding="5" cellspacing="0">
		<tr>
			<td class="highlightbar"><b>Managers Log</b></td>
		</tr>
		<tr>
			<td class="greyrowbottomnopadding">
				<table width="100%" border="0" cellpadding="5" cellspacing="0">
					<tr>
						<td width="4%" class="linedrowrightcolumn">&nbsp;</td>
						<td width="4%" class="linedrowrightcolumn">&nbsp;</td>
						<td width="21%" nowrap="nowrap" class="linedrowrightcolumn"><b>Date/Time Created </b></td>
						<td width="17%" nowrap="nowrap" class="linedrowrightcolumn"><b>Account Number </b></td>
						<td width="37%" nowrap="nowrap" class="linedrowrightcolumn"><b>Subject</b></td>
						<td nowrap="NOWRAP" class="linedrow"><b>Manager</b></td>
					</tr>
					<cfoutput query="GetMgrlog" startrow="#start#" maxrows="#maxrows#">
					<tr>
						<td class="linedrowrightcolumn">#getMgrLog.currentRow#</td>
						<td class="linedrowrightcolumn">
						<a href="viewEntry.cfm?logid=#GetMgrlog.logid#&p=#p#&#searchString#"><img border="0" src="../images/manifyingglass.gif" width="15" height="15" /></a></td>
						<td nowrap="nowrap" class="linedrowrightcolumn">#dateformat(GetMgrlog.dateTimeEntered,'mm/dd/yyyy')# #timeformat(GetMgrlog.dateTimeEntered,'hh:mm tt')#</td>
							<td nowrap="nowrap" class="linedrowrightcolumn"><cfif GetMgrlog.receiver is not "" and GetMgrlog.location is not "">#GetMgrlog.receiver#-#GetMgrlog.location#<cfelse>#GetMgrlog.receiver##GetMgrlog.location#</cfif>&nbsp;</td>
							<td class="linedrowrightcolumn">
						<cfif #GetMgrlog.subject# is "" or #GetMgrlog.subject# is "Other">
							#GetMgrlog.othersubject#
						<cfelse>
							#GetMgrlog.subject#
						</cfif>	
						</td>
						<td nowrap="nowrap" class="linedrow">#GetMgrlog.firstname# #GetMgrlog.lastname#&nbsp;</td>
					</tr>
				</cfoutput>
				</table>
			</td>
		</tr>
		<tr>
			<td class="nopadding">
			<cfif pages gt 1>
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<cfoutput>
				<tr>
					<td width="20%" align="center">
					<cfif p gt 1>
					<a style="text-decoration:underline" href="index.cfm?p=#evaluate(p - 1)#&#searchString#">[Previous Page]</a>
					</cfif>&nbsp;
					</td>
					<td width="60%">&nbsp;</td>
					<td width="20%" align="center">
					<cfif p is not pages>
					<a style="text-decoration:underline" href="index.cfm?p=#evaluate(p + 1)#&#searchString#">[Next Page]</a>
					</cfif>&nbsp;
					</td>
				</tr>
				</cfoutput> 
			</table>
			</cfif>			
			</td>
		</tr>
	</table>
</cfif>

<br>
<a style="text-decoration:underline;" href="/index.cfm">Return to Intranet</a>
</div>

