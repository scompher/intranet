<!--- Managers Log Validate Entry page  - Validation of entries on screen --->

<cfparam name = "form.rec" default = "">
<cfparam name = "form.loc" default = "">
<cfparam name = "form.subject" default = "">
<cfparam name = "form.otherSubject" default = "">
<cfparam name = "form.note" default = "">

<cfset err="">

<cfset dateTime = createodbcdatetime(now())>

<cfif form.rec is "" and form.loc is not "">
	<cfset err = listappend(err,"Receiver is required.", "|")>
</cfif>

<cfif form.rec is not "" and form.loc is "">
	<cfset err = listappend(err,"Location is required.", "|")>
</cfif>

<!---  Check if subject/otherSubject filled in --->
<cfif trim(form.subject) is "">
	 <cfif trim(form.otherSubject) is "">
	  	<cfset err = listappend(err,"Either Subject or Other Subject is required.", "|")>
	 </cfif>
</cfif>

<cfif trim(form.subject) is "" and trim(form.otherSubject) is not "">
	<cfset form.subject = "Other">
</cfif>

<cfif trim(form.subject) is "Other" and trim(form.otherSubject) is "">
 	<cfset err = listappend(err,"Please enter Other Subject.", "|")>
</cfif>

<cfif trim(form.subject) is not "Other" and trim(form.otherSubject) is not "">
	<cfset err = listappend(err,"Cannot enter Other Subject when Subject selected.", "|")>
</cfif>

<cfif trim(form.note) is "">
    <cfset err = listappend(err,"Note is required.", "|")>
</cfif>

<!--- Display error messages --->
<table border="0" cellspacing="0" cellpadding="5">
<cfloop list = "#err#" index = "message" delimiters = "|"> 
	<cfoutput>
	<tr>
		<td nowrap="nowrap" class="alert"><b>#message#</b></td>
	</tr>
	</cfoutput>
</cfloop>
</table>

