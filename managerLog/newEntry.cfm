<!--- Managers Log New Entry page --->
<link rel="stylesheet" type="text/css" href="/styles.css">

<cfparam name = "form.rec" default = "">
<cfparam name = "form.loc" default = "">
<cfparam name = "form.subject" default = "">
<cfparam name = "form.otherSubject" default = "">
<cfparam name = "form.note" default = "">

<div align="center" class="normal" >

<cfif isDefined("form.btnSubmit")>
	<cfinclude template="newEntry_save.cfm">
	<cfif err is "">
		<cflocation url="index.cfm">
	</cfif>
</cfif>

<cfform method="post" action="newEntry.cfm">
    <table border="0" cellspacing="0" cellpadding="5">
        <tr>
            <td class="highlightbar"><b>Managers Log - New Entry </b></td>
        </tr>
        <tr>
            <td class="greyrowbottomnopadding">
			<table border="0" cellspacing="0" cellpadding="5">
                <tr>
                    <td nowrap><b>Account Number:</b></td>
                    <td><cfinput type="text" name="rec" id="rec" value="#form.rec#" style="width:50px">
                        -
                   		<cfinput type="text" name="loc" id="loc" value="#form.loc#" style="width:50px"></td>
                </tr>
                <tr>
                    <td nowrap><b>Enter Subject:</b></td>
                    <td><cfselect enabled="No" name="subject" style="width:375px" multiple="no">
					<option <cfif form.subject is ""> selected </cfif> value=""></option>
					<option <cfif form.subject is "Actual Alarm Notes"> selected </cfif> value="Actual Alarm Notes">Actual Alarm Notes</option>
					<option <cfif form.subject is "Beginning of Shift"> selected </cfif> value="Beginning of Shift">Beginning of Shift</option>
					<option <cfif form.subject is "Delay in Dispatch"> selected </cfif> value="Delay in Dispatch">Delay in Dispatch</option>
                    <option <cfif form.subject is "Open Property"> selected </cfif> value="Open Property">Open Property</option>      		
					<option <cfif form.subject is "Power Outages"> selected </cfif> value="Power Outages">Power Outages</option>
					<option <cfif form.subject is "Storms in Area"> selected </cfif> value="Storms in Area">Storms in Area</option>
                    <option <cfif form.subject is "Other"> selected </cfif> value="Other">Other</option>
                    </cfselect></td>
                </tr>
				<tr>
                    <td nowrap><b>&quot;Other&quot; Subject: </b></td> 
                    <td><cfinput type="text" name="otherSubject" value="#form.otherSubject#" class="normal" id="otherSubject" style="width:375px" maxlength="255"></td>
                </tr>		
                <tr>
                    <td colspan="2"><b>Note:</b></td>
                    </tr>
                <tr>
                    <td colspan="2"><cftextarea name="note" value="#form.note#" rows="5" required="yes" id="note" style="width:500px" message="Note is required." enabled="no" visable="no"></cftextarea></td>
                    </tr>
                <tr>
                    <td colspan="2">
					<cfinput type="submit" name="btnSubmit" id="Save Info" value="Submit Form"> &nbsp;
                    <cfinput type="button" onClick="document.location='newEntry.cfm';" name="Clear" value="Clear Form"></td>
                    </tr>
            </table></td>
        </tr>
    </table>
	
</cfform>

<br>
<a href="index.cfm" style="text-decoration:underline">Return to Previous Page</a>

</div>
