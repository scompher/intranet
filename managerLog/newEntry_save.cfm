<!--- Managers Log New Entry Save page --->

<cfinclude template = "validateEntry.cfm">

<!--- if no error, update database --->
<cfif err is "" >
	<cfquery name = "InsertDb" datasource = "#ds#">
		INSERT INTO managerLog_logEntries(receiver,location,dateTimeEntered,adminUserID,subject,otherSubject,note,firstname,lastname)
		VALUES ('#form.rec#',
				'#form.loc#',
				#dateTime#,
				'#cookie.adminlogin#', 
				'#form.subject#',
				'#form.otherSubject#',
				'#form.note#',
				'#getsec.firstname#',
				'#getsec.lastname#')
	</cfquery>
	<cflocation url="index.cfm">
</cfif>


