<!--- Managers Log View Entry page --->
<cfif isDefined("form.btnSubmit")>
    <cfset url.logid = form.logid>
    
    <cfif form.entrytext is not "">
		<cfinclude template="appendEntry_save.cfm">
		<cflocation url="viewEntry.cfm?logid=#url.logid#&#searchString#">
    </cfif>
<cfelse>
	<!--- convert search parameters into url variables --->
	<cfif url.searchStartDate is not "">
		<cfset searchString = "p=#url.p#&searchRec=#url.searchRec#&searchLoc=#url.searchLoc#&searchKeyword=#url.searchKeyword#&searchSubject=#url.searchSubject#&searchName=#url.searchName#&searchStartDate=#trim(dateformat(url.searchStartDate,'mm/dd/yyyy'))#&searchEndDate=#trim(dateformat(url.searchEndDate,'mm/dd/yyyy'))#">
	<cfelse>
		<cfset searchString = "p=#url.p#&searchRec=#url.searchRec#&searchLoc=#url.searchLoc#&searchKeyword=#url.searchKeyword#&searchSubject=#url.searchSubject#&searchName=#url.searchName#&searchStartDate=&searchEndDate=">
	</cfif>
</cfif>

<cfquery name="GetLog" datasource="#ds#" >
SELECT managerLog_logEntries.*
	FROM managerLog_logEntries 
	WHERE logid = #url.logid#
</cfquery>

<cfquery name="GetAppendLog" datasource="#ds#">
	SELECT managerLog_appendedLogEntries.*, Admin_Users.firstname, Admin_Users.lastname
	FROM managerLog_appendedLogEntries
	LEFT JOIN Admin_Users on managerLog_appendedLogEntries.adminUserID = Admin_Users.adminuserid
	WHERE logid = #url.logid#	

</cfquery>

<link rel="stylesheet" type="text/css" href="/styles.css">
<div align="center" class="normal" >
	<table width="600" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td class="highlightbar"><b>Managers Log View </b></td>
		</tr>
		<tr>
			<td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellpadding="5" cellspacing="0">
					<cfoutput query="GetLog">
					<tr>
						<td width="138" nowrap="nowrap"><b>Date/Time Created:</b></td>
						<td width="440">#DateFormat(GetLog.dateTimeEntered,'mm/dd/yyyy')# #TimeFormat(GetLog.dateTimeEntered,'hh:mm tt')#</td>
					</tr>
					<tr>
						<td nowrap="nowrap"><b>Created By:</b></td>
						<td>#GetLog.firstname# #GetLog.lastname#</td>
					</tr>
					<tr>
						<td nowrap="nowrap"><b>Account Number: </b></td>
						<td>
						<cfif trim(GetLog.receiver) is not "" or trim(GetLog.location) is not "">
							#GetLog.receiver#-#GetLog.location#
						<cfelse>
							&nbsp;
						</cfif>
						</td>
					</tr>
					<tr>
						<td nowrap="nowrap"><b>Subject:</b></td>
						<td><cfif #GetLog.subject# is "" or #GetLog.subject# is "Other">
								#GetLog.othersubject#
								<cfelse>
								#GetLog.subject#
							</cfif>
						</td>
					</tr>
					<tr>
						<td nowrap="nowrap"><b>Note:</b></td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td colspan="2">#replace(GetLog.note, chr(13), "<br />", "all")#</td>
					</tr>
					</cfoutput>
					<cfif GetAppendLog.recordcount gt 0>
						<tr>
							<td colspan="2"><b>Additional Information:</b> </td>
						</tr>
						<cfoutput query="GetAppendLog">
						<tr>
							<td colspan="2">Created by: #GetAppendLog.firstname# #GetappendLog.lastname# #DateFormat(GetAppendLog.entryDateTime,'mm/dd/yyyy')# #TimeFormat(GetAppendLog.entryDateTime,'hh:mm tt')#</td>
						</tr>
						<tr>
							<td class="linedrow" colspan="2">#replace(GetAppendLog.entryText, chr(13), "<br />", "all")#</td>
						</tr>
						</cfoutput>
					</cfif>
			</table></td>
		</tr>
	</table>
    <p class="alert"><b>This will be added to the Note field above. You cannot change the original log details.</b></p>
    <cfform method="post" action="viewEntry.cfm">
        <cfinput type = "hidden" name="logid" value="#url.logid#" />
		<cfinput type = "hidden" name="searchString" value="#searchString#" />
        <table width="600" border="0" cellspacing="0" cellpadding="5">
            <tr>
                <td class="highlightbar"><p><b>Additional Information:</b></p></td>
            </tr>
            <tr>
                <td class="greyrowbottomnopadding"><table width="100%" border="0" cellspacing="0" cellpadding="5">
                        <tr>
                            <td colspan="2"><textarea cols="110" rows="5" name="entryText" id="text"></textarea></td>
                        </tr>
                        <tr>
                            <td colspan="2">
							<cfinput type="submit" name="btnSubmit" id="Save Info" value="Save Info">
							&nbsp;
							<cfinput type="reset"  name="Clear" value="Clear Info">
							</td>
                        </tr>
                    </table>
				</td>
            </tr>
        </table>
    </cfform>
	<br />
	<cfoutput>
    <a style="text-decoration:underline;" href="index.cfm?#searchString#">Return to previous page</a>
	</cfoutput>
</div>
