
<link rel="stylesheet" type="text/css" href="/styles.css">

<cfparam name="form.dealerNumber" default="">
<cfparam name="form.invoiceLookupID" default="">
<cfparam name="form.invoiceFormat" default="">

<cfif trim(form.dealerNumber) is not "">
	<cfquery name="getInvoices" datasource="copalink">
		select * from invoiceLookup 
		where dealerNumber = '#form.dealerNumber#' 
		order by invoicePeriod desc 
	</cfquery>
</cfif>

<cfif isDefined("form.btnDownloadInvoice")>
	<cfinclude template="downloadFile.cfm">
</cfif>

<div align="center">
<table border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Download An Invoice</b> </td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
		<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td><b>To download an invoice, enter the dealer number, invoice and format.</b></td>
			</tr>
			<form method="post" action="index.cfm">
			<tr>
				<td class="nopadding">
				<table border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td>Please Enter the Dealer Number: </td>
						<td>
						<input type="text" name="dealerNumber" style="width:50px; vertical-align:middle;" value="<cfoutput>#form.dealerNumber#</cfoutput>">
						<input name="btnGetDealer" type="submit" class="sidebarsmall" style="vertical-align:middle;" value="Select">
						</td>
					</tr>
					<cfif trim(form.dealerNumber) is not "">
						<cfif getInvoices.recordCount gt 0>
							<tr>
								<td align="left">Select the Invoice: </td>
								<td>
								<select name="invoiceLookupID">
									<cfoutput query="getInvoices">
										<cfset invoicemonth = monthAsString(listgetat(invoicePeriod,2,"."))>
										<cfset invoiceyear = listgetat(invoicePeriod,1,".")>
										<option value="#invoiceLookupID#">#invoiceMonth# #invoiceYear#, Invoice ## #invoiceNumber#</option>
									</cfoutput>
								</select>
								</td>
							</tr>
							<tr>
								<td align="left">Please choose which format: </td>
								<td>
								<input type="radio" name="invoiceFormat" value="PDF" /> PDF Document 
								<input type="radio" name="invoiceFormat" value="XLS" /> Excel Spreadsheet 
								</td>
							</tr>
							<tr>
								<td colspan="2">
								<input name="btnDownloadInvoice" type="submit" class="sidebarsmall" value="Download Invoice">
								</td>
							</tr>
						<cfelse>
							<tr>
								<td colspan="2" class="alert">There are no invoices available for this dealer.</td>
							</tr>
						</cfif>
					</cfif>
				</table>
				</td>
			</tr>
			</form>
		</table>
		</td>
	</tr>
</table>
<cfif isDefined("cookie.adminLogin")>
<br>
<a href="/index.cfm" style="text-decoration:underline;" class="normal">Return to Intranet</a>
</cfif>
</div>
