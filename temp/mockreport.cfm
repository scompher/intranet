

<br>
<cfdocument format="pdf">
<link rel="stylesheet" type="text/css" href="/styles.css">
<div align="center">
	<span class="normal" style="font-size:16px">
	<b>Equipment Log Report for 05/01/2010 - 07/22/2010</b></span><br>
    <br>
	<table width="700" border="0" cellpadding="5" cellspacing="0" class="lightbox" style="border-bottom:none;">
		<tr>
			<td align="center" class="linedrowrightcolumn"><b>Date/Time Reported </b></td>
			<td align="center" class="linedrowrightcolumn"><b>Last Updated </b></td>
			<td align="center" class="linedrowrightcolumn"><b>Equipment Name </b></td>
			<td align="center" class="linedrowrightcolumn"><b>Component Part </b></td>
			<td align="center" class="linedrow"><b>Current Status </b></td>
		</tr>
		<tr>
			<td align="center" class="linedrowrightcolumn">05/17/2010 5:22 PM </td>
			<td align="center" class="linedrowrightcolumn">06/10/2010 10:15 AM </td>
			<td class="linedrowrightcolumn">Station 5027 </td>
			<td class="linedrowrightcolumn">Monitor</td>
			<td class="linedrow">Picked Up</td>
		</tr>
		<tr>
			<td align="center" class="linedrowrightcolumn">06/01/2010 03:30 PM </td>
			<td align="center" class="linedrowrightcolumn">06/01/2010 04:30 PM </td>
			<td class="linedrowrightcolumn">Station 5004 </td>
			<td class="linedrowrightcolumn">Keyboard</td>
			<td class="linedrow">Completed</td>
		</tr>
		<tr>
			<td align="center" class="linedrowrightcolumn">06/15/2010 12:13 PM </td>
			<td align="center" class="linedrowrightcolumn">06/16/2010 09:30 AM </td>
			<td class="linedrowrightcolumn">Station 5041 </td>
			<td class="linedrowrightcolumn">Chair</td>
			<td class="linedrow">In Progress </td>
		</tr>
		<tr>
			<td align="center" class="linedrowrightcolumn">07/01/2010 01:00 AM </td>
			<td align="center" class="linedrowrightcolumn">&nbsp;</td>
			<td class="linedrowrightcolumn">Station 5037 </td>
			<td class="linedrowrightcolumn">Mouse</td>
			<td class="linedrow">Unread</td>
		</tr>
		<tr>
			<td align="center" class="linedrowrightcolumn">07/14/2010 08:00 AM </td>
			<td align="center" class="linedrowrightcolumn">07/14/2010 09:00 AM</td>
			<td class="linedrowrightcolumn">Station 5036 </td>
				<td class="linedrowrightcolumn">Headset Box </td>
				<td class="linedrow">Picked Up </td>
			</tr>
	</table>
</div>
</cfdocument>