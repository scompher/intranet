

<br>
<cfdocument format="pdf">
<link rel="stylesheet" type="text/css" href="/styles.css">
<div align="center">
	<span class="normal" style="font-size:16px">
	<b>Trouble Equipment Report</b>
	</span>
	<br>
	<br>	
	<span class="normal" style="font-size:14px">
	<b>Systems with 3 or tickets since 01/01/2010</b>	</span>
	<br>
    <br>
	<table width="700" border="0" cellpadding="5" cellspacing="0" class="lightbox" style="border-bottom:none;">
		<tr>
			<td align="center" class="linedrowrightcolumn"><b>Equipment Name </b></td>
			<td align="center" class="linedrowrightcolumn"><b># of Incidences </b></td>
			<td align="center" class="linedrowrightcolumn"><b>Last Reported </b></td>
		</tr>
		<tr>
			<td align="center" class="linedrowrightcolumn">Station 5024 </td>
			<td align="center" class="linedrowrightcolumn">5</td>
			<td align="center" class="linedrowrightcolumn">06/17/2010 10:00 AM </td>
		</tr>
		<tr>
			<td align="center" class="linedrowrightcolumn">Station 5022 </td>
			<td align="center" class="linedrowrightcolumn">3</td>
			<td align="center" class="linedrowrightcolumn">05/22/2010 09:00 AM </td>
		</tr>
		<tr>
			<td align="center" class="linedrowrightcolumn">Station 5046 </td>
			<td align="center" class="linedrowrightcolumn">3</td>
			<td align="center" class="linedrowrightcolumn">02/22/2010 04:00 AM </td>
		</tr>
		<tr>
			<td align="center" class="linedrowrightcolumn">Station 5033 </td>
			<td align="center" class="linedrowrightcolumn">10</td>
			<td align="center" class="linedrowrightcolumn">01/22/2010 01:00 PM </td>
		</tr>
	</table>
</div>
</cfdocument>