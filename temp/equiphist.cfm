

<br>
<cfdocument format="pdf">
<div align="center">
	<link rel="stylesheet" type="text/css" href="/styles.css">
	<span class="normal" style="font-size:16px">
	<b>Equipment History Report for Station 5022</b></span>
	<br>
    <br>
	<table width="700" border="0" cellpadding="5" cellspacing="0" class="lightbox" style="border-bottom:none;">
		<tr>
			<td align="center" class="linedrowrightcolumn"><b>Date/Time Reported </b></td>
			<td align="center" class="linedrowrightcolumn"><b>Last Update </b></td>
			<td align="center" class="linedrowrightcolumn"><b>Component/Part</b></td>
			<td align="center" class="linedrowrightcolumn"><b>Current Status </b></td>
			<td align="center" class="linedrow"><b>Tech Assigned </b></td>
		</tr>
		<tr>
			<td align="center" class="linedrowrightcolumn">05/17/2010 03:00 PM </td>
			<td align="center" class="linedrowrightcolumn">05/18/2010 04:30 PM </td>
			<td class="linedrowrightcolumn">Chair</td>
			<td class="linedrowrightcolumn">Closed</td>
			<td class="linedrow">Walter Roth </td>
		</tr>
		<tr>
			<td align="center" class="linedrowrightcolumn">05/23/2010 04:00 PM </td>
			<td align="center" class="linedrowrightcolumn">05/25/2010 10:00 AM </td>
			<td class="linedrowrightcolumn">Floor Mat </td>
			<td class="linedrowrightcolumn">Closed</td>
			<td class="linedrow">James Howe </td>
		</tr>
		<tr>
			<td align="center" class="linedrowrightcolumn">05/30/2010 09:00 AM </td>
			<td align="center" class="linedrowrightcolumn">05/30/2010 10:30 AM </td>
			<td class="linedrowrightcolumn">Keyboard</td>
			<td class="linedrowrightcolumn">Closed</td>
			<td class="linedrow">Walter Roth </td>
		</tr>
		<tr>
			<td align="center" class="linedrowrightcolumn">06/15/2010 01:00 PM </td>
			<td align="center" class="linedrowrightcolumn">06/16/2010 11:17 AM </td>
			<td class="linedrowrightcolumn">Monitor</td>
			<td class="linedrowrightcolumn">Closed</td>
			<td class="linedrow">Walter Roth </td>
		</tr>
	</table>
</div>
</cfdocument>