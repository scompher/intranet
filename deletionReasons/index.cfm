<cfinclude template="/jqueryincludes.cfm">
<cfif isDefined("form.btnSaveReason")>
	<cfquery name="getReason" datasource="copalink">
		select * from deletionReasons
		where reason = '#form.reasonName#'
	</cfquery>
	<cfif getReason.recordcount gt 0>
		<cfquery name="updateReason" datasource="copalink">
			update deletionReasons
			set active = 1
			where reason='#form.reasonName#'
		</cfquery>
		<cfelse>
			<cfquery name="saveReason" datasource="copalink">
				insert into deletionReasons(reason,active)
				values('#form.reasonName#',1)
			</cfquery>
	</cfif>
</cfif>
<cfquery name="getReasons" datasource="copalink">
	select *
	from deletionReasons
	where active = 1
	order by reason desc
</cfquery>



<div align="center" style="width:100%">
<table border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Add deletion Reasons</b></td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
		<form name="addCategory" action="index.cfm" method="post" id="addCategory">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td>Reasons:</td>
					<td><input type="text" maxlength="100" id="reasonName" name="reasonName"/></td>
				</tr>
				<tr>
					<td><input name="btnSaveReason" type="submit" class="sidebar" id="btnSaveReason" value="Save Reason" /></td>
				</tr>
			</table>
		</form>
		</td>
	</tr>
</table>
<br>
<br>
<br>

<table border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Deletion Reasons</b> </td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
		<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<cfoutput query="getReasons">
			<tr>
				<td><a onclick = "return confirm('Are you sure you wish to delete this item?');" href='deleteReason.cfm?reasonID=#getReasons.reasonid#'><img src='/images/delete.gif'></img></a></td>
				<td>#getReasons.reason#</td>
			</tr>
			</cfoutput>
		</table>
		</td>
	</tr>
</table>
<br>
<br>
<a href="/index.cfm" style="text-decoration:underline;" class="normal">Return to Intranet</a>
</div>
