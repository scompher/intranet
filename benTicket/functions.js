$(document).ready(function(){					   
						   
	//click to open account manager			   
	$("#jqxgrid").bind('rowdoubleclick', function (event) {
		var row = event.args.rowindex;
		var data = $("#jqxgrid").jqxGrid('getrowdata', row);
		var w = 955;
		var h = 600;
		w += 32;
		h += 96;
		wleft = (screen.width - w) / 2;
		wtop = (screen.height - h) / 2;
		var managerWin = window.open("/acctmanagment/index.cfm?bFlag=0&a=" + data.account,"accountmanager","height=600,width=955,resizable=yes,toolbar=no,left=" + wleft + ",top=" + wtop);
		managerWin.opener = self;
		managerWin.focus();
	});
	
	//turns the rows blue
	var cellclass = function (row, columnfield, value) { return 'blueclass'; };
	
	//set up data array
	var thisdata = new Array();
    var ticketNum = [ "000001", "000002", "000003", "000004", "000005", "000006", "000007", "000008", "000009", "000010", "000011", "000012", "000013", "000014", "000015", "000016", "000017", "000018" ];
    var date = [ "12/15/16", "12/21/16", "01/01/17", "01/01/17", "01/03/17", "01/17/17", "01/18/17", "02/10/17", "02/13/17", "02/13/17", "02/16/17", "02/16/17", "02/17/17", "02/17/17", "02/17/17", "02/17/17", "02/17/17", "02/17/17" ];
    var time = [ "12:15:41", "15:15:51", "02:26:52", "17:35:34", "07:35:32", "13:13:13", "13:23:44", "12:24:53", "00:44:21", "05:12:32", "08:15:52", "20:45:18", "16:15:32", "17:34:16", "18:14:24", "18:35:32", "18:35:33", "20:16:02" ];
    var department = [ "Operations", "Operations", "Operations", "Operations", "Operations", "Operations", "Dealer Support", "Operations", "Operations", "Operations", "Operations", "Operations", "Operations", "Operations", "Technology", "Operations", "Operations", "Operations" ];
    var creator = [ "Gage Majeske", "Jared Ruelen", "Jennifer Shaw", "Dottie Stein", "Samantha Scrivana", "Eric Scrivana", "Kevin Booth", "Chris Bell", "Josh Bell", "Ben Shaw", "Mike Moz", "Dale Marzili" ];
    var subject = [ "My Chair Won't Spin", "Phone Doesnt Mute", "TV Only Playing Sports Shows", "I have No-one to Talk to", "Employees not Working Hard Enough", "3.6", "3.8", "2.5", "5.0", "1.75", "3.25", "4.0" ];
    var assigned = [ "", "", "", "", "", "", "", "", "", "", "", "" ];
    var status = [ "NOT PICKED UP", "NOT PICKED UP", "NOT PICKED UP", "NOT PICKED UP", "NOT PICKED UP", "NOT PICKED UP", "NOT PICKED UP", "NOT PICKED UP", "NOT PICKED UP", "NOT PICKED UP", "NOT PICKED UP", "NOT PICKED UP" ];
    var t = ticketNum.length
    for (var i = 0; i < t; i++) {
        var row = {};
        row["ticketNum"] = ticketNum[i];
        row["date"] = date[i];
        row["time"] = time[i];
        row["department"] = department[i];
        row["creator"] = creator[i];
        row["subject"] = subject[i];
        row["assigned"] = assigned[i];
        row["status"] = status[i];
        thisdata[i] = row;
    };
        
    //set source              
    var source = {
    	localdata: thisdata,
        datatype: "array"
    };
            
	//data adaptor for source
	var dataAdapter = new $.jqx.dataAdapter(source, {
		downloadComplete: function (thisdata, status, xhr) { },
		loadComplete: function (thisdata) { },  
		loadError: function (xhr, status, error) { }
	});	
	
	// the actual grid	
	$("#jqxgrid").jqxGrid({
		width: '1200',
		source: dataAdapter,
		showfilterrow: true,
		filterable: true,
		columnsreorder: true,
		columnsresize: true,
		pageable: true,
		virtualmode: true,
		pagermode: 'simple',
		rendergridrows: function (params) { return params.data; },
		columns: [
			{ text: 'No.', datafield: 'ticketNum', cellclassname: cellclass, width: 60, minwidth: 60 },
			{ text: 'Date', datafield: 'date', cellclassname: cellclass, width: 75, minwidth: 75 },
			{ text: 'Time', datafield: 'time', cellclassname: cellclass, width: 75, minwidth: 75 },
			{ text: 'Department', datafield: 'department', cellclassname: cellclass, width: 125, minwidth: 125 },
			{ text: 'Creator', datafield: 'creator', cellclassname: cellclass, width: 200, minwidth: 200 },
			{ text: 'Subject', datafield: 'subject', cellclassname: cellclass, width: 315, minwidth: 315 },
			{ text: 'Assigned', datafield: 'assigned', cellclassname: cellclass, width: 200, minwidth: 200 },
			{ text: 'Status', datafield: 'status', cellclassname: cellclass, width: 150, minwidth: 150 }
		]
	});
});