<!---<cfsetting showdebugoutput="no">--->
<cfinclude template="/jqxGrid.cfm">
	
<style>
	.jqx-grid-row           { border-color:#FFFFFF; }
	.jqx-grid-cell          { background-color:#a4a4a4; color:#000000; }
	.jqx-grid-column-header { background-color:#b1becd; }
	.jqx-grid-pager         { background-color:#b1becd; }
	.jqx-grid-cell-selected { background-color:#b1becd; }
	.jqx-grid-cell-hover    { background-color:#b1becd; cursor:pointer; }
	.titleBar               { background-color:#000066; width:1200px; text-align:center; color:#FFFFFF; padding:5px 0px; }
</style>

<body>
	
	
	<table width='1200' align='center'>
		<tr><td>
			<div class="titleBar">C.O.P.S INTRANET TICKETING SYSTEM</div>
		</td></tr>
		<tr><td>
			<div style="font-size:14px;">
				SEARCH FOR TICKETS USING <u>ONE OR MORE</u> FIELD(S).
				<br>- DOUBLE-CLICK Ticket to Open in New Window.
				<br>- SCROLL Left/Right to See Additional Fields.
				<br>- CLICK and DRAG Column Headers to Rearrange.
			</div>
			<br><br>
			<div id="jqxgrid"></div>
		</td></tr>
	</table>
	
</body>
