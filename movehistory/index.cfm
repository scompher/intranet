
<link rel="stylesheet" type="text/css" href="../styles.css">

<cfparam name="form.fromDealer" default="">
<cfparam name="form.toDealer" default="">

<br />
<div align="center">

<cfif isDefined("form.btnMoveNow")>
	<cfset errmsg = "">
	
	<cfif trim(form.fromDealer) is "">
		<cfset errmsg = listappend(errmsg,"The from dealer number cannot be empty.")>
	</cfif>
	<cfif trim(form.toDealer) is "">
		<cfset errmsg = listappend(errmsg,"The to dealer number cannot be empty.")>
	</cfif>

	<cfif trim(errmsg) is "">
		<cfquery name="moveHistory" datasource="copalink">
			update AccumulatedHistory 
			set dealernumber = '#form.toDealer#'
			where dealernumber = '#form.fromDealer#'
		</cfquery>
		
		<cfquery name="movePendingAccounts" datasource="copalink">
			update Accounts_AccountInformation
			set dealernumber = '#form.toDealer#', targetDealerNumber = '#form.toDealer#' 
			where pendingAccount = 1 and dealernumber = '#form.fromDealer#'  
		</cfquery>
	
		<table border="0" cellspacing="0" cellpadding="5" width="400px;">
			<tr>
				<td class="highlightbar">Accumulated History Dealer Move </td>
			</tr>
			<tr>
				<td class="greyrowbottomnopadding" align="center">
				<br />
				Completed, History and Pending Accounts moved successfully. 
				<br />
				<br />
				</td>
			</tr>
		</table>
	<cfelse>
		<table border="0" cellspacing="0" cellpadding="5" width="400px;">
			<tr>
				<td class="highlightbar">Accumulated History Dealer Move </td>
			</tr>
			<tr>
				<td class="greyrowbottomnopadding" align="center">
				<br />
				<b>The following errors occured</b>
				<div align="left">
				<cfloop list="#errmsg#" index="err">
				<cfoutput>
				<ul>
					<li>#err#</li>
				</ul>
				</cfoutput>
				</cfloop>
				</div>
				<br />
				</td>
			</tr>
		</table>
		<br>
		<a style="text-decoration:underline;" class="normal" href="javascript: history.go(-1);">Return to Fix</a>
		<br /><br />

	</cfif>	
	
<cfelse>

	<table border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td class="highlightbar">Accumulated History Dealer Move  </td>
		</tr>
		<tr>
			<td class="greyrowbottomnopadding">
				<form method="post" action="index.cfm">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td nowrap>From Dealer Number: </td>
						<td>
							<input type="text" name="fromDealer" style="width:75px;">
						</td>
					</tr>
					<tr>
						<td nowrap>To Dealer Number: </td>
						<td>
							<input type="text" name="toDealer" style="width:75px;">
						</td>
					</tr>
					<tr>
						<td nowrap>
							<input name="btnMoveNow" type="submit" class="sidebar" id="btnMoveNow" value="Move Now">
						</td>
						<td>&nbsp;</td>
					</tr>
				</table>
				</form>
			</td>
		</tr>
	</table>
</cfif>

<br>
<a style="text-decoration:underline;" class="normal" href="/index.cfm">Return to Intranet</a>

</div>
