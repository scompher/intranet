
<cfif isDefined("form.btnSaveComments")>

	<cfloop from="1" to="#form.technum#" index="techcount">
		<cfset response = evaluate("form.response_#techcount#")>
		<cfset resolution = evaluate("form.resolution_#techcount#")>
		<cfquery name="updateticket" datasource="#ds#">
			update callform_incident_tech_lookup
			set	response = '#response#', resolution = '#resolution#' 
			where incidentID = #incidentID# and techNumber = #techcount#
		</cfquery>
	</cfloop>

	<cflocation url="index.cfm">

<cfelse>

	<cfif isdefined("cookie.adminlogin")><cfinclude template="/auth.cfm"></cfif>
	
	<!--- get incident details --->
	<cfquery name="getdetails" datasource="#ds#">
		select callform_incidents.*, callform_systems.system
		from callform_incidents
		inner join callform_systems on callform_incidents.systemid = callform_systems.systemid
		where callform_incidents.incidentID = #incidentID#
	</cfquery>
	
	<cfquery name="gettechs" datasource="#ds#">
		select callform_incident_tech_lookup.*, admin_users.*
		from callform_incident_tech_lookup
		inner join admin_users on callform_incident_tech_lookup.techID = admin_users.adminuserid
		where incidentID = #incidentID#
		order by techNumber ASC
	</cfquery>
	
	<!--- get list of employees in IT and Tech Svcs Departments--->
	<cfquery name="getemps" datasource="#ds#">
		select distinct admin_users.*
		from admin_users
		inner join Admin_Users_Departments_Lookup on admin_users.adminuserid = Admin_Users_Departments_Lookup.adminuserid
		where Admin_Users_Departments_Lookup.departmentid IN (1,4)
		order by admin_users.lastname asc, admin_users.firstname asc
	</cfquery>
	
	<form method="post" action="view_incident.cfm">
	<cfoutput>
	<input type="hidden" name="incidentID" value="#incidentID#">
	<input type="hidden" name="technum" value="#gettechs.recordcount#">
	</cfoutput>
	<div align="center">
	<table width="760" border="1" cellpadding="5" cellspacing="0">
		<tr>
			<td colspan="2" align="center" bgcolor="ffffcc"><b>I.T. After Hours Call Form</b></td>
		</tr>
		<cfoutput query="getdetails">
		<tr>
			<td width="50%" valign="middle">
			<table border="0" cellpadding="3" cellspacing="0">
				<tr>
					<td><b>System Affected:</b> </td>
					<td>
					#getdetails.system#
					</td>
				</tr>
			</table>
			</td>
			<td width="50%" valign="middle" align="center"><b>Time Ticket Opened: </b>#dateformat(getdetails.opened,'mm/dd/yyyy')# #timeformat(getdetails.opened,'hh:mm tt')#</td>
		</tr>
		<tr>
			<td colspan="2">
			<b>Nature of the problem:</b><br>
			#replace(getdetails.natureofproblem,"#chr(13)#","<br>","all")#
			</td>
		</tr>
		</cfoutput>
		<cfloop query="gettechs">
			<cfquery name="getsm" datasource="#ds#">
				select admin_users.firstname, admin_users.lastname
				from admin_users
				where adminuserid = #gettechs.managerID#
			</cfquery>
		<cfoutput>
		<!--- tech called section --->
		<tr bgcolor="dddddd">
			<td colspan="2"><b>Tech Called ###gettechs.currentrow#</b></td>
		</tr>
		<tr>
			<td>
			<table border="0" cellpadding="3" cellspacing="0">
				<tr>
					<td><b>Shift Manager Making Call :</b></td>
					<td>
					#getsm.firstname# #getsm.lastname#
					</td>
				</tr>
			</table>
			</td>
			<td>
			<table border="0" cellpadding="3" cellspacing="0">
				<tr>
					<td><b>Time Called :</b></td>
					<td>
					#timeformat(gettechs.called,'hh:mm tt')#
					</td>
				</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td><table border="0" cellpadding="3" cellspacing="0">
				<tr>
					<td><b>Tech Name:</b></td>
					<td>
					#gettechs.firstname# #gettechs.lastname#
					</td>
				</tr>
			</table>
			</td>
			<td>
			<table border="0" cellpadding="3" cellspacing="0">
				<tr>
					<td><b>Time Reached :</b></td>
					<td>
					#timeformat(gettechs.reached,'hh:mm tt')#
					</td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td colspan="2">
			<table border="0" cellpadding="3" cellspacing="0">
				<tr>
					<td><b>Result:</b></td>
					<td>#gettechs.callresult#<br>#gettechs.callresultother#</td>
				</tr>
			</table>
			</td>
		</tr>
		<!--- 
		<tr>
			<td colspan="2">
			<table border="0" cellpadding="3" cellspacing="0">
				<tr>
					<td><b>This Tech's Responsiveness Rating:</b> </td>
					<td>#gettechs.responserating#</td>
				</tr>
			</table>
			</td>
		</tr>
		 --->
		<tr>
			<td colspan="2">
			<b>Comments:</b><br>
			#replace(gettechs.comments,"#chr(13)#","<br>","all")#
			</td>
		</tr>
		<cfif isdefined("cookie.adminlogin")>
			<cfif (listfindnocase(departmentlist, 1) IS NOT 0) OR (listfindnocase(departmentlist, 4) IS NOT 0)>
			<tr>
				<td colspan="2"><b>I.T. Comments:</b></td>
			</tr>
			<tr>
				<td colspan="2"><textarea name="response_#technumber#" rows="5" class="normal" style="width:755px ">#gettechs.response#</textarea></td>
			</tr>
			<tr>
				<td colspan="2"><b>I.T. Resolution:</b></td>
			</tr>
			<tr>
				<td colspan="2"><textarea name="resolution_#technumber#" rows="5" class="normal" style="width:755px ">#gettechs.resolution#</textarea></td>
			</tr>
			</cfif>
		</cfif>
		<!--- tech called section --->
		</cfoutput>
		</cfloop>
		<tr>
			<td>
			<cfif isdefined("cookie.adminlogin")>
				<cfif (listfindnocase(departmentlist, 1) IS NOT 0) OR (listfindnocase(departmentlist, 4) IS NOT 0)>
					<input type="submit" name="btnSaveComments" value="Update Ticket with Reponses" class="normal">
				</cfif>
			</cfif>&nbsp;
			</td>
			<td valign="middle">
			<b>Time Ticket Closed:</b> 
			<cfif getdetails.opened is not "" and getdetails.whenclosed is not "">
				<cfoutput>#dateformat(getdetails.whenclosed,'mm/dd/yyyy')# #timeformat(getdetails.whenclosed,'hh:mm tt')#</cfoutput>
			<cfelse>
				This ticket is still open
			</cfif>
			&nbsp;<br>
			<cfif getdetails.opened is not "" and getdetails.whenclosed is not "">
			<b>Time Spent: </b>
			<cfoutput>
			#datediff("h",getdetails.opened,getdetails.whenclosed)# hours, 
			#evaluate(datediff("n",getdetails.opened,getdetails.whenclosed) MOD 60)# minutes, 
			#evaluate(datediff("s",getdetails.opened,getdetails.whenclosed) MOD 60)# seconds
			</cfoutput>
			</cfif>
			</td>
		</tr>
	</table>
	<cfif isdefined("cookie.adminlogin")>
	<br>
	<a class="normal" href="index.cfm">Return to Tickets</a>
	</cfif>
	</div>
	</form>

</cfif>
