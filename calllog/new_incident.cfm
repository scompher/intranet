
<cfif isdefined("form.btnCloseIncident") OR isdefined("form.btnSaveIncident")>

<cfif isdefined("form.btnCloseIncident")>
	<cfset closed = 1>
	<cfset whenclosed = createodbcdatetime(now())>
<cfelse>
	<cfset closed = 0>
	<cfset whenclosed = "NULL">
</cfif>

<cftransaction>
<!--- save basic info --->
<cfquery name="saveIncident" datasource="#ds#">
	insert into callform_incidents (opened, systemID, natureOfProblem, closed, whenclosed)
	values (#form.opened#, #form.systemid#, '#form.natureofproblem#', #closed#, #whenclosed#)
</cfquery>
<!--- look up incicent ID --->
<cfquery name="getID" datasource="#ds#">
	select max(incidentID) as newid from callform_incidents
</cfquery>
<cfset incidentID = getid.newid>
<!--- save tech info --->
<cfquery name="insertTechs" datasource="#ds#">
	<cfloop from="1" to="#technum#" index="techcount">
		<cfset techid = evaluate("form.techid_#techcount#")>
		<cfset managerID = evaluate("form.managerid_#techcount#")>
		<cfset timecalled = evaluate("form.timecalled_#techcount#")>
		<cfif trim(timecalled) IS ""><cfset timecalled = "NULL"></cfif>
		<cfset timereached = evaluate("form.timereached_#techcount#")>
		<cfif trim(timereached) IS ""><cfset timereached = "NULL"></cfif>
		<cfset callresult = evaluate("form.callresult_#techcount#")>
		<cfset callresultother = evaluate("form.callresultother_#techcount#")>
		
		<!--- <cfset responserating = evaluate("form.responserating_#techcount#")> --->
		<cfset responserating = 0>
		
		<cfset comments = evaluate("form.comments_#techcount#")>
		begin
			insert into callform_incident_tech_lookup (incidentID, techNumber, techID, managerID, called, reached, callresult, callresultother, responserating, comments)
			values (#incidentID#, #techcount#, #techID#, #managerID#, #timecalled#, #timereached#, '#callresult#', '#callresultother#', #responserating#, '#comments#')
		end
	</cfloop>
</cfquery>
</cftransaction>

<!--- notify techs --->
<cfloop from="1" to="#technum#" index="techcount">
	<cfset techid = evaluate("form.techid_#techcount#")>
	<cfquery name="getemail" datasource="#ds#">
		select email from admin_users
		where adminuserid = #techid#
	</cfquery>
	<cfif getemail.email is not "">
		<cfmail from="pgregory@copsmonitoring.com" to="#getemail.email#" subject="After hours call log ticket notification" username="copalink@copsmonitoring.com" password="copsmoncal">
			You have been contacted for an after hours call, use the link below to view incident details.
			
			http://192.168.107.10/calllog/view_incident.cfm?incidentid=#incidentid#
		</cfmail>
	</cfif>
</cfloop>

<cfif closed IS 1>
<!--- get incident details --->
<cfquery name="getdetails" datasource="#ds#">
	select callform_incidents.*, callform_systems.system
	from callform_incidents
	inner join callform_systems on callform_incidents.systemid = callform_systems.systemid
	where callform_incidents.incidentID = #incidentID#
</cfquery>

<cfquery name="gettechs" datasource="#ds#">
	select callform_incident_tech_lookup.*, admin_users.*
	from callform_incident_tech_lookup
	inner join admin_users on callform_incident_tech_lookup.techID = admin_users.adminuserid
	where incidentID = #incidentID# and active = 1
	order by techNumber ASC
</cfquery>

<!--- get list of employees in IT and Tech Svcs Departments--->
<cfquery name="getemps" datasource="#ds#">
	select distinct admin_users.*
	from admin_users
	inner join Admin_Users_Departments_Lookup on admin_users.adminuserid = Admin_Users_Departments_Lookup.adminuserid
	where Admin_Users_Departments_Lookup.departmentid IN (1,4)
	order by admin_users.lastname asc, admin_users.firstname asc
</cfquery>

<cfset tolist = "kpickell@copsmonitoring.com;SScrivana@copsmonitoring.com;MSteigerwalt@copsmonitoring.com;pgregory@copsmonitoring.com;ktallman@copsmonitoring.com">
<!--- 
<cfif findnocase("manitou",getdetails.system) is not 0>
	<cfset tolist = listappend(tolist,"ktallman@copsmonitoring.com",";")>
</cfif>
--->

<!--- send email to managers --->
<cfmail from="pgregory@copsmonitoring.com" to="#tolist#" subject="After hours call log closed ticket notification" username="copalink@copsmonitoring.com" password="copsmoncal">
I.T. After Hours Call Form Closed Ticket Notification

<cfloop query="getdetails">
System Affected: #getdetails.system#
Time Ticket Opened: #dateformat(getdetails.opened,'mm/dd/yyyy')# #timeformat(getdetails.opened,'hh:mm tt')#
Nature of the problem:
#getdetails.natureofproblem#
</cfloop>

<cfloop query="gettechs">
<cfquery name="getsm" datasource="#ds#">
	select admin_users.firstname, admin_users.lastname
	from admin_users
	where adminuserid = #gettechs.managerID#
</cfquery>
Tech Called ###gettechs.currentrow#
Shift Manager Making Call: #getsm.firstname# #getsm.lastname#
Time Called: #timeformat(gettechs.called,'hh:mm tt')#
Tech Name: #gettechs.firstname# #gettechs.lastname#
Time Reached: #timeformat(gettechs.reached,'hh:mm tt')#
Result:
#gettechs.callresult#

#gettechs.callresultother#

Comments:
#gettechs.comments#
</cfloop>

Time Ticket Closed: #dateformat(getdetails.whenclosed,'mm/dd/yyyy')# #timeformat(getdetails.whenclosed,'hh:mm tt')#
Time Spent: 
#datediff("h",getdetails.opened,getdetails.whenclosed)# hours, 
#evaluate(datediff("n",getdetails.opened,getdetails.whenclosed) MOD 60)# minutes, 
#evaluate(datediff("s",getdetails.opened,getdetails.whenclosed) MOD 60)# seconds
</cfmail>


<!--- send email to techs involved --->
<cfset tolist = valuelist(gettechs.email,";")>
<cfmail from="pgregory@copsmonitoring.com" to="#tolist#" subject="After hours call log closed ticket notification" username="copalink@copsmonitoring.com" password="copsmoncal">
I.T. After Hours Call Form Closed Ticket Notification

<cfloop query="getdetails">
System Affected: #getdetails.system#
Time Ticket Opened: #dateformat(getdetails.opened,'mm/dd/yyyy')# #timeformat(getdetails.opened,'hh:mm tt')#
Nature of the problem:
#getdetails.natureofproblem#
</cfloop>

<cfloop query="gettechs">
<cfquery name="getsm" datasource="#ds#">
	select admin_users.firstname, admin_users.lastname
	from admin_users
	where adminuserid = #gettechs.managerID#
</cfquery>
Tech Called ###gettechs.currentrow#
Shift Manager Making Call: #getsm.firstname# #getsm.lastname#
Time Called: #timeformat(gettechs.called,'hh:mm tt')#
Tech Name: #gettechs.firstname# #gettechs.lastname#
Time Reached: #timeformat(gettechs.reached,'hh:mm tt')#
Result:
#gettechs.callresult#

#gettechs.callresultother#

Comments:
#gettechs.comments#
</cfloop>

Time Ticket Closed: #dateformat(getdetails.whenclosed,'mm/dd/yyyy')# #timeformat(getdetails.whenclosed,'hh:mm tt')#
Time Spent: 
#datediff("h",getdetails.opened,getdetails.whenclosed)# hours, 
#evaluate(datediff("n",getdetails.opened,getdetails.whenclosed) MOD 60)# minutes, 
#evaluate(datediff("s",getdetails.opened,getdetails.whenclosed) MOD 60)# seconds
</cfmail>

</cfif>

<cflocation url="index.cfm">

<cfabort>
</cfif>

<script language="JavaScript" type="text/JavaScript">
function checkform(frm,technums,btn) {

if (btn == "close" || btn == "save") {
	if (frm.systemID.selectedIndex == 0) {alert('Please select the system affected'); frm.systemID.focus(); return false;}
	if (frm.natureofproblem.value == "") {alert('Please fill in the nature of the problem'); frm.natureofproblem.focus(); return false;}
}

for (var technum = 1;technum <= technums;technum++) {
	var t = "frm.techid_" + technum;
	var tech = eval(t);
	if (tech.selectedIndex == 0) {
		alert('Please select the name for tech ' + technum);
		tech.focus();
		return false;
	}

	var cr = "frm.callresult_" + technum;
	var callresult = eval(cr);
	var cro = "frm.callresultother_" + technum;
	var callresultother = eval(cro);

	if ((!callresult[0].checked) && (!callresult[1].checked) && (!callresult[2].checked) && (!callresult[3].checked)) {
		alert('Please select a call result for tech ' + technum);
		return false;
	}

	if ((callresult[2].checked) && (callresultother.value == "")) {
		alert('Please fill in the reason for selecting Other as the call result for tech ' + technum);
		callresultother.focus();
		return false;
	}

	/*
	var rr = "frm.responserating_" + technum;
	var responserating = eval(rr);
	
	if (!responserating[0].checked && !responserating[1].checked && !responserating[2].checked && !responserating[3].checked && !responserating[4].checked) {
		alert('Please rate tech ' + technum);
		return false;
	}
	*/

	var c = "frm.comments_" + technum;
	var comments = eval(c);
	
	if (comments.value == "") {
		alert('Please enter comments for tech ' + technum);
		comments.focus();
		return false;
	}
}

	return true;
}
</script>

<!--- defaults --->
<cfparam name="systemID" default="0">
<cfparam name="natureofproblem" default="">
<cfparam name="technum" default="1">
<!--- defaults --->

<cfset timeCalledNotMarked = false>

<cfif isdefined("form.btnCallNextTech.x")>
	<cfset technum = technum + 1>
</cfif>

<cfloop from="1" to="#technum#" index="techcount">

	<cfparam name="timecalled_#techcount#" default="">
	<cfparam name="timereached_#techcount#" default="">
	<cfparam name="callresult_#techcount#" default="">
	<cfparam name="callresultother_#techcount#" default="">
	<cfparam name="responserating_#techcount#" default="0">
	<cfparam name="comments_#techcount#" default="">
	
	<cfset MarkTimeCalled = "form.btnMarkTimeCalled_#techcount#">
	<cfif isdefined("#MarkTimeCalled#")>
		<cfset "timecalled_#techcount#" = createodbcdatetime(now())>
	</cfif>

	<cfset MarkTimeReached = "form.btnMarkTimeReached_#techcount#">
	<cfif isdefined("#MarkTimeReached#")>
		<!--- <cfset "timereached_#techcount#" = createodbcdatetime(now())> --->
		<cfset "timereached_#techcount#" = createodbcdatetime("#form.reachedHH#:#form.reachedMM# #form.reachedTT#")>
	</cfif>
</cfloop>

<!--- get systems --->
<cfquery name="getsystems" datasource="#ds#">
	select systemid as sysid, system
	from callform_systems
	order by system ASC
</cfquery>

<cfif not isdefined("opened")>
	<cfset opened = createodbcdatetime(now())>
</cfif>

<!--- get shift manager --->
<cfquery name="getsm" datasource="#ds#">
	select * from admin_users
	where adminuserid = #cookie.adminlogin#
</cfquery>

<!--- get list of employees in IT and Tech Svcs Departments--->
<cfquery name="getemps" datasource="#ds#">
	select distinct admin_users.*
	from admin_users
	inner join Admin_Users_Departments_Lookup on admin_users.adminuserid = Admin_Users_Departments_Lookup.adminuserid
	where Admin_Users_Departments_Lookup.departmentid IN (1,4) and active = 1
	order by admin_users.lastname asc, admin_users.firstname asc
</cfquery>

<cfoutput>
<form method="post" action="new_incident.cfm">

<input type="hidden" name="opened" value="#opened#">
<input type="hidden" name="technum" value="#technum#">
<cfloop from="1" to="#technum#" index="techcount">
	<input type="hidden" name="timecalled_#techcount#" value="#evaluate('timecalled_#techcount#')#">
	<input type="hidden" name="timereached_#techcount#" value="#evaluate('timereached_#techcount#')#">
</cfloop>
</cfoutput>
<div align="center">
<table width="760" border="1" cellpadding="5" cellspacing="0">
	<tr>
		<td colspan="2" align="center" bgcolor="ffffcc"><b>I.T. After Hours Call Form</b></td>
	</tr>
	<tr>
		<td width="50%" valign="middle">
		<table border="0" cellpadding="3" cellspacing="0">
        	<tr>
        		<td><b>System Affected:</b></td>
        		<td>
				<select name="systemID" class="normal">
					<option value="0"></option>
				<cfoutput query="getsystems">
					<option value="#getsystems.sysid#" <cfif systemID IS getsystems.sysid>selected</cfif> >#getsystems.system#</option>
				</cfoutput>
				</select>
				</td>
        	</tr>
       	</table>
		</td>
	    <td width="50%" valign="middle" align="center"><b>Time Ticket Opened: </b><cfoutput>#dateformat(opened,'mm/dd/yyyy')# #timeformat(opened,'hh:mm tt')#</cfoutput></td>
	</tr>
	<tr>
		<td colspan="2"><b>Nature of the problem:</b></td>
	</tr>
	<tr>
		<td colspan="2"><textarea name="natureofproblem" rows="5" class="normal" style="width:755px "><cfoutput>#natureofproblem#</cfoutput></textarea></td>
	</tr>
	<cfloop from="1" to="#technum#" index="techcount">
	<!--- tech called section --->
	<cfoutput>
	<tr bgcolor="dddddd">
		<td colspan="2"><b>Tech Called ###techcount#</b></td>
	</tr>
	</cfoutput>
	<tr>
		<td>
		<table border="0" cellpadding="3" cellspacing="0">
        	<tr>
        		<td>Shift Manager Making Call :</td>
        		<td>
				<cfoutput>#getsm.firstname# #getsm.lastname#</cfoutput>
				<cfset mid = "managerid_#techcount#">
				<cfoutput>
				<cfif isdefined("#mid#")>
					<cfset m = evaluate("#mid#")>
					<input type="hidden" name="managerid_#techcount#" value="#m#">
				<cfelse>
					<input type="hidden" name="managerid_#techcount#" value="#getsm.adminuserid#">
				</cfif>
				</cfoutput>
				</td>
       		</tr>
        </table>
		</td>
		<td>
		<table border="0" cellpadding="3" cellspacing="0">
        	<tr>
        		<td>Time Called :</td>
        		<td>
				<cfset tc = "timecalled_#techcount#">
				<cfif trim(evaluate('#tc#')) IS NOT "">
					<cfset t = evaluate("#tc#")>
					<cfoutput>#timeformat(t,'hh:mm tt')#</cfoutput>
				<cfelse>
					<cfoutput>
					<cfset timeCalledNotMarked = true>
					<input name="btnMarkTimeCalled_#techcount#" type="submit" class="normal" value="Mark Time">
					</cfoutput>
				</cfif>
				</td>
       		</tr>
        	</table>
		</td>
	</tr>
	<tr>
		<td><table border="0" cellpadding="3" cellspacing="0">
        	<tr>
        		<td>Tech Name:</td>
        		<td>
				<cfset tid = "techid_#techcount#">
				<cfif isdefined("#tid#")>
					<cfset id = evaluate("#tid#")>
				<cfelse>
					<cfset id = 0>
				</cfif>
				<cfoutput><select name="techid_#techcount#" class="normal"></cfoutput>
					<option value="0"></option>
				<cfoutput query="getemps">
					<option value="#getemps.adminuserid#" <cfif id IS getemps.adminuserid>selected</cfif> >#getemps.firstname# #getemps.lastname#</option>
				</cfoutput>
				</select>
				</td>
       		</tr>
       	</table>
		</td>
	    <td>
		<table border="0" cellpadding="3" cellspacing="0">
        	<tr>
        		<td>Time Reached :</td>
        		<td>
				<cfset tr = "timereached_#techcount#">
				<cfif trim(evaluate('#tr#')) IS NOT "">
					<cfset r = evaluate("#tr#")>
					<cfoutput>#timeformat(r,'hh:mm tt')#</cfoutput>
				<cfelse>
					<cfif isdefined("variables.t")>
						<cfif technum IS techcount>
							<select name="reachedHH">
							<cfloop from="01" to="12" index="hour">
								<cfoutput>
								<option value="#numberformat(hour,'00')#" <cfif hour IS timeformat(now(),'hh')>selected</cfif> >#numberformat(hour,'00')#</option>
								</cfoutput>
							</cfloop>
							</select> : 
							<select name="reachedMM">
							<cfloop from="00" to="59" index="min">
								<cfoutput>
								<option value="#numberformat(min,'00')#" <cfif min IS timeformat(now(),'mm')>selected</cfif> >#numberformat(min,'00')#</option>
								</cfoutput>
							</cfloop>
							</select> 
							<select name="reachedTT">
								<option value="AM" <cfif timeformat(now(),'tt') IS "am">selected</cfif> >AM</option>
								<option value="PM" <cfif timeformat(now(),'tt') IS "pm">selected</cfif> >PM</option>
							</select>
							<cfoutput><input name="btnMarkTimeReached_#techcount#" type="submit" class="normal" value="Mark Time"></cfoutput>
						<cfelse>
							Not Available
						</cfif>
					</cfif>
				</cfif>
				</td>
       		</tr>
        </table>
		</td>
	</tr>
	<tr>
		<td colspan="2">
		<cfset cr = "callresult_#techcount#">
		<cfset c = evaluate("#cr#")>
		<cfset cro = "callresultother_#techcount#">
		<cfset o = evaluate("#cro#")>
		<table border="0" cellpadding="3" cellspacing="0">
        	<tr>
        		<td colspan="3">Call Result:</td>
        	</tr>
        	<tr>
        		<td width="20"><cfoutput><input type="checkbox" name="callresult_#techcount#" value="located and working on problem" <cfif listfindnocase(c,"located and working on problem")>checked</cfif> ></cfoutput></td>
        		<td colspan="2">Located &amp; Working on problem</td>
        	</tr>
        	<tr>
        		<td width="20"><cfoutput><input type="checkbox" name="callresult_#techcount#" value="left message will call alternate number" <cfif listfindnocase(c,"left message will call alternate number")>checked</cfif> ></cfoutput></td>
        		<td colspan="2">Left Message will call alternate number</td>
        	</tr>
        	<tr>
        		<td width="20"><cfoutput><input type="checkbox" name="callresult_#techcount#" value="other" <cfif listfindnocase(c,"other")>checked</cfif> ></cfoutput></td>
        		<td width="40">Other:</td>
		 		<td><cfoutput><input type="text" name="callresultother_#techcount#" class="normal" style="width:250px" value="#o#"></cfoutput></td>
        	</tr>
        	<tr>
        		<td width="20"><cfoutput><input type="checkbox" name="callresult_#techcount#" value="no answer or response" <cfif listfindnocase(c,"no answer or response")>checked</cfif> ></cfoutput></td>
        		<td colspan="2">No Answer or Response</td>
        	</tr>
			<cfif technum IS techcount and isdefined("variables.t")>
			<cfoutput>
        	<tr>
        		<td colspan="3" align="left">
				<input onclick="return checkform(this.form,'#techcount#','noanswer');" name="btnCallNextTech" type="image" src="/images/btnCallNextTech.gif" class="box">
				</td>
       		</tr>
			</cfoutput>
			</cfif>
       	</table>
		</td>
	</tr>
	<!--- 
	<tr>
		<td colspan="2">
		<cfset rr = "responserating_#techcount#">
		<cfset rating = evaluate("#rr#")>
		<table border="0" cellpadding="3" cellspacing="0">
			<cfoutput>
        	<tr>
        		<td>This Tech's Responsiveness Rating: </td>
        		<td><input name="responserating_#techcount#" type="radio" value="1" <cfif rating IS 1>checked</cfif> >1 (best)</td>
        		<td><input name="responserating_#techcount#" type="radio" value="2" <cfif rating IS 2>checked</cfif> >2</td>
        		<td><input name="responserating_#techcount#" type="radio" value="3" <cfif rating IS 3>checked</cfif> >3</td>
        		<td><input name="responserating_#techcount#" type="radio" value="4" <cfif rating IS 4>checked</cfif> >4</td>
        		<td><input name="responserating_#techcount#" type="radio" value="5" <cfif rating IS 5>checked</cfif> >5 (worst)</td>
       		</tr>
			</cfoutput>
       	</table>
		</td>
	</tr>
	 --->
	<tr>
		<td colspan="2">Comments:</td>
	</tr>
	<cfset comm = "comments_#techcount#">
	<cfset c = evaluate("#comm#")>
	<tr>
		<td colspan="2"><cfoutput><textarea name="comments_#techcount#" rows="5" class="normal" style="width:755px ">#c#</textarea></cfoutput></td>
	</tr>
	<!--- tech called section --->
	</cfloop>
	<tr>
		<td colspan="2">
		<cfif timeCalledNotMarked is true>
			<b style="font-size:16px; color:#FF0000;">DID YOU REMEMBER TO MARK THE TIME CALLED FOR EACH TECH?</b>
		<cfelse>
			<cfoutput>
			<input onclick="return checkform(this.form,'#technum#','close');" type="submit" name="btnCloseIncident" class="normal" value="Complete this call ticket">
			<input onclick="return checkform(this.form,'#technum#','save');" type="submit" name="btnSaveIncident" class="normal" value="Save ticket and leave open">
			</cfoutput>
		</cfif>
		</td>
	</tr>
</table>
<br>
<a class="normal" href="index.cfm">Return to Tickets</a>
</div>
</form>

