
<!--- get list of employees in IT and Tech Svcs Departments--->
<cfquery name="getTechs" datasource="#ds#">
	select distinct admin_users.*
	from admin_users
	inner join Admin_Users_Departments_Lookup on admin_users.adminuserid = Admin_Users_Departments_Lookup.adminuserid
	where Admin_Users_Departments_Lookup.departmentid IN (1,4) and admin_users.active = 1 
	order by admin_users.lastname asc, admin_users.firstname asc
</cfquery>

<script language="javascript" src="cal2.js">
/*
Xin's Popup calendar script-  Xin Yang (http://www.yxscripts.com/)
Script featured on/available at http://www.dynamicdrive.com/
This notice must stay intact for use
*/
</script>
<script language="javascript" src="cal_conf2.js"></script>

<div align="center">
<form method="post" action="viewall.cfm">
<table border="1" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center" class="titlebar" bgcolor="000066"><b>I.T. After Hours Call Log Lookup</b></td>
	</tr>
	<tr>
		<td>
		<b>Choose Date Range:</b><br />
		</td>
	</tr>
	<tr>
		<td>
		<table border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td><b>Beginning Date:</b></td>
				<td width="25">&nbsp;</td>
				<td><b>Ending Date:</b></td>
			</tr>
			<tr>
				<td><input type="text" name="begindate" style="width:75px" maxlength="10">
					<a style="text-decoration:none;" href="javascript:showCal('begindate');"><img border="0" src="../images/calicon.gif" width="20" height="20" align="absmiddle"></a>				</td>
				<td>&nbsp;</td>
				<td><input type="text" name="enddate" style="width:75px" maxlength="10">
					<a style="text-decoration:none;" href="javascript:showCal('enddate');"><img border="0" src="../images/calicon.gif" width="20" height="20" align="absmiddle"></a>				</td>
			</tr>
			<tr>
				<td><b>Beginning Time:</b></td>
				<td width="25">&nbsp;</td>
				<td><b>Ending Time:</b></td>
			</tr>
			<tr>
				<td>
				<select name="beginHour" style="vertical-align:middle;">
					<option value=""></option>
					<cfloop from="00" to="23" index="h">
						<cfoutput>
						<option value="#numberformat(h,'00')#">#numberformat(h,'00')#</option>
						</cfoutput>
					</cfloop>
				</select>
				:
				<select name="beginMin" style="vertical-align:middle;">
					<option value=""></option>
					<cfloop from="00" to="59" index="m">
						<cfoutput>
						<option value="#numberformat(m,'00')#">#numberformat(m,'00')#</option>
						</cfoutput>
					</cfloop>
				</select>
				</td>
				<td>&nbsp;</td>
				<td>
				<select name="endingHour" style="vertical-align:middle;">
					<option value=""></option>
					<cfloop from="00" to="23" index="h">
						<cfoutput>
						<option value="#numberformat(h,'00')#">#numberformat(h,'00')#</option>
						</cfoutput>
					</cfloop>
				</select>
				:
				<select name="endingMin" style="vertical-align:middle;">
					<option value=""></option>
					<cfloop from="00" to="59" index="m">
						<cfoutput>
						<option value="#numberformat(m,'00')#">#numberformat(m,'00')#</option>
						</cfoutput>
					</cfloop>
				</select>
				</td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td><b>Select Techs to Filter:</b> </td>
	</tr>
	<tr>
		<td>
		<select name="adminuseridlist" size="10" class="normal" multiple="multiple" style="width:250px">
		<cfoutput query="getTechs">
			<option value="#adminuserid#">#firstname# #lastname#</option>
		</cfoutput>
		</select><br />
		<span class="small" style="color:#ff0000">Hold CTRL or SHIFT to select multiples</span>		</td>
	</tr>
	<tr>
		<td><input type="checkbox" name="briefview" value="1" /> Brief view</td>
	</tr>
	<tr>
		<td><input type="submit" name="Submit" value="Submit Lookup" />
			<input type="reset" name="Submit2" value="Clear Form" /></td>
	</tr>
</table>
</form>
</div>


