
<cfif isdefined("form.btnNew")>
	<cflocation url="new_incident.cfm">
</cfif>

<cfparam name="sort" default="open_desc">

<cfswitch expression="#sort#">
	<cfcase value="system_asc"><cfset orderby = "callform_systems.system asc"></cfcase>
	<cfcase value="system_desc"><cfset orderby = "callform_systems.system desc"></cfcase>
	<cfcase value="open_asc"><cfset orderby = "callform_incidents.opened asc"></cfcase>
	<cfcase value="open_desc"><cfset orderby = "callform_incidents.opened desc"></cfcase>
	<cfcase value="whenclosed_asc"><cfset orderby = "callform_incidents.whenclosed asc"></cfcase>
	<cfcase value="whenclosed_desc"><cfset orderby = "callform_incidents.whenclosed desc"></cfcase>
	<cfcase value="closed_asc"><cfset orderby = "callform_incidents.closed asc"></cfcase>
	<cfcase value="closed_desc"><cfset orderby = "callform_incidents.closed desc"></cfcase>
</cfswitch>

<cfset yearstart = createodbcdate("1/1/#dateformat(now(),'yyyy')#")>

<cfset yearstart = dateadd("yyyy",-1,yearstart)>

<!--- get incidents --->
<cfquery name="getincidents" datasource="#ds#">
	select callform_incidents.*, callform_systems.system
	from callform_incidents
	inner join callform_systems on callform_incidents.systemid = callform_systems.systemid
	where opened > #yearstart#
	order by #orderby#
</cfquery>

<div align="center">
<br />

<form method="post" action="index.cfm">
<table width="900" border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td><input name="btnNew" type="submit" class="normal" value="Open New Ticket"></td>
	</tr>
</table>
</form>
<table width="900" border="1" cellpadding="5" cellspacing="0">
	<tr align="center" bgcolor="ffffcc">
		<td colspan="6"><b>I.T. After Hours Call Logs</b> </td>
	</tr>
	<cfoutput>
	<tr align="center" bgcolor="dddddd">
		<td nowrap="nowrap">
		<strong>Incident ##</strong>
		</td>
		<td nowrap>
		<cfif sort IS "system_asc"><cfset sortlink = "system_desc"><cfelse><cfset sortlink = "system_asc"></cfif>
		<b><a href="index.cfm?sort=#sortlink#">System</a></b>
		</td>
		<td nowrap>
		<cfif sort IS "open_asc"><cfset sortlink = "open_desc"><cfelse><cfset sortlink = "open_asc"></cfif>
		<b><a href="index.cfm?sort=#sortlink#">Open Time</a></b>
		</td>
		<td nowrap>
		<cfif sort IS "whenclosed_asc"><cfset sortlink = "whenclosed_desc"><cfelse><cfset sortlink = "whenclosed_asc"></cfif>
		<b><a href="index.cfm?sort=#sortlink#">Close Time</a></b>
		</td>
		<td width="500" nowrap><b>Description of Problem</b></td>
	    <td nowrap>
		<cfif sort IS "closed_asc"><cfset sortlink = "closed_desc"><cfelse><cfset sortlink = "closed_asc"></cfif>
		<b><a href="index.cfm?sort=#sortlink#">Status</a></b>
		</td>
	</tr>
	</cfoutput>
	<cfoutput query="getincidents">
	<tr valign="top">
		<td align="center" nowrap="nowrap">#getincidents.incidentid#</td>
		<td align="center" nowrap>#getincidents.system#</td>
		<td align="center" nowrap>#dateformat(getincidents.opened,'mm/dd/yyyy')# #timeformat(getincidents.opened, 'hh:mm tt')#</td>
		<td align="center" nowrap>
		<cfif trim(whenclosed) IS NOT "">
			#dateformat(getincidents.whenclosed,'mm/dd/yyyy')# #timeformat(getincidents.whenclosed, 'hh:mm tt')#
		<cfelse>
			&nbsp;
		</cfif>		
		</td>
		<td>
		<cfif getincidents.closed IS NOT 0>
			<a href="view_incident.cfm?incidentid=#getincidents.incidentid#">
		<cfelse>
			<a href="edit_incident.cfm?incidentid=#getincidents.incidentid#">
		</cfif>
		<cfif len(getincidents.natureOfProblem) GT 100>
			#mid(getincidents.natureOfProblem,1,100)#...
		<cfelse>
			#getincidents.natureOfProblem#
		</cfif>
		</a>
		</td>
		<td align="center" nowrap><cfif getincidents.closed IS NOT 0>Closed<cfelse>Open</cfif></td>
	</tr>
	</cfoutput>
</table>
<br>
<form method="post" action="index.cfm">
<table width="900" border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td><input name="btnNew" type="submit" class="normal" value="Open New Ticket"></td>
	</tr>
</table>
</form>
<table border="0" cellpadding="5" cellspacing="0">
<tr>
	<td><a href="/index.cfm">Return to Administrative Menu</a></td>
</tr>
</table>
</div>
