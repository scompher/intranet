<cfif isdefined("cookie.adminlogin")><cfinclude template="/auth.cfm"></cfif>
<style>
#pageBreakID {
    page-break-before:always;
    mso-break-type:page-break;
}
</style>


<!--- <cfset adminuseridlist = "10,12,33"> --->
<cfparam name="adminuseridlist" default="">
<cfparam name="form.begindate" default="">
<cfparam name="form.enddate" default="">
<cfparam name="variables.begindate" default="">
<cfparam name="variables.enddate" default="">
<cfparam name="form.beginHour" default="">
<cfparam name="form.beginMin" default="">
<cfparam name="form.endHour" default="">
<cfparam name="form.endMin" default="">

<cfif trim(form.beginHour) is not "" and trim(form.beginMin) is not "">
	<cfset beginTime = "#form.beginHour#:#form.beginMin#">
<cfelse>
	<cfset beginTime = "">
</cfif>
<cfif trim(form.endHour) is not "" and trim(form.endMin) is not "">
	<cfset endTime = "#form.endHour#:#form.endMin#">
<cfelse>
	<cfset endTime = "">
</cfif>

<cfif trim(form.begindate) IS NOT "">
	<cfif trim(beginTime) is not "">
		<cfset variables.begindate = createodbcdatetime("#form.begindate# #beginTime#")>
	<cfelse>
		<cfset variables.begindate = createodbcdate("#form.begindate#")>
	</cfif>
</cfif>
<cfif trim(form.enddate) IS NOT "">
	<cfif trim(beginTime) is not "">
		<cfset variables.enddate = createodbcdatetime("#form.enddate# #form.endTime#")>
	<cfelse>
		<cfset variables.enddate = createodbcdate("#form.enddate#")>
	</cfif>
</cfif>

<cfquery name="getall" datasource="#ds#">
	select distinct callform_incidents.incidentID, callform_incidents.opened 
	from callform_incidents
	left join callform_incident_tech_lookup on callform_incidents.incidentID = callform_incident_tech_lookup.incidentID
	where 
	<cfif trim(adminuseridlist) IS NOT "">callform_incident_tech_lookup.techID IN (#adminuseridlist#) and </cfif>
	<cfif trim(variables.begindate) IS NOT "">opened >= #variables.begindate# and </cfif>
	<cfif trim(variables.enddate) IS NOT "">opened <= #variables.enddate# and </cfif>
	0=0 
	order by opened asc
</cfquery>

<cfif getall.recordcount is 0>
	<div align="center">
	<table border="1" cellpadding="5" cellspacing="0">
		<tr>
			<td align="center" bgcolor="ffffcc"><b>I.T. After Hours Call Form</b></td>
		</tr>
		<tr>
			<td>There were no incidents that matched your search criteria</td>
		</tr>
		<tr>
			<td><a href="javascript:history.go(-1);">Return to Lookup</a></td>
		</tr>
	</table>
	</div>
	<cfabort>
</cfif>

<cfset incidentidlist = valuelist(getall.incidentid)>

<div class="normal" align="left">There are <cfoutput>#listlen(incidentidlist)#</cfoutput> items</div>

<br />

<cfloop list="#incidentidlist#" index="incidentid">

	<!--- get incident details --->
	<cfquery name="getdetails" datasource="#ds#">
		select callform_incidents.*, callform_systems.system
		from callform_incidents
		inner join callform_systems on callform_incidents.systemid = callform_systems.systemid
		where callform_incidents.incidentID = #incidentID#
	</cfquery>
	
	<cfquery name="gettechs" datasource="#ds#">
		select callform_incident_tech_lookup.*, admin_users.*
		from callform_incident_tech_lookup
		inner join admin_users on callform_incident_tech_lookup.techID = admin_users.adminuserid
		where incidentID = #incidentID#
		order by techNumber ASC
	</cfquery>
	
	<!--- get list of employees in IT and Tech Svcs Departments--->
	<cfquery name="getemps" datasource="#ds#">
		select distinct admin_users.*
		from admin_users
		inner join Admin_Users_Departments_Lookup on admin_users.adminuserid = Admin_Users_Departments_Lookup.adminuserid
		where Admin_Users_Departments_Lookup.departmentid IN (1,4)
		order by admin_users.lastname asc, admin_users.firstname asc
	</cfquery>
	
	<!--- <form method="post" action="view_incident.cfm"> --->
	<cfoutput>
	<!--- 
	<input type="hidden" name="incidentID" value="#incidentID#">
	<input type="hidden" name="technum" value="#gettechs.recordcount#">
	--->
	</cfoutput>
	<div align="left">
		<cfoutput>
		Incident ##: #incidentID# - #dateformat(getdetails.opened,'mm/dd/yyyy')# #timeformat(getdetails.opened,'hh:mm tt')# - #replace(getdetails.natureofproblem,"#chr(13)#"," ","all")#<br>
		</cfoutput>

		<cfloop query="gettechs">
			<cfquery name="getsm" datasource="#ds#">
				select admin_users.firstname, admin_users.lastname
				from admin_users
				where adminuserid = #gettechs.managerID#
			</cfquery>
			<cfoutput>
			<!--- tech called section --->
				Ops Comments: #replace(gettechs.comments,"#chr(13)#","<br>","all")#<br>
			<cfif isdefined("cookie.adminlogin")>
				<cfif (listfindnocase(departmentlist, 1) IS NOT 0) OR (listfindnocase(departmentlist, 4) IS NOT 0)>
					#getTechs.firstname# #getTechs.lastname# Comments: #gettechs.response#<br>
					<cfif trim(gettechs.resolution) is not "">
						#getTechs.firstname# #getTechs.lastname# Resolution: #gettechs.resolution#<br>
					</cfif>
				</cfif>
			</cfif>
			<!--- tech called section --->
			</cfoutput>
		</cfloop>

	<cfif isdefined("cookie.adminlogin")>
	<br>
	<!--- <a class="normal" href="index.cfm">Return to Tickets</a> --->
	</cfif>
	</div>
	<!--- </form> --->
	<div id="pageBreakID"></div>
</cfloop>
