
<cfparam name="adminuseridlist" default="">
<cfset beginDate = "7/1/2015">
<cfset endDate = "7/8/2015">

<cfquery name="getall" datasource="#ds#">
	select distinct callform_incidents.incidentID, callform_incidents.opened 
	from callform_incidents
	left join callform_incident_tech_lookup on callform_incidents.incidentID = callform_incident_tech_lookup.incidentID
	where 
	<cfif trim(adminuseridlist) IS NOT "">callform_incident_tech_lookup.techID IN (#adminuseridlist#) and </cfif>
	<cfif trim(variables.begindate) IS NOT "">opened >= #variables.begindate# and </cfif>
	<cfif trim(variables.enddate) IS NOT "">opened <= #variables.enddate# and </cfif>
	0=0 
	order by opened asc
</cfquery>



<cfif getall.recordcount is 0>
	<div align="center">
	<table border="1" cellpadding="5" cellspacing="0">
		<tr>
			<td align="center" bgcolor="ffffcc"><b>I.T. After Hours Call Form</b></td>
		</tr>
		<tr>
			<td>There were no incidents that matched your search criteria</td>
		</tr>
		<tr>
			<td><a href="javascript:history.go(-1);">Return to Lookup</a></td>
		</tr>
	</table>
	</div>
	<cfabort>
</cfif>

<cfset incidentidlist = valuelist(getall.incidentid)>

<div class="normal" align="left">There are <cfoutput>#listlen(incidentidlist)#</cfoutput> items</div>

<cfloop list="#incidentidlist#" index="incidentid">

	<!--- get incident details --->
	<cfquery name="getdetails" datasource="#ds#">
		select callform_incidents.*, callform_systems.system
		from callform_incidents
		inner join callform_systems on callform_incidents.systemid = callform_systems.systemid
		where callform_incidents.incidentID = #incidentID#
	</cfquery>
	
	<cfquery name="gettechs" datasource="#ds#">
		select callform_incident_tech_lookup.*, admin_users.*
		from callform_incident_tech_lookup
		inner join admin_users on callform_incident_tech_lookup.techID = admin_users.adminuserid
		where incidentID = #incidentID#
		order by techNumber ASC
	</cfquery>
	
	<!--- get list of employees in IT and Tech Svcs Departments--->
	<cfquery name="getemps" datasource="#ds#">
		select distinct admin_users.*
		from admin_users
		inner join Admin_Users_Departments_Lookup on admin_users.adminuserid = Admin_Users_Departments_Lookup.adminuserid
		where Admin_Users_Departments_Lookup.departmentid IN (1,4)
		order by admin_users.lastname asc, admin_users.firstname asc
	</cfquery>
	
	<!--- <form method="post" action="view_incident.cfm"> --->
	<cfoutput>
	<!--- 
	<input type="hidden" name="incidentID" value="#incidentID#">
	<input type="hidden" name="technum" value="#gettechs.recordcount#">
	--->
	</cfoutput>
	
	<cfset dateTimeOpened = "#dateformat(getdetails.opened,'mm/dd/yyyy')# #timeformat(getdetails.opened,'hh:mm tt')#">
	<cfset issueDescription = getdetails.natureofproblem>
	<cfset callerComments = "">
	<cfset itComments = "">
	<cfset longTermResolution = "">
	
		
	<!--- =============================================== --->
	
	<div align="left">
	<table width="600" border="1" cellpadding="5" cellspacing="0">

		<tr>
			<td colspan="2" align="center" bgcolor="ffffcc"><b>I.T. After Hours Call Form</b></td>
		</tr>
		<cfoutput query="getdetails" group="incidentid">
		<tr>
			<td width="50%" valign="middle">
			<table border="0" cellpadding="3" cellspacing="0">
				<tr>
					<td><b>System Affected:</b></td>
					<td>
					#getdetails.system#
					</td>
				</tr>
			</table>
			</td>
			<td width="50%" valign="middle" align="center"><b>Time Ticket Opened: </b>#dateformat(getdetails.opened,'mm/dd/yyyy')# #timeformat(getdetails.opened,'hh:mm tt')#</td>
		</tr>
		<tr>
			<td colspan="2">
			<b>Nature of the problem:</b><br>
			#replace(getdetails.natureofproblem,"#chr(13)#","<br>","all")#
			</td>
		</tr>
		</cfoutput>
		<cfloop query="gettechs">
			<cfquery name="getsm" datasource="#ds#">
				select admin_users.firstname, admin_users.lastname
				from admin_users
				where adminuserid = #gettechs.managerID#
			</cfquery>
		<cfoutput>
		<!--- tech called section --->
		<tr bgcolor="dddddd">
			<td colspan="2"><b>Tech Called ###gettechs.currentrow#</b></td>
		</tr>
		<tr>
			<td>
			<table border="0" cellpadding="3" cellspacing="0">
				<tr>
					<td><b>Shift Manager Making Call :</b></td>
					<td>
					#getsm.firstname# #getsm.lastname#
					</td>
				</tr>
			</table>
			</td>
			<td>
			<table border="0" cellpadding="3" cellspacing="0">
				<tr>
					<td><b>Time Called :</b></td>
					<td>
					#timeformat(gettechs.called,'hh:mm tt')#
					</td>
				</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td><table border="0" cellpadding="3" cellspacing="0">
				<tr>
					<td><b>Tech Name:</b></td>
					<td>
					#gettechs.firstname# #gettechs.lastname#
					</td>
				</tr>
			</table>
			</td>
			<td>
			<table border="0" cellpadding="3" cellspacing="0">
				<tr>
					<td><b>Time Reached :</b></td>
					<td>
					#timeformat(gettechs.reached,'hh:mm tt')#
					</td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td colspan="2">
			<table border="0" cellpadding="3" cellspacing="0">
				<tr>
					<td><b>Result:</b></td>
					<td>#gettechs.callresult#<br>#gettechs.callresultother#</td>
				</tr>
			</table>
			</td>
		</tr>
		<!--- 
		<tr>
			<td colspan="2">
			<table border="0" cellpadding="3" cellspacing="0">
				<tr>
					<td><b>This Tech's Responsiveness Rating:</b> </td>
					<td>#gettechs.responserating#</td>
				</tr>
			</table>
			</td>
		</tr>
		 --->
		<tr>
			<td colspan="2">
			<b>Comments:</b><br>
			#replace(gettechs.comments,"#chr(13)#","<br>","all")#
			</td>
		</tr>
		<cfif isdefined("cookie.adminlogin")>
			<cfif (listfindnocase(departmentlist, 1) IS NOT 0) OR (listfindnocase(departmentlist, 4) IS NOT 0)>
			<tr>
				<td colspan="2"><b>I.T. Comments:</b></td>
			</tr>
			<tr>
				<td colspan="2">#gettechs.response#</td>
			</tr>
			<tr>
				<td colspan="2"><b>I.T. Resolution:</b></td>
			</tr>
			<tr>
				<td colspan="2">#gettechs.resolution#</td>
			</tr>
			</cfif>
		</cfif>
		<!--- tech called section --->
		</cfoutput>
		</cfloop>
		<tr>
			<td><!--- 
			<cfif isdefined("cookie.adminlogin")>
				<cfif (listfindnocase(departmentlist, 1) IS NOT 0) OR (listfindnocase(departmentlist, 4) IS NOT 0)>
					<input type="submit" name="btnSaveComments" value="Update Ticket with Reponses" class="normal">
				</cfif>
			</cfif>&nbsp; --->&nbsp;
			</td>
			<td valign="middle">
			<b>Time Ticket Closed:</b> 
			<cfif getdetails.opened is not "" and getdetails.whenclosed is not "">
				<cfoutput>#dateformat(getdetails.whenclosed,'mm/dd/yyyy')# #timeformat(getdetails.whenclosed,'hh:mm tt')#</cfoutput>
			<cfelse>
				This ticket is still open
			</cfif>
			&nbsp;<br>
			<cfif getdetails.opened is not "" and getdetails.whenclosed is not "">
			<b>Time Spent: </b>
			<cfoutput>
			#datediff("h",getdetails.opened,getdetails.whenclosed)# hours, 
			#evaluate(datediff("n",getdetails.opened,getdetails.whenclosed) MOD 60)# minutes, 
			#evaluate(datediff("s",getdetails.opened,getdetails.whenclosed) MOD 60)# seconds
			</cfoutput>
			</cfif>
			</td>
		</tr>
	</table>
	<cfif isdefined("cookie.adminlogin")>
	<br>
	<!--- <a class="normal" href="index.cfm">Return to Tickets</a> --->
	</cfif>
	</div>
	<!--- </form> --->
	<div id="pageBreakID"></div>
</cfloop>
