<link rel="stylesheet" type="text/css" href="../styles.css">

<cfif isDefined("form.btnSuggest")>

<!--- get email --->
<cfquery name="getemail" datasource="#ds#">
	select * from admin_users where adminuserid = #cookie.adminlogin#
</cfquery>

<cfmail from="#getemail.email#" to="pgregory@copsmonitoring.com" subject="COPS Monitoring Intranet Suggestion" username="copalink@copsmonitoring.com" password="copsmoncal">

Suggestion from : #getemail.firstname# #getemail.lastname#

Suggestion:
#form.suggestion#

</cfmail>

	<cfset msg = "Your suggestion has been sent.">

	<div align="center">
		<table width="500" border="0" cellpadding="5" cellspacing="0">
			<tr>
				<td colspan="2" align="center" bgcolor="FFFFCC" class="highlightbar"><b>Make A Suggestion </b></td>
			</tr>
			<cfoutput>
			<tr>
				<td colspan="2" align="center" class="greyrowbottom">#msg#</td>
			</tr>
			</cfoutput>
			<tr>
				<td align="center" colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td align="center" colspan="2"><a href="/index.cfm">Return to Main Menu</a></td>
			</tr>
		</table>
	</div>

<cfelse>

	<script language="JavaScript">
	<!--
	function checkform(frm) {
		if (frm.suggestion.value == "") {alert('You must enter a suggestion is required'); frm.newpw.focus(); return false;}
		return true;
	}
	//-->
	</script> 

	<div align="center">
		<table width="500" border="0" cellpadding="5" cellspacing="0">
			<tr>
				<td colspan="2" align="center" bgcolor="FFFFCC" class="highlightbar"><b>Make A Suggestion</b></td>
			</tr>
			<form method="post" action="suggest.cfm">
			<tr>
				<td class="greyrowbottomnopadding">
					<table width="100%" border="0" cellpadding="5" cellspacing="0" class="grey">
						<tr>
							<td>
								<textarea name="suggestion" style="width:490px" rows="10" class="normal"></textarea>
							</td>
						</tr>
						<tr>
							<td>
								<input onclick="return checkform(this.form);" name="btnSuggest" type="submit" class="sidebar" value="Submit Suggestion" />
								<input name="button" type="button" class="sidebar" onclick="history.go(-1);" value="Cancel" />
							</td>
						</tr>
					</table>
				</td>
			</tr>
			</form>			
			<tr>
				<td align="center" colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td align="center" colspan="2"><a href="/index.cfm">Return to Main Menu</a></td>
			</tr>
		</table>
	</div>

</cfif>
