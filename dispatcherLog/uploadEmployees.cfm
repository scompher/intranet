<link rel="stylesheet" type="text/css" href="../styles.css">
<cfif isDefined("form.btnProcessFile")>

	<cffile action="upload" filefield="employeeFile" destination="#request.directpath#\dispatcherLog\files\import.txt" nameconflict="overwrite">

	<cffile action="read" file="#request.directpath#\dispatcherLog\files\import.txt" variable="inFile"> 

	<cfloop list="#inFile#" delimiters="#chr(13)#" index="line">
		<cfset currItem = line>
		<cfif trim(line) is not "">
			<cfset i = findnocase(",",line)>
			<cfset empNum = left(line,i-1)>
			<cfset name = mid(line,i+1,(len(line)))>
			<cfset name = replace(name,"""","","all")>
			<cfif listlen(name) is 2>
				<cfset lname = trim(listgetat(name,1))>
				<cfset fname = trim(listgetat(name,2))>
			<cfelse>
				<cfset lname = "">
				<cfset name = name>
			</cfif>
			<!--- 
			<cfif name is not "NAME">
				<cfoutput>
				#empNum# = #name# = #fname# #lname#<br>
				</cfoutput>
			</cfif>
			--->
			<cfif name is not "NAME">
				<cfquery name="saveEmployee" datasource="#ds#">
					if not exists (select * from dispatcherLog_employees where employeeid = '#trim(empNum)#')
					begin
						insert into dispatcherLog_employees (employeeid, firstname, lastname) 
						values ('#trim(empNum)#', '#trim(fname)#', '#trim(lname)#')
					end
				</cfquery>
			</cfif>
		</cfif>
	</cfloop>
	
	<div align="center">
	<table border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td class="highlightbar"><b>Process employee files </b></td>
		</tr>
		<tr>
			<td class="greyrowbottomnopadding">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td nowrap="nowrap"><b>Employee File Processed Successfully</b></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<br />
	<a style="text-decoration:underline;" class="normal" href="/index.cfm">Return to Intranet</a>
	</div>

<cfelse>

<div align="center">
<form method="post" action="uploadEmployees.cfm" enctype="multipart/form-data">
<table border="0" cellspacing="0" cellpadding="5">
    <tr>
        <td class="highlightbar"><b>Process employee files </b></td>
    </tr>
    <tr>
        <td class="greyrowbottomnopadding">
            <table width="100%" border="0" cellspacing="0" cellpadding="5">
                <tr>
                    <td nowrap="nowrap"><b>Upload File:</b></td>
                    <td nowrap="nowrap">
                        <input type="file" name="employeeFile" style="width:300px; background-color:#FFFFFF; vertical-align:middle;" />
                    </td>
                    <td nowrap="nowrap">
                        <input type="submit" name="btnProcessFile" value="Process File" style="vertical-align:middle;" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</form>
</div>

</cfif>
