
<link rel="stylesheet" type="text/css" href="/styles.css">

<cfparam name="m" default="">
<cfparam name="employeeid" default="">
<cfparam name="name" default="">

<cfif isDefined("form.btnAddEmployee")>

	<cfquery name="checkExisting" datasource="#ds#">
		select * from dispatcherLog_employees 
		where employeeid = '#form.employeeid#'
	</cfquery>

	<cfif checkExisting.recordCount is 0>
		<cfset firstname = listgetat(form.name,1," ")>
		<cfset lastname = listgetat(form.name,2," ")>
		
		<cfquery name="saveEmployee" datasource="#ds#">
			insert into dispatcherLog_employees (employeeid, firstname, lastname) 
			values ('#form.employeeID#', '#firstname#', '#lastname#')
		</cfquery>
	
		<cfset m = "Employee #employeeid# #firstname# #lastname# Added Successfully">
		<cfset employeeID = "">
		<cfset name = "">
	<cfelse>
		<cfset m = "Employee ID #employeeid# already exists">
	</cfif>

</cfif>

<div align="center">
<table width="600" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Add Employee To Database </b></td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<cfif trim(m) is not "">
					<cfloop list="#m#" index="msg">
						<tr>
							<td colspan="2" class="alert">
							<cfoutput><b>#msg#</b></cfoutput>
							</td>
						</tr>
					</cfloop>
				</cfif>
				<form method="post" action="addEmployee.cfm">
				<tr>
					<td width="15%" align="right" nowrap><b>Employee #:</b></td>
					<td width="85%">
						<input name="employeeID" type="text" size="4" style="width:40px" value="<cfoutput>#employeeid#</cfoutput>">
					</td>
					</tr>
				<tr>
					<td align="right" nowrap><b>Name:</b></td>
					<td>
						<input type="text" name="name" style="width:200px" value="<cfoutput>#name#</cfoutput>">
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<input name="btnAddEmployee" type="submit" class="sidebar" value="Add Employee">
						<input type="button" onClick="document.location='addEmployee.cfm';" class="sidebar" value="Clear Form">
					</td>
				</tr>
				</form>
			</table>
		</td>
	</tr>
</table>
<br>
<a href="/index.cfm" style="text-decoration:underline;" class="normal">Return to Intranet</a>
</div>