
<cfquery name="getResults" datasource="#ds#">
	select dispatcherLog.*, dispatcherLog_employees.firstname, dispatcherLog_employees.lastname 
	from dispatcherLog 
	left join dispatcherLog_employees on dispatcherLog.employeeID = dispatcherLog_employees.employeeID 
	where dispatcherLog.employeeID not in (select distinct employeeID from dispatcherLog_employees) and left(dispatcherLog.employeeID,1) <> 'A' 
	ORDER BY 
		CASE WHEN ISNUMERIC(dispatcherLog.employeeID) = 1 THEN 0 ELSE 1 END ASC, 
		CASE WHEN ISNUMERIC(dispatcherLog.employeeID) = 1 THEN CAST(dispatcherLog.employeeID AS INT) ELSE 0 END ASC, 
		dispatcherLog.employeeID ASC 
</cfquery>

<link rel="stylesheet" type="text/css" href="/styles.css">
<div align="center">
	<table width="775" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td class="highlightbar"><b>Dispatcher Log Reporting - Unknown Employees</b></td>
		</tr>
		<tr>
			<td class="greyrowbottomnopadding">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td class="linedrowrightcolumn">&nbsp;</td>
						<td nowrap="nowrap" class="linedrowrightcolumn"><b>Employee # </b></td>
						<td nowrap="nowrap" class="linedrowrightcolumn"><b>Employee Name </b></td>
						<td class="linedrowrightcolumn"><b>Action</b></td>
						<td class="linedrowrightcolumn"><b>Port</b></td>
						<td class="linedrowrightcolumn"><b>Station</b></td>
						<td class="linedrowrightcolumn"><b>Date/Time</b></td>
						<td class="linedrowrightcolumn"><b>Location</b></td>
						<td class="linedrowrightcolumn"><b>User</b></td>
						<td class="linedrow"><b>Account</b></td>
					</tr>
					<cfoutput query="getResults">
					<cfif left(employeeID,1) is "A">
						<cfset getResults.firstname = "United">
						<cfset getResults.lastname = "Dispatcher">
					</cfif>
					<tr>
						<td class="linedrowrightcolumn">#getResults.currentRow#.</td>
						<td class="linedrowrightcolumn">#employeeID#</td>
						<td nowrap="nowrap" class="linedrowrightcolumn">#firstname#&nbsp;#lastname#</td>
						<td nowrap="nowrap" class="linedrowrightcolumn">#loggedAction#</td>
						<td class="linedrowrightcolumn">#port#</td>
						<td class="linedrowrightcolumn">#station#</td>
						<td nowrap="nowrap" class="linedrowrightcolumn">#dateformat(loggedDateTime,'mm/dd/yyyy')# #timeformat(loggedDateTime,'HH:mm:ss')#</td>
						<td nowrap="nowrap" class="linedrowrightcolumn">#location#</td>
						<td class="linedrowrightcolumn">#loggedUser#</td>
						<td class="linedrowrightcolumn">#loggedAcct#</td>
					</tr>
					<cfflush>
					</cfoutput>
				</table>
			</td>
		</tr>
	</table>
	<br>
	<a class="normal" style="text-decoration:underline" href="/index.cfm">Return to Intranet</a>
</div>