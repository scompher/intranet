<link rel="stylesheet" type="text/css" href="../styles.css">

<cfinclude template="functions.cfm">

<cfset maxrows = 100>

<cfparam name="p" default="1">
<cfparam name="pages" default="1">
<cfparam name="err" default="">
<cfparam name="urlString" default="">
<cfparam name="searchStartDate" default="#dateformat(now(),'mm/dd/yyyy')#">
<cfparam name="searchEndDate" default="">
<cfparam name="searchStartTimeHH" default="">
<cfparam name="searchStartTimeMM" default="">
<cfparam name="searchEndTimeHH" default="">
<cfparam name="searchEndTimeMM" default="">
<cfparam name="searchEmployeeID" default="">
<cfparam name="searchAction" default="">
<cfparam name="searchPort" default="">
<cfparam name="searchStation" default="">
<cfparam name="searchLocation" default="">
<cfparam name="searchUser" default="">
<cfparam name="searchAccount" default="">
<cfparam name="searchSortResults" default="loggedDateTime DESC">

<cfset start = (maxrows * p) - (maxrows - 1)>

<script language="javascript" src="cal2.js">
/*
Xin's Popup calendar script-  Xin Yang (http://www.yxscripts.com/)
Script featured on/available at http://www.dynamicdrive.com/
This notice must stay intact for use
*/
</script>
<script language="javascript" src="cal_conf2.js"></script>

<!--- change URL params to FORM params --->
<cfif isDefined("url.btnSearchNow")>
	<cfset urlString = "">
	<cfloop list="#cgi.QUERY_STRING#" delimiters="&" index="item">
		<cfset fieldName = listgetat(item,1,"=")>
		<cfif listlen(item,"=") is 2>
			<cfset fieldValue = listgetat(item,2,"=")>
		<cfelse>
			<cfset fieldValue = "">
		</cfif>
		<cfif fieldName is not "p">
			<cfset urlString = listappend(urlString,"#fieldName#=#fieldValue#","&")>
			<cfset formFieldName = "form.#fieldName#">
			<cfset "#formFieldName#" = "#urlDecode(fieldValue)#">
		</cfif>
	</cfloop>
</cfif>

<!--- build URL string --->
<cfif isDefined("form.fieldNames")>
	<cfset urlString = "">
	<cfloop list="#form.fieldNames#" index="item">
		<cfset urlString = listappend(urlString,"#item#=#evaluate(item)#","&")>
	</cfloop>
</cfif>

<cfif isDefined("form.btnSearchNow")>
	<cfset err = validateSearchForm(form)>
	<cfif trim(err) is "">
		<cfset getResults = searchLog(form)>
		<cfset pages = ceiling(getResults.recordCount / maxrows)>
	</cfif>
</cfif>

<cfquery name="getLocations" datasource="#ds#">
	select distinct location 
	from dispatcherLog 
	order by location asc 
</cfquery>

<cfquery name="getUsers" datasource="#ds#">
	select distinct loggedUser from dispatcherLog order by loggedUser ASC
</cfquery>

<cfquery name="getAccounts" datasource="#ds#">
	select distinct loggedAcct from dispatcherLog order by loggedAcct ASC 
</cfquery>

<br>
<div align="center">
<table width="750" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Dispatcher Log Search Form </b></td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
			<form method="post" action="index.cfm" name="searchForm">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td colspan="7" class="linedrow"><b>Search Items By</b> (Use * as a wildcard) </td>
				</tr>
				<cfif trim(err) is not "">
				<tr>
					<td class="linedrow">
					<cfloop list="#err#" index="e">
					<cfoutput>
					<span style="padding:5px" class="alert"><b>#e#</b></span><br />
					</cfoutput>
					</cfloop>
					</td>
				</tr>
				</cfif>
				<tr>
					<td colspan="7" class="linedrow"><b>Date/Time Range:</b> (time is in 24 hour format)</td>
				</tr>
				<tr>
					<td colspan="7" class="linedrow">
						<table border="0" cellspacing="0" cellpadding="5">
							<tr>
								<td>
								Starting Date: 
								<input type="text" name="searchStartDate" style="vertical-align:middle; width:75px" value="<cfoutput>#searchStartDate#</cfoutput>">
								<a style="text-decoration:none;" href="javascript:showCal('startingDate');"><img border="0" src="../images/calicon.gif" width="20" height="20" align="absmiddle"></a>
								</td>
								<td>&nbsp;</td>
								<td>
								Ending Date:
								<input type="text" name="searchEndDate" style="vertical-align:middle; width:75px" value="<cfoutput>#searchEndDate#</cfoutput>">
								<a style="text-decoration:none;" href="javascript:showCal('endingDate');"><img border="0" src="../images/calicon.gif" width="20" height="20" align="absmiddle"></a>
								</td>
							</tr>
							<tr>
								<td>
								Starting Time:
								<select name="searchStartTimeHH" style="vertical-align:middle;" onchange="if (this.form.searchStartTimeMM.selectedIndex <= 0) {this.form.searchStartTimeMM.selectedIndex = 1}">
										<option value=""></option>
									<cfloop from="0" to="23" index="HH">
										<cfoutput>
										<option <cfif searchStartTimeHH is numberformat(HH,00)>selected</cfif> value="#numberformat(HH,00)#">#numberformat(HH,00)#</option>
										</cfoutput>
									</cfloop>
								</select>
								:
								<select name="searchStartTimeMM" style="vertical-align:middle;">
										<option value=""></option>
									<cfloop from="0" to="59" index="MM">
										<cfoutput>
										<option <cfif searchStartTimeMM is numberformat(MM,00)>selected</cfif> value="#numberformat(MM,00)#">#numberformat(MM,00)#</option>
										</cfoutput>
									</cfloop>
								</select>
								</td>
								<td>&nbsp;</td>
								<td>
								Ending Time:
								<select name="searchEndTimeHH" style="vertical-align:middle;" onchange="if (this.form.searchEndTimeMM.selectedIndex <= 0) {this.form.searchEndTimeMM.selectedIndex = 1}">
										<option value=""></option>
									<cfloop from="0" to="23" index="HH">
										<cfoutput>
										<option <cfif searchEndTimeHH is numberformat(HH,00)>selected</cfif> value="#numberformat(HH,00)#">#numberformat(HH,00)#</option>
										</cfoutput>
									</cfloop>
								</select>
								:
								<select name="searchEndTimeMM" style="vertical-align:middle;">
										<option value=""></option>
									<cfloop from="0" to="59" index="MM">
										<cfoutput>
										<option <cfif searchEndTimeMM is numberformat(MM,00)>selected</cfif> value="#numberformat(MM,00)#">#numberformat(MM,00)#</option>
										</cfoutput>
									</cfloop>
								</select>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="7" class="nopadding">
						<table width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr>
								<td class="linedrowrightcolumn"><b>Employee #:</b></td>
								<td class="linedrowrightcolumn"><b>Action:</b></td>
								<td class="linedrowrightcolumn"><b>Port:</b></td>
								<td class="linedrowrightcolumn"><b>Station:</b></td>
								<td class="linedrowrightcolumn"><b>Location:</b></td>
								<td class="linedrowrightcolumn"><b>User:</b></td>
								<td class="linedrow"><b>Account:</b></td>
							</tr>
							<tr>
								<td class="linedrowrightcolumn">
									<input type="text" name="searchEmployeeID" style="width:50px" maxlength="10" value="<cfoutput>#searchEmployeeID#</cfoutput>">
								</td>
								<td class="linedrowrightcolumn">
									<select name="searchAction" style="vertical-align:middle">
										<option value=""></option>
										<option <cfif searchAction is "LOGGED IN">selected</cfif> value="LOGGED IN">LOGGED IN</option>
										<option <cfif searchAction is "LOGGED OUT">selected</cfif> value="LOGGED OUT">LOGGED OUT</option>
									</select>
								</td>
								<td class="linedrowrightcolumn">
									<input type="text" name="searchPort" style="width:50px" maxlength="10" value="<cfoutput>#searchPort#</cfoutput>">
								</td>
								<td class="linedrowrightcolumn">
									<input type="text" name="searchStation" style="width:50px" maxlength="10" value="<cfoutput>#searchStation#</cfoutput>">
								</td>
								<td class="linedrowrightcolumn">
									<select name="searchLocation" style="vertical-align:middle">
										<option value=""></option>
										<cfoutput query="getLocations">
											<option <cfif searchLocation is getLocations.location>selected</cfif> value="#getlocations.location#">#getlocations.location#</option>
										</cfoutput>
									</select>
								</td>
								<td class="linedrowrightcolumn">
									<select name="searchUser" style="vertical-align:middle">
										<option value=""></option>
										<cfoutput query="getUsers">
											<option <cfif searchUser is getUsers.loggedUser>selected</cfif> value="#getUsers.loggedUser#">#getUsers.loggedUser#</option>
										</cfoutput>
									</select>
								</td>
								<td class="linedrow">
									<select name="searchAccount" style="vertical-align:middle">
										<option value=""></option>
										<cfoutput query="getAccounts">
											<option <cfif searchAccount is getAccounts.loggedAcct>selected</cfif> value="#getAccounts.loggedAcct#">#getAccounts.loggedAcct#</option>
										</cfoutput>
									</select>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="linedrow" colspan="7">
					<b>Sort Results: </b>
					</td>
				</tr>
				<tr>
					<td class="linedrow" colspan="7">
					<select name="searchSortResults">
						<option value=""></option>
						<option <cfif searchSortResults is "loggedDateTime ASC">selected</cfif> value="loggedDateTime ASC">Date/Time Oldest On Top</option>
						<option <cfif searchSortResults is "loggedDateTime DESC">selected</cfif> value="loggedDateTime DESC">Date/Time Most Recent On Top</option>
						<option <cfif searchSortResults is "dispatcherLog.employeeID ASC">selected</cfif> value="dispatcherLog.employeeID ASC">Employee Number Low to High</option>
						<option <cfif searchSortResults is "dispatcherLog.employeeID DESC">selected</cfif> value="dispatcherLog.employeeID DESC">Employee Number High to Low</option>
						<option <cfif searchSortResults is "port ASC">selected</cfif> value="port ASC">Port Low to High</option>
						<option <cfif searchSortResults is "port DESC">selected</cfif> value="port DESC">Port High to Low</option>
						<option <cfif searchSortResults is "station ASC">selected</cfif> value="station ASC">Station Low to High</option>
						<option <cfif searchSortResults is "station DESC">selected</cfif> value="station DESC">Station High to Low</option>
						<option <cfif searchSortResults is "location ASC">selected</cfif> value="location ASC">Location Low to High</option>
						<option <cfif searchSortResults is "location DESC">selected</cfif> value="location DESC">Location High to Low</option>
						<option <cfif searchSortResults is "loggedUser ASC">selected</cfif> value="loggedUser ASC">User Low to High</option>
						<option <cfif searchSortResults is "loggedUser DESC">selected</cfif> value="loggedUser DESC">User High to Low</option>
						<option <cfif searchSortResults is "loggedAcct ASC">selected</cfif> value="loggedAcct ASC">Account Low to High</option>
						<option <cfif searchSortResults is "loggedAcct DESC">selected</cfif> value="loggedAcct DESC">Account High to Low</option>
					</select>
					</td>
				</tr>
				<tr>
					<td colspan="7">
						<input name="btnSearchNow" type="submit" class="sidebar" value="Search Now">
						<input type="button" onClick="document.location = 'index.cfm';" class="sidebar" value="New Search">
					</td>
				</tr>
			</table>
			</form>
		</td>
	</tr>
</table>
<br>
<a style="text-decoration:underline" class="normal" href="/index.cfm">Return to Intranet</a>
<br />
<br>
<cfif isDefined("getResults")>
	<cfif getResults.recordcount is 0>
	<span class="alert"><b>Sorry, there are no items which match your search criteria</b></span>
	<br>
	<cfelse>
	<span class="normal">
	<b style="font-size:18px">Search Results</b>
	<br /><br />
	<cfoutput>#numberFormat(getResults.recordcount)# items found</cfoutput>
	<br /><br />
	<cfset end = start + maxrows - 1>
	<cfif end gt getResults.recordcount>
		<cfset end = getResults.recordcount>
	</cfif>
	<cfoutput>
	Displaying items #start# - #end#
	</cfoutput>
	<br />
	</span>
	<br />
	<cfif pages gt 1>
	<table width="775" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td>
			Page 
			<cfloop from="1" to="#pages#" index="page">
			<cfoutput>
			<cfif page is p>
				<span style="font-size:14px; font-weight:bold">#page#</span>
			<cfelse>
				<a style="text-decoration:underline" href="index.cfm?p=#page#&#urlString#">#page#</a>
			</cfif>
			<cfif page is not pages> | </cfif>
			</cfoutput>
			</cfloop>
			</td>
		</tr>
	</table>
	</cfif>
	<table width="775" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td class="highlightbar"><b>Dispatcher Log Results </b></td>
		</tr>
		<tr>
			<td class="greyrowbottomnopadding">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td class="linedrowrightcolumn">&nbsp;</td>
						<td nowrap="nowrap" class="linedrowrightcolumn"><b>Employee # </b></td>
						<td nowrap="nowrap" class="linedrowrightcolumn"><b>Employee Name </b></td>
						<td class="linedrowrightcolumn"><b>Action</b></td>
						<td class="linedrowrightcolumn"><b>Port</b></td>
						<td class="linedrowrightcolumn"><b>Station</b></td>
						<td class="linedrowrightcolumn"><b>Date/Time</b></td>
						<td class="linedrowrightcolumn"><b>Location</b></td>
						<td class="linedrowrightcolumn"><b>User</b></td>
						<td class="linedrow"><b>Account</b></td>
					</tr>
					<cfoutput query="getResults" startrow="#start#" maxrows="#maxrows#">
					<cfif left(employeeID,1) is "A">
						<cfset getResults.firstname = "United">
						<cfset getResults.lastname = "Dispatcher">
					</cfif>
					<tr>
						<td class="linedrowrightcolumn">#getResults.currentRow#.</td>
						<td class="linedrowrightcolumn">#employeeID#</td>
						<td nowrap="nowrap" class="linedrowrightcolumn">#firstname#&nbsp;#lastname#</td>
						<td nowrap="nowrap" class="linedrowrightcolumn">#loggedAction#</td>
						<td class="linedrowrightcolumn">#port#</td>
						<td class="linedrowrightcolumn">#station#</td>
						<td nowrap="nowrap" class="linedrowrightcolumn">#dateformat(loggedDateTime,'mm/dd/yyyy')# #timeformat(loggedDateTime,'HH:mm:ss')#</td>
						<td nowrap="nowrap" class="linedrowrightcolumn">#location#</td>
						<td class="linedrowrightcolumn">#loggedUser#</td>
						<td class="linedrowrightcolumn">#loggedAcct#</td>
					</tr>
					</cfoutput>
				</table>
			</td>
		</tr>
	</table>
	<cfif pages gt 1>
	<table width="775" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td>
			Page 
			<cfloop from="1" to="#pages#" index="page">
			<cfoutput>
			<cfif page is p>
				<span style="font-size:14px; font-weight:bold">#page#</span>
			<cfelse>
				<a style="text-decoration:underline" href="index.cfm?p=#page#&#urlString#">#page#</a>
			</cfif>
			<cfif page is not pages> | </cfif>
			</cfoutput>
			</cfloop>
			</td>
		</tr>
	</table>
	</cfif>
	</cfif>
</cfif>
</div>
