
<cffunction name="searchLog" access="private" returntype="query">
	<cfargument name="form" type="struct" required="yes">
	
	<cfset startDateTime = "">
	<cfif trim(form.searchStartDate) is not "">
		<cfset startDateTime = "#form.searchStartDate#">
	</cfif>
	<cfif trim(form.searchStartTimeHH) is not "" and trim(form.searchStartTimeMM) is not "">
		<cfset startDateTime = startDateTime & "#form.searchStartTimeHH#:#form.searchStartTimeMM#">
	</cfif>
	
	<cfset endDateTime = "">
	<cfif trim(form.searchEndDate) is not "">
		<cfset endDateTime = "#form.searchEndDate#">
	</cfif>
	<cfif trim(form.searchEndTimeHH) is not "" and trim(form.searchEndTimeMM) is not "">
		<cfset endDateTime = endDateTime & "#form.searchEndTimeHH#:#form.searchEndTimeMM#">
	</cfif>
	
	<cfquery name="getItems" datasource="#ds#">
		select dispatcherLog.*, dispatcherLog_employees.firstname, dispatcherLog_employees.lastname 
		from dispatcherLog 
		left join dispatcherLog_employees on dispatcherLog.employeeID = dispatcherLog_employees.employeeID 
		where 
		<cfif trim(form.searchEmployeeID) is not "">
			dispatcherLog.employeeID like '#replace(form.searchEmployeeID, "*", "%", "all")#' and 
		</cfif>
		<cfif trim(form.searchAction) is not "">
			loggedAction like '#replace(form.searchAction, "*", "%", "all")#' and 
		</cfif>
		<cfif trim(form.searchPort) is not "">
			port like '#replace(form.searchPort, "*", "%", "all")#' and 
		</cfif>
		<cfif trim(form.searchStation) is not "">
			station like '#replace(form.searchStation, "*", "%", "all")#' and 
		</cfif>
		<cfif trim(form.searchLocation) is not "">
			location like '#replace(form.searchLocation, "*", "%", "all")#' and 
		</cfif>
		<cfif trim(form.searchUser) is not "">
			loggedUser like '#replace(form.searchUser, "*", "%", "all")#' and 
		</cfif>
		<cfif trim(form.searchAccount) is not "">
			loggedAcct like '#replace(form.searchAccount, "*", "%", "all")#' and 
		</cfif>
		<cfif trim(startDateTime) is not "">
			loggedDateTime >= #createodbcdatetime(startDateTime)# and 
		</cfif>
		<cfif trim(endDateTime) is not "">
			loggedDateTime <= #createodbcdatetime(endDateTime)# and 
		</cfif>
		1=1
		<cfif trim(form.searchSortResults) is not "">
			<cfif form.searchSortResults is "dispatcherLog.employeeID ASC">
				ORDER BY 
					CASE WHEN ISNUMERIC(dispatcherLog.employeeID) = 1 THEN 0 ELSE 1 END ASC, 
					CASE WHEN ISNUMERIC(dispatcherLog.employeeID) = 1 THEN CAST(dispatcherLog.employeeID AS INT) ELSE 0 END ASC, 
					dispatcherLog.employeeID ASC 
			<cfelseif form.searchSortResults is "dispatcherLog.employeeID DESC">
				ORDER BY 
					CASE WHEN ISNUMERIC(dispatcherLog.employeeID) = 1 THEN 0 ELSE 1 END DESC, 
					CASE WHEN ISNUMERIC(dispatcherLog.employeeID) = 1 THEN CAST(dispatcherLog.employeeID AS INT) ELSE 0 END DESC, 
					dispatcherLog.employeeID DESC
			<cfelse>
				ORDER BY #form.searchSortResults#
			</cfif>
		</cfif>
	</cfquery>

	<cfreturn getItems>
</cffunction>

<cffunction name="validateSearchForm" access="private" returntype="string">
	<cfargument type="struct" name="form" required="yes">
	<cfset err = "">
	
	<cfif trim(form.searchStartDate) is not "">
		<cfif not isDate(form.searchStartDate)><cfset err = listappend(err,"The starting date must be a valid date.")></cfif>
	</cfif>
	<cfif trim(form.searchEndDate) is not "">
		<cfif not isDate(form.searchEndDate)><cfset err = listappend(err,"The ending date must be a valid date.")></cfif>
	</cfif>
	<cfif (trim(form.searchStartTimeHH) is not "") or (trim(form.searchStartTimeMM) is not "")>
		<cfif trim(form.searchStartDate) is "">
			<cfset form.searchStartDate = dateformat(now(),'mm/dd/yyyy')>
		</cfif>
		<cfset dtVal = "#form.searchStartDate# #form.searchStartTimeHH#:#form.searchStartTimeMM#">
		<cfif not isDate(dtVal)>
			<cfset err = listappend(err,"The starting date or time value is invalid.")>
		</cfif>
	</cfif>

	<cfif (trim(form.searchEndTimeHH) is not "") or (trim(form.searchEndTimeMM) is not "")>
		<cfif trim(form.searchEndDate) is "">
			<cfset form.searchEndDate = dateformat(now(),'mm/dd/yyyy')>
		</cfif>
		<cfset dtVal = "#form.searchEndDate# #form.searchEndTimeHH#:#form.searchEndTimeMM#">
		<cfif not isDate(dtVal)>
			<cfset err = listappend(err,"The ending date or time value is invalid.")>
		</cfif>
	</cfif>
	
	<cfreturn err>
</cffunction>














