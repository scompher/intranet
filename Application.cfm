<cfapplication name="Cops Monitoring" clientmanagement="NO" sessionmanagement="yes">

<!--- stuff to prevent caching of pages --->
<!--- Client side cache prevention --->
<meta http-equiv="Expires" content="0">

<!--- Setup our expire times for Netscape and Internet Explorer --->
<cfoutput>
        <!--- Internet Explorer Date Formate: (Fri, 30 Oct 1998 14:19:41 GMT) --->
        <cfset MSIEtimestamp='#dateformat(now(),"DDD")#,#dateformat(now(),"DD")#
		#dateformat(now(),"Mmm")# #timeformat(now(),"HH:MM:SS")#'>

        <!--- Netscape Date Formate: Netscape (Wednesday, Apr 26 2000 17:45:25 PM) --->
        <cfset NETSCAPEtimestamp='#dateformat(now(),"DDDD")#,#dateformat(now(),"MMM")#
		#dateformat(now(),"dd")# #dateformat(now(),"YYY")#
		#timeformat(now(),"HH:MM:SS tt")#'>
</cfoutput>

<!--- Tell HTTP Header to force expire of page - nocache --->
<cfif HTTP_USER_AGENT contains "MSIE">
        <cfheader name="Expires" value="<cfoutput>#MSIEtimestamp#</cfoutput>">
        <cfheader name="Pragma" value="no-cache">
        <cfheader name="cache-control" value="no-cache, no-store, must-revalidate">
<cfelse>
        <cfheader name="Expires" value="<cfoutput>#NETSCAPEtimestamp#</cfoutput>">
        <cfheader name="Pragma" value="no-cache">
        <cfheader name="cache-control" value="no-cache, no-store, must-revalidate">
</cfif>
<!--- stuff to prevent caching of pages --->

<cfif cgi.HTTP_HOST is "intranet.lydia.com"><cflocation url="http://192.168.1.10"></cfif>


<!--- <cferror type="exception" template="errorMsg.cfm" mailto="pgregory@copsmonitoring.com"> --->
<!--- <cferror type="REQUEST" template="errorMsg.cfm" mailto="pgregory@copsmonitoring.com"> --->

<cfinclude template="/mappings.cfm">

<cfset skiplogin = false>

<cfset exceptionlist = "view.cfm,overdueTaskNotify.cfm,view_incident.cfm,dealerdiary,scheduled,getReport.cfm,reports,images,smdr,changeLog,newTicket.cfm,errorMsg.cfm,general_extensions_list.cfm,attachrecording.cfm,playrecording.cfm,suggestionbox">

<cfloop index="page" list="#exceptionlist#">
	<cfif listfindnocase(cf_template_path,page,"\") IS NOT 0>
		<cfset skiplogin = true>
	</cfif>
</cfloop>

<cfif skiplogin is false>
	<cfif NOT isDefined("cookie.adminlogin")>
		<cflocation url="/login/index.cfm">
	</cfif>
	<cfinclude template="auth.cfm">
</cfif>

<cfset suppressTitleList = "confessionalMonitor.cfm,showCam.cfm,confessionalWindow">
<cfset supressTitle = false>
<cfloop list="#suppressTitleList#" index="theItem">
	<cfif findnocase("#theItem#",cgi.PATH_TRANSLATED) is 0>
		<cfset supressTitle = true>
	</cfif>
</cfloop>

<cfif supressTitle is false>
	<title>COPS Monitoring Intranet</title>
</cfif>
