
<cfsetting showdebugoutput="no">

<link rel="stylesheet" type="text/css" href="/styles.css">

<style type="text/css">
<!--
.largeText {
	font-size: 14px;
	font-weight: bold;
}
-->
</style>

<cfquery name="getDetails" datasource="changeLog">
	select * from changeLog 
	where changeLogID = #id# 
</cfquery>

<body leftmargin="0" topmargin="0" rightmargin="0" bottommargin="0">
<cfoutput query="getDetails">
<table width="540" border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td style="padding:0px" colspan="2">
		<a href="javascript:window.print();"></a>
		<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td><a href="javascript:window.print();"><img src="/images/printerfriendlyicon.gif" alt="Print This Page" border="0" align="absmiddle" />&nbsp;<b>Print this page</b></a></td>
				<td align="center"><a href="javascript:window.close();">Close This Window</a> <a href="javascript:window.close();"><img src="/images/closeWin.gif" alt="Close Window" width="21" height="21" border="0" align="absmiddle" /></a></td>
			</tr>
		</table>
		</td>
	</tr>
	<tr><td colspan="2">&nbsp;</td></tr>
	<tr>
		<td colspan="2" class="linedrow"><span class="largeText">Change Log Detail</span></td>
	</tr>
	<tr>
		<td colspan="2" class="nopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td width="30%"><b>Date/Time:</b></td>
					<td width="70%">#dateformat(changeDateTime,'mm/dd/yyyy')# #timeformat(changeDateTime,'HH:mm:ss')#</td>
				</tr>
				<tr>
					<td><b>Department:</b></td>
					<td>
					<cfswitch expression="#process#">
						<cfcase value="CAL">COP-A-LINK</cfcase>
						<cfcase value="DE">Data Entry</cfcase>
						<cfcase value="CF">Change Form</cfcase>
						<cfcase value="IT">Technology</cfcase>
					</cfswitch>
					</td>
				</tr>
				<tr>
					<td><b>Employee Number: </b></td>
					<td>
					<cfif trim(process) is not "CAL">
					#employeeID#
					</cfif>&nbsp;
					</td>
				</tr>
				<tr>
					<td><b>Port Number:</b> </td>
					<td>#employeePortNumber#</td>
				</tr>
				<tr>
					<td><b>Change Type: </b></td>
					<td>#changeType#</td>
				</tr>
				<tr>
					<td><b>Account Number: </b></td>
					<td>#accountNumber#</td>
				</tr>
				<tr>
					<td><b>Dealer Number: </b></td>
					<td>#dealerNumber#</td>
				</tr>
				<tr>
					<td><b>Dealer Passcode Used: </b></td>
					<td>#passwordUsed#</td>
				</tr>
				<tr>
					<td><b>Item Changed: </b></td>
					<td>#attributeDescription#</td>
				</tr>
				<tr>
					<td valign="top"><b>Values Before Change: </b></td>
					<td valign="top">#replace(fullBeforeChange, chr(253), ", ", "all")#</td>
				</tr>
				<tr>
					<td valign="top"><b>Values After Change: </b></td>
					<td valign="top">#replace(fullAfterChange, chr(253), ", ", "all")#</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</cfoutput>
</body>
