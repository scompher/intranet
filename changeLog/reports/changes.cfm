<cfsetting showdebugoutput="yes" requesttimeout="3000000">

<cfset pdfFileName = "DailyChangeLogReport.pdf">
<cfset pdfFileLocation = "#request.directpath#\changeLog\reports">
<cffunction name="getBasicInfo" access="private" returntype="string">
	<cfargument name="accountNumber">
	<cfargument name="dn">
	<cfargument name="pc">
	
	<!--- get basic info --->
	<cfxml variable="xmlRequest">
	<cfoutput>
	<request>
		<dealerNumber>#xmlformat(dn)#</dealerNumber>
		<dealerPasscode>#xmlformat(pc)#</dealerPasscode>
		<command>getBasicInfo</command>
		<data>
			<accountNumber>#xmlformat(accountNumber)#</accountNumber>
		</data>
	</request>
	</cfoutput>
	</cfxml>

	<cf_copalink command="#xmlRequest#">

	<cfreturn result>	
</cffunction>

<cffunction name="getCodeZoneInfo" access="private" returntype="string">
	<cfargument name="accountNumber">
	<cfargument name="dn">
	<cfargument name="pc">
	
	<cfxml variable="xmlRequest">
	<cfoutput>
	<request>
		<dealerNumber>#dn#</dealerNumber>
		<dealerpasscode>#pc#</dealerpasscode>
		<command>getCodeZoneInfo</command>
		<data>
			<accountnumber>#accountNumber#</accountnumber>
			<showTemplates>N</showTemplates>
		</data>
	</request>
	</cfoutput>
	</cfxml>
	
	<cf_copalink command="#xmlRequest#">
	
	<cfreturn result>
</cffunction>

<cffunction name="checkServiceCode" access="private" returntype="boolean">
	<cfargument name="accountNumber">
	<cfargument name="dn">
	<cfargument name="pc">
	
	<cfxml variable="xmlRequest">
	<cfoutput>
	<request>
		<dealerNumber>#dn#</dealerNumber>
		<dealerPasscode>#pc#</dealerPasscode>
		<command>getServiceCodeReport</command>
		<data>
			<accountNumber>#accountNumber#</accountNumber>
		</data>	
	</request>
	</cfoutput>
	</cfxml>
	
	<cf_copalink command="#xmlRequest#">

	<cfset xResult = xmlparse(result)>

	<cfif xResult.response.result.xmltext is "RJ">
		<cfset result = false>
	<cfelse>
		<cfx_xml2query sourceXML="#result#" queryName="getCodes">
		
		<cfquery name="checkCode" dbtype="query">
			select * from getCodes 
			where description = 'Two-Way Voice Service'
		</cfquery>
		
		<cfif checkCode.recordcount gt 0><cfset result = true><cfelse><cfset result = false></cfif>
	</cfif>	
	<cfreturn result>
</cffunction>

<cfset yesterday = dateadd("d",-1,now())>
<cfset today = now()>

<cfquery name="getchanges" datasource="changeLog">
	select *  
	from changeLog with (nolock) 
	where 
	(changeDateTime >= #createOdbcDate(yesterday)# and changeDateTime < #createOdbcDate(today)#) and 
	process = 'CAL' and 
	changeType = 'CHG' and 
	attributeDescription IN ('Building use','Template','Code','Zone handling')
	and dealernumber <> 'COMC' 
	and dealernumber <> '8372'
	order by changeDateTime, dealernumber, accountNumber, attributeDescription 
</cfquery>

<cfset currAN = "">

<cfsavecontent variable="reportOutput">
<style type="text/css">
.linedrow {border-bottom:#D7D7D7 solid 1px; vertical-align:middle}
</style>

<br />

<cfset reportDate = dateadd("d",-1,now())>

<div align="center">
<span style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:16px; color:#000000; font-weight:bold;">COP-A-LINK Change Report <cfoutput>#dateformat(reportDate,'mm/dd/yyyy')#</cfoutput></span>
<br />
<br />
<br />
<br />
<table width="100%" border="0" cellpadding="5" cellspacing="0" style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">
	<tr>
		<td class="linedrow">&nbsp;</td>
		<td class="linedrow"><b>Date/Time</b></td>
		<td class="linedrow"><b>Dealer #</b></td>
		<td class="linedrow"><b>Passcode Used</b></td>
		<td class="linedrow"><b>Acct #</b></td>
		<td class="linedrow"><b>Item Changed</b></td>
	</tr>
	<cfset lineNumber = 0>
	<cfloop query="getChanges">
		<cfif getchanges.accountNumber is not currAN>
			<cfset currAN = getchanges.accountNumber>
			<cfset templateFlag = false>
			<cfset codeZoneFlag = false>
			<cfset showFlag = false>
			<!--- 
			if change is to code, perform following checks:
			Show if account has no template, or no codes/zones 
			Show if account has service code 42 (two way) on it
			--->
			<cfif attributeDescription is "Code"> 
				<cfset D3Flag = true>
				<cfset result = getBasicInfo(currAN,getchanges.dealerNumber, getchanges.passwordUsed)>
				<cfset xResult = xmlparse(result)>
				<cfif xResult.response.result.xmltext is not "RJ">
					<cfset bi = xResult.response.data.basicInformation>
					<cfset template = bi.templateNumber.xmlText>
					<cfif trim(template) is ""><cfset templateFlag = true></cfif>
					<cfset result = getCodeZoneInfo(currAN,getchanges.dealerNumber, getchanges.passwordUsed)>
					<cfset xResult = xmlparse(result)>
					<cfif isDefined('xResult.response.data.codeZoneInformation.codeHandling')>
					<cfset codes = xResult.response.data.codeZoneInformation.codeHandling>
					</cfif>
					<cfif isDefined('xResult.response.data.codeZoneInformation.zoneHandling')>
					<cfset zones = xResult.response.data.codeZoneInformation.zoneHandling>
					</cfif>
					<cfif isDefined('xResult.response.data.codeZoneInformation.specialHandling')>
					<cfset codeZones = xResult.response.data.codeZoneInformation.specialHandling>
					</cfif>
					<cfif not isDefined("codes.item") and not isDefined("zones.item") and not isDefined("codeZones.item")>
						<cfset showFlag = true>
					</cfif>
					<cfif showFlag is false>
						<cfif checkServiceCode(currAN,getchanges.dealerNumber, getchanges.passwordUsed) is true>
							<cfset showFlag = true>
						</cfif>
					</cfif>
				<cfelse>
					<cfset showFlag = false>
				</cfif>
			<cfelse>
				<cfset showFlag = true>
			</cfif>
		</cfif>
		<cfset dateTime = dateformat(changeDateTime,'mm/dd/yyyy') & " " & timeformat(changeDateTime,"HH:mm:ss")>
		<cfif showFlag is true>
			<cfset lineNumber = lineNumber + 1>
			<cfset bgc = "##FFFFFF">
			<cfoutput>
			<tr bgcolor="#bgc#">
				<td class="linedrow">#lineNumber#.</td>
				<td class="linedrow">#dateTime#</td>
				<td class="linedrow">#dealerNumber#</td>
				<td class="linedrow">#passwordUsed#</td>
				<td class="linedrow">#currAN#</td>
				<td class="linedrow">#attributeDescription#</td>
			</tr>
			<cfset D3Flag = false>
			</cfoutput>
		</cfif>
	</cfloop>
</table>
</div>
</cfsavecontent>

<cfdocument format="pdf" scale="100" filename="#pdfFileName#" overwrite="yes">
<cfoutput>#reportOutput#</cfoutput>
</cfdocument>

<!--- <a href="<cfoutput>#pdfFileName#</cfoutput>">download pdf</a> --->

<cfset tolist = "dataentry@copsmonitoring.com">

<cfmail from="pgregory@copsmonitoring.com" to="#tolist#" subject="Daily COP-A-LINK Change Report for #dateformat(reportDate,'mm/dd/yyyy')#" username="copalink@copsmonitoring.com" password="copsmoncal">
<cfmailparam file="#pdfFileLocation#\#pdfFileName#">
</cfmail>
