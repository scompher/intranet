<link rel="stylesheet" type="text/css" href="/styles.css">

<!--- down msg
<div align="center">
<table width="600" border="0" cellpadding="5" cellspacing="0" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold; color:#FFFFFF; border:solid thin #CC0000; background-color:#FF0000; padding:5px">
	<tr>
		<td>IMPORTANT NOTICE</td>
	</tr>
	<cfoutput>
	<tr>
		<td>
		The change log search will be down temporarily while we perform database maintenance.  Expected time back online is 2pm eastern time. 
		</td>
	</tr>
	</cfoutput>
</table>
</div>
<br />
<cfabort>
 --->
<!--- down msg --->

<script language="javascript" src="cal2.js">
/*
Xin's Popup calendar script-  Xin Yang (http://www.yxscripts.com/)
Script featured on/available at http://www.dynamicdrive.com/
This notice must stay intact for use
*/
</script>
<script language="javascript" src="cal_conf2.js"></script>

<script type="text/javascript" language="javascript">
function OpenDetailWin(id) {
	var windowWidth = 565;
	var windowHeight = 470;
	var left =
	 (screen.width / 2) - (windowWidth / 2);
	var top = (screen.height / 2) - (windowHeight/ 2) - 150;
	DetailWin = window.open("viewDetails.cfm?id=" + id,"DetailWin","top=" + top + ",left=" + left + ",height=" + windowHeight + ",width=" + windowWidth + ",resizable=1,status=0,titlebar=0,toolbar=0,scrollbars=1");
}
</script>

<cfinclude template="functions.cfm">

<cfset maxrows = 100>
<cfparam name="p" default="1">
<cfparam name="pages" default="1">
<cfparam name="urlString" default="">
<cfparam name="form.searchStartDate" default="">
<cfparam name="form.searchEndDate" default="">
<cfparam name="form.searchStartTimeHH" default="">
<cfparam name="form.searchStartTimeMM" default="">
<cfparam name="form.searchEndTimeHH" default="">
<cfparam name="form.searchEndTimeMM" default="">
<cfparam name="form.accountNumber" default="">
<cfparam name="form.dealerNumber" default="">
<cfparam name="form.dealerPasscode" default="">
<cfparam name="form.employeeID" default="">
<cfparam name="form.typeOfChange" default="">
<cfparam name="form.process" default="">
<cfparam name="err" default="">

<cfif isDefined("form.btnPrev")>
	<cfset p = p - 1>
</cfif>

<cfif isDefined("form.btnNext")>
	<cfset p = p + 1>
</cfif>

<cfset start = (maxrows * p) - (maxrows - 1)>

<!--- change URL params to FORM params --->
<cfif isDefined("url.btnSearchNow")>
	<cfset urlString = "">
	<cfloop list="#cgi.QUERY_STRING#" delimiters="&" index="item">
		<cfset fieldName = listgetat(item,1,"=")>
		<cfif listlen(item,"=") is 2>
			<cfset fieldValue = listgetat(item,2,"=")>
		<cfelse>
			<cfset fieldValue = "">
		</cfif>
		<cfif fieldName is not "p">
			<cfset urlString = listappend(urlString,"#fieldName#=#fieldValue#","&")>
			<cfset formFieldName = "form.#fieldName#">
			<cfset "#formFieldName#" = "#urlDecode(fieldValue)#">
		</cfif>
	</cfloop>
</cfif>

<!--- build URL string --->
<cfif isDefined("form.fieldNames")>
	<cfset urlString = "">
	<cfloop list="#form.fieldNames#" index="item">
		<cfset urlString = listappend(urlString,"#item#=#evaluate(item)#","&")>
	</cfloop>
</cfif>

<cfif isDefined("form.btnSearchNow")>

	<cfset err = validateSearchForm(form)>
	<cfif trim(err) is "">
		<!---<cfset getResults = searchChangeLog(form,"search")>--->
		<cfset searchType = "search">
		<cfset attributeBlockList = "'Time change','Operator','Date Changed'">
	
		<cfset startDateTime = "">
		<cfif trim(form.searchStartDate) is not "">
			<cfset startDateTime = "#form.searchStartDate#">
		</cfif>
		<cfif trim(form.searchStartTimeHH) is not "" and trim(form.searchStartTimeMM) is not "">
			<cfif trim(form.searchStartDate) is "">
				<cfset startDateTime = "#dateformat(now(),'mm/dd/yyyy')#" & "#form.searchStartTimeHH#:#form.searchStartTimeMM#">
			<cfelse>
				<cfset startDateTime = startDateTime & " #form.searchStartTimeHH#:#form.searchStartTimeMM#">
			</cfif>
		</cfif>
		
		<cfset endDateTime = "">
		<cfif trim(form.searchEndDate) is not "">
			<cfset endDateTime = "#form.searchEndDate#">
			<cfif trim(form.searchEndTimeHH) is not "" and trim(form.searchEndTimeMM) is not "">
				<cfif trim(form.searchEndDate) is "">
					<cfset endDateTime = "#dateformat(now(),'mm/dd/yyyy')#" & "#form.searchEndTimeHH#:#form.searchEndTimeMM#">
				<cfelse>
					<cfset endDateTime = endDateTime & "#form.searchEndTimeHH#:#form.searchEndTimeMM#">
				</cfif>
			<cfelse>
				<cfif form.searchEndDate is form.searchStartDate and form.searchStartDate is not "">
					<cfset endDateTime = "#form.searchStartDate# 23:59:59">
				<cfelse>
					<cfset endDateTime = "#form.searchEndDate# 23:59:59">
				</cfif>
			</cfif>
		</cfif>
				
		<cfif searchType is "download">
			<cfset selectList = "process, changeDatetime, employeeID, passwordUsed, changeType, fileName, accountNumber, attributeNumber, attributeDescription, fullBeforeChange, fullAfterChange, singleBeforeChange, singleAfterChange, processID, employeePortNumber, dealerNumber ">
		<cfelse>
			<cfset selectList = "changeLogID, process, changeDatetime, employeeID, changeType, accountNumber, attributeDescription, dealerNumber, passwordUsed">
		</cfif>
		
		<cfquery name="getRecordCount" datasource="changeLog">
			SELECT count(changeLogID) as totalRows 
			from dbo.changeLog with (nolock) 
			where 
			<cfif trim(startDateTime) is not "">
				changeDateTime >= #createodbcdatetime(startDateTime)# and 
			</cfif>
			<cfif trim(endDateTime) is not "">
				changeDateTime <= #createodbcdatetime(endDateTime)# and 
			</cfif>
			<cfif trim(form.employeeID) is not "">
				employeeID LIKE '#replace(form.employeeID, "*", "%", "all")#' and 
			</cfif>
			<cfif trim(form.dealerNumber) is not "">
				dealerNumber LIKE '#replace(form.dealerNumber, "*", "%", "all")#' and 
			</cfif>
			<cfif trim(form.dealerPasscode) is not "">
				passwordUsed LIKE '#replace(form.dealerPasscode, "*", "%", "all")#' and 
			</cfif>
			<cfif trim(form.typeOfChange) is not "">
				changeType = '#form.typeOfChange#' and 
			</cfif>
			<cfif trim(form.accountNumber) is not "">
				accountNumber LIKE '#replace(form.accountNumber, "*", "%", "all")#' and 
			</cfif>
			<cfif trim(form.process) is not "">
				process = '#form.process#' and 
			</cfif>
			attributeDescription NOT IN (#preserveSingleQuotes(attributeBlockList)#) and 
			<!--- accountNumber not in (select accountNumber from changeLog where attributeDescription = 'Oper orig') and  --->
			1=1
		</cfquery>
		
		<cfquery name="getResults" datasource="changeLog">
			WITH resultSet AS
			(
				SELECT #selectList#, ROW_NUMBER() OVER (ORDER BY changeDateTime) AS 'RowNumber' 
				from dbo.changeLog with (nolock) 
				where 
				<cfif trim(startDateTime) is not "">
					changeDateTime >= #createodbcdatetime(startDateTime)# and 
				</cfif>
				<cfif trim(endDateTime) is not "">
					changeDateTime <= #createodbcdatetime(endDateTime)# and 
				</cfif>
				<cfif trim(form.employeeID) is not "">
					employeeID LIKE '#replace(form.employeeID, "*", "%", "all")#' and 
				</cfif>
				<cfif trim(form.dealerNumber) is not "">
					dealerNumber LIKE '#replace(form.dealerNumber, "*", "%", "all")#' and 
				</cfif>
				<cfif trim(form.dealerPasscode) is not "">
					passwordUsed LIKE '#replace(form.dealerPasscode, "*", "%", "all")#' and 
				</cfif>
				<cfif trim(form.typeOfChange) is not "">
					changeType = '#form.typeOfChange#' and 
				</cfif>
				<cfif trim(form.accountNumber) is not "">
					accountNumber LIKE '#replace(form.accountNumber, "*", "%", "all")#' and 
				</cfif>
				<cfif trim(form.process) is not "">
					process = '#form.process#' and 
				</cfif>
				attributeDescription NOT IN (#preserveSingleQuotes(attributeBlockList)#) and 
				<!--- accountNumber not in (select accountNumber from changeLog where attributeDescription = 'Oper orig') and  --->
				1=1
			) 
			SELECT top #maxrows# * 
			FROM resultSet 
			WHERE RowNumber >= #start# 
			order by changeDateTime asc 
		</cfquery>
		<!---<cfset pages = ceiling(getResults.recordCount / maxrows)>--->
		<cfset pages = ceiling(getRecordCount.totalRows / maxrows)>
	</cfif>
</cfif>

<cfif isDefined("btnExcelDownload")>
	<cfset err = validateSearchForm(form)>
	<cfif trim(err) is "">
		<cfset getResults = searchChangeLog(form,"download")>
		<cfset colList = getResults.columnList>
		<cfif isdefined("cookie.adminlogin")>
			<cfset outputfilename = "#cookie.adminlogin##TimeFormat(now(),'hhmmss')#.xls">
		<cfelse>
			<cfset outputfilename = "#TimeFormat(now(),'hhmmss')#.xls">
		</cfif>
		<cfset outputfilepath = "#request.DirectPath#\changeLog\downloads">
		<cfset downloadURL = "/changeLog/downloads">
		<cfx_query2excel 
			file="#outputfilepath#\#outputfilename#" 
			headings="#colList#" 
			queryFields="#colList#" 
			format="excel" 
			query="#getResults#">
		
		<cfheader name="content-disposition" value="attachment;filename=#outputfilename#">
		<cfcontent type="application/unknown" file="#outputfilepath#\#outputfilename#">  
		<cfset x = structClear(variables)>
		<cfabort>
	</cfif>
</cfif>

<div align="center">
<table border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>COP-A-LINK Change Log </b></td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<cfif trim(err) is not "">
				<cfloop list="#err#" index="e">
				<tr>
					<td colspan="2" class="alert"><b><cfoutput>#e#</cfoutput></b></td>
				</tr>
				</cfloop>
				</cfif>
				<cfform method="post" action="index.cfm" name="searchForm">
				<tr>
					<td colspan="2"><b>Search Items By</b> (Use * as a wildcard)</td>
				</tr>
				<tr>
					<td colspan="2"><b>Date/Time Range: </b></td>
				</tr>
				<tr>
					<td colspan="2" class="nopadding">
						<table border="0" cellspacing="0" cellpadding="5">
							<tr>
								<td>Starting Date:
									<cfinput validate="date" message="The date is invalid" type="text" name="searchStartDate" style="vertical-align:middle; width:75px" value="#searchStartDate#" />
									<a style="text-decoration:none;" href="javascript:showCal('startingDate');"><img border="0" src="../images/calicon.gif" width="20" height="20" align="absmiddle" /></a> </td>
								<td>&nbsp;&nbsp;</td>
								<td> Ending Date:
									<cfinput validate="date" message="The date is invalid" type="text" name="searchEndDate" style="vertical-align:middle; width:75px" value="#searchEndDate#" />
									<a style="text-decoration:none;" href="javascript:showCal('endingDate');"><img border="0" src="../images/calicon.gif" width="20" height="20" align="absmiddle" /></a> </td>
							</tr>
							<tr>
								<td> Starting Time:
									<select name="searchStartTimeHH" style="vertical-align:middle;">
											<option value=""></option>
											<cfloop from="0" to="23" index="HH">
												<cfoutput>
													<option <cfif searchStartTimeHH is numberformat(HH,00)>selected</cfif> value="#numberformat(HH,00)#">#numberformat(HH,00)#</option>
												</cfoutput>
											</cfloop>
										</select>
									:
									<select name="searchStartTimeMM" style="vertical-align:middle;">
										<option value=""></option>
										<cfloop from="0" to="59" index="MM">
											<cfoutput>
												<option <cfif searchStartTimeMM is numberformat(MM,00)>selected</cfif> value="#numberformat(MM,00)#">#numberformat(MM,00)#</option>
											</cfoutput>
										</cfloop>
									</select>
								</td>
								<td>&nbsp;</td>
								<td> Ending Time:
									<select name="searchEndTimeHH" style="vertical-align:middle;">
											<option value=""></option>
											<cfloop from="0" to="23" index="HH">
												<cfoutput>
													<option <cfif searchEndTimeHH is numberformat(HH,00)>selected</cfif> value="#numberformat(HH,00)#">#numberformat(HH,00)#</option>
												</cfoutput>
											</cfloop>
										</select>
									:
									<select name="searchEndTimeMM" style="vertical-align:middle;">
										<option value=""></option>
										<cfloop from="0" to="59" index="MM">
											<cfoutput>
												<option <cfif searchEndTimeMM is numberformat(MM,00)>selected</cfif> value="#numberformat(MM,00)#">#numberformat(MM,00)#</option>
											</cfoutput>
										</cfloop>
									</select>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td nowrap="nowrap"><b>Account Number: </b></td>
					<td>
					<input type="text" name="accountNumber" style="vertical-align:middle; width:90px" maxlength="12" value="<cfoutput>#form.accountNumber#</cfoutput>" /> 
					</td>
				</tr>
				<tr>
					<td width="13%" nowrap="nowrap"><b>Dealer Number: </b><span class="smallred">*</span></td>
					<td width="87%">
						<input type="text" name="dealerNumber" value="<cfoutput>#form.dealerNumber#</cfoutput>" /> (required) 
					</td>
				</tr>
				<tr>
					<td width="13%" nowrap="nowrap"><b>Dealer Passcode: </b></td>
					<td width="87%">
						<input type="text" name="dealerPasscode" value="<cfoutput>#form.dealerPasscode#</cfoutput>" />
					</td>
				</tr>
				<tr>
					<td nowrap="nowrap"><b>Employee ID: </b></td>
					<td>
						<input type="text" name="employeeID" value="<cfoutput>#form.employeeID#</cfoutput>" />
					</td>
				</tr>
				<tr>
					<td nowrap="nowrap"><b>Department: </b></td>
					<td>
						<select name="process">
							<option value=""></option>
							<option <cfif form.process is "CAL">selected</cfif> value="CAL">COP-A-LINK</option>
							<option <cfif form.process is "DE">selected</cfif> value="DE">Data Entry</option>
							<option <cfif form.process is "CF">selected</cfif> value="CF">Change Form</option>
							<option <cfif form.process is "IT">selected</cfif> value="IT">Technology</option>
						</select>
					</td>
				</tr>
				<tr>
					<td nowrap="nowrap"><b>Type of Change:</b> </td>
					<td>
						<select name="typeOfChange">
							<option value=""></option>
							<option <cfif form.typeOfChange is "ADD">selected</cfif> value="ADD">Addition</option>
							<option <cfif form.typeOfChange is "CHG">selected</cfif> value="CHG">Change</option>
							<option <cfif form.typeOfChange is "DEL">selected</cfif> value="DEL">Deletion</option>
						</select>
					</td>
				</tr>
				<tr>
					<td colspan="2" nowrap="nowrap">
						<input name="btnSearchNow" type="submit" class="sidebar" value="Search Now" onclick="this.value = 'Please wait...';" />
						<input type="button" class="sidebar" value="Clear Form" onclick="document.location='index.cfm';" />
						<input type="submit" name="btnExcelDownload" class="sidebar" value="Download Results" onclick="this.value = 'Please wait...';" />
					</td>
				</tr>
				</cfform>
			</table>
		</td>
	</tr>
</table>
<cfif isDefined("cookie.adminlogin")>
<br />
<a style="text-decoration:underline" class="normal" href="/index.cfm">Return to Intranet</a>
</cfif>
<br />
<br />

<cfif isDefined("getResults") and trim(err) is "">
	<cfif getResults.recordcount is 0>
		<span class="alert"><b>Sorry, there are no items which match your search criteria</b></span>
		<br>
	<cfelse>
		<span class="normal">
		<b style="font-size:18px">Search Results</b>
		<br /><br />
		<cfoutput>#numberFormat(getRecordCount.totalRows)# items found</cfoutput>
		<br /><br />
		<cfset end = start + maxrows - 1>
		<cfif end gt getRecordCount.totalRows>
			<cfset end = getRecordCount.totalRows>
		</cfif>
		<cfoutput>
		Displaying items #start# - #end#
		</cfoutput>
		<br />
		</span>
		<br />
		<cfif pages gt 1>
		<!---
		<table width="750" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td>
				Page 
				<cfloop from="1" to="#pages#" index="page">
				<cfoutput>
				<cfif page is p>
					<span style="font-size:14px; font-weight:bold">#page#</span>
				<cfelse>
					<a style="text-decoration:underline" href="index.cfm?p=#page#&#urlString#">#page#</a>
				</cfif>
				<cfif page is not pages> | </cfif>
				</cfoutput>
				</cfloop>
				</td>
			</tr>
		</table>
		--->
		<form method="post" action="index.cfm">
		<cfif isDefined("form.fieldNames")>
			<cfloop list="#form.fieldNames#" index="item">
				<cfoutput>
				<cfif item is not "p" and item is not "btnPrev" and item is not "btnNext" and item is not "btnGo">
				<input type="hidden" name="#item#" value="#evaluate(item)#" />
				</cfif>
				</cfoutput>
			</cfloop>
		</cfif>
		<table width="750" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td>
				Jump to Page 
				<input style="width:45px; vertical-align:middle;" type="text" name="p" value="<cfoutput>#p#</cfoutput>" /> 
				 of <cfoutput>#numberformat(pages)#</cfoutput>&nbsp;
				<input name="btnGo" type="submit" class="sidebarsmall" style="vertical-align:middle;" value="Go" /> 
				<cfif p is not 1>
				<input name="btnPrev" type="submit" class="sidebarsmall" style="vertical-align:middle;" value="Prev Page" /> 
				</cfif>
				<input name="btnNext" type="submit" class="sidebarsmall" style="vertical-align:middle;" value="Next Page" /> 
				</td>
			</tr>
		</table>
		</form>
		</cfif>
		<table width="800" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td class="highlightbar"><b>Change Log</b></td>
			</tr>
			<tr>
				<td class="greyrowbottomnopadding">
					<table width="100%" border="0" cellspacing="0" cellpadding="5">
						<tr>
							<td valign="bottom" class="linedrow">&nbsp;</td>
							<td valign="bottom" class="linedrow">&nbsp;</td>
							<td valign="bottom" class="linedrow"><b>Date/Time</b></td>
							<td valign="bottom" class="linedrow"><b>Dept</b></td>
							<td valign="bottom" class="linedrow"><b>Employee #</b></td>
							<td valign="bottom" class="linedrow"><b>Type</b></td>
							<td valign="bottom" class="linedrow"><b>Dealer #</b></td>
							<td valign="bottom" class="linedrow"><b>PW Used</b></td>
							<td valign="bottom" class="linedrow"><b>Account #</b></td>
							<td valign="bottom" class="linedrow"><b>Item Changed</b></td>
						</tr>
						<cfset rc = start>
						<cfoutput query="getResults">
						<cfif getResults.currentRow mod 2 is 0><cfset bgc = "##EEEEEE"><cfelse><cfset bgc = "##FFFFFF"></cfif>
						<tr bgcolor="#bgc#">
							<td class="linedrow">#rc#.</td>
							<td class="linedrow">
							<a href="javascript:OpenDetailWin(#changeLogID#);"><img src="/images/manifyingglass.gif" border="0" alt="View Details" /></a>
							</td>
							<td class="linedrow" nowrap="nowrap">#dateformat(changeDateTime,'mm/dd/yyyy')# #timeformat(changeDateTime,'HH:mm:ss')#&nbsp;</td>
							<td class="linedrow">
							<cfswitch expression="#process#">
								<cfcase value="CAL">COP-A-LINK</cfcase>
								<cfcase value="DE">Data Entry</cfcase>
								<cfcase value="CF">Change Form</cfcase>
								<cfcase value="IT">Technology</cfcase>
							</cfswitch>
							&nbsp;
							</td>
							<td class="linedrow">
							<cfif trim(process) is not "CAL">
								#employeeID#
							</cfif>
							&nbsp;</td>
							<td class="linedrow">#changeType#&nbsp;</td>
							<td class="linedrow">#dealerNumber#&nbsp;</td>
							<td class="linedrow">#passwordUsed#&nbsp;</td>
							<td class="linedrow">#accountNumber#&nbsp;</td>
							<td class="linedrow">#attributeDescription#&nbsp;</td>
						</tr>
						<cfset rc = rc + 1>
						</cfoutput>
					</table>
				</td>
			</tr>
		</table>
		<cfif pages gt 1>
		<!---
		<table width="750" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td>
				Page 
				<cfloop from="1" to="#pages#" index="page">
				<cfoutput>
				<cfif page is p>
					<span style="font-size:14px; font-weight:bold">#page#</span>
				<cfelse>
					<a style="text-decoration:underline" href="index.cfm?p=#page#&#urlString#">#page#</a>
				</cfif>
				<cfif page is not pages> | </cfif>
				</cfoutput>
				</cfloop>
				</td>
			</tr>
		</table>
		--->
		</cfif>
	</cfif>
</cfif>

</div>