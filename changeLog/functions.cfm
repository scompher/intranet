
<!--- function : validateSearchForm --->
<cffunction name="validateSearchForm" access="private" returntype="string" output="no">
	<cfargument name="form" type="struct" required="yes">
	<cfset err = "">

	<cfset sd = form.searchStartDate>
	<cfset ed = form.searchEndDate>

	<cfif trim(form.searchStartDate) is "">
		<cfset err = listappend(err,"You must enter a starting date.")>
	<cfelse>
		<cfif trim(ed) is "">
			<cfset ed = dateformat(now(),"mm/dd/yyyy")>
		</cfif>
		<cfif datediff("d",sd,ed) gt 365>
			<cfset err = listappend(err,"You cannot search more than 1 year from start date to end date.")>
		</cfif>	
	</cfif>

	<cfif isDefined("form.btnExcelDownload")>
		<cfif trim(form.dealerNumber) is "" and trim(form.accountNumber) is "" and trim(form.employeeID) is "" and trim(form.typeOfChange) is "" and trim(form.process) is "">
			<cfset err = listappend(err,"You must enter another field along with the starting date.")>
		</cfif>
	</cfif>
	
	<cfif isDefined("form.dealerNumber")>
		<cfif trim(form.dealerNumber) is "" or trim(form.dealerNumber) is "*">
			<cfset err = listappend(err, "The dealer number is required.")>
		</cfif>
	</cfif>
	
	<cfreturn err>
</cffunction>
<!--- function : validateSearchForm --->

<!--- function : searchChangeLog --->
<cffunction name="searchChangeLog" access="private" returntype="query" output="no">
	<cfargument name="form" type="struct" required="yes">
	<cfargument name="searchType" type="string" required="yes">

	<cfset attributeBlockList = "'Time change','Operator','Date Changed'">

	<cfset startDateTime = "">
	<cfif trim(form.searchStartDate) is not "">
		<cfset startDateTime = "#form.searchStartDate#">
	</cfif>
	<cfif trim(form.searchStartTimeHH) is not "" and trim(form.searchStartTimeMM) is not "">
		<cfif trim(form.searchStartDate) is "">
			<cfset startDateTime = "#dateformat(now(),'mm/dd/yyyy')#" & "#form.searchStartTimeHH#:#form.searchStartTimeMM#">
		<cfelse>
			<cfset startDateTime = startDateTime & "#form.searchStartTimeHH#:#form.searchStartTimeMM#">
		</cfif>
	</cfif>
	
	<cfset endDateTime = "">
	<cfif trim(form.searchEndDate) is not "">
		<cfset endDateTime = "#dateformat(form.searchEndDate,'mm/dd/yyyy')#">
		<cfif trim(form.searchEndTimeHH) is not "" and trim(form.searchEndTimeMM) is not "">
			<cfif trim(form.searchEndDate) is "">
				<cfset endDateTime = "#dateformat(now(),'mm/dd/yyyy')#" & "#form.searchEndTimeHH#:#form.searchEndTimeMM#">
			<cfelse>
				<cfset endDateTime = endDateTime & "#form.searchEndTimeHH#:#form.searchEndTimeMM#">
			</cfif>
		<cfelse>
			<cfif form.searchEndDate is form.searchStartDate and form.searchStartDate is not "">
				<cfset endDateTime = "#form.searchStartDate# 23:59:59">
			<cfelse>
				<cfset endDateTime = "#form.searchEndDate# 23:59:59">
			</cfif>
		</cfif>
	</cfif>
	
	<cfif searchType is "download">
		<cfset selectList = "process, changeDatetime, employeeID, passwordUsed, changeType, fileName, accountNumber, attributeNumber, attributeDescription, fullBeforeChange, fullAfterChange, singleBeforeChange, singleAfterChange, processID, employeePortNumber, dealerNumber ">
	<cfelse>
		<cfset selectList = "changeLogID, process, changeDatetime, employeeID, changeType, accountNumber, attributeDescription, dealerNumber, passwordUsed">
	</cfif>
	
	<cfquery name="getItems" datasource="changeLog">
		select #selectList#
		from dbo.changeLog with (nolock) 
		where 
		<cfif trim(startDateTime) is not "">
			changeDateTime >= #createodbcdatetime(startDateTime)# and 
		</cfif>
		<cfif trim(endDateTime) is not "">
			changeDateTime <= #createodbcdatetime(endDateTime)# and 
		</cfif>
		<cfif trim(form.employeeID) is not "">
			employeeID LIKE '#replace(form.employeeID, "*", "%", "all")#' and 
		</cfif>
		<cfif trim(form.dealerNumber) is not "">
			dealerNumber LIKE '#replace(form.dealerNumber, "*", "%", "all")#' and 
		</cfif>
		<cfif trim(form.dealerPasscode) is not "">
			passwordUsed LIKE '#replace(form.dealerPasscode, "*", "%", "all")#' and 
		</cfif>
		<cfif trim(form.typeOfChange) is not "">
			changeType = '#form.typeOfChange#' and 
		</cfif>
		<cfif trim(form.accountNumber) is not "">
			accountNumber LIKE '#replace(form.accountNumber, "*", "%", "all")#' and 
		</cfif>
		<cfif trim(form.process) is not "">
			process = '#form.process#' and 
		</cfif>
		attributeDescription NOT IN (#preserveSingleQuotes(attributeBlockList)#) and 
		<!--- accountNumber not in (select accountNumber from changeLog where attributeDescription = 'Oper orig') and  --->
		1=1
		order by changeDateTime asc 
	</cfquery>

	<cfreturn getItems>
</cffunction>
<!--- function : searchChangeLog --->

