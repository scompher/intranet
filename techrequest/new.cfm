<link rel="stylesheet" type="text/css" href="/styles.css">

<cfinclude template="functions.cfm">

<cfparam name="form.status" default="Unread">
<cfparam name="form.dateSubmitted" default="#now()#">
<cfparam name="form.dateRequested" default="">
<cfparam name="form.customerOrDivision" default="">
<cfparam name="form.personRequesting" default="">
<cfparam name="form.personRequestingIs" default="">
<cfparam name="form.requestDetails" default="">
<cfparam name="form.requestTypeID" default="0">
<cfparam name="form.requestApprovedBy" default="">
<cfparam name="form.dateCompleted" default="">
<cfparam name="form.requestCompletedBy" default="">
<cfparam name="err" default="">
<cfparam name="fieldErrors" default="">
<cfparam name="form.fileAttachments" default="">
<cfparam name="form.fileAttachment" default="">
<cfparam name="form.removeAttachmentValue" default="">

<cfif trim(form.fileAttachment) is not "">
	<cfset form.btnAttach = 1>
</cfif>

<cfif isdefined("form.btnAttach") and form.fileAttachment is not "">
	<cffile action="upload" filefield="fileAttachment" destination="#request.DirectPath#\techrequest\attachments\" nameconflict="makeunique">
	<cfset savedFile = file.ServerFile>
	<cfset uploadedFile = file.ClientFile>
	<cfset newSavedFile = replace(savedFile,'##','','all')>
	<cffile action="rename" source="#request.DirectPath#\techrequest\attachments\#savedFile#" destination="#request.DirectPath#\techrequest\attachments\#newSavedFile#">
	<cfset savedFile = newSavedFile>
	<cfset form.fileAttachments = listappend(form.fileAttachments, "#savedFile#:#uploadedFile#")>
</cfif>

<cfif form.removeAttachmentValue is not "">
	<cfset serverFile = listgetat(form.removeAttachmentValue,1,":")>
	<cfset clientFile = listgetat(form.removeAttachmentValue,2,":")>
	<cfif fileExists("#request.DirectPath#\techrequest\attachments\#serverFile#")>
		<cffile action="delete" file="#request.DirectPath#\techrequest\attachments\#serverFile#">
	</cfif>
	<cfset form.fileAttachments = listdeleteat(form.fileAttachments,listfindnocase(form.fileAttachments,form.removeAttachmentValue))>
</cfif>

<cfif isDefined("form.btnSubmitRequest")>
	<!--- validate data --->
	<cfset err = validateForm(form)>

	<cfif trim(err) is "">
		<!--- save data --->
		<cfif saveRequest(form)>
			<cflocation url="index.cfm">		
		</cfif>
	</cfif>
</cfif>

<script type="text/javascript" language="javascript">
function showhide(layer_ref, state){ 
	if(document.all){ //IS IE 4 or 5 (or 6 beta) 
		eval("document.all." +layer_ref+ ".style.display = state"); 
	} 
	if (document.layers) { //IS NETSCAPE 4 or below 
		document.layers[layer_ref].display = state; 
	} 
	if (document.getElementById &&!document.all) { 
		hza = document.getElementById(layer_ref); 
		hza.style.display = state; 
	} 
}
function removeAttachment(serverFile, clientFile) {
	document.mainform.removeAttachmentValue.value = serverFile + ':' + clientFile;
	document.mainform.submit();
}
</script>

<script language="javascript" src="cal2.js">
/*
Xin's Popup calendar script-  Xin Yang (http://www.yxscripts.com/)
Script featured on/available at http://www.dynamicdrive.com/
This notice must stay intact for use
*/
</script>
<script language="javascript" src="cal_conf2.js"></script>

<cfquery name="getRequestTypes" datasource="#ds#">
	select * from techRequest_requestTypes
	order by orderID asc, requestType asc
</cfquery>

<div align="center">
<table width="600" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Technology Request Form</b> </td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<form method="post" action="new.cfm" name="mainform" enctype="multipart/form-data">
			<input type="hidden" name="fileAttachments" value="<cfoutput>#form.fileAttachments#</cfoutput>">
			<input type="hidden" name="removeAttachmentValue" value="">
			<input type="hidden" name="status" value="<cfoutput>#form.status#</cfoutput>" />
				<cfif trim(err) is not "">
					<cfloop list="#err#" index="e">
						<cfset fieldErrors = listappend(fieldErrors,listgetat(e,1,":"))>
						<cfset m = listgetat(e,2,":")>
						<tr>
							<td style="color:#FF0000; font-weight:bold;"><cfoutput>#m#</cfoutput></td>
						</tr>
					</cfloop>
				</cfif>
				<tr>
					<td><b>Please fill in all fields completely </b></td>
				</tr>
				<tr>
					<td class="nopadding">
						<table width="100%" border="0" cellspacing="0" cellpadding="5">
						<tr>
							<td>Attach File(s): (optional)</td>
							<td style="padding:0px">
								<table border="0" cellspacing="0" cellpadding="5" class="grey">
									<tr>
										<td>
											<input type="file" name="fileAttachment">
										</td>
										<td>
											<input name="btnAttach" type="submit" class="sidebar" value="Attach File">
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td valign="top">Current Attatchments: </td>
							<td style="padding:0px">
							<cfif fileAttachments is not "">
								<table border="0" cellpadding="5" cellspacing="0" class="grey">
									<cfloop list="#fileAttachments#" index="theFile">
									<cfoutput>
									<cfset serverFile = listgetat(theFile,1,":")>
									<cfset fileName = listgetat(theFile,2,":")>
									<tr>
										<td><a target="_blank" href="/techrequest/attachments/#serverFile#" style="text-decoration:underline;">#fileName#</a></td>
										<td><a href="javascript:removeAttachment('#serverFile#','#fileName#');" style="text-decoration:underline;">[remove]</a></td>
									</tr>
									</cfoutput>
									</cfloop>
								</table>
							<cfelse>
								<table border="0" cellpadding="5" cellspacing="0" class="grey">
									<tr>
										<td colspan="2">None</td>
									</tr>
								</table>
							</cfif>
							&nbsp;	
							</td>
						</tr>
							<tr>
								<td <cfif listfind(fieldErrors,"customerOrDivision")>bgcolor="#FF7777"</cfif> width="32%" nowrap><b>Dealer Number:</b></td>
								<td <cfif listfind(fieldErrors,"customerOrDivision")>bgcolor="#FF7777"</cfif> width="68%">
									<input type="text" name="customerOrDivision" style="width:390px" value="<cfoutput>#customerOrDivision#</cfoutput>">
								</td>
							</tr>
							<tr>
								<td <cfif listfind(fieldErrors,"dateRequested")>bgcolor="#FF7777"</cfif> nowrap><b>Date Requested: </b></td>
								<td <cfif listfind(fieldErrors,"dateRequested")>bgcolor="#FF7777"</cfif> class="nopadding">
									<table border="0" cellpadding="5" cellspacing="0" class="grey">
										<tr>
											<td <cfif listfind(fieldErrors,"dateRequested")>bgcolor="#FF7777"</cfif> >
												<input name="dateRequested" type="text" style="width:75px" value="<cfoutput>#dateRequested#</cfoutput>" maxlength="10" />
											</td>
											<td <cfif listfind(fieldErrors,"dateRequested")>bgcolor="#FF7777"</cfif> ><a style="text-decoration:none;" href="javascript:showCal('DateRequested');"> <img src="../images/calicon.gif" alt="Select a Date" align="absmiddle" width="20" height="20" border="0" /> </a></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td <cfif listfind(fieldErrors,"personRequesting")>bgcolor="#FF7777"</cfif> nowrap><b>Requestor: </b></td>
								<td <cfif listfind(fieldErrors,"personRequesting")>bgcolor="#FF7777"</cfif> >
									<input type="text" name="personRequesting" style="width:390px" value="<cfoutput>#personRequesting#</cfoutput>">
								</td>
							</tr>
							<tr>
								<td <cfif listfind(fieldErrors,"personRequestingIs")>bgcolor="#FF7777"</cfif> valign="top" nowrap>&nbsp;</td>
								<td <cfif listfind(fieldErrors,"personRequestingIs")>bgcolor="#FF7777"</cfif> class="nopadding">
									<table border="0" cellspacing="0" cellpadding="5">
										<tr>
											<td <cfif listfind(fieldErrors,"personRequestingIs")>bgcolor="#FF7777"</cfif> valign="middle">
												<input type="radio" <cfif form.personRequestingIs is "Dealer">checked</cfif> name="personRequestingIs" value="Dealer">
											</td>
											<td <cfif listfind(fieldErrors,"personRequestingIs")>bgcolor="#FF7777"</cfif> valign="middle" style="padding-left:0px">
												Dealer											</td>
											<td <cfif listfind(fieldErrors,"personRequestingIs")>bgcolor="#FF7777"</cfif> valign="middle">&nbsp;</td>
											<td <cfif listfind(fieldErrors,"personRequestingIs")>bgcolor="#FF7777"</cfif>valign="middle">
												<input type="radio" <cfif personRequestingIs is "In-House">checked</cfif> name="personRequestingIs" value="In-House">
											</td>
											<td <cfif listfind(fieldErrors,"personRequestingIs")>bgcolor="#FF7777"</cfif> valign="middle" style="padding-left:0px">
												In-House											</td>
										</tr>
									</table>
								</td>
							</tr>
							
							<tr>
								<td <cfif listfind(fieldErrors,"requestTypeID")>bgcolor="#FF7777"</cfif> valign="top" nowrap="nowrap"><b>Technology Request: </b></td>
								<td <cfif listfind(fieldErrors,"requestTypeID")>bgcolor="#FF7777"</cfif> >
									<select name="requestTypeID" style="width:390px">
										<option value="0"></option>
										<cfoutput query="getrequestTypes">
											<option <cfif form.requestTypeID is getrequestTypes.requestTypeID>selected</cfif> value="#getrequestTypes.requestTypeID#">#getrequestTypes.requestType#</option>
										</cfoutput>
									</select>
								</td>
							</tr>
							<tr>
								<td <cfif listfind(fieldErrors,"requestDetails")>bgcolor="#FF7777"</cfif> valign="top" nowrap><b>request Details: </b></td>
								<td <cfif listfind(fieldErrors,"requestDetails")>bgcolor="#FF7777"</cfif> >
								<textarea name="requestDetails" style="width:390px" rows="5"><cfoutput>#requestDetails#</cfoutput></textarea>
								</td>
							</tr>
							<tr>
								<td <cfif listfind(fieldErrors,"requestApprovedBy")>bgcolor="#FF7777"</cfif> nowrap><b>request Approved By: </b></td>
								<td <cfif listfind(fieldErrors,"requestApprovedBy")>bgcolor="#FF7777"</cfif> >
									<input type="text" name="requestApprovedBy" style="width:390px" value="<cfoutput>#requestApprovedBy#</cfoutput>">
								</td>
							</tr>
							<cfif form.status is not "Unread">
							<tr>
								<td <cfif listfind(fieldErrors,"requestCompletedBy")>bgcolor="#FF7777"</cfif> nowrap><b>request Completed By: </b></td>
								<td <cfif listfind(fieldErrors,"requestCompletedBy")>bgcolor="#FF7777"</cfif> >
									<input type="text" name="requestCompletedBy" style="width:390px" value="<cfoutput>#requestCompletedBy#</cfoutput>">
								</td>
							</tr>
							<tr>
								<td <cfif listfind(fieldErrors,"dateCompleted")>bgcolor="#FF7777"</cfif> nowrap><b>Date Completed: </b></td>
								<td <cfif listfind(fieldErrors,"dateCompleted")>bgcolor="#FF7777"</cfif> class="nopadding">
									<table border="0" cellpadding="5" cellspacing="0" class="grey">
										<tr>
											<td <cfif listfind(fieldErrors,"dateCompleted")>bgcolor="#FF7777"</cfif> >
												<input name="dateCompleted" type="text" style="width:75px" value="<cfoutput>#dateCompleted#</cfoutput>" maxlength="10" />
											</td>
											<td <cfif listfind(fieldErrors,"dateCompleted")>bgcolor="#FF7777"</cfif> ><a style="text-decoration:none;" href="javascript:showCal('DateCompleted');"> <img src="../images/calicon.gif" alt="Select a Date" align="absmiddle" width="20" height="20" border="0" /> </a></td>
										</tr>
									</table>
								</td>
							</tr>
							</cfif>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<input name="btnSubmitRequest" type="submit" class="sidebar" value="Submit Technology Request" style="width:200px;">
						<input type="button" class="sidebar" value="Clear Form" onClick="document.location = 'new.cfm';">
						<input type="button" class="sidebar" value="Cancel Request" onClick="document.location = 'index.cfm';">
					</td>
				</tr>
			</form>
			</table>
		</td>
	</tr>
</table>
<br>
<a style="text-decoration:underline;" class="normal" href="/index.cfm">Return to Intranet</a> | 
<a style="text-decoration:underline;" class="normal" href="index.cfm">Return to Current Requests</a>
</div>

