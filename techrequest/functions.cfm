
<!--- function: validate form --->
<cffunction name="validateForm" access="private" returntype="string">
	<cfargument name="form" type="struct" required="yes">
	
	<cfset e = "">
	
	<cfif trim(form.customerOrDivision) is "">
		<cfset e = listappend(e,"customerOrDivision:The customer/division is required.")>
	</cfif>
	<cfif trim(form.personRequesting) is "">
		<cfset e = listappend(e,"personRequesting:The person requesting is required.")>
	</cfif>
	<cfif trim(form.personRequestingIs) is "">
		<cfset e = listappend(e,"personRequestingIs:The person requesting type is required. (Dealer or In-House)")>
	</cfif>
	<cfif trim(form.requestDetails) is "">
		<cfset e = listappend(e,"requestDetails:The request details are required.")>
	</cfif>
	<cfif not isdate(form.dateRequested)>
		<cfset e = listappend(e,"dateRequested:The date requested is not a valid date.")>
	</cfif>
	<cfif trim(form.dateCompleted) is not "">
		<cfif not isdate(form.dateCompleted)>
			<cfset e = listappend(e,"dateCompleted:The date completed is not a valid date.")>
		</cfif>
	</cfif>
	<cfif trim(form.dateCompleted) is not "" and trim(form.requestCompletedBy) is "">
		<cfset e = listappend(e,"requestCompletedBy:You must enter who completed the request.")>
	</cfif>
	<cfif form.requestTypeID is 0>
		<cfset e = listappend(e,"requestTypeID:The request is required.")>
	</cfif>
	<cfif trim(form.requestApprovedBy) is "">
		<cfset e = listappend(e,"requestApprovedBy:The person approving the request is required.")>
	</cfif>
	
	<cfreturn e>
</cffunction>
<!--- function: validate form --->

<!--- function : save request data --->
<cffunction name="saveRequest" access="private" returntype="boolean">
	<cfargument name="form" type="struct" required="yes">
	
	<cfset saved = true>
	
	<cfquery name="saveTechRequest" datasource="#ds#">
		declare @ID as int

		insert into techRequest (dateSubmitted, dateRequested, customerOrDivision, personRequesting, personRequestingIs, requestDetails, requestTypeID, requestApprovedBy, currentStatus, createdBy) 
		values (#createOdbcDate(form.dateSubmitted)#, #createOdbcDate(form.dateRequested)#, '#form.customerOrDivision#', '#form.personRequesting#', '#form.personRequestingIs#', '#form.requestDetails#', #form.requestTypeID#, '#form.requestApprovedBy#', '#form.status#', #cookie.adminLogin#) 

		set @ID = (select max(techRequestID) from techRequest where createdBy = #cookie.adminLogin#)
	
		<cfif trim(form.fileAttachments) IS NOT "">
			<cfloop list="#form.fileAttachments#" index="theFile">
				<cfset savedFile = listgetat(theFile,1,":")>
				<cfset clientFile = listgetat(theFile,2,":")>
				if not exists (select * from techRequest_attachments where savedFile = '#savedFile#' and clientFile = '#clientFile#' and techRequestID = @id)
				begin
					insert into techRequest_attachments (savedFile, clientFile, techRequestID)
					values ('#savedFile#', '#clientFile#', @ID)
				end
			</cfloop>
		</cfif>

	</cfquery>
	
<!--- email IT to notify --->
<cfmail from="pgregory@copsmonitoring.com" to="technologyrequest@copsmonitoring.com" subject="New Technology Request Submission Notification">
A new technology request has been submitted.

Date Submitted.............#dateformat(form.dateSubmitted,'mm/dd/yyyy')#
Customer...................#customerOrDivision#
Person Requesting..........#personRequesting# (#personRequestingIs#)
Details:
#form.requestDetails#

</cfmail>
	
	<cfreturn saved>
</cffunction>
<!--- function : save request data --->

<!--- function : update request data --->
<cffunction name="updateRequest" access="private" returntype="boolean">
	<cfargument name="form" type="struct" required="yes">
	
	<cfset saved = true>
	
	<cfquery name="updateTechRequest" datasource="#ds#">
		update techRequest
		set dateRequested = #createOdbcDate(form.dateRequested)#, customerOrDivision = '#form.customerOrDivision#', personRequesting = '#form.personRequesting#', personRequestingIs = '#form.personRequestingIs#', requestDetails = '#form.requestDetails#', requestTypeID = #form.requestTypeID#, requestApprovedBy = '#form.requestApprovedBy#' 
		where techRequestID = #form.rid# 
	</cfquery>
	
	<cfreturn saved>
</cffunction>
<!--- function : update request data --->

