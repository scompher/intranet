
<link rel="stylesheet" type="text/css" href="/styles.css">

<style type="text/css">
<!--
.largeText {
	font-size: 14px;
	font-weight: bold;
}
-->
</style>

<script type="text/javascript" language="javascript">
function OpenDetailWin(id) {
	var windowWidth = 565;
	var windowHeight = 470;
	var left = (screen.width / 2) - (windowWidth / 2);
	var top = (screen.height / 2) - (windowHeight/ 2) - 150;
	DetailWin = window.open("viewRequest.cfm?rid=" + id,"DetailWin","top=" + top + ",left=" + left + ",height=" + windowHeight + ",width=" + windowWidth + ",resizable=1,status=0,titlebar=0,toolbar=0,scrollbars=1");
}
</script>

<cfset maxrows = 50>
<cfparam name="form.filterByStatus" default="Open and Unread">
<cfparam name="form.filterByType" default="0">
<cfparam name="p" default="1">
<cfparam name="pages" default="1">
<cfparam name="urlString" default="">

<cfset start = (maxrows * p) - (maxrows - 1)>

<!--- request URL params to FORM params --->
<cfif isDefined("url.BTNAPPLYFILTERS")>
	<cfset urlString = "">
	<cfloop list="#cgi.QUERY_STRING#" delimiters="&" index="item">
		<cfset fieldName = listgetat(item,1,"=")>
		<cfif listlen(item,"=") is 2>
			<cfset fieldValue = listgetat(item,2,"=")>
		<cfelse>
			<cfset fieldValue = "">
		</cfif>
		<cfif fieldName is not "p">
			<cfset urlString = listappend(urlString,"#fieldName#=#fieldValue#","&")>
			<cfset formFieldName = "form.#fieldName#">
			<cfset "#formFieldName#" = "#urlDecode(fieldValue)#">
		</cfif>
	</cfloop>
</cfif>

<!--- build URL string --->
<cfif isDefined("form.fieldNames")>
	<cfset urlString = "">
	<cfloop list="#form.fieldNames#" index="item">
		<cfset urlString = listappend(urlString,"#item#=#evaluate(item)#","&")>
	</cfloop>
</cfif>

<cfquery name="getRequestTypes" datasource="#ds#">
	select * from techRequest_requestTypes
	order by orderID asc, requestType asc
</cfquery>

<cfquery name="getRequests" datasource="#ds#">
	select 
		techRequest.*, 
		techRequest_requestTypes.requestType, 
		createdUsers.firstname + ' ' + createdUsers.lastname as createdByName, 
		completedUsers.firstname + ' ' + completedUsers.lastname as completedByName, 
		openedUsers.firstname + ' ' + openedUsers.lastname as openedByName 
	from techRequest 
		left join techRequest_requestTypes on techRequest.requestTypeID = techRequest_requestTypes.requestTypeID 
		left join admin_users as createdUsers on techRequest.createdBy = createdUsers.adminuserid 
		left join admin_users as completedUsers on techRequest.requestcompletedby = completedUsers.adminuserid 
		left join admin_users as openedUsers on techRequest.openedBy = openedUsers.adminuserid 
	where 
	<cfif trim(filterByStatus) is not "">
		<cfif filterByStatus is "Open and Unread">
			currentStatus IN ('Opened','Unread') and 
		<cfelse>
			currentStatus = '#filterByStatus#' and 
		</cfif>
	</cfif>
	<cfif trim(filterByType) is not "0">
		techRequest.requestTypeID = #filterByType# and 
	</cfif>
	1 = 1 
	order by techRequest.dateSubmitted asc 
</cfquery>

<cfset pages = ceiling(getRequests.recordCount / maxrows)>

<div align="center">
<table width="955" border="0" cellspacing="0" cellpadding="5">
	<form method="post" action="index.cfm">
	<tr>
		<td><b>Filter By:</b>&nbsp;
			<select name="filterByStatus" style="vertical-align:middle;">
				<option <cfif filterByStatus is "">selected</cfif> value="">All Status</option>
				<option <cfif filterByStatus is "Open and Unread">selected</cfif> value="Open and Unread">Open and Unread</option>
				<option <cfif filterByStatus is "Open">selected</cfif> value="Open">Open</option>
				<option <cfif filterByStatus is "Closed">selected</cfif> value="Closed">Closed</option>
				<option <cfif filterByStatus is "Unread">selected</cfif> value="Unread">Unread</option>
			</select>
			&nbsp;
			<select name="filterByType" style="vertical-align:middle;">
				<option value="0">All requests</option>
				<cfoutput query="getrequestTypes">
					<option <cfif getrequestTypes.requestTypeID is filterByType>selected</cfif> value="#requestTypeID#">#requestType#</option>
				</cfoutput>
			</select>
			&nbsp;
			<input name="btnApplyFilters" type="submit" class="sidebar" value="Apply Filters" style="vertical-align:middle;" />
		</td>
	</tr>
	</form>
</table>
<table width="955" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Technology Requests</b> </td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<form method="post" action="index.cfm">
				<tr>
					<td>
						<input type="button" class="sidebar" value="Enter New Request" onclick="document.location='new.cfm';" />
					</td>
				</tr>
				</form>
				<tr>
					<td class="nopadding">
						<table width="100%" border="0" cellspacing="0" cellpadding="5">
							<tr>
								<td align="center" valign="bottom" class="linedrow">&nbsp;</td>
								<td align="center" valign="bottom" class="linedrow" nowrap="nowrap"><b>Request # </b></td>
								<td align="center" valign="bottom" class="linedrow"><b>Date Requested </b></td>
								<td valign="bottom" class="linedrow"><b>Customer/Divison</b></td>
								<td valign="bottom" class="linedrow"><b>Requested request </b></td>
								<td align="center" valign="bottom" class="linedrow"><b>Date Opened </b></td>
								<td align="center" valign="bottom" class="linedrow"><b>Opened By</b></td>
								<td align="center" valign="bottom" class="linedrow"><b>Date Completed </b></td>
								<td align="center" valign="bottom" class="linedrow"><b>Completed By </b></td>
							</tr>
							<cfoutput query="getRequests" startrow="#start#" maxrows="#maxrows#">
								<cfquery name="checkForAttachments" datasource="#ds#">
									select * from techRequest_attachments where techRequestID = #getRequests.techRequestID# 
								</cfquery>
								<cfset bgc = "##FFFFFF">
								<tr bgcolor="#bgc#">
									<td class="linedrow" nowrap="nowrap">
									&nbsp;
									<cfif checkForAttachments.recordcount gt 0>
										<img src="/images/attachment.gif" alt="Attachment" width="15" height="15" />
									<cfelse>
										<img src="/images/spacer.gif" height="15" width="17" />
									</cfif>
									&nbsp;
									<cfswitch expression="#currentStatus#">
										<cfcase value="Unread">
										<cfif listfindnocase(departmentlist,1) is not 0>
											<a href="openRequest.cfm?rid=#techRequestID#" onclick="return confirm('Are you sure you wish mark this item as picked up?');"><img border="0" src="/images/unread.gif" alt="Open/Pick Up Request" /></a>
										<cfelse>
											<img border="0" src="/images/unread.gif" alt="Pick Up Request" />
										</cfif>
										</cfcase>
										<cfcase value="Opened"><img border="0" src="/images/read.gif" alt="Opened/Picked Up" /></cfcase>
										<cfcase value="Closed"><img border="0" src="/images/closed.gif" alt="Closed" /></cfcase>
									</cfswitch>
									&nbsp;
									<a href="javascript:OpenDetailWin(#techRequestID#);"><img src="/images/manifyingglass.gif" border="0" alt="View Request" /></a>
									<cfif currentStatus is "Opened">
										&nbsp;
										<a href="markcompleted.cfm?rid=#techRequestID#" onclick="return confirm('Are you sure you wish to mark this item completed?');"><img src="/images/markCompleted.gif" alt="Mark Completed" border="0" /></a>
									<cfelse>
										&nbsp;
										<img src="/images/spacer.gif" height="15" width="17" />
									</cfif>
									<cfif cookie.adminlogin is createdBy>
										&nbsp;
										<a href="edit.cfm?rid=#techRequestID#"><img src="/images/edit.gif" alt="Edit Request" border="0" /></a>
									</cfif>
									&nbsp;
									</td>
									<td class="linedrow">#techRequestID#</td>
									<td class="linedrow">#dateformat(dateRequested,'mm/dd/yyyy')#</td>
									<td class="linedrow">#customerOrDivision#&nbsp;</td>
									<td class="linedrow">#requestType#&nbsp;</td>
									<td class="linedrow">
									<cfif trim(dateOpened) is not "">
									#dateformat(dateOpened,'mm/dd/yyyy')#
									</cfif>&nbsp;
									</td>
									<td nowrap="nowrap" class="linedrow">#openedByName#&nbsp;</td>
									<td class="linedrow">
									<cfif trim(datecompleted) is not "">
									#dateformat(dateCompleted,'mm/dd/yyyy')#
									</cfif>&nbsp;
									</td>
									<td class="linedrow" nowrap="nowrap">
									#completedbyName#&nbsp;
									</td>
								</tr>
							</cfoutput>
						</table>
					</td>
				</tr>
				<tr>
					<td>
					<cfif pages gt 1>
						Page 
						<cfloop from="1" to="#pages#" index="page">
						<cfoutput>
						<cfif page is p>
							<span class="largeText">#page#</span>
						<cfelse>
							<a style="text-decoration:underline" href="index.cfm?p=#page#&#urlString#">#page#</a>
						</cfif>
						<cfif page is not pages> | </cfif>
						</cfoutput>
						</cfloop>
					</cfif>
					&nbsp;
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<br />
<a href="/index.cfm" style="text-decoration:underline;" class="normal">Return to Intranet</a>
</div>
