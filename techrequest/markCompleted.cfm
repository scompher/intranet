
<cfif isDefined("form.btnMarkCompleted")>

	<cfquery name="markCompleted" datasource="#ds#">
		update globalChangeRequest 
		set currentStatus = 'Closed', dateCompleted = #createodbcdate(now())#, changeCompletedBy = #cookie.adminlogin#, notes = '#form.notes#' 
		where changeRequestID = #rid#
	</cfquery>
	
	<!--- email creator --->
	<cfquery name="getCreatorID" datasource="#ds#">
		select createdBy from globalChangeRequest 
		where changeRequestID = #rid# 
	</cfquery>
	<cfquery name="getCreatorInfo" datasource="#ds#">
		select * from admin_users 
		where adminuserid = #getcreatorid.createdBy#
	</cfquery>
	
<cfmail from="pgregory@copsmonitoring.com" to="#getCreatorInfo.email#" subject="Your global change request has been completed.">
Global change request ###rid# has been marked completed by #getsec.firstname# #getsec.lastname#.
<cfif trim(form.notes) is not "">
Closing Notes:
#form.notes#
</cfif>
</cfmail>
	
	<cflocation url="index.cfm">

<cfelseif isDefined("form.btnCancel")>

	<cflocation url="index.cfm">

<cfelse>

<link rel="stylesheet" type="text/css" href="/styles.css">

<br />
<div align="center">
<table border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Closing Notes </b></td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td><b>Please Enter any closing notes:</b> </td>
				</tr>
				<form method="post" action="markCompleted.cfm">
				<input type="hidden" name="rid" value="<cfoutput>#rid#</cfoutput>" />
				<tr>
					<td>
						<textarea name="notes" style="width:400px" rows="5"></textarea>
					</td>
				</tr>
				<tr>
					<td>
						<input name="btnMarkCompleted" type="submit" class="sidebar" value="Save and Close" />
						<input name="btnCancel" type="submit" class="sidebar" value="Cancel Close" />
					</td>
				</tr>
				</form>
			</table>
		</td>
	</tr>
</table>

</div>

</cfif>

