
<cfif isdefined("form.btnsaveitem")>

	<cfset itemd = createodbcdate(form.itemdate)>
	<cfparam name="form.currentitem" default="0">
	<cfparam name="form.active" default="0">

	<!--- clear existing current if this is marked as current --->	
	<cfif form.currentitem IS NOT 0>
		<cfquery name="undocurrent" datasource="copalink">
			update news_items set currentitem = 0
		</cfquery>
	</cfif>
		
	<!--- save news item --->
	<cfquery name="saveitem" datasource="copalink">
		update news_items 
		set itemdate = #itemd#, title = '#form.title#', content = '#form.content#', currentitem = #form.currentitem#, active = #form.active#, weblink = '#form.weblink#' 
		where newsitemid = #id#
	</cfquery>

	<cflocation url="index.cfm">

<cfelse>
	
	<script language="JavaScript" type="text/JavaScript">
	/**
	 * DHTML date validation script. Courtesy of SmartWebby.com (http://www.smartwebby.com/dhtml/)
	 */
	// Declaring valid date character, minimum year and maximum year
	var dtCh= "/";
	var minYear=1900;
	var maxYear=2100;
	
	function isInteger(s){
		var i;
		for (i = 0; i < s.length; i++){   
			// Check that current character is number.
			var c = s.charAt(i);
			if (((c < "0") || (c > "9"))) return false;
		}
		// All characters are numbers.
		return true;
	}
	
	function stripCharsInBag(s, bag){
		var i;
		var returnString = "";
		// Search through string's characters one by one.
		// If character is not in bag, append to returnString.
		for (i = 0; i < s.length; i++){   
			var c = s.charAt(i);
			if (bag.indexOf(c) == -1) returnString += c;
		}
		return returnString;
	}
	
	function daysInFebruary (year){
		// February has 29 days in any year evenly divisible by four,
		// EXCEPT for centurial years which are not also divisible by 400.
		return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 );
	}
	function DaysArray(n) {
		for (var i = 1; i <= n; i++) {
			this[i] = 31
			if (i==4 || i==6 || i==9 || i==11) {this[i] = 30}
			if (i==2) {this[i] = 29}
	   } 
	   return this
	}
	function isDate(dtStr){
		var daysInMonth = DaysArray(12)
		var pos1=dtStr.indexOf(dtCh)
		var pos2=dtStr.indexOf(dtCh,pos1+1)
		var strMonth=dtStr.substring(0,pos1)
		var strDay=dtStr.substring(pos1+1,pos2)
		var strYear=dtStr.substring(pos2+1)
		strYr=strYear
		if (strDay.charAt(0)=="0" && strDay.length>1) strDay=strDay.substring(1)
		if (strMonth.charAt(0)=="0" && strMonth.length>1) strMonth=strMonth.substring(1)
		for (var i = 1; i <= 3; i++) {
			if (strYr.charAt(0)=="0" && strYr.length>1) strYr=strYr.substring(1)
		}
		month=parseInt(strMonth)
		day=parseInt(strDay)
		year=parseInt(strYr)
		if (pos1==-1 || pos2==-1){
			alert("The date format should be : mm/dd/yyyy")
			return false
		}
		if (strMonth.length<1 || month<1 || month>12){
			alert("Please enter a valid month")
			return false
		}
		if (strDay.length<1 || day<1 || day>31 || (month==2 && day>daysInFebruary(year)) || day > daysInMonth[month]){
			alert("Please enter a valid day")
			return false
		}
		if (strYear.length != 4 || year==0 || year<minYear || year>maxYear){
			alert("Please enter a valid 4 digit year between "+minYear+" and "+maxYear)
			return false
		}
		if (dtStr.indexOf(dtCh,pos2+1)!=-1 || isInteger(stripCharsInBag(dtStr, dtCh))==false){
			alert("Please enter a valid date")
			return false
		}
	return true
	}
	
	function checkform(frm) {
		if (frm.itemdate.value == "") {alert('The item date is required.'); frm.itemdate.focus(); return false;}
		if (!isDate(frm.itemdate.value)) {frm.itemdate.focus(); return false;}
		if (frm.title.value == "") {alert('The item title is required.'); frm.title.focus(); return false;}
		if (frm.content.value == "" && frm.weblink.value = "") {alert('The item content or weblink is required.');  return false;}
	
		return true;
	}
	</script>

	<cfquery name="getitem" datasource="copalink">
		select * from news_items
		where newsitemid = #id#
	</cfquery>

	<div align="center">
		<form method="post" action="edit.cfm">
		<cfoutput>
			<input type="hidden" name="id" value="#id#">
		</cfoutput>
		<table border="1" cellpadding="5" cellspacing="0">
			<tr>
				<td align="center" bgcolor="#DDDDDD"><b>Edit A News Item </b></td>
			</tr>
			<cfoutput query="getitem">
			<tr>
				<td align="left"><b>News Item Date:</b> (mm/dd/yyyy)<br>
				<input name="itemdate" type="text" class="normal" width="12" value="#dateformat(getitem.itemdate,'mm/dd/yyyy')#">
				</td>
			</tr>
			<tr>
				<td align="left"><b>News Item Title:</b><br>
				<input name="title" type="text" class="normal" style="width:700px" value="#getitem.title#" maxlength="255"></td>
			</tr>
			<tr>
				<td align="left"><b>Web Link:</b><br>
				<input name="weblink" type="text" class="normal" style="width:700px" value="#getitem.weblink#">
				</td>
			</tr>
			<tr>
				<td align="left"><b>News Item Content:</b><br>
				<textarea name="content" class="normal" rows="20" style="width:700px; font-family:'Courier New', Courier, monospace;">#getitem.content#</textarea>
				</td>
			</tr>
			<tr>
				<td align="left"><b>Make this the current news item?</b>
				<input name="currentitem" type="radio" value="1" <cfif getitem.currentitem IS NOT 0>checked</cfif> >Yes
				<input name="currentitem" type="radio" value="0" <cfif getitem.currentitem is 0>checked</cfif> >No
				</td>
			</tr>
			<tr>
				<td align="left"><b>Make this item active on the site?</b> (active but not current items will show in archive) 
				<input name="active" type="radio" value="1" <cfif getitem.active is not 0>checked</cfif> >Yes
				<input name="active" type="radio" value="0" <cfif getitem.active is 0>checked</cfif> >No
				</td>
			</tr>
			</cfoutput>
			<tr>
				<td align="left"><input name="btnsaveitem" onclick="return checkform(this.form);" type="submit" class="normal" value="Update Item"></td>
			</tr>
		</table>
		</form>
	    <a class="normal" href="index.cfm">Return to News &amp; Announcements Menu</a>
		</div>
	
</cfif>
