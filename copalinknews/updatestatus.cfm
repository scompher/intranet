
<cfparam name="form.currentitem" default="0">
<cfparam name="form.active" default="0">

<cfquery name="updatestatus" datasource="copalink">
	begin
		update news_items set active = 0 where newsitemid IN (#form.itemsonpage#)
	end
	begin
		update news_items set active = 1 where newsitemid IN (#form.active#)
	end
	begin
		update news_items set currentitem = 0 where newsitemid IN (#form.itemsonpage#)
	end
	begin
		update news_items set currentitem = 1, active = 1 where newsitemid IN (#form.currentitem#)
	end
</cfquery>

<cflocation url="index.cfm?m=Status Updated&start=#start#">

