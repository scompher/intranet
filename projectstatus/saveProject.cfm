


<cfif projectID gt 0>
<!---	<cfdump var="#form#" >
	<cfabort >--->
	<cfquery name='updateProject' datasource="#DS#" >
		update projectTracker_main
		set requestor='#requestor#',
		<cfif trim(#projectStartDate#) is "">
			projectStartDate=NULL
			<cfelse>
			projectStartDate='#projectStartDate#'
		</cfif>,
		<cfif trim(#projectDueDate#) is "">
			projectDueDate=NULL
			<cfelse>
		projectDueDate='#projectDueDate#'
		</cfif>,
		<cfif trim(#planningDueDate#) is "">
			planningDueDate=NULL
			<cfelse>
			planningDueDate='#planningDueDate#'
		</cfif>,
		<cfif trim(#programmingDueDate#) is "">
			programmingDueDate=NULL
			<cfelse>
			programmingDueDate='#programmingDueDate#'
		</cfif>,
		<cfif trim(#testingDueDate#) is "">
			testingDueDate=NULL
			<cfelse>
			testingDueDate='#testingDueDate#'
		</cfif>,
		status='#projectStatus#',
		projectName='#projectName#',
		projectDescription='#projectDescription#',
		resources='#resources#',
		<cfif trim(#rolloutMeetingDate#) is "">
			rolloutMeetingDate=NULL
			<cfelse>
			rolloutMeetingDate='#rolloutMeetingDate#'
		</cfif>,
		<cfif trim(#projectApprovalDate#) is "">
			approvalDate=NULL
			<cfelse>
			approvalDate='#projectApprovalDate#'
		</cfif>,
		<cfif trim(#warrantyEndDate#) is "">
			warrantyDate=NULL
			<cfelse>
			warrantyDate='#warrantyEndDate#'
		</cfif>,
		<cfif trim(#conceptDueDate#) is "">
			conceptDueDate=NULL
			<cfelse>
			conceptDueDate='#conceptDueDate#'
		</cfif>,
		projectType="#projectTypes#"
		where projectID = #projectID#
	</cfquery>
	<cflocation url="projectView.cfm?projectID=#projectID#">
<cfelse>
	<cfquery name='saveProject' datasource="#DS#">
		INSERT INTO projectTracker_main (active,requestor,projectStartDate,projectDueDate,planningDueDate,programmingDueDate,testingDueDate,status,projectName,projectDescription,resources,rolloutMeetingDate,approvalDate,warrantyDate,conceptDueDate,projectType)
		VALUES (1,
		'#requestor#',
		<cfif trim(#projectStartDate#) is "">
			NULL
			<cfelse>
			'#projectStartDate#'
		</cfif>,
		<cfif trim(#projectDueDate#) is "">
			NULL
			<cfelse>
			'#projectDueDate#'
		</cfif>,
		<cfif trim(#planningDueDate#) is "">
			NULL
			<cfelse>
			'#planningDueDate#'
		</cfif>,
		<cfif trim(#programmingDueDate#) is "">
			NULL
			<cfelse>
			'#programmingDueDate#'
		</cfif>,
		<cfif trim(#testingDueDate#) is "">
			NULL
			<cfelse>
			'#testingDueDate#'
		</cfif>,
		'#projectStatus#',
		'#projectName#',
		'#projectDescription#',
		'#resources#',
		<cfif trim(#rolloutMeetingDate#) is "">
			NULL
			<cfelse>
		'#rolloutMeetingDate#'
		</cfif>,
		<cfif trim(#projectApprovalDate#) is "">
			NULL
			<cfelse>
			'#projectApprovalDate#'
		</cfif>,
		<cfif trim(#warrantyEndDate#) is "">
			NULL
			<cfelse>
			'#warrantyEndDate#'
		</cfif>,
		<cfif trim(#conceptDueDate#) is "">
			NULL
			<cfelse>
			'#conceptDueDate#'
		</cfif>,
		'#projectTypes#')
	</cfquery>
	<cflocation url="index.cfm" >
</cfif>


