<cfquery name="getProjectDetails" datasource="#DS#">
	select projectTracker_main.*, projectTracker_status.status as statusname, admin_users.firstname, admin_users.lastname
	 from projectTracker_main
	 left join projectTracker_status on projectTracker_main.status=projectTracker_status.statusid
	 left join admin_users on projectTracker_main.resources=admin_users.adminuserid
	 where projectTracker_main.projectID = #projectID#
</cfquery>
<cfquery name="getNotes" datasource="#DS#">
	select *
	from projectTracker_notes
	where projectID = #projectID# and active=1
	order by noteID desc
</cfquery>
<cfquery name="getAdditionalResources" datasource="#DS#">
	select projectTracker_AdditionalResources.*, projectTracker_phases.phaseName as additionalPhase, admin_users.firstname as additionalFirst, admin_users.lastname as additionalLast
	from projectTracker_AdditionalResources
	left join projectTracker_phases on projectTracker_AdditionalResources.phase=projectTracker_phases.phaseID
	left join admin_users on projectTracker_AdditionalResources.resource=admin_users.adminuserid
	where projectTracker_AdditionalResources.projectID = #projectID#
</cfquery>

<link rel="stylesheet" type="text/css" href="../styles.css">
<style>
th {
text-align: left;
}
</style>
<div align="center">
<table  width="900" border="0" cellspacing="0" cellpadding="5">
    <tr>
        <td class="highlightbar"><b>Project Info</b></td>
    </tr>
    <tr>
        <td class="greyrowbottomnopadding">
		<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td>
					<cfoutput>
					<table width="100%" border="0" cellspacing="0" cellpadding="5">
  						<tr>
							<th>Requestor:</th>
							<td>#getProjectDetails.requestor#</td>
							<th>Approval Date:</th>
							<td>#DateFormat(getProjectDetails.approvalDate,"MM/DD/YYYY")#</td>
  						</tr>
						<tr>
							<th>Project Name:</th>
							<td>#getProjectDetails.projectName#</td>
						</tr>
						<tr>
							<th>Project Description:</th>
							<td>#getProjectDetails.projectDescription#</td>
						</tr>
						<tr>
							<th>Status:</th>
							<td>#getProjectDetails.statusName#</td>
						</tr>
						<tr>
							<th>Start Date:</th>
							<td>#DateFormat(getProjectDetails.projectStartDate,"MM/DD/YYYY")#</td>
						</tr>
						<tr>
							<th>Finish Date:</th>
							<td>#DateFormat(getProjectDetails.projectDueDate,"MM/DD/YYYY")#</td>
						</tr>
						<tr>
							<th>Planning Due Date:</th>
							<td>#DateFormat(getProjectDetails.planningDueDate,"MM/DD/YYYY")#</td>
						</tr>
						<tr>
							<th>Programming Due Date:</th>
							<td>#DateFormat(getProjectDetails.programmingDueDate,"MM/DD/YYYY")#</td>
						</tr>
						<tr>
							<th>Testing Due Date:</th>
							<td>#DateFormat(getProjectDetails.testingDueDate,"MM/DD/YYYY")#</td>
						</tr>
						<tr>
							<th>Rollout Scheduled?</th>
							<td><cfif getProjectDetails.rolloutMeetingDate is not "">Yes<cfelse>No</cfif></td>
							<th>Rollout Date:</th>
							<td>#DateFormat(getProjectDetails.rolloutMeetingDate,"MM/DD/YYYY")#</td>
						</tr>
						<tr>
							<th>Assigned To:</th>
							<td>#getprojectDetails.firstname# #getprojectDetails.lastname#</td>
						</tr>
						<tr>
							<th>Additional Resources:</th>
						</tr>
						<tr>
							<th>Phase:</th>
							<th>Resource:</th>
						</tr>
						</cfoutput>
						<cfoutput query="getAdditionalResources">
							<tr>
								<td>#additionalPhase#</td>
								<td>#additionalFirst# #additionalLast#</td>
							</tr>
						</cfoutput>
					</table>
					
				</td>
			</tr>
		</table>				
	</td>
    </tr>
</table>
<br />
<cfoutput>
<form id="newNoteForm" name="newNoteForm" method="post" action="saveNote.cfm?projectID=#getProjectDetails.projectID#" data-ajax="false">
</cfoutput>
<table  width="900" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td><textarea name="note" id="note" maxlength="250"></textarea>
		</td>
	</tr>
		<tr>
		<td><button type="submit" id="submitNewNote">Add Note</button></td>
	</tr>
</table>
</form>
<br />
<table  width="900" border="0" cellspacing="0" cellpadding="5">
    <tr>
        <td class="highlightbar"><b>Project Notes</b></td>
    </tr>
    <tr>
        <td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="5">
							<cfoutput query="getNotes">
  							<tr>
  								<td>
  									<a onclick = "return confirm('Are you sure you wish to delete this item?');" href='deleteNote.cfm?projectID=#projectID#&noteID=#getNotes.noteID#'><img src='/images/delete.gif'></img></a>&nbsp #DateFormat(getNotes.dateCreated,"MM/DD/YYYY")# - #note#
  								</td>
  							</tr>
  							</cfoutput>
  						</table>
  					</td>
  				</tr>
  			</table>
  		</td>
  	</tr>
 </table>	
 <br />
<a class='normal' style="text-decoration:underline;" href='/index.cfm'>Return to Intranet</a>			
</div>