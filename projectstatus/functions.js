$(document).ready(function() {
	
	$('#createProjectForm').hide();
	$('#programmingDateInput').hide();
	$('#designDateInput').hide();
	$('#testingDateInput').hide();
	$('#rolloutDateInput').hide();
	$('#initiationDateInput').hide();
	$('#planningDateInput').hide();
	$('#executionDateInput').hide();
	$('#warrantyDateInput').hide();
	$('#controlDateInput').hide();
	$('#conceptDateInput').hide();
	$('#additionalResourcesInput').hide();
	
	
	$('#projectTypes').on("change",function(event,ui) {
		if ($(this).val() == "0") {
			$('#createProjectForm').hide();
			$('#additionalResourcesInput').hide();
		} else {
			$('#createProjectForm').show();
			$('#additionalResourcesInput').show();
			if ($(this).val() == "1") {
				$('#programmingDateInput').show();
				$('#designDateInput').show();
				$('#testingDateInput').show();
				$('#rolloutDateInput').show();
				$('#warrantyDateInput').show();
				$('#conceptDateInput').show();
				$('#initiationDateInput').hide();
				$('#planningDateInput').hide();
				$('#executionDateInput').hide();
				$('#controlDateInput').hide();
			} else {
				$('#programmingDateInput').hide();
				$('#designDateInput').hide();
				$('#testingDateInput').hide();
				$('#rolloutDateInput').hide();
				$('#warrantyDateInput').hide();
				$('#conceptDateInput').hide();
				$('#initiationDateInput').show();
				$('#planningDateInput').show();
				$('#executionDateInput').show();
				$('#controlDateInput').show();
			}
		}
	});
	
	if ($("#projectTypes").val() != 0) {
		$('#createProjectForm').show();
		$('#additionalResourcesInput').show();
	}
	if ($('#projectTypes').val() == 1) {
		$('#programmingDateInput').show();
		$('#designDateInput').show();
		$('#testingDateInput').show();
		$('#rolloutDateInput').show();
		$('#warrantyDateInput').show();
		$('#conceptDateInput').show();
		$('#initiationDateInput').hide();
		$('#planningDateInput').hide();
		$('#executionDateInput').hide();
		$('#controlDateInput').hide();
	} 
	if ($('#projectTypes').val() >=2) {
		$('#programmingDateInput').hide();
		$('#designDateInput').hide();
		$('#testingDateInput').hide();
		$('#rolloutDateInput').hide();
		$('#warrantyDateInput').hide();
		$('#conceptDateInput').hide();
		$('#initiationDateInput').show();
		$('#planningDateInput').show();
		$('#executionDateInput').show();
		$('#controlDateInput').show();
	}
		
	
});