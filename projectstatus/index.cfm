<cfquery name="getProjects" datasource="#DS#">
	select projectTracker_main.*, projectTracker_status.status as statusName
	from projectTracker_main
	left join projectTracker_status
	on projectTracker_main.status=projectTracker_status.statusID
	where projectTracker_main.active = 1
</cfquery>

<link rel="stylesheet" type="text/css" href="../styles.css">
<style>
th {
text-align: left;
}
</style>
<div align="center">
<table  width="900" border="0" cellspacing="0" cellpadding="5">
    <tr>
        <td class="highlightbar"><b>Project Status Tracker</b></td>
    </tr>
    <tr>
        <td class="greyrowbottomnopadding">
		<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td>
					<table width="100%" border="0" cellspacing="0" cellpadding="5">
  						<tr>
    						<th>Action</th>
   						 	<th>Project</th>
    						<th>Status</th>
   							<th>Due Date</th>
    						<th>Requestor</th>
							<!---<th>Notes</th>--->
  						</tr>
  						<cfoutput query="getProjects">
  						<tr>
    						<td><a href='createProject.cfm?projectID=#getProjects.projectID#'><img src='/images/edit.gif'></img></a>&nbsp<a onclick = "return confirm('Are you sure you wish to delete this item?');" href='deleteProject.cfm?projectID=#getProjects.projectID#'><img src='/images/delete.gif'></img></a>&nbsp<a href='projectView.cfm?projectID=#getProjects.projectID#'><img src='/images/view.gif'></img></a></td>
    						<td>#getProjects.projectName#</td>
    						<td>#getProjects.statusName#</td>
    						<td>#DateFormat(getProjects.projectDueDate,"MM/DD/YYYY")#</td>
    						<td>#getProjects.requestor#</td>
							<!---<td>View</td>--->
  						</tr>
  						</cfoutput>
					</table>
				</td>
			</tr>
		</table>
	</td>
    </tr>
</table>
<br />
<table  width="900" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td>
		<form method="post" action="createProject.cfm">
			<input type="submit" value="Create Project" />	
		</form>
		</td>
	</tr>
</table>
	
</form>
	
</button>
<br />
<a class='normal' style="text-decoration:underline;" href='/index.cfm'>Return to Intranet</a>
</div>