<cfparam name="projectID" default="0" >
<cfparam name="requestor" default="" >
<cfparam name="projectStartDate" default="" >
<cfparam name="projectDueDate" default="">
<cfparam name="planningDueDate" default="" >
<cfparam name="programmingDueDate" default="" >
<cfparam name="testingDueDate" default="" >
<cfparam name="status" default="" >
<cfparam name="projectName" default="" >
<cfparam name="projectDescription" default="" >
<cfparam name="resources" default="" >
<cfparam name="rolloutMeetingDate" default="" >
<cfparam name="approvalDate" default="" >
<cfparam name="conceptDueDate" default="" >
<cfparam name="warrantyEndDate" default="" >

<cfif #projectID# is not "0">
	<cfquery name="getProjectInfo" datasource="#DS#">
		select *
		from projectTracker_main
		where projectID=#projectID#
	</cfquery>
	<cfset requestor = getProjectInfo.requestor>
	<cfset projectStartDate = dateFormat(getProjectInfo.projectStartDate,'MM/DD/YYYY')>
	<cfset projectDueDate = dateFormat(getProjectInfo.projectDueDate,'MM/DD/YYYY')>
	<cfset planningDueDate = dateFormat(getProjectInfo.planningDueDate,'MM/DD/YYYY')>
	<cfset programmingDueDate = dateFormat(getProjectInfo.programmingDueDate,'MM/DD/YYYY')>
	<cfset testingDueDate = dateFormat(getProjectInfo.testingDueDate,'MM/DD/YYYY')>
	<cfset status = getProjectInfo.status>
	<cfset projectName = getProjectInfo.projectName>
	<cfset projectDescription = getProjectInfo.projectDescription>
	<cfset resources = getProjectInfo.resources>
	<cfset rolloutMeetingDate = dateFormat(getProjectInfo.rolloutMeetingDate,'MM/DD/YYYY')>
	<cfset approvalDate = dateFormat(getProjectInfo.approvalDate,'MM/DD/YYYY')>
	<cfquery name="getAdditionalResources" datasource="#DS#">
		select projectTracker_AdditionalResources.*, projectTracker_phases.phaseName as additionalPhase, admin_users.firstname as additionalFirst, admin_users.lastname as additionalLast
		from projectTracker_AdditionalResources
		left join projectTracker_phases on projectTracker_AdditionalResources.phase=projectTracker_phases.phaseID
		left join admin_users on projectTracker_AdditionalResources.resource=admin_users.adminuserid
		where projectTracker_AdditionalResources.projectID = #projectID#
	</cfquery>
</cfif>

<cfquery name="populateResources" datasource="#DS#">
	select *
	from dbo.Admin_Users
	where active = 1 and defaultdeptid = 1
	order by lastname
</cfquery>
<cfquery name="populateStatus" datasource="#DS#">
	select *
	from dbo.projectTracker_status
</cfquery>
<cfquery name="populateTypes" datasource="#DS#" >
	select *
	from dbo.projectTracker_types
</cfquery>
<cfquery name="populateProgrammingPhases" datasource="#DS#" >
	select *
	from dbo.projectTracker_phases
	where phaseType = 1 or phaseType = 3
</cfquery>
<cfquery name="populateOtherPhases" datasource="#DS#" >
	select *
	from dbo.projectTracker_phases
	where phaseType = 2 or phaseType = 3
</cfquery>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/themes/smoothness/jquery-ui.css" />
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" href="../styles.css" />
<script src="functions.js"></script>
<style>
th {
text-align: left;
}
</style>
</head>
	<cfform id="newProjectForm" name="newProjectForm" method="post" action="saveProject.cfm" data-ajax="false">
	<cfinput type = 'hidden' name="projectID" value="#projectID#">
	<div  align="center">
	<table width="900" border="0" cellspacing="0" cellpadding="5">
	    <tr>
	        <th class="highlightbar">Select project type to begin</th>
	    </tr>
	    <tr>
	        <td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="5">
							<tr>
								<th>Project Type:</th>
								<td>
									<select id="projectTypes" name="projectTypes">
										<option value="0">Please Select</option>
										<cfoutput query="populateTypes">
											<option value="#populateTypes.typeID#" <cfif status is populateStatus.statusID>selected</cfif>>#populateTypes.type#</option>
										</cfoutput>
									</select>
								</td>
							</tr>	
						</table>
					</td>	
				</tr>	
			</table>
			</td>
		</tr>
	</table>
	<br />
		
<!---Begin the input form --->

	<table id="createProjectForm" width="900" border="0" cellspacing="0" cellpadding="5">
	    <tr>
	        <th class="highlightbar"><cfif #projectID# is not "0">Update Project<cfelse>Create Project</cfif></th>
	    </tr>
	    <tr>
	        <td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="5">
							<tr>
								<th>Project Name:</th>
								<td>
									<cfinput name="projectName" id="projectName" type="text" maxlength="80" required="yes" message="Project name is required" value="#projectName#">
								</td>
							</tr>
							<tr>
								<th>Project Description:</th>
								<td>
									<cftextarea name="projectDescription" id="projectDescription" type="text" style="width:500px" maxlength="255" value="#projectDescription#" ></cftextarea>
								</td>
							</tr>
	  						<tr>
	    						<th>Requestor:</th>
								<td>
									<cfinput name="requestor" id="requestor" maxlength="40" type="text" value="#requestor#">
								</td>
	  						</tr>
	  						<tr>
	  							<th>Approval Date:</th>
	  							<td>
	  								<cfinput name="projectApprovalDate" id="projectApprovalDate" type="datefield" value="#ApprovalDate#" >
	  							</td>
	  						<tr>
	  							<th>Project Start Date:</th>
								<td>
									<cfinput name="projectStartDate" id="projectStartDate" type="datefield" value="#projectStartDate#" >
								</td>	
							</tr>
							<tr>
								<th>Project End Date:</th>
								<td>
									<cfinput name="projectDueDate" id="projectDueDate" type="datefield" value="#projectDueDate#">
								</td>
							</tr>
							<tr id="conceptDateInput">
								<th>Concept Due Date:</th>
									<td>
										<cfinput name="conceptDueDate" id="conceptDueDate" type='datefield' value="#conceptDueDate#"></input>
									</td>
							</tr>
							<tr id="designDateInput">
								<th>Design Due Date:</th>
									<td>
										<cfinput name="planningDueDate" id="planningDueDate" type='datefield' value="#planningDueDate#"></input>
									</td>
							</tr>
							<tr id="programmingDateInput">
								<th>Programming Due Date:</th>
								<td>
									<cfinput name="programmingDueDate" id="programmingDueDate" type='datefield' value="#programmingDueDate#"></input>
								</td>	
							</tr>
							<tr id="testingDateInput">
								<th>Testing Due Date:</th>
								<td>
									<cfinput name="testingDueDate" id="testingDueDate" type='datefield' value="#testingDueDate#"></input>
								</td>
							</tr>
							<tr id="rolloutDateInput">
								<th>Rollout Meeting Date:</th>
								<td>
									<cfinput name="rolloutMeetingDate" id="rolloutMeetingDate" type='datefield' value="#rolloutMeetingDate#"/>
								</td>
							</tr>
							<tr id="warrantyDateInput">
								<th>Warranty End Date:</th>
								<td>
									<cfinput name="warrantyEndDate" id="warrantyEndDate" type='datefield' value="#warrantyEndDate#"/>
								</td>
							</tr>
							<tr id="initiationDateInput">
								<th>Initiation Due Date:</th>
								<td>
									<cfinput name="initiationDate" id="initiationDate" Type="datefield" value="#conceptDueDate#" />
								</td>
							</tr>
							<tr id="planningDateInput">
								<th>Planning Due Date:</th>
									<td>
										<cfinput name="planningDueDate" id="planningDueDate" type='datefield' value="#planningDueDate#"></input>
									</td>
							</tr>
							<tr id="executionDateInput">
								<th>Execution Due Date:</th>
								<td>
									<cfinput name="programmingDueDate" id="programmingDueDate" type='datefield' value="#programmingDueDate#"></input>
								</td>	
							</tr>
							<tr id="controlDateInput">
								<th>Control End Date:</th>
								<td>
									<cfinput name="warrantyEndDate" id="warreatyEndDate" type='datefield' value="#warrantyEndDate#"/>
								</td>
							</tr>
	  						<tr>
								<th>Status:</th>
								<td>
									<select id="projectStatus" name="projectStatus">
										<cfoutput query="populateStatus">
											<option value="#populateStatus.statusID#" <cfif status is populateStatus.statusID>selected</cfif>>#populateStatus.status#</option>
										</cfoutput>
									</select>
								</td>
	  						</tr>
							<tr>
								<th>Resource:</th>
								<td>
									<select id="resources" name="resources">
										<option value="0">Please Select</option>
										<cfoutput query="populateResources">
											<option value="#populateResources.adminuserid#" <cfif resources is populateResources.adminuserid>selected</cfif> >#populateResources.lastname#,#populateResources.firstname#</option>
										</cfoutput>
									</select>
								</td>
							</tr>
							<tr>
								<cfif projectID is "0">
								<th>
									<button type="submit" id="submitNewProject">Save</button>
								</th>
								<cfelse>
								<th>
									<button type="submit" id="updateProject">Update</button>
								</th>
								</cfif>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	    </tr>
	</table>
	</cfform>
	<br />
	<cfif #projectID# is not "0">
		<cfform name="saveAdditionalresources" method="post" action="saveAdditionalResources.cfm?projectID=#projectID#">
		<table  id="additionalResourcesInput" width="900" border="0" cellspacing="0" cellpadding="5">
	    <tr>
	        <th class="highlightbar">Add additional resources:</th>
	    </tr>
	    <tr>
	        <td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="5">
							<tr>
								<th>Project Phase:</th>
								<td>
									<select id="additionalPhases" name="additionalPhases">
										<option value="0">Please Select</option>
										<cfoutput query="populateProgrammingPhases">
											<option value="#populateProgrammingPhases.PhaseID#">#populateProgrammingPhases.phaseName#</option>
										</cfoutput>
									</select>
								</td>
								<th>Additional Resource:</th>
								<td>
									<select id="additionalResources" name="additionalResources">
										<option value="0">Please Select</option>
										<cfoutput query="populateResources">
											<option value="#populateResources.adminuserid#">#populateResources.lastname#,#populateResources.firstname#</option>
										</cfoutput>	
									</select>
								</td>
							</tr>
							<tr>
								<td>
									<button type="submit" id="addResource">Add</button>
								</td>
							</tr>
							<tr>
								<th>Currently assigned additional resources:</th>
							</tr>
							<tr>
								<th>Phase:</th>
								<th>Resource:</th>
							</tr>
							<cfoutput query="getAdditionalResources">
							<tr>
								<td>#additionalPhase#</td>
								<td>#additionalFirst# #additionalLast#</td>
							</tr>
							</cfoutput>	
						</table>
					</td>	
				</tr>	
			</table>
			</td>
		</tr>
	</table>
	</cfform>
	</cfif>
<br />
<a class='normal' style="text-decoration:underline;" href='/index.cfm'>Return to Intranet</a>
	</div>
