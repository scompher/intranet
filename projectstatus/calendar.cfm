<cfquery name="getCalendarData" datasource="#DS#">
	select projectTracker_main.*, Admin_Users.firstname, Admin_users.lastname
	from projectTracker_main
	left join Admin_Users
	on projectTracker_main.resources=Admin_Users.adminuserid
	where projectTracker_main.active = 1
</cfquery>
<head>
<link rel="stylesheet" type="text/css" href="../styles.css"></link>
<link rel="stylesheet" type="text/css" href="calendar/fullcalendar.min.css"></link>
<script src="calendar/lib/jquery.min.js"></script>
<script src="calendar/lib/moment.min.js"></script>
<script src="calendar/fullcalendar.min.js"></script>
<cfoutput>
	<script>
		$(document).ready(function() {

		    // page is now ready, initialize the calendar...
		    $('##calendar').fullCalendar({ 
		    		
	    		  
	    events: [
			<cfloop query="getCalendarData" >
				<cfif getCalendarData.projectStartDate is not "" and getCalendarData.projectDueDate is not "">
		        {
		            title: '#getCalendarData.projectName# <cfif getCalendarData.firstName is not "">- #getCalendarData.firstname#'</cfif>,
		            start: '#DateFormat(getCalendarData.projectStartDate,"YYYY-MM-DD")#',
		            end: '#DateFormat(getCalendarData.projectDueDate,"YYYY-MM-DD")#',
		            url: 'projectView.cfm?projectID=#getCalendarData.projectID#'
		        },
		        </cfif>
			</cfloop>
			<cfloop query="getCalendarData">
				<cfif getCalendarData.programmingDueDate is not "">
				{
					title: '#getCalendarData.projectName#',
					start: '#DateFormat(getCalendarData.programmingDueDate,"YYYY-MM-DD")#',
					backgroundColor: 'orange',
		            url: 'projectView.cfm?projectID=#getCalendarData.projectID#'				
				},
				</cfif>
			</cfloop>
			<cfloop query="getCalendarData">
				<cfif getCalendarData.planningDueDate is not "">
				{
					title: '#getCalendarData.projectName#',
					start: '#DateFormat(getCalendarData.planningDueDate,"YYYY-MM-DD")#',
					backgroundColor: 'red',
		            url: 'projectView.cfm?projectID=#getCalendarData.projectID#'
				},
				</cfif>
			</cfloop>
			<cfloop query="getCalendarData">
				<cfif getCalendarData.testingDueDate is not "">
				{
					title: '#getCalendarData.projectName#',
					start: '#DateFormat(getCalendarData.testingDueDate,"YYYY-MM-DD")#',
					backgroundColor: 'yellow',
					textColor: 'black',
		            url: 'projectView.cfm?projectID=#getCalendarData.projectID#'
				},
				</cfif>
			</cfloop>
			<cfloop query="getCalendarData">
				<cfif getCalendarData.rolloutMeetingDate is not "">
				{
					title: '#getCalendarData.projectName#',
					start: '#DateFormat(getCalendarData.rolloutMeetingDate,"YYYY-MM-DD")#',
					backgroundColor: 'green',
		            url: 'projectView.cfm?projectID=#getCalendarData.projectID#'
				},
				</cfif>
			</cfloop>

		    ],
		    
		    })
		});
	</script>
</cfoutput>
</head>
<div align="center"><div style="height:1000px;width:1000px"><div id='calendar'>
	<br/>
	<b>
	<span>Legend:</span>
	<span style="border-radius:5px;padding:2px;background:red;color:white;width:200px;border:solid 1px black;">Planning Due Date</span>

	<span style="border-radius:5px;padding:2px;background:orange;color:black;width:200px;border:solid 1px black;">Programming Due Date</span>

	<span style="border-radius:5px;padding:2px;background:yellow;color:black;width:200px;border:solid 1px black;">Testing Due Date</span>

	<span style="border-radius:5px;padding:2px;background:green;color:white;width:200px;border:solid 1px black;">Rollout Meeting Date</span>
	</b>
</div>
<br />
<a class='normal' style="text-decoration:underline;" href='/index.cfm'>Return to Intranet</a>
</div></div>

