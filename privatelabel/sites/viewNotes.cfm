
<cfquery name="getNotes" datasource="copalink_cobrand">
	select * from site_notes where siteid = #sid# order by noteDateTime asc 
</cfquery>

<link rel="stylesheet" type="text/css" href="/styles.css">

<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<cfif getNotes.recordcount gt 0>
	<cfoutput query="getNotes">
    <tr>
        <td><b>#dateformat(noteDateTime,'mm/dd/yyyy')# #timeformat(noteDateTime,'hh:mm: tt')#</b></td>
    </tr>
    <tr>
        <td>
		#replace(noteText,chr(13),"<br>","all")#
		</td>
    </tr>
	<tr><td>&nbsp;</td></tr>
	</cfoutput>
	<cfelse>
	<tr>
		<td>There are no notes on file</td>
	</tr>
	</cfif>
</table>

