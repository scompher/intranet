<link rel="stylesheet" type="text/css" href="/styles.css">

<cfparam name="sid" default="0">

<cfset imagelocation = "e:\websites\cobrand\custimages\#form.siteIdentifier#">
<cfset err = "">

<cfif trim(err) is "">
	<!--- save image files --->
	<cftry>
	<cfif not directoryExists("e:\websites\cobrand\d\#siteIdentifier#")>
		<cfdirectory action="create" directory="e:\websites\cobrand\d\#form.siteIdentifier#">
		<cffile action="copy" source="e:\websites\cobrand\d\temp\index.cfm" destination="e:\websites\cobrand\d\#form.siteIdentifier#\index.cfm">
	</cfif>
	<cfif not directoryExists("#imagelocation#")>
		<cfdirectory action="create" directory="#imagelocation#">
	</cfif>
	<cfif trim(desktopBannerImage) is not "">
		<cffile action="upload" filefield="desktopBannerImage" destination="#imagelocation#\" nameconflict="overwrite">
		<cfset desktopBannerImageSavedFile = file.ServerFile>
		<cfset desktopBannerImageUploadedFile = file.ClientFile>
	</cfif>
	<cfif trim(desktopBannerBGImage) is not "">
		<cffile action="upload" filefield="desktopBannerBGImage" destination="#imagelocation#\" nameconflict="overwrite">
		<cfset desktopBannerBGImageSavedFile = file.ServerFile>
		<cfset desktopBannerBGImageUploadedFile = file.ClientFile>
	</cfif>
	<cfif trim(mobileSiteBanner) is not "">
		<cffile action="upload" filefield="mobileSiteBanner" destination="#imagelocation#\" nameconflict="overwrite">
		<cfset mobileSiteBannerSavedFile = file.ServerFile>
		<cfset mobileSiteBannerUploadedFile = file.ClientFile>
	</cfif>
	<cfif trim(mobileSiteDesktopIcon) is not "">
		<cffile action="upload" filefield="mobileSiteDesktopIcon" destination="#imagelocation#\" nameconflict="overwrite">
		<cfset mobileSiteDesktopIconSavedFile = file.ServerFile>
		<cfset mobileSiteDesktopIconUploadedFile = file.ClientFile>
	</cfif>
	<cfcatch type="any">
		<cfset err = listappend(err,"An error occurred while saving the image files:","~")>
		<cfset err = listappend(err,"#cfcatch.Message#","~")>
	</cfcatch>
	</cftry>
</cfif>

<cfif trim(err) is "">
	<!--- save site info --->
	<cftry>
	<!---
	<cfquery name="insertsite" datasource="copalink_cobrand">
		insert into site_main (dealerNumber, dealerid, adminid, dealerpw, logoimage, barcolor, menucolor, menutxtcolor, menutxthovercolor, imageCollage, logobgcolor, siteurl, returnurl, logincomments, contactemail, centerlogo, live, logoBgImage, launchDate, buttonScheme, mobileLogo, mobileIcon, mobileLogoBGColor, serviceDepartmentPhone, serviceDepartmentEmail, menuHovercolor)
		values ('#dealerNumber#', '#siteIdentifier#', '#dealerNumber#', '#dealerPasscode#', '#siteIdentifier#/#desktopBannerImageSavedFile#', '#barColor#', '#menucolor#', '#menuTextColor#', '#menuTextHoverColor#', '', '#desktopSiteBannerBGColor#', 'http://#siteIdentifier#.alarminfo.net', 'http://#siteIdentifier#.alarminfo.net', '', '#contactFormEmailAddress#', 
		#centerDesktopSiteLogo#, 0, '#siteIdentifier#/#desktopBannerBGImageSavedFile#', NULL, '#buttonTheme#', '#mobileSiteBannerSavedFile#', '#mobileSiteDesktopIconSavedFile#', '#mobileSiteBGColor#', '#serviceDepartmentPhoneNumber#', '#contactFormEmailAddress#', '#menuHoverColor#')
	</cfquery>
	--->
	<cfquery name="updateSite" datasource="copalink_cobrand">
		update site_main 
		set 
			<cfif trim(desktopBannerImage) is not "">
			logoimage = '#siteIdentifier#/#desktopBannerImageSavedFile#', 
			</cfif>
			barcolor = '#barColor#', 
			menucolor = '#menuColor#', 
			menutxtcolor = '#menuTextColor#', 
			menutxthovercolor = '#menuTextHoverColor#', 
			logobgcolor = '#desktopSiteBannerBGColor#', 
			contactemail = '#contactFormEmailAddress#', 
			centerlogo = #centerDesktopSiteLogo#, 
			<cfif trim(desktopBannerBGImage) is not "">
			logoBgImage = '#siteIdentifier#/#desktopBannerBGImageSavedFile#', 
			</cfif>
			buttonScheme = '#buttonTheme#', 
			<cfif trim(mobileSiteBanner) is not "">
			mobileLogo = '#mobileSiteBannerSavedFile#', 
			</cfif>
			<cfif trim(mobileSiteDesktopIcon) is not "">
			mobileIcon = '#mobileSiteDesktopIconSavedFile#', 
			</cfif>
			mobileLogoBGColor = '#mobileSiteBGColor#', 
			serviceDepartmentPhone = '#serviceDepartmentPhoneNumber#', 
			serviceDepartmentEmail = '#contactFormEmailAddress#', 
			menuHovercolor = '#menuHoverColor#' 
		where siteid = #sid# 
	</cfquery>
	<cfcatch type="any">
		<cfset err = listappend(err,"An error occurred while saving the site data:","~")>
		<cfset err = listappend(err,"#cfcatch.Message#","~")>
	</cfcatch>
	</cftry>
</cfif>

<cfif trim(err) is "">
	<cflocation url="/privatelabel/index.cfm">
<cfelse>
<div align="center">
<table width="600" border="0" cellspacing="0" cellpadding="5">
    <tr>
        <td class="highlightbar"><b>Add a Site - Errors Found</b></td>
    </tr>
    <tr>
        <td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td>
					<b>The following errors were found with your submission:</b>
					</td>
				</tr>
				<tr>
					<td>
					<ul>
					<cfloop list="#err#" delimiters="~" index="e">
					<cfoutput>
					<li>#e#</li>
					</cfoutput>
					</cfloop>
					</ul>
					</td>
				</tr>
			</table>
		</td>
    </tr>
</table>
<br />
<a style="text-decoration:underline;" class="normal" href="javascript:history.go(-1);">Please return and correct</a>
</div>
</cfif>