
<!--- close out site --->
<cfquery name="removesite" datasource="copalink_cobrand">
	update site_main 
	set closedOut = 1, inDevelopment = 0, live = 0 
	where siteid = #sid# 
</cfquery>

<!--- update site status and make note --->
<cfquery name="updateStatus" datasource="copalink_cobrand">
	update site_main 
	set currentStatus = 'Closed out' 
	where siteid = #sid# 
</cfquery>
<cfquery name="updateStatus" datasource="copalink_cobrand">
	insert into site_notes (siteid, noteDateTime, noteText) 
	values (#sid#, getdate(), 'Closed out site') 
</cfquery>

<cflocation url="/privatelabel/index.cfm">

