
<link rel="stylesheet" type="text/css" href="/styles.css">

<cfif isDefined("form.btnLaunch")>

<cfmail from="scompher@copsmonitoring.com" to="#toEmail#" subject="#subject#" username="copalink@copsmonitoring.com" password="copsmoncal">
#trim(content)#
</cfmail>

	<cfquery name="getIdentifier" datasource="copalink_cobrand">
		select dealerid from site_main 
		where siteid = #sid# 
	</cfquery>
	
	<cfquery name="launchSite" datasource="copalink_cobrand">
		update site_main 
		set currentStatus = 'Launched', launchDate = getdate(), live = 1, inDevelopment = 0, siteURL = 'http://#getIdentifier.dealerID#.alarminfo.net', returnURL = 'http://#getIdentifier.dealerID#.alarminfo.net' 
		where siteid = #sid# 
	</cfquery>
	<cfquery name="updateNotes" datasource="copalink_cobrand">
		insert into site_notes (siteid, noteDateTime, noteText) 
		values (#sid#, getdate(), 'Launched Site') 
	</cfquery>

	<div align="center">
	<table width="400" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td class="highlightbar"><b>Launch Private Label Site</b></td>
		</tr>
		<tr>
			<td class="greyrowbottomnopadding">
				<cfoutput>
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td>Site at <a target="_blank" style="text-decoration:underline;" href="http://#getIdentifier.dealerID#.alarminfo.net">http://#getIdentifier.dealerID#.alarminfo.net</a> Launched Successfully</td>
					</tr>
				</table>
				</cfoutput>
			</td>
		</tr>
	</table>
	<br>
	<a class="normal" style="text-decoration:underline;" href="/privatelabel/index.cfm">Return to site management</a>
	</div>

<cfelse>

<cfquery name="getSiteInfo" datasource="copalink_cobrand">
	select * from site_main
	where siteid = #sid# 
</cfquery>

<cfset emailSubject = "Congratulations, your C.O.P.S. Monitoring private label is now live!">
<cfset emailContent = "
Hello,

Your private-label MPower subscriber site is complete.

Here is the URL for the website that you would link to, so your users can log-in.

http://#getSiteInfo.dealerid#.alarminfo.net
 
You simply put a link on your website which points to that URL so your customers can log into your private-label MPower subscriber access site.  
 
You also have a mobile version of your private label site, that address is: 

http://#getSiteInfo.dealerid#.alarminfo.net (the site will detect your mobile browser and re-direct you to the proper site) or you can reach it directly by going to http://#getSiteInfo.dealerid#.alarminfo.net/m
 
If you have any questions, please don't hesitate to contact me. 

Stephen Compher
scompher@copsmonitoring.com
Web Developer
C.O.P.S. Monitoring
800-367-2677 ext. 1716
">

<div align="center">
<form method="post" action="launchSite.cfm">
<input type="hidden" name="sid" value="<cfoutput>#sid#</cfoutput>" />
<table width="950" border="0" cellspacing="0" cellpadding="5">
    <tr>
        <td class="highlightbar"><b>Launch Private Label Site</b></td>
    </tr>
    <tr>
        <td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td width="10%" nowrap><b>To:</b></td>
				    <td width="90%">
				        <input type="text" name="toEmail" style="width:100%;" value="<cfoutput>#getsiteinfo.contactEmail#</cfoutput>">
				    </td>
				</tr>
				<tr>
				    <td nowrap><b>Subject:</b></td>
				    <td>
				        <input type="text" name="subject" style="width:100%;" value="<cfoutput>#trim(emailSubject)#</cfoutput>">
				    </td>
				</tr>
				<tr>
				    <td colspan="2"><b>Content:</b></td>
			    </tr>
				<cfoutput>
				<tr>
				    <td colspan="2">
				        <textarea name="content" rows="10" style="width:100%">#trim(emailContent)#</textarea>
				    </td>
			    </tr>
				</cfoutput>
				<tr>
				    <td colspan="2">
				        <input type="submit" name="btnLaunch" value="Launch Site">
				        <input type="reset" name="btnReset" value="Reset Form">
						<input type="button" name="btnCancel" value="Cancel" onclick="javascript:history.go(-1);">
				    </td>
				</tr>
			</table>
		</td>
    </tr>
</table>
</form>
<br>
<a class="normal" style="text-decoration:underline;" href="/privatelabel/index.cfm">Return to previous page</a>
</div>

</cfif>
