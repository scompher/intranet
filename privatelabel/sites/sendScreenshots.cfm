<cfparam name="sid" default="0">

<cfquery name="getSiteInfo" datasource="copalink_cobrand">
	select * from site_main
	where siteid = #sid# 
</cfquery>

<link rel="stylesheet" type="text/css" href="/styles.css">

<cfset emailSubject = "Your COPS Monitoring private label site screen shots are ready for approval">
<cfset emailContent = "
Hello, here are the screen shots for your private label site.  Items I can change are the top banner image as well as the site and menu colors. 

If all looks good then just give me the OK and I will make it live ASAP. Once it goes live you will receive a welcome email containing all the information for the site including the web address to link to your site and the web address for the mobile version. 

If you have any questions feel free to contact me. 


Stephen Compher
Web Developer
C.O.P.S. Monitoring
800-367-2677 ext. 1716
scompher@copsmonitoring.com 
">

<div align="center">
<form method="post" action="sendScreenshots_send.cfm" enctype="multipart/form-data">
<input type="hidden" name="sid" value="<cfoutput>#sid#</cfoutput>" />
<table width="950" border="0" cellspacing="0" cellpadding="5">
    <tr>
        <td class="highlightbar"><b>Send Private Label Screenshots </b></td>
    </tr>
    <tr>
        <td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td width="10%" nowrap><b>To:</b></td>
				    <td width="90%">
				        <input type="text" name="toEmail" style="width:100%;" value="<cfoutput>#getsiteinfo.contactEmail#</cfoutput>">
				    </td>
				</tr>
				<tr>
				    <td nowrap><b>Subject:</b></td>
				    <td>
				        <input type="text" name="subject" style="width:100%;" value="<cfoutput>#trim(emailSubject)#</cfoutput>">
				    </td>
				</tr>
				<tr>
				    <td colspan="2"><b>Content:</b></td>
			    </tr>
				<cfoutput>
				<tr>
				    <td colspan="2">
				        <textarea name="content" rows="10" style="width:100%">#trim(emailContent)#</textarea>
				    </td>
			    </tr>
				</cfoutput>
				<tr>
					<td nowrap><b>Desktop Screenshot 1:</b></td>
					<td><input type="file" name="screenshot1"></td>
				</tr>
				<tr>
					<td nowrap><b>Desktop Screenshot 2:</b></td>
					<td><input type="file" name="screenshot2"></td>
				</tr>
				<tr>
					<td nowrap><b>Mobile Screenshot 1:</b></td>
					<td><input type="file" name="mobilescreenshot1"></td>
				</tr>
				<tr>
					<td nowrap><b>Mobile Screenshot 2:</b></td>
					<td><input type="file" name="mobilescreenshot2"></td>
				</tr>
				<tr>
				    <td colspan="2">
				        <input type="submit" name="btnSend" value="Send Email">
				        <input type="reset" name="btnReset" value="Reset Form">
						<input type="button" name="btnCancel" value="Cancel" onclick="javascript:history.go(-1);">
				    </td>
				</tr>
			</table>
		</td>
    </tr>
</table>
</form>
<br>
<a class="normal" style="text-decoration:underline;" href="/privatelabel/index.cfm">Return to previous page</a>
</div>
