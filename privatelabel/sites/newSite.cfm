
<link rel="stylesheet" type="text/css" href="/styles.css">

<cfif isDefined("form.btnAddSite")>
	<cfset err = "">
	<!--- check date validity --->
	<cfif datecompare(dateReceived,now()) is 1>
		<cfset err = listappend(err,"The date received cannot be after todays date.","~")>
	</cfif>
	<!--- check if site or name exists --->
	<cfquery name="checkName" datasource="copalink_cobrand">
		select * 
		from site_main 
		where dealerid = '#trim(siteIdentifier)#' 
	</cfquery>
	<cfif checkName.recordcount gt 0>
		<cfset err = listappend(err,"The site identifier already exists.","~")>
	</cfif>
	<cfquery name="checkDealerNumber" datasource="copalink_cobrand">
		select * 
		from site_main 
		where dealerNumber = '#trim(dealerNumber)#' 
	</cfquery>
	<cfif checkDealerNumber.recordcount gt 0>
		<cfset err = listappend(err,"The dealer number already exists with a different site.","~")>
	</cfif>

	<cfif trim(err) is "">
		<cfquery name="saveSiteInfo" datasource="copalink_cobrand">
			insert into site_main (dealerNumber, dealerID, adminID, dealerPW, inDevelopment, dateTimeReceived, currentStatus) 
			values ('#dealerNumber#', '#siteIdentifier#', '#dealerNumber#', '#dealerPasscode#', 1, '#dateReceived#', 'Received')
		</cfquery>
		<cflocation url="/privatelabel/index.cfm">
	<cfelse>
	<br />
	<div align="center">
	<table width="600" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td class="highlightbar"><b>Private Label Sites - Add New Site</b></td>
		</tr>
		<tr>
			<td class="greyrowbottomnopadding">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td><b>The following errors occurred:</b></td>
					</tr>
					<tr>
						<td>
						<ul>
						<cfloop list="#err#" delimiters="~" index="e">
						<cfoutput>
						<li>#e#</li>
						</cfoutput>
						</cfloop>
						</ul>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<br />
	<a class="normal" style="text-decoration:underline;" href="javascript:history.go(-1);">Please return and fix</a>
	</div>
	</cfif>
	<cfabort>
</cfif>

<script type="text/javascript">
function updateURL(valueToUpdate, theItem) {
	var span = document.getElementById(theItem);
	
	while( span.firstChild ) {
		span.removeChild( span.firstChild );
	}
	span.appendChild( document.createTextNode(valueToUpdate.toLowerCase()) );
}
</script>

<br />

<div align="center">
<cfform method="post">
<table width="600" border="0" cellspacing="0" cellpadding="5">
    <tr>
        <td class="highlightbar"><b>Private Label Sites - Add New Site</b></td>
    </tr>
    <tr>
        <td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td width="42%" nowrap class="linedrowrightcolumn">Site Identifier </td>
					<td width="58%" class="linedrow">
					    <cfinput type="text" name="siteIdentifier" style="width:300px;" onkeyup="updateURL(this.value, 'siteid');" required="yes" message="The site identifier is required.">
					</td>
				</tr>
				<tr>
				    <td colspan="2" nowrap class="linedrowrightcolumn">Based on the site identifier, the site URL will be: http://<span id="siteid"></span>.alarminfo.net</td>
				    </tr>
				<tr>
				    <td nowrap class="linedrowrightcolumn">Dealer # </td>
				    <td class="linedrow">
				        <cfinput name="dealerNumber" type="text" id="dealerNumber" style="width:300px;" required="yes" message="The dealer number is required." />
				    </td>
				    </tr>
				<tr>
				    <td nowrap class="linedrowrightcolumn">Dealer Password </td>
				    <td class="linedrow">
				        <cfinput name="dealerPasscode" type="text" id="dealerPasscode" style="width:300px;" required="yes" message="The dealer password is required." />
				    </td>
				    </tr>
				<tr>
				<tr>
				    <td nowrap class="linedrowrightcolumn">Date Received</td>
				    <td class="linedrow">
						<cfinput type="datefield" name="dateReceived" validate="date" value="#dateformat(now(),'mm/dd/yyyy')#" style="width:75px;" required="yes" message="A valid date received is required.">
				    </td>
				    </tr>
				<tr>
			</table>
		</td>
    </tr>
	<tr>
		<td>
		<cfinput type="submit" name="btnAddSite" value="Add Site" />
		&nbsp;
		<cfinput name="btnReset" type="reset" value="Clear Form">
		&nbsp;
		<cfinput type="button" onClick="document.location = '/privatelabel/index.cfm';" name="btnCancel" value="Cancel" />
		</td>
	</tr>
</table>
</cfform>
</div>

