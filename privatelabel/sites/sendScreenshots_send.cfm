<link rel="stylesheet" type="text/css" href="/styles.css">

<cfquery name="getSiteInfo" datasource="copalink_cobrand">
	select * from site_main 
	where siteid = #sid# 
</cfquery>

<cfset imagelocation = "e:\websites\cobrand\custimages\#getSiteInfo.dealerid#">

<!--- save attachments --->
<cfif trim(screenshot1) is not "">
	<cffile action="upload" filefield="screenshot1" destination="#imagelocation#\" nameconflict="overwrite">
	<cfset screenshot1SavedFile = file.ServerFile>
	<cfset screenshot1UploadedFile = file.ClientFile>
</cfif>
<cfif trim(screenshot2) is not "">
	<cffile action="upload" filefield="screenshot2" destination="#imagelocation#\" nameconflict="overwrite">
	<cfset screenshot2SavedFile = file.ServerFile>
	<cfset screenshot2UploadedFile = file.ClientFile>
</cfif>
<cfif trim(mobilescreenshot1) is not "">
	<cffile action="upload" filefield="mobilescreenshot1" destination="#imagelocation#\" nameconflict="overwrite">
	<cfset mobilescreenshot1SavedFile = file.ServerFile>
	<cfset mobilescreenshot1UploadedFile = file.ClientFile>
</cfif>
<cfif trim(mobilescreenshot2) is not "">
	<cffile action="upload" filefield="mobilescreenshot2" destination="#imagelocation#\" nameconflict="overwrite">
	<cfset mobilescreenshot2SavedFile = file.ServerFile>
	<cfset mobilescreenshot2UploadedFile = file.ClientFile>
</cfif>

<cfmail from="scompher@copsmonitoring.com" to="#toEmail#" subject="#subject#" username="copalink@copsmonitoring.com" password="copsmoncal">
#trim(content)#
<cfmailparam disposition="inline" file="#imagelocation#\#screenshot1SavedFile#">
<cfmailparam disposition="inline" file="#imagelocation#\#screenshot2SavedFile#">
<cfmailparam disposition="inline" file="#imagelocation#\#mobilescreenshot1SavedFile#">
<cfmailparam disposition="inline" file="#imagelocation#\#mobilescreenshot2SavedFile#">
</cfmail>

<!--- update status and add note --->
<cfquery name="updateStatus" datasource="copalink_cobrand">
	update site_main 
	set currentStatus = 'Waiting on Dealer' 
	where siteid = #sid# 
</cfquery>
<cfquery name="updateStatus" datasource="copalink_cobrand">
	insert into site_notes (siteid, noteDateTime, noteText) 
	values (#sid#, getdate(), 'Sent screen shots') 
</cfquery>

<div align="center">
<table width="400" border="0" cellspacing="0" cellpadding="5">
    <tr>
        <td class="highlightbar"><b>Send Private Label Screenshots </b></td>
    </tr>
    <tr>
        <td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td>E-mail Sent Successfully</td>
				</tr>
			</table>
		</td>
    </tr>
</table>
<br>
<a class="normal" style="text-decoration:underline;" href="/privatelabel/index.cfm">Return to site management</a>
</div>
