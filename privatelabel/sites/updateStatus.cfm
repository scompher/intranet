<cfsetting showdebugoutput="no">

<link rel="stylesheet" type="text/css" href="/styles.css">

<cfif isDefined("form.btnSaveStatus")>
	
	<cfquery name="updateStatus" datasource="copalink_cobrand">
		update site_main 
		set currentStatus = '#status#' 
		where siteid = #sid# 
	</cfquery>
	<cfif trim(notes) is not "">
		<cfquery name="updateStatus" datasource="copalink_cobrand">
			insert into site_notes (siteid, noteDateTime, noteText) 
			values (#sid#, getdate(), '#notes#') 
		</cfquery>
	</cfif>
	<script type="text/javascript">
	opener.location = opener.location;
	window.close();
	</script>
	<cfabort>
</cfif>

<cfquery name="getcurrentstatus" datasource="copalink_cobrand">
	select * from site_main 
	where siteid = #sid# 
</cfquery>

<div align="center">
<table width="312" border="0" cellspacing="0" cellpadding="5">
    <tr>
        <td width="302" class="highlightbar"><b>Update Site Status</b></td>
    </tr>
    <tr>
        <td class="greyrowbottomnopadding">
			<form method="post">
			<input type="hidden" name="sid" value="<cfoutput>#sid#</cfoutput>">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td>
					<select name="status" style="width:300px;">
						<option value="">Please Select...</option>
						<option <cfif getcurrentstatus.currentstatus is "Received">selected</cfif> value="Received">Received</option>
						<option <cfif getcurrentstatus.currentstatus is "In Progress">selected</cfif> value="In Progress">In Progress</option>
						<option <cfif getcurrentstatus.currentstatus is "Waiting on Dealer">selected</cfif> value="Waiting on Dealer">Waiting on Dealer</option>
						<option <cfif getcurrentstatus.currentstatus is "Launched">selected</cfif> value="Launched">Launched</option>
					</select>
					</td>
				</tr>
				<tr>
				    <td>Relevant Notes:</td>
				    </tr>
				<tr>
				    <td>
				        <textarea name="notes" style="width:300px;" rows="5"></textarea>
				    </td>
				    </tr>
				<tr>
					<td>
					<input type="submit" name="btnSaveStatus" value="Update Status">
					</td>
				</tr>
			</table>
			</form>
		</td>
    </tr>
</table>
</div>
