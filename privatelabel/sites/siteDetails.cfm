
<cfparam name="emsg" default="">
<cfparam name="sid" default="0">

<script type="text/javascript">
function updateURL(valueToUpdate, theItem) {
	var span = document.getElementById(theItem);
	
	while( span.firstChild ) {
		span.removeChild( span.firstChild );
	}
	span.appendChild( document.createTextNode(valueToUpdate.toLowerCase()) );
}
</script>

<link rel="stylesheet" type="text/css" href="/styles.css">

<style type="text/css">
	.alerttextlarge {font-family:Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold; color:#FFFFFF; border:solid thin #000000; background-color:#FF0000; padding:5px}
</style>

<!--- get site info --->
<cfquery name="getSiteInfo" datasource="copalink_cobrand">
	select * from site_main 
	where siteid = #sid# 
</cfquery>

<cfquery name="getbuttons" datasource="copalink_cobrand">
	select * from mobileCustomButtons 
	order by cssName asc 
</cfquery>

<cfdirectory action="list" name="getImages" directory="e:\websites\cobrand\mobile\iui\t\default\custombuttons\" type="file" filter="*.png">
<cfset totalimages = getimages.recordcount>
<cfset cols = ceiling(totalimages / 5)>

<div align="center">
<cfif trim(emsg) is not "">
<table width="600" border="0" cellspacing="0" cellpadding="5" class="alerttextlarge">
	<tr>
		<td>
		<cfloop list="#emsg#" delimiters="~" index="e">
		<li>#e#</li>
		</cfloop>
		</td>
	</tr>
</table>
<br />
</cfif>
<form method="post" action="saveDetails.cfm" enctype="multipart/form-data">
<cfoutput>
<input type="hidden" name="siteIdentifier" value="#getsiteinfo.dealerid#" />
<input type="hidden" name="sid" value="#getsiteinfo.siteID#" />
</cfoutput>
<table width="600" border="0" cellspacing="0" cellpadding="5">
    <tr>
        <td class="highlightbar"><b>Add a Site - Site Details</b></td>
    </tr>
    <tr>
        <td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
				    <td colspan="2" nowrap class="linedrowrightcolumn">All colors must be in HEX format (i.e. RRGGBB FF0000 for red)</td>
			    </tr>
				<cfoutput>
				<tr>
					<td width="42%" nowrap class="linedrowrightcolumn">Site Identifier </td>
					<td width="58%" class="linedrow">
					#getSiteInfo.dealerid#
					</td>
				</tr>
				<tr>
				    <td colspan="2" nowrap class="linedrowrightcolumn">Based on the site identifier, the site URL will be: http://#getsiteinfo.dealerid#.alarminfo.net</td>
			    </tr>
				<tr>
				    <td nowrap class="linedrowrightcolumn">Dealer ## </td>
				    <td class="linedrow">
					#getSiteInfo.dealerNumber#
				    </td>
				    </tr>
				<tr>
				    <td nowrap class="linedrowrightcolumn">Dealer Password </td>
				    <td class="linedrow">
				    #getSiteInfo.dealerPW#
				    </td>
			    </tr>
				<tr>
				    <td nowrap class="linedrowrightcolumn">Bar Color</td>
				    <td class="linedrow">
				        <input name="barColor" type="text" id="barColor" value="#getsiteinfo.barcolor#" style="width:300px;" />
				    </td>
				    </tr>
				<tr>
				    <td nowrap class="linedrowrightcolumn">Menu Color</td>
				    <td class="linedrow">
				        <input name="menuColor" type="text" id="menuColor" value="#getsiteinfo.menuColor#" style="width:300px;" />
				    </td>
				</tr>
				<tr>
				    <td nowrap class="linedrowrightcolumn">Menu Text Color </td>
				    <td class="linedrow">
				        <input type="text" name="menuTextColor" value="#getsiteinfo.menuTxtColor#" style="width:300px;" />
				    </td>
				</tr>
				<tr>
				    <td nowrap class="linedrowrightcolumn">Menu Hover Color </td>
				    <td class="linedrow">
				        <input name="menuHoverColor" type="text" id="menuHoverColor" value="#getsiteinfo.menuHoverColor#" style="width:300px;" />
				    </td>
				    </tr>
				<tr>
				    <td nowrap class="linedrowrightcolumn">Menu Text Hover Color </td>
				    <td class="linedrow">
				        <input name="menuTextHoverColor" type="text" id="menuTextHoverColor" value="#getsiteinfo.menuTxtHoverColor#" style="width:300px;" />
				    </td>
				    </tr>
				<tr>
				    <td nowrap class="linedrowrightcolumn">Desktop Site Banner Image </td>
				    <td class="linedrow">
				        <input type="file" name="desktopBannerImage" style="width:300px;" value="" />
				    </td>
				</tr>
				<cfif trim(getsiteinfo.logoImage) is not "">
				<tr>
				    <td nowrap class="linedrowrightcolumn"></td>
				    <td class="linedrow">
					<a target="_blank" href="http://www.alarminfo.net/custimages/#getsiteinfo.logoImage#">[View Existing Image]</a>
				    </td>
				</tr>
				</cfif>
				<tr>
				    <td nowrap class="linedrowrightcolumn">Desktop Site Banner BG Color (optional) </td>
				    <td class="linedrow">
				        <input name="desktopSiteBannerBGColor" type="text" id="desktopSiteBannerBGColor" value="#getsiteinfo.logoBgColor#" style="width:300px;" />
				    </td>
				</tr>
				<tr>
				    <td nowrap class="linedrowrightcolumn">Desktop Site Banner BG Image (optional) </td>
				    <td class="linedrow">
				        <input type="file" name="desktopBannerBGImage" value="" style="width:300px;" />
				    </td>
				</tr>
				<cfif trim(getsiteinfo.logoBgImage) is not "">
				<tr>
				    <td nowrap class="linedrowrightcolumn"></td>
				    <td class="linedrow">
					<a target="_blank" href="http://www.alarminfo.net/custimages/#getsiteinfo.logoBgImage#">[View Existing Image]</a>
				    </td>
				</tr>
				</cfif>
				</cfoutput>
				<tr>
                    <td nowrap="nowrap" class="linedrowrightcolumn">Center Desktop Site Logo </td>
				    <td class="linedrow">
                        <select name="centerDesktopSiteLogo" id="centerDesktopSiteLogo">
                            <option value="">Please select...</option>
                            <option <cfif getsiteinfo.centerLogo is 1>selected</cfif> value="1">Yes</option>
                            <option <cfif getsiteinfo.centerLogo is 0>selected</cfif> value="0">No</option>
                        </select>
                    </td>
				    </tr>
				<cfoutput>
				<tr>
				    <td nowrap class="linedrowrightcolumn">Mobile Site Banner </td>
				    <td class="linedrow">
				        <input type="file" name="mobileSiteBanner" value="" style="width:300px;" />
				    </td>
				    </tr>
				<cfif trim(getsiteinfo.mobileLogo) is not "">
				<tr>
				    <td nowrap class="linedrowrightcolumn"></td>
				    <td class="linedrow">
					<a target="_blank" href="http://www.alarminfo.net/custimages/#getSiteInfo.dealerid#/#getsiteinfo.mobileLogo#">[View Existing Image]</a>
				    </td>
				</tr>
				</cfif>
				<tr>
				    <td nowrap class="linedrowrightcolumn">Mobile Site Banner BG Color</td>
				    <td class="linedrow">
				        <input type="text" name="mobileSiteBGColor" value="#getsiteinfo.mobileLogoBGColor#" style="width:300px;" />
				    </td>
				    </tr>
				<tr>
				    <td nowrap class="linedrowrightcolumn">Mobile Site Desktop Icon </td>
				    <td class="linedrow">
				        <input type="file" name="mobileSiteDesktopIcon" value="" style="width:300px;" />
				    </td>
			    </tr>
				<cfif trim(getsiteinfo.mobileIcon) is not "">
				<tr>
				    <td nowrap class="linedrowrightcolumn"></td>
				    <td class="linedrow">
					<a target="_blank" href="http://www.alarminfo.net/custimages/#getSiteInfo.dealerid#/#getsiteinfo.mobileIcon#">[View Existing Image]</a>
				    </td>
				</tr>
				</cfif>
				</cfoutput>
				<tr>
				    <td nowrap class="linedrowrightcolumn" valign="top">Mobile Site Button Theme </td>
				    <td class="linedrow" class="nopadding" valign="top">
					<table>
					<tr>
						<cfloop from="1" to="#cols#" index="c">
						<td valign="top">
						<cfset start = (c*5)-4>
						<cfoutput query="getButtons" startrow="#start#" maxrows="5">
						<table border="0" cellspacing="0" cellpadding="5">
							<tr>
								<td><input <cfif getsiteinfo.buttonScheme is getButtons.cssName>checked</cfif> type="radio" name="buttonTheme" value="#getButtons.cssName#" /></td>
								<td><img src="http://www.alarminfo.net/mobile/iui/t/default/custombuttons/#getButtons.fileName#" alt="#getButtons.buttonLabel#" /></td>
							</tr>
						</table>
						</cfoutput>
						</td>
						</cfloop>
					</tr>
					</table>
					</td>
			    </tr>
				<cfoutput>
				<tr>
				    <td nowrap class="linedrowrightcolumn">Contact Form Email Address </td>
				    <td class="linedrow">
				        <input name="contactFormEmailAddress" type="text" id="contactFormEmailAddress" value="#getsiteinfo.contactEmail#" style="width:300px;" />
				    </td>
				    </tr>
				<tr>
				    <td nowrap class="linedrowrightcolumn">Service Department Phone Number </td>
				    <td class="linedrow">
				        <input name="serviceDepartmentPhoneNumber" type="text" value="#getsiteinfo.serviceDepartmentPhone#" id="serviceDepartmentPhoneNumber" style="width:300px;" />
				    </td>
			    </tr>
				<tr>
				    <td nowrap class="linedrowrightcolumn"></td>
				    <td class="linedrow">
					Must be in the format 1-XXX-XXX-XXXX
				    </td>
			    </tr>
				<tr>
				    <td nowrap class="linedrowrightcolumn">Service Department Email Address </td>
				    <td class="linedrow">
				        <input type="text" name="serviceDepartmentEmailAddress" value="#getsiteinfo.serviceDepartmentEmail#" style="width:300px;" />
				    </td>
			    </tr>
				</cfoutput>
				<tr>
				    <td colspan="2" align="center">
					<input type="submit" name="btnSaveSite" value="Save Info" />
					&nbsp;
					<input type="reset" name="btnReset" value="Reset Form" />
					&nbsp;
					<input type="button" onclick="document.location='/privatelabel/index.cfm';" name="btnCancel" value="Cancel" />
					</td>
				    </tr>
			</table>
		</td>
    </tr>
</table>
</form>
</div>
