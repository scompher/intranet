
<link rel="stylesheet" type="text/css" href="/styles.css">

<cfif not isDefined("form.btnProcess")>

<br>

<div align="center">
<table width="600" border="0" cellspacing="0" cellpadding="5">
    <tr>
        <td class="highlightbar"><b>Private label site reconcilliation utility</b></td>
    </tr>
    <tr>
        <td class="greyrowbottomnopadding">
			<form method="post">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td>Paste report below</td>
				</tr>
				<tr>
					<td><textarea name="report" rows="20" style="width:600px;"></textarea></td>
				</tr>
				<tr>
					<td>
						<input type="submit" name="btnProcess" value="Process Report">
					</td>
				</tr>
			</table>
			</form>
		</td>
    </tr>
</table>
<br />
<a class="normal" style="text-decoration:underline;" href="/index.cfm">Return to Intranet</a>
</div>

<cfelse>

<cfset report = trim(report)>
<cfset startread = findnocase("dealer",report,1)>
<cfset report = mid(report,startread, len(report))>
<cfset report = replace(report,"#chr(32)##chr(32)##chr(32)##chr(32)##chr(32)##chr(32)##chr(32)##chr(32)##chr(32)#","#chr(9)#","all")>
<cfset report = replace(report,"#chr(32)##chr(32)##chr(32)##chr(32)##chr(32)#","#chr(9)#","all")>

<cfset counter = 1>
<cfset dealerList = "">
<cfloop list="#report#" index="line" delimiters="#chr(13)#">
	<cfset lineArray = line.split("#chr(9)#")>
	<cfif counter gt 2>
		<cfset dealerList = listappend(dealerList,"'#trim(lineArray[1])#'")>
		<!--- 
		<cfoutput>
		#lineArray[1]#~
		#lineArray[2]#~
		#lineArray[3]#
		</cfoutput>
		<br>
		 --->
	</cfif>
	<cfset counter = counter + 1>
</cfloop>

<cfquery name="checkDealersNotInList" datasource="copalink_cobrand">
	select dealernumber from site_main where dealernumber NOT IN (#preserveSingleQuotes(dealerList)#) and live = 1 and nocharge = 0
</cfquery>
<cfset NotBilled = valuelist(checkDealersNotInList.dealerNumber)>

<cfset ErrBilled = "">
<cfloop list="#dealerList#" index="dn">
	<cfquery name="CheckListExistance" datasource="copalink_cobrand">
		select dealernumber from site_main where dealernumber = #preserveSingleQuotes(dn)# and live = 1 
	</cfquery>
	<cfif CheckListExistance.recordcount is 0>
		<cfset ErrBilled = listappend(ErrBilled,dn)>
	</cfif>
</cfloop>

<cfset ErrBilled = replace(ErrBilled,"'","","all")>

<br />

<div align="center">
<table width="400" border="0" cellspacing="0" cellpadding="5">
    <tr>
        <td class="highlightbar"><b>Private label site reconcilliation utility</b></td>
    </tr>
    <tr>
        <td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td class="linedrowrightcolumn" width="50%">Site exits, no billing</td>
					<td class="linedrow" width="50%">Billing exists, no site</td>
				</tr>
				<cfoutput>
				<tr>
					<td class="linedrowrightcolumn" width="50%" valign="top">
					<cfloop list="#notbilled#" index="dn">
					#dn#<br>
					</cfloop>
					</td>
					<td class="linedrow" width="50%" valign="top">
					<cfloop list="#ErrBilled#" index="dn">
					#dn#<br>
					</cfloop>
					</td>
				</tr>
				</cfoutput>
			</table>
		</td>
    </tr>
</table>
<br />
<a class="normal" style="text-decoration:underline;" href="/index.cfm">Return to Intranet</a>
</div>

</cfif>