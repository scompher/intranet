
<link rel="stylesheet" type="text/css" href="/styles.css">

<cfset buttonImageDir = "e:\websites\cobrand\mobile\iui\t\default\custombuttons">

<cfif isDefined("form.btnAdd")>
	<cffile action="upload" filefield="buttonImage" destination="#buttonImageDir#" nameconflict="makeunique">
	<cfset buttonFile = file.ServerFile>
	<cfquery name="addButton" datasource="copalink_cobrand">
		insert into mobileCustomButtons (fileName, cssName, buttonLabel) 
		values ('#buttonFile#', '#cssLabel#', '#buttonLabel#') 
	</cfquery>
	<cfinclude template="writefiles.cfm">
	<cflocation url="index.cfm">
</cfif>

<div align="center">
<table border="0" cellspacing="0" cellpadding="5">
    <tr>
        <td class="highlightbar"><b>Add a custom button </b></td>
    </tr>
    <tr>
        <td class="greyrowbottomnopadding">
			<form method="post" enctype="multipart/form-data">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td width="15%" nowrap>Button label </td>
				    <td width="85%">
				        <input type="text" name="buttonLabel" style="width:300px;" maxlength="50">
				    </td>
				</tr>
				<tr>
				    <td nowrap>CSS label </td>
				    <td>
				        <input type="text" name="cssLabel" style="width:300px;" maxlength="50">
				    </td>
				</tr>
				<tr>
				    <td nowrap>Button Image </td>
				    <td><input type="file" name="buttonImage"></td>
			    </tr>
				<tr>
				    <td colspan="2">
				        <input name="btnAdd" type="submit" id="btnAdd" value="Add Button">
				        <input name="btnCancel" type="button" id="btnCancel" value="Cancel" onclick="document.location='index.cfm';">
				    </td>
				</tr>
			</table>
			</form>
		</td>
    </tr>
</table>
<br />
<a class="normal" style="text-decoration:underline;" href="/index.cfm">Return to Intranet</a>
</div>

