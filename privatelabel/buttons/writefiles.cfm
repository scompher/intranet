<cfset cssFile1 = "e:\websites\cobrand\mobile\iui\t\default\custombuttons.css">
<cfset cssFile2 = "e:\websites\cobrand\mobile\iui\custombuttons.css">

<cfquery name="getButtons" datasource="copalink_cobrand">
	select * from mobileCustomButtons 
	order by cssName asc 
</cfquery>

<!--- write css file 1 --->
<cffile action="write" file="#cssFile1#" output="" nameconflict="overwrite">
<cfoutput query="getButtons">
	<cfset line = ".#cssName# {-webkit-border-image: url(custombuttons/#fileName#) 0 12 0 12; -moz-border-image: url(custombuttons/#fileName#) 0 12 0 12; color: ##FFFFFF; height: 30px;}">
	<cffile action="append" file="#cssFile1#" output="#line#">
</cfoutput>


<!--- write css file 2 --->
<cfset buttonList = "">
<cfloop query="getButtons">
	<cfset buttonList = listappend(buttonList, "#chr(13)##chr(10)#.#cssName#")>
</cfloop>

<cfset fileContent = "
#buttonList# {
	display: block;
	border-width: 0 12px;
	padding: 10px;
	text-align: center;
	font-size: 22px;
	font-weight: bold;
	text-decoration: inherit;
	color: inherit;
}
">
<cffile action="write" file="#cssFile2#" output="#fileContent#" nameconflict="overwrite">
