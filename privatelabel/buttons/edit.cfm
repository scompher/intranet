
<link rel="stylesheet" type="text/css" href="/styles.css">

<cfset buttonImageDir = "e:\websites\cobrand\mobile\iui\t\default\custombuttons">

<cfif isDefined("form.btnEdit")>
	<cfif form.buttonImage is not "">
		<cffile action="upload" filefield="buttonImage" destination="#buttonImageDir#" nameconflict="makeunique">
		<cfset buttonFile = file.ServerFile>	
		<cfif fileExists("#buttonImageDir#\#form.currentImage#")>
			<cffile action="delete" file="#buttonImageDir#\#form.currentImage#">
		</cfif>
	<cfelse>
		<cfset buttonFile = form.currentImage>
	</cfif>
	<cfquery name="updateButtonInfo" datasource="copalink_cobrand">
		update mobileCustomButtons 
		set fileName = '#buttonFile#', cssName = '#form.cssLabel#', buttonLabel = '#form.buttonLabel#' 
		where buttonid = #bid# 
	</cfquery>
	<cfinclude template="writefiles.cfm">
	<cflocation url="index.cfm">
</cfif>

<cfquery name="getButton" datasource="copalink_cobrand">
	select * from mobileCustomButtons 
	where buttonid = #bid# 
</cfquery>

<div align="center">
<table border="0" cellspacing="0" cellpadding="5">
    <tr>
        <td class="highlightbar"><b>Edit a custom button</b></td>
    </tr>
    <tr>
        <td class="greyrowbottomnopadding">
			<form method="post" enctype="multipart/form-data">
			<cfoutput>
				<input type="hidden" name="bid" value="#bid#" />
				<input type="hidden" name="currentImage" value="#getbutton.fileName#" />
			</cfoutput>
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td width="15%" nowrap>Button label </td>
				    <td width="85%">
				        <input type="text" name="buttonLabel" style="width:300px;" maxlength="50" value="<cfoutput>#getButton.buttonLabel#</cfoutput>">
				    </td>
				</tr>
				<tr>
				    <td nowrap>CSS label </td>
				    <td>
				        <input type="text" name="cssLabel" style="width:300px;" maxlength="50" value="<cfoutput>#getButton.cssName#</cfoutput>">
				    </td>
				</tr>
				<tr>
				    <td nowrap>Current Button Image </td>
				    <td><img src="http://www.alarminfo.net/mobile/iui/t/default/custombuttons/<cfoutput>#getButton.fileName#</cfoutput>"></td>
				</tr>
				<tr>
				    <td nowrap>New Button Image </td>
				    <td><input type="file" name="buttonImage"></td>
			    </tr>
				<tr>
				    <td colspan="2">
				        <input name="btnEdit" type="submit" id="btnAdd" value="Update Button">
				        <input name="btnCancel" type="button" id="btnCancel" value="Cancel" onclick="document.location='index.cfm';">
				    </td>
				</tr>
			</table>
			</form>
		</td>
    </tr>
</table>
<br />
<a class="normal" style="text-decoration:underline;" href="/index.cfm">Return to Intranet</a>
</div>

