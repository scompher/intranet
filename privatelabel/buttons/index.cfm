<link rel="stylesheet" type="text/css" href="/styles.css">

<cfquery name="getButtons" datasource="copalink_cobrand">
	select * from mobileCustomButtons 
	order by cssName asc 
</cfquery>

<script type="text/javascript">
function performAction(bid,action) {
	var i = action.selectedIndex;
	var doAction = action.options[i].value;
	
	if (doAction == "edit") {
		document.location = 'edit.cfm?bid=' + bid;
	} else if (doAction == "delete") {
		if (confirm('Are you sure you wish to remove this button?')) {
			document.location = 'delete.cfm?bid=' + bid;
		}
	}
	
	action.selectedIndex = -1;
}
</script>

<div align="center">
<table width="600" border="0" cellspacing="0" cellpadding="5">
    <tr>
        <td class="highlightbar"><b>Manage Custom Buttons</b></td>
    </tr>
    <tr>
        <td class="greyrowbottomnopadding">
			<form method="post">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td align="center" class="linedrowrightcolumn"><b>Action</b></td>
					<td class="linedrowrightcolumn"><b>CSS Label</b></td>
					<td class="linedrowrightcolumn"><b>Image Name</b></td>
					<td align="center" class="linedrow"><b>Image</b></td>
				</tr>
				<tr>
					<td class="linedrowrightcolumn" align="center"><input type="button" onclick="document.location='add.cfm'" value="New Button"></td>
					<td class="linedrow" colspan="3">&nbsp;</td>
				</tr>
				<cfoutput query="getButtons">
				<tr>
					<td align="center" class="linedrowrightcolumn">
						<select name="adminaction" onchange="performAction('#getButtons.buttonid#',this);" style="width:100px;">
							<option value=""></option>
							<option value="edit">Edit</option>
							<option value="delete">Delete</option>
						</select>
					</td>
					<td class="linedrowrightcolumn">#cssName#</td>
					<td class="linedrowrightcolumn">#fileName#</td>
					<td align="center" class="linedrow"><img src="http://www.alarminfo.net/mobile/iui/t/default/custombuttons/#fileName#"></td>
				</tr>
				</cfoutput>
				<tr>
					<td class="linedrowrightcolumn" align="center"><input type="button" onclick="document.location='add.cfm'" value="New Button"></td>
					<td class="linedrow" colspan="3">&nbsp;</td>
				</tr>
			</table>
			</form>
		</td>
    </tr>
</table>
<br />
<a class="normal" style="text-decoration:underline;" href="/index.cfm">Return to Intranet</a>
</div>
