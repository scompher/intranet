
<link rel="stylesheet" type="text/css" href="/styles.css">

<cfparam name="searchDealerNumber" default="">
<cfparam name="searchDealerID" default="">
<cfparam name="searchinDevelopment" default="1">
<cfparam name="searchcurrentStatus" default="">

<cfquery name="getSites" datasource="copalink_cobrand">
	select * from site_main 
	where 
		<cfif searchinDevelopment is not "-1">
			<cfif searchinDevelopment is 0>
				live = 1 and 
			<cfelseif searchinDevelopment is 2>
				closedOut = 1 and 
			<cfelse>
				inDevelopment = #searchinDevelopment# and 
			</cfif>
		</cfif>
		<cfif trim(searchDealerNumber) is not "">
			dealerNumber = '#searchDealerNumber#' and 
		</cfif>
		<cfif trim(searchDealerID) is not "">
			dealerID = '#searchDealerID#' and 
		</cfif>
		<cfif trim(searchcurrentStatus) is not "">
			currentStatus = '#searchcurrentStatus#' and 
		</cfif>		
		1=1 
	order by dealerid asc 
</cfquery>

<script type="text/javascript">
function updateStatus(sid) {
	wleft = (screen.width - 350) / 2;
	wtop = (screen.height - 250) / 2;
	window.open("sites/updateStatus.cfm?sid=" + sid,"statusWin","height=250,width=350,resizeable=no,scrollbars=no,left=" + wleft + ",top=" + wtop);
}
function viewNotes(sid) {
	wleft = (screen.width - 400) / 2;
	wtop = ((screen.height - 400) / 2) - 150;
	window.open("sites/viewNotes.cfm?sid=" + sid,"notesWin","height=400,width=400,resizeable=no,scrollbars=yes,left=" + wleft + ",top=" + wtop);
}
function performAction(sid,did,action) {
	var i = action.selectedIndex;
	var doAction = action.options[i].value;
	if (doAction == "edit") {
		document.location = 'sites/siteDetails.cfm?sid=' + sid;
	} else if (doAction == "remove") {
		if (confirm('Are you sure you wish to remove this site?')) {
			document.location = 'sites/remove.cfm?sid=' + sid;
		}
	} else if (doAction == "updateStatus") {
		updateStatus(sid);
	} else if (doAction == "previewDesktop") {
		// window.open("http://www.alarminfo.net/login/index.cfm?dealer=" + did + "&preview=1");
		window.open("sites/desktopPreview.cfm?dealer=" + did);
	} else if (doAction == "previewMobile") {
		window.open("sites/mobilePreview.cfm?dealer=" + did);
	} else if (doAction == "launchSite") {
		document.location = 'sites/launchSite.cfm?sid=' + sid;
	} else if (doAction == "sendScreenshots") {
		document.location = 'sites/sendScreenshots.cfm?sid=' + sid;
	} else if (doAction == "closeSite") {
		if (confirm('Are you sure you wish to close out this site?')) {
			document.location = 'sites/closeSite.cfm?sid=' + sid;
		}
	}
	action.selectedIndex = -1;
}
</script><div align="center">
<form method="post">
<table width="400" border="0" cellspacing="0" cellpadding="5">
    <tr>
        <td class="highlightbar"><b>Private Label Site Maintenance Search</b></td>
    </tr>
    <tr>
        <td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
				    <td colspan="2"><b>Search using the criteria below</b></td>
			    </tr>
				<cfoutput>
				<tr>
					<td width="16%" nowrap="nowrap">Dealer ## </td>
				    <td width="84%">
				        <input name="searchdealerNumber" type="text" id="dealerNumber" value="#searchdealerNumber#" />
				    </td>
				</tr>
				<tr>
				    <td nowrap="nowrap">Site Identifier </td>
				    <td>
				        <input name="searchdealerID" type="text" id="dealerID" value="#searchdealerID#" />
				    </td>
			    </tr>
				</cfoutput>
				<tr>
				    <td nowrap="nowrap">Site Type </td>
				    <td>
				        <select name="searchinDevelopment">
							<option <cfif searchinDevelopment is "-1">selected</cfif> value="-1">All Sites</option>
							<option <cfif searchinDevelopment is "1">selected</cfif> value="1">In Development</option>
							<option <cfif searchinDevelopment is "0">selected</cfif> value="0">Current Live Sites</option>
							<option <cfif searchinDevelopment is "2">selected</cfif> value="2">Closed out Sites</option>
                        </select>
				    </td>
				    </tr>
				<tr>
				    <td nowrap="nowrap">Current Status </td>
				    <td>
				        <select name="searchcurrentStatus">
							<option value=""></option>
							<option <cfif searchcurrentstatus is "Received">selected</cfif> value="Received">Received</option>
							<option <cfif searchcurrentstatus is "In Progress">selected</cfif> value="In Progress">In Progress</option>
							<option <cfif searchcurrentstatus is "Waiting on Dealer">selected</cfif> value="Waiting on Dealer">Waiting on Dealer</option>
							<option <cfif searchcurrentstatus is "Launched">selected</cfif> value="Launched">Launched</option>
			            </select>
					</td>
				</tr>
				<tr>
				    <td colspan="2">
				        <input name="btnSearch" type="submit" id="btnSearch" value="Search Now" />
				        <input type="button" onclick="document.location = 'index.cfm';" value="Clear Fields" />
				    </td>
			    </tr>
			</table>
		</td>
    </tr>
</table>
<br />
<a class="normal" style="text-decoration:underline;" href="buttons/index.cfm">Manage Custom Buttons</a>
<br />
<br />
<table width="950" border="0" cellspacing="0" cellpadding="5">
    <tr>
        <td class="highlightbar"><b>Private Label Site Maintenance</b></td>
    </tr>
    <tr>
        <td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td align="center" class="linedrowrightcolumn"><b>Action</b></td>
				    <td align="center" nowrap="nowrap" class="linedrowrightcolumn"><b>Dealer # </b></td>
				    <td align="left" class="linedrowrightcolumn"><b>Site Identifier </b></td>
				    <td align="left" class="linedrowrightcolumn"><b>Site URL </b></td>
				    <td align="center" class="linedrowrightcolumn"><b>Date Received </b></td>
				    <td align="left" class="linedrowrightcolumn"><b>Current Status</b></td>
				    <td align="center" nowrap="nowrap" class="linedrowrightcolumn"><b>Notes</b></td>
				    <td align="center" class="linedrow"><b>Date Launched </b></td>
				</tr>
				<cfoutput query="getsites">
				<cfset redFlag = false>
				<cfif getSites.dateTimeReceived is not "">
					<cfif (datediff("d",getsites.dateTimeReceived,now()) gt 7) and (getSites.inDevelopment is 1)><cfset redFlag = true></cfif>
				</cfif>
				<tr>
				    <td align="center" class="linedrowrightcolumn">
					<select name="adminaction" onchange="performAction('#getsites.siteid#','#getsites.dealerid#',this);" style="width:125px;">
						<option value=""></option>
						<option value="edit">Edit</option>
						<cfif lcase(trim(getsites.currentStatus)) is "launched">
							<option value="remove">Remove</option>
						</cfif>
						<cfif lcase(trim(getsites.currentStatus)) is not "launched">
							<option value="updateStatus">Update Status</option>
						</cfif>
						<cfif trim(getsites.logoImage) is not "">
							<option value="previewDesktop">Preview Desktop</option>
						</cfif>
						<cfif trim(getsites.mobileLogo) is not "">
							<option value="previewMobile">Preview Mobile</option>
						</cfif>
						<option value="sendScreenshots">Send Screenshots</option>
						<cfif lcase(trim(getsites.currentStatus)) is not "launched" and trim(getsites.logoImage) is not "">
							<option value="launchSite">Launch Site</option>
						</cfif>
						<option value="closeSite">Close out site</option>
					</select>
					</td>
				    <td <cfif redFlag>style="color:FF0000"</cfif> align="center" class="linedrowrightcolumn">#dealernumber#</td>
				    <td <cfif redFlag>style="color:FF0000"</cfif> align="left" class="linedrowrightcolumn">#dealerid#</td>
				    <td <cfif redFlag>style="color:FF0000"</cfif> align="left" class="linedrowrightcolumn">#siteURL#</td>
				    <td <cfif redFlag>style="color:FF0000"</cfif> align="center" class="linedrowrightcolumn">
					<cfif trim(dateTimeReceived) is not "">
						#dateformat(dateTimeReceived,'mm/dd/yyyy')#
					<cfelse>
						N/A
					</cfif>
					</td>
				    <td <cfif redFlag>style="color:FF0000"</cfif> align="left" class="linedrowrightcolumn">#currentStatus#</td>
				    <td <cfif redFlag>style="color:FF0000"</cfif> align="center" nowrap="nowrap" class="linedrowrightcolumn"><a href="javascript:viewNotes(#getsites.siteid#);">View Notes </a></td>
				    <td <cfif redFlag>style="color:FF0000"</cfif> align="center" class="linedrow">
					<cfif trim(launchdate) is not "">
						#dateformat(launchdate,'mm/dd/yyyy')#
					<cfelse>
						N/A
					</cfif>
					</td>
				</tr>
				</cfoutput>
			</table>
		</td>
    </tr>
	<tr>
		<td>
		<input type="button" value="Add New Site" onclick="document.location='sites/newsite.cfm';" />
		</td>
	</tr>
</table>
</form>
<br />
<a class="normal" style="text-decoration:underline;" href="/index.cfm">Return to Intranet</a>
</div>
