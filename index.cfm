
<script type="text/javascript">
function launchViewer() {
	window.open("/smdr/scroller.cfm","","toolbar=0,menubar=0,resizable=1,width=270,height=450,status=0,scrollbars=1");
}
</script>

<link rel="stylesheet" type="text/css" href="styles.css" />
<cfquery name="getitems" datasource="#request.odbc_datasource#">
select Admin_Security_Sections.*, Admin_Security_Items.*
	from Admin_Security_Sections
	inner join admin_security_items on admin_security_sections.sectionid = admin_security_items.sectionid
	inner join admin_security_lookup on admin_security_items.itemid = admin_security_lookup.itemid
	where admin_security_lookup.adminuserid = #cookie.adminlogin#
	order by admin_security_sections.sectionname asc, admin_security_items.itemname asc
</cfquery>
<cfset backdate = dateadd("d",-14,now())>
<cfset backdate = createodbcdate(backdate)>

<cfif findnocase("blackberry", cgi.HTTP_USER_AGENT) is 0>

<head>
	<title>C.O.P.S. Monitoring Intranet</title>
</head>
<body>
<div align="center">
    <table width="100%" border="0" cellpadding="5" cellspacing="0">
		<tr>
			<td align="center" class="header"><b>Welcome to the C.O.P.S. Monitoring Intranet</b></td>
		</tr>
		<tr>
			<td style="padding-left:0px; padding-right:0px">
				<table width="100%" border="0" cellpadding="5" cellspacing="0">
					<tr>
						<td width="225" align="center" valign="top" style="padding-left:0px;">
							<cfinclude template="/ticketEntry/index.cfm">
							<table width="100%" border="0" cellpadding="5" cellspacing="0">
								<tr>
									<td class="highlightbar"><b>My IP Address</b></td>
								</tr>
								<tr class="datatable">
									<td class="greyrowbottom">My IP Address is: <cfoutput>#cgi.REMOTE_ADDR#</cfoutput></td>
								</tr>
							</table>
							<br>
							<table width="100%" border="0" cellpadding="5" cellspacing="0">
								<tr>
									<td class="highlightbar"><b>Employee Suggestions</b></td>
								</tr>
								<tr class="datatable">
									<td class="greyrow"><a href="http://www.copsmonitoring.com/suggest/">Make A Suggestion</a></td>
								</tr>
								<tr class="datatable">
									<td class="greyrowbottom"><a href="/suggestionBox/suggestions/searchSuggestion.cfm">View Past Suggestions</a></td>
								</tr>
							</table>
							<br>
							<table width="100%" border="0" cellpadding="5" cellspacing="0">
								<tr>
									<td class="highlightbar"><b>Utilities</b></td>
								</tr>
								<tr class="datatable">
									<td class="greyrow"><a href="/ops/">Ops Intranet Menu</a></td>
								</tr>
								<tr class="datatable">
									<td class="greyrow"><a href="/projectTracking/">My Tasks</a></td>
								</tr>
								<tr class="datatable">
									<td class="greyrow"><a href="javascript:launchViewer();">My Call Viewer</a></td>
								</tr>
								<tr class="datatable">
									<td class="greyrow"><a href="/users/changepw.cfm">Change Password</a></td>
								</tr>
								<!---
								<tr>
									<td class="greyrow"><a href="/misc/suggest.cfm">Make an Intranet Suggestion</a></td>
								</tr>
								--->
								<tr>
									<td class="greyrow"><a target="_blank" onClick="if (navigator.appName.indexOf('Internet Explorer') > 0) {return true;} else {alert('You must be using Microsoft Internet Explorer to access this function'); return false;}" href="http://rightfax.lydia.com/webutil/">RightFax Web Interface</a></td>
								</tr>
								<tr>
									<td class="greyrowbottom"><a href="/login/logout.cfm">Log Out</a></td>
								</tr>
							</table>
							<br />
							<table width="100%" border="0" cellpadding="5" cellspacing="0">
								<tr>
									<td class="highlightbar"><b>Groupwise Information </b></td>
								</tr>
								<tr>
									<td class="greyrowbottomnopadding">
										<table width="100%" border="0" cellpadding="5" cellspacing="0" class="grey">
											<tr>
												<td nowrap="nowrap"><a href="http://mail.copsmonitoring.com/gw/webacc" target="_blank">Webmail Access Login </a> </td>
											</tr>
											<tr>
												<td nowrap="nowrap"><a target="_blank" href="pdf/gw2014.pdf" target="_blank">Quick Start Guide</a> </td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
							<br />
							<table width="100%" border="0" cellspacing="0" cellpadding="5">
								<tr>
									<td class="highlightbar"><b>Phone Information </b></td>
								</tr>
								<tr>
									<td class="greyrowbottomnopadding">
										<table width="100%" border="0" cellpadding="5" cellspacing="0" class="grey">
											<tr>
												<td>
												<a href="/pdf/voicemailguide.pdf" target="_blank">Voicemail Guide</a>
												</td>
											</tr>
											<tr>
												<td>
												<a href="http://telemax.copsmonitoring.com/user" target="_blank">Voicemail Access</a>
												</td>
											</tr>
											<tr>
												<td>
												<a href="/phonelist/general_extensions_list.cfm" target="_blank">Phone Extension List</a>
												</td>
											</tr>									<tr>
												<td>
												<a href="/phonelist/uccphone.htm" target="_blank">UCC Phone List</a>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
							<br />
							<cfoutput query="getitems" group="sectionid">
							<table width="100%" border="0" cellpadding="5" cellspacing="0">
								<tr>
									<td class="highlightbar"><b>#getitems.sectionName#</b></td>
								</tr>
								<tr>
									<td class="greyrowbottomnopadding">
										<table width="100%" border="0" cellpadding="5" cellspacing="0" class="grey">
										<cfoutput>
											<tr>
												<td><a href="#getitems.itemURL#">#getitems.itemName#</a></td>
											</tr>
										</cfoutput>
										</table>
									</td>
								</tr>
							</table>
							<br />
							</cfoutput>
						</td>
						<td valign="top" style="padding-right:0px">
							<cfinclude template="specialUserContent.cfm">
							<br />
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</div>
</body>

<cfelse>

<cfinclude template="specialUserContent.cfm">

<br><br>
<p style="font-size:10px; font-family:Arial, Helvetica, sans-serif">

<b>Main Menu</b>

<br>

<cfoutput query="getitems" group="sectionid">
<br>
<b>#getitems.sectionname#</b><br>
<cfoutput>
<a style="color:##000099; text-decoration:underline;" href="#getitems.itemURL#">#getitems.itemName#</a><br>
</cfoutput>
<br />
</cfoutput>

</p>

</cfif>