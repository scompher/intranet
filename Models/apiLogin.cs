namespace Intranet.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("apiLogin")]
    public partial class apiLogin
    {
        public int id { get; set; }

        [StringLength(50)]
        public string login { get; set; }

        [StringLength(50)]
        public string password { get; set; }

        public bool? active { get; set; }

        [StringLength(50)]
        public string apidoc { get; set; }

        public bool? receiveAlarm { get; set; }

        [StringLength(100)]
        public string emailAddress { get; set; }

        [StringLength(50)]
        public string QADealerNum { get; set; }
    }
}
