﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Intranet.Validations;
using System.Xml;
using System.Xml.Serialization;
using System.Text;
using System.IO;

namespace Intranet.Models
{
  
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class ChangeRequest
    {

        private DateTime _changeDateTime = DateTime.Now;
        private DateTime _requestDateTime = DateTime.Now;
        private List<string> _changeType = new List<string> { "Major", "Minor" };
        private List<string> _status = new List<string> { "Approved", "Rejected", "Completed" };
        private IEnumerable<SelectListItem> _deptList;

        
        public int RequestID { get; set; }

        [StringLength(255)]
        [Required]
        [Display(Name = "Requestor Name")]
        [XmlElement(ElementName = "RequestorName")]
        public string RequestorName { get; set; }

        [StringLength(255)]
        [Required]
        [Display(Name = "Requestor Email")]
        [XmlElement(ElementName = "RequestorEmail")]
        public string RequestorEmail { get; set; }


        [Display(Name = "Requestor Department")]
        public string RequestorDepartment { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Request Date")]
        public DateTime RequestDate {
            get { return _requestDateTime; }
            set { _requestDateTime = value; }
        }

        [Display(Name = "Management Approved/Rejected By")]
        public string Manager { get; set; }

        [Display(Name = "Approve/Reject Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime? StatusDate { get; set; }

        [Required (ErrorMessage="Change Type is required.") ]
        [Display(Name = "What type of change is this?")]
        public string ChangeType { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        [PastDate(ErrorMessage = "Change Date is in the past.")]
        [Required(ErrorMessage = "Change Date is required.")]
        [Display(Name = "What is the requested date to perform the change?")]
        public DateTime ChangeDate {
            get { return _changeDateTime; }
            set { _changeDateTime = value; }
        }

        [Required(ErrorMessage = "Technical changes are required.")]
        [DataType(DataType.MultilineText)]
        [Display(Name = "What are the proposed technical changes?")]
        public string ChangeDescription { get; set;  }

        [Required(ErrorMessage = "Impacted systems is required.")]
        [Display(Name = "What systems/programs are impacted?")]
        public string Systems { get; set; }

        [Required(ErrorMessage = "Change reason is required.")]
        [DataType(DataType.MultilineText)]
        [Display(Name = "Why is this change necessary?")]
        public string ChangeReason { get; set; }

        [Display(Name = "What people will be involved in performing this change?")]
        public string ChangeMadeBy{ get; set; }

        [Display(Name = "Was this change tested in non-production any by whom?")]
        public string TestedBy { get; set; }

        [Display (Name = "Status")]
        public string Status { get; set; }

        [XmlIgnoreAttribute]
        public IEnumerable<SelectListItem> ChangeTypeList
        {
            get { return new SelectList(_changeType); }
        }


        [XmlIgnoreAttribute]
        public string History { get; set; }

        [Display(Name = "Updated By")]
        public string UpdatedBy { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Date Updated")]
        public DateTime UpdateDate { get; set; }



        [XmlIgnoreAttribute]
        public IEnumerable<SelectListItem> StatusList
        {
            get { return new SelectList(_status); }
        }

        [XmlIgnoreAttribute]
        public IEnumerable<SelectListItem> DepartmentList
        {
            get { return _deptList; }
            internal set { _deptList = value; }
        }


        internal void UpdateHistory()
        {
            //Serialize this object excluding the history property
            //Open or create history as an xml doc and append the newly serialized object

            XmlDocument xml = new XmlDocument();
            if (String.IsNullOrWhiteSpace(this.History))
            {
                XmlNode cNode = xml.CreateElement("history");
                xml.AppendChild(cNode);
            }
            else
            {
                xml.LoadXml(this.History);
            }

            string ser = this.Serialize();
            XmlDocument s = new XmlDocument();
            s.LoadXml(ser);

            XmlNode newNode = xml.ImportNode(s.SelectSingleNode("//ChangeRequest"), true);
            xml.SelectSingleNode("//history").AppendChild(newNode);
            this.History = xml.OuterXml;
        }


        public ChangeRequest FromHistory(int historyId)
        {

            if (String.IsNullOrWhiteSpace(this.History))
                return new ChangeRequest();


            try
            {
                XmlDocument xml = new XmlDocument();
                xml.LoadXml(this.History);
                XmlNode node = xml.SelectSingleNode("//ChangeRequest[" + historyId + "]");
                var serializer = new XmlSerializer(typeof(ChangeRequest));
                ChangeRequest result = (ChangeRequest)serializer.Deserialize(new StringReader(node.OuterXml));
                return result;
            }
            catch (Exception)
            {
                return new ChangeRequest();
            }


            

        }

        public string Serialize()
        {
            var serializer = new XmlSerializer(typeof(ChangeRequest));
            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            serializer.Serialize(sw, this);
            string xmlResult = sw.GetStringBuilder().ToString();
            return xmlResult;
        }

        


    }
}
