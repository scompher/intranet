﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Intranet.Models
{
    public class WestResult
    {

        private WestAddress _address = new WestAddress();
        private Intranet.West.responderInfoResponse _response = null;
        private Intranet.West.geodeticQueryResponse _geoResposne = null;


        public WestResult()
        {
        }

        public WestResult(WestAddress address, Intranet.West.geodeticQueryResponse response)
        {
            _address = address;
            _geoResposne = response;
        }


        public WestResult(WestAddress address, Intranet.West.responderInfoResponse response)
        {
            _address = address;
            _response = response;
        }


        public WestAddress Address
        {
            get { return _address; }
        }

        public Intranet.West.responderInfoResponse Response
        {
            get { return _response; }
        }

        public Intranet.West.geodeticQueryResponse GeoResponse
        {
            get { return _geoResposne; }
        }


        /// <summary>
        /// returns the West result as a fromatted address and color codes the matched, altered values 
        /// </summary>
        /// <returns></returns>
        public string WestFormattedMatch()
        {

            if (this._response.addressMatchCode.overallMatch != Intranet.West.overallMatchCodeValues.Failure)
            {
                Intranet.West.rawMatchCode code = this._response.addressMatchCode.rawMatchCode;

                string hno = string.Concat("<span class=\"", code.HNO.HouseNumMatch, "\">", code.HNO.HouseNum.Trim(), "</span>");
                string prd = string.Concat("<span class=\"", code.PRD.PrefixDirectionalMatch, "\">", code.PRD.PrefixDirectional.Trim(), "</span>");
                string stn = string.Concat("<span class=\"", code.STN.StreetNameMatch, "\">", code.STN.StreetName.Trim(), "</span>");
                string sts = string.Concat("<span class=\"", code.STS.StreetSuffixMatch, "\">", code.STS.StreetSuffix.Trim(), "</span>");
                string pod = string.Concat("<span class=\"", code.POD.PostDirectionalMatch, "\">", code.POD.PostDirectional.Trim(), "</span>");

                string utp = string.Empty;
                string unm = string.Empty;

                if (!string.IsNullOrWhiteSpace(code.UTP.UnitType))
                    utp = string.Concat("<span class=\"", code.UTP.UnitTypeMatch, "\">", code.UTP.UnitType.Trim(), "</span>");
                if (!string.IsNullOrWhiteSpace(code.UNM.UnitNumber))
                    unm = string.Concat("<span class=\"", code.UNM.UnitNumberMatch, "\">", code.UNM.UnitNumber.Trim(), "</span>");

                string pcn = string.Concat("<span class=\"", code.PCN.PostalCommunityMatch, "\">", code.PCN.PostalCommunity.Trim(), "</span>");
                string sta = string.Concat("<span class=\"", code.STA.StateProvinceMatch, "\">", code.STA.StateProvince.Trim(), "</span>");
                string zip = string.Concat("<span class=\"", code.ZIP.PostalZipCodeMatch, "\">", code.ZIP.PostalZipCode.Trim(), "</span>");


                string ln1 = string.Concat(hno, " ", prd, " ", stn, " ", sts, " ", pod).Trim();
                string ln2 = string.Concat(utp, " ", unm).Trim();
                string ln3 = string.Concat(pcn, " ", sta, ", ", zip).Trim();

                string retVal = string.Concat(string.IsNullOrWhiteSpace(ln1) ? "" : ln1 + "<br/>", string.IsNullOrWhiteSpace(ln2) ? "" : ln2 + "<br/>", string.IsNullOrWhiteSpace(ln3) ? "" : ln3);
                return retVal;
            }
            return "";
        }

        /// <summary>
        /// Convert a coordinate in Degrees, minutes, secods to degrees (for use with Google Maps)
        /// </summary>
        /// <returns></returns>
        public string DMSToDegrees(int degrees, int minutes, double seconds)
        {

            double s = seconds / 60;
            double m = (minutes + s) / 60;
            double d = Math.Abs(degrees) + m;

            if (degrees < 0)
                d = d * -1;

            return d.ToString();
        }


    } //class
} //ns