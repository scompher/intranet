namespace Intranet.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("serviceConversion")]
    public partial class serviceConversion
    {
      
        public int id { get; set; }

        [StringLength(255)]
        public string vendor { get; set; }

        [StringLength(255)]
        public string item { get; set; }

        [StringLength(255)]
        public string description { get; set; }
        [StringLength(250)]
        public string serviceCode { get; set; }

        public bool? Ignore { get; set; }

        public int? vendorID { get; set; }
    }
}
