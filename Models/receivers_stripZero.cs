namespace Intranet.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class receivers_stripZero
    {
        [Key]
        public int receiverID { get; set; }

        [StringLength(50)]
        public string receiver { get; set; }
    }
}
