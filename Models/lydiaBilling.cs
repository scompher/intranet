namespace Intranet.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class lydiaBilling : DbContext
    {
        public lydiaBilling()
            : base("name=lydiaBilling")
        {
        }

        public virtual DbSet<serviceConversion> serviceConversions { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
