namespace Intranet.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class copalink : DbContext
    {
        public copalink()
            : base("name=copalink")
        {
        }

        public virtual DbSet<apiLogin> apiLogins { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }

        public System.Data.Entity.DbSet<Intranet.Models.receivers_stripZero> receivers_stripZero { get; set; }
    }
}
