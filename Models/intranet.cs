﻿using System.Data.Entity;

namespace Intranet.Models
{
    public class intranet : DbContext
    {
        public DbSet<request> Requests { get; set; }
        public DbSet<Admin_Users> Admin_Users { get; set; }
        public DbSet<Admin_Users_Departments> Admin_Users_Departments { get; set; }

        public System.Data.Entity.DbSet<Intranet.Models.apiLogin> apiLogin { get; set; }

        public System.Data.Entity.DbSet<Intranet.Models.testtable> testtables { get; set; }
    }
}