﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Intranet.Models
{
    public class WestAddress
    {
        [StringLength(12)]
        [Display(Name = "House Number")]
        public string HouseNumber { get; set; }

        [StringLength(3)]
        [Display(Name = "Prefix Directional")]
        public string PrefixDirectional { get; set; }

        [StringLength(41)]
        [Display(Name = "Street Name")]
        public string StreetName { get; set; }

        [StringLength(4)]
        [Display(Name = "Street Suffix")]
        public string StreetSuffix { get; set; }

        [StringLength(3)]
        [Display(Name = "Post Directional")]
        public string PostDirectional { get; set; }

        [StringLength(5)]
        [Display(Name = "Unit Type")]
        public string UnityType { get; set; }

        [StringLength(12)]
        [Display(Name = "Unit Number")]
        public string UnitNumber { get; set; }

        [StringLength(29)]
        [Required]
        [Display(Name = "City")]
        public string City { get; set; }

        [StringLength(3)]
        [Display(Name = "State")]
        public string State { get; set; }

        [StringLength(11)]
        [Display(Name = "Postal Code")]
        public string PostalCode { get; set; }

        [Range(-90, 90)]
        [Display(Name = "Latitude Degrees")]
        public int LatitudeDegrees { get; set; }

        [Range(0, 60)]
        [Display(Name = "Latitude Minutes")]
        public int LatitudeMinutes { get; set; }

        [Range(0, 60)]
        [Display(Name = "Latitude Seconds")]
        public double LatitudeSeconds { get; set; }

        [Range(-180, 180)]
        [Display(Name = "Longitude Degrees")]
        public int LongitudeDegrees { get; set; }

        [Range(0, 60)]
        [Display(Name = "Longitude Minutes")]
        public int LongitudeMinutes { get; set; }

        [Range(0, 60)]
        [Display(Name = "Longitude Seconds")]
        public double LongitudeSeconds { get; set; }




        public string FormattedAddress
        {
            get
            {
                string ln1 = string.Concat(this.HouseNumber, " ", this.PrefixDirectional, " ", this.StreetName, " ", this.StreetSuffix, " ", this.PostDirectional).Trim();
                string ln2 = string.Concat(this.UnityType, " ", this.UnitNumber).Trim();
                string ln3 = string.Concat(this.City, " ", this.State, ", ", this.PostalCode).Trim();
                if (ln3.Equals(",", StringComparison.OrdinalIgnoreCase))
                    ln3 = "";

                string retVal = string.Concat(string.IsNullOrWhiteSpace(ln1) ? "" : ln1 + "<br/>", string.IsNullOrWhiteSpace(ln2) ? "" : ln2 + "<br/>", string.IsNullOrWhiteSpace(ln3) ? "" : ln3);
                return retVal;
            }
        }




    }
}