﻿using System;
using System.ComponentModel;
using System.Data.Entity;

namespace Intranet.Models
{
 
    public class request
    {
        public int ID { get; set; }

        [DisplayName("Date Time Created")]
        public DateTime dateTimeCreated { get; set; }

        [DisplayName("Department")]
        public int department { get; set; }

        [DisplayName("Requestor")]
        public int requestor { get; set; }

        [DisplayName("Current Tech")]
        public int? currentTech { get; set; }

        [DisplayName("Last Update")]
        public DateTime lastUpdated {get; set;}
        public Boolean active { get; set; }

        [DisplayName("Request Details")]
        public string requestText { get; set; }
        public Boolean opened { get; set; }
        public string requestTextTrimmed { get
            {
                string requestTextTrimmed = requestText;
                if (requestTextTrimmed.Length > 20)
                {
                   requestTextTrimmed = requestTextTrimmed.Substring(0, 20) + "...";
                }
                return requestTextTrimmed;
            }
        }
    }

    public class requestsView
    {
        public request request { get; set; }
        public Admin_Users Admin_Users { get; set; }
        public Admin_Users_Departments Admin_Users_Departments { get; set; }
        public Admin_Users Admin_Users2 { get; set; }

    }

    public class requestsCreate
    {
        public request request { get; set; }
        public string employeeName { get; set; }

    }

}