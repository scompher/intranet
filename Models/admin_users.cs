namespace Intranet.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Admin_Users
    {


        public override string ToString()
        {
            return string.Concat(this.firstname, " ", this.lastname);
        }

        public string FullName
        {
            get { return string.Concat(this.firstname, " ", this.lastname); }
        }

        [Key]
        public int adminuserid { get; set; }

        public int? seclevelid { get; set; }

        [StringLength(50)]
        public string firstname { get; set; }

        [StringLength(50)]
        public string lastname { get; set; }

        [StringLength(50)]
        public string username { get; set; }

        [StringLength(50)]
        public string password { get; set; }

        [StringLength(100)]
        public string email { get; set; }

        public bool active { get; set; }

        public DateTime? lastlogin { get; set; }

        public int defaultdeptid { get; set; }

        [StringLength(50)]
        public string photo { get; set; }

        public bool confessionalNotifications { get; set; }

        [StringLength(4)]
        public string extension { get; set; }

        [Column(TypeName = "date")]
        public DateTime? birthdate { get; set; }

        public bool? confessionalAccess { get; set; }

        [StringLength(255)]
        public string confessionalDisplayName { get; set; }

        public bool communicatorNotifications { get; set; }

        public bool? communicatorAccess { get; set; }

        [StringLength(255)]
        public string communicatorDisplayName { get; set; }
    }

    
}
