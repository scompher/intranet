﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Intranet.Models
{
    public class UCCDownloadAccounts
    {

        private DateTime _startDate = new DateTime();

        [Required]
        [Display(Name = "Start Date")]
        public DateTime StartDate {
            get { return _startDate; }
            set { _startDate = value; }
        }

        [Display(Name = "No Billing ID")]
        public bool NoBillingID { get; set; }

    }
}