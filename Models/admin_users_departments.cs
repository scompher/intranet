namespace Intranet.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Admin_Users_Departments
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int departmentID { get; set; }

        [StringLength(50)]
        public string department { get; set; }
    }
}
