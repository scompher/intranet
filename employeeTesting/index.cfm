
<link rel="stylesheet" type="text/css" href="/styles.css">

<cfparam name="form.dateTaken" default="">
<cfparam name="form.employeeNumber" default="">
<cfparam name="form.testName" default="">

<script language="javascript" src="cal2.js">
/*
Xin's Popup calendar script-  Xin Yang (http://www.yxscripts.com/)
Script featured on/available at http://www.dynamicdrive.com/
This notice must stay intact for use
*/
</script>
<script language="javascript" src="cal_conf2.js"></script>

<cfquery name="getTestNames" datasource="#ds#">
	select distinct testName from employeeTesting_core 
	order by testName asc 
</cfquery>

<cfif isDefined("form.btnNewSearch")>
	<cfset form.dateTaken = "">
	<cfset form.employeeNumber = "">
	<cfset form.testName = "">
</cfif>

<cfif not isDefined("form.btnSearchTests")>
	<cfquery name="getTests" datasource="#ds#">
		select * 
		from employeeTesting_core 
		order by testDateTime DESC 
	</cfquery>
<cfelse>
	<cfquery name="getTests" datasource="#ds#">
		select * 
		from employeeTesting_core 
		where 
		<cfif trim(form.employeeNumber) is not "">
		employeeNumber = '#form.employeeNumber#' and 
		</cfif>
		<cfif trim(form.testName) is not "">
		testName = '#form.testName#' and 
		</cfif>
		<cfif trim(form.dateTaken) is not "">
		testDateTime >= #createodbcdate(form.dateTaken)# and testDateTime < #createodbcdate(dateadd("d",1,form.dateTaken))# and 
		</cfif>
		1=1 
		order by testDateTime DESC 
	</cfquery>
</cfif>

<div align="center">
<table width="750" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Employee Testing</b> </td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding"><table width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td><b>Search For A Test</b> </td>
			</tr>
			<form method="post" action="index.cfm">
			<tr>
				<td class="nopadding">
				<table border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td>Employee #: </td>
						<td>
						<input type="text" name="employeeNumber" value="<cfoutput>#form.employeeNumber#</cfoutput>" style="width:50px" />
						</td>
					</tr>
					<tr>
						<td>Test Name: </td>
						<td>
						<select name="testName">
							<option value=""></option>
							<cfoutput query="getTestNames">
								<option <cfif form.testName is getTestNames.testName>selected</cfif> value="#testName#">#testName#</option>
							</cfoutput>
						</select>
						</td>
					</tr>
					<tr>
						<td>Date Taken: </td>
						<td>
						<input type="text" name="dateTaken" style="vertical-align:middle; width:75px" value="<cfoutput>#form.dateTaken#</cfoutput>" /> 
						<a style="text-decoration:none;" href="javascript:showCal('DateTaken');"><img src="/images/calicon.gif" width="20" height="20" align="absmiddle" border="0" /></a>
						</td>
					</tr>
					<tr>
						<td colspan="2">
						<input name="btnSearchTests" type="submit" class="sidebar" value="Search Now" />
						<input name="btnNewSearch" type="submit" class="sidebar" value="New Search" />
						</td>
					</tr>
				</table>
				</td>
			</tr>
			</form>
			<tr><td>&nbsp;</td></tr>
			<tr>
				<td class="nopadding">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td width="15" align="center" nowrap class="linedrow">&nbsp;</td>
						<td align="center" nowrap class="linedrow"><b>Date/Time Taken</b></td>
						<td class="linedrow"><b>Test Name </b></td>
						<td class="linedrow"><b>Employee Name </b></td>
						<td class="linedrow"><b>Score</b></td>
						<td class="linedrow"><b>Result</b></td>
					</tr>
					<cfoutput query="getTests">
					<tr>
						<td class="linedrowrightcolumn" align="center"><a href="viewDetail.cfm?tid=#employeeTestID#"><img border="0" src="/images/manifyingglass.gif" alt="View Detail" width="15" height="15" /></a></td>
						<td align="center" nowrap class="linedrowrightcolumn">
						<cfif trim(testDateTime) is not "">
						#dateformat(testDateTime, 'mm/dd/yyyy')# #timeformat(testDateTime, 'hh:mm tt')#
						</cfif>
						</td>
						<td class="linedrowrightcolumn">#testName#</td>
						<td class="linedrowrightcolumn">#employeeFirstName# #employeeLastName#</td>
						<td class="linedrowrightcolumn">
						<cfif trim(percentileScore) is not "">
						#numberformat(evaluate(percentileScore * 100),999)#%
						</cfif>
						</td>
						<td>#status#</td>
					</tr>
					</cfoutput>
				</table></td>
			</tr>
		</table></td>
	</tr>
</table>
<br />
<a class="normal" style="text-decoration:underline;" href="/index.cfm">Return to Intranet</a>
</div>
