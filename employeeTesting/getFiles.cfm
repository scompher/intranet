
<link rel="stylesheet" type="text/css" href="/styles.css">

<cfset un = "employee">
<cfset pw = "etesting">
<cfset fileLocation = "#request.directpath#employeeTesting\attachments">

<!--- get emails --->
<cfimap 
	action="getall" 
	name="getMail" 
	username="#un#" 
	password="#pw#" 
	attachmentpath="#fileLocation#" 
	port="143" 
	server="mail.copsmonitoring.com" >

<cfif isDefined("form.btnImportNow")>

	<cfloop query="getMail">
		<!--- read file --->
		<cffile action="read" file="#fileLocation#\#attachments#" variable="content">
	
		<!--- 
		<cfoutput>
		<pre>
		#content#
		</pre>
		</cfoutput>
		--->
	
		<cfset coreStart = listcontains(content, "Core Data", chr(13)) + 2>
		<cfset interactionStart = listcontains(content, "Interaction Data", chr(13)) + 2>
	
		<!--- read core data --->
		<cfset coreLine = listgetat(content,coreStart,chr(13))>
		<cfset coreLine = replace(coreLine, "#chr(34)#,#chr(34)#", "#chr(6)#", "all")>
		<cfset coreLine = replace(coreLine, "#chr(34)#", "", "all")>
		<cfset coreLineArray = coreLine.split("#chr(6)#")>
		<cfset coreStatus = coreLineArray[1]>
		<cfset coreLocation = coreLineArray[2]>
		<cfset coreRawScore = coreLineArray[3]>
		<cfset coreMaxScore = coreLineArray[4]>
		<cfset coreMinScore = coreLineArray[5]>
		<cfset coreTime = coreLineArray[6]>
		<cfset testName = "">
		<cfset employeeFirstName = "">
		<cfset employeeLastName = "">
		<cfset employeeNumber = "">
		
		<cfquery name="insertCoreData" datasource="#ds#">
			insert into employeeTesting_core (testName, employeeFirstName, employeeLastName, employeeNumber, status, location, rawScore, maxScore, minScore, time) 
			values ('#testName#', '#employeeFirstName#', '#employeeLastName#', '#employeeNumber#', '#coreStatus#', '#coreLocation#', '#coreRawScore#', '#coreMaxScore#', '#coreMinScore#', '#coreTime#') 
		</cfquery> 
		<cfquery name="getnewid" datasource="#ds#">
			select max(employeeTestID) as newid from employeeTesting_core 
		</cfquery>
		<cfset employeeTestID = getnewid.newid>
			
		<!--- read interaction data --->
		<cfset stopflag = false>
		<cfset index = interactionStart>
		<cfset linecount = 1>
		<cfset numWrong = 0>
		<cfset numRight = 0>
		<cfset totalQuestions = 0>
		<cfloop condition="#stopflag# is false">
			<cfset line = listgetat(content,index,chr(13))>
			<cfif trim(line) is not "">
				<cfset line = replace(line, "#chr(34)#,#chr(34)#", "#chr(6)#", "all")>
				<cfset line = replace(line, "#chr(34)#", "", "all")>
				<cfset lineArray = line.split("#chr(6)#")>
				<cfset date = lineArray[1]>
				<cfset time = lineArray[2]>
				<cfset testDateTime = date & " " & time>
				<cfset interactionID = lineArray[3]>
				<cfset objectiveID = lineArray[4]>
				<cfset interactionType = lineArray[5]>
				<cfset correctResponse = lineArray[6]>
				<cfset studentResponse = lineArray[7]>
				<cfset result = lineArray[8]>
				<cfset weight = lineArray[9]>
				<cfset latency = lineArray[10]>
				<cfquery name="insertInteractionData" datasource="#ds#">
					insert into employeeTesting_Interaction (employeeTestID, testDateTime, interactionID, objectiveID, interactionType, correctResponse, studentResponse, result, weight, latency)
					values (#employeeTestID#, #createodbcdatetime(testDateTime)#, '#interactionID#', '#objectiveID#', '#interactionType#', '#correctResponse#', '#studentResponse#', '#result#', '#weight#', '#latency#')
				</cfquery>
				<cfif linecount is 1>
					<!--- update core data --->
					<cfquery name="updateCoreInfo" datasource="#ds#">
						update employeeTesting_core 
						set testName = '#objectiveID#', employeeFirstName = '#listgetat(studentResponse,1)#', employeeLastName = '#listgetat(studentResponse,2)#', employeeNumber = #listgetat(studentResponse,3)#, testDateTime = #createodbcdatetime(testDateTime)#
						where employeeTestID = #employeeTestID# 
					</cfquery>
				<cfelse>
					<cfset totalQuestions = totalQuestions + 1>
					<!--- calculate percentile score --->
					<cfswitch expression="#result#">
						<cfcase value="C"><cfset numRight = numRight + 1></cfcase>
						<cfcase value="W"><cfset numWrong = numWrong + 1></cfcase>
					</cfswitch>
				</cfif>
				<cfset index = index + 1>
			<cfelse>
				<cfset stopflag = true>
				<!--- save percentile score --->
				<cfset percentileScore = numRight / totalQuestions>
				<cfquery name="savePercentile" datasource="#ds#">
					update employeeTesting_core 
					set percentileScore = #percentileScore# 
					where employeeTestID = #employeeTestID#  
				</cfquery>
			</cfif>
			<cfset linecount = linecount + 1>
		</cfloop>
		
		<!--- del email --->
		<cfimap 
			action="delete"
			uid="#uid#" 
			username="#variables.un#" 
			password="#variables.pw#" 
			port="143" 
			server="mail.copsmonitoring.com">
		<!--- del file --->
		<cfif fileExists("#fileLocation#\#attachments#")>
			<cffile action="delete" file="#fileLocation#\#attachments#">
		</cfif>
	
	</cfloop>
	
	<br />
	<div align="center">
	<table width="500" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td class="highlightbar"><b>Employee Testing Import Tests</b> </td>
		</tr>
		<tr>
			<td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td align="center">
					Import of tests has completed.
					</td>
				</tr>
			</table>
			</td>
		</tr>
	</table>
	<br />
	<a class="normal" style="text-decoration:underline;" href="/index.cfm">Return to Intranet</a>
	</div>

<cfelse>
	
	<br />
	<div align="center">
	<table width="500" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td class="highlightbar"><b>Employee Testing Import Tests</b> </td>
		</tr>
		<tr>
			<td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<form method="post" action="getfiles.cfm">
				<tr>
					<td align="center">
					<cfif getMail.recordCount gt 0>
					<cfif getMail.recordcount is 1>
					There is <cfoutput>#getMail.recordCount#</cfoutput> test ready for importing
					<cfelse>
					There are <cfoutput>#getMail.recordCount#</cfoutput> tests ready for importing
					</cfif>					
					<br />
					<br />
					<input name="btnImportNow" type="submit" class="sidebar" value="Import Tests Now" />
					<cfelse>
					There are no pending tests to import
					</cfif>
					</td>
				</tr>
				</form>
			</table>
			</td>
		</tr>
	</table>
	<br />
	<a class="normal" style="text-decoration:underline;" href="/index.cfm">Return to Intranet</a>
	</div>

</cfif>
