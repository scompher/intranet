<cfsilent>
<!---
	Author:			Nahuel Foronda & Laura Arguello
	Created:		August 07, 2005
	This work is licensed under the Creative Commons Attribution-ShareAlike License. To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/2.5/ or send a letter to Creative Commons, 543 Howard Street, 5th Floor, San Francisco, California, 94105, USA.

Please keep this notice to comply with license
------------------------------------------------------------------------------------------------------------------------------------------------
		
	Attributes:
		name: Required;  Name of the text input that will contain the name of the file to upload
		actionFile:  Required; File that will handle the upload. It can include query string variables to identify this file. Example: upload.cfm?id=15
		label: Label to put next to the control.
		fileTypes: extensions to accept, separated by semicolons. Example: *.jpg;*.png;*.gif
		fileDescription: Text to describe accepted files
		maxSize: maximum file size in Kb to upload. Default to no limit
		swf: name of the swf file that contains the io libraries. only needed if your swf is not in the same dir as your cfform
	
	Usage:
		This tag must be used with flashUploadInput tag.
		
		Example:
		default:
		<cf_flashUpload name="defaultFile" actionFile="upload.cfm">
			<cf_flashUploadInput />
		</cf_flashUpload>
		
		customized
		<cf_flashUpload label="Picture" name="myFile2" fileTypes="*.jpg;*.png;*.gif" fileDescription="Image files" actionFile="upload.cfm">
			<cf_flashUploadInput buttonStyle="corner-radius: 0;" inputStyle="fontWeight:bold" inputWidth="80" uploadButton="true" uploadButtonLabel="Upload Label" chooseButtonLabel="Choose file" progressBar="true" progressInfo="true" />
		</cf_flashUpload>

		flashUploadInput Usage:
		Attributes:
			inputWidth: with of the text input where file name is shown
			buttonStyle: style applied to choose and upload buttons
			uploadButton: true/false, default true. Adds an upload button. If you set it false, you must put the generated variable called "theNameOfYourInput_uploadScript" in some other button ("theNameOfYourInput" is the name assigned in the flashUpload tag name attribute)
			progressBar: true/false default true. Adds a progress bar.
			progressInfo: true/false default true. Adds an output area to show progress info
			progressBarStyle: style of progress bar
			uploadButtonLabel: label of "Upload" button
			chooseButtonLabel: label of "File browse" button
			required: will make the file input required (it will validate if user just writes some text)
			message: validation failure message

--->
<cfsavecontent variable="buttonStyle">
   corner-radius: 2;
    borderThickness: 0;
   fill-colors: #B4E055, #9FD32E;
   color: #ffffff;
</cfsavecontent>
<cfsavecontent variable="progressBarStyle">
   border-thickness:0;
   corner-radius: 0;
	 fill-colors: #ffffff, #DEEC6A;
	 theme-color: #A2DA2C;
	 border-color:#A2DA2C;
	 color:#ffffff;
</cfsavecontent>
<cfsavecontent variable="outputStyle">
	borderStyle:none;
	disabledColor:#333333;
	backgroundAlpha:0;
</cfsavecontent>
<cfsavecontent variable="contentPanelStyle">
	panelBorderStyle:'roundCorners';
	backgroundColor:#EFF7DF;
	headerColors:#CBEC84, #B0D660;
</cfsavecontent>	
</cfsilent>

<cfparam name="url.d" default="">

<div align="center">
<br />
<span class="normal">
<cfoutput>
Test URL will be: #request.AppURL#/employeetesting/tests/#url.d#/
</cfoutput>
</span>
<br /><br />
<cfform name="myform" format="Flash" width="600" height="315">
	<!--- skinned --->
	<cfformgroup type="panel" label="Upload Files" style="#contentPanelStyle#" width="600">
		<cf_flashUpload label="HTML File:" name="file1" actionFile="upload.cfm?d=#url.d#">
			<cf_flashUploadInput progressBarStyle="#progressBarStyle#" buttonStyle="#buttonStyle#" inputWidth="250" />
		</cf_flashUpload>
		<cf_flashUpload label="SWF File:" name="file2" actionFile="upload.cfm?d=#url.d#">
			<cf_flashUploadInput progressBarStyle="#progressBarStyle#" buttonStyle="#buttonStyle#" inputWidth="250" />
		</cf_flashUpload>
		<cf_flashUpload label="Javascript File:" name="file3" actionFile="upload.cfm?d=#url.d#">
			<cf_flashUploadInput progressBarStyle="#progressBarStyle#" buttonStyle="#buttonStyle#" inputWidth="250" />
		</cf_flashUpload>
	</cfformgroup>
	<!--- 	
	<cfformitem type="text" style="fontWeight:bold">Default</cfformitem>
		<cf_flashUpload name="defaultFile" actionFile="upload.cfm">
			<cf_flashUploadInput />
		</cf_flashUpload>
	<cfformitem type="hrule"></cfformitem>

	<!--- no bar --->
	<cfformitem type="text" style="fontWeight:bold">No progress bar (image files only, with max size of 200Kb)</cfformitem>
	<cf_flashUpload name="noBar" actionFile="upload.cfm" fileTypes="*.jpg;*.png;*.gif" fileDescription="Image files" maxSize="200">
			<cf_flashUploadInput progressBar="false" />
		</cf_flashUpload>
	
	<cfformitem type="hrule"></cfformitem>

	<!--- no info --->
	<cfformitem type="text" style="fontWeight:bold">No progress info</cfformitem>
	<cf_flashUpload name="noInfo" actionFile="upload.cfm">
			<cf_flashUploadInput progressInfo="false" />
		</cf_flashUpload>	

	<!--- with labels --->
	<cfformitem type="text" style="fontWeight:bold">Non-default labels</cfformitem>
	<cf_flashUpload name="withLabels" label="Picture" actionFile="upload.cfm" fileTypes="*.jpg;*.png;*.gif" fileDescription="Image files" >
			<cf_flashUploadInput uploadButtonLabel="Upload Picture" chooseButtonLabel="Browse..." inputWidth="100"  />
		</cf_flashUpload>	
	 --->

	<!--- if we need to trigger upload from a different button --->
	<!--- 
	<cfformitem type="text" style="fontWeight:bold">Trigger upload from other button</cfformitem>
	<cf_flashUpload name="myUpload" actionFile="upload.cfm">
			<cf_flashUploadInput uploadButton="false" required="true" message="File is required" />
		</cf_flashUpload>			

	<cfinput type="Button" name="myUploadButton" onclick="#myUpload_uploadScript#" value="Trigger Upload"/>
	 --->
</cfform>
<br />
<a class="normal" style="text-decoration:underline" href="/index.cfm">Click Here When Completed</a>
</div>
