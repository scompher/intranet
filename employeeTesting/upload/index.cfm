
<link rel="stylesheet" type="text/css" href="/styles.css">

<cfif isDefined("form.btnContinue")>

	<cfset form.newTestName = replace(form.newTestName, " ", "_", "all")>

	<cfif trim(form.newTestName) is not "">
		<cfif not DirectoryExists("#request.directpath#employeeTesting\tests\#form.newTestName#")>
			<cfdirectory action="create" directory="#request.directpath#employeeTesting\tests\#form.newTestName#">
		</cfif>
		<cflocation url="uploadForm.cfm?d=#urlencodedformat(form.newTestName)#">
	<cfelse>
		<cflocation url="uploadForm.cfm?d=#urlencodedformat(form.existingTestName)#">
	</cfif>

<cfelseif isDefined("form.btnCancel")>

	<cflocation url="/index.cfm">

<cfelse>
	
	<cfdirectory action="list" name="getTestNames" directory="#request.directPath#\employeeTesting\tests\">
	
	<div align="center">
	<table border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td class="highlightbar"><b>New Test Setup</b> </td>
		</tr>
		<tr>
			<td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<form method="post" action="index.cfm">
				<tr>
					<td width="22%" nowrap>New Test Name:  </td>
					<td width="78%"><input type="text" name="newTestName" style="width:300px"></td>
				</tr>
				<tr>
					<td colspan="2">Existing Tests (if updating)</td>
				</tr>
				<tr>
					<td colspan="2">
					<select name="existingTestName">
						<option value=""></option>
						<cfoutput query="getTestNames">
							<option value="#listgetat(directory,listlen(directory,'\'),'\')#">#listgetat(directory,listlen(directory,'\'),'\')#</option>
						</cfoutput>
					</select>
					</td>
				</tr>
				<tr>
					<td colspan="2">
					<input name="btnContinue" type="submit" class="sidebar" value="Continue">
					<input name="btnCancel" type="submit" class="sidebar" value="Cancel">
					</td>
				</tr>
				</form>
			</table>
			</td>
		</tr>
	</table>
	</div>

</cfif>
