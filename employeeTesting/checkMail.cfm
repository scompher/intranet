
<cfparam name="action" default="list">

<cfset un = "employee">
<cfset pw = "etesting">

<cfif action is "list">

	<cfimap 
		action="getheaderonly" 
		name="getMail" 
		username="#un#" 
		password="#pw#" 
		port="143" 
		server="mail.copsmonitoring.com" >
		
	<div align="center">
	<table width="950" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td>Attachment</td>
			<td>Date</td>
			<td>From</td>
			<td>Subject</td>
			<td>UID</td>
		</tr>
		<cfoutput query="getMail">
		<tr <cfif getMail.currentrow mod 2 is 0>bgcolor="##FFFFFF"<cfelse>bgcolor="##EEEEEE"</cfif> >
			<td align="center">
			<cfif findnocase("Content-Type: multipart/mixed;",getMail.header) is not 0>
			Yes
			</cfif>
			&nbsp;
			</td>
			<td nowrap="nowrap">#dateformat(rxddate,'mm/dd/yyyy')# #timeformat(rxddate,'hh:mm:ss tt')#</td>
			<td nowrap="nowrap">#getMail.From#</td>
			<td>
			<a target="_blank" href="checkMail.cfm?id=#uid#&action=getdetail">
			#getMail.Subject#
			</a>
			</td>
			<td>#getMail.UID#</td>
			<td><a onclick="return confirm('Are you sure you wish to delete this message?');" href="checkMail.cfm?action=delete&id=#uid#">[delete]</a></td>
		</tr>
		</cfoutput>
	</table>
	</div>	

<cfelseif action is "getdetail">

	<cfimap 
		action="getall"
		attachmentpath="e:\websites\intranet\employeeTesting\attachments"
		name="getDetail" 
		uid="#id#" 
		username="#un#" 
		password="#pw#" 
		port="143" 
		server="mail.copsmonitoring.com">
		
	<div align="center">
	<cfoutput query="getDetail">
	<table width="600" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td>Subject: #subject#</td>
		</tr>
		<tr>
			<td>
			Body:<br />
			<cfif trim(htmlbody) is not "">
			#htmlbody#
			<cfelse>
			#body#
			</cfif>
			</td>
		</tr>
		<cfif trim(attachments) is not "">
		<tr>
			<td>Attachment: <a href="attachments/#attachments#">#attachments#</a></td>
		</tr>
		</cfif>
	</table>
	</cfoutput>
	</div>

<cfelseif action is "delete">

	<cfimap 
		action="delete"
		attachmentpath="e:\websites\intranet\employeeTesting\attachments"
		uid="#id#" 
		username="#un#" 
		password="#pw#" 
		port="143" 
		server="mail.copsmonitoring.com">
	
	<cflocation url="checkMail.cfm">

</cfif>


