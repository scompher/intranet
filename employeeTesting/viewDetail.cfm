
<link rel="stylesheet" type="text/css" href="/styles.css">

<cfquery name="getCoreDetail" datasource="#ds#">
	select distinct employeeTesting_core.*, 
	(select COUNT(employeeTestingInteractionID) from employeeTesting_interaction where employeeTestID = #tid# and result = 'C') as numRight, 
	(select COUNT(employeeTestingInteractionID) from employeeTesting_interaction where employeeTestID = #tid# and result = 'W') as numWrong   
	from employeeTesting_core 
	left join employeeTesting_interaction on employeeTesting_core.employeeTestID = employeeTesting_interaction.employeeTestID 
	where employeeTesting_core.employeeTestID = #tid# 
</cfquery>

<cfquery name="getQuestionDetail" datasource="#ds#">
	select * from employeeTesting_interaction where employeeTestID = #tid# 
	order by employeeTestingInteractionID asc 
</cfquery>

<div align="center">
<table border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Test Detail</b> </td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding"><table width="100%" border="0" cellpadding="5" cellspacing="0">
			<cfoutput query="getCoreDetail">
			<tr>
				<td nowrap><b>Test Name:</b></td>
				<td>#testName#</td>
			</tr>
			<tr>
				<td width="16%" nowrap><b>Employee Name: </b></td>
				<td width="84%">#employeeFirstName# #employeeLastName#</td>
			</tr>
			<tr>
				<td nowrap><b>Employee Number: </b></td>
				<td>#employeeNumber#</td>
			</tr>
			<tr>
				<td nowrap><b>Test Score:</b></td>
				<td>#numberformat(evaluate(percentileScore * 100),999)#%</td>
			</tr>
			<tr>
				<td nowrap><b>Test Result: </b></td>
				<td>#status#</td>
			</tr>
			<tr>
				<td nowrap><b>Number of Questions: </b></td>
				<td>#evaluate((numRight + numWrong) - 1)#</td>
			</tr>
			<tr>
				<td nowrap><b>Number Correct: </b></td>
				<td>#evaluate(numRight - 1)#</td>
			</tr>
			<tr>
				<td nowrap><b>Number Incorrect: </b></td>
				<td>#numWrong#</td>
			</tr>
			</cfoutput>
			<tr>
				<td colspan="2">&nbsp;</td>
				</tr>
			<tr>
				<td colspan="2"><b>Question Detail</b></td>
			</tr>
			<tr>
				<td colspan="2" class="nopadding">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td width="20" align="center" class="linedrow">&nbsp;</td>
						<td width="11" nowrap class="linedrow">&nbsp;</td>
						<td width="328" class="linedrow"><b>Question</b></td>
						<td width="227" class="linedrow"><b>Employee Answer </b></td>
						<td width="312" class="linedrow"><b>Correct Answer </b></td>
					</tr>
					<cfset qNum = 1>
					<cfoutput query="getQuestionDetail">
						<cfif getQuestionDetail.currentRow is not 1>
						<tr>
							<td width="20" align="center" class="linedrow">
							<cfif result is "C">
							<img src="/images/correct.gif" width="20" height="20">
							<cfelse>
							<img src="/images/incorrect.gif" width="20" height="20">
							</cfif>							</td>
							<td nowrap class="linedrow">#qNum#.</td>
							<td class="linedrowrightcolumn">#interactionID#</td>
							<td class="linedrowrightcolumn">#studentResponse#</td>
							<td class="linedrow">#correctResponse#</td>
						</tr>
						<cfset qNum = qNum + 1>
					</cfif>
					</cfoutput>
				</table>				</td>
			</tr>
			
		</table></td>
	</tr>
</table>
<br />
<a class="normal" style="text-decoration:underline;" href="javascript:history.go(-1);">Return to Previous Page</a>
</div>

