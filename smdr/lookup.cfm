
<link rel="stylesheet" type="text/css" href="/styles.css">

<script language="javascript" src="cal2.js">
/*
Xin's Popup calendar script-  Xin Yang (http://www.yxscripts.com/)
Script featured on/available at http://www.dynamicdrive.com/
This notice must stay intact for use
*/
</script>
<script language="javascript" src="cal_conf2.js"></script>

<script type="text/javascript">
function OpenWin(url, w, h) {
	var windowWidth = w;
	var windowHeight = h;
	var left = (screen.width / 2) - (windowWidth / 2);
	var top = (screen.height / 2) - (windowHeight/ 2) - 150;
	DetailWin = window.open(url,"DetailWin","top=" + top + ",left=" + left + ",height=" + windowHeight + ",width=" + windowWidth + ",resizable=1,status=0,titlebar=0,toolbar=0,scrollbars=1");
}

function showhide(layer_ref, state){ 
	if(document.all){ //IS IE 4 or 5 (or 6 beta) 
		eval("document.all." +layer_ref+ ".style.display = state"); 
	} 
	if (document.layers) { //IS NETSCAPE 4 or below 
		document.layers[layer_ref].display = state; 
	} 
	if (document.getElementById &&!document.all) { 
		hza = document.getElementById(layer_ref); 
		hza.style.display = state; 
	} 
}

function jumpToPage(pageToJumpTo,urlString) {
	document.location = 'lookup.cfm?p=' + pageToJumpTo + '&' + urlString;
}
</script>

<cfinclude template="functions.cfm">

<cfset maxrows = 100>
<cfparam name="p" default="1">
<cfparam name="pages" default="1">
<cfparam name="urlString" default="">
<cfparam name="form.callType" default="All">
<cfparam name="form.searchCallingNumber" default="">
<cfparam name="form.searchDuration" default="">
<cfparam name="form.searchDialedNumber" default="">
<cfparam name="form.searchDurationHH" default="">
<cfparam name="form.searchDurationMM" default="">
<cfparam name="form.searchDurationSS" default="">
<cfparam name="form.searchStartDate" default="#dateformat(now(),'mm/dd/yyyy')#">
<cfparam name="form.searchEndDate" default="">
<cfparam name="form.searchStartTimeHH" default="">
<cfparam name="form.searchStartTimeMM" default="">
<cfparam name="form.searchEndTimeHH" default="">
<cfparam name="form.searchEndTimeMM" default="">
<cfparam name="form.searchDNIS" default="">
<cfparam name="form.searchTAC" default="">
<cfparam name="form.sortBy" default="datetimeoccurred">
<cfparam name="form.sortHow" default="desc">
<cfparam name="form.department" default="">
<cfparam name="form.includeRaw" default="0">
<cfset err = "">

<cfset start = (maxrows * p) - (maxrows - 1)>

<!--- change URL params to FORM params --->
<cfif isDefined("url.btnSearchNow")>
	<cfset urlString = "">
	<cfloop list="#cgi.QUERY_STRING#" delimiters="&" index="item">
		<cfset fieldName = listgetat(item,1,"=")>
		<cfif listlen(item,"=") is 2>
			<cfset fieldValue = listgetat(item,2,"=")>
		<cfelse>
			<cfset fieldValue = "">
		</cfif>
		<cfif fieldName is not "p">
			<cfset urlString = listappend(urlString,"#fieldName#=#fieldValue#","&")>
			<cfset formFieldName = "form.#fieldName#">
			<cfset "#formFieldName#" = "#urlDecode(fieldValue)#">
		</cfif>
	</cfloop>
</cfif>

<!--- build URL string --->
<cfif isDefined("form.fieldNames")>
	<cfset urlString = "">
	<cfloop list="#form.fieldNames#" index="item">
		<cfset urlString = listappend(urlString,"#item#=#evaluate(item)#","&")>
	</cfloop>
</cfif>

<cfif isDefined("form.btnSearchNow")>
	<!--- validate --->
	<cfif trim(form.searchStartDate) is not "">
		<cfif not isDate(form.searchStartDate)>
			<cfset err = listappend(err,"The starting date must be a valid date.")>
		<cfelse>
			<cfset sixMonthsBack = dateadd("m",-6,now())>
			<cfif datecompare(form.searchStartDate,sixMonthsBack) is -1>
				<cfset err = listappend(err,"You cannot search more than 6 months prior to today.")>
			</cfif>
		</cfif>
	<cfelse>
		<cfset err = listappend(err,"The starting date is required.")>
	</cfif>
	<!--- validate --->
	<cfif trim(err) is "">
		<cfset getResults = searchSMDR(form,start,maxrows,false)>
		<cfif isnumeric(getResults.itemCount)>
			<cfset totalItems = getResults.itemCount>
		<cfelse>
			<cfset totalItems = 0>
		</cfif>
		<cfset pages = ceiling(totalItems / maxrows)>
	</cfif>
</cfif>

<cfif isDefined("btnExcelDownload") or isDefined("btnExcelDownloadDetail")>
	<cfif isDefined("btnExcelDownloadDetail")>
		<cfset getResults = searchSMDR(form,start,maxrows,true,true)>
	<cfelse>
		<cfset getResults = searchSMDR(form,start,maxrows,true,false)>
	</cfif>
	<cfset colList = getResults.columnList>
	<cfset outputfilename = "#gettickcount()##TimeFormat(now(),'hhmmss')#.xls">
	<cfset outputfilepath = "#request.DirectPath#\smdr\downloads">
	<cfset downloadURL = "/smdr/downloads">

	<cfx_query2excel 
		file="#outputfilepath#\#outputfilename#" 
		headings="#colList#" 
		queryFields="#colList#" 
		format="excel" 
		query="#getResults#">
	
	<cfheader name="content-disposition" value="attachment;filename=#outputfilename#"> 
	<cfcontent type="application/unknown" file="#outputfilepath#\#outputfilename#"> 
	<cfabort>
</cfif>

<cfquery name="getDepts" datasource="smdr">
	select * from departments 
	order by departmentName asc 
</cfquery>

<!--- 
<br />
<div align="center">
<table width="600" border="0" cellpadding="5" cellspacing="0" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold; color:#FFFFFF; border:solid thin #CC0000; background-color:#FF0000; padding:5px">
	<tr>
		<td>IMPORTANT NOTICE</td>
	</tr>
	<cfoutput>
	<tr>
		<td>
		SMDR Lookups only go back as far as 6 months. Any information needed beyond that please contact Phil Gregory at ext. 1706
		</td>
	</tr>
	</cfoutput>
</table>
</div>
 --->

<br />

<div align="center">
<cfif trim(err) is not "">
<span class="alert">
<cfloop list="#err#" index="e">
<cfoutput>
<b style="font-size:14px;">#e#</b><br />
</cfoutput>
</cfloop>
</span>
<br />
</cfif>
<table border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Search SMDR 9000</b></td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
			<table border="0" cellspacing="0" cellpadding="5">
				<cfform method="post" action="lookup.cfm" name="searchForm">
				<tr>
					<td class="nopadding">
						<table width="100%" border="0" cellspacing="0" cellpadding="5">
							<tr>
								<td> <b>Search Items By</b> (Use * as a wildcard) </td>
							</tr>
							<tr>
								<td><b>Date/Time Range:</b></td>
							</tr>
							<tr>
								<td class="nopadding">
									<table border="0" cellspacing="0" cellpadding="5">
										<tr>
											<td> Starting Date:
												<cfinput type="text" name="searchStartDate" style="vertical-align:middle; width:75px" value="#searchStartDate#" validate="date" message="Please enter a valid start date" />
												<a style="text-decoration:none;" href="javascript:showCal('startingDate');"><img border="0" src="../images/calicon.gif" width="20" height="20" align="absmiddle" /></a>											</td>
											<td>&nbsp;&nbsp;</td>
											<td> Ending Date:
												<cfinput type="text" name="searchEndDate" style="vertical-align:middle; width:75px" value="#searchEndDate#" validate="date" message="Please enter a valid end date" />
												<a style="text-decoration:none;" href="javascript:showCal('endingDate');"><img border="0" src="../images/calicon.gif" width="20" height="20" align="absmiddle" /></a>											</td>
										</tr>
										<tr>
											<td> Starting Time:
												<select name="searchStartTimeHH" style="vertical-align:middle;" onchange="this.form.searchStartTimeMM.selectedIndex = 1;">
													<option value=""></option>
													<cfloop from="0" to="23" index="HH">
														<cfoutput>
															<option <cfif searchStartTimeHH is numberformat(HH,00)>selected</cfif> value="#numberformat(HH,00)#">#numberformat(HH,00)#</option>
														</cfoutput>
													</cfloop>
												</select>
												:
												<select name="searchStartTimeMM" style="vertical-align:middle;">
													<option value=""></option>
													<cfloop from="0" to="59" index="MM">
														<cfoutput>
															<option <cfif searchStartTimeMM is numberformat(MM,00)>selected</cfif> value="#numberformat(MM,00)#">#numberformat(MM,00)#</option>
														</cfoutput>
													</cfloop>
												</select>											</td>
											<td>&nbsp;</td>
											<td> Ending Time:
												<select name="searchEndTimeHH" style="vertical-align:middle;" onchange="this.form.searchEndTimeMM.selectedIndex = 1;">
													<option value=""></option>
													<cfloop from="0" to="23" index="HH">
														<cfoutput>
															<option <cfif searchEndTimeHH is numberformat(HH,00)>selected</cfif> value="#numberformat(HH,00)#">#numberformat(HH,00)#</option>
														</cfoutput>
													</cfloop>
												</select>
												:
												<select name="searchEndTimeMM" style="vertical-align:middle;">
													<option value=""></option>
													<cfloop from="0" to="59" index="MM">
														<cfoutput>
															<option <cfif searchEndTimeMM is numberformat(MM,00)>selected</cfif> value="#numberformat(MM,00)#">#numberformat(MM,00)#</option>
														</cfoutput>
													</cfloop>
												</select>											</td>
										</tr>
										<tr>
										    <td colspan="3" class="smallred">SMDR Lookup data only exists for the last 6 months.<br />
										        If you need data beyond that, please contact Phil Gregory at ext.1706 </td>
										    </tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td width="120" align="center" nowrap="nowrap">
					<a target="_blank" href="http://en.wikipedia.org/wiki/HAL_9000">
					<img src="/images/smdr9000logo.gif" width="100" height="86" border="0" />					</a>					</td>
				</tr>
				<tr>
					<td colspan="2" class="nopadding">
						<table border="0" cellspacing="0" cellpadding="5">
							<tr>
								<td><b>Call Type:</b></td>
								<td>&nbsp;</td>
								<td><b>Calls Made From Number/Extension: </b></td>
							</tr>
							<tr>
								<td>
									<select name="callType">
										<option <cfif form.callType is "All">selected</cfif> value="All">All Calls</option>
										<option <cfif form.callType is "Incoming">selected</cfif> value="Incoming">Incoming Calls</option>
										<option <cfif form.callType is "Outgoing">selected</cfif> value="Outgoing">Outgoing Calls</option>
									</select>
								</td>
								<td>&nbsp;</td>
								<td>
									<input name="searchCallingNumber" type="text" value="<cfoutput>#form.searchCallingNumber#</cfoutput>" />
								</td>
							</tr>
							<tr>
								<td><b>Duration:</b></td>
								<td>&nbsp;</td>
								<td><b>Calls Made To Number/Extension: </b></td>
							</tr>
							<tr>
								<td nowrap="nowrap">
									<select name="searchDuration" style="vertical-align:middle">
										<option value=""></option>
										<option <cfif form.searchDuration is "longerThan">selected</cfif> value="longerThan">Longer than</option>
										<option <cfif form.searchDuration is "shorterThan">selected</cfif> value="shorterThan">Shorter than</option>
										<option <cfif form.searchDuration is "equalTo">selected</cfif> value="equalTo">Equal to</option>
									</select>
									&nbsp;
									<input type="text" maxlength="2" name="searchDurationHH" value="<cfoutput>#form.searchDurationHH#</cfoutput>" style="width:25px; vertical-align:middle" />
									Hours
									&nbsp;
									<input type="text" maxlength="2" name="searchDurationMM" value="<cfoutput>#form.searchDurationMM#</cfoutput>" style="width:25px; vertical-align:middle" />
									Minutes
									&nbsp;
									<input type="text" maxlength="2" name="searchDurationSS" value="<cfoutput>#form.searchDurationSS#</cfoutput>" style="width:25px; vertical-align:middle" />
									Seconds									</td>
								<td>&nbsp;</td>
								<td>
									<input name="searchDialedNumber" type="text" value="<cfoutput>#form.searchDialedNumber#</cfoutput>" />
								</td>
							</tr>
							<tr>
								<td nowrap="nowrap"><b>DNIS:</b> (separate multiple DNIS with a comma)</td>
								<td nowrap="nowrap" class="nopadding">&nbsp;</td>
								<td valign="top" nowrap="nowrap" class="nopadding">&nbsp;</td>
							</tr>
							<tr>
								<td nowrap="nowrap">
									<input name="searchDNIS" type="text" id="searchDNIS" value="<cfoutput>#form.searchDNIS#</cfoutput>" />
								</td>
								<td nowrap="nowrap" class="nopadding">&nbsp;</td>
								<td valign="top" nowrap="nowrap" class="nopadding">&nbsp;</td>
							</tr>
							<tr>
								<td nowrap="nowrap" class="nopadding">
									<table border="0" cellspacing="0" cellpadding="5">
										<tr>
											<td><b>Sort By: </b></td>
											<td><b>Sort How: </b></td>
										</tr>
										<tr>
											<td>
												<select name="sortBy" style="vertical-align:middle">
													<option value=""></option>
													<option <cfif form.sortby is "datetimeoccurred">selected</cfif> value="datetimeoccurred">Date/Time</option>
													<option <cfif form.sortby is "duration">selected</cfif> value="duration">Duration</option>
													<option <cfif form.sortby is "dialedNumber">selected</cfif> value="dialedNumber">Dialed Number</option>
													<option <cfif form.sortby is "callingNumber">selected</cfif> value="callingNumber">Calling Number</option>
												</select>
											</td>
											<td>
												<select name="sortHow" style="vertical-align:middle">
													<option value=""></option>
													<option <cfif form.sortHow is "asc">selected</cfif> value="asc">Ascending</option>
													<option <cfif form.sortHow is "desc">selected</cfif> value="desc">Descending</option>
												</select>
											</td>
										</tr>
									</table>
								</td>
								<td nowrap="nowrap" class="nopadding">&nbsp;</td>
								<td valign="top" nowrap="nowrap" class="nopadding">
									<table width="100%" border="0" cellspacing="0" cellpadding="5">
										<tr>
											<td><b>Department:</b></td>
										</tr>
										<tr>
											<td>
												<select name="department">
													<option value=""></option>
													<cfoutput query="getDepts">
														<option <cfif form.department is getDepts.departmentid>selected</cfif> value="#departmentid#">#departmentName#</option>
													</cfoutput>
												</select>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
					</tr>
				<tr>
					<td colspan="2"><b>TAC:</b> (separate multiples with a comma)</td>
				</tr>
				<tr>
					<td colspan="2">
						<input name="searchTAC" type="text" value="<cfoutput>#form.searchTAC#</cfoutput>" />
					</td>
				</tr>
				<tr>
				    <td colspan="2" valign="middle">
				        <input type="checkbox" name="includeRaw" value="checkbox" style="vertical-align:middle;" /> Include RAW </td>
				    </tr>
				<tr>
					<td colspan="2" nowrap="nowrap">
						<input onclick="showhide('loadingLayer','block');" name="btnSearchNow" type="submit" class="sidebar" value="Search Now" />
						<input type="button" class="sidebar" value="New Search" onclick="document.location='lookup.cfm';" />
						<input type="submit" name="btnExcelDownload" class="sidebar" value="Download Results" style="width:140px;" />
						<input type="submit" name="btnExcelDownloadDetail" class="sidebar" value="Download Detailed Results" style="width:175px;" />
					</td>
					</tr>
				</cfform>	
			</table>
		</td>
	</tr>
</table>
<br>
<br />
<a style="text-decoration:underline" class="normal" href="/index.cfm">Return to Intranet</a>
<div id="loadingLayer" style="display:none;">
<br />
<img src="/images/loading.gif" width="30" height="30" />
<br />
<br />
<span class="normal"><b>Searching for Results</b></span>
<br />
</div>
<br />
<br />
<cfif isDefined("getResults")>
	<cfif totalItems is 0>
		<span class="alert"><b>Sorry, there are no items which match your search criteria</b></span>
		<br>
	<cfelse>
		<span class="normal">
		<b style="font-size:18px">Search Results</b>
		<br /><br />
		<cfoutput>#numberFormat(getResults.itemCount)# items found</cfoutput>
		<br /><br />
		<cfset end = start + maxrows - 1>
		<cfif end gt getResults.itemCount>
			<cfset end = getResults.itemCount>
		</cfif>
		<cfoutput>
		Displaying items #start# - #end#
		</cfoutput>
		<br />
		</span>
		<br />
		<cfif pages gt 1>
		<form method="post" action="lookup.cfm" name="pageForm">
		<table width="725" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td>
				<!---
				Page 
				<cfloop from="1" to="#pages#" index="page">
				<cfoutput>
				<cfif page is p>
					<span style="font-size:14px; font-weight:bold">#page#</span>
				<cfelse>
					<a style="text-decoration:underline" href="lookup.cfm?p=#page#&#urlString#">#page#</a>
				</cfif>
				<cfif page is not pages> | </cfif>
				</cfoutput>
				</cfloop>
				--->
				<cfoutput>
				Jump to page 
				<input type="text" name="p" value="#p#" style="width:40px; vertical-align:middle;" />
				of #numberformat(pages)# 
				<a style="text-decoration:underline" href="javascript:jumpToPage(document.pageForm.p.value,'#urlString#')">[GO]</a>
				<cfif p gt 1>
				<a style="text-decoration:underline" href="lookup.cfm?p=#evaluate(p - 1)#&#urlString#">[Previous Page]</a>
				</cfif>
				<cfif p is not pages>
				<a style="text-decoration:underline" href="lookup.cfm?p=#evaluate(p + 1)#&#urlString#">[Next Page]</a>
				</cfif>
				</cfoutput> 
				</td>
			</tr>
		</table>
		</form>
		</cfif>
		<table width="725" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td class="highlightbar"><b>SMDR</b></td>
			</tr>
			<tr>
				<td class="greyrowbottomnopadding">
					<table width="100%" border="0" cellspacing="0" cellpadding="5">
						<tr>
							<td valign="bottom" class="linedrow">&nbsp;</td>
							<td valign="bottom" class="linedrow">&nbsp;</td>
							<td valign="bottom" class="linedrow"><b>Type</b></td>
							<td valign="bottom" class="linedrow"><b>Date/Time</b></td>
							<td valign="bottom" class="linedrow"><b>Duration</b><br />(hh:mm:ss)</td>
							<td valign="bottom" class="linedrow"><b>Duration</b><br />(in seconds)</td>
							<td valign="bottom" class="linedrow"><b>SMDR<br />DNIS</b></td>
							<td valign="bottom" class="linedrow"><b>Dialed #</b></td>
							<td valign="bottom" class="linedrow"><b>Calling #</b></td>
						</tr>
						<cfset currRow = start>
						<cfoutput query="getResults">
							<cfif getResults.currentRow mod 2 is 0><cfset bgc = "##EEEEEE"><cfelse><cfset bgc = "##FFFFFF"></cfif>
							<cfif len(duration) is 4>
								<cfset durationHrs = mid(duration,1,1)>
								<cfset durationMins = mid(duration,2,2)>
								<cfset durationSec = mid(duration,4,1)>
								<cfset durationSec = durationSec * 6>
							<cfelseif len(duration) is 5>
								<cfset durationHrs = mid(duration,1,1)>
								<cfset durationMins = mid(duration,2,2)>
								<cfset durationSec = mid(duration,4,2)>
							</cfif>
							<cfset callDurationInSeconds = 0>
							<cfset callDurationInSeconds = callDurationInSeconds + (left(getResults.duration,1) * 3600)>
							<cfset callDurationInSeconds = callDurationInSeconds + (mid(getResults.duration,2,2) * 60)>
							<cfset callDurationInSeconds = callDurationInSeconds + right(getResults.duration,2)>
							<tr bgcolor="#bgc#">
								<td class="linedrow">#currRow#.</td>
								<td class="linedrow">
								<a target="_blank" href="viewDetail.cfm?itemid=#itemid#">
								<img src="/images/manifyingglass.gif" alt="View Details" width="15" height="15" border="0" /></a>
								</td>
								<td class="linedrow">
								<cfif accessCodeDialed is not "">
								Outgoing
								<cfelse>
								Incoming
								</cfif>
								</td>
								<td class="linedrow">#dateformat(datetimeoccurred,'mm/dd/yyyy')# #timeformat(datetimeoccurred,'HH:mm')#&nbsp;</td>
								<td class="linedrow">
								#numberformat(durationHrs,'00')#:#durationMins#:#numberformat(durationSec,'00')#&nbsp;
								</td>
								<td class="linedrow" align="right">
								#callDurationInSeconds#&nbsp;&nbsp;
								</td>
								<td class="linedrow">
								<cfset ustring = "allLinesCrosslookup.cfm?d=#dnis#">
								<a style="text-decoration:underline" href="javascript:OpenWin('#ustring#',400,200);">#dnis#</a>
								&nbsp;
								</td>
								<td class="linedrow">
								<cfif len(dialedNumber) is 11>
									<a target="_blank" style="text-decoration:underline;" href="http://www.whitepages.com/search/ReversePhone?full_phone=#formatPhone(dialedNumber)#">#formatPhone(dialedNumber)#</a>&nbsp;
								<cfelse>
									#formatPhone(dialedNumber)#
								</cfif>
								</td>
								<td class="linedrow">
								<cfif len(callingNumber) is 10>
									<a target="_blank" style="text-decoration:underline;" href="http://www.whitepages.com/search/ReversePhone?full_phone=#formatPhone(callingNumber)#">#formatPhone(callingNumber)#</a>&nbsp;
								<cfelse>
									#formatPhone(callingNumber)#&nbsp;
								</cfif>
								</td>
							</tr>
							<cfset currRow = currRow + 1>
						</cfoutput>
					</table>
				</td>
			</tr>
		</table>
		<!---
		<cfif pages gt 1>
		<table width="725" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td>
				Page 
				<cfloop from="1" to="#pages#" index="page">
				<cfoutput>
				<cfif page is p>
					<span style="font-size:14px; font-weight:bold">#page#</span>
				<cfelse>
					<a style="text-decoration:underline" href="lookup.cfm?p=#page#&#urlString#">#page#</a>
				</cfif>
				<cfif page is not pages> | </cfif>
				</cfoutput>
				</cfloop>
				</td>
			</tr>
		</table>
		</cfif>
		--->
		</cfif>
</cfif>
</div>
