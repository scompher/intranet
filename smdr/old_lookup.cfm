
<link rel="stylesheet" type="text/css" href="/styles.css">

<script language="javascript" src="cal2.js">
/*
Xin's Popup calendar script-  Xin Yang (http://www.yxscripts.com/)
Script featured on/available at http://www.dynamicdrive.com/
This notice must stay intact for use
*/
</script>
<script language="javascript" src="cal_conf2.js"></script>

<script type="text/javascript">
function OpenWin(url, w, h) {
	var windowWidth = w;
	var windowHeight = h;
	var left = (screen.width / 2) - (windowWidth / 2);
	var top = (screen.height / 2) - (windowHeight/ 2) - 150;
	DetailWin = window.open(url,"DetailWin","top=" + top + ",left=" + left + ",height=" + windowHeight + ",width=" + windowWidth + ",resizable=1,status=0,titlebar=0,toolbar=0,scrollbars=1");
}

function showhide(layer_ref, state){ 
	if(document.all){ //IS IE 4 or 5 (or 6 beta) 
		eval("document.all." +layer_ref+ ".style.display = state"); 
	} 
	if (document.layers) { //IS NETSCAPE 4 or below 
		document.layers[layer_ref].display = state; 
	} 
	if (document.getElementById &&!document.all) { 
		hza = document.getElementById(layer_ref); 
		hza.style.display = state; 
	} 
}
</script>

<cfinclude template="functions.cfm">

<cfset maxrows = 100>
<cfparam name="p" default="1">
<cfparam name="pages" default="1">
<cfparam name="urlString" default="">
<cfparam name="form.callType" default="All">
<cfparam name="form.searchCallingNumber" default="">
<cfparam name="form.searchDuration" default="">
<cfparam name="form.searchDialedNumber" default="">
<cfparam name="form.searchDurationHH" default="">
<cfparam name="form.searchDurationMM" default="">
<cfparam name="form.searchStartDate" default="">
<cfparam name="form.searchEndDate" default="">
<cfparam name="form.searchStartTimeHH" default="">
<cfparam name="form.searchStartTimeMM" default="">
<cfparam name="form.searchEndTimeHH" default="">
<cfparam name="form.searchEndTimeMM" default="">
<cfparam name="form.searchDNIS" default="">
<cfparam name="form.searchTAC" default="">
<cfparam name="form.sortBy" default="datetimeoccurred">
<cfparam name="form.sortHow" default="desc">
<cfparam name="form.department" default="">

<cfset start = (maxrows * p) - (maxrows - 1)>

<!--- change URL params to FORM params --->
<cfif isDefined("url.btnSearchNow")>
	<cfset urlString = "">
	<cfloop list="#cgi.QUERY_STRING#" delimiters="&" index="item">
		<cfset fieldName = listgetat(item,1,"=")>
		<cfif listlen(item,"=") is 2>
			<cfset fieldValue = listgetat(item,2,"=")>
		<cfelse>
			<cfset fieldValue = "">
		</cfif>
		<cfif fieldName is not "p">
			<cfset urlString = listappend(urlString,"#fieldName#=#fieldValue#","&")>
			<cfset formFieldName = "form.#fieldName#">
			<cfset "#formFieldName#" = "#urlDecode(fieldValue)#">
		</cfif>
	</cfloop>
</cfif>

<!--- build URL string --->
<cfif isDefined("form.fieldNames")>
	<cfset urlString = "">
	<cfloop list="#form.fieldNames#" index="item">
		<cfset urlString = listappend(urlString,"#item#=#evaluate(item)#","&")>
	</cfloop>
</cfif>

<cfif isDefined("form.btnSearchNow")>
	<cfset getResults = searchSMDR(form)>
	<cfset pages = ceiling(getResults.recordCount / maxrows)>
</cfif>

<cfif isDefined("btnExcelDownload")>
	<cfset getResults = searchSMDR(form)>
	<cfset colList = getResults.columnList>
	<cfset outputfilename = "#cookie.adminlogin##TimeFormat(now(),'hhmmss')#.xls">
	<cfset outputfilepath = "#request.DirectPath#\smdr\downloads">
	<cfset downloadURL = "/smdr/downloads">
	<cfx_query2excel 
		file="#outputfilepath#\#outputfilename#" 
		headings="#colList#" 
		queryFields="#colList#" 
		delimiter="#chr(9)#" 
		query="#getResults#">
	
	<cfheader name="content-disposition" value="attachment;filename=#outputfilename#"> 
	<cfcontent type="application/unknown" file="#outputfilepath#\#outputfilename#"> 
	<cfabort>
</cfif>

<cfquery name="getDepts" datasource="smdr">
	select * from departments 
	order by departmentName asc 
</cfquery>

<br />

<div align="center">
<table border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Search SMDR 9000</b></td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
			<table border="0" cellspacing="0" cellpadding="5">
				<form method="post" action="lookup.cfm" name="searchForm">
				<tr>
					<td class="nopadding">
						<table width="100%" border="0" cellspacing="0" cellpadding="5">
							<tr>
								<td> <b>Search Items By</b> (Use * as a wildcard) </td>
							</tr>
							<tr>
								<td><b>Date/Time Range:</b></td>
							</tr>
							<tr>
								<td class="nopadding">
									<table border="0" cellspacing="0" cellpadding="5">
										<tr>
											<td> Starting Date:
												<input type="text" name="searchStartDate" style="vertical-align:middle; width:75px" value="<cfoutput>#searchStartDate#</cfoutput>" />
												<a style="text-decoration:none;" href="javascript:showCal('startingDate');"><img border="0" src="../images/calicon.gif" width="20" height="20" align="absmiddle" /></a>											</td>
											<td>&nbsp;&nbsp;</td>
											<td> Ending Date:
												<input type="text" name="searchEndDate" style="vertical-align:middle; width:75px" value="<cfoutput>#searchEndDate#</cfoutput>" />
												<a style="text-decoration:none;" href="javascript:showCal('endingDate');"><img border="0" src="../images/calicon.gif" width="20" height="20" align="absmiddle" /></a>											</td>
										</tr>
										<tr>
											<td> Starting Time:
												<select name="searchStartTimeHH" style="vertical-align:middle;">
													<option value=""></option>
													<cfloop from="0" to="23" index="HH">
														<cfoutput>
															<option <cfif searchStartTimeHH is numberformat(HH,00)>selected</cfif> value="#numberformat(HH,00)#">#numberformat(HH,00)#</option>
														</cfoutput>
													</cfloop>
												</select>
												:
												<select name="searchStartTimeMM" style="vertical-align:middle;">
													<option value=""></option>
													<cfloop from="0" to="59" index="MM">
														<cfoutput>
															<option <cfif searchStartTimeMM is numberformat(MM,00)>selected</cfif> value="#numberformat(MM,00)#">#numberformat(MM,00)#</option>
														</cfoutput>
													</cfloop>
												</select>
											</td>
											<td>&nbsp;</td>
											<td> Ending Time:
												<select name="searchEndTimeHH" style="vertical-align:middle;">
													<option value=""></option>
													<cfloop from="0" to="23" index="HH">
														<cfoutput>
															<option <cfif searchEndTimeHH is numberformat(HH,00)>selected</cfif> value="#numberformat(HH,00)#">#numberformat(HH,00)#</option>
														</cfoutput>
													</cfloop>
												</select>
												:
												<select name="searchEndTimeMM" style="vertical-align:middle;">
													<option value=""></option>
													<cfloop from="0" to="59" index="MM">
														<cfoutput>
															<option <cfif searchEndTimeMM is numberformat(MM,00)>selected</cfif> value="#numberformat(MM,00)#">#numberformat(MM,00)#</option>
														</cfoutput>
													</cfloop>
												</select>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td width="120" align="center" nowrap="nowrap">
					<a target="_blank" href="http://en.wikipedia.org/wiki/HAL_9000">
					<img src="/images/smdr9000logo.gif" width="100" height="86" border="0" />
					</a>
					</td>
				</tr>
				<tr>
					<td colspan="2" class="nopadding">
						<table border="0" cellspacing="0" cellpadding="5">
							<tr>
								<td><b>Call Type:</b></td>
								<td>&nbsp;</td>
								<td><b>Calls Made From Number/Extension: </b></td>
							</tr>
							<tr>
								<td>
									<select name="callType">
										<option <cfif form.callType is "All">selected</cfif> value="All">All Calls</option>
										<option <cfif form.callType is "Incoming">selected</cfif> value="Incoming">Incoming Calls</option>
										<option <cfif form.callType is "Outgoing">selected</cfif> value="Outgoing">Outgoing Calls</option>
									</select>
								</td>
								<td>&nbsp;</td>
								<td>
									<input name="searchCallingNumber" type="text" value="<cfoutput>#form.searchCallingNumber#</cfoutput>" />
								</td>
							</tr>
							<tr>
								<td><b>Duration:</b></td>
								<td>&nbsp;</td>
								<td><b>Calls Made To Number/Extension: </b></td>
							</tr>
							<tr>
								<td nowrap="nowrap">
									<select name="searchDuration" style="vertical-align:middle">
										<option value=""></option>
										<option <cfif form.searchDuration is "longerThan">selected</cfif> value="longerThan">Longer than</option>
										<option <cfif form.searchDuration is "shorterThan">selected</cfif> value="shorterThan">Shorter than</option>
									</select>
									&nbsp;
									<input type="text" maxlength="2" name="searchDurationHH" value="<cfoutput>#form.searchDurationHH#</cfoutput>" style="width:25px; vertical-align:middle" />
									Hours
									&nbsp;
								<input type="text" maxlength="2" name="searchDurationMM" value="<cfoutput>#form.searchDurationMM#</cfoutput>" style="width:25px; vertical-align:middle" />
									Minutes </td>
								<td>&nbsp;</td>
								<td>
									<input name="searchDialedNumber" type="text" value="<cfoutput>#form.searchDialedNumber#</cfoutput>" />
								</td>
							</tr>
							<tr>
								<td nowrap="nowrap"><b>DNIS:</b> (separate multiple DNIS with a comma)</td>
								<td nowrap="nowrap" class="nopadding">&nbsp;</td>
								<td valign="top" nowrap="nowrap" class="nopadding">&nbsp;</td>
							</tr>
							<tr>
								<td nowrap="nowrap">
									<input name="searchDNIS" type="text" id="searchDNIS" value="<cfoutput>#form.searchDNIS#</cfoutput>" />
								</td>
								<td nowrap="nowrap" class="nopadding">&nbsp;</td>
								<td valign="top" nowrap="nowrap" class="nopadding">&nbsp;</td>
							</tr>
							<tr>
								<td nowrap="nowrap" class="nopadding">
									<table border="0" cellspacing="0" cellpadding="5">
										<tr>
											<td><b>Sort By: </b></td>
											<td><b>Sort How: </b></td>
										</tr>
										<tr>
											<td>
												<select name="sortBy" style="vertical-align:middle">
													<option value=""></option>
													<option <cfif form.sortby is "datetimeoccurred">selected</cfif> value="datetimeoccurred">Date/Time</option>
													<option <cfif form.sortby is "duration">selected</cfif> value="duration">Duration</option>
													<option <cfif form.sortby is "dialedNumber">selected</cfif> value="dialedNumber">Dialed Number</option>
													<option <cfif form.sortby is "callingNumber">selected</cfif> value="callingNumber">Calling Number</option>
												</select>
											</td>
											<td>
												<select name="sortHow" style="vertical-align:middle">
													<option value=""></option>
													<option <cfif form.sortHow is "asc">selected</cfif> value="asc">Ascending</option>
													<option <cfif form.sortHow is "desc">selected</cfif> value="desc">Descending</option>
												</select>
											</td>
										</tr>
									</table>
								</td>
								<td nowrap="nowrap" class="nopadding">&nbsp;</td>
								<td valign="top" nowrap="nowrap" class="nopadding">
									<table width="100%" border="0" cellspacing="0" cellpadding="5">
										<tr>
											<td><b>Department:</b></td>
										</tr>
										<tr>
											<td>
												<select name="department">
													<option value=""></option>
													<cfoutput query="getDepts">
														<option <cfif form.department is getDepts.departmentid>selected</cfif> value="#departmentid#">#departmentName#</option>
													</cfoutput>
												</select>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
					</tr>
				<tr>
					<td colspan="2"><b>TAC:</b></td>
				</tr>
				<tr>
					<td colspan="2">
						<input name="searchTAC" type="text" value="<cfoutput>#form.searchTAC#</cfoutput>" />
					</td>
				</tr>
				<tr>
					<td colspan="2" nowrap="nowrap">
						<input onclick="showhide('loadingLayer','block');" name="btnSearchNow" type="submit" class="sidebar" value="Search Now" />
						<input type="button" class="sidebar" value="New Search" onclick="document.location='lookup.cfm';" />
						<input type="submit" name="btnExcelDownload" class="sidebar" value="Download Results" />
					</td>
					</tr>
				</form>	
			</table>
		</td>
	</tr>
</table>
<br>
<br />
<a style="text-decoration:underline" class="normal" href="/index.cfm">Return to Intranet</a>
<div id="loadingLayer" style="display:none;">
<br />
<img src="/images/loading.gif" width="30" height="30" />
<br />
<br />
<span class="normal"><b>Searching for Results</b></span>
<br />
</div>
<br />
<br />
<cfif isDefined("getResults")>
	<cfif getResults.recordcount is 0>
		<span class="alert"><b>Sorry, there are no items which match your search criteria</b></span>
		<br>
	<cfelse>
		<span class="normal">
		<b style="font-size:18px">Search Results</b>
		<br /><br />
		<cfoutput>#numberFormat(getResults.recordcount)# items found</cfoutput>
		<br /><br />
		<cfset end = start + maxrows - 1>
		<cfif end gt getResults.recordcount>
			<cfset end = getResults.recordcount>
		</cfif>
		<cfoutput>
		Displaying items #start# - #end#
		</cfoutput>
		<br />
		</span>
		<br />
		<cfif pages gt 1>
		<table width="725" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td>
				Page 
				<cfloop from="1" to="#pages#" index="page">
				<cfoutput>
				<cfif page is p>
					<span style="font-size:14px; font-weight:bold">#page#</span>
				<cfelse>
					<a style="text-decoration:underline" href="lookup.cfm?p=#page#&#urlString#">#page#</a>
				</cfif>
				<cfif page is not pages> | </cfif>
				</cfoutput>
				</cfloop>
				</td>
			</tr>
		</table>
		</cfif>
		<table width="725" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td class="highlightbar"><b>SMDR</b></td>
			</tr>
			<tr>
				<td class="greyrowbottomnopadding">
					<table width="100%" border="0" cellspacing="0" cellpadding="5">
						<tr>
							<td valign="bottom" class="linedrow">&nbsp;</td>
							<td valign="bottom" class="linedrow">&nbsp;</td>
							<td valign="bottom" class="linedrow"><b>Type</b></td>
							<td valign="bottom" class="linedrow"><b>Date/Time</b></td>
							<td valign="bottom" class="linedrow"><b>Duration</b><br />(hh:mm:ss)</td>
							<td valign="bottom" class="linedrow"><b>SMDR<br />DNIS</b></td>
							<td valign="bottom" class="linedrow"><b>Dialed #</b></td>
							<td valign="bottom" class="linedrow"><b>Calling #</b></td>
						</tr>
						<cfoutput query="getResults" startrow="#start#" maxrows="#maxrows#">
						<cfif getResults.currentRow mod 2 is 0><cfset bgc = "##EEEEEE"><cfelse><cfset bgc = "##FFFFFF"></cfif>
						<cfif len(duration) is 4>
							<cfset durationHrs = mid(duration,1,1)>
							<cfset durationMins = mid(duration,2,2)>
							<cfset durationSec = mid(duration,4,1)>
							<cfset durationSec = durationSec * 6>
						<cfelseif len(duration) is 5>
							<cfset durationHrs = mid(duration,1,1)>
							<cfset durationMins = mid(duration,2,2)>
							<cfset durationSec = mid(duration,4,2)>
						</cfif>
						<tr bgcolor="#bgc#">
							<td class="linedrow">#getResults.currentRow#.</td>
							<td class="linedrow">
							<a target="_blank" href="viewDetail.cfm?itemid=#itemid#">
							<img src="/images/manifyingglass.gif" alt="View Details" width="15" height="15" border="0" />							</a>							</td>
							<td class="linedrow">
							<cfif accessCodeDialed is not "">
							Outgoing
							<cfelse>
							Incoming
							</cfif>
							</td>
							<td class="linedrow">#dateformat(datetimeoccurred,'mm/dd/yyyy')# #timeformat(datetimeoccurred,'HH:mm')#&nbsp;</td>
							<td class="linedrow">
							#numberformat(durationHrs,'00')#:#durationMins#:#numberformat(durationSec,'00')#&nbsp;
							</td>
							<td class="linedrow">
							<cfset ustring = "allLinesCrossLookup.cfm?d=#dnis#">
							<a style="text-decoration:underline" href="javascript:OpenWin('#ustring#',400,200);">#dnis#</a>
							&nbsp;
							</td>
							<td class="linedrow">
							<cfif len(dialedNumber) is 11>
								<a target="_blank" style="text-decoration:underline;" href="http://www.whitepages.com/search/ReversePhone?full_phone=#formatPhone(dialedNumber)#">#formatPhone(dialedNumber)#</a>&nbsp;
							<cfelse>
								#formatPhone(dialedNumber)#
							</cfif>
							</td>
							<td class="linedrow">
							<cfif len(callingNumber) is 10>
								<a target="_blank" style="text-decoration:underline;" href="http://www.whitepages.com/search/ReversePhone?full_phone=#formatPhone(callingNumber)#">#formatPhone(callingNumber)#</a>&nbsp;
							<cfelse>
								#formatPhone(callingNumber)#&nbsp;
							</cfif>
							</td>
						</tr>
						</cfoutput>
					</table>
				</td>
			</tr>
		</table>
		<cfif pages gt 1>
		<table width="725" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td>
				Page 
				<cfloop from="1" to="#pages#" index="page">
				<cfoutput>
				<cfif page is p>
					<span style="font-size:14px; font-weight:bold">#page#</span>
				<cfelse>
					<a style="text-decoration:underline" href="lookup.cfm?p=#page#&#urlString#">#page#</a>
				</cfif>
				<cfif page is not pages> | </cfif>
				</cfoutput>
				</cfloop>
				</td>
			</tr>
		</table>
		</cfif>
		</cfif>
</cfif>
</div>
