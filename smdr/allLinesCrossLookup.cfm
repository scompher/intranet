<link rel="stylesheet" type="text/css" href="/styles.css">

<script type="text/javascript" language="javascript">
function OpenDetailWin(id) {
	var windowWidth = 750;
	var windowHeight = 470;
	var left = (screen.width / 2) - (windowWidth / 2);
	var top = (screen.height / 2) - (windowHeight/ 2) - 150;
	NewDetailWin = window.open("/allLines/detailview.cfm?id=" + id,"DetailWin","top=" + top + ",left=" + left + ",height=" + windowHeight + ",width=" + windowWidth + ",resizable=1,status=0,titlebar=0,toolbar=0");
}
</script>

<cfparam name="form.searchphoneNumber" default="">
<cfparam name="form.searchdnis" default="">
<cfparam name="form.searchreceiver" default="">
<cfparam name="form.searchdealerNumber" default="">
<cfparam name="form.searchusedFor" default="">
<cfparam name="form.sortResults" default="">
<cfparam name="form.deleteConfirmed" default="">

<cfset ds = "allLines">

<cfinclude template="/allLines/functions.cfm">

<cfset form.searchDnis = url.d>
<cfset form.sortResults = "phoneASC">

<cfset searchResults = SearchSMDRItems(form)>

<div>
<table border="0" cellspacing="0" cellpadding="3">
	<tr>
		<td colspan="2"><b>Phone Numbers Assoicated with DNIS <cfoutput>#url.d#</cfoutput></b></td>
	</tr>
	<cfif searchResults.recordCount gt 0>
		<tr>
			<td class="linedrow"><b>Phone Number</b></td>
			<td class="linedrow"><b>Receiver</b></td>
		</tr>
		<cfoutput query="searchResults">
		<tr>
			<td>#searchResults.currentRow#. <a style="text-decoration:underline;" target="_blank" href="/allLines/detailView.cfm?id=#id#">#phoneNumber#</a></td>
			<td>#receiver#</td>
		</tr>
		</cfoutput>
	<cfelse>
	<tr>
		<td colspan="2">Sorry there are no phone numbers associated with this DNIS </td>
	</tr>
	</cfif>
</table>
</div>

