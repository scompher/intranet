
<cfsetting showdebugoutput="no">

<cfinclude template="functions.cfm">

<cfparam name="extension" default="">

<cfif trim(extension) is "">
	<cfquery name="getExt" datasource="#ds#">
		select extension from admin_users 
		where adminuserid = #cookie.adminlogin# 
	</cfquery>
	<cfset extension = getExt.extension>
</cfif>

<cfparam name="refresh" default="10">

<cfset startDate = dateadd("d",-1,now())>

<cfif extension is not "">
	<cfif len(extension) is 4>
		<cfquery name="getCalls" datasource="smdr">
			select itemid, accessCodeDialed, dateTimeOccurred, dialedNumber, callingNumber from dbo.smdrData with (nolock) 
			where 
			(dateTimeOccurred >= #createodbcdate(startDate)#) and 
			(dialedNumber = '#extension#' or callingNumber = '#extension#') 
			order by dateTimeOccurred desc
		</cfquery>
	<cfelse>
		<cfquery name="getCalls" datasource="smdr">
			select top 100 itemid, accessCodeDialed, dateTimeOccurred, dialedNumber, callingNumber from dbo.smdrData with (nolock) 
			where 
			(dateTimeOccurred >= #createodbcdate(startDate)#) and 
			(dialedNumber = '#extension#' or callingNumber = '#extension#' or dnis = '#extension#' or accessCodeUsed = '#extension#' or incomingTac = '#extension#' or conditionCode = '#extension#') 
			order by dateTimeOccurred desc
		</cfquery>
	</cfif>
</cfif>

<link rel="stylesheet" type="text/css" href="/styles.css">

<cfif isnumeric(extension) and refresh is not "OFF">
	<cfoutput>
	<meta http-equiv="refresh" content="#refresh#;url=scroller.cfm?extension=#extension#&refresh=#refresh#" />
	</cfoutput>
</cfif>

<title><!--- <cfoutput>#dateformat(now(),'mm/dd')#</cfoutput> --->Call History For Ext. <cfoutput>#extension#</cfoutput></title>

<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0">
<div align="center">
<table width="250" border="1" cellspacing="0" cellpadding="3">
    <tr>
        <td class="highlightbar"><b><!--- <cfoutput>#dateformat(now(),'mm/dd')#</cfoutput> --->Call History For Ext. <cfoutput>#extension#</cfoutput></b></td>
    </tr>
    <tr>
        <td class="greyrowbottomnopadding">
		<table width="100%" border="0" cellspacing="0" cellpadding="3">
			<form method="post" action="scroller.cfm">
            <tr>
                <td class="linedrow">
				<cfif extension is ""><cfset extensionVal = "Extension"><cfelse><cfset extensionVal = extension></cfif>
				<cfoutput>
				<input name="extension" onFocus="this.select();" type="text" style="vertical-align:middle; width:60px" value="#extensionVal#">
				</cfoutput>
                <input name="Submit" type="submit" class="sidebarsmall" value="Update" style="vertical-align:middle;">
				</td>
            </tr>
			</form>
			<cfif extension is not "">
			<tr>
				<td>
				Refresh every  
				<cfoutput>
				<cfif refresh is not 5>
				<a style="text-decoration:underline;" href="scroller.cfm?extension=#extension#&refresh=5">5</a> | 
				<cfelse>
				5 | 
				</cfif>
				<cfif refresh is not 10>
				<a style="text-decoration:underline;" href="scroller.cfm?extension=#extension#&refresh=10">10</a> | 
				<cfelse>
				10 | 
				</cfif>
				<a style="text-decoration:underline;" href="scroller.cfm?extension=#extension#&refresh=#refresh#">Now</a> | 
				<cfif refresh is not "OFF">
				<a style="text-decoration:underline;" href="scroller.cfm?extension=#extension#&refresh=OFF">OFF</a> 
				<cfelse>
				OFF 
				</cfif>
				</cfoutput>
				</td>
			</tr>
			</cfif>
            <tr>
                <td class="nopadding">
					<table width="100%" border="0" cellspacing="0" cellpadding="3">
						<tr>
							<td width="17">TYPE</td>
							<td width="45" align="center">TIME</td>
							<td>NUMBER/EXT</td>
						</tr>
						<cfif extension is not "">
							<cfoutput query="getCalls">
							<cfif getCalls.currentRow mod 2 is 0>
								<cfset bgc = "FFFFFF">
							<cfelse>
								<cfset bgc = "DDDDDD">
							</cfif>
							<tr bgcolor="#bgc#">
								<td width="17">
								<a target="_blank" href="viewDetail.cfm?itemid=#itemid#">
								<cfif callingNumber is extension>
									<!--- <img border="0" src="/images/outgoingCall.gif" alt="Outgoing Call" /> --->
									OUTGOING
								<cfelse>
									<!--- <img border="0" src="/images/incomingCall.gif" alt="Incoming Call" /> --->
									INCOMING
								</cfif>
								</a>
								</td>
								<td width="45" align="center">#timeformat(dateTimeOccurred,'HH:mm')#</td>
								<td>
								<cfif callingNumber is not extension>
									<cfset displayNum = formatPhone(callingNumber)>
								<cfelse>
									<cfset displayNum = formatPhone(dialedNumber)>
								</cfif>
								<a target="_blank" href="viewDetail.cfm?itemid=#itemid#">
								<cfif left(displayNum,2) is "1-">
									#right(displayNum,len(displayNum)-2)#
								<cfelse>
									#displayNum#
								</cfif>
								</a>
								</td>
								<!---
								<td>
								<cfif left(otherNum,2) is "1-">
									#right(otherNum,len(otherNum)-2)#
								<cfelse>
									#otherNum#
								</cfif>
								</td>
								--->
							</tr>
							</cfoutput>
						</cfif>
					</table>
				</td>
            </tr>
        </table>
	</td>
    </tr>
</table>
</div>
</body>