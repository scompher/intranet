
<script type="text/javascript" language="javascript">
function openWin(url, w, h) {
	var windowWidth = w;
	var windowHeight = h;
	var left = (screen.width / 2) - (windowWidth / 2);
	var top = (screen.height / 2) - (windowHeight/ 2) - 150;
	DetailWin = window.open(url,'',"top=" + top + ",left=" + left + ",height=" + windowHeight + ",width=" + windowWidth + ",resizable=1,status=0,titlebar=0,toolbar=0,scrollbars=1");
}
</script>

<cfinclude template="functions.cfm">

<link rel="stylesheet" type="text/css" href="/styles.css">

<cfquery name="getCallDetail" datasource="smdr">
	select * 
	from smdrData 
	where itemid = #itemid# 
</cfquery>

<cfquery name="getTacInfo" datasource="smdr">
	select * from trunkTAC_Lookup 
	where trunkTAC = '#getCallDetail.incomingTAC#'
</cfquery>

<cfquery name="getAccessCodeDialed" datasource="smdr">
	select * from trunkTAC_Lookup 
	where trunkTAC = '#getCallDetail.accessCodeDialed#'
</cfquery>

<cfquery name="getAccessCodeUsed" datasource="smdr">
	select * from trunkTAC_Lookup 
	where trunkTAC = '#getCallDetail.accessCodeUsed#'
</cfquery>

<div align="center">
<table border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Call Detail</b>  </td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
			<cfoutput query="getCallDetail">

			<cfif len(duration) is 4>
				<cfset durationHrs = mid(duration,1,1)>
				<cfset durationMins = mid(duration,2,2)>
				<cfset durationSec = mid(duration,4,1)>
				<cfset durationSec = durationSec * 6>
			<cfelseif len(duration) is 5>
				<cfset durationHrs = mid(duration,1,1)>
				<cfset durationMins = mid(duration,2,2)>
				<cfset durationSec = mid(duration,4,2)>
			</cfif>
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td nowrap class="linedrowrightcolumn"><b>Date/Time Occurred: </b></td>
					<td nowrap class="linedrowrightcolumn"><b>Duration:</b> (hh:mm:ss)</td>
					<td nowrap class="linedrowrightcolumn"><b>Condition:</b></td>
					<td nowrap class="linedrowrightcolumn"><b>Access Code Dialed: </b></td>
					<td nowrap class="linedrow"><b>Access Code Used: </b></td>
				</tr>
				<tr>
					<td valign="top" class="linedrowrightcolumn">#dateformat(dateTimeOccurred,'mm/dd/yyyy')# #timeFormat(dateTimeOccurred,'HH:mm')#&nbsp;</td>
					<td valign="top" class="linedrowrightcolumn">#numberformat(durationHrs,'00')#:#durationMins#:#numberformat(durationSec,'00')#&nbsp;</td>
					<td valign="top" class="linedrowrightcolumn">
					#conditionCode#
					<br><br>
					<a style="text-decoration:underline" href="javascript:openWin('conditionCodes.cfm','640','500');">Click Here to View Condition Codes</a>
					&nbsp;
					</td>
					<td valign="top" class="linedrowrightcolumn">
					<cfif getAccessCodeDialed.recordcount is not 0>
					#getAccessCodeDialed.trunkNumber#/#getAccessCodeDialed.trunkTAC#/#getAccessCodeDialed.trunkName#
					</cfif>
					<!--- #accessCodeDialed# --->
					&nbsp;
					</td>
					<td valign="top" class="linedrow">
					<cfif getAccessCodeUsed.recordcount is not 0>
					#getAccessCodeUsed.trunkNumber#/#getAccessCodeUsed.trunkTAC#/#getAccessCodeUsed.trunkName#
					</cfif>
					<!--- #accessCodeUsed# --->
					&nbsp;
					</td>
				</tr>
				<tr>
					<td nowrap class="linedrowrightcolumn"><b>Dialed Number: </b></td>
					<td nowrap class="linedrowrightcolumn"><b>Calling Number: </b></td>
					<td nowrap class="linedrowrightcolumn"><b>DNIS: </b></td>
					<td nowrap class="linedrowrightcolumn"><b>Authorization Code: </b></td>
					<td nowrap class="linedrow"><b>Time In Queue: </b></td>
				</tr>
				<tr>
					<td valign="top" class="linedrowrightcolumn">#formatPhone(dialedNumber)#&nbsp;</td>
					<td valign="top" class="linedrowrightcolumn">#formatPhone(callingNumber)#&nbsp;</td>
					<td valign="top" class="linedrowrightcolumn">#dnis#&nbsp;</td>
					<td valign="top" class="linedrowrightcolumn">#authorizationCode#&nbsp;</td>
					<td valign="top" class="linedrow">#timeInQueue#&nbsp;</td>
				</tr>
				<tr>
					<td nowrap class="linedrowrightcolumn"><b>Disconnect Side:</b></td>
					<td nowrap class="linedrowrightcolumn"><b>Incoming Circuit ID: </b></td>
					<td nowrap class="linedrowrightcolumn"><b>Outgoing Circuit ID: </b></td>
					<td nowrap class="linedrowrightcolumn"><b>Feature Flag: </b></td>
					<td nowrap class="linedrow"><b>Attendant Console: </b></td>
				</tr>
				<tr>
					<td valign="top" class="linedrowrightcolumn">
					<cfswitch expression="#frl#">
						<cfcase value="1">calling number disconnected first</cfcase>
						<cfcase value="2">dialed number disconnected first</cfcase>
						<cfcase value="0">Cannot determine who disconnected first</cfcase>
						<cfdefaultcase>#frl#</cfdefaultcase>
					</cfswitch>
					&nbsp;
					</td>
					<td valign="top" class="linedrowrightcolumn">#incomingCircuitID#&nbsp;</td>
					<td valign="top" class="linedrowrightcolumn">#outgoingCircuitID#&nbsp;</td>
					<td valign="top" class="linedrowrightcolumn">
					<cfswitch expression="#featureFlag#">
						<cfcase value="0">Voice call without network answer, Supervision or a call for which NCA-TSC is not established.</cfcase>
						<cfcase value="1">Data call without network answer supervision.</cfcase>
						<cfcase value="2">Voice call with network answer supervision, but interworked.</cfcase>
						<cfcase value="3">Data call with network answer supervision, but interworked.</cfcase>
						<cfcase value="4">Voice call with network answer supervision.</cfcase>
						<cfcase value="5">Data call with network answer supervision.</cfcase>
						<cfdefaultcase>#featureFlag#</cfdefaultcase>
					</cfswitch>
					&nbsp;
					</td>
					<td valign="top" class="linedrow">#attendantConsole#&nbsp;</td>
				</tr>
				<tr>
					<td nowrap class="linedrowrightcolumn"><b>Incoming TAC: </b></td>
					<td nowrap class="linedrowrightcolumn"><b>Node Number: </b></td>
					<td nowrap class="linedrowrightcolumn"><b>ISDN NSV: </b></td>
					<td nowrap class="linedrowrightcolumn"><b>IXC Code: </b></td>
					<td nowrap class="linedrow"><b>Bearer Capability Class:</b></td>
				</tr>
				<tr>
					<td valign="top" class="linedrowrightcolumn">
					<cfif getTACInfo.recordcount is not 0>
					#getTACInfo.trunkNumber#/#getTACInfo.trunkTAC#/#getTACInfo.trunkName#
					</cfif>
					&nbsp;
					</td>
					<td valign="top" class="linedrowrightcolumn">#nodeNumber#&nbsp;</td>
					<td valign="top" class="linedrowrightcolumn">#isdnNsv#&nbsp;</td>
					<td valign="top" class="linedrowrightcolumn">#ixcCode#&nbsp;</td>
					<td valign="top" class="linedrow">
					<cfswitch expression="#bcc#">
						<cfcase value="0">Voice Grade Data &amp; Voice</cfcase>
						<cfcase value="1">Mode 1, 56 kbps synchronous data</cfcase>
						<cfcase value="2">Mode 2, less than 19.2 kbps synchronous or asynchronous data</cfcase>
						<cfcase value="3">Mode 3, 64 kbps data for Link Access Procedure data (LAPD) protocol</cfcase>
						<cfcase value="4">Mode 0, 64 kbps data clear</cfcase>
						<cfcase value="w">Wideband</cfcase>
						<cfdefaultcase>#bcc#</cfdefaultcase>
					</cfswitch>
					&nbsp;
					</td>
				</tr>
				<tr>
					<td nowrap class="linedrowrightcolumn"><b>MA-UUI:</b></td>
					<td nowrap class="linedrowrightcolumn"><b>Resource Flag: </b></td>
					<td nowrap class="linedrowrightcolumn"><b>Packet Count: </b></td>
					<td nowrap class="linedrowrightcolumn"><b>TSC FLag: </b></td>
					<td nowrap class="linedrow"><b>Bandwidth:</b></td>
				</tr>
				<tr>
					<td valign="top" class="linedrowrightcolumn">#maUui#&nbsp;</td>
					<td valign="top" class="linedrowrightcolumn">
					<cfswitch expression="#resourceFlag#">
						<cfcase value="0">Circuit switched, no conversion device used.</cfcase>
						<cfcase value="1">Packet switched, no conversion device used.</cfcase>
						<cfcase value="2">Circuit switched, conversion device used.</cfcase>
						<cfcase value="3">Packet switched, conversion device used.</cfcase>
						<cfcase value="8">MASI call.</cfcase>
						<cfdefaultcase>#resourceFlag#</cfdefaultcase>
					</cfswitch>
					&nbsp;
					</td>
					<td valign="top" class="linedrowrightcolumn">#packetCount#&nbsp;</td>
					<td valign="top" class="linedrowrightcolumn">
					<cfswitch expression="#tscFlag#">
						<cfcase value="0">Circuit-switched call without TSC requests.</cfcase>
						<cfdefaultcase>#tscFlag#</cfdefaultcase>
					</cfswitch>
					&nbsp;
					</td>
					<td valign="top" class="linedrow">#bandwidth#&nbsp;</td>
				</tr>
				<tr>
					<td nowrap class="linedrowrightcolumn"><b>ISDN CC: </b></td>
					<td nowrap class="linedrowrightcolumn">&nbsp;</td>
					<td nowrap class="linedrowrightcolumn">&nbsp;</td>
					<td nowrap class="linedrowrightcolumn">&nbsp;</td>
					<td nowrap class="linedrow">&nbsp;</td>
				</tr>
				<tr>
					<td valign="top" class="linedrowrightcolumn">#isdnCC#</td>
					<td valign="top" class="linedrowrightcolumn">&nbsp;</td>
					<td valign="top" class="linedrowrightcolumn">&nbsp;</td>
					<td valign="top" class="linedrowrightcolumn">&nbsp;</td>
					<td valign="top" class="linedrow">&nbsp;</td>
				</tr>
			</table>
			</cfoutput>
		</td>
	</tr>
</table>
</div>

<br>
