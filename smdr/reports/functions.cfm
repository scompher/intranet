
<cffunction name="validateForm" access="private" returntype="string">
	<cfargument type="struct" name="form" required="yes">
	<cfset err = "">
	
	<cfif trim(form.searchStartDate) is not "">
		<cfif not isDate(form.searchStartDate)><cfset err = listappend(err,"The starting date must be a valid date.")></cfif>
	</cfif>
	<cfif trim(form.searchEndDate) is not "">
		<cfif not isDate(form.searchEndDate)><cfset err = listappend(err,"The ending date must be a valid date.")></cfif>
	</cfif>
	<cfif (trim(form.searchStartTimeHH) is not "") or (trim(form.searchStartTimeMM) is not "")>
		<cfif trim(form.searchStartDate) is "">
			<cfset form.searchStartDate = dateformat(now(),'mm/dd/yyyy')>
		</cfif>
		<cfset dtVal = "#form.searchStartDate# #form.searchStartTimeHH#:#form.searchStartTimeMM#">
		<cfif not isDate(dtVal)>
			<cfset err = listappend(err,"The starting date or time value is invalid.")>
		</cfif>
	</cfif>
	<cfif (trim(form.searchEndTimeHH) is not "") or (trim(form.searchEndTimeMM) is not "")>
		<cfif trim(form.searchEndDate) is "">
			<cfset form.searchEndDate = dateformat(now(),'mm/dd/yyyy')>
		</cfif>
		<cfset dtVal = "#form.searchEndDate# #form.searchEndTimeHH#:#form.searchEndTimeMM#">
		<cfif not isDate(dtVal)>
			<cfset err = listappend(err,"The ending date or time value is invalid.")>
		</cfif>
	</cfif>
	
	<cfif trim(form.intervalAmount) is "">
		<cfset err = listappend(err,"You must choose an interval amount.")>
	</cfif>
	
	<cfreturn err>
</cffunction>

<cffunction name="generateReport" access="public" returntype="void" output="true">
	<cfargument type="struct" name="form" required="yes">
	
	<cfset startDateTime = "">
	<cfif trim(form.searchStartDate) is not "">
		<cfset startDateTime = "#form.searchStartDate#">
	</cfif>
	<cfif trim(form.searchStartTimeHH) is not "" and trim(form.searchStartTimeMM) is not "">
		<cfset startDateTime = startDateTime & " #form.searchStartTimeHH#:#form.searchStartTimeMM#">
	</cfif>
	
	<cfset endDateTime = "">
	<cfif trim(form.searchEndDate) is not "">
		<cfset endDateTime = "#form.searchEndDate#">
	</cfif>
	<cfif trim(form.searchEndTimeHH) is not "" and trim(form.searchEndTimeMM) is not "">
		<cfset endDateTime = endDateTime & " #form.searchEndTimeHH#:#form.searchEndTimeMM#">
	</cfif>
	
	<cfif form.callType is "all" or form.callType is "incoming">
		<cfif form.intervalAmount is "15">
			<cfquery name="getIncomingCallData" datasource="smdr">
				SELECT 
				  CAST(CONVERT(nvarchar(12),dateTimeOccurred,112) + ' ' + CONVERT(nvarchar(3),dateTimeOccurred,114) + cast(datepart(minute,dateTimeOccurred)/15*15 as nvarchar(6))+':00' as datetime) as dateTimeOccurred,
				  COUNT(itemid) as callCount 
				FROM smdrData with (nolock) 
				where accessCodeDialed <> '9' 
				<cfif trim(startDateTime) is not "">
					and dateTimeOccurred > #createodbcdatetime(startDateTime)# 
				</cfif>
				<cfif trim(endDateTime) is not "">
					and dateTimeOccurred < #createodbcdatetime(endDateTime)# 
				</cfif>
				<cfif trim(form.department) is not "">
					and dialedNumber IN (select extension from department_extension_lookup where departmentid = #department#) and len(dialedNumber) = 4 
				</cfif>
				<cfif trim(form.incomingTAC) is not "">
					and incomingTAC = '#trim(form.incomingTAC)#' 
				</cfif>
				GROUP BY CAST(CONVERT(nvarchar(12),dateTimeOccurred,112) + ' ' + CONVERT(nvarchar(3),dateTimeOccurred,114) + cast(datepart(minute,dateTimeOccurred)/15*15 as nvarchar(6))+':00' as datetime)
				ORDER BY CAST(CONVERT(nvarchar(12),dateTimeOccurred,112) + ' ' + CONVERT(nvarchar(3),dateTimeOccurred,114) + cast(datepart(minute,dateTimeOccurred)/15*15 as nvarchar(6))+':00' as datetime)
			</cfquery>
		<cfelseif form.intervalAmount is "hourly">
			<cfquery name="getIncomingCallData" datasource="smdr">
				select 
					convert(datetime, convert(varchar(13), dateTimeOccurred, 120) +':00:00', 120) AS dateTimeOccurred, 
					count(itemid) as callCount
				from smdrData with (nolock) 
				where accessCodeDialed <> '9' 
				<cfif trim(startDateTime) is not "">
					and dateTimeOccurred >= #createodbcdatetime(startDateTime)# 
				</cfif>
				<cfif trim(endDateTime) is not "">
					and dateTimeOccurred <= #createodbcdatetime(endDateTime)# 
				</cfif>
				<cfif trim(form.department) is not "">
					and dialedNumber IN (select extension from department_extension_lookup where departmentid = #department#) and len(dialedNumber) = 4 
				</cfif>
				<cfif trim(form.incomingTAC) is not "">
					and incomingTAC = '#trim(form.incomingTAC)#' 
				</cfif>
				group by 
					convert(datetime, convert(varchar(13), dateTimeOccurred, 120) +':00:00', 120) 
				order by 
					dateTimeOccurred
			</cfquery>
		<cfelseif form.intervalAmount is "daily">
			<cfquery name="getIncomingCallData" datasource="smdr">
				select 
					convert(nvarchar(10), dateTimeOccurred, 101) AS dateTimeOccurred, 
					count(itemid) as callCount
				from smdrData with (nolock) 
				where accessCodeDialed <> '9' 
				<cfif trim(startDateTime) is not "">
					and dateTimeOccurred > #createodbcdatetime(startDateTime)# 
				</cfif>
				<cfif trim(endDateTime) is not "">
					and dateTimeOccurred < #createodbcdatetime(endDateTime)# 
				</cfif>
				<cfif trim(form.department) is not "">
					and dialedNumber IN (select extension from department_extension_lookup where departmentid = #department#) and len(dialedNumber) = 4 
				</cfif>
				<cfif trim(form.incomingTAC) is not "">
					and incomingTAC = '#trim(form.incomingTAC)#' 
				</cfif>
				group by 
					convert(nvarchar(10), dateTimeOccurred, 101) 
				order by 
					dateTimeOccurred
			</cfquery>
		</cfif>
	</cfif>
	
	<cfif form.callType is "all" or form.callType is "outgoing">
		<cfif form.intervalAmount is "15">
			<cfquery name="getOutgoingCallData" datasource="smdr">
				SELECT 
				  CAST(CONVERT(nvarchar(12),dateTimeOccurred,112) + ' ' + CONVERT(nvarchar(3),dateTimeOccurred,114) + cast(datepart(minute,dateTimeOccurred)/15*15 as nvarchar(6))+':00' as datetime) as dateTimeOccurred,
				  COUNT(itemid) as callCount 
				FROM smdrData with (nolock) 
				where accessCodeDialed = '9' 
				<cfif trim(startDateTime) is not "">
					and dateTimeOccurred > #createodbcdatetime(startDateTime)# 
				</cfif>
				<cfif trim(endDateTime) is not "">
					and dateTimeOccurred < #createodbcdatetime(endDateTime)# 
				</cfif>
				<cfif trim(form.department) is not "">
					and callingNumber IN (select extension from department_extension_lookup where departmentid = #department#) and len(callingNumber) = 4 
				</cfif>
				<cfif trim(form.incomingTAC) is not "">
					and incomingTAC = '#trim(form.incomingTAC)#' 
				</cfif>
				GROUP BY CAST(CONVERT(nvarchar(12),dateTimeOccurred,112) + ' ' + CONVERT(nvarchar(3),dateTimeOccurred,114) + cast(datepart(minute,dateTimeOccurred)/15*15 as nvarchar(6))+':00' as datetime)
				ORDER BY CAST(CONVERT(nvarchar(12),dateTimeOccurred,112) + ' ' + CONVERT(nvarchar(3),dateTimeOccurred,114) + cast(datepart(minute,dateTimeOccurred)/15*15 as nvarchar(6))+':00' as datetime)
			</cfquery>
		<cfelseif form.intervalAmount is "hourly">
			<cfquery name="getOutgoingCallData" datasource="smdr">
				select 
					convert(datetime, convert(varchar(13), dateTimeOccurred, 120) +':00:00', 120) AS dateTimeOccurred, 
					count(itemid) as callCount
				from smdrData with (nolock) 
				where accessCodeDialed = '9' 
				<cfif trim(startDateTime) is not "">
					and dateTimeOccurred > #createodbcdatetime(startDateTime)# 
				</cfif>
				<cfif trim(endDateTime) is not "">
					and dateTimeOccurred < #createodbcdatetime(endDateTime)# 
				</cfif>
				<cfif trim(form.department) is not "">
					and callingNumber IN (select extension from department_extension_lookup where departmentid = #department#) and len(callingNumber) = 4 
				</cfif>
				<cfif trim(form.incomingTAC) is not "">
					and incomingTAC = '#trim(form.incomingTAC)#' 
				</cfif>
				group by 
					convert(datetime, convert(varchar(13), dateTimeOccurred, 120) +':00:00', 120) 
				order by 
					dateTimeOccurred
			</cfquery>
		<cfelseif form.intervalAmount is "daily">
			<cfquery name="getOutgoingCallData" datasource="smdr">
				select 
					convert(nvarchar(10), dateTimeOccurred, 101) AS dateTimeOccurred, 
					count(itemid) as callCount
				from smdrData with (nolock) 
				where accessCodeDialed = '9' 
				<cfif trim(startDateTime) is not "">
					and dateTimeOccurred > #createodbcdatetime(startDateTime)# 
				</cfif>
				<cfif trim(endDateTime) is not "">
					and dateTimeOccurred < #createodbcdatetime(endDateTime)# 
				</cfif>
				<cfif trim(form.department) is not "">
					and callingNumber IN (select extension from department_extension_lookup where departmentid = #department#) and len(callingNumber) = 4 
				</cfif>
				<cfif trim(form.incomingTAC) is not "">
					and incomingTAC = '#trim(form.incomingTAC)#' 
				</cfif>
				group by 
					convert(nvarchar(10), dateTimeOccurred, 101) 
				order by 
					dateTimeOccurred 
			</cfquery>
		</cfif>
	</cfif>
	
	<cfif findnocase("blackberry",cgi.HTTP_USER_AGENT) is 0>
		<cfset chartFormat = "#form.chartFormat#">
	<cfelse>
		<cfset chartFormat = "jpg">
	</cfif>
	
	<cfchart showlegend="no" databackgroundcolor="##F7F7F7" title="Number of incoming calls" show3d="no"  xaxistitle="Date/Time"  yaxistitle="Number of Calls" format="#chartFormat#" chartwidth="900" chartheight="500">
		<cfif form.callType is "all" or form.callType is "incoming">
			<cfchartseries type="bar" seriescolor="##92A3B6" serieslabel="Incoming Calls">
				<cfloop query="getIncomingCallData">
					<cfchartdata item="#dateformat(dateTimeOccurred, 'mm/dd')# #timeformat(dateTimeOccurred,'HH:mm')#" value="#callcount#">
				</cfloop>
			</cfchartseries>
		</cfif>
		<cfif form.callType is "all" or form.callType is "outgoing">
			<cfchartseries type="bar" seriescolor="##00CC00" serieslabel="Outgoing Calls">
				<cfloop query="getOutgoingCallData">
					<cfchartdata item="#dateformat(dateTimeOccurred, 'mm/dd')# #timeformat(dateTimeOccurred,'HH:mm')#" value="#callcount#">
				</cfloop>
			</cfchartseries>
		</cfif>
	</cfchart>
	
</cffunction>

