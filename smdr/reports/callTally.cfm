
<cfinclude template="functions.cfm">

<cfparam name="err" default="">
<cfparam name="form.searchStartDate" default="">
<cfparam name="form.searchEndDate" default="">
<cfparam name="form.searchStartTimeHH" default="">
<cfparam name="form.searchStartTimeMM" default="">
<cfparam name="form.searchEndTimeHH" default="">
<cfparam name="form.searchEndTimeMM" default="">
<cfparam name="form.intervalAmount" default="">
<cfparam name="form.callType" default="all">
<cfparam name="form.department" default="all">
<cfparam name="form.chartFormat" default="flash">
<cfparam name="form.incomingTAC" default="">

<link rel="stylesheet" type="text/css" href="/styles.css">

<script language="javascript" src="cal2.js">
/*
Xin's Popup calendar script-  Xin Yang (http://www.yxscripts.com/)
Script featured on/available at http://www.dynamicdrive.com/
This notice must stay intact for use
*/
</script>
<script language="javascript" src="cal_conf2.js"></script>

<cfif isdefined("form.btnGenerateReport")>
	<cfset err = validateForm(form)>
</cfif>

<cfquery name="getDepts" datasource="smdr">
	select * from departments 
	order by departmentName asc 
</cfquery>
<div align="center">
<table border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>SMDR Reporting</b> </td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<cfif trim(err) is not "">
				<tr>
					<td colspan="2" style="padding-left:0px">
					<cfloop list="#err#" index="e">
					<cfoutput>
					<span style="padding:5px" class="alert"><b>#e#</b></span><br />
					</cfoutput>
					</cfloop>
					</td>
				</tr>
				</cfif>
				<form method="post" action="callTally.cfm" name="reportForm">
				<tr>
					<td colspan="2"><b>Date/Time Range:</b></td>
				</tr>
				<tr>
					<td colspan="2" class="nopadding">
						<table border="0" cellspacing="0" cellpadding="5">
							<tr>
								<td> Starting Date:
									<input type="text" name="searchStartDate" style="vertical-align:middle; width:75px" value="<cfoutput>#searchStartDate#</cfoutput>" />
									<a style="text-decoration:none;" href="javascript:showCal('startingDate');"><img border="0" src="/images/calicon.gif" width="20" height="20" align="absmiddle" /></a> </td>
								<td>&nbsp;&nbsp;</td>
								<td> Ending Date:
									<input type="text" name="searchEndDate" style="vertical-align:middle; width:75px" value="<cfoutput>#searchEndDate#</cfoutput>" />
									<a style="text-decoration:none;" href="javascript:showCal('endingDate');"><img border="0" src="/images/calicon.gif" width="20" height="20" align="absmiddle" /></a> </td>
							</tr>
							<tr>
								<td> Starting Time:
									<select name="searchStartTimeHH" style="vertical-align:middle;">
										<option value=""></option>
										<cfloop from="0" to="23" index="HH">
											<cfoutput>
												<option <cfif searchStartTimeHH is numberformat(HH,00)>selected</cfif> value="#numberformat(HH,00)#">#numberformat(HH,00)#</option>
											</cfoutput>
										</cfloop>
									</select>
									:
									<select name="searchStartTimeMM" style="vertical-align:middle;">
										<option value=""></option>
										<cfloop from="0" to="59" index="MM">
											<cfoutput>
												<option <cfif searchStartTimeMM is numberformat(MM,00)>selected</cfif> value="#numberformat(MM,00)#">#numberformat(MM,00)#</option>
											</cfoutput>
										</cfloop>
									</select>
								</td>
								<td>&nbsp;</td>
								<td> Ending Time:
									<select name="searchEndTimeHH" style="vertical-align:middle;">
										<option value=""></option>
										<cfloop from="0" to="23" index="HH">
											<cfoutput>
												<option <cfif searchEndTimeHH is numberformat(HH,00)>selected</cfif> value="#numberformat(HH,00)#">#numberformat(HH,00)#</option>
											</cfoutput>
										</cfloop>
									</select>
									:
									<select name="searchEndTimeMM" style="vertical-align:middle;">
										<option value=""></option>
										<cfloop from="0" to="59" index="MM">
											<cfoutput>
												<option <cfif searchEndTimeMM is numberformat(MM,00)>selected</cfif> value="#numberformat(MM,00)#">#numberformat(MM,00)#</option>
											</cfoutput>
										</cfloop>
									</select>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><b>Select Interval:</b> <span class="smallred">*</span></td>
					<td><b>Select Call Type: </b></td>
				</tr>
				<tr>
					<td>
					<select name="intervalAmount">
						<option value=""></option>
						<option <cfif form.intervalAmount is "15">selected</cfif> value="15">15 minutes</option>
						<option <cfif form.intervalAmount is "hourly">selected</cfif> value="hourly">Hourly</option>
						<option <cfif form.intervalAmount is "daily">selected</cfif> value="daily">Daily</option>
					</select>
					</td>
					<td>
						<select name="callType">
							<option <cfif form.callType is "all">selected</cfif> value="all">All Calls</option>
							<option <cfif form.callType is "incoming">selected</cfif> value="incoming">Incoming Calls</option>
							<option <cfif form.callType is "outgoing">selected</cfif> value="outgoing">Outgoing Calls</option>
						</select>
					</td>
				</tr>
				<tr>
					<td colspan="2"><b>Select Department:</b> </td>
				</tr>
				<tr>
					<td colspan="2">
						<select name="department">
							<option value="">All Departments</option>
							<cfoutput query="getDepts">
								<option <cfif form.department is getDepts.departmentid>selected</cfif> value="#departmentid#">#departmentName#</option>
							</cfoutput>
						</select>
					</td>
				</tr>
				<tr>
				    <td colspan="2"><b>Incoming TAC:</b></td>
				    </tr>
				<tr>
				    <td colspan="2">
				        <input type="text" name="incomingTAC" maxlength="50" value="<cfoutput>#incomingTAC#</cfoutput>" />
				    </td>
				    </tr>
				<tr>
					<td colspan="2">
					Choose format for graph: &nbsp;
					<select name="chartFormat" style="vertical-align:middle">
						<option <cfif form.chartFormat is "flash">selected</cfif> value="flash">Flash</option>
						<option <cfif form.chartFormat is "jpg">selected</cfif> value="jpg">Image</option>
					</select>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<input name="btnGenerateReport" type="submit" class="sidebar" value="Generate Report" />
						<input onclick="document.location='callTally.cfm';" type="button" class="sidebar" value="Clear Form" />
					</td>
				</tr>
				</form>
			</table>
		</td>
	</tr>
</table>
<br />
<a href="/index.cfm" style="text-decoration:underline;" class="normal">Return to Intranet</a>
</div>

<cfif isdefined("form.btnGenerateReport") and trim(err) is "">
<br />
<div align="center">
<cfoutput>#generateReport(form)#</cfoutput>
</div>
</cfif>