
<style type="text/css">
	.box {font-family:"Courier New", Courier, monospace; font-size:12px; color:#000000; border:#CCCCCC 1px solid; border-right:none; border-bottom:none;} 
	.normal {font-family:"Courier New", Courier, monospace; font-size:12px; color:#000000;}
	.linedRow {border-bottom:#CCCCCC 1px solid; border-right:#CCCCCC 1px solid;}
</style>

<cfset goal = 00025>
<cfset startDateTime = createodbcdatetime(sd)>
<cfif sd is ed>
	<cfset endDateTime = createodbcdatetime(ed)>
	<cfset endDateTime = dateadd("d",1,endDateTime)>
<cfelse>
	<cfset endDateTime = createodbcdatetime(ed)>
</cfif>

<cfset keepLooping = true>
<cfset currDateTime = startDateTime>

<!--- get total data --->
<cfquery name="getAllCalls" datasource="smdr">
	select * 
	from dbo.smdrData
	where (dateTimeOccurred >= #startDateTime# and dateTimeOccurred < #endDateTime#) 
	and dnis = 6857 
</cfquery>

<div align="center">
<cfoutput>
<br />
<b style="font-size:14px;">Xfinity Phone Report</b>
<br />
<br />
<b style="font-size:14px;">#dateformat(startDateTime,'mm/dd/yyyy')# #timeformat(startDateTime,'HH:mm')# - #dateformat(endDateTime,'mm/dd/yyyy')# #timeformat(endDateTime,'HH:mm')#</b>
</cfoutput>
<br />
<br />
<table border="0" cellspacing="0" cellpadding="5" class="box">
	<tr>
		<td class="linedRow">&nbsp;</td>
		<td align="center" class="linedRow">Total Calls </td>
		<td align="center" class="linedRow">Total within Goal </td>
		<td align="center" class="linedRow">% within Goal </td>
		<td align="center" class="linedRow">Abandon Calls </td>
		<td align="center" class="linedRow">Total Calls Abandon w/in Goal </td>
		<td align="center" class="linedRow">% within Goal </td>
	</tr>
	<cfoutput>
	<cfloop condition="#keepLooping#">
		<cfset currDateTime = dateadd("h",1,currDateTime)>
		<cfif ("#dateformat(currDateTime,'mm/dd/yyyy')# #timeformat(currDateTime,'HH:mm')#" is endDateTime)>
			<cfset keepLooping = false>
		</cfif>

		<cfquery name="getPeriodCalls" dbtype="query">
			select *  
			from getAllCalls 
			where (dateTimeOccurred >= #createodbcdatetime(dateadd("h",-1,currDateTime))# and dateTimeOccurred < #createodbcdatetime(currDateTime)#) 
		</cfquery>

		<cfset periodTotalCalls  = 0>
		<cfset periodTotalWithinGoal = 0>
		<cfset periodTotalWithinGoalPercentage = 0>
		<cfset periodAbandonCalls = 0>
		<cfset periodTotalCallsAbandonWithinGoal = 0>
		<cfset periodTotalCallsAbandonWithinGoalPercentage = 0>
	
		<cfloop query="getPeriodCalls">
			<cfif conditionCode is not "G" and conditionCode is not "H" and conditionCode is not "I">
				<cfset periodTotalCalls = periodTotalCalls + 1>
			</cfif>
			<cfset totalDuration = 0>
			<cfset durationHrs = mid(duration,1,1)>
			<cfset durationMins = mid(duration,2,2)>
			<cfset durationSec = mid(duration,4,2)>
			<cfset totalDuration = totalDuration + durationSec + (durationMins * 60) + (durationHrs * 3600)>
			<cfswitch expression="#conditionCode#">
				<!--- ring times but answered --->
				<cfcase value="G">
					<cfif totalDuration lte goal>
						<cfset periodTotalWithinGoal = periodTotalWithinGoal + 1>
					</cfif>
				</cfcase>
				<!--- abandoned --->
				<cfcase value="H">
					<cfset periodAbandonCalls = periodAbandonCalls + 1>
					<cfif totalDuration lte goal>
						<cfset periodTotalCallsAbandonWithinGoal = periodTotalCallsAbandonWithinGoal + 1>
					</cfif>
				</cfcase>
			</cfswitch>
		</cfloop>
		
		<cfif periodTotalCalls is not 0>
			<cfset periodTotalWithinGoalPercentage = (periodTotalWithinGoal/periodTotalCalls) * 100>
		<cfelse>
			<cfset periodTotalWithinGoalPercentage = 0>
		</cfif>
		
		<cfif periodAbandonCalls is not 0>
			<cfset periodTotalCallsAbandonWithinGoalPercentage = (periodTotalCallsAbandonWithinGoal/periodAbandonCalls) * 100>
		<cfelse>
			<cfset periodTotalCallsAbandonWithinGoalPercentage = 0>
		</cfif>

		<tr>
			<td class="linedRow">
			#dateformat(dateadd("h",-1,currDateTime),'mm/dd')# #timeformat(dateadd("h",-1,currDateTime),'HH:mm')# - #dateformat(currDateTime,'mm/dd')# #timeformat(currDateTime,'HH:mm')#
			</td>
			<td align="center" class="linedRow">#numberformat(periodTotalCalls)#</td>
			<td align="center" class="linedRow">#numberformat(periodTotalWithinGoal)#</td>
			<td align="center" class="linedRow">#numberformat(periodTotalWithinGoalPercentage,'0.00')#%</td>
			<td align="center" class="linedRow">#numberformat(periodAbandonCalls)#</td>
			<td align="center" class="linedRow">#numberformat(periodTotalCallsAbandonWithinGoal)#</td>
			<td align="center" class="linedRow">#numberformat(periodTotalCallsAbandonWithinGoalPercentage,'0.00')#%</td>
		</tr>
	</cfloop>
	</cfoutput>
</table>
<br />
<a class="normal" style="text-decoration:underline;" href="index.cfm">Return to Date Selection</a>
</div>

