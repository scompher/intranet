
<cfif isDefined("form.btnViewReport")>
	<cfinclude template="xfinityReport.cfm">
	<cfabort>
<cfelseif isDefined("form.btnDownloadReport")>
	<cfinclude template="printReport.cfm">
	<cfabort>
</cfif>

<link rel="stylesheet" type="text/css" href="/styles.css">

<br>

<div align="center">
<cfform method="post" action="index.cfm" name="mainform">
<table border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Xfinity Phone Report</b> </td>
	</tr>
	<tr>
		<td class="greyrow"><b>Select Reporting Date Range:</b></td>
	</tr>
	<tr>
		<td class="greyrow" style="padding:0px">
			<cfoutput>
			<table border="0" cellspacing="0" cellpadding="5" class="grey">
				<tr>
					<td>Starting:</td>
					<td class="grey">
						<cfinput name="sd" type="datefield" id="sd" style="width:75px" value="#dateformat(now(),'mm/dd/yyyy')#" maxlength="10" />
					</td>
					<td width="20" class="grey">&nbsp;</td>
					<td>Ending:</td>
					<td class="grey">
						<cfinput name="ed" type="datefield" id="ed" style="width:75px" value="#dateformat(now(),'mm/dd/yyyy')#" maxlength="10" />
					</td>
				</tr>
			</table>
			</cfoutput>
		</td>
	</tr>
	<tr>
		<td class="greyrowbottom">
			<cfinput name="btnViewReport" type="submit" class="sidebar" value="View Report">
			<cfinput name="btnDownloadReport" type="submit" class="sidebar" value="Download Report">
		</td>
	</tr>
</table>
</cfform>
<br />
<p class="normal"><a style="text-decoration:underline" href="/index.cfm">Return to Intranet</a> </p>
</div>
