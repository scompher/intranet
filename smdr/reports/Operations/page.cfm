
<!--- 
<cfset startDateTime = "#dateadd('d',-1,now())#">
<cfset startDateTime = dateformat(startDateTime,'mm/dd/yyyy') & " 23:00">
<cfset endDateTime = dateformat(now(),'mm/dd/yyyy') & " 07:00">
--->

<cfquery name="getCalls" datasource="smdr">
	select *  
	from smdrData with (nolock) 
	where (dateTimeOccurred >= #createodbcdatetime(startDateTime)# and dateTimeOccurred < #createodbcdatetime(endDateTime)#) 
	and dnis IN (select distinct opsDNIS from ops_dnis_lookup) 
</cfquery>

<cfset index = 0>
<cfset keepLooping = true>
<cfset currDateTime = startDateTime>

<style type="text/css">
	TABLE {font-family:"Courier New", Courier, monospace; font-size:12px; color:#000000; border:#CCCCCC 1px solid; border-right:none; border-bottom:none;} 
	.linedRow {border-bottom:#CCCCCC 1px solid; border-right:#CCCCCC 1px solid;}
</style>

<div align="center">
<cfoutput>
<br />
<b style="font-size:14px;">#dateformat(startDateTime,'mm/dd/yyyy')# #timeformat(startDateTime,'HH:mm')# - #dateformat(endDateTime,'mm/dd/yyyy')# #timeformat(endDateTime,'HH:mm')#</b>
</cfoutput>
<br />
<br />
<table border="0" cellspacing="0" cellpadding="2">
<tr>
	<td align="center" class="linedRow">&nbsp;</td>
	<td align="center" class="linedRow">&nbsp;</td>
	<td colspan="5" align="center" class="linedRow">Answered</td>
	<td colspan="3" align="center" class="linedRow">Abandoned</td>
	</tr>
<tr>
	<td align="center" class="linedRow">Date/Time</td>
	<td align="center" class="linedRow"># of Calls </td>
	<td align="center" class="linedRow">Avg Answer Speed </td>
	<td align="center" class="linedRow">15 sec or less </td>
	<td align="center" class="linedRow">16-30 sec </td>
	<td align="center" class="linedRow">31-60 sec </td>
	<td align="center" class="linedRow">&gt; 60 sec </td>
	<td align="center" class="linedRow"> # Abandoned </td>
	<td align="center" class="linedRow">Avg Abandoned Time </td>
	<td align="center" class="linedRow">Abandoned &gt; 30 sec </td>
</tr>

<cfoutput>
<cfloop condition="#keepLooping#">
	<cfset currDateTime = dateadd("n",15,currDateTime)>
	<cfif ("#dateformat(currDateTime,'mm/dd/yyyy')# #timeformat(currDateTime,'HH:mm')#" is endDateTime)>
		<cfset keepLooping = false>
	</cfif>

	<cfquery name="getAlarms" dbtype="query">
		select *  
		from getCalls 
		where (dateTimeOccurred >= #createodbcdatetime(dateadd("n",-15,currDateTime))# and dateTimeOccurred < #createodbcdatetime(currDateTime)#) 
	</cfquery>
	
	<!--- 
	tier1 : 15 seconds or less
	tier2 : 16-30 seconds
	tier3 : 31-60 seconds
	--->

	<cfset numOfCalls = 0>
	<cfset totalAnswerSpeed = 0>
	<cfset totalAnswered = 0>
	<cfset avgAnswerSpeed = 0>
	<cfset tier1 = 0>
	<cfset tier2 = 0>
	<cfset tier3 = 0>
	<cfset tier4 = 0>
	<cfset abandoned = 0>
	<cfset avgAbandonedTime = 0>
	<cfset totalAbandonedTime = 0>
	<cfset abandonedGT30 = 0>
	<cfset totalCalls = 0>
	
	<cfloop query="getAlarms">
		<cfif conditionCode is not "G" and conditionCode is not "H" and conditionCode is not "I">
			<cfset totalCalls = totalCalls + 1>
		</cfif>
		<cfset totalDuration = 0>
		<cfset durationHrs = mid(duration,1,1)>
		<cfset durationMins = mid(duration,2,2)>
		<cfset durationSec = mid(duration,4,2)>
		<cfset totalDuration = totalDuration + durationSec + (durationMins * 60) + (durationHrs * 3600)>
		<cfif conditionCode is "G"> <!--- answered tallies --->
			<cfset totalAnswered = totalAnswered + 1>
			<cfset totalAnswerSpeed = totalAnswerSpeed + totalDuration>
			<cfif totalDuration lte 15>
				<cfset tier1 = tier1 + 1>
			<cfelseif totalDuration gt 15 and totalDuration lte 30>
				<cfset tier2 = tier2 + 1>
			<cfelseif totalDuration gt 30 and totalDuration lte 60>
				<cfset tier3 = tier3 + 1>
			<cfelse>
				<cfset tier4 = tier4 + 1>
			</cfif>
		<cfelseif conditionCode is "H"> <!--- abandoned tallies --->
			<cfset abandoned = abandoned + 1>
			<cfset totalAbandonedTime = totalAbandonedTime + totalDuration>
			<cfif totalDuration gt 30>
				<cfset abandonedGT30 = abandonedGT30 + 1>
			</cfif>
		</cfif>
	</cfloop>
	
	<cfif totalAnswered is not 0>
		<cfset avgAnswerSpeed = totalAnswerSpeed / totalAnswered> 
	<cfelse>
		<cfset avgAnswerSpeed = 0> 
	</cfif>
	<cfif abandoned is not 0>
		<cfset avgAbandonedTime = totalAbandonedTime / abandoned>
	<cfelse>
		<cfset avgAbandonedTime = 0>
	</cfif>
	<cfset bd = dateformat(dateadd("n",-15,currDateTime),'mm/dd')>
	<cfset bt = timeformat(dateadd("n",-15,currDateTime),'HH:mm')>
	<cfset ed = dateformat(currDateTime,'mm/dd')>
	<cfset et = timeformat(currDateTime,'HH:mm')>
	<tr>
		<td align="center" class="linedRow">
		#bd# #bt# - <cfif bd is not ed>#ed#</cfif> #et#
		</td>
		<td align="center" class="linedRow">#numberformat(totalCalls)#</td>
		<td align="center" class="linedRow">#numberformat(avgAnswerSpeed)# seconds</td>
		<td align="center" class="linedRow">#numberformat(tier1)#</td>
		<td align="center" class="linedRow">#numberformat(tier2)#</td>
		<td align="center" class="linedRow">#numberformat(tier3)#</td>
		<td align="center" class="linedRow">#numberformat(tier4)#</td>
		<td align="center" class="linedRow">#numberformat(abandoned)#</td>
		<td align="center" class="linedRow">#numberformat(avgAbandonedTime)# seconds</td>
		<td align="center" class="linedRow">#numberformat(abandonedGT30)#</td>
	</tr>
</cfloop>
</cfoutput>
</table>
</div>






























	<!--- 
<style type="text/css">
	TABLE {font-family:"Courier New", Courier, monospace; font-size:12px; color:#000000; border:#CCCCCC 1px solid; border-right:none; border-bottom:none;} 
	.linedRow {border-bottom:#CCCCCC 1px solid; border-right:#CCCCCC 1px solid;}
</style>

<div align="center">
<table border="0" cellspacing="0" cellpadding="2">
	<tr>
		<td align="center" class="linedRow">Date/Time</td>
		<td align="center" class="linedRow"># of Calls </td>
		<td align="center" class="linedRow">Avg Answer Speed </td>
		<td align="center" class="linedRow">15 sec or less </td>
		<td align="center" class="linedRow">16-30 sec </td>
		<td align="center" class="linedRow">31-60 sec </td>
		<td align="center" class="linedRow">&gt; 60 sec </td>
		<td align="center" class="linedRow"> Abandoned </td>
		<td align="center" class="linedRow">Avg Abandoned Time </td>
		<td align="center" class="linedRow">Abandoned &gt; 30 sec </td>
	</tr>

	<cfloop from="-1" to="6" index="x">
		<cfif x is -1>
			<cfset currHour = 23>
		<cfelse>
			<cfset currHour = x>
		</cfif>
		<cfloop from="0" to="45" step="15" index="min">
		<cfoutput>
		<tr>
			<td nowrap class="linedRow">#numberformat(currHour,'00')#:#numberformat(min,'00')#</td>
			<td align="right" class="linedRow">1,942</td>
			<td align="right" class="linedRow">12.5 seconds </td>
			<td align="right" class="linedRow">23</td>
			<td align="right" class="linedRow">454</td>
			<td align="right" class="linedRow">654</td>
			<td align="right" class="linedRow">1</td>
			<td align="right" class="linedRow">132</td>
			<td align="right" class="linedRow">12.5 seconds </td>
			<td align="right" class="linedRow">22</td>
		</tr>
		</cfoutput>
		</cfloop>
	</cfloop>
	<tr>
		<td nowrap class="linedRow">08/05 23:00-23:15 </td>
		<td align="right" class="linedRow">1,942</td>
		<td align="right" class="linedRow">12.5 seconds </td>
		<td align="right" class="linedRow">23</td>
		<td align="right" class="linedRow">454</td>
		<td align="right" class="linedRow">654</td>
		<td align="right" class="linedRow">1</td>
		<td align="right" class="linedRow">132</td>
		<td align="right" class="linedRow">12.5 seconds </td>
		<td align="right" class="linedRow">22</td>
	</tr>
	<tr>
		<td nowrap class="linedRow">08/05 23:15-23:30 </td>
		<td align="right" class="linedRow">&nbsp;</td>
		<td align="right" class="linedRow">&nbsp;</td>
		<td align="right" class="linedRow">&nbsp;</td>
		<td align="right" class="linedRow">&nbsp;</td>
		<td align="right" class="linedRow">&nbsp;</td>
		<td align="right" class="linedRow">&nbsp;</td>
		<td align="right" class="linedRow">&nbsp;</td>
		<td align="right" class="linedRow">&nbsp;</td>
		<td align="right" class="linedRow">&nbsp;</td>
	</tr>
	<tr>
		<td nowrap class="linedRow">08/05 23:30-23:45</td>
		<td align="right" class="linedRow">&nbsp;</td>
		<td align="right" class="linedRow">&nbsp;</td>
		<td align="right" class="linedRow">&nbsp;</td>
		<td align="right" class="linedRow">&nbsp;</td>
		<td align="right" class="linedRow">&nbsp;</td>
		<td align="right" class="linedRow">&nbsp;</td>
		<td align="right" class="linedRow">&nbsp;</td>
		<td align="right" class="linedRow">&nbsp;</td>
		<td align="right" class="linedRow">&nbsp;</td>
	</tr>
	<tr>
		<td nowrap class="linedRow">08/05 23:45-00:00</td>
		<td align="right" class="linedRow">&nbsp;</td>
		<td align="right" class="linedRow">&nbsp;</td>
		<td align="right" class="linedRow">&nbsp;</td>
		<td align="right" class="linedRow">&nbsp;</td>
		<td align="right" class="linedRow">&nbsp;</td>
		<td align="right" class="linedRow">&nbsp;</td>
		<td align="right" class="linedRow">&nbsp;</td>
		<td align="right" class="linedRow">&nbsp;</td>
		<td align="right" class="linedRow">&nbsp;</td>
	</tr>
	<tr>
		<td nowrap class="linedRow">08/06 00:00-00:15</td>
		<td align="right" class="linedRow">&nbsp;</td>
		<td align="right" class="linedRow">&nbsp;</td>
		<td align="right" class="linedRow">&nbsp;</td>
		<td align="right" class="linedRow">&nbsp;</td>
		<td align="right" class="linedRow">&nbsp;</td>
		<td align="right" class="linedRow">&nbsp;</td>
		<td align="right" class="linedRow">&nbsp;</td>
		<td align="right" class="linedRow">&nbsp;</td>
		<td align="right" class="linedRow">&nbsp;</td>
	</tr>
	<tr>
		<td nowrap class="linedRow">08/06 00:15-00:30</td>
		<td align="right" class="linedRow">&nbsp;</td>
		<td align="right" class="linedRow">&nbsp;</td>
		<td align="right" class="linedRow">&nbsp;</td>
		<td align="right" class="linedRow">&nbsp;</td>
		<td align="right" class="linedRow">&nbsp;</td>
		<td align="right" class="linedRow">&nbsp;</td>
		<td align="right" class="linedRow">&nbsp;</td>
		<td align="right" class="linedRow">&nbsp;</td>
		<td align="right" class="linedRow">&nbsp;</td>
	</tr>
	<tr>
		<td nowrap class="linedRow">08/06 00:30-00:45</td>
		<td align="right" class="linedRow">&nbsp;</td>
		<td align="right" class="linedRow">&nbsp;</td>
		<td align="right" class="linedRow">&nbsp;</td>
		<td align="right" class="linedRow">&nbsp;</td>
		<td align="right" class="linedRow">&nbsp;</td>
		<td align="right" class="linedRow">&nbsp;</td>
		<td align="right" class="linedRow">&nbsp;</td>
		<td align="right" class="linedRow">&nbsp;</td>
		<td align="right" class="linedRow">&nbsp;</td>
	</tr>
	<tr>
		<td nowrap class="linedRow">08/06 00:45-01:00</td>
		<td align="right" class="linedRow">&nbsp;</td>
		<td align="right" class="linedRow">&nbsp;</td>
		<td align="right" class="linedRow">&nbsp;</td>
		<td align="right" class="linedRow">&nbsp;</td>
		<td align="right" class="linedRow">&nbsp;</td>
		<td align="right" class="linedRow">&nbsp;</td>
		<td align="right" class="linedRow">&nbsp;</td>
		<td align="right" class="linedRow">&nbsp;</td>
		<td align="right" class="linedRow">&nbsp;</td>
	</tr>
	<tr>
		<td nowrap class="linedRow">08/06 01:00-01:15</td>
		<td align="right" class="linedRow">&nbsp;</td>
		<td align="right" class="linedRow">&nbsp;</td>
		<td align="right" class="linedRow">&nbsp;</td>
		<td align="right" class="linedRow">&nbsp;</td>
		<td align="right" class="linedRow">&nbsp;</td>
		<td align="right" class="linedRow">&nbsp;</td>
		<td align="right" class="linedRow">&nbsp;</td>
		<td align="right" class="linedRow">&nbsp;</td>
		<td align="right" class="linedRow">&nbsp;</td>
	</tr>

</table>
</div>
	 --->