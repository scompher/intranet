<!--- 
7/1/2011 : PG : Changed duration lookup to work with 5 digit durations (we switched a year+ ago from 4) 
HMMSS
Also changed TAC search to allow multiple tac's separated by a comma 
--->

<!--- function : formatPhone --->
<cffunction name="formatPhone" returntype="string">
	<cfargument name="inPhone" required="yes">
	<cfset formattedPhone = inPhone>
	<cfif len(formattedPhone) is 11>
		<cfset formattedPhone = mid(formattedPhone,1,1) & "-" & mid(formattedPhone,2,3) & "-" & mid(formattedPhone,5,3) & "-" & mid(formattedPhone,8,4)>
	<cfelseif len(formattedPhone) is 10>
		<cfset formattedPhone = mid(formattedPhone,1,3) & "-" & mid(formattedPhone,4,3) & "-" & mid(formattedPhone,7,4)>	
	</cfif>
	<cfreturn formattedPhone>
</cffunction>
<!--- function : formatPhone --->

<!--- function : searchSMDR --->
<cffunction name="searchSMDR" access="private" output="yes" returntype="query">
	<cfargument name="form" type="struct" required="yes">
	<cfargument name="start" type="numeric" required="yes">
	<cfargument name="maxrows" type="numeric" required="yes">
	<cfargument name="excelDownload" type="boolean" required="no">
	<cfargument name="excelDownloadDetail" type="boolean" required="no">

	<cfif not isDefined("excelDownload")>
		<cfset excelDownload = false>
	</cfif>
	<cfif not isDefined("excelDownloadDetail")>
		<cfset excelDownloadDetail = false>
	</cfif>
	
	<cfif isDefined("cookie.adminlogin")>
		<cfquery name="getsec" datasource="intranet">
			select admin_users.*, admin_users_departments_lookup.*
			from admin_users 
			inner join admin_users_departments_lookup on admin_users.adminuserid = admin_users_departments_lookup.adminuserid
			where admin_users.adminuserid = #cookie.adminlogin#
		</cfquery>
		<cfif getsec.seclevelid is 3>
			<cfset restrictedLookup = true>
		<cfelse>
			<cfset restrictedLookup = false>
		</cfif>
	<cfelse>
		<cfset restrictedLookup = true>
	</cfif>

	<cfset duration = "00000">
	<cfif trim(form.searchDurationHH) is not "" or trim(form.searchDurationMM) is not "" or trim(form.searchDurationSS) is not "">
		<cfif form.searchDurationHH is not ""><cfset durationHH = form.searchDurationHH><cfelse><cfset durationHH = "0"></cfif>
		<cfif form.searchDurationMM is not ""><cfset durationMM = numberformat(form.searchDurationMM,00)><cfelse><cfset durationMM = "00"></cfif>
		<cfif form.searchDurationSS is not ""><cfset durationSS = numberformat(form.searchDurationSS,00)><cfelse><cfset durationSS = "00"></cfif>
		<cfset duration = "#durationHH##durationMM##durationSS#">
	</cfif>
	
	<cfset startDateTime = "">
	<cfif trim(form.searchStartDate) is not "">
		<cfset startDateTime = "#dateformat(form.searchStartDate,'mm/dd/yyyy')#">
	</cfif>
	<cfif trim(form.searchStartTimeHH) is not "" and trim(form.searchStartTimeMM) is not "">
		<cfif trim(form.searchStartDate) is "">
			<cfset startDateTime = "#dateformat(now(),'mm/dd/yyyy')#" & "#form.searchStartTimeHH#:#form.searchStartTimeMM#">
		<cfelse>
			<cfset startDateTime = startDateTime & "#form.searchStartTimeHH#:#form.searchStartTimeMM#">
		</cfif>
	</cfif>
	
	<cfset endDateTime = "">
	<cfif trim(form.searchEndDate) is not "">
		<cfset endDateTime = "#dateformat(form.searchEndDate,'mm/dd/yyyy')#">
	</cfif>
	<cfif trim(form.searchEndTimeHH) is not "" and trim(form.searchEndTimeMM) is not "">
		<cfif trim(form.searchEndDate) is "">
			<cfset endDateTime = "#dateformat(now(),'mm/dd/yyyy')#" & "#form.searchEndTimeHH#:#form.searchEndTimeMM#">
		<cfelse>
			<cfset endDateTime = endDateTime & "#form.searchEndTimeHH#:#form.searchEndTimeMM#">
		</cfif>
	<cfelse>
		<cfif form.searchEndDate is form.searchStartDate and form.searchStartDate is not "">
			<cfset endDateTime = "#form.searchStartDate# 23:59:59">
		</cfif>
	</cfif>
	
	<cfif trim(form.sortBy) is not "" and trim(form.sortHow) is not "">
		<cfset sort = form.sortBy & " " & form.sortHow>
	<cfelse>
		<cfset sort = "itemid desc">
	</cfif>

	<cfif trim(form.searchDialedNumber) is not "">
		<cfset form.searchDialedNumber = replace(form.searchDialedNumber," ","","all")>
		<cfset form.searchDialedNumber = replace(form.searchDialedNumber,"(","","all")>
		<cfset form.searchDialedNumber = replace(form.searchDialedNumber,")","","all")>
		<cfset form.searchDialedNumber = replace(form.searchDialedNumber,"-","","all")>
		<cfif len(form.searchDialedNumber) is 10 and left(form.searchDialedNumber,1) is not "1">
			<cfset form.searchDialedNumber = "1" & form.searchDialedNumber>
		</cfif>
	</cfif>
	
	<cfif trim(form.searchCallingNumber) is not "">
		<cfset form.searchCallingNumber = replace(form.searchCallingNumber," ","","all")>
		<cfset form.searchCallingNumber = replace(form.searchCallingNumber,"(","","all")>
		<cfset form.searchCallingNumber = replace(form.searchCallingNumber,")","","all")>
		<cfset form.searchCallingNumber = replace(form.searchCallingNumber,"-","","all")>
		<cfif len(form.searchCallingNumber) is 11 and left(form.searchCallingNumber,1) is "1">
			<cfset form.searchCallingNumber = right(form.searchCallingNumber,10)>
		</cfif>
	</cfif>
	
	<cfif trim(form.searchDNIS) is not "">
		<cfset dnisList = "">
		<cfloop list="#form.searchDNIS#" index="dnis">
			<cfif trim(dnis) is not "">
				<cfset dnisList = listappend(dnislist, "'#trim(dnis)#'")>
			</cfif>
		</cfloop>
	</cfif>
	
	<cfset TACList = "">
	<cfif trim(form.searchTAC) is not "">
		<cfloop list="#form.searchTAC#" index="TAC">
			<cfset TACList = listappend(TACList,"'#trim(TAC)#'")>
		</cfloop>
	</cfif>
	
	<!--- build where clause --->
	<cfset whereClause = "">
	<cfif trim(form.callType) is not "All">
		<cfif trim(form.callType) is "Incoming"><cfset whereClause = whereClause & "dbo.smdrData.accessCodeDialed <> '9' and "></cfif>
		<cfif trim(form.callType) is "Outgoing"><cfset whereClause = whereClause & "dbo.smdrData.accessCodeDialed = '9' and "></cfif>
	</cfif>
	<cfif trim(form.searchCallingNumber) is not "">
		<cfset whereClause = whereClause & "dbo.smdrData.callingNumber like '#replace(form.searchCallingNumber,"*","%","all")#' and ">
		<cfif restrictedLookup is true>
			<cfset whereClause = whereClause & "dbo.smdrData.callingNumber not in ('1100','1101','1104','1108','1109','1110','1111','1116') and ">
		</cfif>
	</cfif>
	<cfif trim(form.searchDialedNumber) is not "">
		<cfset whereClause = whereClause & "dbo.smdrData.dialedNumber like '#replace(form.searchDialedNumber,"*","%","all")#' and ">
		<cfif restrictedLookup is true>
			<cfset whereClause = whereClause & "dbo.smdrData.dialedNumber not in ('1100','1101','1104','1108','1109','1110','1111','1116') and ">
		</cfif>
	</cfif>
	<cfif trim(form.searchDNIS) is not "">
		<cfset whereClause = whereClause & "dbo.smdrData.dnis IN (#preservesinglequotes(dnisList)#) and ">
	</cfif>
	<cfif trim(TACList) is not ""><br />
		<cfset whereClause = whereClause & "(dbo.smdrData.incomingTAC IN (#preserveSingleQuotes(TACList)#) OR dbo.smdrData.accessCodeUsed IN (#preserveSingleQuotes(TACList)#)) and ">
	</cfif>
	<!--- 
	<cfif trim(form.searchTAC) is not "">
		<cfset whereClause = whereClause & "(dbo.smdrData.incomingTAC LIKE '#replace(form.searchTAC,"*","%","all")#' OR dbo.smdrData.accessCodeUsed like '#replace(form.searchTAC,"*","%","all")#') and ">
	</cfif>
	--->
	<cfif trim(duration) is not "00000">
		<cfif form.searchDuration is "longerThan">
			<cfset whereClause = whereClause & "convert(int,dbo.smdrData.duration) > '#variables.duration#' and ">
		<cfelseif form.searchDuration is "shorterThan">
			<cfset whereClause = whereClause & "convert(int,dbo.smdrData.duration) < '#variables.duration#' and ">
		<cfelseif form.searchDuration is "equalTo">
			<cfset whereClause = whereClause & "convert(int,dbo.smdrData.duration) = '#variables.duration#' and ">
		</cfif>
	</cfif>
	<cfif trim(startDateTime) is not "">
		<cfset whereClause = whereClause & "dbo.smdrData.dateTimeOccurred >= #createodbcdatetime(startDateTime)# and ">
	</cfif>
	<cfif trim(endDateTime) is not "">
		<cfset whereClause = whereClause & "dbo.smdrData.dateTimeOccurred <= #createodbcdatetime(endDateTime)# and ">
	</cfif>
	<cfif trim(department) is not "">
		<cfif form.callType is "All">
			<cfset whereClause = whereClause & "((dbo.smdrData.callingNumber IN (select extension from dbo.department_extension_lookup where departmentid = #department#)) or (dbo.smdrData.dialedNumber IN (select extension from dbo.department_extension_lookup where departmentid = #department#))) and ">
		</cfif>
		<cfif form.callType is "Outgoing">
			<cfset whereClause = whereClause & "(dbo.smdrData.callingNumber IN (select extension from dbo.department_extension_lookup where departmentid = #department#)) and ">
		</cfif>
		<cfif form.callType is "Incoming">
			<cfset whereClause = whereClause & "(dbo.smdrData.dialedNumber IN (select extension from dbo.department_extension_lookup where departmentid = #department#)) and ">
		</cfif>
		<!--- 
		<cfif form.callType is "All">
			<cfset whereClause = whereClause & "((dbo.smdrData.callingNumber IN (select extension from dbo.department_extension_lookup where departmentid = #department#) and len(dbo.smdrData.callingNumber) = 4) or (dbo.smdrData.dialedNumber IN (select extension from dbo.department_extension_lookup where departmentid = #department#) and len(dbo.smdrData.dialedNumber) = 4)) and ">
		</cfif>
		<cfif form.callType is "Outgoing">
			<cfset whereClause = whereClause & "(dbo.smdrData.callingNumber IN (select extension from dbo.department_extension_lookup where departmentid = #department#) and len(dbo.smdrData.callingNumber) = 4) and ">
		</cfif>
		<cfif form.callType is "Incoming">
			<cfset whereClause = whereClause & "(dbo.smdrData.dialedNumber IN (select extension from dbo.department_extension_lookup where departmentid = #department#) and len(dbo.smdrData.dialedNumber) = 4) and ">
		</cfif>
		--->
	</cfif>
	<!--- <cfset whereClause = whereClause & "(conditionCode <> 'G' and conditionCode <> 'H' and conditionCode <> 'I') and "> --->
	<cfset whereClause = whereClause & "(conditionCode <> 'G' and conditionCode <> 'H') and ">
	<cfset whereClause = whereClause & "1=1">
	<!--- build where clause --->
	
	<cfif excelDownloadDetail is true>
		<cfset selectList = "itemid, rawData, dateTimeOccurred, timeOccurred, duration, left(duration,1) + ':' + substring(duration,2,2) + ':' + right(duration,2) as callduration, left(duration,1) * 3600 +  substring(duration,2,2) * 60 + right(duration,2) as durationInSeconds, conditionCode, accessCodeDialed, accessCodeUsed, dialedNumber, callingNumber, dnis, authorizationCode, timeInQueue, FRL, incomingCircuitID, outgoingCircuitID, featureFlag, attendantConsole, incomingTAC, nodeNumber, isdnNsv, ixcCode, bcc, maUui, resourceFlag, packetCount, tscFlag, bandWidth, isdnCC"> 	
	<cfelse>
		<cfset selectList = "dbo.smdrData.itemid, dbo.smdrData.accessCodeDialed, dbo.smdrData.duration, left(duration,1) + ':' + substring(duration,2,2) + ':' + right(duration,2) as callduration, left(duration,1) * 3600 +  substring(duration,2,2) * 60 + right(duration,2) as durationInSeconds, dbo.smdrData.dateTimeOccurred, dbo.smdrData.dialedNumber, dbo.smdrData.callingNumber , dbo.smdrData.dnis">	
	</cfif>

	<cfif excelDownload is false>
		<cfquery name="getItems" datasource="smdr">
			select top #maxrows# * 
			from 
			(
			select 
			ROW_NUMBER() over (order by #sort#) as RowNumber, 
			(select COUNT(*) from dbo.smdrData where #preserveSingleQuotes(whereClause)#) as itemCount, 
			#preserveSingleQuotes(selectList)# 
			from dbo.smdrData with (nolock) 
			where #preserveSingleQuotes(whereClause)#
			) AS myResults 
			where RowNumber >= #start# order by #sort# 
		</cfquery>
	<cfelse>
		<cfquery name="getItems" datasource="smdr">
			select 
			#preserveSingleQuotes(selectList)# 
			from dbo.smdrData with (nolock) 
			where #preserveSingleQuotes(whereClause)# 
			order by #sort# 
		</cfquery>
	</cfif>

	<cfreturn getItems>
</cffunction>
<!--- function : searchSMDR --->

