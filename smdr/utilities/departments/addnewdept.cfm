
<cfset ds = "smdr">

<cfif isdefined("form.btnSave")>
	
	<cftransaction>
	<cfquery name="saveDept" datasource="#ds#">
		if exists (select departmentid from departments where departmentname = '#form.departmentName#') 
			print ('already exists')
		else
		begin
			insert into departments (departmentName) 
			values ('#form.departmentName#') 
		end
	</cfquery>
	<cfquery name="getID" datasource="#ds#">
		select max(departmentid) as newid from departments
		where departmentname = '#departmentName#'
	</cfquery>
	<cfset deptid = getid.newid>
	<cfquery name="insertLookup" datasource="#ds#">
		<cfloop list="#form.extensions#" index="ext" delimiters="#chr(13)#">
			<cfif trim(ext) is not "">
			begin
				if exists (select departmentid from department_extension_lookup where departmentid = #deptid# and extension = #ext#) 
					print ('already exists')
				else
				begin
					insert into department_extension_lookup (departmentid, extension) 
					values (#deptid#, #ext#) 
				end
			end
			</cfif>
		</cfloop>
	</cfquery>
	</cftransaction>
	
	<cflocation url="index.cfm">

<cfelseif isdefined("form.btnCancel")>

	<cflocation url="index.cfm">

<cfelse>

<link rel="stylesheet" type="text/css" href="/styles.css">
<br>
<div align="center">
<table width="500" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Add a new Department </b></td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
			<table width="450%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td>Department Name: </td>
				</tr>
				<form method="post" action="addnewdept.cfm">
				<tr>
					<td>
						<input type="text" name="departmentName" style="width:485px" maxlength="255">
					</td>
				</tr>
				<tr>
					<td>Extensions: (each extension on a new line)</td>
				</tr>
				<tr>
					<td>
						<textarea name="extensions" rows="10" style="width:485px"></textarea>
					</td>
				</tr>
				<tr>
					<td>
						<input name="btnSave" type="submit" class="sidebar" value="Save">
						<input name="btnCancel" type="submit" class="sidebar" value="Cancel">
					</td>
				</tr>
				</form>
			</table>
		</td>
	</tr>
</table>
</div>

</cfif>