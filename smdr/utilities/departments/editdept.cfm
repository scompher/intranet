

<cfset ds = "smdr">

<cfif isdefined("form.btnSave")>
	
	<cftransaction>
	<cfquery name="saveDept" datasource="#ds#">
		update departments 
		set departmentName = '#form.departmentName#' 
		where departmentid = #form.deptid# 
	</cfquery>
	<cfquery name="insertLookup" datasource="#ds#">
		begin
			delete from department_extension_lookup where departmentid = #form.deptid# 
		end
		<cfloop list="#form.extensions#" index="ext" delimiters="#chr(13)#">
			<cfif trim(ext) is not "">
			begin
				if exists (select departmentid from department_extension_lookup where departmentid = #form.deptid# and extension = #ext#) 
					print ('already exists')
				else
				begin
					insert into department_extension_lookup (departmentid, extension) 
					values (#form.deptid#, #ext#) 
				end
			end
			</cfif>
		</cfloop>
	</cfquery>
	</cftransaction>
	
	<cflocation url="index.cfm">

<cfelseif isdefined("form.btnCancel")>

	<cflocation url="index.cfm">

<cfelse>

<cfparam name="form.departmentName" default="">
<cfparam name="form.extensions" default="">

<cfquery name="getDept" datasource="#ds#">
	select * from departments 
	where departmentid = #d# 
</cfquery>

<cfif getDept.recordcount gt 0>
	<cfset form.departmentName = getDept.departmentName>
</cfif>

<cfquery name="getExtensions" datasource="#ds#">
	select * from department_extension_lookup 
	where departmentid = #d# 
	order by extension asc 
</cfquery>

<cfif getExtensions.recordcount gt 0>
	<cfset form.extensions = valuelist(getExtensions.extension,chr(13))>
</cfif>

<link rel="stylesheet" type="text/css" href="/styles.css">
<br>
<div align="center">
<table width="500" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Edit a Department </b></td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
			<table width="450%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td>Department Name: </td>
				</tr>
				<form method="post" action="editdept.cfm">
				<cfoutput>
				<input type="hidden" name="deptid" value="#d#" />
				<tr>
					<td>
						<input type="text" name="departmentName" style="width:485px" maxlength="255" value="#form.departmentName#">
					</td>
				</tr>
				<tr>
					<td>Extensions: (each extension on a new line)</td>
				</tr>
				<tr>
					<td>
						<textarea name="extensions" rows="10" style="width:485px">#form.extensions#</textarea>
					</td>
				</tr>
				</cfoutput>
				<tr>
					<td>
						<input name="btnSave" type="submit" class="sidebar" value="Save">
						<input name="btnCancel" type="submit" class="sidebar" value="Cancel">
					</td>
				</tr>
				</form>
			</table>
		</td>
	</tr>
</table>
</div>

</cfif>