
<cfset ds = "smdr">

<cfquery name="getDepts" datasource="#ds#">
	select * from departments order by departmentName asc 
</cfquery>

<link rel="stylesheet" type="text/css" href="/styles.css">
<div align="center">
<table width="600" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Manage Departments </b></td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				
				<tr>
					<td width="25%" align="center" class="linedrowrightcolumn"><b>Action</b></td>
					<td width="75%" class="linedrow"><b>Department</b></td>
				</tr>
				<cfoutput query="getDepts">
				<tr>
					<td align="center" class="linedrowrightcolumn">
					<a href="editdept.cfm?d=#departmentid#">[edit]</a>&nbsp;
					<a onclick="return confirm('Are you sure you wish to delete this department?');" href="deldept.cfm?d=#departmentid#">[delete]</a>
					</td>
					<td class="linedrow">#departmentName#</td>
				</tr>
				</cfoutput>
				<tr>
					<td align="center"><a href="addnewdept.cfm">[Add New Department]</a></td>
					<td>&nbsp;</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<br />
<a href="/index.cfm" style="text-decoration:underline;" class="normal">Return to Intranet</a>
</div>
