<cfsetting showdebugoutput="no">

<link rel="stylesheet" type="text/css" href="/styles.css">

<title>Condition Codes</title>

<div align="center">
<table width="600" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Condition Codes</b> </td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td width="109" nowrap class="linedrowrightcolumn"><b>Condition Codes </b></td>
					<td width="469" nowrap class="linedrow"><b>Description</b></td>
				</tr>
				<tr>
					<td valign="top" class="linedrowrightcolumn">0</td>
					<td valign="top" class="linedrow">Identifies an intraswitch call, which is a call that originates and<br>
						terminates on the switch</td>
				</tr>
				<tr>
					<td valign="top" class="linedrowrightcolumn">1 (A)</td>
					<td valign="top" class="linedrow">Identifies an attendant-handled call or an attendant-assisted call,<br>
						except conference calls</td>
				</tr>
				<tr>
					<td valign="top" class="linedrowrightcolumn">4 (D) </td>
					<td valign="top" class="linedrow">Identifies an extremely long call or a call with an extremely high<br>
						message count TSC. An extremely long call is a call that lasts for 10<br>
						or more hours. An extremely high message count TSC is 9999 or<br>
						more messages.<br>
						When a call exceeds10 hours, the system creates a call record with<br>
						this condition code and a duration of 9 hours, 59 minutes, and 1-9<br>
						tenths of a minute.<br>
						The system creates a similar call record with this condition code after<br>
						each succeeding 10-hour period.<br>
						When the call terminates, the system creates a final call record with a<br>
						different condition code that identifies the type of call.</td>
				</tr>
				<tr>
					<td valign="top" class="linedrowrightcolumn">6 (E) </td>
					<td valign="top" class="linedrow">Identifies calls that the system does not record because system<br>
						resources are unavailable. The CDR record includes the time and the<br>
						duration of the outage.<br>
						The system generates this condition code for:<br>
						&#9679; Calls that the system routes to the attendant<br>
						&#9679; Calls that require the CDR feature to overwrite records<br>
						&#9679; ISDN calls that are not completed at the far end, if the Q.931<br>
						message includes the reason that the call was not completed.<br>
						The system does not generate the</td>
				</tr>
				<tr>
					<td valign="top" class="linedrowrightcolumn">7 (G) </td>
					<td valign="top" class="linedrow">Identifies calls that use the AAR or ARS feature.</td>
				</tr>
				<tr>
					<td valign="top" class="linedrowrightcolumn">8 (H) </td>
					<td valign="top" class="linedrow">Identifies calls that use the AAR or ARS feature.</td>
				</tr>
				<tr>
					<td valign="top" class="linedrowrightcolumn">9 (I) </td>
					<td valign="top" class="linedrow">Identifies:<br>
						&#9679; An incoming call<br>
						&#9679; A tandem call<br>
						&#9679; An incoming NCA-TSC call<br>
						&#9679; A tandem NCA-TSC call</td>
				</tr>
				<tr>
					<td valign="top" class="linedrowrightcolumn">A</td>
					<td valign="top" class="linedrow">Identifies an outgoing call.</td>
				</tr>
				<tr>
					<td valign="top" class="linedrowrightcolumn">B</td>
					<td valign="top" class="linedrow">Identifies an adjunct-placed outgoing call.</td>
				</tr>
				<tr>
					<td valign="top" class="linedrowrightcolumn">C (L) </td>
					<td valign="top" class="linedrow">Identifies a conference call.<br>
						For trunk CDR, the system creates a separate call record, with this<br>
						condition code, for each incoming or outgoing trunk that is used<br>
						during the conference call.<br>
						If you disable ITCS and OTCS, the system records the extension of<br>
						the originator of the conference call. The system does not record any<br>
						other extension.<br>
						If you disable ITCS, and you administer the originator of the<br>
						conference call to use Intraswitch CDR, the system generates a call<br>
						with this condition code whenever the originator of the conference<br>
						dials a nontrunk conference participant.<br>
						If ITCS is active, and you do not administer the originator of the<br>
						conference call to use Intraswitch CDR, the system generates a call<br>
						with this condition code whenever the originator of the conference<br>
						dials an intraswitch conference</td>
				</tr>
				<tr>
					<td valign="top" class="linedrowrightcolumn">E (N) </td>
					<td valign="top" class="linedrow">Identifies a call that the system does not complete because the<br>
						following facilities to complete the call are unavailable:<br>
						&#9679; Outgoing calls<br>
						- The trunks are busy, and no queue exists.<br>
						- The trunks are busy, and the queue is full.<br>
						&#9679; Incoming calls<br>
						- The extension is busy.<br>
						- The extension is unassigned.<br>
						This condition code also identifies an ISDN Call By Call Service<br>
						Selection call that is unsuccessful because of an administered trunk<br>
						usage allocation plan. Incoming trunk calls to a busy telephone do not<br>
						generate a CDR record.</td>
				</tr>
				<tr>
					<td valign="top" class="linedrowrightcolumn">F</td>
					<td valign="top" class="linedrow">Identifies a call that the system does not complete because of one of<br>
						the following conditions:<br>
						&#9679; The originator of the calls has insufficient calling privileges.<br>
						&#9679; An NSF mismatch occurs for an ISDN call.<br>
						&#9679; An authorization mismatch occurs for a data call.</td>
				</tr>
				<tr>
					<td valign="top" class="linedrowrightcolumn">G</td>
					<td valign="top" class="linedrow">Identifies a call that the system terminates to a ringing station.</td>
				</tr>
				<tr>
					<td valign="top" class="linedrowrightcolumn">H</td>
					<td valign="top" class="linedrow">Notes that the system abandoned a ring call.</td>
				</tr>
				<tr>
					<td valign="top" class="linedrowrightcolumn">I</td>
					<td valign="top" class="linedrow">Identifies a call that the system terminates to a busy station.</td>
				</tr>
				<tr>
					<td valign="top" class="linedrowrightcolumn">J</td>
					<td valign="top" class="linedrow">Identifies an incoming trunk call that is a new connection that uses<br>
						Additional Network Feature-Path Replacement (ANF-PR) or DCS<br>
						with Rerouting. For more information on QSIG and ANF-PR, see the<br>
						Administrator Guide for Avaya Communication Manager.</td>
				</tr>
				<tr>
					<td valign="top" class="linedrowrightcolumn">K</td>
					<td valign="top" class="linedrow">Identifies an outgoing trunk call that is a new connection that uses<br>
						ANF-PR or DCS with Rerouting. For more information on QSIG and<br>
						ANF-PR, see the Administrator Guide for Avaya Communication<br>
						Manager.</td>
				</tr>
				<tr>
					<td valign="top" class="linedrowrightcolumn">M</td>
					<td valign="top" class="linedrow">Identifies an outgoing trunk calls that the system disconnects because<br>
						the call exceeds the time allowed.</td>
				</tr>
				<tr>
					<td valign="top" class="linedrowrightcolumn">T</td>
					<td valign="top" class="linedrow">Identifies CDR records for calls that meet the following conditions:<br>
						&#9679; The Condition Code &lsquo;T&rsquo; for Redirected Calls? field on the<br>
						CDR System Parameters screen is set to y.<br>
						&#9679; The incoming trunk group is direct inward dialing (DID).<br>
						&#9679; The system automatically redirects an incoming call off of the<br>
						server.</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

</div>

