
<cfparam name="form.filterDealerName" default="">

<cfquery name="getReport" datasource="copalink">
	select * from accountmanager_tracking
	<cfif isdefined("form.btnFilter") and trim(form.filterDealerName) is not "">
	where dealerName = '#form.filterDealerName#'
	</cfif>
	order by datetime, dealernumber, accountnumber
</cfquery>

<cfquery name="getDealers" datasource="copalink">
	select distinct dealerName from accountmanager_tracking order by dealerName asc
</cfquery>

<link rel="stylesheet" type="text/css" href="../styles.css">

<div class="normal" align="center">
<table width="725" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Report</b></td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellpadding="5" cellspacing="0" class="grey">
				
				<tr>
					<td colspan="4" class="nopadding">
						<form method="post" action="report.cfm">
						<table border="0" cellspacing="0" cellpadding="5">
							<tr>
								<td>Filter by dealer name: </td>
								<td>
									<select name="filterDealerName" class="smalltable">
										<option <cfif form.filterdealername is "">selected</cfif> value="">All</option>
									<cfoutput query="getDealers">
										<option <cfif form.filterdealername is getdealers.dealername>selected</cfif> value="#dealerName#">#ucase(dealerName)#</option>
									</cfoutput>
									</select>
									</td>
								<td>
									<input name="btnFilter" type="submit" class="sidebarsmall" value="Apply">
								</td>
							</tr>
						</table>
						</form>
					</td>
					</tr>
				<tr>
					<td class="linedrow" width="5">&nbsp;</td>
					<td class="linedrow"><b>Dealer Name </b></td>
					<td class="linedrow"><b>Dealer Number </b></td>
					<td class="linedrow"><b>Account Number </b></td>
					<td class="linedrow"><b>Date/Time</b></td>
				</tr>
				<cfoutput query="getReport">
				<tr>
					<td class="linedrow">#getreport.currentrow#.</td>
					<td class="linedrow">#getReport.dealerName#</td>
					<td class="linedrow">#getReport.dealerNumber#</td>
					<td class="linedrow">#getReport.accountNumber#</td>
					<td class="linedrow">#dateformat(getReport.datetime,'mm/dd/yyyy')# #timeformat(getReport.datetime,'hh:mm tt')#</td>
				</tr>
				</cfoutput>
			</table>
		</td>
	</tr>
</table>
<p><a href="/" style="text-decoration:underline">Return to Intranet</a></p>
</div>
