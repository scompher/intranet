<cfparam name="alarmdb" default="alarmServer">
<cfif isDefined("acct") and isDefined("refid") and isDefined("env")>
	<cfif env is 'PROD'>
		<cfset alarmdb = 'AlarmServer'>
	</cfif>
	<cfif env is 'PRODFL'>
		<cfset alarmdb = 'AlarmServer_FL'>
	</cfif>
	<cfoutput>
		<cfquery name="getURL" datasource="#alarmdb#">
			select verificationURL
			from alarms_url
			where accountNumber = '#acct#' and referenceID = #refid#
		</cfquery>
		<cfif getURL.recordCount is 1>
			<meta http-equiv="refresh" content="0; url=#getURL.verificationURL#">
		</cfif>
	</cfoutput>
	<cfelse>
	Video Unavailable
</cfif> 