<cfsetting showdebugoutput="no">

<title>Ops Intranet Menu</title>

<cfquery name="GetItems" datasource="#ds#">
	select * 
	from OpsMenuItems
	order by itemid asc 
</cfquery>

<link rel="stylesheet" type="text/css" href="../styles.css">

<div align="center">
<table width="800" border="0" cellspacing="0" cellpadding="5" style="font-size:14px;">
	<tr>
		<td>
<b>What is the CSAA Five Diamond Central Station Certification Program?</b>
<br />
<br />
The CSAA Five Diamond Program is a balanced certification program providing a differentiation in the central station monitoring marketplace because it signifies <b>five points of excellence</b>:
<br />
<br />
<li><b>Commitment to random inspections and quality criteria standards by a nationally recognized testing laboratory such as FM Global, Intertek/ETL and UL.</b></li>
<br /><br />
<li><b>Commitment to the highest levels of customer service.</b></li>
<br /><br />
<li><b>Commitment to ongoing job-related education and testing by having 100% of its central station operators certified using the CSAA online training series.</b></li>
<br /><br />
<li><b>Commitment to raising the industry standards through CSAA membership and participation in its activities.</b></li>
<br /><br />
<li><b>Commitment to reducing false dispatches.</b></li>
		</td>
	</tr>
</table>
<br />
<h2>YOUR I.P. ADDRESS IS <cfoutput>#cgi.REMOTE_ADDR#</cfoutput></h2>
<table width="400" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center" class="highlightbar"><b>Operations Menu</b></td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<cfoutput query="getItems">
				<cfif trim(itemURL) is "">
					<cfset location = "/Ops/files/#fileName#">
				<cfelse>
					<cfset location = itemURL>
				</cfif>
				<tr>
					<td width="4%" class="linedrow">#getItems.currentrow#.</td>
					<td width="96%" class="linedrow"><a target="_blank" style="text-decoration:underline" href="#location#">#itemName#</a></td>
				</tr>
				</cfoutput>
			</table>
		</td>
	</tr>
</table>
</div>
