<br>
<div style="text-align:center; font-size:24px"><u>PRIORITY LIST</u></div><br>
<br>
<br>
<div style="width:75%;  margin:0 auto;">
2 = Low Battery, Restores, Cancels, Resets, Troubles with no dispatch, Ac Failure & Loss, Shunts, Memory Loss, 24hr, 48hr, 72hr, etc tests, , Openings & Closings (log in only), Tamper (if no dispatch), Open after alarm, Shunts, Telco Line Fault </div>

<br>
<br>
<br>

<div style="width:75%; margin:0 auto;">
3 = DNO (Did not Open), DNC (Did not Close) & Supervised Opening & Closing codes, DNT (Did not test), DNR (Did not Restore), Fire Trouble, Fire Supervisory
</div>
<br>
<br>
<br>
<div style="width:75%; margin:0 auto;">
4 = Burglary Codes, Electronic cancels, Tampers (if dispatched), Radio Telco Line Fault, Temperature Alarms, Environmental
</div>
<br>
<br>
<br>
<div style="width:75%; margin:0 auto;">
5 = Not used at this time.
</div>
<br>
<br>
<br>
<div style="width:75%; margin:0 auto;">
6 = Panic, Duress, Ambush, Hostage, Hold-up, Police Panic, Intrusion
</div>
<br>
<br>
<br>
<div style="width:75%;  margin:0 auto;">
7 = Fire, Fire Panic, Medical, Ambulance, Emergency, Smoke & Heat Detectors, Pull Stations.
</div>
<br>
<br>
<br>
<div style="width:75%;  margin:0 auto;">
8 = <b><u><i>Two Way Voice Only: </i></u></b> All Burgs, Fire, Medical, Ambulance, Emergency, Panic, Duress, Ambush, etc.
</div>
<br>
<br>
<br>
<div style="width:75%; margin:0 auto; font-size:18px">
<b>NOTE: ANY CODE THAT HAS A DISPATCH HANDLE ON IT SHOULD UP THE PRIORITY TO A 4 OR 7 DEPENDING ON WHETHER IT IS A PD DISP OR FIRE/MEDICAL DISP.</b>
</div>
<br>
<br>
<div style="width:75%; margin:0 auto; font-size:18px">
<b>NOTEl CODES LABELED BURG, FIRE, MEDICAL, OR PANIC WITHOUT A DISPATCH MUST ALWAYS REMAIN THEIR SAME HIGH PRIORITY.</b>
</div>

