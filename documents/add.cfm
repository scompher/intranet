
<cfif isDefined("form.btnAddDoc")>

	<cftransaction>
		<!--- save file --->
		<cffile action="upload" filefield="filename" destination="#request.directpath#\documents\docs\" nameconflict="makeunique">
		<cfset savedfile = file.ServerFile>
		<cfset docname = file.ClientFile>
		<!--- remove spaces and replace with _ character --->
		<cfset docname = replace(docname, " ", "_", "all")>
		<cfset docname = replace(docname, "'", "", "all")>
		
		<cffile action="rename" source="#request.directpath#\documents\docs\#savedfile#" destination="#request.directpath#\documents\docs\#docname#">
	
		<!--- save info in database --->
		<cfquery name="saveInfo" datasource="#request.odbc_datasource#">
			insert into documents_main (name, filename, departmentid)
			values ('#form.name#', '#docname#', #form.departmentid#)
		</cfquery>
	</cftransaction>

	<cflocation url="index.cfm">

<cfelse>
	
	<cfquery name="getdepts" datasource="#ds#">
		select * from Admin_Users_Departments
		order by department asc
	</cfquery>
	
	<script language="JavaScript" type="text/JavaScript">
	function checkform(frm) {
		if (frm.name.value == '') {
			alert('The name is required.');
			frm.name.focus();
			return false;
		}
		if (frm.filename.value == '') {
			alert('The file is required.');
			frm.filename.focus();
			return false;
		}
		if (frm.departmentid.selectedIndex == 0) {
			alert('The department is required');
			frm.departmentid.focus();
			return false;
		}
		return true;
	}
	</script>

	<div align="center">
	<form method="post" action="add.cfm" enctype="multipart/form-data">
	<table border="1" cellpadding="5" cellspacing="0">
		<tr bgcolor="#DDDDDD">
			<td colspan="2" align="center"><b>Add a Document</b></td>
		</tr>
		<tr>
			<td>Document Name: </td>
			<td><input name="name" type="text" class="normal" size="50" maxlength="255"></td>
		</tr>
		<tr>
			<td>Document File: </td>
			<td><input name="filename" type="file" class="normal" id="document" size="50"></td>
		</tr>
			<tr>
				<td>Department:</td>
				<td>
				<select name="departmentid">
					<option value="-1"></option>
					<option value="0">Forms</option>
				<cfoutput query="getdepts">
					<option value="#getdepts.departmentid#">#getdepts.department#</option>
				</cfoutput>
				</select>
				</td>
			</tr>
		<tr>
			<td colspan="2" align="center"><input onClick="return checkform(this.form);" name="btnAddDoc" type="submit" class="normal" value="Add Document"></td>
		</tr>
	</table>
	</form>
	<table width="500" border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td align="center">
		<a href="index.cfm">Return to Documents Menu</a> | <a href="/index.cfm">Return to Administrative Menu</a>
		</td>
	</tr>
	</table>
	</div>

</cfif>

