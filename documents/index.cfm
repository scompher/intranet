
<cfquery name="getdocs" datasource="#request.odbc_datasource#">
	select documents_main.*, Admin_Users_Departments.department
	from documents_main
	left join Admin_Users_Departments on documents_main.departmentid = Admin_Users_Departments.departmentid
	order by department ASC, name ASC
</cfquery>
<div align="center">
<table border="1" cellpadding="5" cellspacing="0">
    <tr bgcolor="#DDDDDD">
        <td colspan="2" align="center"><b>Available Documents</b></td>
    </tr>
    <tr><td colspan="2">&nbsp;</td></tr>
	<tr>
		<td colspan="2" align="center" bgcolor="#FFFFCC"><a href="add.cfm">Add a new document</a></td>
	</tr>
    <tr><td colspan="2">&nbsp;</td></tr>
	<cfif getdocs.recordcount GT 0>
	<cfoutput query="getdocs" group="departmentid">
	<tr bgcolor="eeeeee">
		<td colspan="2" align="center"><b><cfif getdocs.departmentid IS 0>Forms<cfelse>#getdocs.department#</cfif></b></td>
	</tr>
	<cfoutput>
	<tr>
		<td width="20%" align="center">
		<a href="edit.cfm?did=#documentid#"><img alt="Edit" src="/images/edit.gif" border="0"></a>&nbsp;&nbsp;
		<a onClick="return confirm('Are you sure you wish to delete this document?');" href="delete.cfm?did=#documentid#"><img alt="Delete" src="/images/delete.gif" border="0"></a>
		</td>
        <td><a target="_blank" href="docs/#filename#">#name#</a></td>
    </tr>
	</cfoutput>
	</cfoutput>
	<cfelse>
	<tr>
		<td colspan="2" align="center">There are currently no documents on file</td>
	</tr>
	</cfif>
</table>
<br>
<table width="500" border="0" cellpadding="5" cellspacing="0">
<tr>
	<td align="center"><a href="/index.cfm">Return to Administrative Menu</a></td>
</tr>
</table>
</div>

