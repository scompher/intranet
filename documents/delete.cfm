
<cfquery name="getdoc" datasource="#request.odbc_datasource#">
	select * from documents_main
	where documentid = #url.did#
</cfquery>

<cfif getdoc.recordcount GT 0>

	<!--- delete doc --->
	<cfif fileexists("#request.directpath#\documents\docs\#getdoc.filename#")>
		<cffile action="delete" file="#request.directpath#\documents\docs\#getdoc.filename#">
	</cfif>

	<!--- delete db info --->
	<cfquery name="deldocinfo" datasource="#request.odbc_datasource#">
		delete from documents_main
		where documentid = #url.did#
	</cfquery>

</cfif>

<cflocation url="index.cfm">
