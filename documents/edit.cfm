
<cfif isDefined("form.btnEditDoc")>

	<cftransaction>
		<cfif trim(form.filename) IS NOT "">
			<!--- delete existing --->
			<cfif fileexists("#request.directpath#\documents\docs\#form.currentfile#")>
				<cffile action="delete" file="#request.directpath#\documents\docs\#form.currentfile#">
			</cfif>
			<!--- save file --->
			<cffile action="upload" filefield="filename" destination="#request.directpath#\documents\docs\" nameconflict="makeunique">
			<cfset savedfile = file.ServerFile>
			<cfset docname = file.ClientFile>
			<!--- remove spaces and replace with _ character --->
			<cfset docname = replace(docname, " ", "_", "all")>
			<cfset docname = replace(docname, "'", "", "all")>
			
			<cffile action="rename" source="#request.directpath#\documents\docs\#savedfile#" destination="#request.directpath#\documents\docs\#docname#">
		<cfelse>
			<cfset docname = "">
		</cfif>
		<!--- save info in database --->
		<cfquery name="saveInfo" datasource="#request.odbc_datasource#">
			update documents_main
			set departmentid = #form.departmentid#, name = '#form.name#'<cfif trim(docname) IS NOT "">, filename = '#docname#'</cfif>
			where documentid = #form.documentid#
		</cfquery>
	</cftransaction>

	<cflocation url="index.cfm">

<cfelse>

	<cfquery name="getdepts" datasource="#ds#">
		select * from Admin_Users_Departments
		order by department asc
	</cfquery>

	<script language="JavaScript" type="text/JavaScript">
	function checkform(frm) {
		if (frm.name.value == '') {
			alert('The name is required.');
			frm.name.focus();
			return false;
		}

		return true;
	}
	</script>

	<cfquery name="getdoc" datasource="#request.odbc_datasource#">
		select * from documents_main
		where documentid = #url.did#
	</cfquery>

	<div align="center">
	<form method="post" action="edit.cfm" enctype="multipart/form-data">
	<cfoutput query="getdoc">
		<input type="hidden" name="documentid" value="#url.did#">
		<input type="hidden" name="currentfile" value="#getdoc.filename#">
		<table border="1" cellpadding="5" cellspacing="0">
			<tr bgcolor="DDDDDD">
				<td colspan="2" align="center"><b>Edit a Document</b></td>
			</tr>
			<tr>
				<td>Document Name: </td>
				<td><input name="name" type="text" class="normal" size="50" maxlength="255" value="#getdoc.name#"></td>
			</tr>
			<tr>
				<td>Document File: </td>
				<td><input name="filename" type="file" class="normal" size="50"></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td><a target="_blank" href="docs/#filename#">View Current</a></td>
			</tr>
			<tr>
				<td>Department:</td>
				<td>
				<select name="departmentid">
					<option value="0" <cfif getdoc.departmentid IS 0>selected</cfif> >Forms</option>
				<cfloop query="getdepts">
					<option value="#getdepts.departmentid#" <cfif getdepts.departmentid IS getdoc.departmentid>selected</cfif> >#getdepts.department#</option>
				</cfloop>
				</select>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center"><input onClick="return checkform(this.form);" name="btnEditDoc" type="submit" class="normal" value="Update Document"></td>
			</tr>
		</table>
	</cfoutput>
	</form>
	<table width="500" border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td align="center">
		<a href="index.cfm">Return to Documents Menu</a> | <a href="/index.cfm">Return to Administrative Menu</a>
		</td>
	</tr>
	</table>
	</div>

</cfif>
