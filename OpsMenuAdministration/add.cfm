
<cfset filePath = request.DirectPath & "\Ops\files">
<cfset ID = "">

<cfparam name="newFileName" default="">
<cfparam name="itemURL" default="">

<cfif isdefined("form.btnAdd")>
	<cfif trim(itemFile) is not "">
		<cffile action="upload" filefield="itemFile" destination="#filePath#\" nameconflict="overwrite">
		<cfset savedFile = file.ServerFile>
		<cfset uploadedFile = file.ClientFile>
		<cfset newFileName = ID & replace(uploadedFile, " ", "")>
		<cffile action="rename" source="#filePath#\#savedFile#" destination="#filePath#\#newFileName#">
	</cfif>
	
	<cfif trim(itemURL) is not "">
		<cfif findnocase("http://", itemURL) is 0>
			<cfset itemURL = "http://" & itemURL>
		</cfif>
	</cfif>

	<cfquery name="addItem" datasource="#ds#">
		insert into OpsMenuItems (itemName, itemURL, fileName) 
		values ('#form.itemName#', '#itemURL#', '#newFileName#')
	</cfquery>
	
	<cflocation url="index.cfm">
</cfif>

<cfif isDefined("form.btnCancel")>
	<cflocation url="index.cfm">
</cfif>

<link rel="stylesheet" type="text/css" href="../styles.css">

<br>
<div align="center">
	<table border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td class="highlightbar"><b>Operations Menu Administration - Add a Menu Item</b> </td>
		</tr>
		<tr>
			<td class="greyrowbottomnopadding">
				<table border="0" cellspacing="0" cellpadding="5">
					<form method="post" action="add.cfm" enctype="multipart/form-data">
					<tr>
						<td nowrap><b>Menu Item Label: </b></td>
						<td>
							<input type="text" name="itemName" style="width:300px" maxlength="250">
						</td>
					</tr>
					<tr>
						<td colspan="2" align="center" nowrap><b>You can link to either a file you upload, or URL</b> </td>
					</tr>
					<tr>
						<td nowrap><b>File to link to:</b></td>
						<td>
							<input type="file" name="itemFile">
						</td>
					</tr>
					<tr>
						<td nowrap><b>Link (URL):</b></td>
						<td>
							<input type="text" name="itemURL" style="width:300px" maxlength="250">
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<input name="btnAdd" type="submit" class="sidebar" value="Add Item">
							<input name="btnCancel" type="submit" class="sidebar" value="Cancel">
						</td>
					</tr>
					</form>
				</table>
			</td>
		</tr>
	</table>
</div>
