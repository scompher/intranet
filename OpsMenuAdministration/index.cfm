
<cfquery name="GetItems" datasource="#ds#">
	select * 
	from OpsMenuItems
	order by itemid asc 
</cfquery>

<link rel="stylesheet" type="text/css" href="../styles.css">

<div align="center">
<table width="400" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center" class="highlightbar"><b>Operations Menu  Administration</b></td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td width="29%" align="center" class="linedrow"><b>Action</b></td>
					<td width="71%" class="linedrow"><b>Item Label</b></td>
				</tr>
				<cfoutput query="getItems">
				<cfif trim(itemURL) is "">
					<cfset location = "/Ops/files/#fileName#">
				<cfelse>
					<cfset location = itemURL>
				</cfif>
				<tr>
					<td align="center" nowrap="nowrap" class="linedrow">
					<a href="edit.cfm?itemid=#itemid#">[edit]</a>
					&nbsp;
					<a onclick="return confirm('Are you sure you wish to delete this item?');" href="delete.cfm?itemid=#itemid#">[remove]</a>					</td>
					<td class="linedrow">#getItems.currentRow#. <a target="_blank" style="text-decoration:underline" href="#location#">#itemName#</a></td>
				</tr>
				</cfoutput>
				<tr>
					<td align="center" nowrap="nowrap" class="linedrow"><a href="add.cfm">Add New </a></td>
					<td>&nbsp;</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</div>
