
<cfset filePath = request.DirectPath & "\Ops\files">

<cfquery name="getItem" datasource="#ds#">
	select * 
	from OpsMenuItems
	where itemid = #itemid#
</cfquery>

<!--- delete any files --->
<cfif trim(getitem.fileName) is not "">
	<cfif fileExists("#filePath#\#getItem.fileName#")>
		<cffile action="delete" file="#filePath#\#getItem.fileName#">
	</cfif>
</cfif>

<cfquery name="updateFileName" datasource="#ds#">
	update OpsMenuItems
	set fileName = '' 
	where itemid = #itemid# 
</cfquery>

<cflocation url="edit.cfm?itemid=#itemid#">

