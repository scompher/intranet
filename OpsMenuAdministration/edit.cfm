

<cfset filePath = request.DirectPath & "\Ops\files">
<cfset ID = "">

<cfparam name="newFileName" default="">
<cfparam name="itemURL" default="">

<cfif isdefined("form.btnUpdate")>
	
	<cftransaction>
		<cfif trim(itemFile) is not "">
			<cfif trim(currentFile) is not "">
				<cfif fileExists("#filePath#\#currentFile#")>
					<cffile action="delete" file="#filePath#\#currentFile#">
				</cfif>
			</cfif>
			<cffile action="upload" filefield="itemFile" destination="#filePath#\" nameconflict="overwrite">
			<cfset savedFile = file.ServerFile>
			<cfset uploadedFile = file.ClientFile>
			<cfset newFileName = ID & replace(uploadedFile, " ", "")>
			<cffile action="rename" source="#filePath#\#savedFile#" destination="#filePath#\#newFileName#">
		</cfif>
	
		<cfif trim(itemURL) is not "">
			<cfif findnocase("http://", itemURL) is 0>
				<cfset itemURL = "http://" & itemURL>
			</cfif>
		</cfif>
	
		<cfif trim(newFileName) is ""><cfset newFileName = currentFile></cfif>
	
		<cfquery name="updateItem" datasource="#ds#">
			update OpsMenuItems 
			set itemName = '#itemName#', itemURL = '#itemURL#', fileName = '#newFileName#'
			where itemid = #itemid#
		</cfquery>
	</cftransaction>
	
	<cflocation url="index.cfm">
</cfif>

<cfif isDefined("form.btnCancel")>
	<cflocation url="index.cfm">
</cfif>

<link rel="stylesheet" type="text/css" href="../styles.css">

<cfquery name="getItem" datasource="#ds#">
	select * 
	from OpsMenuItems
	where itemid = #itemid#
</cfquery>

<br>
<div align="center">
	<table border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td class="highlightbar"><b>Operations Menu Administration - Add a Menu Item</b> </td>
		</tr>
		<tr>
			<td class="greyrowbottomnopadding">
				<table border="0" cellspacing="0" cellpadding="5">
					<cfoutput query="getItem">
					<form method="post" action="edit.cfm" enctype="multipart/form-data">
					<input type="hidden" name="currentFile" value="#fileName#" />
					<input type="hidden" name="itemid" value="#itemid#" />
					<tr>
						<td nowrap><b>Menu Item Label: </b></td>
						<td>
							<input type="text" name="itemName" style="width:300px" maxlength="250" value="#getitem.itemname#">
						</td>
					</tr>
					<tr>
						<td colspan="2" align="center" nowrap><b>You can link to either a file you upload, or URL</b> </td>
					</tr>
					<tr>
						<td nowrap><b>File to link to:</b></td>
						<td>
							<input type="file" name="itemFile">
						</td>
					</tr>
					<cfif trim(fileName) is not "">
					<tr>
						<td nowrap>&nbsp;</td>
						<td>
						<a target="_blank" href="/Ops/files/#fileName#">[View Existing File]</a>&nbsp;
						<a onclick="return confirm('Are you sure you wish to delete this file?');" href="delFile.cfm?itemid=#itemid#">[Delete Existing File]</a>&nbsp;
						</td>
					</tr>
					</cfif>
					<tr>
						<td nowrap><b>Link (URL):</b></td>
						<td>
							<input type="text" name="itemURL" style="width:300px" maxlength="250" value="#getitem.itemURL#">
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<input name="btnUpdate" type="submit" class="sidebar" value="Update Item">
							<input name="btnCancel" type="submit" class="sidebar" value="Cancel">
						</td>
					</tr>
					</form>
					</cfoutput>
				</table>
			</td>
		</tr>
	</table>
</div>
