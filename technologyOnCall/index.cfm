
<cfif isDefined("form.btnCreateNew")>
	<cflocation url="new.cfm">
</cfif>

<cfif isDefined("form.btnPrintReport")>
	<cfinclude template="print.cfm">
	<cfabort>
</cfif>

<cfif isDefined("form.btnShowAll")>
	<cfset form.startDate = "">
	<cfset form.endDate = "">
	<cfset form.onCallTechID = 0>
	<cfset form.departmentID = 0>
</cfif>

<link rel="stylesheet" type="text/css" href="../styles.css">

<script language="javascript" src="cal2.js">
/*
Xin's Popup calendar script-  Xin Yang (http://www.yxscripts.com/)
Script featured on/available at http://www.dynamicdrive.com/
This notice must stay intact for use
*/
</script>
<script language="javascript" src="cal_conf2.js"></script>

<cfparam name="form.startDate" default="">
<cfparam name="form.endDate" default="">
<cfparam name="form.onCallTechID" default="0">
<cfparam name="form.departmentID" default="0">

<cfquery name="getCalls" datasource="#ds#">
	select technology_call_log.callid, technology_call_log.dateTimeOccurred, technology_call_log.issue, admin_users_departments.department, admin_users.firstname, admin_users.lastname,  technology_call_log.calledBy 
	from technology_call_log 
	left join admin_users_departments on technology_call_log.departmentid = admin_users_departments.departmentid
	left join admin_users on technology_call_log.onCallTechID = admin_users.adminuserid 
	where 
	<cfif startDate is not "">technology_call_log.dateTimeOccurred >= #createodbcdate(startDate)# and </cfif>
	<cfif endDate is not "">technology_call_log.dateTimeOccurred <= #createodbcdate(endDate)# and </cfif>
	<cfif departmentID is not 0>technology_call_log.departmentid = #departmentid# and </cfif>
	<cfif onCallTechID is not 0>technology_call_log.onCallTechID = #onCallTechID# and </cfif> 
	1=1 
	order by technology_call_log.dateTimeOccurred desc 
</cfquery>
<cfset callItems = valuelist(getCalls.callid)>

<cfquery name="getDepts" datasource="#ds#">
	select * from Admin_Users_Departments
	order by department asc
</cfquery>

<cfquery name="getTechInfo" datasource="#ds#">
	select admin_users.* 
	from admin_users
	inner join Admin_Users_Departments_Lookup on admin_users.adminuserid = Admin_Users_Departments_Lookup.adminuserid
	where Admin_Users_Departments_Lookup.departmentid = 1 and admin_users.active = 1 
	order by lastname, firstname
</cfquery>

<br>
<div align="center" class="normal">
<table width="950" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Technology After Hours Call Log</b> </td>
	</tr>
	<form method="post" action="index.cfm" name="mainform">
	<cfoutput>
	<input type="hidden" name="callItems" value="#callItems#" />
	</cfoutput>
	<tr>
		<td class="greyrow"><input name="btnCreateNew" type="submit" class="sidebar" value="Create New Call Ticket"></td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td align="center" class="nopadding">
						<table border="0" cellspacing="0" cellpadding="5">
							<tr>
								<td nowrap="nowrap">Start Date: </td>
								<td class="nopadding">
									<table border="0" cellspacing="0" cellpadding="5">
										<tr>
											<td>
											<cfoutput>
											<input name="startDate" type="text" style="width:75px" value="#form.startDate#" maxlength="10" />
											</cfoutput>
											</td>
											<td><a style="text-decoration:none;" href="javascript:showCal('StartDate');"><img src="../images/calicon.gif" alt="Select a Date" align="absmiddle" width="20" height="20" border="0" /></a></td>
										</tr>
									</table>
								</td>
								<td nowrap="nowrap">End Date: </td>
								<td class="nopadding">
									<table border="0" cellspacing="0" cellpadding="5">
										<tr>
											<td>
											<cfoutput>
											<input name="endDate" type="text" style="width:75px" value="#form.endDate#" maxlength="10" />
											</cfoutput>
											</td>
											<td><a style="text-decoration:none;" href="javascript:showCal('EndDate');"><img src="../images/calicon.gif" alt="Select a Date" align="absmiddle" width="20" height="20" border="0" /></a></td>
										</tr>
									</table>
								</td>
								<td nowrap="nowrap">Department:</td>
								<td>
								<select name="departmentid" class="smalltable">
									<option value="0"></option>
									<cfoutput query="getDepts">
										<option <cfif getdepts.departmentid is form.departmentid>selected</cfif> value="#getdepts.departmentid#">#department#</option>
									</cfoutput>
								</select>
								</td>
								<td nowrap="nowrap">Tech:</td>
								<td nowrap="nowrap">
									<select name="onCallTechID" class="smalltable">
										<option value="0"></option>
										<cfoutput query="getTechInfo">
											<option <cfif form.onCallTechID is getTechInfo.adminuserid>selected</cfif> value="#adminuserid#">#firstname# #lastname#</option>
										</cfoutput>
									</select>
								</td>
								<td nowrap="nowrap">
									<input name="btnApply" type="submit" class="sidebarsmall" value="Apply" />
									<input name="btnShowAll" type="submit" class="sidebarsmall" value="Show All" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<table border="0" cellspacing="0" cellpadding="5" width="100%">
				<tr>
					<td width="127" align="center" nowrap class="linedrow"><b>Occurred </b></td>
					<td width="100" align="center" nowrap class="linedrow"><b>Called By</b></td>
					<td width="77" align="center" nowrap class="linedrow"><b>Department</b></td>
					<td width="100" align="center" nowrap class="linedrow"><b>Tech Name </b></td>
					<td nowrap class="linedrow"><b>Issue</b></td>
				</tr>
				<cfif getCalls.recordcount gt 0>
					<cfoutput query="getCalls">
					<a href="view.cfm?cid=#callid#">
					<tr>
						<td align="center" valign="top" nowrap class="linedrowrightcolumn">#dateformat(dateTimeOccurred, 'mm/dd/yyyy')# #timeFormat(dateTimeOccurred, 'hh:mm tt')#</td>
						<td valign="top" class="linedrowrightcolumn">#calledBy#</td>
						<td valign="top" nowrap class="linedrowrightcolumn">#department#</td>
						<td valign="top" nowrap class="linedrowrightcolumn">#firstname# #lastname#</td>
						<td valign="top" class="linedrowrightcolumn">#left(issue,255)#<cfif len(issue) gt 255>...</cfif></td>
					</tr>
					</a>
					</cfoutput>
				<cfelse>
					<tr>
						<td colspan="4">
						Currently there are no calls on file
						</td>
					</tr>
				</cfif>
				<tr>
					<td valign="top" nowrap colspan="4">
						<input name="btnCreateNew" type="submit" class="sidebar" value="Create New Call Ticket">&nbsp;
						<cfif getCalls.recordcount GT 0 and getsec.seclevelid LTE 2>
							<input name="btnPrintReport" type="submit" class="sidebar" value="Print these Results">
						</cfif>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	</form>
</table>
<br>
<a style="text-decoration:underline" href="/index.cfm">Return to Intranet</a>
<br>
</div>