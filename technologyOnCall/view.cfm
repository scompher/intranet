
<link rel="stylesheet" type="text/css" href="../styles.css">

<cfparam name="cid" default="0">
<cfparam name="onCallTech" default="">
<cfparam name="dateOccurred" default="#dateformat(now(),'mm/dd/yyyy')#">
<cfparam name="timeOccurredHH" default="#timeformat(now(),'hh')#">
<cfparam name="timeOccurredMM" default="#timeformat(now(),'mm')#">
<cfparam name="timeOccurredTT" default="#timeformat(now(),'tt')#">
<cfparam name="dateCalledBack" default="#dateformat(now(),'mm/dd/yyyy')#">
<cfparam name="timeCalledBackHH" default="#timeformat(now(),'hh')#">
<cfparam name="timeCalledBackMM" default="#timeformat(now(),'mm')#">
<cfparam name="timeCalledBackTT" default="#timeformat(now(),'tt')#">
<cfparam name="calledBy" default="">
<cfparam name="departmentid" default="0">
<cfparam name="issue" default="">
<cfparam name="otherTechs" default="">
<cfparam name="resolution" default="">
<cfparam name="callBackPerson" default="">

<cfquery name="getCallDetails" datasource="#ds#">
	select technology_call_log.*, admin_users_departments.department, callform_systems.system, admin_users.firstname, admin_users.lastname 
	from technology_call_log 
	left join admin_users_departments on technology_call_log.departmentid = admin_users_departments.departmentid
	left join admin_users on technology_call_log.onCallTechID = admin_users.adminuserid 
	left join callform_systems on technology_call_log.systemid = callform_systems.systemid
	where callid = #cid#
</cfquery>

<br>
<div align="center" class="normal">
<table width="725" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Technology After Hours Call Log </b></td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
			<table border="0" cellpadding="5" cellspacing="0" width="100%">
				<cfoutput query="getCallDetails">
				<tr>
					<td width="27%" nowrap class="linedrowrightcolumn"><b>On Call Tech: </b></td>
					<td width="73%" class="linedrowrightcolumn">
						#firstname# #lastname#
					</td>
				</tr>
				<tr>
					<td nowrap class="linedrowrightcolumn"><b>Ocurred On: </b></td>
					<td class="linedrowrightcolumn">
					#dateformat(dateTimeOccurred, 'mm/dd/yyyy')# #timeformat(dateTimeOccurred, 'hh:mm tt')#
					</td>
				</tr>
				<tr>
					<td nowrap class="linedrowrightcolumn"><b>Called By:</b></td>
					<td class="linedrowrightcolumn">#calledBy#</td>
				</tr>
				<tr>
					<td nowrap class="linedrowrightcolumn"><b>Department Calling: </b></td>
					<td class="linedrowrightcolumn">
					#department#&nbsp;					</td>
				</tr>
				<tr>
					<td nowrap class="linedrowrightcolumn"><b>System Affected: </b></td>
					<td class="linedrowrightcolumn">
					#system#&nbsp;					</td>
				</tr>
				<tr>
					<td colspan="2" nowrap class="linedrowrightcolumn"><b>Issue/Problem/Question:</b></td>
					</tr>
				<tr>
					<td colspan="2" class="linedrowrightcolumn">
						#replace(issue, chr(13), "<br>", "all")#&nbsp;					</td>
				</tr>
				<tr>
					<td nowrap class="linedrowrightcolumn"><b>Other Techs Involved: </b></td>
					<td class="linedrowrightcolumn">
						#otherTechs#&nbsp;					</td>
				</tr>
				<tr>
					<td colspan="2" nowrap class="linedrowrightcolumn"><b>Resolution:</b></td>
				</tr>
				<tr>
					<td colspan="2" class="linedrowrightcolumn">
						#replace(resolution, chr(13), "<br>", "all")#&nbsp;					</td>
				</tr>
				<tr>
					<td nowrap class="linedrowrightcolumn"><b>Call back person spoken to:</b></td>
					<td class="linedrowrightcolumn">
						#callBackPerson#&nbsp;					</td>
				</tr>
				<tr>
					<td nowrap class="linedrowrightcolumn"><b>Date/Time Called Back: </b></td>
					<td class="linedrowrightcolumn">
					#dateformat(dateTimeCalledBack, 'mm/dd/yyyy')# #timeformat(dateTimeCalledBack, 'hh:mm tt')#&nbsp;					</td>
				</tr>
				<tr>
					<td colspan="2" nowrap class="linedrowrightcolumn"><b>Additional Notes:</b> </td>
				</tr>
				<tr>
					<td colspan="2" nowrap class="linedrowrightcolumn">#replace(notes, chr(13), "<br>", "all")#&nbsp; </td>
				</tr>
				</cfoutput>
			</table>
		</td>
	</tr>
</table>
<br>
<a style="text-decoration:underline" href="/index.cfm">Return to Intranet</a> | 
<a style="text-decoration:underline" href="index.cfm">Return to Call List</a> 
</div>
<br>
