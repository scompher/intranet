
<cfquery name="getItems" datasource="#ds#">
	select callid 
	from technology_call_log 
	where callid IN (#form.callItems#) 
	order by technology_call_log.callid desc 
</cfquery>

<cfdocument format="pdf" orientation="portrait" scale="75">

<cfloop query="getItems">
	<cfset i = getItems.callid>
	<cf_printItem id="#i#" datasource="#ds#">
	<cfdocumentitem type="footer"><cfoutput>Page #cfdocument.currentpagenumber# of #cfdocument.totalpagecount#</cfoutput></cfdocumentitem>
	<cfif getItems.currentrow is not getItems.recordcount>
		<cfdocumentitem type="pagebreak"></cfdocumentitem>
	</cfif>
</cfloop>

</cfdocument>

