
<link rel="stylesheet" type="text/css" href="../styles.css">

<cfparam name="err" default="">
<cfparam name="onCallTechID" default="#cookie.adminlogin#">
<cfparam name="dateOccurred" default="#dateformat(now(),'mm/dd/yyyy')#">
<cfparam name="timeOccurredHH" default="#timeformat(now(),'hh')#">
<cfparam name="timeOccurredMM" default="#timeformat(now(),'mm')#">
<cfparam name="timeOccurredTT" default="#timeformat(now(),'tt')#">
<cfparam name="dateCalledBack" default="#dateformat(now(),'mm/dd/yyyy')#">
<cfparam name="timeCalledBackHH" default="#timeformat(now(),'hh')#">
<cfparam name="timeCalledBackMM" default="#timeformat(now(),'mm')#">
<cfparam name="timeCalledBackTT" default="#timeformat(now(),'tt')#">
<cfparam name="calledBy" default="">
<cfparam name="deptid" default="0">
<cfparam name="systemid" default="0">
<cfparam name="issue" default="">
<cfparam name="otherTechs" default="">
<cfparam name="resolution" default="">
<cfparam name="callBackPerson" default="">
<cfparam name="notes" default="">

<cfif isdefined("form.btnCancel")>
	<cflocation url="index.cfm">
</cfif>

<cfif isdefined("form.btnSaveInformation")>

	<cfset dateTimeOccurred = dateOccurred & " " & timeOccurredHH & ":" & timeOccurredMM & " " & timeOccurredTT>
	<cfset dateTimeCalledBack= dateCalledBack & " " & timeCalledBackHH & ":" & timeCalledBackMM & " " & timeCalledBackTT>

	<cfinclude template="validateform.cfm">

	<cfif isValid>
		<cfquery name="saveDetails" datasource="#ds#">
			insert into technology_call_log (onCallTechID, dateTimeOccurred, dateTimeCalledBack, calledBy, departmentid, issue, otherTechs, resolution, callBackPerson, systemid, notes)
			values (#onCallTechID#, #createodbcdatetime(dateTimeOccurred)#, #createodbcdatetime(dateTimeCalledBack)#, '#calledBy#', #deptid#, '#issue#', '#otherTechs#', '#resolution#', '#callBackPerson#', #systemid#, '#notes#')
		</cfquery>
		
		<cflocation url="index.cfm">
	</cfif>
</cfif>

<script language="javascript" src="cal2.js">
/*
Xin's Popup calendar script-  Xin Yang (http://www.yxscripts.com/)
Script featured on/available at http://www.dynamicdrive.com/
This notice must stay intact for use
*/
</script>
<script language="javascript" src="cal_conf2.js"></script>

<cfquery name="getTechInfo" datasource="#ds#">
	select admin_users.* 
	from admin_users
	inner join Admin_Users_Departments_Lookup on admin_users.adminuserid = Admin_Users_Departments_Lookup.adminuserid
	where Admin_Users_Departments_Lookup.departmentid = 1 and admin_users.active = 1
	order by lastname, firstname
</cfquery>

<cfquery name="getDepts" datasource="#ds#">
	select * from Admin_Users_Departments
	order by department asc
</cfquery>

<cfquery name="getsystems" datasource="#ds#">
	select systemid as sysid, system
	from callform_systems
	order by system ASC
</cfquery>

<br>
<div align="center" class="normal">
<cfif trim(err) is not "">
	<table width="725" border="0" cellspacing="0" cellpadding="5">
		<cfloop list="#err#" index="e">
		<cfoutput>
		<tr>
			<td class="alert"><b>#e#</b></td>
		</tr>
		</cfoutput>
		</cfloop>
	</table>
</cfif>
<table width="725" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Technology After Hours Call Log </b></td>
	</tr>
	<form method="post" action="new.cfm" name="mainform">
	<tr>
		<td class="greyrowbottomnopadding">
			<table border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td width="184" nowrap><b>Tech Called: </b></td>
					<td>
						<select name="onCallTechID">
							<cfoutput query="getTechInfo">
								<option <cfif onCallTechID is getTechInfo.adminuserid>selected</cfif> value="#adminuserid#">#firstname# #lastname#</option>
							</cfoutput>
						</select>
					</td>
				</tr>
				<tr>
					<td nowrap><b>Ocurred On:  </b></td>
					<td class="nopadding">
					<cfoutput>
						<table border="0" cellpadding="5" cellspacing="0" class="grey">
							<tr>
								<td>Date:</td>
								<td>
									<input name="dateOccurred" type="text" style="width:75px" value="#dateOccurred#" maxlength="10" />
								</td>
								<td> <a style="text-decoration:none;" href="javascript:showCal('DateOccurred');"> <img src="../images/calicon.gif" alt="Select a Date" align="absmiddle" width="20" height="20" border="0" /> </a> </td>
								<td>Time:</td>
								<td class="nopadding">
									<table border="0" cellpadding="5" cellspacing="0" class="grey">
										<tr>
											<td class="nopadding">
												<input name="timeOccurredHH" type="text" style="width:25px" value="#timeOccurredHH#" maxlength="2" />
											</td>
											<td valign="middle">:</td>
											<td class="nopadding">
												<input name="timeOccurredMM" type="text" style="width:25px" value="#timeOccurredMM#" maxlength="2" />
											</td>
											<td>
												<select name="timeOccurredTT">
													<option <cfif timeOccurredTT is "AM">selected</cfif> value="AM">AM</option>
													<option <cfif timeOccurredTT is "PM">selected</cfif> value="PM">PM</option>
												</select>
											</td>
											</tr>
									</table>
								</td>
							</tr>
						</table>
					</cfoutput>
					</td>
				</tr>
				<cfoutput>
				<tr>
					<td nowrap><b>Called By: </b></td>
					<td>
						<input name="calledBy" type="text" style="width:520px" value="#calledBy#" maxlength="100">
					</td>
				</tr>
				</cfoutput>
				<tr>
					<td nowrap><b>Department Calling: </b></td>
					<td>
						<select name="deptid" class="normal">
							<option value="0">Please Select</option>
							<cfoutput query="getDepts">
								<option <cfif getDepts.departmentid is deptid>selected</cfif> value="#getdepts.departmentid#">#department#</option>
							</cfoutput>
						</select>
					</td>
				</tr>
				<tr>
					<td><b>System Affected:</b></td>
					<td>
					<select name="systemID" class="normal">
						<option value="0">Please Select</option>
					<cfoutput query="getsystems">
						<cfif getsystems.system is not "OTHER">
							<option <cfif systemid is getsystems.sysid>selected</cfif> value="#getsystems.sysid#">#getsystems.system#</option>
						</cfif>
					</cfoutput>
					<cfoutput query="getsystems">
						<cfif getsystems.system is "OTHER">
							<option <cfif systemid is getsystems.sysid>selected</cfif> value="#getsystems.sysid#">#getsystems.system#</option>
						</cfif>
					</cfoutput>
					</select>
					</td>
				</tr>
				<cfoutput>
				<tr>
					<td colspan="2" nowrap><b>Issue/Problem/Question:</b></td>
					</tr>
				<tr>
					<td colspan="2">
						<textarea name="issue" rows="10" class="normal" style="width:715px">#issue#</textarea>
					</td>
				</tr>
				<tr>
					<td nowrap><b>Other Techs Involved: </b></td>
					<td>
						<input name="otherTechs" type="text" style="width:520px" value="#otherTechs#" maxlength="255">
					</td>
				</tr>
				<tr>
					<td colspan="2" nowrap><b>Resolution:</b></td>
				</tr>
				<tr>
					<td colspan="2">
						<textarea name="resolution" rows="10" class="normal" style="width:715px">#resolution#</textarea>
					</td>
				</tr>
				<tr>
					<td nowrap><b>Call back person spoken to:</b></td>
					<td>
						<input name="callBackPerson" type="text" style="width:520px" value="#callBackPerson#" maxlength="100" onfocus="this.value = this.form.calledBy.value;">
					</td>
				</tr>
				</cfoutput>
				<tr>
					<td nowrap><b>Date/Time Called Back: </b></td>
					<td style="padding:0px">
					<cfoutput>
							<table border="0" cellpadding="5" cellspacing="0" class="grey">
								<tr>
									<td>Date:</td>
									<td>
										<input name="dateCalledBack" type="text" style="width:75px" value="#dateCalledBack#" maxlength="10" />
									</td>
									<td> <a style="text-decoration:none;" href="javascript:showCal('DateCalledBack');"> <img src="../images/calicon.gif" alt="Select a Date" align="absmiddle" width="20" height="20" border="0" /> </a> </td>
									<td>Time:</td>
									<td class="nopadding">
										<table border="0" cellpadding="5" cellspacing="0" class="grey">
											<tr>
												<td class="nopadding">
													<input name="timeCalledBackHH" type="text" style="width:25px" value="#timeCalledBackHH#" maxlength="2" />
												</td>
												<td valign="middle">:</td>
												<td class="nopadding">
													<input name="timeCalledBackMM" type="text" style="width:25px" value="#timeCalledBackMM#" maxlength="2" />
												</td>
												<td>
													<select name="timeCalledBackTT">
														<option <cfif timeCalledBackTT is "AM">selected</cfif> value="AM">AM</option>
														<option <cfif timeCalledBackTT is "PM">selected</cfif> value="PM">PM</option>
													</select>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
					</cfoutput>
					</td>
				</tr>
				<tr>
					<td colspan="2"><b>Additional Notes:</b></td>
				</tr>
				<cfoutput>
				<tr>
					<td colspan="2">
						<textarea name="notes" rows="10" class="normal" style="width:715px">#notes#</textarea>
					</td>
				</tr>
				</cfoutput>
				<tr>
					<td colspan="2" class="nopadding">
						<table border="0" cellspacing="0" cellpadding="5">
							<tr>
								<td>
									<input name="btnSaveInformation" type="submit" class="sidebar" value="Save Information" style="width:125px; font-weight:bold;">
								</td>
								<td>
									<input name="btnCancel" type="submit" class="sidebar" value="Cancel" style="width:125px; font-weight:bold;">
								</td>
							</tr>
						</table>
					</td>
					</tr>
			</table>
		</td>
		</form>
	</tr>
</table>
<br>
<a style="text-decoration:underline" href="/index.cfm">Return to Intranet</a> | 
<a style="text-decoration:underline" href="index.cfm">Return to Call List</a> 
</div>
<br>
