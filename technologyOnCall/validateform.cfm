
<cfset isValid = true>
<cfset err = "">

<!--- validate form --->
<cfif trim(dateOccurred) is "">
	<cfset err = listappend(err, "The date occurred is required.")>
</cfif>
<cfif trim(timeOccurredHH) is "" or trim(timeOccurredMM) is "">
	<cfset err = listappend(err, "The time occurred is required.")>
</cfif>
<cfif not isDate(dateTimeOccurred)>
	<cfset err = listappend(err, "The date or time occured is invalid.")>
</cfif>
<cfif trim(calledBy) is "">
	<cfset err = listappend(err, "The person called by is required.")>
</cfif>
<cfif deptid is 0>
	<cfset err = listappend(err, "The department calling is required.")>
</cfif>
<cfif systemid is 0>
	<cfset err = listappend(err, "The system affected is required.")>
</cfif>
<cfif trim(issue) is "">
	<cfset err = listappend(err, "The issue is required.")>
</cfif>
<cfif trim(resolution) is "">
	<cfset err = listappend(err, "The resolution is required.")>
</cfif>
<cfif trim(dateCalledBack) is not "" or trim(timeCalledBackHH) is not "" or trim(timeCalledBackMM) is not "">
	<cfif not isDate(dateTimeCalledBack)>
		<cfset err = listappend(err, "The date or time called back is invalid.")>
	</cfif>
</cfif>

<cfif trim(err) is not "">
	<cfset isValid = false>
</cfif>

