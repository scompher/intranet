
<link rel="stylesheet" type="text/css" href="/styles.css">

<cfquery name="getDealers" datasource="#ds#">
	select * from dealer_login_tracking 
	order by dealerNumber asc 
</cfquery>

<div align="center">
<table width="600" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Dealer Login Tracking</b></td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
		<table width="100%" border="0" cellpadding="5" cellspacing="0">
			<tr>
				<td class="linedrow" colspan="4"><a style="text-decoration:underline;" href="add.cfm">Add New Dealer</a></td>
			</tr>
			<tr>
				<td class="linedrow" align="center" nowrap><b>Action</b></td>
				<td class="linedrow"><b>Dealer Number </b></td>
				<td class="linedrow" align="center"><b>Logged In Yet?</b> </td>
				<td class="linedrow" align="center"><b>Date/Time Logged In</b> </td>
			</tr>
			<cfoutput query="getDealers">
			<tr>
				<td align="center" nowrap>
				<a href="edit.cfm?tid=#trackingid#"><img border="0" src="/images/edit.gif" width="16" height="16"></a>
				&nbsp;&nbsp;
				<a onclick="return confirm('Are you sure you wish to remove this dealer?');" href="del.cfm?tid=#trackingid#"><img border="0" src="/images/delete.gif" width="16" height="16"></a>
				</td>
				<td>#dealerNumber#</td>
				<td align="center">
				<cfif loggedInYet is 0>NO<cfelse>YES</cfif>
				</td>
				<td align="center">
				#dateformat(dateTimeLoggedIn,'mm/dd/yyyy')# #timeformat(dateTimeLoggedIn,'hh:mm tt')#
				</td>
			</tr>
			</cfoutput>
		</table>
		</td>
	</tr>
</table>
<br />
<a class="normal" style="text-decoration:underline;" href="/index.cfm">Return to Intranet</a>
</div>
