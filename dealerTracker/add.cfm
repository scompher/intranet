
<link rel="stylesheet" type="text/css" href="/styles.css">

<cfparam name="msg" default="">
<cfparam name="dealerNumber" default="">

<cfif isDefined("form.btnAddDealer")>
	<cfif trim(msg) is "">
		<cfquery name="insertDealer" datasource="#ds#">
			insert into dealer_login_tracking (dealerNumber, loggedInYet) 
			values ('#form.dealerNumber#', 0) 
		</cfquery>
	</cfif>
	<cflocation url="index.cfm">
</cfif>

<div align="center">
<cfif trim(msg) is not "">
<b class="alert"><cfoutput>#msg#</cfoutput></b>
</cfif>
<br>
<br>
<table border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Add a dealer to track </b></td>
	</tr>
	<form method="post" action="add.cfm">
	<tr>
		<td class="greyrowbottomnopadding">
		<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td width="19%" nowrap><b>Dealer Number: </b></td>
				<td width="81%"><input name="dealerNumber" style="width:50px;" value="<cfoutput>#dealerNumber#</cfoutput>" type="text" maxlength="4"></td>
			</tr>
			<tr>
				<td colspan="2">
				<input name="btnAddDealer" type="submit" class="sidebar" value="Add">
				<input name="btnCancel" type="button" onclick="document.location='index.cfm';" class="sidebar" value="Cancel">
				</td>
			</tr>
		</table>
		</td>
	</tr>
	</form>
</table>
</div>
