
<cfsetting showdebugoutput="no">

<cfif isdefined("form.btnEditresolution")>

	<cfquery name="Updateresolution" datasource="#ds#">
		begin
			update inquiries_resolution
			set resolutionText = '#form.resolutionText#' 
			where resolutionid = #resolutionid#
		end
		begin
			select inquiryid from inquiries_resolution where resolutionid = #resolutionid#
		end
	</cfquery>


	<cfoutput>
	<script type="text/javascript">
		opener.location='openInquiry.cfm?i=#updateresolution.inquiryid#';
		self.close();
	</script>
	</cfoutput>

<cfelse>

	<cfquery name="getresolution" datasource="#ds#">
		select 
		inquiries_resolution.* 
		from inquiries_resolution 
		where resolutionid = #i#
	</cfquery>
	
	<link rel="stylesheet" type="text/css" href="../styles.css">
	
	<div align="center">
	<title>Edit Resolution</title>
	<body onLoad="this.focus();">
	<table width="475" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td class="highlightbar"><b>Edit Resolution Item</b></td>
		</tr>
		<form method="post" action="editresolution.cfm">
		<cfoutput>
		<input type="hidden" name="resolutionid" value="#i#">
		</cfoutput>
		<tr>
			<td class="greyrowbottom" align="center">
				<table border="0" cellpadding="5" cellspacing="0" class="grey">
					<tr>
						<td>
							<textarea name="resolutionText" rows="5" style="width:450px"><cfoutput>#getresolution.resolutionText#</cfoutput></textarea>
						</td>
					</tr>
					<tr>
						<td>
							<input name="btnEditresolution" type="submit" class="sidebar" value="Update Resolution">
						</td>
					</tr>
				</table>
			</td>
		</tr>
		</form>
	</table>
	</body>
	</div>

</cfif>
