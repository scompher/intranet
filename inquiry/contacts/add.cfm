
<link rel="stylesheet" type="text/css" href="../../styles.css">

<cfif isDefined("form.btnSave")>
	<!--- save contact info --->
	<cfquery name="SaveInfo" datasource="#ds#">
		insert into inquiries_contacts (firstname, lastname, extention, departmentid)
		values ('#form.firstname#', '#form.lastname#', '#form.extention#', #deptid#)
	</cfquery>

	<cflocation url="index.cfm">
</cfif>

<cfquery name="getDepts" datasource="#ds#">
	select * from Admin_Users_Departments
	order by department asc
</cfquery>

<div align="center">
<form method="post" action="add.cfm">
<table border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td colspan="2" align="center" class="highlightbar"><b>Add an inquiry contact</b></td>
	</tr>
	<tr>
		<td nowrap class="greyrowleft">First Name: </td>
		<td class="greyrowright">
			<input type="text" name="firstname" style="width:400px">
		</td>
	</tr>
	<tr>
		<td nowrap class="greyrowleft">Last Name: </td>
		<td class="greyrowright">
			<input type="text" name="lastname" style="width:400px">
		</td>
	</tr>
	<tr>
		<td nowrap class="greyrowleft">Extension:</td>
		<td class="greyrowright">
			<input type="text" name="extention" style="width:400px">
		</td>
	</tr>
	<tr>
		<td nowrap class="greyrowleft">Department:</td>
		<td class="greyrowright">
			<select name="deptid">
					<option value=""></option>
				<cfoutput query="getDepts">
					<option <cfif deptid is departmentid>selected</cfif> value="#departmentid#">#department#</option>
				</cfoutput>
			</select>
		</td>
	</tr>
	<tr>
		<td class="greyrowbottomleft">&nbsp;</td>
		<td class="greyrowbottomright">
			<input name="btnSave" type="submit" class="sidebar" value="Submit">
			<input type="button" class="sidebar" onClick="document.location='index.cfm';" value="Cancel">
		</td>
	</tr>
</table>
</div>
</form>
