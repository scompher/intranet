
<link rel="stylesheet" type="text/css" href="../../styles.css">

<cfif isDefined("form.btnSave")>
	<!--- save contact info --->
	<cfquery name="SaveInfo" datasource="#ds#">
		update inquiries_contacts
		set	firstname = '#form.firstname#', lastname = '#form.lastname#', extention = '#form.extention#', departmentid = #form.deptid#
		where contactid = #form.cid#
	</cfquery>

	<cflocation url="index.cfm">
</cfif>

<cfquery name="getDepts" datasource="#ds#">
	select * from Admin_Users_Departments
	order by department asc
</cfquery>

<cfquery name="getContact" datasource="#ds#">
	select * from inquiries_contacts
	where contactid = #cid#
</cfquery>

<div align="center">
<form method="post" action="edit.cfm">
<cfoutput>
<input type="hidden" name="cid" value="#cid#" />
</cfoutput>
<table border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td colspan="2" align="center" class="highlightbar"><b>Edit an inquiry contact</b></td>
	</tr>
	<cfoutput query="getcontact">
	<tr>
		<td nowrap class="greyrowleft">First Name: </td>
		<td class="greyrowright">
			<input type="text" name="firstname" style="width:400px" value="#firstname#">
		</td>
	</tr>
	<tr>
		<td nowrap class="greyrowleft">Last Name: </td>
		<td class="greyrowright">
			<input type="text" name="lastname" style="width:400px" value="#lastname#">
		</td>
	</tr>
	<tr>
		<td nowrap class="greyrowleft">Extension:</td>
		<td class="greyrowright">
			<input type="text" name="extention" style="width:400px" value="#extention#">
		</td>
	</tr>
	</cfoutput>
	<tr>
		<td nowrap class="greyrowleft">Department:</td>
		<td class="greyrowright">
			<select name="deptid">
					<option value=""></option>
				<cfoutput query="getDepts">
					<option <cfif getcontact.departmentid is getdepts.departmentid>selected</cfif> value="#departmentid#">#department#</option>
				</cfoutput>
			</select>
		</td>
	</tr>
	<tr>
		<td class="greyrowbottomleft">&nbsp;</td>
		<td class="greyrowbottomright">
			<input name="btnSave" type="submit" class="sidebar" value="Submit">
			<input type="button" class="sidebar" onClick="document.location='index.cfm';" value="Cancel">
		</td>
	</tr>
</table>
</div>
</form>
