
<link rel="stylesheet" type="text/css" href="../../styles.css">

<cfparam name="deptid" default="0">

<cfquery name="getDepts" datasource="#ds#">
	select * from Admin_Users_Departments
	order by department asc
</cfquery>

<cfquery name="getContacts" datasource="#ds#">
	select 
	inquiries_contacts.*, 
	Admin_Users_Departments.department 
	from inquiries_contacts 
	inner join Admin_Users_Departments on inquiries_contacts.departmentid = Admin_Users_Departments.departmentid
	<cfif deptid is not 0>
		where inquiries_contacts.departmentid = #deptid# 
	</cfif>
	order by inquiries_contacts.lastname, inquiries_contacts.firstname, Admin_Users_Departments.department
</cfquery>

<div align="center">
<form method="post" action="index.cfm">
<table width="600" border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td>
		<img align="absmiddle" border="0" src="../../images/edit.gif" alt="Edit Contact" width="16" height="16"> = Edit Item<br />
		<img align="absmiddle" border="0" src="../../images/delete.gif" alt="Delete Contact" width="16" height="16"> = Delete Item
		</td>
	</tr>
	<tr>
		<td align="center" class="highlightbar"><b>Administer Inquiry Contacts</b></td>
	</tr>
	<tr>
		<td class="greyrow">&nbsp;</td>
	</tr>
	<tr>
		<td class="greyrow" style="padding:0px">
			<table width="100" border="0" cellpadding="5" cellspacing="0" class="grey">
				<tr>
					<td nowrap>Select Department: </td>
					<td>
						<select name="deptid" onchange="this.form.submit();">
							<option <cfif deptid is 0>selected</cfif> value="0">All</option>
							<cfoutput query="getDepts">
								<option <cfif deptid is departmentid>selected</cfif> value="#departmentid#">#department#</option>
							</cfoutput>
						</select>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td class="greyrow" style="padding:0px">
			<table width="100%" border="0" cellpadding="5" cellspacing="0" class="grey">
				<tr>
					<td width="10%" align="center"><b>Action</b></td>
					<td width="45%"><b>Name</b></td>
					<td width="14%" align="center"><b>Extension</b></td>
					<td width="31%"><b>Department</b></td>
				</tr>
				<cfoutput query="getcontacts">
				<tr>
					<td align="center">
					<a href="edit.cfm?cid=#contactid#"><img border="0" src="../../images/edit.gif" alt="Edit Contact" width="16" height="16"></a>&nbsp;
					<a onclick="return confirm('Are you sure you wish to delete this contact?');" href="delete.cfm?cid=#contactid#"><img border="0" src="../../images/delete.gif" alt="Delete Contact" width="16" height="16"></a>
					</td>
					<td>#firstname# #lastname#</td>
					<td align="center">#extention#</td>
					<td>#department#</td>
				</tr>
				</cfoutput>
			</table>
		</td>
	</tr>
	<cfoutput>
	<tr>
		<td class="greyrowbottom"><input type="button" class="sidebar" onclick="document.location='add.cfm?deptid=#deptid#';" value="Add New Contact" />
		</td>
	</tr>
	</cfoutput>
</table>
</form>
<br />
<a class="normal" href="/index.cfm" style="text-decoration:underline">Return to Intranet Menu</a>
</div>
