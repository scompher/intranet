<link rel="stylesheet" type="text/css" href="../styles.css">

<cfif isDefined("form.btnChangeCat")>
	<cfquery name="updateCat" datasource="#ds#">
		update inquiries_main 
		set categoryid = #form.categoryid#, otherCategory = ''
		where inquiryid = #i#
	</cfquery>
	<script type="text/javascript">
	opener.location='openInquiry.cfm?i=<cfoutput>#i#</cfoutput>';
	self.close();
	</script>
</cfif>

<cfquery name="getInq" datasource="#ds#">
	select otherCategory from inquiries_main where inquiryid = #i#
</cfquery>

<cfquery name="getCats" datasource="#ds#">
	select * from inquiries_categories
	order by category asc
</cfquery>

<table border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Change Category</b> </td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
			<table border="0" cellspacing="0" cellpadding="5">
				<form method="post" action="changeCat.cfm">
				<cfoutput>
				<input type="hidden" name="i" value="#i#">
				</cfoutput>
				<tr>
					<td><b>Existing Category: </b></td>
					<td colspan="2" nowrap><cfoutput>#getInq.otherCategory#</cfoutput></td>
				</tr>
				<tr>
					<td><b>New Category </b></td>
					<td nowrap>
						<select name="categoryid" class="smalltable">
							<option value="0">Select Category</option>
							<cfoutput query="getCats">
								<option value="#categoryid#">#category#</option>
							</cfoutput>
						</select>
					</td>
					<td>
						<input name="btnChangeCat" type="submit" class="sidebarsmall" value="Apply">
					</td>
				</tr>
				</form>
			</table>
		</td>
	</tr>
</table>
