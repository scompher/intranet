<!--- 
10/28/2015 : PG : Changed overdue time from 24 hours to 72 hours per Sarah Brooks
--->
<cfquery name="getInquiries" datasource="#ds#">
	select inquiryid, dateTimeCreated  
	from inquiries_main 
	order by inquiryid asc 
</cfquery>

<cfset counter = 1>
<cfloop query="getInquiries">
	
	<cfset today = dateTimeCreated>
	
	<cfset exceptiondays = "1,6,7">
	<cfset dow = dayofweek(today)>
	<cfif listfind(exceptiondays,dow) is not 0>
		<cfswitch expression="#dow#">
			<cfcase value="6"><cfset dateTimeOverdue = dateadd("d",3,today)></cfcase>
			<cfcase value="7"><cfset dateTimeOverdue = dateadd("d",2,today)></cfcase>
			<cfcase value="1"><cfset dateTimeOverdue = dateadd("d",1,today)></cfcase>
		</cfswitch>
		<cfset dateTimeOverdue = createodbcdatetime("#dateformat(dateTimeOverdue,'mm/dd/yyyy')# 17:00:00")>
	<cfelse>
		<cfset dateTimeOverdue = dateadd("h",72,today)>
	</cfif>
	
	<cfquery name="updateInq" datasource="#ds#">
		update inquiries_main
		set dateTimeOverdue = #variables.dateTimeOverdue# 
		where inquiryid = #inquiryid# 
	</cfquery>

	<cfset counter = counter + 1>
</cfloop>

<cfoutput>done, #counter# items updated</cfoutput>
