
<cfparam name="form.btnForwardInquiry" default="0">

<script type="text/javascript">
function viewPrintWin(u) {
	window.open(u,"inqWin","width=600,height=500,scrollbars=1");
}
</script>

<cfif form.btnForwardInquiry is not 0>

	<!--- check # of forwards, if 2 or greater, disallow forward --->
	<cfquery name="checkforwards" datasource="#ds#">
		select count(inquiryid) as numforwards
		from inquiries_status_history
		where inquiryid = #form.inquiryid# and statusid = 3
	</cfquery>

	<cfif checkforwards.numforwards gte 2>
	
		<link rel="stylesheet" type="text/css" href="../styles.css">
		<div align="center">
		<table width="500" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td colspan="2" nowrap class="highlightbar" align="center"><b>Inquiry Forwarded</b></td>
			</tr>
			<tr>
				<td colspan="2" class="greyrow" align="center">
				<cfoutput>
				Sorry, the maximum number of fowards on this inquiry has been reached, please <a style="text-decoration:underline" href="javascript:viewPrintWin('viewInquiry.cfm?i=#form.inquiryid#');">print this inquiry out</a> and handle manually. 
				</cfoutput>
				</td>
			</tr>
			<tr>
				<td colspan="2" class="greyrowbottom" align="center">
				<a style="text-decoration:underline" href="/index.cfm">Click here to return to Intranet Menu</a>
				</td>
			</tr>
		</table>	
		</div>
		<cfabort>

	<cfelse>
	
		<cfset today = createodbcdatetime(now())>
		<cftransaction>
			<cfquery name="updateStatus" datasource="#ds#">
				declare @fromDeptID as int
				set @fromDeptID = (select toDepartmentID from inquiries_status_history where inquiryid = #form.inquiryid# and isCurrentStatus = 1)
				begin
					insert into inquiries_status_history (inquiryid, dateTimeUpdated, statusid, adminuserid, isCurrentStatus, fromDepartmentID, toDepartmentID)
					select inquiryid, dateTimeUpdated, statusid, #cookie.adminlogin#, isCurrentStatus, fromDepartmentID, toDepartmentID
					from inquiries_status_history
					where inquiries_status_history.inquiryid = #form.inquiryid# and isCurrentStatus = 1
				end
				begin
					update inquiries_status_history
					set	isCurrentStatus = 0 
					where inquiryid = #form.inquiryid#
				end
				begin
					update inquiries_status_history
					set dateTimeUpdated = #today#, statusid = 3, fromDepartmentid = @fromDeptID, toDepartmentid = #form.forDeptID#, isCurrentStatus = 1, adminuserid = #cookie.adminlogin#, isRead = 0, forwardedNote = '#forwardedNote#'
					where inquiryid = #form.inquiryid# and statusHistoryID = (select max(statusHistoryID) from inquiries_status_history where inquiryid = #form.inquiryid#)
				end
			</cfquery>
		</cftransaction>
	
		<cfquery name="GetContact" datasource="#ds#">
			select * from inquiries_contacts
			where departmentid = #forDeptID#
		</cfquery>
		<cfquery name="Getinqnum" datasource="#ds#">
			select inquirynumber from inquiries_main
			where inquiryid = #form.inquiryid#
		</cfquery>
		<link rel="stylesheet" type="text/css" href="../styles.css">
		<div align="center">
		<table width="500" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td colspan="2" nowrap class="highlightbar" align="center"><b>Inquiry Forwarded</b></td>
			</tr>
			<tr>
				<td colspan="2" class="greyrow" align="center">
				<cfoutput>
				Inquiry ###getinqnum.inquirynumber# has been forwarded.<br>
				<br>
				Please contact #getcontact.firstname# #getcontact.lastname# at extension #getcontact.extention# to notify of your inquiry.
				</cfoutput>
				</td>
			</tr>
			<tr>
				<td colspan="2" class="greyrowbottom" align="center">
				<a style="text-decoration:underline" href="index.cfm">Click here to return to Inquiry Menu</a>
				</td>
			</tr>
		</table>	
		</div>
		<cfabort>
	
	</cfif>

</cfif>

<cfquery name="getDepartments" datasource="#ds#">
	select * 
	from admin_users_departments
	order by department asc
</cfquery>

<script language="JavaScript" type="text/javascript">
	function checkForm(frm) {
		if (frm.forDeptID.selectedIndex == 0) {alert('Please select the department this inquiry is being sent to.'); frm.forDeptID.focus(); return false;}
		if (frm.forwardedNote.value == "") {alert('Please provide the reason you are forwarding this inquiry.'); frm.forwardedNote.focus(); return false;}
		
		frm.btnForwardInquiry.value = 1;
		frm.submit();
	}
</script>

<form method="post" action="forwardInquiry.cfm">
<cfoutput>
<input type="hidden" name="inquiryid" value="#i#" />
</cfoutput>
<table width="500" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center" class="highlightbar">Forward Inquiry</td>
	</tr>
	<tr>
		<td class="greyrow">&nbsp;</td>
	</tr>
	<tr>
		<td align="center" class="greyrow">Please select which department you wish to forward this inquiry to: </td>
	</tr>
	<tr>
		<td class="greyrow">&nbsp;</td>
	</tr>
	<tr>
		<td align="center" class="greyrow">
			<table border="0" cellpadding="5" cellspacing="0" class="grey">
				<tr>
					<td>Department:</td>
					<td>
					<select name="forDeptID">
						<option value="0">Select Department</option>
						<cfoutput query="getDepartments">
							<option value="#getDepartments.departmentID#">#getDepartments.department#</option>
						</cfoutput>
					</select>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td align="left" class="greyrow">Reason for forwarding: </td>
	</tr>
	<tr>
		<td align="left" class="greyrow">
		<textarea name="forwardedNote" rows="3" style="width:490px;"></textarea>
		</td>
	</tr>
	<tr>
		<td class="greyrow">&nbsp;</td>
	</tr>
	<tr>
		<td align="center" class="greyrow">
			<input type="hidden" name="btnForwardInquiry" value="0" />
			<input type="button" onclick="checkForm(this.form);" class="lightbutton" value="Forward Inquiry">
		</td>
	</tr>
	
	<tr>
		<td class="greyrowbottom">&nbsp;</td>
	</tr>
</table>
</form>

