
<link href="../styles.css" rel="stylesheet" type="text/css">

<cfif not isdefined("cookie.enablePopUpText")>
	<cfcookie name="enablePopupText" expires="never" value="1">
<cfelse>
	<cfif isdefined("form.popupsubmitted")>
		<cfparam name="form.enablepopuptext" default="0">	
		<cfcookie name="enablePopupText" expires="never" value="#form.enablePopUpText#">
	</cfif>
</cfif>

<script language="javascript" src="cal2.js"></script>
<script language="javascript" src="cal_conf2.js"></script>
<script language="javascript" src="functions.js"></script>

<script language="javascript" src="main.js" type="text/javascript"></script> 
<div id="TipLayer" style="visibility:hidden;position:absolute;z-index:1000;top:-100;"></div>

<script language="javascript" src="style.js" type="text/javascript"></script>  

<style type="text/css">
#dropmenudiv{
position:absolute;
border:1px solid black;
border-bottom-width: 0;
font:normal 12px Verdana;
line-height:18px;
z-index:100;
}

#dropmenudiv a{
width: 100%;
display: block;
text-indent: 3px;
border-bottom: 1px solid black;
padding: 1px 0;
text-decoration: none;
font-weight: normal;
}

#dropmenudiv a:hover{ /*hover background color*/
background-color: #C7CFD9;
}
</style>

<script language="javascript" type="text/javascript">
var menuwidth='165px' //default menu width
var menubgcolor='#AFBBC9'  //menu bgcolor
var disappeardelay=1000  //menu disappear speed onMouseout (in miliseconds)
var hidemenu_onclick="yes" //hide menu when user clicks within menu?

function pickUpInquiry(inqId) {
	if (confirm('Confirm pick-up of this inquiry')) {
		document.location='openInquiry.cfm?i=' + inqId + '&pickup=yes';
	}
}

function reOpen(inqId) {
	if (confirm('Are you sure you wish to re-open this inquiry?')) {
		document.location='reopen.cfm?i=' + inqId;
	}
}

function viewPrintWin(u) {
	wleft = (screen.width - 600) / 2;
	wtop = (screen.height - 500) / 2;
	window.open(u,"inqWin","width=600,height=500,scrollbars=1,left=" + wleft + ",top=" + wtop);
}
</script>

<cfparam name="catid" default="0">
<cfparam name="form.inqNum" default="">
<cfparam name="form.dealerNum" default="">
<cfparam name="form.rec" default="">
<cfparam name="form.account" default="">
<cfparam name="form.keywords" default="">
<cfparam name="idlist" default="0">
<cfparam name="form.disp" default="">
<cfparam name="form.fs" default="">
<cfparam name="form.fe" default="">
<cfparam name="foundedOn" default="0">

<cfquery name="getCats" datasource="#ds#">
	select * from inquiries_categories
	order by category asc
</cfquery>

<cfquery name="getDepts" datasource="#ds#">
	select * from Admin_Users_Departments
	order by department asc
</cfquery>

<cfquery name="GetStatus" datasource="#ds#">
	select * from inquiries_status
	order by status asc
</cfquery>

<cfquery name="GetDisposition" datasource="#ds#">
	select * from inquiries_inquiryIsOptions
	order by inquiryIsOrder asc
</cfquery>

<cfquery name="GetStatusButClosed" datasource="#ds#">
	select * from inquiries_status
	where statusid <> 4 and statusid <> 5
	order by status asc
</cfquery>

<cfif not isdefined("s") or s is "">
	<cfset s = valuelist(GetStatusButClosed.statusid)>
</cfif>

<cfif not isdefined("deptlist") or deptlist is "">
	<cfset deptlist = departmentlist>
</cfif>

<cfif trim(form.keywords) is not "">
	<cfsearch name = "getResults" collection = "intranet_inquiries" criteria = "#form.keywords#">
	<cfif getResults.recordcount is 0>
		<cfset idlist = 0>
	<cfelse>
		<cfset idlist = valuelist(getResults.key)>
	</cfif>
</cfif>

<cfquery name="getInqs" datasource="#ds#">
	select 
	inquiries_main.inquiryID, 
	inquiries_main.inquiryNumber, 
	inquiries_main.dealerNumber, 
	inquiries_main.rec + '-' + inquiries_main.acct as accountNumber, 
	inquiries_main.salesFollowUp, 
	inquiries_main.dateTimeCreated, 
	inquiries_main.dateTimeOverdue, 
	inquiries_main.reviewed, 
	inquiries_main.otherCategory, 
	inquiries_main.inquiryText, 
	inquiries_attachments.savedFile, 
	inquiries_status.status, 
	inquiries_status.statusid, 
	inquiries_categories.category, 
	statusUsers.firstname as statusFname, 
	statusUsers.lastname as statusLname, 
	inquiries_status_history.dateTimeUpdated, 
	inquiries_status_history.isRead, 
	inquiries_status_history.adminuserid as statusUserID, 
	fromDepts.department as fromDepartment, 
	toDepts.department as toDepartment, 
	inquiries_inquiryIsOptions.inquiryIs
	from inquiries_main 
	left join inquiries_attachments on inquiries_main.inquiryID = inquiries_attachments.inquiryid 
	left join inquiries_status_history on inquiries_main.inquiryid = inquiries_status_history.inquiryid  
	left join inquiries_categories on inquiries_main.categoryid = inquiries_categories.categoryid
	left join inquiries_status on inquiries_status_history.statusid = inquiries_status.statusid
	left join admin_users as statusUsers on inquiries_status_history.adminuserid = statusUsers.adminuserid
	left join inquiries_inquiryIsOptions on inquiries_main.inquiryIsID = inquiries_inquiryIsOptions.inquiryIsID
	left join admin_users_departments as fromDepts on inquiries_status_history.fromDepartmentID = fromDepts.departmentid
	left join admin_users_departments as toDepts on inquiries_status_history.toDepartmentID = toDepts.departmentid
	where 
	inquiries_status_history.isCurrentStatus = 1 and inquiries_main.active = 1 and 
	<cfif isdefined("form.btnSearch")>
		<cfif trim(form.inqNum) IS NOT "">inquiryNumber like '%#trim(form.inqNum)#%' and </cfif>
		<cfif trim(form.dealerNum) IS NOT "">dealerNumber like '%#trim(form.dealerNum)#%' and </cfif>
		<cfif trim(form.rec) IS NOT "">rec like '%#trim(form.rec)#%' and </cfif>
		<cfif trim(form.account) IS NOT "">acct like '%#trim(form.account)#%' and </cfif>
		<cfif trim(form.keywords) is not "">inquiries_main.inquiryid IN (#idlist#) and </cfif>
	<cfelse>
		<cfif foundedOn is not 0>
			inquiryIsDepartmentID in (#foundedOn#) and 
		</cfif>
		<cfif deptlist is not 0>
			inquiries_status_history.toDepartmentID IN (#deptlist#) and 
		</cfif>
		<cfif s is not "">
			inquiries_status_history.statusid IN (#s#) and 
		</cfif>
		<cfif catid is not 0>
			inquiries_main.categoryid = #catid# and 
		</cfif>
		<cfif disp is not "">
			inquiries_main.inquiryIsID IN (#disp#) and 
		</cfif>
	</cfif>
	(
	<cfif trim(fs) is not "">
		inquiries_main.dateTimeCreated >= '#fs#' 
		<cfif trim(fe) is not "">
		and inquiries_main.dateTimeCreated < '#dateformat(dateadd("d",1,fe),'mm/dd/yyyy')#' 
		</cfif>
	<cfelseif trim(fe) is not "">
		inquiries_main.dateTimeCreated >= '01/01/2012' and inquiries_main.dateTimeCreated < '#dateformat(dateadd("d",1,fe),'mm/dd/yyyy')#' 
	<cfelse>
		inquiries_main.dateTimeCreated >= '01/01/2012'
	</cfif>
	) and 
	1=1 
	order by inquiries_main.inquiryNumber asc
</cfquery>

<div align="center">
<table width="725" border="0" cellspacing="0" cellpadding="5">
	<!--- 
	<cfif listfind("1,85,94",cookie.adminlogin) is not 0>
		<tr>
			<td align="center"><img src="/images/hotpotato.png" /></td>
		</tr>
	<cfelse>
		<tr>
			<td align="center"><b>Inquiry Pick-Up</b></td>
		</tr>
	</cfif>
	--->
	<tr>
		<td align="center"><b>Inquiry Pick-Up</b></td>
	</tr>
	<tr>
		<td style="padding:0px">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td colspan="2">Legend:</td>
					</tr>
				<tr>
					<td width="30%" valign="top">
						<table border="0" cellspacing="0" cellpadding="5">
							<tr>
								<td><img src="../images/unread.gif" alt="Inquiry Unread" width="15" height="15" /></td>
								<td>Unopened </td>
							</tr>
							<tr>
								<td><img src="../images/read.gif" alt="Inquiry Picked Up" width="15" height="15" /></td>
								<td> Opened and picked-up </td>
							</tr>
							<tr>
								<td><img src="../images/forwarded.gif" alt="Inquiry Forwarded" width="15" height="15" /></td>
								<td> Forwarded and picked-up </td>
							</tr>
							<tr>
								<td><img src="../images/forwarded-unread.gif" alt="Inquiry Forwarded and Not picked up" width="15" height="15" /></td>
								<td> Forwarded and unopened </td>
							</tr>
						</table>
					</td>
					<td width="70%" valign="top">
						<table border="0" cellspacing="0" cellpadding="5">
							
							<tr>
								<td><img src="/images/followUp.gif" alt="Flagged for Sales Follow Up" width="15" height="15" /></td>
								<td> Flagged for sales follow up </td>
							</tr>
							<tr>
								<td><img src="/images/attachment.gif" alt="Attachment" width="15" height="15" /></td>
								<td> Inquiry has attachments </td>
							</tr>
							<tr>
								<td><img alt="Overdue Item" src="/images/overdue.gif" /></td>
								<td>Overdue Item</td>
							</tr>
						</table>
					</td>
				</tr>
				<form method="post" action="index.cfm" name="filterform">
				<tr>
					<td colspan="2" valign="top" style="padding:0px">
						<br />
						<table width="100%" border="0" cellspacing="0" cellpadding="5">
							<tr>
								<td width="14%" nowrap="nowrap">Filter by Date Range:</td>
								<td width="86%" class="nopadding">
									<table border="0" cellspacing="0" cellpadding="3">
										<tr>
											<td>
											Starting:
											</td>
											<td>
											<input name="fs" type="text" style="width:75px" value="<cfoutput>#fs#"</cfoutput> maxlength="10" />
											</td>
											<td>
											<a style="text-decoration:none;" href="javascript:showCal('filterStartDate');"> <img src="../images/calicon.gif" alt="Select a Date" align="absmiddle" width="20" height="20" border="0" /> </a>
											</td>
											<td>&nbsp;</td>
											<td>
											Ending:
											</td>
											<td>
											<input name="fe" type="text" style="width:75px" value="<cfoutput>#fe#</cfoutput>" maxlength="10" />
											</td>
											<td>
											<a style="text-decoration:none;" href="javascript:showCal('filterEndDate');"> <img src="../images/calicon.gif" alt="Select a Date" align="absmiddle" width="20" height="20" border="0" /> </a>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td width="14%" nowrap="nowrap">Filter by Status:</td>
								<td width="86%" class="nopadding">
									<table width="24%" border="0" cellspacing="0" cellpadding="3">
										<tr>
										<cfoutput query="getStatus">
											<td width="5%" valign="middle" nowrap="nowrap">
												<input <cfif (listfind(s,statusid) is not 0)>checked</cfif> type="checkbox" name="s" value="#getstatus.statusid#" />
											</td>
											<td width="95%" valign="middle" nowrap="nowrap">
											<cfif getstatus.status is "Created">Unopened<cfelse>#getStatus.status#</cfif>
											</td>
											<td width="10" valign="middle" nowrap="nowrap">&nbsp;</td>
										</cfoutput>
										</tr>
									</table>
								</td>
								</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2" valign="top" style="padding:0px">
						<table width="100%" border="0" cellspacing="0" cellpadding="5">
							<tr>
								<td width="14%" nowrap="nowrap">Filter by Disposition:</td>
								<td width="86%" class="nopadding">
									<table width="24%" border="0" cellspacing="0" cellpadding="3">
										<tr>
										<cfoutput query="GetDisposition">
											<td width="5%" valign="middle" nowrap="nowrap">
												<input <cfif (listfind(disp,inquiryIsID) is not 0)>checked</cfif> type="checkbox" name="disp" value="#getDisposition.inquiryIsID#" />
											</td>
											<td width="95%" valign="middle" nowrap="nowrap">
											#GetDisposition.inquiryIs#
											</td>
											<td width="10" valign="middle" nowrap="nowrap">&nbsp;</td>
										</cfoutput>
										</tr>
									</table>
								</td>
								</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2" valign="top" style="padding:0px">
						<table border="0" cellspacing="0" cellpadding="5">
							<tr>
								<td nowrap="nowrap">Filter by Founded On: </td>
								<td>
									<select name="foundedOn">
										<option value="0" <cfif foundedOn is 0>selected</cfif> >All</option>
										<cfoutput query="getdepts">
											<option <cfif getdepts.departmentid is foundedOn>selected</cfif> value="#departmentid#">#department#</option>
										</cfoutput>
									</select>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2" valign="top" style="padding:0px">
						<table border="0" cellspacing="0" cellpadding="5">
							<tr>
								<td nowrap="nowrap">Filter by Category: </td>
								<td colspan="2">
									<select name="catid">
										<option value="0" <cfif catid is 0>selected</cfif> >All</option>
										<cfoutput query="getcats">
											<option <cfif getcats.categoryid is catid>selected</cfif> value="#categoryid#">#category#</option>
										</cfoutput>
									</select>
								</td>
							</tr>
							<tr>
								<td nowrap="nowrap">Filter by Department: </td>
								<td>
									<select name="deptlist">
										<option value="">My Department(s)</option>
										<option value="0" <cfif deptlist is 0>selected</cfif> >All</option>
										<cfoutput query="getdepts">
											<option <cfif getdepts.departmentid is deptlist>selected</cfif> value="#departmentid#">#department#</option>
										</cfoutput>
									</select>
								</td>
								<td>
									<input type="submit" class="sidebar" value="Apply Filters" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2" valign="top">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="2" valign="top" class="nopadding">
						<table border="0" cellspacing="0" cellpadding="5">
							<tr>
								<td>Search by Inquiry Number: </td>
								<td align="left">
									<input name="inqNum" type="text" size="10" maxlength="10" value="<cfoutput>#form.inqNum#</cfoutput>" />
								</td>
							</tr>
							<tr>
								<td>Search by Dealer Number: </td>
								<td align="left">
									<input name="dealerNum" type="text" size="10" maxlength="10" value="<cfoutput>#form.dealerNum#</cfoutput>" />
								</td>
							</tr>
							<tr>
								<td>Search by Account Number: </td>
								<td align="left" class="nopadding">
									<table border="0" cellspacing="0" cellpadding="5">
										<tr>
											<td style="padding-right:0px">
												<input onkeyup="if (this.value.length == 4) this.form.account.focus();" type="text" style="width:45px" name="rec" maxlength="4" value="<cfoutput>#form.rec#</cfoutput>" />
											</td>
											<td>-</td>
											<td style="padding-left:0px"><input type="text" name="account" maxlength="5" style="width:50px" value="<cfoutput>#form.account#</cfoutput>"></td>
										</tr>
									</table>
								</td>
							</tr>
							<!---
							<tr>
								<td>Search by Keywords: </td>
								<td>
									<input type="text" name="keywords" style="width:250px" value="<cfoutput>#form.keywords#</cfoutput>" />
								</td>
							</tr>
							--->
							<tr>
								<td>&nbsp;</td>
								<td>
									<input name="btnSearch" type="submit" class="sidebar" id="btnSearch" value="Search Inquiries" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</form>
			</table>
		</td>
	</tr>
</table>
<br />
<table width="725" border="0" cellpadding="3" cellspacing="0" class="smalltable">
	<tr>
		<td colspan="10" align="left" nowrap class="nopadding">
		<form method="post" action="index.cfm">
		<input type="hidden" name="popupsubmitted" value="1" />
		<table border="0" cellspacing="0" cellpadding="3">
			<tr>
				<td>
					<cfif cookie.enablepopuptext is 1><cfset val = 0><cfelse><cfset val = 1></cfif>
					<input onclick="this.value=<cfoutput>#val#</cfoutput>; this.form.submit();" type="checkbox" name="enablePopupText" <cfif cookie.enablePopUpText is 1>checked</cfif> />
				</td>
				<td>Enable popup text </td>
				<td>&nbsp;</td>
				<td>
				<cfoutput>
				<input type="button" name="btnPrintAll" value="Print These Inquiries" class="sidebar" style="padding:2px 2px 2px 2px" onclick="viewPrintWin('printAll.cfm?iList=#valuelist(getInqs.inquiryID)#');" />
				</cfoutput>
				</td>
			</tr>
		</table>
		</form>
		</td>
	</tr>
	<tr>
		<td align="left" nowrap class="horizontalnavbuttons" style="border-left-width:0px">&nbsp;</td>
		<td width="70" align="center" nowrap class="horizontalnavbuttons">Inquiry #</td>
		<td nowrap class="horizontalnavbuttons">Dealer #</td>
		<td nowrap class="horizontalnavbuttons">Acct #</td>
		<td nowrap class="horizontalnavbuttons">From</td>
		<td nowrap class="horizontalnavbuttons">To</td>
		<td nowrap class="horizontalnavbuttons">Created</td>
		<td nowrap class="horizontalnavbuttons">Status</td>
		<td nowrap class="horizontalnavbuttons">Category</td>
		<td nowrap class="horizontalnavbuttons" style="border-right-width:0px">Conclusion</td>
	</tr>
	<cfset counter = 0>
	<cfoutput query="getInqs" group="inquiryid">
	<cfset counter = counter + 1>
	<cfif counter mod 2 is not 0>
		<cfset bgcolor="EEEEEE">
	<cfelse>
		<cfset bgcolor="FFFFFF">
	</cfif>
	<cfset count = evaluate(getInqs.currentrow)>
	<script language="javascript" type="text/javascript">
		//Contents for menu
		var menu#count#=new Array()
		<!--- <cfif statusid is "1" or statusid is 3> --->
		<cfif isRead is 0>
			menu#count#[0]='<a href="javascript: pickUpInquiry(#inquiryid#);">Pick Up Inquiry</a>';
		<cfelse> 
			menu#count#[0]='<a href="openInquiry.cfm?i=#inquiryid#">Open Inquiry</a>';
		</cfif>
			menu#count#[1]='<a href="javascript:viewPrintWin(\'viewInquiry.cfm?i=#inquiryid#\');">View/Print Inquiry</a>';
		<cfif (getsec.seclevelid is 1) and (statusid is 4 or statusid is 5)>
			menu#count#[2]='<a href="javascript:reOpen(#inquiryid#);">Re-Open Inquiry</a>';
		</cfif>
	</script>
	<cfset itext = inquirytext>
	<cfset itext = replace(itext, chr(34), "", "all")>
	<cfset itext = replace(itext, "'", "", "all")>
	<cfset itext = replace(itext, chr(13), "", "all")>
	<cfset itext = replace(itext, chr(10), "", "all")>
	<script language="javascript" type="text/javascript">
		Text[#counter#]=["","#iText#"]
	</script>
	<cfif getinqs.reviewed is 0 and (statusid is 4 or statusid is 5)>
		<cfset boldon = true><cfelse><cfset boldon = false></cfif>
	<cfif cookie.enablepopuptext is 1>
	<a href="##" onmouseover="stm(Text[#counter#],Style[0])" onmouseout="htm()">
	</cfif>
	<tr <cfif boldon>style="font-weight:bold;"</cfif> onclick="return dropdownmenu(this, event, menu#count#, '150px');" onmouseover="this.style.backgroundColor='##DDDDDD'; table#getInqs.currentRow#.style.backgroundColor='##DDDDDD';" onmouseout="this.style.backgroundColor='##FFFFFF'; table#getInqs.currentRow#.style.backgroundColor='##FFFFFF';">
		<td align="left" nowrap style="padding:0px">
		<table width="100%" border="0" cellspacing="0" cellpadding="1" id="table#getInqs.currentRow#" <cfif boldon>style="font-weight:bold;"</cfif>>
			<tr>			
				<td>#counter#. </td>
				<td align="center" valign="middle" width="15">
				<cfif dateTimeOverdue lt now() and statusid is not 4 and statusid is not 5>
					<img alt="Overdue Item" src="/images/overdue.gif" align="absmiddle" height="15" width="15" />
				<cfelse>
					<img src="/images/spacer.gif" height="15" width="15" />
				</cfif>				
				</td>
				<td align="center" valign="middle" width="42">
				<!--- show proper read/unread/forwarded message icons --->
				<cfif isRead is 1 and statusid is 3>
					<img src="../images/forwarded.gif" alt="Inquiry Forwarded" width="15" height="15" align="absmiddle">
				<cfelseif isRead is 0 and statusid is 3>
					<img src="../images/forwarded-unread.gif" alt="Inquiry Forwarded and Not picked up" width="15" height="15" align="absmiddle">
				<cfelseif statusid is 4 or statusid is 5>
					<img src="../images/closed.gif" alt="Inquiry Closed" width="42" height="15" align="absmiddle">
				<cfelseif isRead is 1>
					<img src="../images/read.gif" alt="Inquiry Picked Up" width="15" height="15" align="absmiddle">
				<cfelse>
					<img src="../images/unread.gif" alt="Inquiry Unread" width="15" height="15" align="absmiddle">
				</cfif>
				</td>
				<td align="center" width="15" valign="middle"><cfif salesFollowUp is not 0><img src="/images/followUp.gif" alt="Flagged for Sales Follow Up" width="15" height="15" /><cfelse><img src="/images/spacer.gif" height="15" width="15" /></cfif></td>
				<td align="center" width="15" valign="middle"><cfif savedFile is not ""><img src="/images/attachment.gif" alt="Attachment" width="15" height="15" /><cfelse><img src="/images/spacer.gif" height="15" width="15" /></cfif></td>
			</tr>
		</table>
		</td>
		<td align="center" nowrap>#inquiryNumber#</td>
		<td nowrap align="center">#dealerNumber#</td>
		<td nowrap align="center">#accountNumber#</td>
		<td nowrap>#fromDepartment#</td>
		<td nowrap>#toDepartment#</td>
		<td nowrap="nowrap">#dateformat(dateTimeCreated, 'mm/dd/yyyy')#</td>
		<td nowrap>
		<cfif statusid is 3>
		#status# to #toDepartment# 
		<cfelse>
		#status#
		</cfif>
		by 
		<cfif statusUserID is 0>
			System 
		<cfelse>
			#statusFname# #statusLname# 
		</cfif>
		on #dateformat(dateTimeUpdated, 'mm/dd/yyyy')# #timeformat(dateTimeUpdated, 'hh:mm tt')#
		</td>
		<td nowrap>
		<cfif category is not "other">
			#category#
		<cfelse>
			#otherCategory#
		</cfif>
		</td>
		<td nowrap>#inquiryIs#</td>
	</tr>
	<cfif cookie.enablepopuptext is 1>
	</a>
	</cfif>
	</cfoutput>
</table>

<br />
<a class="normal" href="/index.cfm" style="text-decoration:underline">Return to Intranet Menu</a>
</div>

<script language="javascript" type="text/javascript" src="dropdown.js"></script>
