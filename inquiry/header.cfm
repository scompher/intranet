
<link rel="stylesheet" type="text/css" href="../styles.css">

<cfparam name="contactAttemptIDList" default="">
<cfparam name="i" default="0">

<cfquery name="checkInq" datasource="#ds#">
	select * from inquiries_main with (nolock) where inquiryid = #i#
</cfquery>

<cfif (listfind(contactAttemptIDList,2) is not 0 or listfind(contactAttemptIDList,3) is not 0 or listfind(contactAttemptIDList,4) is not 0) or (checkInq.personInquiringIs is "Employee (in-house)")>
	<cfset allowClose = true>
<cfelse>
	<cfset allowClose = false>
</cfif>

<cfoutput>
<script language="JavaScript" type="text/javascript">
function addResearch() {
	document.mainform.addResearch.value = 1;
	document.mainform.addResolution.value = 0;
	document.mainform.addConclusion.value = 0;
	document.mainform.forwardInquiry.value = 0;
	document.mainform.closeInquiry.value = 0;
	document.mainform.addContactAttempt.value = 0;
	document.mainform.submit();
}
function addResolution() {
	document.mainform.addResearch.value = 0;
	document.mainform.addResolution.value = 1;
	document.mainform.addConclusion.value = 0;
	document.mainform.forwardInquiry.value = 0;
	document.mainform.closeInquiry.value = 0;
	document.mainform.addContactAttempt.value = 0;
	document.mainform.submit();
}
function addConclusion() {
	document.mainform.addResearch.value = 0;
	document.mainform.addResolution.value = 0;
	document.mainform.addConclusion.value = 1;
	document.mainform.forwardInquiry.value = 0;
	document.mainform.closeInquiry.value = 0;
	document.mainform.addContactAttempt.value = 0;
	document.mainform.submit();
}
function forwardInquiry() {
	document.mainform.addResearch.value = 0;
	document.mainform.addResolution.value = 0;
	document.mainform.addConclusion.value = 0;
	document.mainform.forwardInquiry.value = 1;
	document.mainform.closeInquiry.value = 0;
	document.mainform.addContactAttempt.value = 0;
	document.mainform.submit();
}
function closeInquiry() {
	document.mainform.addResearch.value = 0;
	document.mainform.addResolution.value = 0;
	document.mainform.addConclusion.value = 0;
	document.mainform.forwardInquiry.value = 0;
	document.mainform.closeInquiry.value = 1;
	document.mainform.addContactAttempt.value = 0;
	document.mainform.submit();
}
function addContactAttempt() {
	document.mainform.addResearch.value = 0;
	document.mainform.addResolution.value = 0;
	document.mainform.addConclusion.value = 0;
	document.mainform.forwardInquiry.value = 0;
	document.mainform.closeInquiry.value = 0;
	document.mainform.addContactAttempt.value = 1;
	document.mainform.submit();
}
<cfif isdefined("i")>
function personalFollowUp() {
	var followUpWin = window.open("personalFollowUp.cfm?i=#i#","followUpWindow","height=275,width=520");
}
</cfif>
function flagSalesFollowUp() {
	document.mainform.salesFollowUp.value = 1;
	document.mainform.submit();
}
</script>
</cfoutput>

<cfset currPage = listgetat(cf_template_path, listlen(cf_template_path,"\"), "\")>

<div align="center">
<table width="900" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="150" align="center" valign="top" class="sidebar">
			<!--- menu --->
			<cfoutput>
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<cfif listfindnocase(menuOptions,"scheduleFollowUp") is not 0>
				<tr>
					<td onmouseover="this.className='hoverOver'" onmouseout="this.className='hoverOut'" class="navbuttons"><a href="javascript: personalFollowUp();" class="menulink">Add personal follow up</a></td>
				</tr>
				</cfif>
				<cfif listfindnocase(menuOptions,"addResearch") is not 0>
				<tr>
					<td onmouseover="this.className='hoverOver'" onmouseout="this.className='hoverOut'" class="navbuttons"><a class="menulink" href="javascript:addResearch();">Add Research</a></td>
				</tr>
				</cfif>
				<cfif listfindnocase(menuOptions,"addConclusion") is not 0>
				<tr>
					<td onmouseover="this.className='hoverOver'" onmouseout="this.className='hoverOut'" class="navbuttons"><a href="javascript:addConclusion();" class="menulink">Add Conclusion</a></td>
				</tr>
				</cfif>
				<cfif listfindnocase(menuOptions,"addResolution") is not 0>
				<tr>
					<td onmouseover="this.className='hoverOver'" onmouseout="this.className='hoverOut'" class="navbuttons"><a href="javascript:addResolution();" class="menulink">Add Resolution</a> </td>
				</tr>
				</cfif>
				<cfif listfindnocase(menuOptions,"forwardInquiry") is not 0>
				<tr>
					<td onmouseover="this.className='hoverOver'" onmouseout="this.className='hoverOut'" class="navbuttons"><a href="javascript:forwardInquiry();" class="menulink">Forward Inquiry </a></td>
				</tr>
				</cfif>
				<cfif listfindnocase(menuOptions,"addContactAttempt") is not 0>
				<tr>
					<td onmouseover="this.className='hoverOver'" onmouseout="this.className='hoverOut'" class="navbuttons"><a href="javascript:addContactAttempt();" class="menulink">Add Contact Attempt</a></td>
				</tr>
				</cfif>
				<cfif listfindnocase(menuOptions,"closeInquiry") is not 0>
				<tr>
					<td onmouseover="this.className='hoverOver'" onmouseout="this.className='hoverOut'" class="navbuttons">
					<cfif allowClose is true>
					<a href="javascript: if (confirm('Are you sure you wish to close out this inquiry?\nYou cannot add any more information to an inquiry after it has been closed out.')) {closeInquiry('final')};" class="menulink">
					<cfelse>
					<a href="javascript: alert('You cannot close this inquiry until at least a second or final contact attempt has been made.');" class="menulink">
					</cfif>
					Close Out This Inquiry
					</a>
					</td>
				</tr>
				</cfif>
				<tr>
					<td onmouseover="this.className='hoverOver'" onmouseout="this.className='hoverOut'" class="navbuttons"><a href="index.cfm" class="menulink">Return to Inquiries</a></td>
				</tr>
			</table>
			</cfoutput>
			<!--- menu --->
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />


		</td>
		<td align="center" valign="top">