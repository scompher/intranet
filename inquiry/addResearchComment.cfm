
<cfsetting showdebugoutput="no">

<cfif isdefined("form.btnEditResearch")>

	<cfif trim(form.researchText) is not "">
		<cfset creator = getsec.firstname & " " & getsec.lastname>
		<cfset form.researchText = "#urldecode(form.oldResearchText)# #chr(13)# #chr(13)# Added by #creator# on #dateformat(now(),'mm/dd/yyyy')# #timeformat(now(),'hh:mm tt')#:#chr(13)#" & form.researchText>
		<cfquery name="UpdateResearch" datasource="#ds#">
			begin
				update inquiries_research
				set researchText = '#form.researchText#' 
				where researchid = #researchid#
			end
			begin
				select inquiryid from inquiries_research where researchid = #researchid#
			end
		</cfquery>
	</cfif>
	
	<cfoutput>
	<script type="text/javascript">
		opener.location='openInquiry.cfm?i=#updateresearch.inquiryid#';
		self.close();
	</script>
	</cfoutput>

<cfelse>

	<cfquery name="getResearch" datasource="#ds#">
		select 
		inquiries_research.* 
		from inquiries_research 
		where researchid = #i#
	</cfquery>
	
	<link rel="stylesheet" type="text/css" href="../styles.css">
	
	<div align="center">
	<title>Edit Research</title>
	<body onLoad="this.focus();">
	<table width="475" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td class="highlightbar"><b>Add Additional Research Text</b></td>
		</tr>
		<form method="post" action="addResearchComment.cfm">
		<cfoutput>
		<input type="hidden" name="researchid" value="#i#">
		<input type="hidden" name="oldResearchText" value="#urlencodedformat(getResearch.researchText)#">
		</cfoutput>
		<tr>
			<td class="greyrowbottom" align="center">
				<table border="0" cellpadding="5" cellspacing="0" class="grey">
					<tr>
						<td>
							<textarea name="researchText" rows="5" style="width:450px"></textarea>
						</td>
					</tr>
					<tr>
						<td>
							<input name="btnEditResearch" type="submit" class="sidebar" value="Add Comment">
						</td>
					</tr>
				</table>
			</td>
		</tr>
		</form>
	</table>
	</body>
	</div>

</cfif>
