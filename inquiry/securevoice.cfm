
<!---
12/24/2012 : PG : Merrry Christmas - this page takes in 3 variables (account number, recording name, inquiry number) and attaches recording to an inquiry 
http://192.168.1.10/inquiry/attachrecording.cfm?an=99-1000&rn=recordingname.wav&i=123454
--->

<cfparam name="url.an" default="">
<cfparam name="url.rn" default="">
<cfparam name="url.i" default="">
<cfparam name="url.c" default="">
<cfparam name="url.rdt" default="#dateformat(now(),'mmddyyyy')##timeformat(now(),'HHmmss')#">

<cfset accountNumber = url.an>
<cfset recordingName = url.rn>
<cfset inquiryNumber = url.i>
<cfset condition = url.c>
<cfset rdt = url.rdt>

<cfset rd = left(url.rdt,8)>
<cfset recordingDate = left(rd,2) & "/" & mid(rd,3,2) & "/" & right(rd,4)>
<cfset rt = right(url.rdt,6)>
<cfset recordingTime = left(rt,2) & ":" & mid(rt,3,2) & ":" & right(rt,2)>

<cfset recordingDateTime = recordingDate & " " & recordingTime>

<cfset recordingName = listgetat(recordingName,listlen(recordingName,"\"),"\")>

<cftry>
	<cfset recordingDir = listgetat(recordingName,2,"_")>
	<cfset sourceFile = "\\192.168.9.234\recordings\#recordingDir#\#recordingName#">
	<cfif not directoryExists("e:\websites\copalink\recordingmailbox\recordings\#recordingDir#")>
		<cfdirectory action="create" directory="e:\websites\copalink\recordingmailbox\recordings\#recordingDir#">
	</cfif>
	<cfset destinationFile = "e:\websites\copalink\recordingmailbox\recordings\#recordingDir#\#replace(recordingName,".wav",".mp3","all")#">
	
	<!--- convert recording from wav to mp3 and place on web server --->
	<cfx_wav2mp3 source="#sourceFile#" destination="#destinationFile#" samplerate="44100"> 
	
	<cfset recordingName = replace(recordingName,".wav",".mp3","all")>
	
	<cfquery name="getInquiryID" datasource="intranet">
		select inquiryid from inquiries_main where inquirynumber = '#inquiryNumber#' 
	</cfquery>
	<cfif getInquiryID.recordcount gt 0>
		<!--- check to see if recording is already attached --->
		<cfquery name="checkExistingAttachment" datasource="intranet">
			select * from inquiries_recordings 
			where inquiryid = #getInquiryID.inquiryID# and recordingName = '#recordingName#'
		</cfquery>
		<cfif checkExistingAttachment.recordcount gt 0>
			RJ|Recording already attached
		<cfelse>
			<!--- check if inquiry is closed yet or not --->
			<cfquery name="checkStatus" datasource="intranet">
				select * from inquiries_status_history 
				where inquiryid = #getInquiryID.inquiryID# and isCurrentStatus = 1 
			</cfquery>
			<cfif checkStatus.statusid is 4 or checkStatus.statusid is 5>
				RJ|Inquiry already closed
			<cfelse>
				<cfquery name="attachrecording" datasource="intranet">
					insert into inquiries_recordings (inquiryid, recordingName, accountNumber, dateTimeAttached, condition, recordingDateTime)
					values (#getInquiryID.inquiryID#, '#recordingName#', '#accountNumber#', GETDATE(), '#condition#', #createodbcdatetime(recordingDateTime)#) 
				</cfquery>
				OK|recording attached successfully
			</cfif>
		</cfif>
	<cfelse>
		RJ|inquiry doesn't exist, recording not attached
	</cfif>
	<cfcatch type="any"><cfoutput>RJ|#cfcatch.Message#<br />#cfcatch.Detail#</cfoutput></cfcatch>
</cftry>

