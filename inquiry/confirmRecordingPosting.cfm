<link rel="stylesheet" type="text/css" href="../styles.css">
<div align="center">
<table width="400" border="0" cellspacing="0" cellpadding="5">
    <tr>
        <td class="highlightbar"><b>Confirm Recording Posting </b></td>
    </tr>
    <tr>
        <td class="greyrowbottomnopadding">
            <table width="100%" border="0" cellspacing="0" cellpadding="5">
                <tr>
                    <td align="center">Are you sure you wish to post these recordings to MPower?</td>
                </tr>
                <tr>
                    <td align="center">&nbsp;</td>
                </tr>
				<form method="post" action="openInquiry.cfm">
				<cfoutput>
				<input type="hidden" name="inquiryid" value="#i#" />
				<input type="hidden" name="i" value="#i#" />
				<input type="hidden" name="closeInquiry" value="1" />
				</cfoutput>
                <tr>
                    <td align="center">
                        <input name="btnConfirmRecordingPost" type="submit" class="sidebar" value="Yes I'm sure, Proceed">
						&nbsp;
                        <input name="btnCancelRecordingPost" type="submit" class="sidebar" value="No, Cancel and go back">
                    </td>
                </tr>
				</form>
            </table>
		</td>
    </tr>
</table>

</div>
