
<link rel="stylesheet" type="text/css" href="../../styles.css">

<cfif isDefined("form.inquirynumber")>

<!--- get inquiryid --->
<cfquery name="getInqid" datasource="#ds#">
	select * from inquiries_main 
	where inquirynumber = #form.inquirynumber# 
</cfquery>

<cfif getInqid.recordcount gt 0>
<cfset inqid = getinqid.inquiryid>

<!--- del latest active status --->
<cfquery name="delLastStatus" datasource="#ds#">
	delete from inquiries_status_history 
	where inquiryid = #inqid# and isCurrentStatus = 1
</cfquery>

<!--- get last status --->
<cfquery name="getLastStatus" datasource="#ds#">
	select max(statusHistoryID) as lastid 
	from inquiries_status_history 
	where inquiryid = #inqid# 
</cfquery>
<cfset lastid = getLastStatus.lastid>

<!--- mark as current status --->
<cfquery name="markStatus" datasource="#ds#">
	update inquiries_status_history 
	set isCurrentStatus = 1 
	where statusHistoryID = #lastid# 
</cfquery>

<div align="center" class="normal">
<table border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Rollback Last Inquiry Status</b></td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td>Done.</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<br />
<a style="text-decoration:underline;" href="/index.cfm">Return to Intranet</a>
</div>

<cfelse>

<div align="center" class="normal">
<table border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Rollback Last Inquiry Status</b></td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td>Inquiry not found.</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<br />
<a style="text-decoration:underline;" href="/index.cfm">Return to Intranet</a>
</div>

</cfif>

<cfelse>

<div align="center" class="normal">
<table border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Rollback Last Inquiry Status</b></td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<form method="post" action="rollbackstatus.cfm">
				<tr>
					<td valign="middle">
					Enter Inquiry Number: 
					<input type="text" name="inquiryNumber" style="vertical-align:middle;">
					<input name="Submit" type="submit" class="sidebarsmall" value="Submit" style="vertical-align:middle;">
					</td>
				</tr>
				</form>
			</table>
		</td>
	</tr>
</table>
<br />
<a style="text-decoration:underline;" href="/index.cfm">Return to Intranet</a>
</div>

</cfif>
