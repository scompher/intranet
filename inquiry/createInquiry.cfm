
<!---
10/28/2015 : PG : Changed inquiry overdue timeframe to 3 days (72 hours) from 24 hours per Sarah Brooks. 
--->


<link rel="stylesheet" type="text/css" href="../styles.css">

<cfparam name="form.rec" default="">
<cfparam name="form.acct" default="">

<script language="javascript" src="cal2.js">
/*
Xin's Popup calendar script-  Xin Yang (http://www.yxscripts.com/)
Script featured on/available at http://www.dynamicdrive.com/
This notice must stay intact for use
*/
</script>
<script language="javascript" src="cal_conf2.js"></script>

<script language="javascript" src="functions.js"></script>

<cffunction name="isAllZeros" returntype="boolean" access="private">
	<cfargument name="valueToSearch" type="string" required="yes">
	<cfset theResult = true>
	<cfloop from="1" to="#len(valueToSearch)#" index="c">
		<cfset currentChar = mid(valueToSearch,c,1)>
		<cfif currentChar is not "0">
			<cfset theResult = false>
			<cfbreak>
		</cfif>
	</cfloop>
	<cfreturn theResult>
</cffunction>

<cfquery name="getResearchCats" datasource="#ds#">
	select * from inquiries_research_options 
	where active = 1 
	order by researchOption asc 
</cfquery>
<cfset researchCatList = valuelist(getResearchCats.researchOption)>

<!--- get categoryIDs for recordings --->
<cfquery name="getRecordingCatIDs" datasource="#ds#">
	select * from inquiries_categories 
	where category in ('Recording Requested','MPower Re-Posting') 
</cfquery>
<cfset recordingCatList = valuelist(getRecordingCatIDs.categoryid)>

<cfif isdefined("form.btnCheckForDups")>
	<cfif isAllZeros(form.rec) is false and isAllZeros(form.acct) is false>
		<!--- 
		<cfset prevDay = createodbcdate(dateAdd("d",-2,form.dateOccurred))>
		<cfset nextDay = createodbcdate(dateAdd("d",1,form.dateOccurred))>
		--->
		<cfset prevTime = createodbcdate(dateAdd("h",-12,now()))>
		<!--- 
		<cfquery name="checkfordups" datasource="#ds#">
			select * 
			from inquiries_main
			where 
				categoryid = #form.catid# and 
				rec = '#form.rec#' and 
				acct = '#form.acct#' and 
				dealerNumber = '#form.dealerNumber#' and 
				dateTimeOccurred >= #prevDay# and dateTimeOccurred < #nextDay#
		</cfquery>
		--->
		<cfquery name="checkfordups" datasource="#ds#">
			select * 
			from inquiries_main
			where 
				rec = '#form.rec#' and 
				acct = '#form.acct#' and 
				dealerNumber = '#form.dealerNumber#' and 
				dateTimeCreated >= #createodbcdatetime(prevTime)# 
		</cfquery>
		<!--- if category is "recording requested or mpower reposting" then let it go through since dealers may ask for multiple recordings : PG : 5/16/2013 --->
		<cfif checkfordups.recordcount is 0 or listfindnocase(recordingCatList,form.catid) is not 0>
			<cfquery name="getDealerInfo" datasource="#ds#">
				select * from dealerdiary_dealers
				where dealernumber = '#form.dealerNumber#'
			</cfquery>
			<cfset DealerName = getDealerInfo.dealerName>
			<cfset form.showAll = 1>
		<cfelse>
			<cfquery name="getDealerInfo" datasource="#ds#">
				select * from dealerdiary_dealers
				where dealernumber = '#form.dealerNumber#'
			</cfquery>
			<form method="post" action="createInquiry.cfm" name="mainform">
			<cfoutput>
			<input type="hidden" name="catid" value="#catid#">
			<input type="hidden" name="rec" value="#form.rec#">
			<input type="hidden" name="acct" value="#form.acct#">
			<input type="hidden" name="dealerName" value="#getDealerInfo.dealerName#">
			<input type="hidden" name="dealerNumber" value="#form.dealerNumber#">
			<input type="hidden" name="dateOccurred" value="#form.dateOccurred#">
			</cfoutput>
			<div align="center">
			<table width="500" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td colspan="2" nowrap class="highlightbar"><b>Create a new Inquiry </b></td>
				</tr>
				<tr>
					<td colspan="2" class="greyrowbottom">
					<cfoutput>
					There is another inquiry started for this rec-acct number within the last 12 hours.  <a href="appendInquiry.cfm?i=#checkfordups.inquiryid#" style="text-decoration:underline;"><b>Click here</b></a> to view the details, where you can append to the existing inquiry.
					</cfoutput>
					</td>
				</tr>
				<!--- 
				<tr>
					<td colspan="2" class="greyrowbottom">
						<input name="showAll" type="submit" class="sidebar" value="Continue and create a new inquiry" style="padding:1px">
					</td>
				</tr>
				--->
			</table>
			</div>
			</form>
			<cfabort>
		</cfif>
	<cfelse>
		<cfset form.showAll = 1>
	</cfif>
</cfif>

<cfif isdefined("form.showAll")>
	<cfset menuOptions = "addResearch,flagForSales">
<cfelse>
	<cfset menuOptions = "">
</cfif>
<cfif isDefined("form.btnGenerateInquiry")>
	<cfset menuOptions = "">
</cfif>

<cfinclude template="header.cfm">

<cfparam name="form.personInquiringIs" default="0">
<cfparam name="form.recordingRequested" default="0">
<cfparam name="form.removeAttachmentValue" default="">
<cfparam name="form.fileAttachments" default="">
<cfparam name="form.fileAttachment" default="">
<cfparam name="inquiryID" default="0">
<cfparam name="catid" default="0">
<cfparam name="addResearch" default="0">
<cfparam name="addResolution" default="0">
<cfparam name="addConclusion" default="0">
<cfparam name="salesFollowUp" default="0">
<cfparam name="rec" default="">
<cfparam name="acct" default="">
<cfparam name="subscriberName" default="">
<cfparam name="dealerNumber" default="">
<cfparam name="dealerName" default="">
<cfparam name="personInquiring" default="">
<cfparam name="personInquiringIs" default="">
<cfparam name="personInquiringPhone" default="">
<cfparam name="dateTimeOccurred" default="">
<cfparam name="timeOccurredTimeZone" default="Eastern">
<cfparam name="codes" default="">
<cfparam name="zones" default="">
<cfparam name="inquiryText" default="">
<cfparam name="recordingRequested" default="">
<cfparam name="recordingReason" default="">
<cfparam name="forDeptID" default="0">
<cfparam name="fromDeptID" default="0">
<cfparam name="salesFollowUp" default="">
<cfparam name="researchText" default="">
<cfparam name="dateOccurred" default="dateformat(now(),'mm/dd/yyyy')">
<cfparam name="otherCategory" default="">
<cfparam name="recordingPlayedManually" default="0">
<cfparam name="recordingDownload" default="0">
<cfparam name="checkedDealerAccess" default="0">
<cfparam name="researchCategory" default="">
<cfparam name="researchLocation" default="">

<!--- does dealer have CAL access 
<cfif listfindnocase(recordingCatList,catid) is not 0>
	<cfif checkedDealerAccess is 0>
		<cfinvoke 
		 component="inquiry.cfc.calAccess"
		 method="getCalAccess"
		 returnvariable="getCalAccessRet">
			<cfinvokeargument name="dealerNumber" value="#dealerNumber#"/>
		</cfinvoke>
		<cfset checkedDealerAccess = 1>
		<cfif getCalAccessRet is true>
			<cfset recordingDownload = 1>
		</cfif>
	</cfif>
</cfif>
--->

<cfquery name="getCategories" datasource="#ds#">
	select * 
	from inquiries_categories
	order by category asc
</cfquery>

<cfif not isDefined("form.showAll")>
<form method="post" action="createInquiry.cfm" name="mainform">
<table border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td colspan="2" nowrap class="highlightbar"><b>Create a new Inquiry </b></td>
	</tr>
	<tr>
		<td colspan="2" class="greyrow">Please enter the receiver, account number, dealer number, category and date occurred to check for duplicate inquiries</td>
	</tr>
	<cfoutput>
	<tr>
		<td colspan="2" class="greyrow"><b>Created By: #getsec.firstname# #getsec.lastname#</b> <br>
		(if you are not #getsec.firstname# #getsec.lastname# please <a style="text-decoration:underline;" href="/login/logout.cfm">log out</a> of the Intranet and log back in under your account)
		</td>
	</tr>
	</cfoutput>
	<tr>
		<td width="31%" class="greyrowleft" style="padding:0px">
			<table border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td nowrap class="grey">Rec-Acct #: </td>
					<td class="grey">
						<table border="0" cellpadding="1" cellspacing="0" class="grey">
							<tr>
								<td>
									<input name="rec" type="text" style="width:45px" value="<cfoutput>#rec#</cfoutput>" size="0" maxlength="4">
								</td>
								<td>&ndash;</td>
								<td>
									<input name="acct" type="text" style="width:45px" value="<cfoutput>#acct#</cfoutput>" maxlength="6">
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td><td width="69%" class="greyrowright" style="padding:0px">
			<table border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td nowrap class="grey">Dealer #: </td>
					<td class="grey">
						<input name="dealerNumber" value="<cfoutput>#dealerNumber#</cfoutput>" type="text" style="width:45px" maxlength="4">
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2" class="greyrow" style="padding:0px">
			<table border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td nowrap class="grey">Category:</td>
					<td class="grey">
						<select name="catid">
							<option <cfif catid is 0>selected</cfif> value="0"></option>
							<cfoutput query="getCategories">
								<cfif getCategories.category is not "Other">
									<option <cfif catid is getCategories.categoryid>selected</cfif> value="#categoryid#">#category#</option>
								</cfif>
							</cfoutput>
							<!--- 
							<cfoutput query="getCategories">
								<cfif getCategories.category is "Other">
									<option <cfif catid is getCategories.categoryid>selected</cfif> value="#categoryid#">#category#</option>
								</cfif>
							</cfoutput>
							--->
						</select>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<!--- 
	<tr>
		<td colspan="2" class="greyrow" style="padding:0px">
			<table border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td nowrap class="grey">If category is Other, please specify :</td>
					<td class="grey">
						<input name="otherCategory" type="text" size="50" maxlength="100">
					</td>
				</tr>
			</table>
		</td>
	</tr>
	--->
	<tr>
		<td colspan="2" class="greyrow" style="padding:0px">
			<table border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td nowrap class="grey">Occurred on: </td>
					<td class="grey" style="padding:0px">
						<cfoutput>
						<table border="0" cellpadding="5" cellspacing="0" class="grey">
							<tr>
								<td>Date:</td>
								<td>
									<input name="dateOccurred" type="text" style="width:75px" value="#dateformat(now(),'mm/dd/yyyy')#" maxlength="10" />
								</td>
								<td><a style="text-decoration:none;" href="javascript:showCal('DateOccurred');"> <img src="../images/calicon.gif" alt="Select a Date" align="absmiddle" width="20" height="20" border="0" /> </a></td>
								</tr>
						</table>
						</cfoutput>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2" class="greyrowbottom">
			<input name="btnCheckForDups" type="submit" class="sidebar" value="Continue" style="padding:1px">
		</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
</table>
</form>
<cfabort>
</cfif>

<cfif form.removeAttachmentValue is not "">
	<cfset serverFile = listgetat(form.removeAttachmentValue,1,":")>
	<cfset clientFile = listgetat(form.removeAttachmentValue,2,":")>
	<cfif fileExists("#request.DirectPath#\inquiry\attachments\#serverFile#")>
		<cffile action="delete" file="#request.DirectPath#\inquiry\attachments\#serverFile#">
	</cfif>
	<cfset fileAttachments = listdeleteat(form.fileAttachments,listfindnocase(form.fileAttachments,form.removeAttachmentValue))>
</cfif>

<cfif trim(form.fileAttachment) is not "">
	<cfset form.btnAttach = 1>
</cfif>

<cfif isdefined("form.btnAttach") and form.fileAttachment is not "">
	<cffile action="upload" filefield="fileAttachment" destination="#request.DirectPath#\inquiry\attachments\" nameconflict="makeunique">
	<cfset savedFile = file.ServerFile>
	<cfset uploadedFile = file.ClientFile>
	<cfset safeFileName = replace(savedFile, " ", "", "all")>
	<cfset safeFileName = replace(safeFileName, "##", "", "all")>
	<cfset safeFileName = replace(safeFileName, ",", "", "all")>
	<cfset safeFileName = replace(safeFileName, ":", "", "all")>
	<cffile action="rename" source="#request.DirectPath#\inquiry\attachments\#savedFile#" destination="#request.DirectPath#\inquiry\attachments\#safeFileName#">
	<cfset form.fileAttachments = listappend(form.fileAttachments, "#safeFileName#:#uploadedFile#")>
</cfif>

<cfif isdefined("form.btnGenerateInquiry")>

	<cfset today = createodbcdatetime(now())>
	<cfset dateTimeOccurred = createodbcdatetime("#dateOccurred# #timeOccurredHH#:#timeOccurredMM# #timeOccurredTT#")>
	
	<cfset exceptiondays = "1,6,7">
	<cfset dow = dayofweek(today)>
	<cfif listfind(exceptiondays,dow) is not 0>
		<cfswitch expression="#dow#">
			<cfcase value="6"><cfset dateTimeOverdue = dateadd("d",6,today)></cfcase>
			<cfcase value="7"><cfset dateTimeOverdue = dateadd("d",5,today)></cfcase>
			<cfcase value="1"><cfset dateTimeOverdue = dateadd("d",4,today)></cfcase>
		</cfswitch>
		<cfset dateTimeOverdue = createodbcdatetime("#dateformat(dateTimeOverdue,'mm/dd/yyyy')# 17:00:00")>
	<cfelse>
		<cfset dateTimeOverdue = dateadd("h",72,today)>
	</cfif>

	<cftransaction>
	<cfquery name="SaveInquiry" datasource="#ds#">
		declare @inqID as int
		declare @inqNum as int

		set @inqNum = (select max(inquiryNumber) from inquiries_main)
		
		if @inqNum IS NULL
			set @inqNum = 10000
		else
			set @inqNum = @inqNum + 1

		insert into inquiries_main (inquiryNumber, rec, acct, subscriberName, dealerNumber, dealerName, personInquiring, personInquiringIs, personInquiringPhone, dateTimeOccurred, timeOccurredTimeZone, codes, zones, inquiryText, recordingRequested, recordingReason, salesFollowUp, dateTimeCreated, categoryid, otherCategory, dateTimeOverdue, recordingPlayedManually, recordingDownload)
		values (@inqNum, '#ucase(form.rec)#', '#ucase(form.acct)#', '#form.subscriberName#', '#ucase(form.dealerNumber)#', '#form.dealerName#', '#form.personInquiring#', '#form.personInquiringIs#', '#form.personInquiringPhone#', #dateTimeOccurred#, '#form.timeOccurredTimeZone#', '#form.codes#', '#form.zones#', '#form.inquiryText#', #form.recordingRequested#, '#form.recordingReason#', #form.salesFollowUp#, #today#, #form.catid#, '#form.otherCategory#', #dateTimeOverdue#, #recordingPlayedManually#, #recordingDownload#)

		set @inqID = (select inquiryID from inquiries_main where inquiryNumber = @inqNum)
		 
		insert into inquiries_status_history (inquiryid, dateTimeUpdated, statusid, adminuserid, isCurrentStatus, fromDepartmentID, toDepartmentID)
		values (@inqID, #today#, 1, #cookie.adminlogin#, 1, #fromDeptID#, #forDeptID#)
	
		<cfif trim(form.fileAttachments) IS NOT "">
			<cfloop list="#form.fileAttachments#" index="theFile">
				<cfset savedFile = listgetat(theFile,1,":")>
				<cfset clientFile = listgetat(theFile,2,":")>
				if not exists (select * from inquiries_attachments where savedFile = '#savedFile#' and clientFile = '#clientFile#' and inquiryid = @inqID)
				begin
					insert into inquiries_attachments (savedFile, clientFile, inquiryID)
					values ('#savedFile#', '#clientFile#', @inqID)
				end
			</cfloop>
		</cfif>

		select @inqID as inquiryID
	</cfquery>
	
	</cftransaction>
	<cfset inquiryID = saveInquiry.inquiryID>

	<!--- add to dealer diary notes --->
	<cfquery name="getInq" datasource="#ds#">
		select * from inquiries_main where inquiryid = #inquiryid#
	</cfquery>
	<cfset subject = "Inquiry #getInq.inquiryNumber# was created on #dateformat(getInq.dateTimeCreated,'mm/dd/yyyy')#">
	<cfset inqText = "<a href=""javascript:viewPrintWin(''/inquiry/viewInquiry.cfm?i=#getInq.inquiryid#'');"">View/Print Full Inquiry</a>#chr(13)##chr(13)##getInq.inquiryText#">
	<cfquery name="insertIntoDiary" datasource="#ds#">
		insert into dealerdiary_entries (creatorID, dealernumber, created, content, subject, departmentid, parentid, inquiryNote, inquiryID)
		values (#cookie.adminlogin#, '#getInq.dealerNumber#', #createodbcdatetime(getInq.dateTimeCreated)#, '#inqText#', '#subject#', #fromDeptID#, 0, 1, #getInq.inquiryid#)
	</cfquery>

	<!--- save research if any --->
	<cfif form.addResearch is 1>
		<cfquery name="saveResearch" datasource="#ds#">
			insert into inquiries_research (inquiryid, researchText, creatorid, datetimecreated, researchLocation, researchCategory)
			values (#inquiryid#, '#researchText#', #cookie.adminlogin#, #today#, '#researchLocation#', '#researchCategory#')
		</cfquery>

	</cfif>

	<cfquery name="GetContact" datasource="#ds#">
		select * from inquiries_contacts
		where departmentid = #forDeptID#
	</cfquery>
	<cfquery name="Getinqnum" datasource="#ds#">
		select inquirynumber from inquiries_main
		where inquiryid = #inquiryid#
	</cfquery>
	<table border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td colspan="2" nowrap class="highlightbar" align="center"><b>Create a new Inquiry </b></td>
		</tr>
		<tr>
			<td colspan="2" class="greyrow" align="center">
			<cfoutput>
			Inquiry ###getinqnum.inquirynumber# has been created.<br>
			<br>
			<cfif forDeptID is not 1>
				Please contact #getcontact.firstname# #getcontact.lastname# at extension #getcontact.extention# to notify of your inquiry.
			</cfif>
			</cfoutput>
			</td>
		</tr>
		<tr>
			<td colspan="2" class="greyrowbottom" align="center">
			<a style="text-decoration:underline" href="/index.cfm">Click here to return to Intranet Menu</a>
			</td>
		</tr>
	</table>	
	<cfif forDeptID is 3>
	<!--- 6/10/2013 : PG : added for steve --->
	<cfmail type="html" from="pgregory@copsmonitoring.com" to="dsupport@copsmonitoring.com" subject="A new inquiry has been created for Dealer Support">
	<div style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:11px; color:##000000">
	Inquiry ## #getinqnum.inquirynumber# has been genereated for the Dealer Support department. 
	<br>
	<br>
	"#replace(form.inquiryText,chr(13),"<br>","all")#"
	<br>
	<br>
	<a href="#request.appurl#/inquiry/index.cfm?deptlist=3">Click here to go to the inquiry pickup screen</a>
	</div>
	</cfmail>
	</cfif>
	<cfif forDeptID is 2>
	<!--- 6/10/2013 : PG : added for steve --->
	<cfmail type="html" from="pgregory@copsmonitoring.com" to="accounting@copsmonitoring.com" subject="A new inquiry has been created for Accounting">
	<div style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:11px; color:##000000">
	Inquiry ## #getinqnum.inquirynumber# has been genereated for the Accounting department. 
	<br>
	<br>
	"#replace(form.inquiryText,chr(13),"<br>","all")#"
	<br>
	<br>
	<a href="#request.appurl#/inquiry/index.cfm?deptlist=2">Click here to go to the inquiry pickup screen</a>
	</div>
	</cfmail>
	</cfif>
	<cfif forDeptID is 1>
	<!--- 6/10/2013 : PG : added for steve --->
	<cfmail type="html" from="pgregory@copsmonitoring.com" to="technologyrequest@copsmonitoring.com" subject="A new inquiry (## #getinqnum.inquirynumber#) has been created for Technology">
	<div style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:11px; color:##000000">
	Inquiry ## #getinqnum.inquirynumber# has been genereated for the Technology department. 
	<br>
	<br>
	"#replace(form.inquiryText,chr(13),"<br>","all")#"
	<br>
	<br>
	<a href="#request.appurl#/inquiry/index.cfm?deptlist=1">Click here to go to the inquiry pickup screen</a>
	</div>
	</cfmail>
	</cfif>

	<cfif forDeptID is 8>
	<!--- 10/17/2017 : CM : added for Angie Teasley (Ticket #2330)--->
	<!--- 10/19/2017 : CM : updated to ateasley@copsmonitoring.com per Angies ticket response --->
	<cfmail type="html" from="pgregory@copsmonitoring.com" to="ateasley@copsmonitoring.com" subject="A new inquiry (## #getinqnum.inquirynumber#) has been created for Data Entry">	
	<div style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:11px; color:##000000">
	Inquiry ## #getinqnum.inquirynumber# has been genereated for the Data Entry department. 
	<br>
	<br>
	"#replace(form.inquiryText,chr(13),"<br>","all")#"
	<br>
	<br>
	<a href="#request.appurl#/inquiry/index.cfm?deptlist=8">Click here to go to the inquiry pickup screen</a>
	</div>
	</cfmail>
	</cfif>


	<cfabort>
	<!--- <cflocation url="index.cfm"> --->
	
</cfif>

<!--- 
<cfquery name="getInquiryIsOptions" datasource="#ds#">
	select * 
	from inquiries_inquiryIsOptions
	order by inquiryIsOrder
</cfquery>
--->
<cfquery name="getDepartments" datasource="#ds#">
	select * 
	from admin_users_departments
	where departmentid != '15'
	order by department asc
</cfquery>

<cfquery name="getUserDepartments" datasource="#ds#">
	select admin_users_departments.*
	from admin_users_departments_lookup 
	inner join admin_users_departments on admin_users_departments_lookup.departmentid = admin_users_departments.departmentid
	where admin_users_departments_lookup.adminuserid = #cookie.adminlogin#
</cfquery>

<script language="JavaScript" type="text/javascript">
function removeAttachment(serverFile, clientFile) {
	document.mainform.removeAttachmentValue.value = serverFile + ':' + clientFile;
	document.mainform.submit();
}

function checkform(frm) {
	if (frm.catid.selectedIndex == 0) {alert('The category is required.'); frm.catid.focus(); return false;}
	if (frm.fromDeptID.selectedIndex == 0) {alert('Originating department is required.'); frm.fromDeptID.focus(); return false;}
	if (frm.rec.value == "" || frm.acct.value == "") {alert('Rec-Acct is required.'); frm.rec.focus(); return false;}
	if (frm.subscriberName.value == "") {alert('Subscriber is required.'); frm.subscriberName.focus(); return false;}
	if (frm.dealerNumber.value == "") {alert('Dealer number is required.'); frm.dealerNumber.focus(); return false;}
	if (frm.dealerName.value == "") {alert('Dealer name is required.'); frm.dealerName.focus(); return false;}
	if (frm.personInquiring.value == "") {alert('Person Inquiring is required.'); frm.personInquiring.focus(); return false;}
	if (!frm.personInquiringIs[0].checked && !frm.personInquiringIs[1].checked && !frm.personInquiringIs[2].checked) {alert('Person inquiring is: is required.'); return false;}
	if (frm.personInquiringPhone.value == "") {alert('The person inquiring phone # is required.'); frm.personInquiringPhone.focus(); return false;}
	if (frm.dateOccurred.value == "") {alert('The date occurred is required.'); frm.dateOccurred.focus(); return false;}
	if (isDate(frm.dateOccurred.value) == false) {alert('The date occurred must be a valid date.'); frm.dateOccurred.focus(); return false;}
	if (frm.timeOccurredHH.value == "") {alert('The time occurred hours is required.'); frm.timeOccurredHH.focus(); return false;}
	if (frm.timeOccurredHH.value > 12) {alert('The time occurred hours cannot be more than 12.'); frm.timeOccurredHH.focus(); return false;}
	if (isNaN(frm.timeOccurredHH.value)) {alert('The time occurred hours must be a number.'); frm.timeOccurredHH.focus(); return false;}
	if (frm.timeOccurredMM.value == "") {alert('The time occurred minutes is required.'); frm.timeOccurredMM.focus(); return false;}
	if (frm.timeOccurredMM.value > 59) {alert('The time occurred minutes cannot be more than 59.'); frm.timeOccurredMM.focus(); return false;}
	if (isNaN(frm.timeOccurredMM.value)) {alert('The time occurred minutes must be a number.'); frm.timeOccurredMM.focus(); return false;}
	if (frm.timeOccurredTimeZone.selectedIndex == 0) {alert('The time occured time zone is required.'); frm.timeOccurredTimeZone.focus(); return false;}
	if (frm.inquiryText.value == "" || frm.inquiryText.value == "(Clearly state the person's questions or statement)") {alert('The inquiry description is required.'); frm.inquiryText.focus(); return false;}
	if (frm.recordingRequested.checked && frm.recordingReason.value == "") {alert('The recording requested reason is required.'); frm.recordingReason.focus(); return false;}
	if (frm.forDeptID.selectedIndex == 0) {alert('Transfer Inquiry To, is required.'); frm.forDeptID.focus(); return false; }

	return true;
}

function attachRecording(i) {
	window.open('attachToMpower.cfm?l=createInquiry.cfm&i=' + i,'recWin','width=470,height=185,scrollbars=no,resizeable=no');
}

</script>

<body onLoad="document.mainform.fromDeptID.focus();">
<form onSubmit="return checkform(this);" action="createInquiry.cfm" method="post" enctype="multipart/form-data" name="mainform">
<cfoutput>
<input type="hidden" name="showAll" value="#form.showAll#">
<input type="hidden" name="addResearch" value="#addResearch#" />
<input type="hidden" name="addResolution" value="#addResolution#" />
<input type="hidden" name="addConclusion" value="#addConclusion#" />
<input type="hidden" name="forwardInquiry" value="0" />
<input type="hidden" name="closeInquiry" value="0" />
<input type="hidden" name="addContactAttempt" value="0" />
<input type="hidden" name="salesFollowUp" value="#salesFollowUp#" />
<input type="hidden" name="fileAttachments" value="#form.fileAttachments#">
<input type="hidden" name="removeAttachmentValue" value="">
<input type="hidden" name="checkedDealerAccess" value="#checkedDealerAccess#">
</cfoutput>
<table border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td align="center" nowrap class="highlightbar" style="border-right:0px"><b>New Inquiry</b></td>
		<td nowrap class="highlightbar" style="border-left:0px">Please fill out the information below </td>
	</tr>
	<cfif salesFollowUP is 1>
	<tr>
		<td class="greyrowleft">&nbsp;</td>
		<td class="greyrowright">
		<b>Inquiry is flagged for sales follow up.</b>		</td>
	</tr>
	</cfif>
	<tr>
		<td nowrap class="greyrowleft">Category:</td>
		<td class="greyrowright">
		<select name="catid">
			<option <cfif catid is 0>selected</cfif> value="0"></option>
		<cfoutput query="getCategories">
			<cfif getCategories.category is not "Other">
				<option <cfif catid is getCategories.categoryid>selected</cfif> value="#categoryid#">#category#</option>
			</cfif>
		</cfoutput>
		<cfoutput query="getCategories">
			<cfif getCategories.category is "Other">
				<option <cfif catid is getCategories.categoryid>selected</cfif> value="#categoryid#">#category#</option>
			</cfif>
		</cfoutput>
		</select>
		</td>
	</tr>
	<tr>
		<td colspan="2" class="greyrow" style="padding:0px">
			<table border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td nowrap class="grey">If category is Other, please specify :</td>
					<td class="grey">
						<input name="otherCategory" type="text" size="50" maxlength="100" value="<cfoutput>#otherCategory#</cfoutput>">
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td nowrap class="greyrowleft">Originating Department: </td>
		<td class="greyrowright">
			<select name="fromDeptID" tabindex="1">
				<option <cfif fromDeptID is 0>selected</cfif> value="0"></option>
				<cfoutput query="getUserDepartments">
					<cfif getUserDepartments.department is not "System">
					<option <cfif fromDeptID is getUserDepartments.departmentid>selected</cfif> value="#getUserDepartments.departmentID#">#getUserDepartments.department#</option>
					</cfif>
				</cfoutput>
			</select>
		</td>
	</tr>
	<tr>
		<td nowrap class="greyrowleft">Rec-Acct #: </td>
		<td class="greyrowright">
			<table border="0" cellpadding="1" cellspacing="0" class="grey">
				<tr>
					<td>
						<input name="rec" type="text" style="width:45px" value="<cfoutput>#rec#</cfoutput>" size="0" maxlength="4">
					</td>
					<td>&ndash;</td>
					<td>
						<input name="acct" type="text" style="width:45px" value="<cfoutput>#acct#</cfoutput>" maxlength="4">
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td nowrap class="greyrowleft">Subscriber:</td>
		<td class="greyrowright">
			<input name="subscriberName" value="<cfoutput>#subscriberName#</cfoutput>" type="text" maxlength="255" style="width:550px">
		</td>
	</tr>
	<tr>
		<td nowrap class="greyrowleft">Dealer #: </td>
		<td class="greyrowright">
			<input name="dealerNumber" value="<cfoutput>#dealerNumber#</cfoutput>" type="text" style="width:45px" maxlength="4">
		</td>
	</tr>
	<tr>
		<td nowrap class="greyrowleft">Occurred on: </td>
		<td class="greyrowright" style="padding:0px"> <cfoutput>
				<table border="0" cellpadding="5" cellspacing="0" class="grey">
					<tr>
						<td>Date:</td>
						<td>
							<input name="dateOccurred" type="text" style="width:75px" value="#dateOccurred#" maxlength="10" />
						</td>
						<td> <a style="text-decoration:none;" href="javascript:showCal('DateOccurred');"> <img src="../images/calicon.gif" alt="Select a Date" align="absmiddle" width="20" height="20" border="0" /> </a> </td>
						<td>Time:</td>
						<td class="nopadding">
							<table border="0" cellpadding="5" cellspacing="0" class="grey">
								<tr>
									<td class="nopadding">
										<input name="timeOccurredHH" type="text" style="width:25px" value="#timeformat(now(),'hh')#" maxlength="2" />
									</td>
									<td valign="middle">:</td>
									<td class="nopadding">
										<input name="timeOccurredMM" type="text" style="width:25px" value="#timeformat(now(),'mm')#" maxlength="2" />
									</td>
									<td>
										<select name="timeOccurredTT">
											<option <cfif timeformat(now(),'tt') is "AM">selected</cfif> value="AM">AM</option>
											<option <cfif timeformat(now(),'tt') is "PM">selected</cfif> value="PM">PM</option>
										</select>
									</td>
									<td>Time Zone: </td>
									<td>
										<select name="timeOccurredTimeZone">
											<option value="" selected="selected"></option>
											<option <cfif timeOccurredTimeZone IS "Atlantic">selected</cfif> value="Atlantic">Atlantic</option>
											<option <cfif timeOccurredTimeZone IS "Eastern">selected</cfif> value="Eastern">Eastern</option>
											<option <cfif timeOccurredTimeZone IS "Central">selected</cfif> value="Central">Central</option>
											<option <cfif timeOccurredTimeZone IS "Mountain">selected</cfif> value="Mountain">Mountain</option>
											<option <cfif timeOccurredTimeZone IS "Pacific">selected</cfif> value="Pacific">Pacific</option>
											<option <cfif timeOccurredTimeZone IS "Arizona">selected</cfif> value="Arizona">Arizona</option>
											<option <cfif timeOccurredTimeZone IS "Hawaii">selected</cfif> value="Hawaii">Hawaii</option>
											<option <cfif timeOccurredTimeZone IS "Alaska">selected</cfif> value="Alaska">Alaska</option>
										</select>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
		</cfoutput> </td>
	</tr>
	<tr>
		<td nowrap class="greyrowleft">Dealer Name: </td>
		<td class="greyrowright">
			<input name="dealerName" value="<cfoutput>#dealerName#</cfoutput>" type="text" style="width:550px" maxlength="255">
		</td>
	</tr>
	<tr>
		<td nowrap class="greyrowleft">Person Inquiring: </td>
		<td class="greyrowright">
			<input name="personInquiring" value="<cfoutput>#personInquiring#</cfoutput>" type="text" style="width:550px" maxlength="255">
		</td>
	</tr>
	<tr>
		<td nowrap class="greyrowleft">Person Inquiring is: </td>
		<td class="greyrowright" style="padding:0px">
			<table width="75%" border="0" cellpadding="3" cellspacing="0" class="grey">
				<tr>
					<td width="20" nowrap="nowrap">
						<input <cfif personInquiringIs is "Dealer">checked</cfif> type="radio" name="personInquiringIs" value="Dealer" />
					</td>
					<td width="81" nowrap="nowrap">Dealer</td>
					<td width="20" nowrap="nowrap">
						<input <cfif personInquiringIs is "Subscriber">checked</cfif> type="radio" name="personInquiringIs" value="Subscriber" />
					</td>
					<td width="96" nowrap="nowrap">Subscriber</td>
					<td width="20" nowrap="nowrap">
						<input <cfif personInquiringIs is "Employee (in-house)">checked</cfif> type="radio" name="personInquiringIs" value="Employee (in-house)" />
					</td>
					<td width="188" nowrap="nowrap">Employee (in-house) </td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td nowrap class="greyrowleft">Phone #: </td>
		<td class="greyrowright">
			<input name="personInquiringPhone" value="<cfoutput>#personInquiringPhone#</cfoutput>" type="text" style="width:125px" maxlength="50" />
		</td>
	</tr>
	
	<tr>
		<td nowrap class="greyrowleft">If applicable, Code(s): </td>
		<td class="greyrowright">
			<input name="codes" value="<cfoutput>#codes#</cfoutput>" type="text" style="width:550px" maxlength="255">
		</td>
	</tr>
	<tr>
		<td nowrap class="greyrowleft">If applicable, Zone(s): </td>
		<td class="greyrowright">
			<input name="zones" value="<cfoutput>#zones#</cfoutput>" type="text" style="width:550px" maxlength="255">
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap class="greyrowleft">Inquiry:</td>
		<td class="greyrowright">
			<textarea name="inquiryText" cols="" rows="5" style="width:550px" onFocus="this.select();"><cfoutput>#inquiryText#</cfoutput></textarea>
		</td>
	</tr>
	<tr>
		<td nowrap class="greyrowleft">Recording Requested: </td>
		<td class="greyrowright">
			<input type="checkbox" name="recordingRequested" <cfif recordingRequested is 1>checked</cfif> value="1" />
		</td>
	</tr>
	<tr>
		<td nowrap class="greyrowleft">Reason for Recording: </td>
		<td class="greyrowright">
			<input name="recordingReason" value="<cfoutput>#recordingReason#</cfoutput>" type="text" style="width:550px" maxlength="255" />
		</td>
	</tr>
	<!--- 12/17/2012 : PG : added recording play options --->
	<tr>
		<td class="greyrow" colspan="2">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td width="60" align="right" valign="middle"><input type="checkbox" name="recordingPlayedManually" <cfif recordingPlayedManually is 1>checked</cfif> value="1"></td>
					<td valign="middle">
						Recording will be played manually for the dealer
					</td>
				</tr>
				<tr>
					<td width="60" align="right" valign="middle">
					<input type="checkbox" name="recordingDownload" <cfif recordingDownload is 1>checked</cfif> value="1">&nbsp;
					</td>
					<td valign="middle">
						<!--- <input type="checkbox" name="recordingDownload" value="0"> --->
						<!--- <span style="text-decoration:line-through"> --->Place recording on MPower in Recording Center<!--- </span> ---><br>
						<!--- <b style="color:#FF0000">THIS FEATURE IS UNAVAILABLE UNTIL FURTHER NOTICE WHILE MAINTENANCE IS BEING PERFORMED ON THE RECORDINGS DATABASE. THE RECORDING MAY BE PLAYED MANUALLY FOR THE DEALER</b> --->
					</td>
				</tr>
			</table>
		</td>
	</tr>	
	<tr>
		<td nowrap class="greyrowleft">Transfer Inquiry To: </td>
		<td class="greyrowright">
			<select name="forDeptID">
				<option <cfif forDeptID is 0>selected</cfif> value="0"></option>
				<cfoutput query="getDepartments">
					<cfif getDepartments.department is not "System">
					<option <cfif forDeptID is getDepartments.departmentid>selected</cfif> value="#getDepartments.departmentID#">#getDepartments.department#</option>
					</cfif>
				</cfoutput>
			</select>
		</td>
	</tr>
	<tr>
		<td class="greyrowleft">Attach File(s): </td>
		<td class="greyrowright" style="padding:0px">
			<table border="0" cellspacing="0" cellpadding="5" class="grey">
				<tr>
					<td>
						<input type="file" name="fileAttachment">
					</td>
					<td>
						<input name="btnAttach" type="submit" class="sidebar" value="Attach File">
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top" class="greyrowleft">Current Attachments: </td>
		<td class="greyrowright" style="padding:0px">
		<cfif fileAttachments is not "">
			<table border="0" cellpadding="5" cellspacing="0" class="grey">
				<cfloop list="#fileAttachments#" index="theFile">
				<cfoutput>
				<cfset serverFile = listgetat(theFile,1,":")>
				<cfset fileName = listgetat(theFile,2,":")>
				<tr>
					<td><a target="_blank" href="/inquiry/attachments/#serverFile#" style="text-decoration:underline;">#fileName#</a></td>
					<td><a href="javascript:removeAttachment('#serverFile#','#fileName#');" style="text-decoration:underline;">[remove]</a></td>
				</tr>
				</cfoutput>
				</cfloop>
			</table>
		<cfelse>
			<table border="0" cellpadding="5" cellspacing="0" class="grey">
				<tr>
					<td colspan="2">None</td>
				</tr>
			</table>
		</cfif>
		&nbsp;	
		</td>
	</tr>
	<cfif addResearch is 1>
	<tr><td style="border-top:1px solid #000000;" colspan="2">&nbsp;</td></tr>
	<tr>
		<td colspan="2" class="highlightbar"><b>Add Research</b> (Clearly Explain)</td>
	</tr>
	<tr>
		<td class="greyrow" colspan="2">
		<cfset locationList = "NJ,FL,TX,AZ,TN,MD">
		Site location of the problem: 
		<select name="researchLocation">
			<option value=""></option>
			<cfoutput>
				<cfloop list="#locationList#" index="site">
					<option value="#site#">#site#</option>
				</cfloop>
			</cfoutput>
		</select>
		</td>
	</tr>
	<tr>
		<td class="greyrow" colspan="2">
		Problem category: 
		<!--- <cfset researchCatList = "2-way Handling,Technical Issue,False Alarm Handling,Account on Test,Telemax,Delay in Handling,Verifying Address,Unprofessional,Passcode Verification"> --->
		<select name="researchCategory">
			<option value=""></option>
			<cfoutput>
				<cfloop list="#researchCatList#" index="cat">
					<option value="#cat#">#cat#</option>
				</cfloop>
			</cfoutput>
		</select>
		</td>
	</tr>
	<tr>
		<td class="greyrow" colspan="2">
			<textarea name="researchText" cols="" rows="5" style="width:693px" onFocus="this.select();">(explain research here)</textarea>
		</td>
	</tr>
	<tr>
		<td class="greyrow" colspan="2" align="right" nowrap>&nbsp;</td>
	</tr>
	</cfif>
	<tr>
		<td colspan="2" align="center" class="greyrowbottom">
			<input name="btnGenerateInquiry" type="submit" class="lightbutton" value="Generate Inquiry">
			&nbsp;&nbsp;
			<input type="button" class="lightbutton" value="Cancel Inquiry" onClick="document.location='index.cfm';">
		</td>
	</tr>
</table>
</form>
</body>

<cfinclude template="footer.cfm">
