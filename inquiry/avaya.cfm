
<!---
http://192.168.1.10/inquiry/attachrecording.cfm?an=99-1000&rn=recordingname.wav&i=123454
--->

<!---
http://192.168.107.81/inquiry/attachrecording.cfm?rn=00001032781491001439&format=a&startedat=20170331-18:00:32&endedat=20170331-19:04:32&an=99-1000&i=12345
an = account number
i = inquiry number
--->

<cfparam name="url.an" default="">
<cfparam name="url.i" default="">
<cfparam name="url.rn" default="">
<cfparam name="url.c" default="">

<cfif isDefined("url.startedat")>
	<cfset sd = listgetat(url.startedat,1,"-")>
	<cfset st = listgetat(url.startedat,2,"-")>
	<cfset startDate = right(sd,2) & "/"  & mid(sd,5,2) & "/" & left(sd,4)>
	<cfset startTime = st>
<cfelse>
	<cfset startDate = dateformat(now(),'dd/mm/yyyy')>
	<cfset startTime = timeformat(now(),'HH:mm:ss')>
</cfif>

<cfif isDefined("url.endedat")>
	<cfset ed = listgetat(url.endedat,1,"-")>
	<cfset et = listgetat(url.endedat,2,"-")>
	<cfset endDate = right(ed,2) & "/"  & mid(ed,5,2) & "/" & left(ed,4)>
	<cfset endTime = et>
<cfelse>
	<cfset endDate = dateformat(now(),'dd/mm/yyyy')>
	<cfset endTime = timeformat(now(),'HH:mm:ss')>
</cfif>

<cfset condition = url.c>
<cfset accountNumber = url.an>
<cfset inquiryNumber = url.i>

<!--- API --->
<cfset apiurl = "http://192.168.28.51:8080/searchapi">
<cfset apiuser = "apiuser">
<cfset apipassword = "gocops">
<cfset recordingDir = "e:\websites\copalink\recordingmailbox\recordings\#dateformat(now(),'yyyymmdd')#">
<cfset switchcallid = "#url.rn#">
<cfset recordingName = "#switchcallid#.mp3">
<cfset tempName = "#switchcallid#.wav">
<cfset playbackDir = "#dateformat(now(),'yyyymmdd')#">
<!--- API --->

<cftry>	
	
	<!--- get recording and convert --->
	<cfhttp url="#apiurl#" method="get" result="getresults" username="#apiuser#" password="#apipassword#" >
		<cfhttpparam type="url" name="command" value="search" >
		<cfhttpparam type="url" name="layout" value="searchapi" >
		<cfhttpparam type="url" name="operator_startedat" value="9" >
		<cfhttpparam type="url" name="param1_startedat" value="#startDate#" >
		<cfhttpparam type="url" name="param2_startedat" value="#startTime#" >
		<cfhttpparam type="url" name="param3_startedat" value="#endDate#" >
		<cfhttpparam type="url" name="param4_startedat" value="#endTime#" >
		<cfhttpparam type="url" name="operator_switchcallid" value="6" >
		<cfhttpparam type="url" name="param1_switchcallid" value="#switchcallid#" >
	</cfhttp>
	
	<cfset xResult = xmlparse(getresults.Filecontent)>
	
	<cfset binList = "">
	<cfset results = xResult.results>
	<cfset recordingNum = arraylen(results.result)>
	<cfset outputcontent = "">
	<cfset idlist = "">
	<cfset wavlist = "">
	
	<cfif arraylen(results.result) gte 1>
		
		<!--- create temp directory --->
		<cfif not directoryExists("#recordingDir#")>
			<cfdirectory action="create" directory="#recordingDir#">
		</cfif>
		
		<!--- get files from recorder --->
		<cfloop from="1" to="#arraylen(results.result)#" index="i">
			<cfset result = results.result[i]>
			<cfif i is 1>
				<cfset rdt = result.field[1].xmltext>
				<cfset recordingDateTime = "#dateformat(listgetat(rdt,1,'T'),'mm/dd/yyyy')# #timeformat(listgetat(rdt,2,'T'),'HH:mm:ss')#">
			</cfif>
			<cfset id = result.xmlAttributes.inum>
			<cfset idlist = listappend(idlist,id)>
			<cfset wavlist = listappend(wavlist,"#recordingDir#\#id#.wav")>
			<cfhttp url="#apiurl#" method="get" getasbinary="auto" file="#id#.wav" path="#recordingDir#" result="getrecording" username="#apiuser#" password="#apipassword#" >
				<cfhttpparam type="url" name="command" value="replay" >
				<cfhttpparam type="url" name="id" value="#id#" >
			</cfhttp>
		</cfloop>
		
		<!--- concatinate all wav files into one recording --->
		<cfx_concatenateWavFiles files="#wavlist#" destination="#recordingDir#\#tempName#">
		
		<!--- convert recording to mp3 --->
		<cfx_wav2mp3 source="#recordingDir#\#tempName#" destination="#recordingDir#\#recordingName#" samplerate="44100"> 
		
	</cfif>
	<!--- get recording and convert --->

	<!--- clean up temp files --->
	<cfset wavlist = listappend(wavlist,"#recordingDir#\#tempName#")>
	<cfloop list="#wavlist#" index="wavFile">
		<cfif fileExists("#wavFile#")>
			<cffile action="delete" file="#wavFile#">
		</cfif>
	</cfloop>

	<cfquery name="getInquiryID" datasource="intranet">
		select inquiryid from inquiries_main where inquirynumber = '#inquiryNumber#' 
	</cfquery>
	
	<cfif getInquiryID.recordcount gt 0>
	
		<!--- check to see if recording is already attached --->
		<cfquery name="checkExistingAttachment" datasource="intranet">
			select * from inquiries_recordings 
			where inquiryid = #getInquiryID.inquiryID# and recordingName = '#recordingName#'
		</cfquery>
		<cfif checkExistingAttachment.recordcount gt 0>
			RJ|Recording already attached
		<cfelse>
			<!--- check if inquiry is closed yet or not --->
			<cfquery name="checkStatus" datasource="intranet">
				select * from inquiries_status_history 
				where inquiryid = #getInquiryID.inquiryID# and isCurrentStatus = 1 
			</cfquery>
			<cfif checkStatus.statusid is 4 or checkStatus.statusid is 5>
				RJ|Inquiry already closed
			<cfelse>
				<cfquery name="attachrecording" datasource="intranet">
					insert into inquiries_recordings (inquiryid, recordingName, accountNumber, dateTimeAttached, condition, recordingDateTime)
					values (#getInquiryID.inquiryID#, '#playbackDir#-#recordingName#', '#accountNumber#', GETDATE(), '#condition#', #createodbcdatetime(recordingDateTime)#) 
				</cfquery>
				OK|recording attached successfully
			</cfif>
		</cfif>
	
	<cfelse>
	
		RJ|inquiry doesn't exist, recording not attached
	
	</cfif>
	<cfcatch type="any"><cfoutput>RJ|#cfcatch.Message#<br />#cfcatch.Detail#</cfoutput></cfcatch>
</cftry>





