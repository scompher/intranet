
<cfquery name="GetStatus" datasource="#ds#">
	begin
		delete from inquiries_status_history
		where inquiryid = #i# and isCurrentStatus = 1
	end
	begin
		update inquiries_status_history
		set isCurrentStatus = 1
		where inquiryid = #i# and statusHistoryID = (select max(statusHistoryID) from inquiries_status_history where inquiryid = #i#)
	end
</cfquery>

<cflocation url="index.cfm">
