<html xmlns:v="urn:schemas-microsoft-com:vml"
xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:w="urn:schemas-microsoft-com:office:word"
xmlns:m="http://schemas.microsoft.com/office/2004/12/omml"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=ProgId content=Word.Document>
<meta name=Generator content="Microsoft Word 14">
<meta name=Originator content="Microsoft Word 14">
<link rel=File-List href="Electronic%20Inquiry%20Tutorial_files/filelist.xml">
<link rel=Edit-Time-Data
href="Electronic%20Inquiry%20Tutorial_files/editdata.mso">
<link rel=OLE-Object-Data
href="Electronic%20Inquiry%20Tutorial_files/oledata.mso">
<!--[if !mso]>
<style>
v\:* {behavior:url(#default#VML);}
o\:* {behavior:url(#default#VML);}
w\:* {behavior:url(#default#VML);}
.shape {behavior:url(#default#VML);}
</style>
<![endif]-->
<title>Electronic Inquiry Tutorial</title>
<!--[if gte mso 9]><xml>
 <o:DocumentProperties>
  <o:Author>Philip Gregory</o:Author>
  <o:LastAuthor>Phil Gregory</o:LastAuthor>
  <o:Revision>2</o:Revision>
  <o:TotalTime>142</o:TotalTime>
  <o:Created>2013-02-19T14:45:00Z</o:Created>
  <o:LastSaved>2013-02-19T14:45:00Z</o:LastSaved>
  <o:Pages>7</o:Pages>
  <o:Words>990</o:Words>
  <o:Characters>5091</o:Characters>
  <o:Company>COPS Monitoring</o:Company>
  <o:Lines>42</o:Lines>
  <o:Paragraphs>12</o:Paragraphs>
  <o:CharactersWithSpaces>6069</o:CharactersWithSpaces>
  <o:Version>14.00</o:Version>
 </o:DocumentProperties>
 <o:OfficeDocumentSettings>
  <o:AllowPNG/>
 </o:OfficeDocumentSettings>
</xml><![endif]-->
<link rel=themeData href="Electronic%20Inquiry%20Tutorial_files/themedata.thmx">
<link rel=colorSchemeMapping
href="Electronic%20Inquiry%20Tutorial_files/colorschememapping.xml">
<!--[if gte mso 9]><xml>
 <w:WordDocument>
  <w:SpellingState>Clean</w:SpellingState>
  <w:GrammarState>Clean</w:GrammarState>
  <w:TrackMoves>false</w:TrackMoves>
  <w:TrackFormatting/>
  <w:ValidateAgainstSchemas/>
  <w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>
  <w:IgnoreMixedContent>false</w:IgnoreMixedContent>
  <w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>
  <w:DoNotPromoteQF/>
  <w:LidThemeOther>EN-US</w:LidThemeOther>
  <w:LidThemeAsian>X-NONE</w:LidThemeAsian>
  <w:LidThemeComplexScript>X-NONE</w:LidThemeComplexScript>
  <w:Compatibility>
   <w:SelectEntireFieldWithStartOrEnd/>
   <w:UseWord2002TableStyleRules/>
   <w:DontUseIndentAsNumberingTabStop/>
   <w:FELineBreak11/>
   <w:WW11IndentRules/>
   <w:DontAutofitConstrainedTables/>
   <w:AutofitLikeWW11/>
   <w:UnderlineTabInNumList/>
   <w:HangulWidthLikeWW11/>
   <w:UseNormalStyleForList/>
   <w:DontVertAlignCellWithSp/>
   <w:DontBreakConstrainedForcedTables/>
   <w:DontVertAlignInTxbx/>
   <w:Word11KerningPairs/>
   <w:CachedColBalance/>
  </w:Compatibility>
  <m:mathPr>
   <m:mathFont m:val="Cambria Math"/>
   <m:brkBin m:val="before"/>
   <m:brkBinSub m:val="&#45;-"/>
   <m:smallFrac m:val="off"/>
   <m:dispDef/>
   <m:lMargin m:val="0"/>
   <m:rMargin m:val="0"/>
   <m:defJc m:val="centerGroup"/>
   <m:wrapIndent m:val="1440"/>
   <m:intLim m:val="subSup"/>
   <m:naryLim m:val="undOvr"/>
  </m:mathPr></w:WordDocument>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <w:LatentStyles DefLockedState="false" DefUnhideWhenUsed="true"
  DefSemiHidden="true" DefQFormat="false" DefPriority="99"
  LatentStyleCount="267">
  <w:LsdException Locked="false" Priority="0" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Normal"/>
  <w:LsdException Locked="false" Priority="9" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="heading 1"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 2"/>
  <w:LsdException Locked="false" Priority="9" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="heading 3"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 4"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 5"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 6"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 7"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 8"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 9"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 1"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 2"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 3"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 4"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 5"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 6"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 7"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 8"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 9"/>
  <w:LsdException Locked="false" Priority="35" QFormat="true" Name="caption"/>
  <w:LsdException Locked="false" Priority="10" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Title"/>
  <w:LsdException Locked="false" Priority="0" Name="Default Paragraph Font"/>
  <w:LsdException Locked="false" Priority="11" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Subtitle"/>
  <w:LsdException Locked="false" Priority="22" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Strong"/>
  <w:LsdException Locked="false" Priority="20" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Emphasis"/>
  <w:LsdException Locked="false" Priority="59" SemiHidden="false"
   UnhideWhenUsed="false" Name="Table Grid"/>
  <w:LsdException Locked="false" UnhideWhenUsed="false" Name="Placeholder Text"/>
  <w:LsdException Locked="false" Priority="1" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="No Spacing"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 1"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 1"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 1"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 1"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 1"/>
  <w:LsdException Locked="false" UnhideWhenUsed="false" Name="Revision"/>
  <w:LsdException Locked="false" Priority="34" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="List Paragraph"/>
  <w:LsdException Locked="false" Priority="29" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Quote"/>
  <w:LsdException Locked="false" Priority="30" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Intense Quote"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 1"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 1"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 1"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 1"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 1"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 1"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 2"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 2"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 2"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 2"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 2"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 2"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 2"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 2"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 2"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 2"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 2"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 3"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 3"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 3"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 3"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 3"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 3"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 3"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 3"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 3"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 3"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 3"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 4"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 4"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 4"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 4"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 4"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 4"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 4"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 4"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 4"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 4"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 4"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 5"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 5"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 5"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 5"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 5"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 5"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 5"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 5"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 5"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 5"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 5"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 6"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 6"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 6"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 6"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 6"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 6"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 6"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 6"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 6"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 6"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 6"/>
  <w:LsdException Locked="false" Priority="19" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Subtle Emphasis"/>
  <w:LsdException Locked="false" Priority="21" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Intense Emphasis"/>
  <w:LsdException Locked="false" Priority="31" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Subtle Reference"/>
  <w:LsdException Locked="false" Priority="32" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Intense Reference"/>
  <w:LsdException Locked="false" Priority="33" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Book Title"/>
  <w:LsdException Locked="false" Priority="37" Name="Bibliography"/>
  <w:LsdException Locked="false" Priority="39" QFormat="true" Name="TOC Heading"/>
 </w:LatentStyles>
</xml><![endif]-->
<style>
<!--
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-parent:"";
	margin:0in;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:10.0pt;
	mso-bidi-font-size:12.0pt;
	font-family:"Arial","sans-serif";
	mso-fareast-font-family:"Times New Roman";}
h1
	{mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-next:Normal;
	margin:0in;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	page-break-after:avoid;
	mso-outline-level:1;
	font-size:10.0pt;
	mso-bidi-font-size:12.0pt;
	font-family:"Arial","sans-serif";
	mso-font-kerning:0pt;}
h3
	{mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-next:Normal;
	margin-top:12.0pt;
	margin-right:0in;
	margin-bottom:3.0pt;
	margin-left:0in;
	mso-pagination:widow-orphan;
	page-break-after:avoid;
	mso-outline-level:3;
	font-size:13.0pt;
	font-family:"Arial","sans-serif";}
p.MsoBodyText, li.MsoBodyText, div.MsoBodyText
	{mso-style-noshow:yes;
	mso-style-unhide:no;
	margin:0in;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:10.0pt;
	mso-bidi-font-size:12.0pt;
	font-family:"Arial","sans-serif";
	mso-fareast-font-family:"Times New Roman";
	font-style:italic;}
span.SpellE
	{mso-style-name:"";
	mso-spl-e:yes;}
span.GramE
	{mso-style-name:"";
	mso-gram-e:yes;}
@page WordSection1
	{size:8.5in 11.0in;
	margin:1.0in 1.25in 1.0in 1.25in;
	mso-header-margin:.5in;
	mso-footer-margin:.5in;
	mso-paper-source:0;}
div.WordSection1
	{page:WordSection1;}
-->
</style>
<!--[if gte mso 10]>
<style>
 /* Style Definitions */
 table.MsoNormalTable
	{mso-style-name:"Table Normal";
	mso-tstyle-rowband-size:0;
	mso-tstyle-colband-size:0;
	mso-style-noshow:yes;
	mso-style-priority:99;
	mso-style-parent:"";
	mso-padding-alt:0in 5.4pt 0in 5.4pt;
	mso-para-margin:0in;
	mso-para-margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:10.0pt;
	font-family:"Times New Roman","serif";}
</style>
<![endif]--><!--[if gte mso 9]><xml>
 <o:shapedefaults v:ext="edit" spidmax="2050"/>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <o:shapelayout v:ext="edit">
  <o:idmap v:ext="edit" data="1"/>
 </o:shapelayout></xml><![endif]-->
</head>

<body lang=EN-US style='tab-interval:.5in'>

<div class=WordSection1>

<div style='mso-element:para-border-div;border:none;border-bottom:solid windowtext 1.0pt;
mso-border-bottom-alt:solid windowtext .75pt;padding:0in 0in 1.0pt 0in'>

<h3 style='border:none;mso-border-bottom-alt:solid windowtext .75pt;padding:
0in;mso-padding-alt:0in 0in 1.0pt 0in'>Electronic Inquiry Tutorial</h3>

</div>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<p class=MsoNormal>The purpose of this tutorial is to give you an understanding
of how the new electronic inquiry system works.<span style='mso-spacerun:yes'>�
</span>Before you can start using this new system you�ll need a COPS Monitoring
Intranet account, and the proper access.</p>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<p class=MsoNormal>After you log into the Intranet, you�ll see a new menu
section:</p>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<p class=MsoNormal><!--[if gte vml 1]><v:shapetype id="_x0000_t75" coordsize="21600,21600"
 o:spt="75" o:preferrelative="t" path="m@4@5l@4@11@9@11@9@5xe" filled="f"
 stroked="f">
 <v:stroke joinstyle="miter"/>
 <v:formulas>
  <v:f eqn="if lineDrawn pixelLineWidth 0"/>
  <v:f eqn="sum @0 1 0"/>
  <v:f eqn="sum 0 0 @1"/>
  <v:f eqn="prod @2 1 2"/>
  <v:f eqn="prod @3 21600 pixelWidth"/>
  <v:f eqn="prod @3 21600 pixelHeight"/>
  <v:f eqn="sum @0 0 1"/>
  <v:f eqn="prod @6 1 2"/>
  <v:f eqn="prod @7 21600 pixelWidth"/>
  <v:f eqn="sum @8 21600 0"/>
  <v:f eqn="prod @7 21600 pixelHeight"/>
  <v:f eqn="sum @10 21600 0"/>
 </v:formulas>
 <v:path o:extrusionok="f" gradientshapeok="t" o:connecttype="rect"/>
 <o:lock v:ext="edit" aspectratio="t"/>
</v:shapetype><v:shape id="_x0000_i1025" type="#_x0000_t75" style='width:222pt;
 height:83.25pt' o:ole="">
 <v:imagedata src="Electronic%20Inquiry%20Tutorial_files/image001.png" o:title=""/>
</v:shape><![endif]--><![if !vml]><img width=296 height=111
src="Electronic%20Inquiry%20Tutorial_files/image002.jpg" v:shapes="_x0000_i1025"><![endif]><!--[if gte mso 9]><xml>
 <o:OLEObject Type="Embed" ProgID="Photoshop.Image.8" ShapeID="_x0000_i1025"
  DrawAspect="Content" ObjectID="_1422772281">
  <o:WordFieldCodes>\s</o:WordFieldCodes>
 </o:OLEObject>
</xml><![endif]--></p>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<p class=MsoNormal>Here you will be able to create inquiries, as well as pick
them up if any have been assigned to your department.</p>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<h1>Create New Inquiry</h1>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<p class=MsoNormal>Let�s start off by creating a new Inquiry </p>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<p class=MsoNormal><!--[if gte vml 1]><v:shape id="_x0000_i1026" type="#_x0000_t75"
 style='width:6in;height:135pt' o:ole="">
 <v:imagedata src="Electronic%20Inquiry%20Tutorial_files/image003.png" o:title=""/>
</v:shape><![endif]--><![if !vml]><img width=576 height=180
src="Electronic%20Inquiry%20Tutorial_files/image004.jpg" v:shapes="_x0000_i1026"><![endif]><!--[if gte mso 9]><xml>
 <o:OLEObject Type="Embed" ProgID="Photoshop.Image.8" ShapeID="_x0000_i1026"
  DrawAspect="Content" ObjectID="_1422772282">
  <o:WordFieldCodes>\s</o:WordFieldCodes>
 </o:OLEObject>
</xml><![endif]--></p>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<p class=MsoNormal>Initially enter the receiver, account number, dealer number,
category (if category is not on the list, select �Other� and manually enter a
category � these are reviewed and potentially added to the list), and the date
the incident occurred.</p>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<p class=MsoNormal>The system will then search for other inquiries of this
nature and if there are none, will continue to complete the rest of the inquiry
information.<span style='mso-spacerun:yes'>� </span>If there is another inquiry
that matches these criteria, this message will be displayed:</p>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<p class=MsoNormal><!--[if gte vml 1]><v:shape id="_x0000_i1027" type="#_x0000_t75"
 style='width:363pt;height:71.25pt' o:ole="">
 <v:imagedata src="Electronic%20Inquiry%20Tutorial_files/image005.png" o:title=""/>
</v:shape><![endif]--><![if !vml]><img width=484 height=95
src="Electronic%20Inquiry%20Tutorial_files/image005.png" v:shapes="_x0000_i1027"><![endif]><!--[if gte mso 9]><xml>
 <o:OLEObject Type="Embed" ProgID="Photoshop.Image.8" ShapeID="_x0000_i1027"
  DrawAspect="Content" ObjectID="_1422772283">
  <o:WordFieldCodes>\s</o:WordFieldCodes>
 </o:OLEObject>
</xml><![endif]--></p>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<p class=MsoNormal>You can click on the link inside the paragraph to view the
inquiry details, if you feel the inquiry you are creating is not a duplicate,
click the �Continue and create a new inquiry� button to continue creating the
inquiry</p>

<p class=MsoNormal>Here is what the full inquiry creation screen looks like:</p>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<p class=MsoNormal><!--[if gte vml 1]><v:shape id="_x0000_i1038" type="#_x0000_t75"
 style='width:6in;height:484.5pt'>
 <v:imagedata src="Electronic%20Inquiry%20Tutorial_files/image006.jpg" o:title="newInquiryScreen"/>
</v:shape><![endif]--><![if !vml]><img width=576 height=646
src="Electronic%20Inquiry%20Tutorial_files/image007.jpg" v:shapes="_x0000_i1038"><![endif]></p>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<p class=MsoNormal>Every field is required, except for the codes and zones and �Place
Recording on MPower� selection. </p>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span
style='color:red'>If you are planning to post a recording to MPower, this
checkbox MUST be checked.<span style='mso-spacerun:yes'>� </span>Once you
generate the inquiry, the recording must be attached to the inquiry via our in-house
software, Generations.<span style='mso-spacerun:yes'>� </span><o:p></o:p></span></b></p>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<p class=MsoNormal>If you�ve done some research for this inquiry already, you
may add that research to the inquiry by pressing the �Add Research� menu item.</p>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<p class=MsoNormal>You can attach files by clicking on the �browse� button to
locate the file on your computer, then �attach file�.<span
style='mso-spacerun:yes'>� </span>There is no limit to the number of files you
can attach to the inquiry.<span style='mso-spacerun:yes'>� </span></p>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<p class=MsoNormal><span class=GramE>Once complete, click on �Generate Inquiry�
to create your inquiry.</span> A message will be displayed indicating you
should call and inform the receiving department you�ve created an inquiry and
it�s waiting to be picked up.</p>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<p class=MsoNormal>This is what that message looks like: </p>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<p class=MsoNormal><!--[if gte vml 1]><v:shape id="_x0000_i1029" type="#_x0000_t75"
 style='width:5in;height:90pt' o:ole="">
 <v:imagedata src="Electronic%20Inquiry%20Tutorial_files/image008.png" o:title=""/>
</v:shape><![endif]--><![if !vml]><img width=480 height=120
src="Electronic%20Inquiry%20Tutorial_files/image009.jpg" v:shapes="_x0000_i1029"><![endif]><!--[if gte mso 9]><xml>
 <o:OLEObject Type="Embed" ProgID="Photoshop.Image.8" ShapeID="_x0000_i1029"
  DrawAspect="Content" ObjectID="_1422772284">
  <o:WordFieldCodes>\s</o:WordFieldCodes>
 </o:OLEObject>
</xml><![endif]--></p>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span
style='color:red'>NOTE: If you are planning to attach a recording to this
inquiry via Generations, make a note of this inquiry #, as the employee will
need to know which inquiry number to attach the recording. <o:p></o:p></span></b></p>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<h1>Picking up an Inquiry</h1>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<p class=MsoNormal>If you are picking up inquires in your department, simply
click on the �Pick up inquiry� menu item in the Inquires section of the
Intranet.</p>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<p class=MsoNormal>Once there, you�ll be presented with a listing of inquiries
by department that have been or need to be picked up.</p>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<p class=MsoNormal><!--[if gte vml 1]><v:shape id="_x0000_i1030" type="#_x0000_t75"
 style='width:431.25pt;height:238.5pt'>
 <v:imagedata src="Electronic%20Inquiry%20Tutorial_files/image010.jpg" o:title="pickup"/>
</v:shape><![endif]--><![if !vml]><img width=575 height=318
src="Electronic%20Inquiry%20Tutorial_files/image011.jpg" v:shapes="_x0000_i1030"><![endif]></p>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<p class=MsoNormal>Here you can view, print, and pick-up inquiries.<span
style='mso-spacerun:yes'>� </span>The list defaults to inquiries sent to your department;
however there is a drop-down list, which allows you to view all or other
department inquiries.<span style='mso-spacerun:yes'>� </span>This is helpful if
you are checking on the status of an inquiry you sent to another department.</p>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<p class=MsoNormal>There is also a status filter, which allows you to view only
inquiries of the selected status.<span style='mso-spacerun:yes'>� </span>For
example, you can view only open inquiries, or only new non-picked up inquiries.</p>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<p class=MsoNormal>To view or print an inquiry, simply click on the inquiry and
select �view/print� from the pop up menu.<span style='mso-spacerun:yes'>�
</span></p>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:"Arial","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:
EN-US;mso-bidi-language:AR-SA'><br clear=all style='page-break-before:always'>
</span>

<p class=MsoNormal><span class=GramE>To open or pick up an inquiry, select
�open inquiry� or �pick up inquiry� from the pop up menu.</span><span
style='mso-spacerun:yes'>� </span></p>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<p class=MsoNormal>This will take you to the opened inquiry screen:</p>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<p class=MsoNormal><!--[if gte vml 1]><v:shape id="_x0000_i1031" type="#_x0000_t75"
 style='width:431.25pt;height:403.5pt'>
 <v:imagedata src="Electronic%20Inquiry%20Tutorial_files/image012.jpg" o:title="Untitled-1"/>
</v:shape><![endif]--><![if !vml]><img width=575 height=538
src="Electronic%20Inquiry%20Tutorial_files/image013.jpg" v:shapes="_x0000_i1031"><![endif]></p>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<p class=MsoNormal>The screenshot is cut-off, but here are the different
parts.<span style='mso-spacerun:yes'>� </span>Along the top is the inquiry
number, along with all the information provided when the inquiry was created.
Below that are sections where you can add the research, conclusion, and
resolution to the inquiry.<span style='mso-spacerun:yes'>� </span>To add each
section simply <span class=GramE>fill</span> out the text area and click it�s
respective save button.</p>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<p class=MsoNormal>If you wish to exit the inquiry without making any changes,
click on the upper-right red X or the �Return to Inquiries� menu item.</p>

<span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:"Arial","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:
EN-US;mso-bidi-language:AR-SA'><br clear=all style='page-break-before:always'>
</span>

<p class=MsoNormal><b>Forwarding an Inquiry<o:p></o:p></b></p>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<p class=MsoNormal>If you need to forward the inquiry, simply click on the
�forward inquiry� menu item and you are presented with this screen:</p>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<p class=MsoNormal><!--[if gte vml 1]><v:shape id="_x0000_i1032" type="#_x0000_t75"
 style='width:383.25pt;height:233.25pt'>
 <v:imagedata src="Electronic%20Inquiry%20Tutorial_files/image014.jpg" o:title="forward"/>
</v:shape><![endif]--><![if !vml]><img width=511 height=311
src="Electronic%20Inquiry%20Tutorial_files/image014.jpg" v:shapes="_x0000_i1032"><![endif]></p>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<p class=MsoNormal>Choose which department you are sending the inquiry to,
along with the reason you are forwarding the inquiry, and click �Forward
Inquiry�.</p>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<h1>Adding a Contact Attempt</h1>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<p class=MsoNormal>If the inquiry is completed and you are contacting the
dealer or subscriber regarding the inquiry, you must fill out the contact
attempts.<span style='mso-spacerun:yes'>� </span>Clicking on the �Add contact
attempt� menu item does this. </p>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<p class=MsoNormal>A new section will appear at the bottom of the inquiry,
which looks like this:</p>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<p class=MsoNormal><!--[if gte vml 1]><v:shape id="_x0000_i1033" type="#_x0000_t75"
 style='width:5in;height:44.25pt' o:ole="">
 <v:imagedata src="Electronic%20Inquiry%20Tutorial_files/image015.png" o:title=""/>
</v:shape><![endif]--><![if !vml]><img width=480 height=59
src="Electronic%20Inquiry%20Tutorial_files/image016.jpg" v:shapes="_x0000_i1033"><![endif]><!--[if gte mso 9]><xml>
 <o:OLEObject Type="Embed" ProgID="Photoshop.Image.8" ShapeID="_x0000_i1033"
  DrawAspect="Content" ObjectID="_1422772285">
  <o:WordFieldCodes>\s</o:WordFieldCodes>
 </o:OLEObject>
</xml><![endif]--></p>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<p class=MsoNormal>Select which contact attempt you are performing, (1<sup>st</sup>,
2<sup>nd</sup>, or actually contacted the person) and you are presented with
the following section:</p>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<p class=MsoNormal>For 1<sup>st</sup> and 2<sup>nd</sup> attempts, left message
for call back:</p>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<p class=MsoNormal><!--[if gte vml 1]><v:shape id="_x0000_i1034" type="#_x0000_t75"
 style='width:5in;height:111pt' o:ole="">
 <v:imagedata src="Electronic%20Inquiry%20Tutorial_files/image017.png" o:title=""/>
</v:shape><![endif]--><![if !vml]><img width=480 height=148
src="Electronic%20Inquiry%20Tutorial_files/image018.jpg" v:shapes="_x0000_i1034"><![endif]><!--[if gte mso 9]><xml>
 <o:OLEObject Type="Embed" ProgID="Photoshop.Image.8" ShapeID="_x0000_i1034"
  DrawAspect="Content" ObjectID="_1422772286">
  <o:WordFieldCodes>\s</o:WordFieldCodes>
 </o:OLEObject>
</xml><![endif]--></p>

<span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:"Arial","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-ansi-language:EN-US;mso-fareast-language:
EN-US;mso-bidi-language:AR-SA'><br clear=all style='page-break-before:always'>
</span>

<p class=MsoNormal>For notifying the dealer:</p>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<p class=MsoNormal><!--[if gte vml 1]><v:shape id="_x0000_i1035" type="#_x0000_t75"
 style='width:5in;height:117pt' o:ole="">
 <v:imagedata src="Electronic%20Inquiry%20Tutorial_files/image019.png" o:title=""/>
</v:shape><![endif]--><![if !vml]><img width=480 height=156
src="Electronic%20Inquiry%20Tutorial_files/image020.jpg" v:shapes="_x0000_i1035"><![endif]><!--[if gte mso 9]><xml>
 <o:OLEObject Type="Embed" ProgID="Photoshop.Image.8" ShapeID="_x0000_i1035"
  DrawAspect="Content" ObjectID="_1422772287">
  <o:WordFieldCodes>\s</o:WordFieldCodes>
 </o:OLEObject>
</xml><![endif]--></p>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<p class=MsoNormal>In either case, fill out the information completely and
click on the �Save Attempt Information� button.</p>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<p class=MsoBodyText><b>If you are not the person doing the follow-up, forward
the inquiry to whoever is.<span style='mso-spacerun:yes'>� </span>Do not close
the inquiry without someone entering the contact attempt information.<o:p></o:p></b></p>

<p class=MsoNormal><b><o:p>&nbsp;</o:p></b></p>

<h1>Closing the Inquiry</h1>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<p class=MsoNormal>When everything is done with the inquiry, it�s time to close
it.<span style='mso-spacerun:yes'>� </span>Once an inquiry is closed, no
further modifications can be made to it. </p>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<p class=MsoNormal>Clicking on the �Close this inquiry� menu item will close
the inquiry.<span style='mso-spacerun:yes'>� </span>You are presented with a
drop down list to indicate the conclusion of the inquiry.<span
style='mso-spacerun:yes'>� </span>Currently there is Founded, Unfounded,
In-house founded and panel programming.<span style='mso-spacerun:yes'>� </span></p>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<p class=MsoNormal>Then choose which department it applies to. </p>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<p class=MsoNormal><!--[if gte vml 1]><v:shape id="_x0000_i1036" type="#_x0000_t75"
 style='width:379.5pt;height:183pt'>
 <v:imagedata src="Electronic%20Inquiry%20Tutorial_files/image021.jpg" o:title="close"/>
</v:shape><![endif]--><![if !vml]><img width=506 height=244
src="Electronic%20Inquiry%20Tutorial_files/image021.jpg" v:shapes="_x0000_i1036"><![endif]></p>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<p class=MsoNormal>Next click the �close inquiry� button to close the
inquiry.<span style='mso-spacerun:yes'>� </span>On the inquiry pick up screen,
it will now be listed as closed.</p>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span
style='color:red'>NOTE: If you are attaching a recording to post to MPower, you
will be prompted one last time to be sure you want that recording (or
recordings) posted to MPower. If you are sure, click yes and the process will
continue.<span style='mso-spacerun:yes'>� </span>Once you click the �close
inquiry� button above, the recording(s) are posted to MPower. <o:p></o:p></span></b></p>

<p class=MsoNormal><b style='mso-bidi-font-weight:normal'><span
style='color:red'><o:p>&nbsp;</o:p></span></b></p>

</div>

</body>

</html>
