

<cfparam name="errlist" default="">
<cfparam name="url.an" default="">
<cfparam name="url.rn" default="">
<cfparam name="url.i" default="">
<cfparam name="url.c" default="">
<cfparam name="url.rdt" default="#dateformat(now(),'mmddyyyy')##timeformat(now(),'HHmmss')#">

<cfset accountNumber = "#checkSalesFollowUp.rec#-#checkSalesFollowUp.acct#">
<cfset inquiryNumber = "#checkSalesFollowUp.inquiryNumber#">
<cfset rdt = url.rdt>

<cfset rd = left(url.rdt,8)>
<cfset recordingDate = left(rd,2) & "/" & mid(rd,3,2) & "/" & right(rd,4)>
<cfset rt = right(url.rdt,6)>
<cfset recordingTime = left(rt,2) & ":" & mid(rt,3,2) & ":" & right(rt,2)>

<cfset recordingDateTime = recordingDate & " " & recordingTime>

<cfquery name="getAttachedRecordings" datasource="#ds#">
	select * from inquiries_attachments 
	where inquiryID = #form.inquiryid# and savedFile like '%.wav'
</cfquery>

<!--- loop through wav attachments and make them mp3 recordings to post to MPower --->
<cfloop query="getAttachedRecordings">
	<cfset recordingName = getAttachedRecordings.savedFile>
	<cftry>
		<cfset recordingDir = dateformat(now(),'yyyymmdd')>
		<cfset sourceFile = "e:\websites\intranet\inquiry\attachments\#recordingName#">
		<cfif not directoryExists("e:\websites\copalink\recordingmailbox\recordings\#recordingDir#")>
			<cfdirectory action="create" directory="e:\websites\copalink\recordingmailbox\recordings\#recordingDir#">
		</cfif>
		<cfset recordingName = replace(recordingName,"_","-","all")>
		<cfset destinationFile = "e:\websites\copalink\recordingmailbox\recordings\#recordingDir#\000_#recordingDir#_#replace(recordingName,".wav",".mp3","all")#">
		
		<!--- convert recording from wav to mp3 and place on web server --->
		<cfx_wav2mp3 source="#sourceFile#" destination="#destinationFile#" samplerate="44100"> 
		
		<cfset recordingName = replace(recordingName,".wav",".mp3","all")>
		
		<cfquery name="getInquiryID" datasource="intranet">
			select inquiryid from inquiries_main where inquirynumber = '#inquiryNumber#' 
		</cfquery>
		<cfif getInquiryID.recordcount gt 0>
			<!--- check to see if recording is already attached --->
			<cfquery name="checkExistingAttachment" datasource="intranet">
				select * from inquiries_recordings 
				where inquiryid = #getInquiryID.inquiryID# and recordingName = '#recordingName#'
			</cfquery>
			<cfif checkExistingAttachment.recordcount gt 0>
				<cfset errlist = listappend(errlist, "Recording #recordingName# already attached", "~")>
			<cfelse>
				<!--- check if inquiry is closed yet or not --->
				<cfquery name="checkStatus" datasource="intranet">
					select * from inquiries_status_history 
					where inquiryid = #getInquiryID.inquiryID# and isCurrentStatus = 1 
				</cfquery>
				<cfif checkStatus.statusid is 4 or checkStatus.statusid is 5>
					<cfset errlist = listappend(errlist, "Inquiry already closed", "~")>
				<cfelse>
					<cfquery name="attachrecording" datasource="intranet">
						insert into inquiries_recordings (inquiryid, recordingName, accountNumber, dateTimeAttached, condition, recordingDateTime)
						values (#getInquiryID.inquiryID#, '000_#recordingDir#_#recordingName#', '#accountNumber#', GETDATE(), '', #createodbcdatetime(recordingDateTime)#) 
					</cfquery>
				</cfif>
			</cfif>
		<cfelse>
			<cfset errlist = listappend(errlist, "inquiry doesn't exist, recording not attached", "~")>
		</cfif>
		<!--- remove from attachments 
		<cfquery name="delAttachment" datasource="#ds#">
			delete from inquiries_attachments 
			where attachmentID = #getAttachedRecordings.attachmentID# 
		</cfquery>
		--->
		<cfcatch type="any">
			<cfset errlist = listappend(errlist, "#cfcatch.Message# #cfcatch.Detail#", "~")>
		</cfcatch>
	</cftry>
</cfloop>


