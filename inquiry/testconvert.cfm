
<cfxml variable="xmlRequest">
<cfoutput>
<request>
	<command>intranetBasicInfo</command>
	<data>
		<accountNumber>99-1000</accountNumber>
	</data>
</request>
</cfoutput>
</cfxml>
<cf_copalink command="#xmlRequest#">
<cfset xResult = xmlparse(result)>

<cfset accountName = xResult.response.data.basicInformation.name.xmlText>
<cfset address = xResult.response.data.basicInformation.address.xmlText>
<cfset phone = xResult.response.data.basicInformation.ctvPhone1.xmlText>


<cfdump var="#xresult#">

<cfabort>

<cfset form.inquiryid = "2978">

<cfquery name="checkSalesFollowUp" datasource="#ds#">
	select * from inquiries_main 
	where inquiryid = #form.inquiryid#
</cfquery>
<cfquery name="checkContactAttempt" datasource="#ds#">
	select * from inquiries_contactAttempts
	where inquiryid = #form.inquiryid# and contactResult <> '' 
</cfquery>

<!--- 12/21/2012 : PG : put recording in MPower mailbox if box is selected --->
<cfif checkSalesFollowUp.recordingDownload is not 0>
	<cfquery name="getRecordings" datasource="#ds#">
		select * from inquiries_recordings where inquiryid = #form.inquiryid#
	</cfquery>
	<cfloop query="getRecordings">
		
		<cfset expirationDate = dateadd("d",30,now())>	
		
		<!--- convert recording to mp3 and place on web server --->
		<cfset recordingDir = listgetat(recordingName,2,"_")>
		<cfset sourceFile = "\\192.168.9.234\recordings\#recordingDir#\#recordingName#">
		<cfif not directoryExists("e:\websites\copalink\recordingmailbox\recordings\#recordingDir#")>
			<cfdirectory action="create" directory="e:\websites\copalink\recordingmailbox\recordings\#recordingDir#">
		</cfif>
		<cfset destinationFile = "e:\websites\copalink\recordingmailbox\recordings\#recordingDir#\#replace(recordingName,".wav",".mp3","all")#">
		
		<cfx_wav2mp3 source="#sourceFile#" destination="#destinationFile#" samplerate="8000"> 
		
		<cfabort>
		<cfquery name="publishRecording" datasource="copalink">
			insert into dealer_recordings (dealerNumber, accountNumber, inquiryid, recordingDateTime, category, accountName, expirationDate, recordingName) 
			values ('#checkSalesFollowUp.dealerNumber#', '#checkSalesFollowUp.rec#-#checkSalesFollowUp.acct#', #form.inquiryid#, getdate(), '#getRecordings.condition#', '#checkSalesFollowUp.subscriberName#',#createodbcdatetime(expirationDate)#, '#recordingName#') 
		</cfquery>
	</cfloop>
</cfif>