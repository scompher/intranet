
<cfif isDefined("form.btnSaveAppend")>
	<cfquery name="getInqText" datasource="#ds#">
		select * from inquiries_main 
		where inquiryid = #i# 
	</cfquery>
	<cfset dateTimeAppended = dateformat(now(),'mm/dd/yyyy') & " " & timeformat(now(),'hh:mm tt')>
	<cfquery name="getWhoAppended" datasource="#ds#">
		select * from admin_users 
		where adminuserid = #cookie.adminlogin#
	</cfquery>
	<cfset whoAppended = getWhoAppended.firstname & " " & getWhoAppended.lastname>
	<cfset appendStuff = getInqText.inquiryText & chr(13) & chr(13) & "Additional information added on " & dateTimeAppended & " by " & whoAppended & chr(13) & form.appendText>
	<cfquery name="Append" datasource="#ds#">
		update inquiries_main 
		set inquiryText = '#appendStuff#' 
		where inquiryid = #i# 
	</cfquery>
	<cflocation url="appendInquiry.cfm?i=#i#">
</cfif>

<cfparam name="forwardInquiry" default="0">
<cfparam name="closeInquiry" default="0">
<cfparam name="form.inquiryIsID" default="0">
<cfparam name="form.conclusionOptionID" default="0">
<cfparam name="form.resolutionOptionID" default="0">
<cfparam name="form.contactAttemptID" default="0">
<cfparam name="form.btnSaveAttempt" default="0">
<cfparam name="form.researchText" default="">
<cfparam name="form.conclusionText" default="">
<cfparam name="form.resolutionText" default="">

<cfif isdefined("form.btnAttach") and form.fileAttachment is not "">
	<cffile action="upload" filefield="fileAttachment" destination="#request.DirectPath#\inquiry\attachments\" nameconflict="makeunique">
	<cfset savedFile = file.ServerFile>
	<cfset uploadedFile = file.ClientFile>
	<cfset safeFileName = replace(savedFile, " ", "", "all")>
	<cfset safeFileName = replace(safeFileName, "##", "", "all")>
	<cffile action="rename" source="#request.DirectPath#\inquiry\attachments\#savedFile#" destination="#request.DirectPath#\inquiry\attachments\#safeFileName#">
	<cfquery name="saveAttachmentInfo" datasource="#ds#">
		insert into inquiries_attachments (savedFile, clientFile, inquiryid)
		values ('#safeFileName#', '#safeFileName#', #i#)
	</cfquery>
</cfif>

<cfif isDefined("url.removeAttachment")>
	<cfquery name="getAttachmentInfo" datasource="#ds#">
		select * 
		from inquiries_attachments
		where attachmentid = #removeAttachment# and inquiryid = #i#
	</cfquery>
	<cfif getAttachmentInfo.recordcount GT 0>
		<cfif fileExists("#request.DirectPath#\inquiry\attachments\#getAttachmentInfo.savedFile#")>
			<cffile action="delete" file="#request.DirectPath#\inquiry\attachments\#getAttachmentInfo.savedFile#">
		</cfif>
		<cfquery name="delDatabaseEntry" datasource="#ds#">
			delete from inquiries_attachments
			where inquiryid = #i# and attachmentid = #removeAttachment#
		</cfquery>
	</cfif>
</cfif>

<!--- <cfif isDefined("form.btnSaveAttempt")> --->
<cfif form.btnSaveAttempt is 1>
	<cfset attemptDateTime = createodbcdatetime("#form.attemptDate# #form.attemptHH#:#form.attemptMM# #form.attemptTT#")>
	<cfif form.contactAttemptID is not 3>
		<cfquery name="saveAttempt" datasource="#ds#">
			if not exists (select * from inquiries_contactAttempts where inquiryid = #i# and contactAttemptID = #form.contactAttemptID#)
			begin
				insert into inquiries_contactAttempts (inquiryID, contactAttemptID, leftMsgWith, leftMsgWithPerson, attemptDateTime, comments)
				values (#i#, #form.contactAttemptID#, '#form.leftMsgWith#', '#form.leftMsgWithPerson#', #attemptDateTime#, '#form.comments#')
			end
		</cfquery>
	<cfelse>
		<cfquery name="saveAttempt" datasource="#ds#">
			if not exists (select * from inquiries_contactAttempts where inquiryid = #i# and contactAttemptID = #form.contactAttemptID#)
			begin
				insert into inquiries_contactAttempts (inquiryID, contactAttemptID, spokeTo, attemptDateTime, contactResult, comments)
				values (#i#, #form.contactAttemptID#, '#form.spokeTo#', #attemptDateTime#, '#form.contactResult#', '#form.comments#')
			end
			<cfif listfindnocase(form.contactResult,"Sales follow up requested") is not 0>
			begin
				update inquiries_main 
				set salesFollowUp = 1
				where inquiryid = #i#
			end
			</cfif>
		</cfquery>
	</cfif>
	<cfset contactAttemptID = 0>
	<cfset addContactAttempt = 0>
</cfif>

<cfquery name="checkStatus" datasource="#ds#">
	select * from inquiries_status_history
	where inquiryid = #i# and isCurrentStatus = 1
</cfquery>

<cfif checkStatus.statusid is 4 or checkStatus.statusid is 5>
	<cfif listfind(departmentlist, 3) is not 0>
		<cfset menuOptions = "forwardInquiry,addContactAttempt">
	<cfelse>
		<cfset menuOptions = "">
	</cfif>
<cfelse>
	<cfset menuOptions = "">
</cfif>

<cfinclude template="header.cfm">

<!--- get status history --->
<cfquery name="getStatusHist" datasource="#ds#">
	select 
	inquiries_status_history.*, 
	inquiries_status.status,
	statusUsers.firstname, 
	statusUsers.lastname, 
	fromDepartment.department as fromDeptName, 
	toDepartment.department as toDeptName
	from inquiries_status_history
	left join inquiries_status on inquiries_status_history.statusid = inquiries_status.statusid
	left join admin_users as statusUsers on inquiries_status_history.adminuserid = statusUsers.adminuserid
	left join admin_users_departments as fromDepartment on inquiries_status_history.fromDepartmentid = fromDepartment.departmentid
	left join admin_users_departments as toDepartment on inquiries_status_history.toDepartmentid = toDepartment.departmentid
	where inquiries_status_history.inquiryid = #i#
	order by dateTimeUpdated ASC
</cfquery>

<cfquery name="getInq" datasource="#ds#">
	select 
	inquiries_main.*, 
	inquiries_categories.category, 
	inquiries_inquiryIsOptions.*, 
	inquiries_attachments.attachmentid, 
	inquiries_attachments.savedFile, 
	inquiries_attachments.clientFile 
	from inquiries_main
	left join inquiries_inquiryIsOptions on inquiries_main.inquiryIsID = inquiries_inquiryIsOptions.inquiryIsID
	left join inquiries_attachments on inquiries_main.inquiryID = inquiries_attachments.inquiryID
	left join inquiries_categories on inquiries_main.categoryid = inquiries_categories.categoryid
	where inquiries_main.inquiryid = #i#
</cfquery>

<cfquery name="getResearch" datasource="#ds#">
	select 
	inquiries_research.*, 
	admin_users.firstname, 
	admin_users.lastname 
	from inquiries_research 
	left join admin_users on inquiries_research.creatorid = admin_users.adminuserid
	where inquiries_research.inquiryid = #i#
</cfquery>

<cfquery name="getconclusion" datasource="#ds#">
	select 
	inquiries_conclusion.*, 
	inquiries_conclusion_options_lookup.conclusionOptionID, 
	inquiries_conclusion_options.conclusionOption, 
	admin_users.firstname, 
	admin_users.lastname 
	from inquiries_conclusion 
	left join inquiries_conclusion_options_lookup on inquiries_conclusion.conclusionID = inquiries_conclusion_options_lookup.conclusionID 
	left join inquiries_conclusion_options on inquiries_conclusion_options_lookup.conclusionOptionID = inquiries_conclusion_options.conclusionOptionID
	left join admin_users on inquiries_conclusion.creatorid = admin_users.adminuserid
	where inquiries_conclusion.inquiryid = #i#
	order by inquiries_conclusion.conclusionID, conclusionOption
</cfquery>

<cfquery name="getresolution" datasource="#ds#">
	select 
	inquiries_resolution.*, 
	inquiries_resolution_options_lookup.resolutionOptionID, 
	inquiries_resolution_options.resolutionOption, 
	admin_users.firstname, 
	admin_users.lastname 
	from inquiries_resolution 
	left join inquiries_resolution_options_lookup on inquiries_resolution.resolutionID = inquiries_resolution_options_lookup.resolutionID 
	left join inquiries_resolution_options on inquiries_resolution_options_lookup.resolutionOptionID = inquiries_resolution_options.resolutionOptionID
	left join admin_users on inquiries_resolution.creatorid = admin_users.adminuserid
	where inquiries_resolution.inquiryid = #i#
	order by inquiries_resolution.resolutionID, resolutionOption
</cfquery>

<cfquery name="getFollowUps" datasource="#ds#">
	select 
	inquiries_followUp.*,
	admin_users.firstname, admin_users.lastname
	from inquiries_followUp
	left join admin_users on inquiries_followUp.adminuserid = admin_users.adminuserid
	where inquiries_followUp.inquiryid = #i#
</cfquery>

<cfquery name="getConclusionOptions" datasource="#ds#">
	select * 
	from inquiries_conclusion_options
	order by conclusionOption asc
</cfquery>

<cfquery name="getresolutionOptions" datasource="#ds#">
	select * 
	from inquiries_resolution_options
	order by resolutionOption asc
</cfquery>

<cfquery name="getInquiryIsOptions" datasource="#ds#">
	select * 
	from inquiries_inquiryIsOptions
	order by inquiryIsOrder
</cfquery>

<cfquery name="getCurrentStatus" dbtype="query">
	select * from getStatusHist
	where isCurrentStatus = 1 
</cfquery>

<cfif getCurrentStatus.statusid is 4 or getCurrentStatus.statusid is 5>
	<cfparam name="addResearch" default="0">
	<cfparam name="addResolution" default="0">
	<cfparam name="addConclusion" default="0">
<cfelse>
	<cfparam name="addResearch" default="1">
	<cfparam name="addResolution" default="1">
	<cfparam name="addConclusion" default="1">
</cfif>

<cfparam name="forwardInquiry" default="0">
<cfparam name="salesFollowUp" default="0">
<cfparam name="addContactAttempt" default="0">
<cfparam name="personalFollowUp" default="0">

<script language="JavaScript" type="text/javascript">
function changeCat(inqid) {
	window.open("changeCat.cfm?i=<cfoutput>#i#</cfoutput>","changeCatWin","width=400,height=150,scrollbars=0");
}
function showEditWin(url) {
	window.open(url,"editWin","width=515,height=200,scrollbars=0");
}
function checkContactAttempt(frm, attempt) {
	if (attempt == 3) {
		if (!frm.contactResult[0].checked && !frm.contactResult[1].checked && !frm.contactResult[2].checked) {alert('Please select if the dealer was satisifed, unsatisfied or requested a follow up.'); return false;}
		frm.btnSaveAttempt.value = 1;
	} else {
		if (!frm.leftMsgWith[0].checked && !frm.leftMsgWith[1].checked) {alert('Please indicate who/what you left the message with.'); return false;}
		frm.btnSaveAttempt.value = 1;
	}
	frm.submit();
}
function viewPrintWin(u) {
	window.open(u,"inqWin","width=600,height=500,scrollbars=1");
}
function populateText(sourceField,targetField) {
	var valsArray = new Array();
	var valToInsert = "";
	if (sourceField.length) {
		for (var i = 0; i < sourceField.length; i++) {
			if (sourceField[i].checked) {
				valsArray.push(sourceField[i].value);
			}
		}
		for (var x = 0; x < valsArray.length; x++) {
			valToInsert += valsArray[x];
			if (x < valsArray.length-1) {
				valToInsert += ", ";
			}
		}
	} else {
		valToInsert = sourceField.value;
	}
	targetField.value = valToInsert;
}
</script>

<script language="javascript" src="cal2.js">
/*
Xin's Popup calendar script-  Xin Yang (http://www.yxscripts.com/)
Script featured on/available at http://www.dynamicdrive.com/
This notice must stay intact for use
*/
</script>
<script language="javascript" src="cal_conf2.js"></script>

<form method="post" action="appendInquiry.cfm" name="mainform" enctype="multipart/form-data">
<cfoutput>
<input type="hidden" name="i" value="#i#" />
</cfoutput>
<table width="725" border="0" cellpadding="5" cellspacing="0">
	<cfoutput>
	<tr>
		<td class="highlightbar" style="padding:0px">
		<table border="0" cellspacing="0" cellpadding="3" width="100%">
		<tr class="highlightbar">
			<td>
			<b>Inquiry #getInq.inquiryNumber#</b>			
			</td>
			<td align="right" style="padding:0px">
			<table border="0" cellspacing="0" cellpadding="3">
				<tr class="highlightbar">
					<td><a href="index.cfm">Exit Inquiry</a></td>
					<td><a href="index.cfm"><img src="/images/closeWin.gif" align="absmiddle" border="0" /></a></td>
				</tr>
			</table>
			</td>
		</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td class="greyrow">
		<a href="javascript:viewPrintWin('viewInquiry.cfm?i=#i#');">
		<img src="/images/printerfriendlyicon.gif" alt="Print This Page" border="0" align="absmiddle" />&nbsp;<b>Print this Inquiry</b>
		</a>
		</td>
	</tr>
	</cfoutput>
	<tr>
		<td class="greyrow"><b>Status History:</b></td>
	</tr>
	<tr>
		<td class="greyrow" style="padding:0px">
		<table border="0" cellspacing="0" cellpadding="3" class="grey">
			<cfoutput query="getStatusHist">
			<tr>
				<td>
				#status#
				<cfswitch expression="#statusid#">
					<cfcase value="1"> by </cfcase>
					<cfcase value="3"> from #fromDeptName# to #toDeptName# by </cfcase>
					<cfcase value="4"> by </cfcase>
					<cfdefaultcase> by </cfdefaultcase>
				</cfswitch>
				<cfif adminuserid is 0>
					System 
				<cfelse>
					#firstname# #lastname# 
				</cfif>
				at #dateformat(dateTimeUpdated, 'mm/dd/yyyy')# #timeformat(dateTimeUpdated, 'hh:mm tt')#
				</td>
			</tr>
			<cfif statusid is 3 and forwardedNote is not "">
			<tr>
				<td style="font-style:italic">Forwarding comments: #replace(forwardedNote, "#chr(13)#", "<br />", "all")#</td>
			</tr>
			</cfif>
			<cfif statusid is 5>
			<tr>
				<td style="font-style:italic">Follow up scheduled for #dateformat(followUpDate, 'mm/dd/yyyy')#</td>
			</tr>
			<tr>
				<td style="font-style:italic">Follow up notes: #replace(followUpNote, "#chr(13)#", "<br />", "all")#</td>
			</tr>
			</cfif>
			</cfoutput>
		</table>
		</td>
	</tr>
	<cfoutput query="getInq" group="inquiryID">
	<tr>
		<td class="greyrow"><b>Details:</b></td>
	</tr>
	<tr>
		<td class="greyrow">
		<cfif lcase(category) is not "other">
			Category: #category#
		<cfelse>
			Category: #otherCategory# 
		</cfif>
		<cfif listfind(departmentlist, 1) gt 0>
			&nbsp;<a href="javascript:changeCat();"><i>[click here to change category]</i></a>
		</cfif>
		</td>
	</tr>
	<tr>
		<td class="greyrow" style="padding:0px">
			<table width="100%" border="0" cellspacing="0" cellpadding="5" class="grey">
				<tr>
					<td width="27%">Rec-Acct ##: #rec#-#acct#</td>
					<td width="73%">Subscriber: #subscriberName#</td>
				</tr>
				<tr>
					<td>Dealer ##: #dealerNumber# </td>
					<td>Dealer Name: #dealerName#</td>
				</tr>
				<cfif trim(inquiryIs) is not "">
				<tr>
					<td colspan="2">Status is #inquiryIs#</td>
				</tr>
				</cfif>
			</table>
		</td>
	</tr>
	<tr>
		<td class="greyrow">Person Inquiring is #personInquiring# (#personInquiringIs#). </td>
	</tr>
	<tr>
		<td class="greyrow">Phone ##: #personInquiringPhone# </td>
	</tr>
	<tr>
		<td class="greyrow">Occurred on: #dateformat(dateTimeOccurred, 'mm/dd/yyyy')# at #timeformat(dateTimeOccurred, 'hh:mm tt')# #timeOccurredTimeZone# Time Zone </td>
	</tr>
	<tr>
		<td class="greyrow">Applicable Code(s): <cfif trim(codes) is "">None<cfelse>#codes#</cfif></td>
	</tr>
	<tr>
		<td class="greyrow">Applicable Zone(s): <cfif trim(codes) is "">None<cfelse>#zones#</cfif></td>
	</tr>
	<tr>
		<td class="greyrow"><b>Inquiry:</b></td>
	</tr>
	<cfif recordingRequested is not 0>
		<tr>
			<td class="greyrow">#replace(inquiryText, "#chr(13)#", "<br />", "all")#</td>
		</tr>
		<tr>
			<td class="greyrow"><b>Recording requested for the following reason :</b> #recordingReason#</td>
		</tr>
	<cfelse>
		<tr>
			<td class="greyrow">#replace(inquiryText, "#chr(13)#", "<br />", "all")#</td>
		</tr>
	</cfif>
	<cfif clientFile is not "">
		<tr>
			<td class="greyrow">
			<b>Attachments:</b> <br />
			<table border="0" cellspacing="0" cellpadding="5" class="grey">
				<cfoutput>
				<tr>
					<td><a target="_blank" href="/inquiry/attachments/#savedFile#" style="text-decoration:underline;">#clientFile#</a></td>
					<td><a href="javascript: if (confirm('Are you sure you wish to delete this attachment?')) {document.location='openInquiry.cfm?i=#i#&removeAttachment=#attachmentid#';}" style="text-decoration:underline;">[remove]</a></td>
				</tr>
				</cfoutput>
			</table>
			</td>
		</tr>
	</cfif>
		<tr>
			<td class="greyrow" style="padding:0px">
				<table border="0" cellspacing="0" cellpadding="5" class="grey">
					<tr>
						<td>Attach File(s): </td>
						<td>
							<input type="file" name="fileAttachment">
						</td>
						<td>
							<input name="btnAttach" type="submit" class="sidebar" value="Attach File">
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</cfoutput>
	<cfif getFollowUPs.recordcount gt 0>
		<tr>
			<td class="greyrow" style="padding:0px">
			<table width="100%" border="0" cellspacing="0" cellpadding="5" class="grey">
				<tr>
					<td><b>Follow Ups Scheduled:</b></td>
				</tr>
				<cfoutput query="getFollowUPs">
				<tr>
					<td>#firstname# #lastname# for #dateformat(followUpDate, 'mm/dd/yyyy')#</td>
				</tr>
				</cfoutput>
			</table>
			</td>
		</tr>
	<cfelse>
		<tr><td class="greyrow">&nbsp;</td></tr>
	</cfif>
	<tr>
		<td class="greyrow">
		<b>Information to Append to Inquiry:</b>
		</td>
	</tr>
	<tr>
		<td class="greyrow">
		<textarea name="appendText" style="width:600px;" rows="7"></textarea>
		</td>
	</tr>
	<tr>
		<td class="greyrow">
		<input type="submit" name="btnSaveAppend" value="Save Additional Inquiry Information" class="sidebar">
		</td>
	</tr>
	<tr>
		<td class="greyrowbottom">&nbsp;</td>
	</tr>
</table>
</form>
<cfinclude template="footer.cfm">
