
<cfif isDefined("form.btnReassign")>

	<cfquery name="reassign" datasource="#ds#">
		<cfloop list="#rlist#" index="item">
			<cfif item is not 0>
				<cfset id = listgetat(item,1,":")>
				<cfset catid = listgetat(item,2,":")>
				begin
					update inquiries_main
					set categoryid = #catid#
					where inquiryid = #id#
				end
			</cfif>
		</cfloop>
	</cfquery>

	<cflocation url="reassign.cfm">

<cfelse>

	<link rel="stylesheet" type="text/css" href="../../styles.css">
	
	<script language="javascript" type="text/javascript">
	function viewPrintWin(u) {
		window.open(u,"inqWin","width=600,height=500,scrollbars=1");
	}
	</script>
	
	<cfquery name="getInqs" datasource="#ds#">
		select inquiries_main.* 
		from inquiries_main 
		inner join inquiries_status_history on inquiries_main.inquiryid = inquiries_status_history.inquiryid
		where 
		categoryid = 15 and 
		inquiries_status_history.isCurrentStatus = 1 and 
		inquiries_status_history.statusid <> 4 
		order by inquiryNumber asc
	</cfquery>
	
	<cfquery name="getcats" datasource="#ds#">
		select * from inquiries_categories
		where category <> 'other'
		order by category asc
	</cfquery>
	
	<div align="center">
	<table border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td class="highlightbar"><b>Reassign &quot;Other&quot; Inquiry Categories</b></td>
		</tr>
		<tr>
			<td class="greyrowbottom" style="padding:0px;">
				<table width="600" border="0" cellpadding="5" cellspacing="0" class="grey">
					<tr>
						<td align="center" nowrap><b>Inquiry # </b></td>
						<td><b>Other Category Input </b></td>
						<td><b>Assign to Category</b></td>
					</tr>
					<form method="post" action="reassign.cfm">
					<cfloop query="getInqs">
					<cfset inqid = inquiryid>
					<tr>
						<td align="center"><cfoutput><a href="javascript:viewPrintWin('../viewInquiry.cfm?i=#inquiryid#');" style="text-decoration:underline;">#inquiryNumber#</a></cfoutput></td>
						<td><cfoutput>#otherCategory#</cfoutput></td>
						<td>
							<select name="rlist">
								<option value="0"></option>
								<cfoutput query="getcats">
									<option value="#inqid#:#categoryid#">#category#</option>
								</cfoutput>
							</select>
						</td>
					</tr>
					</cfloop>
					<tr>
						<td colspan="2" align="center">&nbsp;</td>
						<td>
							<input name="btnReassign" type="submit" class="sidebar" value="Process Selections">
						</td>
					</tr>
					</form>
				</table>
			</td>
		</tr>
	</table>
	<br />
	<a href="/index.cfm" class="normal" style="text-decoration:underline;">Return to Intranet Menu</a>
	</div>

</cfif>
