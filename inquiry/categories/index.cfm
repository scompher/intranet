
<link rel="stylesheet" type="text/css" href="../../styles.css">

<cfquery name="getCats" datasource="#ds#">
	select * from inquiries_categories
	order by category asc
</cfquery>

<div align="center">
<table width="600" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td style="padding:0px">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td width="4%"><img src="../../images/edit.gif" alt="Edit" width="16" height="16"></td>
					<td width="96%">= Edit Category </td>
				</tr>
				<tr>
					<td><img src="../../images/delete.gif" alt="Delete" width="16" height="16"></td>
					<td>= Delete Category </td>
				</tr>
			</table>
		</td></tr>
	<tr>
		<td class="highlightbar"><b>Manage Inquiry Categories</b></td>
	</tr>
	<tr>
		<td class="greyrowbottom" style="padding:0px">
			<table width="100%" border="0" cellpadding="5" cellspacing="0" class="grey">
				<tr>
					<td width="9%" align="center" nowrap><b>Action</b></td>
					<td width="91%" nowrap><b>Category</b></td>
				</tr>
				<cfoutput query="getcats">
				<cfif lcase(category) is not "other">
				<tr>
					<td align="center">
					<a href="edit.cfm?cid=#categoryid#"><img src="../../images/edit.gif" alt="Edit" width="16" height="16" border="0"></a>&nbsp;
					<a href="javascript: if (confirm('Are you sure you wish to delete this category?')) {document.location='delete.cfm?cid=#categoryid#';}"><img src="../../images/delete.gif" alt="Delete" width="16" height="16" border="0"></a>
					</td>
					<td>#category#</td>
				</tr>
				</cfif>
				</cfoutput>
				<form method="post" action="add.cfm">
				<tr>
					<td colspan="2">
						<input name="Submit" type="submit" class="sidebar" value="Add New Category">
					</td>
				</tr>
				</form>
			</table>
		</td>
	</tr>
</table>
<br />
<a href="/index.cfm" class="normal" style="text-decoration:underline;">Return to Intranet Menu</a>
</div>
