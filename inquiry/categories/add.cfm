
<link rel="stylesheet" type="text/css" href="../../styles.css">

<cfif isDefined("form.btnAddCategory")>

	<cfquery name="addcat" datasource="#ds#">
		insert into inquiries_categories (category)
		values ('#form.category#')
	</cfquery>
	
	<cflocation url="index.cfm">

<cfelse>

	<div align="center">
	<table border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td class="highlightbar"><b>Add a Category</b></td>
		</tr>
		<tr>
			<td class="greyrowbottom" style="padding:0px;">
				<table border="0" cellpadding="5" cellspacing="0" class="grey">
					<form method="post" action="add.cfm">
					<tr>
						<td nowrap>Category Name:</td>
						<td>
							<input type="text" name="category" style="width:250px;" maxlength="100">
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<input name="btnAddCategory" type="submit" class="sidebar" value="Add Category">
						</td>
					</tr>
					</form>
				</table>
			</td>
		</tr>
	</table>
	</div>

</cfif>
