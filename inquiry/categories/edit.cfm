
<link rel="stylesheet" type="text/css" href="../../styles.css">

<cfif isDefined("form.btnEditCategory")>

	<cfquery name="Editcat" datasource="#ds#">
		update inquiries_categories 
		set category = '#form.category#'
		where categoryid = #cid#
	</cfquery>
	
	<cflocation url="index.cfm">

<cfelse>

	<cfquery name="getCat" datasource="#ds#">
		select * from inquiries_categories
		where categoryid = #cid#
	</cfquery>

	<div align="center">
	<table border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td class="highlightbar"><b>Edit a Category</b></td>
		</tr>
		<tr>
			<td class="greyrowbottom" style="padding:0px;">
				<table border="0" cellpadding="5" cellspacing="0" class="grey">
					<cfoutput query="getcat">
					<form method="post" action="edit.cfm">
					<input type="hidden" name="cid" value="#cid#">
					<tr>
						<td nowrap>Category Name:</td>
						<td>
							<input type="text" name="category" style="width:250px;" maxlength="100" value="#getcat.category#">
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<input name="btnEditCategory" type="submit" class="sidebar" value="Update Category">
						</td>
					</tr>
					</form>
					</cfoutput>
				</table>
			</td>
		</tr>
	</table>
	</div>

</cfif>
