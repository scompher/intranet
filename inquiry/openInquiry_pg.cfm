
<cfparam name="forwardInquiry" default="0">
<cfparam name="closeInquiry" default="0">
<cfparam name="form.inquiryIsID" default="0">
<cfparam name="form.conclusionOptionID" default="0">
<cfparam name="form.resolutionOptionID" default="0">
<cfparam name="form.contactAttemptID" default="0">
<cfparam name="form.btnSaveAttempt" default="0">

<cfif isdefined("form.btnAttach") and form.fileAttachment is not "">
	<cffile action="upload" filefield="fileAttachment" destination="#request.DirectPath#\inquiry\attachments\" nameconflict="makeunique">
	<cfset savedFile = file.ServerFile>
	<cfset uploadedFile = file.ClientFile>
	<cfquery name="saveAttachmentInfo" datasource="#ds#">
		insert into inquiries_attachments (savedFile, clientFile, inquiryid)
		values ('#savedFile#', '#uploadedFile#', #i#)
	</cfquery>
</cfif>

<cfif isDefined("url.removeAttachment")>
	<cfquery name="getAttachmentInfo" datasource="#ds#">
		select * 
		from inquiries_attachments
		where attachmentid = #removeAttachment# and inquiryid = #i#
	</cfquery>
	<cfif getAttachmentInfo.recordcount GT 0>
		<cfif fileExists("#request.DirectPath#\inquiry\attachments\#getAttachmentInfo.savedFile#")>
			<cffile action="delete" file="#request.DirectPath#\inquiry\attachments\#getAttachmentInfo.savedFile#">
		</cfif>
		<cfquery name="delDatabaseEntry" datasource="#ds#">
			delete from inquiries_attachments
			where inquiryid = #i# and attachmentid = #removeAttachment#
		</cfquery>
	</cfif>
</cfif>

<!--- <cfif isDefined("form.btnSaveAttempt")> --->
<cfif form.btnSaveAttempt is 1>
	<cfset attemptDateTime = createodbcdatetime("#form.attemptDate# #form.attemptHH#:#form.attemptMM# #form.attemptTT#")>
	<cfif form.contactAttemptID is not 3>
		<cfquery name="saveAttempt" datasource="#ds#">
			if not exists (select * from inquiries_contactAttempts where inquiryid = #i# and contactAttemptID = #form.contactAttemptID#)
			begin
				insert into inquiries_contactAttempts (inquiryID, contactAttemptID, leftMsgWith, leftMsgWithPerson, attemptDateTime, comments)
				values (#i#, #form.contactAttemptID#, '#form.leftMsgWith#', '#form.leftMsgWithPerson#', #attemptDateTime#, '#form.comments#')
			end
		</cfquery>
	<cfelse>
		<cfquery name="saveAttempt" datasource="#ds#">
			if not exists (select * from inquiries_contactAttempts where inquiryid = #i# and contactAttemptID = #form.contactAttemptID#)
			begin
				insert into inquiries_contactAttempts (inquiryID, contactAttemptID, spokeTo, attemptDateTime, contactResult, comments)
				values (#i#, #form.contactAttemptID#, '#form.spokeTo#', #attemptDateTime#, '#form.contactResult#', '#form.comments#')
			end
			<cfif listfindnocase(form.contactResult,"Sales follow up requested") is not 0>
			begin
				update inquiries_main 
				set salesFollowUp = 1
				where inquiryid = #i#
			end
			</cfif>
		</cfquery>
	</cfif>
	<cfset contactAttemptID = 0>
	<cfset addContactAttempt = 0>
</cfif>

<cfif isDefined("url.pickup")>
	<cfset today = createodbcdatetime(now())>
	<cftransaction>
		<cfquery name="updateStatus" datasource="#ds#">
			begin
				insert into inquiries_status_history (inquiryid, dateTimeUpdated, statusid, adminuserid, isCurrentStatus, fromDepartmentID, toDepartmentID)
				select inquiryid, dateTimeUpdated, statusid, #cookie.adminlogin#, isCurrentStatus, fromDepartmentID, toDepartmentID
				from inquiries_status_history
				where inquiries_status_history.inquiryid = #i# and isCurrentStatus = 1
			end
			begin
				update inquiries_status_history
				set	isCurrentStatus = 0 
				where inquiryid = #i#
			end
			begin
				update inquiries_status_history
				set dateTimeUpdated = #today#, statusid = 2, isCurrentStatus = 1, adminuserid = #cookie.adminlogin#, isRead = 1
				where inquiryid = #i# and statusHistoryID = (select max(statusHistoryID) from inquiries_status_history where inquiryid = #i#)
			end
		</cfquery>
	</cftransaction>
</cfif>

<cfif forwardInquiry IS 1>
	<cfset menuOptions = "">
	<cfinclude template="header.cfm">
	<cfinclude template="forwardInquiry.cfm">
	<cfinclude template="footer.cfm">
	<cfabort>
</cfif>

<cfif closeInquiry IS 1>
	<cfset menuOptions = "">
	<cfinclude template="header.cfm">
	<cfinclude template="closeInquiry.cfm">
	<cfinclude template="footer.cfm">
	<cfabort>
</cfif>

<cfquery name="getContactAttempts" datasource="#ds#">
	select * 
	from inquiries_contactAttempts
	where inquiryid = #i#
	order by inquiries_contactAttempts.contactAttemptID asc
</cfquery>
<cfset contactAttemptIDlist = valuelist(getcontactattempts.contactAttemptID)>

<cfquery name="checkStatus" datasource="#ds#">
	select * from inquiries_status_history
	where inquiryid = #i# and isCurrentStatus = 1
</cfquery>

<cfif checkStatus.statusid is 4 or checkStatus.statusid is 5>
	<cfif listfind(departmentlist, 3) is not 0>
		<cfset menuOptions = "forwardInquiry,addContactAttempt">
	<cfelse>
		<cfset menuOptions = "">
	</cfif>
<cfelse>
	<cfset menuOptions = "flagForSales,forwardInquiry,flagForSales,addContactAttempt,closeInquiry,scheduleFollowUp">
</cfif>

<cfinclude template="header.cfm">

<cfif isdefined("form.btnSaveFollowUp") and form.salesFollowUpComments is not "(enter follow-up comments, please include any dealer comments as well)">
	<cfquery name="saveFollowUp" datasource="#ds#">
		update inquiries_main 
		set salesFollowUpComments = '#form.salesFollowUpComments#'
		where inquiryid = #i#
	</cfquery>
</cfif>

<!--- save research if any --->
<cfif isDefined("form.btnAddResearch")>
	<cfset today = createodbcdatetime(now())>
	<cftransaction>
		<cfquery name="saveResearch" datasource="#ds#">
			insert into inquiries_research (inquiryid, researchText, creatorid, datetimecreated)
			values (#i#, '#form.researchText#', #cookie.adminlogin#, #today#)
		</cfquery>
	</cftransaction>
	<cfset addResearch = 0>
</cfif>

<!--- save conclusion if any --->
<cfif isDefined("form.btnAddConclusion")>
	<cfset today = createodbcdatetime(now())>
	<cftransaction>
		<cfquery name="saveconclusion" datasource="#ds#">
			insert into inquiries_conclusion (inquiryid, conclusionText, creatorid, datetimecreated)
			values (#i#, '#form.conclusionText#', #cookie.adminlogin#, #today#)
		</cfquery>
		<cfif form.conclusionOptionID is not 0>
			<!--- get new id --->
			<cfquery name="getconclusionid" datasource="#ds#">
				select max(conclusionID) as newid from inquiries_conclusion
				where inquiryid = #i# and creatorid = #cookie.adminlogin# and datetimecreated = #today#
			</cfquery>
			<cfset conclusionID = getconclusionid.newid>
			<cfquery name="saveconclusionOptions" datasource="#ds#">
				<cfloop list="#conclusionOptionID#" index="ID">
					begin
						if not exists (select * from inquiries_conclusion_options_lookup where conclusionOptionID = #id# and inquiryid = #i# and conclusionid = #conclusionid#)
						begin
							insert into inquiries_conclusion_options_lookup (conclusionOptionID, inquiryid, conclusionID)
							values (#id#, #i#, #conclusionID#)
						end
					end
				</cfloop>
			</cfquery>
		</cfif>
	</cftransaction>
	<cfset addconclusion = 0>
</cfif>

<!--- save resolution if any --->
<cfif isDefined("form.btnAddResolution")>
	<cfset today = createodbcdatetime(now())>
	<cftransaction>
		<cfquery name="saveresolution" datasource="#ds#">
			insert into inquiries_resolution (inquiryid, resolutionText, creatorid, datetimecreated)
			values (#i#, '#form.resolutionText#', #cookie.adminlogin#, #today#)
		</cfquery>
		<cfif form.resolutionOptionID is not 0>
			<!--- get new id --->
			<cfquery name="getresolutionid" datasource="#ds#">
				select max(resolutionID) as newid from inquiries_resolution
				where inquiryid = #i# and creatorid = #cookie.adminlogin# and datetimecreated = #today#
			</cfquery>
			<cfset resolutionID = getresolutionid.newid>
			<cfquery name="saveresolutionOptions" datasource="#ds#">
				<cfloop list="#resolutionOptionID#" index="ID">
					begin
						if not exists (select * from inquiries_resolution_options_lookup where resolutionOptionID = #id# and inquiryid = #i# and resolutionid = #resolutionid#)
						begin
							insert into inquiries_resolution_options_lookup (resolutionOptionID, inquiryid, resolutionID)
							values (#id#, #i#, #resolutionID#)
						end
				end
				</cfloop>
			</cfquery>
		</cfif>
	</cftransaction>
	<cfset addresolution = 0>
</cfif>

<!--- delete research Item --->
<cfif isDefined("form.btnDeleteResearch")>
	<cfquery name="deleteResearch" datasource="#ds#">
		delete from inquiries_research where researchID = #form.deleteResearchID#
	</cfquery>
	<cfset addResearch = 1>
</cfif>

<!--- delete conclusion Item --->
<cfif isDefined("form.btnDeleteconclusion")>
	<cfquery name="deleteconclusion" datasource="#ds#">
		begin
			delete from inquiries_conclusion_options_lookup where conclusionID = #form.deleteconclusionID#
		end
		begin
			delete from inquiries_conclusion where conclusionID = #form.deleteconclusionID#
		end
	</cfquery>
	<cfset addConclusion = 1>
</cfif>

<!--- delete resolution Item --->
<cfif isDefined("form.btnDeleteresolution")>
	<cfquery name="deleteresolution" datasource="#ds#">
		begin
			delete from inquiries_resolution_options_lookup where resolutionID = #form.deleteresolutionID#
		end
		begin
			delete from inquiries_resolution where resolutionID = #form.deleteresolutionID#
		end
	</cfquery>
	<cfset addResolution = 1>
</cfif>

<!--- set reviewed to 1 --->
<cfquery name="updateStatus" datasource="#ds#">
	update inquiries_main
	set reviewed = 1
	where inquiryid = #i#
</cfquery>

<!--- get status history --->
<cfquery name="getStatusHist" datasource="#ds#">
	select 
	inquiries_status_history.*, 
	inquiries_status.status,
	statusUsers.firstname, 
	statusUsers.lastname, 
	fromDepartment.department as fromDeptName, 
	toDepartment.department as toDeptName
	from inquiries_status_history
	left join inquiries_status on inquiries_status_history.statusid = inquiries_status.statusid
	left join admin_users as statusUsers on inquiries_status_history.adminuserid = statusUsers.adminuserid
	left join admin_users_departments as fromDepartment on inquiries_status_history.fromDepartmentid = fromDepartment.departmentid
	left join admin_users_departments as toDepartment on inquiries_status_history.toDepartmentid = toDepartment.departmentid
	where inquiries_status_history.inquiryid = #i#
	order by dateTimeUpdated ASC
</cfquery>

<cfquery name="getInq" datasource="#ds#">
	select 
	inquiries_main.*, 
	inquiries_categories.category, 
	inquiries_inquiryIsOptions.*, 
	inquiries_attachments.attachmentid, 
	inquiries_attachments.savedFile, 
	inquiries_attachments.clientFile 
	from inquiries_main
	left join inquiries_inquiryIsOptions on inquiries_main.inquiryIsID = inquiries_inquiryIsOptions.inquiryIsID
	left join inquiries_attachments on inquiries_main.inquiryID = inquiries_attachments.inquiryID
	left join inquiries_categories on inquiries_main.categoryid = inquiries_categories.categoryid
	where inquiries_main.inquiryid = #i#
</cfquery>

<cfquery name="getResearch" datasource="#ds#">
	select 
	inquiries_research.*, 
	admin_users.firstname, 
	admin_users.lastname 
	from inquiries_research 
	left join admin_users on inquiries_research.creatorid = admin_users.adminuserid
	where inquiries_research.inquiryid = #i#
</cfquery>

<cfquery name="getconclusion" datasource="#ds#">
	select 
	inquiries_conclusion.*, 
	inquiries_conclusion_options_lookup.conclusionOptionID, 
	inquiries_conclusion_options.conclusionOption, 
	admin_users.firstname, 
	admin_users.lastname 
	from inquiries_conclusion 
	left join inquiries_conclusion_options_lookup on inquiries_conclusion.conclusionID = inquiries_conclusion_options_lookup.conclusionID 
	left join inquiries_conclusion_options on inquiries_conclusion_options_lookup.conclusionOptionID = inquiries_conclusion_options.conclusionOptionID
	left join admin_users on inquiries_conclusion.creatorid = admin_users.adminuserid
	where inquiries_conclusion.inquiryid = #i#
	order by inquiries_conclusion.conclusionID, conclusionOption
</cfquery>

<cfquery name="getresolution" datasource="#ds#">
	select 
	inquiries_resolution.*, 
	inquiries_resolution_options_lookup.resolutionOptionID, 
	inquiries_resolution_options.resolutionOption, 
	admin_users.firstname, 
	admin_users.lastname 
	from inquiries_resolution 
	left join inquiries_resolution_options_lookup on inquiries_resolution.resolutionID = inquiries_resolution_options_lookup.resolutionID 
	left join inquiries_resolution_options on inquiries_resolution_options_lookup.resolutionOptionID = inquiries_resolution_options.resolutionOptionID
	left join admin_users on inquiries_resolution.creatorid = admin_users.adminuserid
	where inquiries_resolution.inquiryid = #i#
	order by inquiries_resolution.resolutionID, resolutionOption
</cfquery>

<cfquery name="getFollowUps" datasource="#ds#">
	select 
	inquiries_followUp.*,
	admin_users.firstname, admin_users.lastname
	from inquiries_followUp
	left join admin_users on inquiries_followUp.adminuserid = admin_users.adminuserid
	where inquiries_followUp.inquiryid = #i#
</cfquery>

<cfquery name="getConclusionOptions" datasource="#ds#">
	select * 
	from inquiries_conclusion_options
	order by conclusionOption asc
</cfquery>

<cfquery name="getresolutionOptions" datasource="#ds#">
	select * 
	from inquiries_resolution_options
	order by resolutionOption asc
</cfquery>

<cfquery name="getInquiryIsOptions" datasource="#ds#">
	select * 
	from inquiries_inquiryIsOptions
	order by inquiryIsOrder
</cfquery>

<cfquery name="getCurrentStatus" dbtype="query">
	select * from getStatusHist
	where isCurrentStatus = 1 
</cfquery>

<cfif getCurrentStatus.statusid is 4 or getCurrentStatus.statusid is 5>
	<cfparam name="addResearch" default="0">
	<cfparam name="addResolution" default="0">
	<cfparam name="addConclusion" default="0">
<cfelse>
	<cfparam name="addResearch" default="1">
	<cfparam name="addResolution" default="1">
	<cfparam name="addConclusion" default="1">
</cfif>

<cfparam name="forwardInquiry" default="0">
<cfparam name="salesFollowUp" default="0">
<cfparam name="addContactAttempt" default="0">
<cfparam name="personalFollowUp" default="0">

<!--- if research resolution and conclusion are already there by this user, dont show input boxes --->
<cfquery name="checkResearch" dbtype="query">
	select * from getResearch
	where creatorid = #cookie.adminlogin#
</cfquery>
<cfif checkResearch.recordcount gt 0><cfset addResearch = 0></cfif>

<cfquery name="checkConclusion" dbtype="query">
	select * from getConclusion
	where creatorid = #cookie.adminlogin#
</cfquery>
<cfif checkConclusion.recordcount gt 0><cfset addConclusion = 0></cfif>

<cfquery name="checkResolution" dbtype="query">
	select * from getResolution
	where creatorid = #cookie.adminlogin#
</cfquery>
<cfif checkResolution.recordcount gt 0><cfset addResolution = 0></cfif>

<script language="JavaScript" type="text/javascript">
function showEditWin(url) {
	window.open(url,"editWin","width=515,height=200,scrollbars=0");
}
function checkContactAttempt(frm, attempt) {
	if (attempt == 3) {
		if (!frm.contactResult[0].checked && !frm.contactResult[1].checked && !frm.contactResult[2].checked) {alert('Please select if the dealer was satisifed, unsatisfied or requested a follow up.'); return false;}
		frm.btnSaveAttempt.value = 1;
	} else {
		if (!frm.leftMsgWith[0].checked && !frm.leftMsgWith[1].checked) {alert('Please indicate who/what you left the message with.'); return false;}
		frm.btnSaveAttempt.value = 1;
	}
	frm.submit();
}
function viewPrintWin(u) {
	window.open(u,"inqWin","width=600,height=500,scrollbars=1");
}
</script>

<script language="javascript" src="cal2.js">
/*
Xin's Popup calendar script-  Xin Yang (http://www.yxscripts.com/)
Script featured on/available at http://www.dynamicdrive.com/
This notice must stay intact for use
*/
</script>
<script language="javascript" src="cal_conf2.js"></script>

<form method="post" action="openInquiry.cfm" name="mainform" enctype="multipart/form-data">
<cfoutput>
<input type="hidden" name="i" value="#i#" />
<input type="hidden" name="addResearch" value="#addResearch#" />
<input type="hidden" name="addConclusion" value="#addConclusion#" />
<input type="hidden" name="addResolution" value="#addResolution#" />
<input type="hidden" name="forwardInquiry" value="#forwardInquiry#" />
<input type="hidden" name="closeInquiry" value="#closeInquiry#" />
<input type="hidden" name="salesFollowUp" value="#salesFollowUp#" />
<input type="hidden" name="deleteResearchID" value="0" />
<input type="hidden" name="deleteConclusionID" value="0" />
<input type="hidden" name="deleteResolutionID" value="0" />
<input type="hidden" name="addContactAttempt" value="#addContactAttempt#" />
</cfoutput>
<table width="725" border="0" cellpadding="5" cellspacing="0">
	<cfoutput>
	<tr>
		<td class="highlightbar" style="padding:0px">
		<table border="0" cellspacing="0" cellpadding="3" width="100%">
		<tr class="highlightbar">
			<td>
			<b>Inquiry #getInq.inquiryNumber#</b>			
			</td>
			<td align="right" style="padding:0px">
			<table border="0" cellspacing="0" cellpadding="3">
				<tr class="highlightbar">
					<td><a href="index.cfm">Exit Inquiry</a></td>
					<td><a href="index.cfm"><img src="/images/closeWin.gif" align="absmiddle" border="0" /></a></td>
				</tr>
			</table>
			</td>
		</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td class="greyrow">
		<a href="javascript:viewPrintWin('viewInquiry.cfm?i=#i#');">
		<img src="/images/printerfriendlyicon.gif" alt="Print This Page" border="0" align="absmiddle" />&nbsp;<b>Print this Inquiry</b>
		</a>
		</td>
	</tr>
	</cfoutput>
	<tr>
		<td class="greyrow"><b>Status History:</b></td>
	</tr>
	<tr>
		<td class="greyrow" style="padding:0px">
		<table border="0" cellspacing="0" cellpadding="3" class="grey">
			<cfoutput query="getStatusHist">
			<tr>
				<td>
				#status#
				<cfswitch expression="#statusid#">
					<cfcase value="1"> by </cfcase>
					<cfcase value="3"> from #fromDeptName# to #toDeptName# by </cfcase>
					<cfcase value="4"> by </cfcase>
					<cfdefaultcase> by </cfdefaultcase>
				</cfswitch>
				<cfif adminuserid is 0>
					System 
				<cfelse>
					#firstname# #lastname# 
				</cfif>
				at #dateformat(dateTimeUpdated, 'mm/dd/yyyy')# #timeformat(dateTimeUpdated, 'hh:mm tt')#
				</td>
			</tr>
			<cfif statusid is 3 and forwardedNote is not "">
			<tr>
				<td style="font-style:italic">Forwarding comments: #replace(forwardedNote, "#chr(13)#", "<br />", "all")#</td>
			</tr>
			</cfif>
			<cfif statusid is 5>
			<tr>
				<td style="font-style:italic">Follow up scheduled for #dateformat(followUpDate, 'mm/dd/yyyy')#</td>
			</tr>
			<tr>
				<td style="font-style:italic">Follow up notes: #replace(followUpNote, "#chr(13)#", "<br />", "all")#</td>
			</tr>
			</cfif>
			</cfoutput>
		</table>
		</td>
	</tr>
	<cfoutput query="getInq" group="inquiryID">
	<tr>
		<td class="greyrow"><b>Details:</b></td>
	</tr>
	<tr>
		<td class="greyrow">
		<cfif lcase(category) is not "other">
			Category: #category#
		<cfelse>
			Category: #otherCategory#
		</cfif>
		</td>
	</tr>
	<tr>
		<td class="greyrow" style="padding:0px">
			<table width="100%" border="0" cellspacing="0" cellpadding="5" class="grey">
				<tr>
					<td width="27%">Rec-Acct ##: #rec#-#acct#</td>
					<td width="73%">Subscriber: #subscriberName#</td>
				</tr>
				<tr>
					<td>Dealer ##: #dealerNumber# </td>
					<td>Dealer Name: #dealerName#</td>
				</tr>
				<cfif trim(inquiryIs) is not "">
				<tr>
					<td colspan="2">Status is #inquiryIs#</td>
				</tr>
				</cfif>
			</table>
		</td>
	</tr>
	<tr>
		<td class="greyrow">Person Inquiring is #personInquiring# who is a #personInquiringIs#. </td>
	</tr>
	<tr>
		<td class="greyrow">Phone ##: #personInquiringPhone# </td>
	</tr>
	<tr>
		<td class="greyrow">Occurred on: #dateformat(dateTimeOccurred, 'mm/dd/yyyy')# at #timeformat(dateTimeOccurred, 'hh:mm tt')# #timeOccurredTimeZone# Time Zone </td>
	</tr>
	<tr>
		<td class="greyrow">Applicable Code(s): <cfif trim(codes) is "">None<cfelse>#codes#</cfif></td>
	</tr>
	<tr>
		<td class="greyrow">Applicable Zone(s): <cfif trim(codes) is "">None<cfelse>#zones#</cfif></td>
	</tr>
	<tr>
		<td class="greyrow"><b>Inquiry:</b></td>
	</tr>
	<cfif recordingRequested is not 0>
		<tr>
			<td class="greyrow">#replace(inquiryText, "#chr(13)#", "<br />", "all")#</td>
		</tr>
		<tr>
			<td class="greyrow"><b>Recording requested for the following reason :</b> #recordingReason#</td>
		</tr>
	<cfelse>
		<tr>
			<td class="greyrow">#replace(inquiryText, "#chr(13)#", "<br />", "all")#</td>
		</tr>
	</cfif>
	<cfif clientFile is not "">
		<tr>
			<td class="greyrow">
			<b>Attachments:</b> <br />
			<table border="0" cellspacing="0" cellpadding="5" class="grey">
				<cfoutput>
				<tr>
					<td><a target="_blank" href="/inquiry/attachments/#savedFile#" style="text-decoration:underline;">#clientFile#</a></td>
					<td><a href="javascript: if (confirm('Are you sure you wish to delete this attachment?')) {document.location='openInquiry.cfm?i=#i#&removeAttachment=#attachmentid#';}" style="text-decoration:underline;">[remove]</a></td>
				</tr>
				</cfoutput>
			</table>
			</td>
		</tr>
	</cfif>
		<tr>
			<td class="greyrow" style="padding:0px">
				<table border="0" cellspacing="0" cellpadding="5" class="grey">
					<tr>
						<td>Attach File(s): </td>
						<td>
							<input type="file" name="fileAttachment">
						</td>
						<td>
							<input name="btnAttach" type="submit" class="sidebar" value="Attach File">
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</cfoutput>
	<cfif getFollowUPs.recordcount gt 0>
		<tr>
			<td class="greyrow" style="padding:0px">
			<table width="100%" border="0" cellspacing="0" cellpadding="5" class="grey">
				<tr>
					<td><b>Follow Ups Scheduled:</b></td>
				</tr>
				<cfoutput query="getFollowUPs">
				<tr>
					<td>#firstname# #lastname# for #dateformat(followUpDate, 'mm/dd/yyyy')#</td>
				</tr>
				</cfoutput>
			</table>
			</td>
		</tr>
	<cfelse>
		<tr><td class="greyrow">&nbsp;</td></tr>
	</cfif>
	<tr>
		<td class="greyrowbottom">&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<cfif getResearch.recordcount gt 0>
		<cfoutput query="getResearch" group="researchID">
		<tr>
			<td class="nopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="3" class="highlightbar">
				<tr>
					<td width="70%"><b>Research added by #firstname# #lastname# on #dateformat(dateTimeCreated, 'mm/dd/yyyy')# at #timeformat(dateTimeCreated, 'hh:mm tt')#</b></td>
					<td align="right" width="30%">
					<cfif (creatorID is cookie.adminlogin and checkStatus.statusid is not 4) or (getsec.seclevelid LTE 2)>
					<input type="button" onclick="showEditWin('editResearch.cfm?i=#getResearch.researchID#')" value="Edit" class="lightbutton"/>
					<input type="submit" onclick="if (confirm('Are you sure you wish to delete this research item?')) {document.mainform.deleteResearchID.value = #getResearch.researchID#; return true;} else {return false;}" name="btnDeleteResearch" value="Delete" class="lightbutton"/>
					<cfelse>
					&nbsp;
					</cfif>
					</td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td class="greyrowbottom">#replace(researchText, "#chr(13)#", "<br />", "all")#</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		</cfoutput>
	</cfif>
	<cfif addResearch is 1>
		<tr>
			<td class="highlightbar"><b>Add Research</b> (Clearly Explain)</td>
		</tr>
		<tr>
			<td class="greyrow">
				<textarea name="researchText" cols="" rows="5" style="width:715px" onfocus="this.select();">(explain research here)</textarea>
			</td>
		</tr>
		<tr>
			<td class="greyrowbottom">
				<input name="btnAddResearch" type="submit" class="sidebar" value="Save Research" />
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
	</cfif>
	<cfif getconclusion.recordcount gt 0>
		<cfoutput query="getconclusion" group="conclusionID">
		<tr>
			<td class="nopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="3" class="highlightbar">
				<tr>
					<td width="70%"><b>Conclusion added by: #firstname# #lastname# on #dateformat(dateTimeCreated, 'mm/dd/yyyy')# at #timeformat(dateTimeCreated, 'hh:mm tt')#</b></td>
					<td align="right" width="30%">
					<cfif (creatorID is cookie.adminlogin and checkStatus.statusid is not 4) or (getsec.seclevelid LTE 2)>
					<input type="button" onclick="showEditWin('editConclusion.cfm?i=#getConclusion.conclusionID#')" value="Edit" class="lightbutton"/>
					<input type="submit" onclick="if (confirm('Are you sure you wish to delete this conclusion item?')) {document.mainform.deleteConclusionID.value = #getconclusion.conclusionID#; return true;} else {return false;}" name="btnDeleteconclusion" value="Delete" class="lightbutton"/>
					<cfelse>
					&nbsp;
					</cfif>
					</td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td class="greyrowbottom">#replace(conclusionText, "#chr(13)#", "<br />", "all")#</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		</cfoutput>
	</cfif>
	<cfif addConclusion is 1>
		<tr>
			<td class="highlightbar"><b>Add Conclusion</b> (Clearly explain what happened)</td>
		</tr>
		<tr class="greyrow">
			<td class="greyrow">
				<textarea name="conclusionText" cols="" rows="5" style="width:715px" onfocus="this.select();">(explain conclusion here)</textarea>
			</td>
		</tr>
		<tr class="greyrow">
			<td class="greyrowbottom">
				<input name="btnAddConclusion" type="submit" class="sidebar" value="Save Conclusion" />
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
	</cfif>
	<cfif getresolution.recordcount gt 0>
		<cfoutput query="getresolution" group="resolutionID">
		<tr>
			<td class="nopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="3" class="highlightbar">
				<tr>
					<td width="70%"><b>Resolution added by: #firstname# #lastname# on #dateformat(dateTimeCreated, 'mm/dd/yyyy')# at #timeformat(dateTimeCreated, 'hh:mm tt')#</b></td>
					<td align="right" width="30%">
					<cfif (creatorID is cookie.adminlogin and checkStatus.statusid is not 4) or (getsec.seclevelid LTE 2)>
					<input type="button" onclick="showEditWin('editResolution.cfm?i=#getResolution.resolutionID#')" value="Edit" class="lightbutton"/>
					<input type="submit" onclick="if (confirm('Are you sure you wish to delete this resolution item?')) {document.mainform.deleteResolutionID.value = #getresolution.resolutionID#; return true;} else {return false;}" name="btnDeleteresolution" value="Delete" class="lightbutton"/>
					<cfelse>
					&nbsp;
					</cfif>
					</td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td class="greyrowbottom">#replace(resolutionText, "#chr(13)#", "<br />", "all")#</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		</cfoutput>
	</cfif>
	<cfif addResolution is 1>
		<tr>
			<td align="left" class="highlightbar"><b>Add Resolution</b> (Clearly explain what is being done to correct the problem) </td>
		</tr>
		<tr>
			<td class="greyrow">
				<textarea name="resolutionText" cols="" rows="5" style="width:715px" onfocus="this.select();">(explain resolution here)</textarea>
			</td>
		</tr>
		<tr>
			<td class="greyrowbottom">
				<input name="btnAddResolution" type="submit" class="sidebar" value="Save Resolution" />
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
	</cfif>
	<cfif getContactAttempts.recordcount gt 0 or addContactAttempt is not 0>
		<tr>
			<td class="highlightbar"><b>Contact Attempts</b></td>
		</tr>	
		<cfif getContactAttempts.recordcount gt 0>
			<tr>
				<td class="greyrow"><b>Previous Attempts: </b></td>
			</tr>
			<tr>
				<td class="greyrow" style="padding:0px">
				<table border="0" cellspacing="0" cellpadding="3" class="grey">
					<cfoutput query="getcontactattempts">
					<tr>
						<td valign="top">#getcontactattempts.currentrow#.</td>
						<td valign="top">
						<cfswitch expression="#contactAttemptID#">
							<cfcase value="1">First attempt left message for call back <cfif leftMsgWith is "answering machine">on #leftMsgWith#<cfelse>with #leftMsgWithPerson#</cfif> on #dateformat(attemptDateTime, 'mm/dd/yyyy')# at #timeformat(attemptDateTime, 'hh:mm tt')#<br />Comments: #comments#</cfcase>
							<cfcase value="2">Second attempt left message for call back on #leftMsgWith# on #dateformat(attemptDateTime, 'mm/dd/yyyy')# at #timeformat(attemptDateTime, 'hh:mm tt')#<br />Comments: #comments#</cfcase>
							<cfcase value="3">Spoke to #spokeTo# on #dateformat(attemptDateTime, 'mm/dd/yyyy')# at #timeformat(attemptDateTime, 'hh:mm tt')#<br />Result: #contactResult#<br />Comments: #comments#</cfcase>
						</cfswitch>
						</td>
					</tr>
					</cfoutput>
				</table>
				</td>
			</tr>
		</cfif>
		<cfif (contactAttemptIDList is not "1,2,3" or contactAttemptIDList is not "4") and addContactAttempt is not 0>
		<tr>
			<td class="greyrow">
				<select name="contactAttemptID" onchange="this.form.submit();">
					<option value="0" <cfif contactAttemptID is 0>selected</cfif> >Please select</option>
					<cfif listfind(contactAttemptIDList,1) IS 0><option value="1" <cfif contactAttemptID is 1>selected</cfif> >First Attempt left message for call back</option></cfif>
					<cfif listfind(contactAttemptIDList,2) IS 0><option value="2" <cfif contactAttemptID is 2>selected</cfif> >Second Attempt left message for call back</option></cfif>
					<cfif listfind(contactAttemptIDList,3) IS 0><option value="3" <cfif contactAttemptID is 3>selected</cfif> >Dealer Notified</option></cfif>
					<!--- <cfif listfind(contactAttemptIDList,4) IS 0><option value="4" <cfif contactAttemptID is 4>selected</cfif> >In House Inquiry - Employee Notified</option></cfif> --->
				</select>
			</td>
		</tr>
		</cfif>
		<cfif (contactAttemptID is not 0 and contactAttemptID is not 3 and contactAttemptID is not 4)>
		<tr>
			<td class="greyrow" style="padding:0px">
				<table border="0" cellspacing="0" cellpadding="5">
					<tr class="greyrow">
						<td>
							<input type="checkbox" name="leftMsgWith" value="answering machine" onclick="if (this.form.leftMsgWith[0].checked) this.form.leftMsgWith[1].checked = false;" />
						</td>
						<td colspan="2">Answering Machine</td>
						</tr>
					<tr class="greyrow">
						<td>
							<input type="checkbox" name="leftMsgWith" value="with" onclick="if (this.form.leftMsgWith[1].checked) {this.form.leftMsgWith[0].checked = false; this.form.leftMsgWithPerson.focus();}" />
						</td>
						<td>With</td>
						<td>
							<input name="leftMsgWithPerson" type="text" style="width:500px" maxlength="255" onfocus="this.form.leftMsgWith[0].checked = false; this.form.leftMsgWith[1].checked = true; " />
						</td>
					</tr>
					<tr class="greyrow" style="padding:0px">
						<td>&nbsp;</td>
						<td colspan="2" class="nopadding">
						<cfoutput>
							<table border="0" cellpadding="5" cellspacing="0" class="grey">
								<tr>
									<td>Date:</td>
									<td>
										<input name="attemptDate" type="text" style="width:75px" value="#dateformat(now(),'mm/dd/yyyy')#" maxlength="10" />
									</td>
									<td> <a style="text-decoration:none;" href="javascript:showCal('AttemptDate');"> <img src="../images/calicon.gif" alt="Select a Date" align="absmiddle" width="20" height="20" border="0" /> </a> </td>
									<td>Time:</td>
									<td class="nopadding">
										<table border="0" cellpadding="5" cellspacing="0" class="grey">
											<tr>
												<td class="nopadding">
													<input name="attemptHH" type="text" style="width:25px" value="#timeformat(now(),'hh')#" maxlength="2" />
												</td>
												<td valign="middle">:</td>
												<td class="nopadding">
													<input name="attemptMM" type="text" style="width:25px" value="#timeformat(now(),'mm')#" maxlength="2" />
												</td>
												<td>
													<select name="attemptTT">
														<option <cfif timeformat(now(),'tt') is "AM">selected</cfif> value="AM">AM</option>
														<option <cfif timeformat(now(),'tt') is "PM">selected</cfif> value="PM">PM</option>
													</select>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</cfoutput>
						</td>
					</tr>
					<tr class="greyrow" style="padding:0px">
						<td>&nbsp;</td>
						<td colspan="2" class="nopadding">
							<table width="100%" border="0" cellspacing="0" cellpadding="5">
								<tr class="greyrow">
									<td width="11%">Comments:</td>
									<td width="89%">
										<input name="comments" type="text" style="width:500px" maxlength="255" />
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td class="greyrowbottom">
				<cfoutput>
				<input type="hidden" name="btnSaveAttempt" value="0" />
				<input onclick="checkContactAttempt(this.form, #contactAttemptID#);" type="button" class="sidebar" value="Save Attempt Information" style="width:150px" />
				</cfoutput>
			</td>
		</tr>
		<cfelseif contactAttemptID is 3>
		<tr>
			<td class="greyrow" style="padding:0px">
				<table border="0" cellspacing="0" cellpadding="5" class="grey">
					<tr>
						<td>Spoke To: </td>
						<td>
							<input name="spokeTo" type="text" style="width:400px" />
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td class="greyrow" style="padding:0px"><cfoutput>
				<table border="0" cellpadding="5" cellspacing="0" class="grey">
					<tr>
						<td>Date:</td>
						<td>
							<input name="attemptDate" type="text" style="width:75px" value="#dateformat(now(),'mm/dd/yyyy')#" maxlength="10" />
						</td>
						<td> <a style="text-decoration:none;" href="javascript:showCal('AttemptDate');"> <img src="../images/calicon.gif" alt="Select a Date" align="absmiddle" width="20" height="20" border="0" /> </a> </td>
						<td>Time:</td>
						<td class="nopadding">
							<table border="0" cellpadding="5" cellspacing="0" class="grey">
								<tr>
									<td class="nopadding">
										<input name="attemptHH" type="text" style="width:25px" value="#timeformat(now(),'hh')#" maxlength="2" />
									</td>
									<td valign="middle">:</td>
									<td class="nopadding">
										<input name="attemptMM" type="text" style="width:25px" value="#timeformat(now(),'mm')#" maxlength="2" />
									</td>
									<td>
										<select name="attemptTT">
											<option <cfif timeformat(now(),'tt') is "AM">selected</cfif> value="AM">AM</option>
											<option <cfif timeformat(now(),'tt') is "PM">selected</cfif> value="PM">PM</option>
										</select>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</cfoutput>
			</td>
		</tr>
		<tr>
			<td class="greyrow" style="padding:0px">
				<table width="100%" border="0" cellspacing="0" cellpadding="5" class="grey">
					<tr>
						<td width="4%">
							<input type="checkbox" name="contactResult" value="Dealer Satisfied" />
						</td>
						<td width="23%">Dealer Satisfied </td>
						<td width="4%">
							<input type="checkbox" name="contactResult" value="Dealer Unsatisfied" />
						</td>
						<td width="24%">Dealer Unsatisified </td>
						<td width="4%">
							<input type="checkbox" name="contactResult" value="Sales follow up requested" />
						</td>
						<td width="41%">Sales Follow Up Requested </td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td class="greyrow">
			<table width="100%" border="0" cellspacing="0" cellpadding="5" class="grey">
				<tr>
					<td width="11%">Comments:</td>
					<td width="89%">
						<input name="comments" type="text" style="width:500px" maxlength="255" />
					</td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td class="greyrowbottom">
				<cfoutput>
				<input type="hidden" name="btnSaveAttempt" value="0" />
				<input onclick="checkContactAttempt(this.form, #contactAttemptID#);" type="button" class="sidebar" value="Save Attempt Information" style="width:150px" />
				</cfoutput>
			</td>
		</tr>
		<cfelse>
		<tr>
			<td class="greyrowbottom">&nbsp;</td>
		</tr>
		</cfif>
	</cfif>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<cfif getInq.salesFollowUp is 1 and getInq.salesFollowUpComments IS "">
	<tr>
		<td class="highlightbar"><b>Sales Follow-Up</b></td>
	</tr>
	<tr>
		<td class="greyrow">
			<textarea name="salesFollowUpComments" cols="" rows="5" style="width:715px" onfocus="this.select();">(enter follow-up comments, please include any dealer comments as well)</textarea>
		</td>
	</tr>
	<tr>
		<td class="greyrowbottom">
			<input name="btnSaveFollowUp" type="submit" class="sidebar" value="Save Follow Up" />
		</td>
	</tr>
	</cfif>
	<cfif getInq.salesFollowUpComments is not "">
	<tr>
		<td class="highlightbar"><b>Sales Follow-Up</b></td>
	</tr>
	<tr>
		<td class="greyrowbottom">
		<cfoutput>
		#replace(getInq.salesFollowUpComments, "#chr(13)#", "<br />", "all")#
		</cfoutput>
		</td>
	</tr>
	</cfif>
</table>
</form>
<cfinclude template="footer.cfm">
