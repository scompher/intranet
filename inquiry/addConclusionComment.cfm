
<cfsetting showdebugoutput="no">

<cfif isdefined("form.btnEditconclusion")>

	<cfif trim(form.conclusionText) is not "">
		<cfset creator = getsec.firstname & " " & getsec.lastname>
		<cfset form.conclusionText = "#urldecode(form.oldConclusionText)# #chr(13)# #chr(13)# Added by #creator# on #dateformat(now(),'mm/dd/yyyy')# #timeformat(now(),'hh:mm tt')#:#chr(13)#" & form.conclusionText>
		<cfquery name="Updateconclusion" datasource="#ds#">
			begin
				update inquiries_conclusion
				set conclusionText = '#form.conclusionText#' 
				where conclusionid = #conclusionid#
			end
			begin
				select inquiryid from inquiries_conclusion where conclusionid = #conclusionid#
			end
		</cfquery>
	</cfif>

	<cfoutput>
	<script type="text/javascript">
		opener.location='openInquiry.cfm?i=#updateconclusion.inquiryid#';
		self.close();
	</script>
	</cfoutput>

<cfelse>

	<cfquery name="getconclusion" datasource="#ds#">
		select 
		inquiries_conclusion.* 
		from inquiries_conclusion 
		where conclusionid = #i#
	</cfquery>
	
	<link rel="stylesheet" type="text/css" href="../styles.css">
	
	<div align="center">
	<title>Edit Conclusion</title>
	<body onLoad="this.focus();">
	<table width="475" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td class="highlightbar"><b>Add Additional Conclusion Text</b></td>
		</tr>
		<form method="post" action="addConclusionComment.cfm">
		<cfoutput>
		<input type="hidden" name="conclusionid" value="#i#">
		<input type="hidden" name="oldConclusionText" value="#urlencodedformat(getconclusion.conclusionText)#">		
		</cfoutput>
		<tr>
			<td class="greyrowbottom" align="center">
				<table border="0" cellpadding="5" cellspacing="0" class="grey">
					<tr>
						<td>
							<textarea name="conclusionText" rows="5" style="width:450px"></textarea>
						</td>
					</tr>
					<tr>
						<td>
							<input name="btnEditconclusion" type="submit" class="sidebar" value="Add Comment">
						</td>
					</tr>
				</table>
			</td>
		</tr>
		</form>
	</table>
	</body>
	</div>

</cfif>
