
<cfsetting showdebugoutput="no">

<cfif isdefined("form.btnEditconclusion")>

	<cfquery name="Updateconclusion" datasource="#ds#">
		begin
			update inquiries_conclusion
			set conclusionText = '#form.conclusionText#' 
			where conclusionid = #conclusionid#
		end
		begin
			select inquiryid from inquiries_conclusion where conclusionid = #conclusionid#
		end
	</cfquery>

	<cfoutput>
	<script type="text/javascript">
		opener.location='openInquiry.cfm?i=#updateconclusion.inquiryid#';
		self.close();
	</script>
	</cfoutput>

<cfelse>

	<cfquery name="getconclusion" datasource="#ds#">
		select 
		inquiries_conclusion.* 
		from inquiries_conclusion 
		where conclusionid = #i#
	</cfquery>
	
	<link rel="stylesheet" type="text/css" href="../styles.css">
	
	<div align="center">
	<title>Edit Conclusion</title>
	<body onLoad="this.focus();">
	<table width="475" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td class="highlightbar"><b>Edit Conclusion Item</b></td>
		</tr>
		<form method="post" action="editconclusion.cfm">
		<cfoutput>
		<input type="hidden" name="conclusionid" value="#i#">
		</cfoutput>
		<tr>
			<td class="greyrowbottom" align="center">
				<table border="0" cellpadding="5" cellspacing="0" class="grey">
					<tr>
						<td>
							<textarea name="conclusionText" rows="5" style="width:450px"><cfoutput>#getconclusion.conclusionText#</cfoutput></textarea>
						</td>
					</tr>
					<tr>
						<td>
							<input name="btnEditconclusion" type="submit" class="sidebar" value="Update Conclusion">
						</td>
					</tr>
				</table>
			</td>
		</tr>
		</form>
	</table>
	</body>
	</div>

</cfif>
