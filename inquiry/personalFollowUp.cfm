
<cfsetting showdebugoutput="no">

<link rel="stylesheet" type="text/css" href="../styles.css">

<script language="javascript" src="cal2.js">
/*
Xin's Popup calendar script-  Xin Yang (http://www.yxscripts.com/)
Script featured on/available at http://www.dynamicdrive.com/
This notice must stay intact for use
*/
</script>
<script language="javascript" src="cal_conf2.js"></script>

<cfif isdefined("form.btnFollowUp")>

	<cfoutput>
	<script language="javascript" type="text/javascript">
	opener.location='openInquiry.cfm?i=#i#'
	</script>
	</cfoutput>

	<cfquery name="updateStatus" datasource="#ds#">
		insert into inquiries_followUp (inquiryid, adminuserid, followUpDate, followUpNote)
		values (#form.i#, #cookie.adminlogin#, '#form.followUpDate#', '#form.followUpNote#')
	</cfquery>
	
 	<div align="center">
	<table width="500" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td colspan="2" nowrap class="highlightbar" align="center"><b>Schedule a Personal Follow Up</b></td>
		</tr>
		<tr>
			<td colspan="2" class="greyrow" align="center">
			<cfoutput>
			Your follow up has been scheduled for #form.followUpDate#<br>
			</cfoutput>
			</td>
		</tr>
		<tr>
			<td colspan="2" class="greyrowbottom" align="center">
			<cfoutput>
			<a href="javascript:self.close();" style="text-decoration:underline">Click here to close this window</a>
			</cfoutput>
			</td>
		</tr>
	</table>	
	</div>
	<cfabort>
</cfif>

<cfquery name="getDepartments" datasource="#ds#">
	select * 
	from admin_users_departments
	order by department asc
</cfquery>

<script language="JavaScript" type="text/javascript">
	function checkForm(frm) {
		if (frm.forDeptID.selectedIndex == 0) {alert('Please select the department this inquiry is being sent to.'); frm.forDeptID.focus(); return false;}
		if (frm.forwardedNote.value == "") {alert('Please provide the reason you are forwarding this inquiry.'); frm.forwardedNote.focus(); return false;}
		
		frm.btnForwardInquiry.value = 1;
	}
</script>

<div align="center">
<table width="500" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center" class="highlightbar"><b>Schedule a Personal Follow Up</b></td>
	</tr>
	<tr>
		<td class="greyrow">&nbsp;</td>
	</tr>
	<form method="post" action="personalFollowUp.cfm" name="mainform">
	<cfoutput>
	<input type="hidden" name="i" value="#i#" />
	</cfoutput>
	<tr>
		<td class="greyrow" style="padding:0px">
			<table border="0" cellspacing="0" cellpadding="5" class="grey">
				<tr>
					<td>Follow up date: </td>
					<td>
						<input name="followUpDate" type="text" style="width:75px" value="" maxlength="10" />
					</td>
					<td><a style="text-decoration:none;" href="javascript:showCal('FollowUpDate');"><img src="../images/calicon.gif" alt="Select a Date" align="absmiddle" width="20" height="20" border="0" /></a></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td align="left" class="greyrow">Follow up comments or notes for yourself:</td>
	</tr>
	<tr>
		<td align="left" class="greyrow">
		<textarea name="followUpNote" rows="3" style="width:490px;"></textarea>
		</td>
	</tr>
	<tr>
		<td class="greyrow">&nbsp;</td>
	</tr>
	<tr>
		<td align="center" class="greyrow">
			<input type="submit" name="btnFollowUp" class="lightbutton" value="Schedule Follow Up">
			<input type="button" class="lightbutton" value="Close Window" onclick="self.close();">
		</td>
	</tr>
	</form>
	<tr>
		<td class="greyrowbottom">&nbsp;</td>
	</tr>
</table>
</div>

