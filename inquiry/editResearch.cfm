
<cfsetting showdebugoutput="no">

<cfif isdefined("form.btnEditResearch")>
	<cfparam name="form.researchLevel" default="">
	<cfquery name="UpdateResearch" datasource="#ds#">
		begin
			update inquiries_research
			set researchText = '#form.researchText#', researchLevel = '#form.researchLevel#', researchLocation = '#form.researchLocation#', researchCategory = '#form.researchCategory#' 
			where researchid = #researchid#
		end
		begin
			select inquiryid from inquiries_research where researchid = #researchid#
		end
	</cfquery>

	<cfoutput>
	<script type="text/javascript">
		opener.location='openInquiry.cfm?i=#updateresearch.inquiryid#';
		self.close();
	</script>
	</cfoutput>

<cfelse>

	<cfquery name="getResearch" datasource="#ds#">
		select 
		inquiries_research.* 
		from inquiries_research 
		where researchid = #i#
	</cfquery>
	
	<cfquery name="getResearchCats" datasource="#ds#">
		select * from inquiries_research_options 
		where active = 1 
		order by researchOption asc 
	</cfquery>
	<cfset researchCatList = valuelist(getResearchCats.researchOption)>
	
	<link rel="stylesheet" type="text/css" href="../styles.css">
	
	<div align="center">
	<title>Edit Research</title>
	<body onLoad="this.focus();">
	<table width="475" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td class="highlightbar"><b>Edit Research Item</b></td>
		</tr>
		<form method="post" action="editResearch.cfm">
		<cfoutput>
		<input type="hidden" name="researchid" value="#i#">
		</cfoutput>
		<tr>
			<td class="greyrowbottom" align="center">
				<table border="0" cellpadding="5" cellspacing="0" class="grey">
				<tr>
					<td>
					<cfset locationList = "NJ,FL,TX,AZ,TN,MD">
					Site location of the problem: 
					<select name="researchLocation">
						<option value=""></option>
						<cfoutput>
							<cfloop list="#locationList#" index="site">
								<option <cfif getResearch.researchLocation is site>selected</cfif> value="#site#">#site#</option>
							</cfloop>
						</cfoutput>
					</select>
					</td>
				</tr>
				<tr>
					<td>
					Problem category: 
					<!--- <cfset researchCatList = "2-way Handling,Technical Issue,False Alarm Handling,Account on Test,Telemax,Delay in Handling,Verifying Address,Unprofessional,Passcode Verification"> --->
					<select name="researchCategory">
						<option value=""></option>
						<cfoutput>
							<cfloop list="#researchCatList#" index="cat">
								<option <cfif getResearch.researchCategory is cat>selected</cfif> value="#cat#">#cat#</option>
							</cfloop>
						</cfoutput>
					</select>
					</td>
				</tr>
					<tr>
						<td>
							<textarea name="researchText" rows="5" style="width:450px"><cfoutput>#getResearch.researchText#</cfoutput></textarea>
						</td>
					</tr>
					<cfif listfindnocase(getsec.departmentid, 5) is not 0>
					<tr>
						<td>
							<cfset researchLevelList = "1,2,3,4,5,6,Shift Supervisor,Shift Manager">
							Research Level: 
							<select name="researchLevel">
								<option value="">Please choose</option>
								<cfloop list="#researchLevelList#" index="i">
									<cfoutput>
									<option <cfif getResearch.researchLevel is i>selected</cfif> value="#i#">#i#</option>
									</cfoutput>
								</cfloop>
							</select>
						</td>
					</tr>
					</cfif>
					<tr>
						<td>
							<input name="btnEditResearch" type="submit" class="sidebar" value="Update Research">
						</td>
					</tr>
				</table>
			</td>
		</tr>
		</form>
	</table>
	</body>
	</div>

</cfif>
