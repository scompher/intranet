
<cfsetting showdebugoutput="false" >

<cfparam name="i" default="0" >

<cfif isDefined("form.btnSendReport")>

	<div align="center" style="width:400px;">
		<table>
			<tr><td>&nbsp;</td></tr>
			<tr>
				<td>
					Thank you, inquiry #<cfoutput>#form.inqnum#</cfoutput> has been reported to the bug tracker
				</td>
			</tr>
			<tr><td>&nbsp;</td></tr>
			<tr>
				<td>
				<input type="button" value="Close" onclick="window.close();" />
				</td>
			</tr>
		</table>
	</div>

<cfmail from="#getsec.email#" subject="#form.subject#" to="philipgregory7+680w3qofhubqulpriyam@boards.trello.com" >
project: #form.project#
tracker: bug

Inquiry Number: #form.inqnum#

#form.inqText#

#form.additionalInfo#
</cfmail>

	<cfabort>
</cfif>

<cfquery name="getInqInfo" datasource="intranet">
	select * 
	from inquiries_main 
	where inquiryid = #i#
</cfquery>

<form method="post" action="reportbug.cfm">
	<input type="hidden" name="inqnum" value="<cfoutput>#getinqinfo.inquiryNumber#</cfoutput>">
	<input type="hidden" name="inqText" value="<cfoutput>#getinqinfo.inquiryText#</cfoutput>" />
	<div align="center" style="width:400px;">
		<table>
			<tr>
				<td>
				System Affected
				</td>
			</tr>
			<tr>
				<td>
					<select name="project">
						<option value="">Unassigned</option>
						<option value="d3">D3</option>
						<option value="web">Web</option>
						<option value="receivers">Receivers</option>
						<option value="EP Book">EP Book</option>
					</select>
				</td>
			</tr>
			<tr><td>&nbsp;</td></tr>
			<tr>
				<td>
				Subject
				</td>
			</tr>
			<tr>
				<td>
					<input type="text" name="subject" maxlength="100" style="width:300px;" />
				</td>
			</tr>
			<tr><td>&nbsp;</td></tr>
			<tr>
				<td>
					Inquiry Text:
					<p>
					<cfoutput>
						#getinqinfo.inquiryText#
					</cfoutput>
					</p>
				</td>
			</tr>
			<tr><td>&nbsp;</td></tr>
			<tr>
				<td>
					Enter any additional information below
				</td>
			</tr>
			<tr>
				<td>
					<textarea name="additionalInfo" rows="5" style="width:300px;"></textarea>
				</td>
			</tr>
			<tr><td>&nbsp;</td></tr>
			<tr>
				<td>
					<input type="submit" name="btnSendReport" value="Send Report"></input>
					<input type="reset" value="Reset"></input>
				</td>
			</tr>
		</table>
	</div>
</form>
