
<link rel="stylesheet" type="text/css" href="/styles.css">

<table width="450" border="0" cellspacing="0" cellpadding="5">
    <tr>
        <td class="highlightbar">Manually attach recording(s) to MPower</td>
    </tr>
    <tr>
        <td class="greyrowbottomnopadding">
			<cfform method="post" enctype="multipart/form-data">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td colspan="2">
					    <cfinput type="file" name="recordingFile">
					</td>
				</tr>
				<tr>
				    <td width="24%" nowrap>Recording Date: </td>
				    <td width="76%"><cfinput type="datefield" name="recordingDate" validate="date" required="yes" message="A valid recording date is required, in mm/dd/yyyy format."></td>
				</tr>
				<tr>
				    <td nowrap>Recording Time: (hh:mm:ss)</td>
				    <td><cfinput type="text" name="recordingTime" required="yes" validate="time" message="A valid recording time is required, in 24 hour format (i.e. 13:24:00)."></td>
			    </tr>
				<tr>
				    <td colspan="2">
				        <cfinput type="submit" name="btnAttachMPowerRecording" value="Attach Recording">
				    </td>
				    </tr>
			</table>
			</cfform>
		</td>
    </tr>
</table>

