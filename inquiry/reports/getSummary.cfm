<cfsetting showdebugoutput="yes">

<!--- 
03/17/2017 : PG : Julie Tenerelli requested the "re-posting to mpower" category inquiries be a part of the detail printout, but not count toward the totals or percentages
--->

<!--- get sec level --->
<cfquery name="getsec" datasource="#ds#">
	select admin_users.*, admin_users_departments_lookup.*
	from admin_users 
	inner join admin_users_departments_lookup on admin_users.adminuserid = admin_users_departments_lookup.adminuserid
	where admin_users.adminuserid = #cookie.adminlogin#
</cfquery>
<cfset departmentlist = valuelist(getsec.departmentid)>
<!--- get sec level --->

<link rel="stylesheet" type="text/css" href="../../styles.css">

<script language="javascript" type="text/javascript">
function viewPrintWin(u) {
	window.open(u,"inqWin","width=600,height=500,scrollbars=1");
}
</script>

<cfquery name="getDepts" datasource="#ds#">
	select * 
	from Admin_Users_Departments
	order by department asc
</cfquery>

<cfset startDate = createodbcdate(sd)>
<cfset endDate = createodbcdate(dateadd("d",1,ed))>

<cfquery name="getResearchCats" datasource="#ds#">
	select * from inquiries_research_options 
	where active = 1 
	order by researchOption asc 
</cfquery>
<cfset researchCatList = valuelist(getResearchCats.researchOption)>

<cfquery name="getInqsWithRepostings" datasource="#ds#">
	select 
	inquiries_main.*, 
	inquiries_categories.category, 
	inquiries_status_history.*, 
	Admin_Users_Departments.*, 
	inquiries_inquiryIsOptions.*, 
	inquiries_research.researchLevel,
	inquiries_research.researchLocation,
	inquiries_research.researchCategory
	from inquiries_main 
	inner join inquiries_categories on inquiries_main.categoryid = inquiries_categories.categoryid
	inner join inquiries_status_history on inquiries_main.inquiryid = inquiries_status_history.inquiryid
	inner join Admin_Users_Departments on inquiries_main.inquiryIsDepartmentID = Admin_Users_Departments.departmentid

	left join inquiries_inquiryIsOptions on inquiries_main.inquiryIsID = inquiries_inquiryIsOptions.inquiryIsID
	left join inquiries_research on inquiries_main.inquiryid = inquiries_research.inquiryID 
	where 
	<cfif inquiryIsDepartmentID is not 0>
		<cfif inquiryIsDepartmentID is 5>
			inquiries_main.inquiryIsDepartmentID IN (5,16,17,18,19,20) and 
		<cfelse>
			inquiries_main.inquiryIsDepartmentID = #inquiryIsDepartmentID# and 
		</cfif>
	</cfif>
	<cfif specificSite is not 0>
		inquiries_research.researchLocation = '#specificSite#' and
	</cfif>
	inquiries_status_history.isCurrentStatus = 1 and 
	inquiries_status_history.statusid IN (4,5) and 
	(inquiries_main.dateTimeClosed > #startDate# and inquiries_main.dateTimeClosed < #endDate#)
	order by inquiries_main.inquiryNumber ASC
</cfquery>

<cfquery name="getInqs" datasource="#ds#">
	select 
	inquiries_main.*, 
	inquiries_categories.category, 
	inquiries_status_history.*, 
	Admin_Users_Departments.*, 
	inquiries_inquiryIsOptions.*, 
	inquiries_research.researchLevel,
	inquiries_research.researchLocation,
	inquiries_research.researchCategory
	from inquiries_main 
	inner join inquiries_categories on inquiries_main.categoryid = inquiries_categories.categoryid
	inner join inquiries_status_history on inquiries_main.inquiryid = inquiries_status_history.inquiryid
	inner join Admin_Users_Departments on inquiries_main.inquiryIsDepartmentID = Admin_Users_Departments.departmentid

	left join inquiries_inquiryIsOptions on inquiries_main.inquiryIsID = inquiries_inquiryIsOptions.inquiryIsID
	left join inquiries_research on inquiries_main.inquiryid = inquiries_research.inquiryID 
	where 
	<cfif inquiryIsDepartmentID is not 0>
		<cfif inquiryIsDepartmentID is 5>
			inquiries_main.inquiryIsDepartmentID IN (5,16,17,18,19,20) and 
		<cfelse>
			inquiries_main.inquiryIsDepartmentID = #inquiryIsDepartmentID# and 
		</cfif>
	</cfif>
	<cfif specificSite is not 0>
		inquiries_research.researchLocation = '#specificSite#' and
	</cfif>
	inquiries_status_history.isCurrentStatus = 1 and 
	inquiries_status_history.statusid IN (4,5) and 
	(inquiries_main.dateTimeClosed > #startDate# and inquiries_main.dateTimeClosed < #endDate#)
	and inquiries_main.categoryid <> 27
	order by inquiries_main.inquiryNumber ASC
</cfquery>

<cfquery name="getInqNums" dbtype="query">
	select distinct inquiryid from getInqs order by inquiryid asc 
</cfquery>
<cfset totalInqs = getInqNums.recordcount>

<cfquery name="getFounded" dbtype="query">
	select * from getInqs
	where inquiryIsID IN (1,3)
</cfquery>

<cfif totalInqs is not 0>
	<cfquery name="getUniqueFounded" dbtype="query">
		select distinct inquiryNumber from getInqs
		where inquiryIsID IN (1,3)
	</cfquery>
	<cfset totalFounded = getUniqueFounded.recordcount>
	<cfset totalPercentFounded = (totalFounded / totalInqs) * 100>
<cfelse>
	<cfset totalFounded = 0>
	<cfset totalPercentFounded = 0>
</cfif>

<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="left"><a style="text-decoration:underline" href="summary.cfm">Return to Date Selection</a></td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	<tr>
		<td>
		<table border="0" width="100%" cellspacing="0" cellpadding="5">
			<tr>
				<td class="nopadding">
					<table border="0" cellspacing="0" cellpadding="5">
						<cfoutput>
						<tr>
							<td>
							<a target="_blank" href="summary_printall.cfm?sd=#sd#&ed=#ed#&did=#inquiryisdepartmentid#"><img src="../../images/printerfriendlyicon.gif" alt="Print" width="30" height="30" border="0" /></a></td>
							<td><a style="text-decoration:underline;" target="_blank" href="summary_printall.cfm?sd=#sd#&ed=#ed#&did=#inquiryIsDepartmentID#">Print this report</a></td>
							<td>(this report requires adobe acrobat reader. <a href="/download/adobe_reader.exe" style="text-decoration:underline;">click here to download</a> if you do not have it)</td>
						</tr>
						</cfoutput>
					</table>
				</td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td align="center"><b>Closed Inquiry Report Summary</b></td>
	</tr>
	<cfoutput>
	<tr>
		<td align="center"><b>From #dateformat(sd,'mm/dd/yyyy')# to #dateformat(ed,'mm/dd/yyyy')#</b></td>
	</tr>
	</cfoutput>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td class="nopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td align="center"><b>Total Inquiries </b></td>
					<td align="center"><b>Total Founded </b></td>
					<td align="center"><b>Total % Founded </b></td>
				</tr>
				<cfoutput>
				<tr>
					<td align="center">#totalInqs#</td>
					<td align="center">#totalFounded#</td>
					<td align="center">#numberformat(totalPercentFounded)#%</td>
				</tr>
				</cfoutput>
			</table>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td class="nopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td align="left" valign="bottom"><b>Department</b></td>
					<td align="center" valign="bottom"><b>Total <br />
					Inquiries </b></td>
					<td align="center" valign="bottom"><b>Total <br />
					Founded </b></td>
					<td align="center" valign="bottom"><b>% Founded<br />
					in Dept. </b></td>
					<td align="center" valign="bottom"><b>% Founded by Dept.<br />
					out of total founded </b></td>
					<td align="center" valign="bottom"><b>% inquiries by dept.<br />
					out of total inquiries </b></td>
				</tr>
				<cfloop query="getDepts">
					<cfquery name="getDeptTotal" dbtype="query">
						select distinct inquirynumber  
						from getInqs
						where inquiryIsDepartmentID = #getDepts.departmentid#
					</cfquery>
					<cfset deptTotal = getDeptTotal.recordcount>
					
					<cfif deptTotal gt 0>
						<cfquery name="getDeptFounded" dbtype="query">
							select distinct inquirynumber  
							from getFounded 
							where inquiryIsDepartmentID = #getDepts.departmentid#
						</cfquery>
						<cfset deptFounded = getDeptFounded.recordcount>
						
						<cfset percentFoundedInDepartment = (deptFounded / deptTotal) * 100>
						<cfif totalFounded is not 0>
							<cfset percentFoundedByDeparment = (deptFounded / totalFounded) * 100>
						<cfelse>
							<cfset percentFoundedByDeparment = 0 * 100>
						</cfif>
						<cfset percentInqsByDepartment = (deptTotal / totalInqs) * 100>
					<cfelse>
						<cfset deptFounded = 0>
						<cfset percentFoundedInDepartment = 0>
						<cfset percentFoundedByDeparment = 0>
						<cfset percentInqsByDepartment = 0>
					</cfif>
					<cfoutput>
					<tr>
						<td>#department#</td>
						<td align="center">#deptTotal#</td>
						<td align="center">#deptFounded#</td>
						<td align="center">#numberformat(percentFoundedInDepartment)#%</td>
						<td align="center">#numberformat(percentFoundedByDeparment)#%</td>
						<td align="center">#numberformat(percentInqsByDepartment)#%</td>
					</tr>
					</cfoutput>
					<cfif  department is 'Operations'>
						<!--- <cfset researchCatList = "2-way Handling,Technical Issue,False Alarm Handling,Account on Test,Telemax,Delay in Handling,Verifying Address,Unprofessional,Passcode Verification"> --->
						<cfoutput>
						<cfif inquiryIsDepartmentID is 5>
							<cfset category="Uncategorized">
								<cfquery name='uncategorizedCount' dbtype="query">
									select distinct inquiryNumber
									from getInqs
									where researchCategory=''
								</cfquery>
								<cfquery name='uncategorizedFounded' dbtype='query'>
									select distinct inquirynumber
									from getFounded
									where researchCategory=''								
								</cfquery>
								<cfset categoryFounded=uncategorizedFounded.recordcount>
								<cfset getCategoryTotal=uncategorizedCount.recordcount>
								<cfif deptTotal is 0>
									<cfset percentFoundedInDepartment = 0>
									<cfset percentCategoryOfTotal = 0>
								<cfelse>
									<cfset percentFoundedInDepartment = (categoryFounded / deptTotal) * 100>
									<cfset percentCategoryOfTotal = (getCategoryTotal / deptTotal) * 100>
								</cfif>
								<cfif deptFounded is 0>
									<cfset percentFoundedOfTotal = 0>
								<cfelse>
									<cfset percentFoundedOfTotal = (categoryFounded / deptFounded) * 100>
								</cfif>
								
								<tr>
									<td>&nbsp;&nbsp;&nbsp;#category#</td>
									<td align="center">#getCategoryTotal#</td>
									<td align="center">#categoryFounded#</td>
									<td align="center">#numberformat(percentFoundedInDepartment)#%</td>
									<td align="center">#numberformat(percentFoundedOfTotal)#%</td>
									<td align="center">#numberformat(percentCategoryOfTotal)#%</td>
								</tr>
							<cfloop list='#researchCatList#' index='category'>
								<cfquery name='categoryFounded' dbtype='query'>
									select distinct inquirynumber
									from getFounded
									where researchCategory='#category#'								
								</cfquery>
								<cfquery name='categoryCount' dbtype='query'>
									select distinct inquiryNumber
									from getInqs
									where researchCategory='#category#'
								</cfquery>
								<cfset categoryFounded=categoryFounded.recordcount>
								<cfset getCategoryTotal=categoryCount.recordcount>
								<cfif deptTotal is 0>
									<cfset percentFoundedInDepartment = 0>
									<cfset percentCategoryOfTotal = 0>
								<cfelse>
									<cfset percentFoundedInDepartment = (categoryFounded / deptTotal) * 100>
									<cfset percentCategoryOfTotal = (getCategoryTotal / deptTotal) * 100>
								</cfif>
								<cfif deptFounded is 0>
									<cfset percentFoundedOfTotal = 0>
								<cfelse>
									<cfset percentFoundedOfTotal = (categoryFounded / deptFounded) * 100>
								</cfif>
								<tr>
									<td>&nbsp;&nbsp;&nbsp;#category#</td>
									<td align="center">#getCategoryTotal#</td>
									<td align="center">#categoryFounded#</td>
									<td align="center">#numberformat(percentFoundedInDepartment)#%</td>
									<td align="center">#numberformat(percentFoundedOfTotal)#%</td>
									<td align="center">#numberformat(percentCategoryOfTotal)#%</td>
								</tr>
								
							</cfloop>
							</cfif>
						</cfoutput>
					</cfif>
				</cfloop>
				<cfif listfind(departmentlist,5) is not 0>
				</table>
				<table>
					<tr>
						<td><b><span>Level 1</span></b></td>
						<td><b><span style="background-color:009933;color:#FFFFFF">Level 2</span></b></td>
						<td><b><span style="background-color:FF0000;color:#FFFFFF">Level 3</span></b></td>
						<td><b><span style="background-color:FF0FFF;color:#FFFFFF">Level 4</span></b></td>
						<td><b><span style="background-color:CC33FF;color:#FFFFFF">Level 5</span></b></td>
						<td><b><span style="background-color:0000FF;color:#FFFFFF">Level 6</span></b></td>
					</tr>
				</cfif>				
			</table>
		</td>
	</tr>
</table>
<br>
<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<cfif listfind(departmentlist,5) is not 0>
			<td>&nbsp;</td>
		</cfif>
		<td>&nbsp;</td>
		<td align="center"><b>Inquiry # </b></td>
		<td align="center"><b>Generated</b></td>
		<td align="center"><b>Completed</b></td>
		<td align="center"><b>Dealer # </b></td>
		<td align="center"><b>Account # </b></td>
		<td align="center"><b>Category</b></td>
		<td align="center"><b>Conclusion</b></td>
		<td align="center"><b>Dept</b></td>
		<cfif inquiryIsDepartmentID is '5'>
			<td align="center"><b>Site</b></td>
			<td align="center"><b>Category</b></td>
		</cfif>
	</tr>
	<cfset count = 1>
	<cfoutput query="getInqsWithRepostings" group="inquiryid">
	<tr>
		<cfif listfind(departmentlist,5) is not 0>
			<cfswitch expression="#trim(getInqsWithRepostings.researchLevel)#">
				<cfcase value="1"><cfset bgc = "000000"></cfcase>
				<cfcase value="2"><cfset bgc = "009933"></cfcase>
				<cfcase value="3"><cfset bgc = "FF0000"></cfcase>
				<cfcase value="4"><cfset bgc = "FF0FFF"></cfcase>
				<cfcase value="5"><cfset bgc = "CC33FF"></cfcase>
				<cfcase value="6"><cfset bgc = "0000FF"></cfcase>
				<cfcase value="Shift Supervisor"><cfset bgc = "000000"></cfcase>
				<cfcase value="Shift Manager"><cfset bgc = "000000"></cfcase>				
				<cfdefaultcase><cfset bgc = "FFFFFF"></cfdefaultcase>
			</cfswitch>
			<td valign="middle" align="center" bgcolor="#bgc#"><img src="/images/spacer.gif" width="5" height="1" /></td>
		</cfif>
		<td align="right" width="1%">#count#.</td>
		<td align="center"><a href="javascript:viewPrintWin('../viewInquiry.cfm?i=#inquiryid#');" style="text-decoration:underline;">#inquiryNumber#</a></td>
		<td align="center">#dateformat(dateTimeCreated,'mm/dd/yyyy')#</td>
		<td align="center">#dateformat(dateTimeClosed,'mm/dd/yyyy')#</td>
		<td align="center">#dealerNumber#</td>
		<td align="center">#rec#&ndash;#acct#</td>
		<td align="center">#category#</td>
		<td align="center">#inquiryIs#</td>
		<td align="center">#department#</td>
		<cfif inquiryIsDepartmentID is '5'>
			<td align="center">#researchLocation#</td>
			<td align="center">#researchCategory#</td>
		</cfif>
	</tr>
	<cfset count = count + 1>
	</cfoutput>
</table>
