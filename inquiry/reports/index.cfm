
<link rel="stylesheet" type="text/css" href="../../styles.css">

<div align="center">
<table border="0" cellspacing="0" cellpadding="7">
	<tr>
		<td class="highlightbar" align="center"><b>Inquiry Reporting Menu</b></td>
	</tr>
	<tr>
		<td class="greyrow">1. <a href="summary.cfm" style="text-decoration:underline;">Summary Report</a></td>
	</tr>
	<tr>
		<td class="greyrow">2. <a href="overdue.cfm" style="text-decoration:underline;">Overdue Report</a></td>
	</tr>
	<tr>
		<td class="greyrow">3. <a href="open.cfm" style="text-decoration:underline;">Open Inquiry Report</a></td>
	</tr>
	<tr>
		<td class="greyrow">4. <a href="category.cfm" style="text-decoration:underline;">Inquiry Report by Category Summary</a></td>
	</tr>
	<tr>
		<td class="greyrow">5. <a href="inquiriesbycategory.cfm" style="text-decoration:underline;">Inquiry Report by Specific Category</a></td>
	</tr>
	<tr>
		<td class="greyrow">6. <a href="dealerInquiries.cfm" style="text-decoration:underline;">Inquiries by Dealer</a></td>
	</tr>
	<tr>
		<td class="greyrowbottom">7. <a href="other.cfm" style="text-decoration:underline;">Report of "Other" category write-ins</a></td>
	</tr>
</table>
<br />
<a href="/index.cfm" class="normal" style="text-decoration:underline;">Return to Intranet Menu</a>
</div>

