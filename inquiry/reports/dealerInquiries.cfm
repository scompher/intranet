
<cfparam name="dealerNumber" default="">
<cfparam name="sd" default="#now()#">
<cfparam name="ed" default="#now()#">

<cfset sd = createodbcdate(sd)>
<cfset ed = createodbcdate(ed)>
<cfset endDate = dateadd("d",1,ed)>
<cfset ed = endDate>

<script language="javascript" src="../cal2.js">
/*
Xin's Popup calendar script-  Xin Yang (http://www.yxscripts.com/)
Script featured on/available at http://www.dynamicdrive.com/
This notice must stay intact for use
*/
</script>
<script language="javascript" src="../cal_conf2.js"></script>

<script language="javascript" type="text/javascript">
function viewPrintWin(u) {
	window.open(u,"inqWin","width=600,height=500,scrollbars=1");
}
</script>

<link rel="stylesheet" type="text/css" href="../../styles.css">

<cfquery name="getDealer" datasource="#ds#">
	select dealerName 
	from dealerdiary_dealers
	where dealerNumber = '#dealerNumber#'
</cfquery>

<cfquery name="getInqs" datasource="#ds#">
	select 
	inquiries_main.inquiryid, 
	inquiries_main.inquirynumber, 
	inquiries_main.rec, 
	inquiries_main.acct, 
	inquiries_main.dateTimeCreated,
	inquiries_main.dateTimeClosed,
	Admin_Users_Departments.department, 
	inquiries_categories.category, 
	inquiries_inquiryIsOptions.inquiryIs as conclusion
	from inquiries_main 
	inner join inquiries_status_history on inquiries_main.inquiryid = inquiries_status_history.inquiryid
	inner join Admin_Users_Departments on inquiries_status_history.toDepartmentID = Admin_Users_Departments.departmentid
	inner join inquiries_categories on inquiries_main.categoryid = inquiries_categories.categoryid
	left join inquiries_inquiryIsOptions on inquiries_main.inquiryIsID = inquiries_inquiryIsOptions.inquiryIsID
	where 
	dealernumber = '#dealerNumber#' and 
	<!--- (dateTimeCreated > #dateadd("d",-1,sd)# and dateTimeCreated < #ed#) and  --->
	inquiries_status_history.isCurrentStatus = 1
</cfquery>

<div align="center">
<table width="750" border="0" cellpadding="5" cellspacing="0">
	
	<tr>
		<td align="center"><b>Inquiries By Dealer</b></td>
	</tr>
	
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td align="center" class="nopadding">
			<form method="post" action="dealerInquiries.cfm" name="mainform">
			<table border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td class="highlightbar"><b>Enter Dealer Number and Date Generated Range:</b></td>
				</tr>
				<tr>
					<td class="greyrow" style="padding:0px">
						<table border="0" cellpadding="5" cellspacing="0" class="grey">
							<tr>
								<td>Dealer Number:</td>
								<td>
									<input name="dealerNumber" type="text" style="width:45px" maxlength="4" value="<cfoutput>#dealerNumber#</cfoutput>" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<!--- 
				<tr>
					<td class="greyrow" style="padding:0px">
					<cfoutput>
						<table border="0" cellpadding="5" cellspacing="0" class="grey">
							<tr>
								<td>Starting:</td>
								<td>
									<input name="sd" type="text" id="sd" style="width:75px" value="#dateformat(sd, 'mm/dd/yyyy')#" maxlength="10" />
								</td>
								<td> <a style="text-decoration:none;" href="javascript:showCal('startDate');"> <img src="/images/calicon.gif" alt="Select a Date" align="absmiddle" width="20" height="20" border="0" /> </a> </td>
								<td width="5">&nbsp;</td>
								<td>Ending:</td>
								<td>
									<input name="ed" type="text" id="ed" style="width:75px" value="#dateformat(ed, 'mm/dd/yyyy')#" maxlength="10" />
								</td>
								<td><a style="text-decoration:none;" href="javascript:showCal('endDate');"><img src="/images/calicon.gif" alt="Select a Date" align="absmiddle" width="20" height="20" border="0" /></a></td>
							</tr>
						</table>
					</cfoutput>
					</td>
				</tr>
				 --->
				<tr>
					<td align="center" class="greyrowbottom">
					<input name="Submit" type="submit" class="sidebar" value="Get Report" />
					</td>
				</tr>
			</table>
			</form>
		</td>
	</tr>
	<tr>
		<td class="nopadding">
			<table border="0" cellspacing="0" cellpadding="5">
				<cfoutput>
				<cfif getInqs.recordcount gt 0>
					<tr>
						<!--- <td><a target="_blank" href="dealerInquiries_printall.cfm?dn=#dealerNumber#&sd=#dateformat(sd,'mm/dd/yyyy')#&ed=#dateformat(ed,'mm/dd/yyyy')#"><img src="../../images/printerfriendlyicon.gif" alt="Print" width="30" height="30" border="0" /></a></td> --->
						<td><a target="_blank" href="dealerInquiries_printall.cfm?dn=#dealerNumber#"><img src="../../images/printerfriendlyicon.gif" alt="Print" width="30" height="30" border="0" /></a></td>
						<td><a target="_blank" href="dealerInquiries_printall.cfm?dn=#dealerNumber#" style="text-decoration:underline;">Print these inquiries</a></td>
						<td>(this report requires adobe acrobat reader. <a href="/download/adobe_reader.exe" style="text-decoration:underline;">click here to download</a> if you do not have it)</td>
					</tr>
				<cfelse>
					<tr><td>&nbsp;</td></tr>
				</cfif>
				</cfoutput>
			</table>
		</td>
	</tr>
	<tr>
		<td><b><cfoutput>#getDealer.dealerName#</cfoutput></b></td>
	</tr>
	<tr>
		<td class="nopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td><b>Inquiry # </b></td>
					<td><b>Account # </b></td>
					<td><b>Dept.</b></td>
					<td><b>Generated</b></td>
					<td><b>Completed</b></td>
					<td><b>Category</b></td>
					<td><b>Conclusion</b></td>
				</tr>
				<cfoutput query="getInqs">
				<tr>
					<td><a href="javascript:viewPrintWin('../viewInquiry.cfm?i=#inquiryid#');" style="text-decoration:underline;">#inquiryNumber#</a></td>
					<td>#rec#&ndash;#acct#</td>
					<td>#department#</td>
					<td>#dateformat(dateTimeCreated,'mm/dd/yyyy')#</td>
					<td>#dateformat(dateTimeClosed,'mm/dd/yyyy')#</td>
					<td>#category#</td>
					<td>#conclusion#</td>
				</tr>
				</cfoutput>
			</table>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
</table>
<p class="normal"><a style="text-decoration:underline" href="index.cfm">Return to Reporting Menu</a> </p>
</div>
