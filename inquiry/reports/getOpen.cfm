
<link rel="stylesheet" type="text/css" href="../../styles.css">

<script language="javascript" type="text/javascript">
function viewPrintWin(u) {
	window.open(u,"inqWin","width=600,height=500,scrollbars=1");
}
</script>

<cfquery name="getDepts" datasource="#ds#">
	select * 
	from Admin_Users_Departments
	order by department asc
</cfquery>

<cfquery name="getOpen" datasource="#ds#">
	select 
	inquiries_main.*, 
	inquiries_categories.category, 
	inquiries_status.status, 
	inquiries_status_history.*, 
	toDepartments.department as toDeptName, 
	fromDepartments.department as fromDeptName, 
	statusUsers.firstname, 
	statusUsers.lastname, 
	inquiries_inquiryIsOptions.* 
	from inquiries_main 
	inner join inquiries_categories on inquiries_main.categoryid = inquiries_categories.categoryid
	inner join inquiries_status_history on inquiries_main.inquiryid = inquiries_status_history.inquiryid
	left join Admin_Users_Departments as toDepartments on inquiries_status_history.toDepartmentID = toDepartments.departmentid
	left join Admin_Users_Departments as fromDepartments on inquiries_status_history.fromDepartmentID = fromDepartments.departmentid
	left join admin_users as statusUsers on inquiries_status_history.adminuserid = statusUsers.adminuserid
	left join inquiries_inquiryIsOptions on inquiries_main.inquiryIsID = inquiries_inquiryIsOptions.inquiryIsID
	left join inquiries_status on inquiries_status_history.statusid = inquiries_status.statusid
	where 
	inquiries_status_history.isCurrentStatus = 1 and inquiries_status_history.statusid NOT IN (4,5)
	order by inquiries_main.inquiryNumber ASC
</cfquery>

<cfset totalOpen = getOpen.recordcount>

<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="left"><a style="text-decoration:underline" href="index.cfm">Return to Reporting Menu</a></td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	<tr>
		<td>
		<table border="0" width="100%" cellspacing="0" cellpadding="5">
			<tr>
				<td class="nopadding">
					<table border="0" cellspacing="0" cellpadding="5">
						<cfoutput>
						<tr>
							<td>
							<a target="_blank" href="open_printall.cfm"><img src="../../images/printerfriendlyicon.gif" alt="Print" width="30" height="30" border="0" /></a></td>
							<td><a style="text-decoration:underline;" target="_blank" href="open_printall.cfm">Print this report</a></td>
							<td>(this report requires adobe acrobat reader. <a href="/download/adobe_reader.exe" style="text-decoration:underline;">click here to download</a> if you do not have it)</td>
						</tr>
						</cfoutput>
					</table>
				</td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td align="center"><b>Open Inquiry Report Summary as of <cfoutput>#dateformat(now(), 'mm/dd/yyyy')#</cfoutput></b></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td class="nopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td align="center"><b>Total Open </b></td>
				</tr>
				<cfoutput>
				<tr>
					<td align="center">#totalOpen#</td>
				</tr>
				</cfoutput>
			</table>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td class="nopadding" align="center">
			<table width="500" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td align="left" valign="bottom"><b>Department</b></td>
					<td align="center" valign="bottom"><b>Total Open </b></td>
					<td align="center" valign="bottom"><b>% Open in Department </b></td>
				</tr>
				<cfloop query="getDepts">
					<cfset deptOpen = 0>
					<cfset percentOpenByDeparment = 0>
					
					<cfquery name="getDeptTotal" dbtype="query">
						select * 
						from getOpen
						where toDepartmentID = #getDepts.departmentid#
					</cfquery>
					<cfset deptTotal = getDeptTotal.recordcount>
					
					<cfif deptTotal gt 0>
						<cfset percentOpenByDeparment = (deptTotal / totalOpen) * 100>
					<cfelse>
						<cfset percentOpenByDeparment = 0>
					</cfif>
					<cfoutput>
					<tr>
						<td>#department#</td>
						<td align="center">#deptTotal#</td>
						<td align="center">#numberformat(percentOpenByDeparment)#%</td>
					</tr>
					</cfoutput>
				</cfloop>				
			</table>
		</td>
	</tr>
</table>
<br>
<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center"><b>Inquiry # </b></td>
		<td align="center"><b>Dept</b></td>
		<td align="center"><b>Generated</b></td>
		<td align="center"><b>Dealer # </b></td>
		<td align="center"><b>Account # </b></td>
		<td align="center"><b>Category</b></td>
		<td align="left"><b>Current Status</b></td>
	</tr>
	<cfoutput query="getOpen">
	<tr>
		<td align="center"><a href="javascript:viewPrintWin('../viewInquiry.cfm?i=#inquiryid#');" style="text-decoration:underline;">#inquiryNumber#</a></td>
		<td align="center">#toDeptName#</td>
		<td align="center">#dateformat(dateTimeCreated,'mm/dd/yyyy')#</td>
		<td align="center">#dealerNumber#</td>
		<td align="center">#rec#&ndash;#acct#</td>
		<td align="center">#category#</td>
		<td align="left">
		#status#
		<cfswitch expression="#statusid#">
			<cfcase value="1"> by </cfcase>
			<cfcase value="3"> from #fromDeptName# to #toDeptName# by </cfcase>
			<cfcase value="4"> by </cfcase>
			<cfdefaultcase> by </cfdefaultcase>
		</cfswitch>
		#firstname# #lastname# at #dateformat(dateTimeUpdated, 'mm/dd/yyyy')# #timeformat(dateTimeUpdated, 'hh:mm tt')#
		</td>
	</tr>
	</cfoutput>
</table>


