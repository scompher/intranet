
<link rel="stylesheet" type="text/css" href="../../styles.css">

<cfquery name="getInqs" datasource="#ds#">
	select 
	inquiries_main.*, 
	inquiries_status_history.*, 
	inquiries_status.status,
	statusUsers.firstname, 
	statusUsers.lastname, 
	fromDepartment.department as fromDeptName, 
	toDepartment.department as toDeptName, 
	inquiries_categories.* 
	from inquiries_main
	left join inquiries_status_history on inquiries_main.inquiryid = inquiries_status_history.inquiryid
	left join inquiries_status on inquiries_status_history.statusid = inquiries_status.statusid
	left join admin_users as statusUsers on inquiries_status_history.adminuserid = statusUsers.adminuserid
	left join admin_users_departments as fromDepartment on inquiries_status_history.fromDepartmentid = fromDepartment.departmentid
	left join admin_users_departments as toDepartment on inquiries_status_history.toDepartmentid = toDepartment.departmentid
	left join inquiries_categories on inquiries_main.categoryid = inquiries_categories.categoryid
	where inquiries_status_history.isCurrentstatus = 1
	order by dateTimeUpdated ASC
</cfquery>

<cfquery name="" datasource="#ds#"></cfquery>

<div align="center">
<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td colspan="2"><b>Filter Controls</b></td>
	</tr>
	<tr>
		<td width="9%">Department:</td>
		<td width="91%">
			<select name="select">

			</select>
			</td>
	</tr>
	<tr>
		<td>Status:</td>
		<td>
			<select name="select2">
			</select>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<input type="submit" name="Submit" value="Apply Filters" />
		</td>
		</tr>
</table>
<br />
<br />
<table border="0" width="725" cellspacing="0" cellpadding="5">
	<tr>
		<td style="padding:0px">
			<table width="100%" border="1" cellspacing="0" cellpadding="5">
				<tr>
					<td align="center"><b>Inquiry # </b></td>
					<td align="center"><b>Dept</b></td>
					<td align="center"><b>Created</b></td>
					<td><b>Current Status</b></td>
					<td><b>Category</b></td>
				</tr>
				<cfoutput query="getInqs">
				<tr>
					<td align="center">#inquiryNumber#</td>
					<td align="center">#toDeptName#</td>
					<td align="center">#dateformat(dateTimeCreated,'mm/dd/yyyy')#</td>
					<td>
					#status#
					<cfswitch expression="#statusid#">
						<cfcase value="1"> by </cfcase>
						<cfcase value="3"> from #fromDeptName# to #toDeptName# by </cfcase>
						<cfcase value="4"> by </cfcase>
						<cfdefaultcase> by </cfdefaultcase>
					</cfswitch>
					#firstname# #lastname# at #dateformat(dateTimeUpdated, 'mm/dd/yyyy')# #timeformat(dateTimeUpdated, 'hh:mm tt')#
					</td>
					<td>#category#</td>
				</tr>
				</cfoutput>
			</table>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
</table>
</div>

