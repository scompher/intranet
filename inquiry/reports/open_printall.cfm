<cfdocument format="pdf" orientation="portrait" scale="75">

<link rel="stylesheet" type="text/css" href="../../styles.css">

<script language="javascript" type="text/javascript">
function viewPrintWin(u) {
	window.open(u,"inqWin","width=600,height=500,scrollbars=1");
}
</script>

<cfquery name="getDepts" datasource="#ds#">
	select * 
	from Admin_Users_Departments
	order by department asc
</cfquery>

<!--- 
<cfset startDate = createodbcdate(sd)>
<cfset endDate = createodbcdate(dateadd("d",1,ed))>
--->

<!--- <cfquery name="getInqs" datasource="#ds#">
	select 
	inquiries_main.*, 
	inquiries_categories.category, 
	inquiries_status.status, 
	inquiries_status_history.*, 
	toDepartments.department as toDeptName, 
	fromDepartments.department as fromDeptName, 
	statusUsers.firstname, 
	statusUsers.lastname, 
	inquiries_inquiryIsOptions.* 
	from inquiries_main 
	inner join inquiries_categories on inquiries_main.categoryid = inquiries_categories.categoryid
	inner join inquiries_status_history on inquiries_main.inquiryid = inquiries_status_history.inquiryid
	left join Admin_Users_Departments as toDepartments on inquiries_status_history.toDepartmentID = toDepartments.departmentid
	left join Admin_Users_Departments as fromDepartments on inquiries_status_history.fromDepartmentID = fromDepartments.departmentid
	left join admin_users as statusUsers on inquiries_status_history.adminuserid = statusUsers.adminuserid
	left join inquiries_inquiryIsOptions on inquiries_main.inquiryIsID = inquiries_inquiryIsOptions.inquiryIsID
	left join inquiries_status on inquiries_status_history.statusid = inquiries_status.statusid
	where 
	inquiries_status_history.isCurrentStatus = 1 and inquiries_status_history.statusid <> 4
	order by inquiries_main.inquiryNumber ASC
</cfquery> --->

<cfquery name="getOpen" datasource="#ds#">
	select 
	inquiries_main.*, 
	inquiries_categories.category, 
	inquiries_status.status, 
	inquiries_status_history.*, 
	toDepartments.department as toDeptName, 
	fromDepartments.department as fromDeptName, 
	statusUsers.firstname, 
	statusUsers.lastname, 
	inquiries_inquiryIsOptions.* 
	from inquiries_main 
	inner join inquiries_categories on inquiries_main.categoryid = inquiries_categories.categoryid
	inner join inquiries_status_history on inquiries_main.inquiryid = inquiries_status_history.inquiryid
	left join Admin_Users_Departments as toDepartments on inquiries_status_history.toDepartmentID = toDepartments.departmentid
	left join Admin_Users_Departments as fromDepartments on inquiries_status_history.fromDepartmentID = fromDepartments.departmentid
	left join admin_users as statusUsers on inquiries_status_history.adminuserid = statusUsers.adminuserid
	left join inquiries_inquiryIsOptions on inquiries_main.inquiryIsID = inquiries_inquiryIsOptions.inquiryIsID
	left join inquiries_status on inquiries_status_history.statusid = inquiries_status.statusid
	where 
	inquiries_status_history.isCurrentStatus = 1 and inquiries_status_history.statusid <> 4
	order by inquiries_main.inquiryNumber ASC
</cfquery>

<cfset totalOpen = getOpen.recordcount>
<!--- 
<cfquery name="getOpen" dbtype="query">
	select * from getInqs
	where statusid <> 4
</cfquery> --->
	
<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="left"><a style="text-decoration:underline" href="open.cfm">Return to Date Selection</a></td>
	</tr>
	<tr>
		<td align="center"><b>Open Inquiry Report Summary as of <cfoutput>#dateformat(now(), 'mm/dd/yyyy')#</cfoutput></b></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td class="nopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td align="center"><b>Total Open </b></td>
				</tr>
				<cfoutput>
				<tr>
					<td align="center">#totalOpen#</td>
				</tr>
				</cfoutput>
			</table>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td class="nopadding" align="center">
			<table width="500" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td align="left" valign="bottom"><b>Department</b></td>
					<td align="center" valign="bottom"><b>Total Open </b></td>
					<td align="center" valign="bottom"><b>% Open in Department </b></td>
				</tr>
				<cfloop query="getDepts">
					<cfset deptOpen = 0>
					<cfset percentOpenByDeparment = 0>
					
					<cfquery name="getDeptTotal" dbtype="query">
						select * 
						from getOpen
						where toDepartmentID = #getDepts.departmentid#
					</cfquery>
					<cfset deptTotal = getDeptTotal.recordcount>
					
					<cfif deptTotal gt 0>
						<cfset percentOpenByDeparment = (deptTotal / totalOpen) * 100>
					<cfelse>
						<cfset percentOpenByDeparment = 0>
					</cfif>
					<cfoutput>
					<tr>
						<td>#department#</td>
						<td align="center">#deptTotal#</td>
						<td align="center">#numberformat(percentOpenByDeparment)#%</td>
					</tr>
					</cfoutput>
				</cfloop>				
			</table>
		</td>
	</tr>
</table>

<cfdocumentitem type="pagebreak"></cfdocumentitem>

<cfloop query="getOpen">
	<cfset i = getOpen.inquiryid>
	<cf_printInq i="#i#" datasource="#ds#">
	<cfdocumentitem type="footer"><cfoutput>Page #cfdocument.currentpagenumber# of #cfdocument.totalpagecount#</cfoutput></cfdocumentitem>
	<cfdocumentitem type="pagebreak"></cfdocumentitem>
</cfloop>

</cfdocument>
