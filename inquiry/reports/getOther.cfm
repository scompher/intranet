
<link rel="stylesheet" type="text/css" href="../../styles.css">

<script language="javascript" type="text/javascript">
function viewPrintWin(u) {
	window.open(u,"inqWin","width=600,height=500,scrollbars=1");
}
</script>

<cfparam name="includeClosed" default="0">
<cfparam name="sd" default="#createodbcdate(now())#">
<cfparam name="ed" default="#createodbcdate(now())#">

<cfset startDate = createodbcdate(sd)>
<cfset endDate = createodbcdate(dateadd("d",1,ed))>

<cfquery name="getInqs" datasource="#ds#">
	select inquiries_main.* 
	from inquiries_main 
	inner join inquiries_status_history on inquiries_main.inquiryid = inquiries_status_history.inquiryid
	where 
	(inquiries_main.dateTimeCreated > #startDate# and inquiries_main.dateTimeCreated < #endDate#) and 
	inquiries_status_history.isCurrentStatus = 1 and 
	<cfif includeClosed is 0>inquiries_status_history.statusid <> 4 and </cfif>
	inquiries_main.categoryid = 15 
	order by inquiries_main.inquiryNumber ASC
</cfquery>

<cfset totalInqs = getInqs.recordcount>

<div align="center">
<table width="750" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="left"><a style="text-decoration:underline" href="other.cfm">Return to Date Selection</a></td>
	</tr>
	<tr>
		<td align="center"><b>Inquiry Report by Category</b></td>
	</tr>
	<cfoutput>
	<tr>
		<td align="center"><b>From #dateformat(sd,'mm/dd/yyyy')# to #dateformat(ed,'mm/dd/yyyy')#</b></td>
	</tr>
	</cfoutput>
	<tr><td>&nbsp;</td></tr>
	<tr>
		<td class="nopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td align="center"><b>Total Inquiries </b></td>
				</tr>
				<cfoutput>
				<tr>
					<td align="center">#totalInqs#</td>
				</tr>
				</cfoutput>
			</table>
		</td>
	</tr>
</table>
<br>
<table  border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td align="center"><b>Inquiry #</b></td>
		<td width="10">&nbsp;</td>
		<td><b>Other Category</b></td>
	</tr>
	<cfoutput query="getinqs">
	<tr>
		<td align="center"><a href="javascript:viewPrintWin('../viewInquiry.cfm?i=#inquiryid#');" style="text-decoration:underline;">#inquiryNumber#</a></td>
		<td>&nbsp;</td>
		<td>#getinqs.otherCategory#</td>
	</tr>
	</cfoutput>
</table>
</div>
