
<cfset i = attributes.i>
<cfset ds = attributes.datasource>

<cfquery name="getStatusHist" datasource="#ds#">
	select 
	inquiries_status_history.*, 
	inquiries_status.status,
	statusUsers.firstname, 
	statusUsers.lastname, 
	fromDepartment.department as fromDeptName, 
	toDepartment.department as toDeptName
	from inquiries_status_history
	left join inquiries_status on inquiries_status_history.statusid = inquiries_status.statusid
	left join admin_users as statusUsers on inquiries_status_history.adminuserid = statusUsers.adminuserid
	left join admin_users_departments as fromDepartment on inquiries_status_history.fromDepartmentid = fromDepartment.departmentid
	left join admin_users_departments as toDepartment on inquiries_status_history.toDepartmentid = toDepartment.departmentid
	where inquiries_status_history.inquiryid = #i#
	order by dateTimeUpdated ASC
</cfquery>

<cfquery name="getInq" datasource="#ds#">
	select 
	inquiries_main.*, 
	inquiries_categories.category, 
	inquiries_inquiryIsOptions.*, 
	inquiries_attachments.savedFile, 
	inquiries_attachments.clientFile 
	from inquiries_main
	left join inquiries_inquiryIsOptions on inquiries_main.inquiryIsID = inquiries_inquiryIsOptions.inquiryIsID
	left join inquiries_attachments on inquiries_main.inquiryID = inquiries_attachments.inquiryID
	left join inquiries_categories on inquiries_main.categoryid = inquiries_categories.categoryid
	where inquiries_main.inquiryid = #i#
</cfquery>

<cfquery name="getResearch" datasource="#ds#">
	select 
	inquiries_research.*, 
	admin_users.firstname, 
	admin_users.lastname 
	from inquiries_research 
	left join admin_users on inquiries_research.creatorid = admin_users.adminuserid
	where inquiries_research.inquiryid = #i#
	order by inquiries_research.researchID
</cfquery>

<cfquery name="getconclusion" datasource="#ds#">
	select 
	inquiries_conclusion.*, 
	inquiries_conclusion_options_lookup.conclusionOptionID, 
	inquiries_conclusion_options.conclusionOption, 
	admin_users.firstname, 
	admin_users.lastname 
	from inquiries_conclusion 
	left join inquiries_conclusion_options_lookup on inquiries_conclusion.conclusionID = inquiries_conclusion_options_lookup.conclusionID 
	left join inquiries_conclusion_options on inquiries_conclusion_options_lookup.conclusionOptionID = inquiries_conclusion_options.conclusionOptionID
	left join admin_users on inquiries_conclusion.creatorid = admin_users.adminuserid
	where inquiries_conclusion.inquiryid = #i#
	order by inquiries_conclusion.conclusionID, conclusionOption
</cfquery>

<cfquery name="getresolution" datasource="#ds#">
	select 
	inquiries_resolution.*, 
	inquiries_resolution_options_lookup.resolutionOptionID, 
	inquiries_resolution_options.resolutionOption, 
	admin_users.firstname, 
	admin_users.lastname 
	from inquiries_resolution 
	left join inquiries_resolution_options_lookup on inquiries_resolution.resolutionID = inquiries_resolution_options_lookup.resolutionID 
	left join inquiries_resolution_options on inquiries_resolution_options_lookup.resolutionOptionID = inquiries_resolution_options.resolutionOptionID
	left join admin_users on inquiries_resolution.creatorid = admin_users.adminuserid
	where inquiries_resolution.inquiryid = #i#
	order by inquiries_resolution.resolutionID, resolutionOption
</cfquery>

<cfquery name="getContactAttempts" datasource="#ds#">
	select * 
	from inquiries_contactAttempts
	where inquiryid = #i#
	order by inquiries_contactAttempts.contactAttemptID asc
</cfquery>
<cfset contactAttemptIDlist = valuelist(getcontactattempts.contactAttemptID)>

<div align="left">
<table width="550" border="0" cellpadding="5" cellspacing="0">
	<cfoutput>
	<tr>
		<td><b>Inquiry #getInq.inquiryNumber#</b></td>
	</tr>
	</cfoutput>
	<tr><td>&nbsp;</td></tr>
	<tr>
		<td>
		<b>Attachments:</b> (you'll need to view and print separately) <br />
		<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<cfloop query="getinq">
			<cfoutput>
			<tr>
				<td>#clientFile#<br /></td>
			</tr>
			</cfoutput>
			</cfloop>
		</table>
		</td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	<tr>
		<td><b>Status History:</b></td>
	</tr>
	<tr>
		<td style="padding:0px">
		<table border="0" cellspacing="0" cellpadding="3">
			<cfoutput query="getStatusHist">
			<tr>
				<td>
				#status#
				<cfswitch expression="#statusid#">
					<cfcase value="1"> by </cfcase>
					<cfcase value="3"> from #fromDeptName# to #toDeptName# by </cfcase>
					<cfcase value="4"> by </cfcase>
					<cfdefaultcase> by </cfdefaultcase>
				</cfswitch>
				<cfif adminuserid is 0>
					System 
				<cfelse>
					#firstname# #lastname# 
				</cfif>
				at #dateformat(dateTimeUpdated, 'mm/dd/yyyy')# #timeformat(dateTimeUpdated, 'hh:mm tt')#				</td>
			</tr>
			<cfif statusid is 3 and forwardedNote is not "">
			<tr>
				<td style="font-style:italic">Forwarding comments: #replace(forwardedNote, "#chr(13)#", "<br />", "all")#</td>
			</tr>
			</cfif>
			<cfif statusid is 5>
			<tr>
				<td style="font-style:italic">Follow up scheduled for #dateformat(followUpDate, 'mm/dd/yyyy')#</td>
			</tr>
			<tr>
				<td style="font-style:italic">Follow up notes: #replace(followUpNote, "#chr(13)#", "<br />", "all")#</td>
			</tr>
			</cfif>
			</cfoutput>
		</table>
		</td>
	</tr>
	<cfoutput query="getInq" group="inquiryID">
	<tr>
		<td><b>Details:</b></td>
	</tr>
	<tr>
		<td>Category: #category#</td>
	</tr>
	<tr>
		<td style="padding:0px">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td width="27%">Rec-Acct ##: #rec#-#acct#</td>
					<td width="73%">Subscriber: #subscriberName#</td>
				</tr>
				<tr>
					<td>Dealer ##: #dealerNumber# </td>
					<td>Dealer Name: #dealerName#</td>
				</tr>
				<cfif trim(inquiryIs) is not "">
				<tr>
					<td colspan="2"><b>Status: #inquiryIs#</b></td>
				</tr>
				</cfif>
			</table>
		</td>
	</tr>
	<tr>
		<td>Person Inquiring: #personInquiring# (#personInquiringIs#). </td>
	</tr>
	<tr>
		<td>Phone ##: #personInquiringPhone# </td>
	</tr>
	<tr>
		<td>Occurred on: #dateformat(dateTimeOccurred, 'mm/dd/yyyy')# at #timeformat(dateTimeOccurred, 'hh:mm tt')# #timeOccurredTimeZone# Time Zone </td>
	</tr>
	<tr>
		<td>Applicable Code(s): <cfif trim(codes) is "">None<cfelse>#codes#</cfif></td>
	</tr>
	<tr>
		<td>Applicable Zone(s): <cfif trim(codes) is "">None<cfelse>#zones#</cfif></td>
	</tr>
	
	<tr>
		<td><b>Inquiry:</b></td>
	</tr>
	<cfif recordingRequested is not 0>
		<tr>
			<td>#replace(inquiryText, "#chr(13)#", "<br />", "all")#</td>
		</tr>
		<tr>
			<td><b>Recording requested for the following reason :</b> #recordingReason#</td>
		</tr>
	<cfelse>
		<tr>
			<td>#replace(inquiryText, "#chr(13)#", "<br />", "all")#</td>
		</tr>
	</cfif>
	</cfoutput>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<cfif getResearch.recordcount gt 0>
		<cfoutput query="getResearch" group="researchID">
		<tr>
			<td>
			Location: #researchLocation#
			</td>
		</tr>
		<tr>
			<td>
			Category: #researchCategory#
			</td>
		</tr>
		<tr>
			<td class="nopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="3">
				<tr>
					<td width="70%"><b>Research added by #firstname# #lastname# on #dateformat(dateTimeCreated, 'mm/dd/yyyy')# at #timeformat(dateTimeCreated, 'hh:mm tt')#</b></td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td>#replace(researchText, "#chr(13)#", "<br />", "all")#</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		</cfoutput>
	</cfif>
	<cfif getconclusion.recordcount gt 0>
		<cfoutput query="getconclusion" group="conclusionID">
		<tr>
			<td class="nopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="3">
				<tr>
					<td width="70%"><b>Conclusion added by: #firstname# #lastname# on #dateformat(dateTimeCreated, 'mm/dd/yyyy')# at #timeformat(dateTimeCreated, 'hh:mm tt')#</b></td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td>#replace(conclusionText, "#chr(13)#", "<br />", "all")#</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		</cfoutput>
	</cfif>
	<cfif getresolution.recordcount gt 0>
		<cfoutput query="getresolution" group="resolutionID">
		<tr>
			<td class="nopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="3">
				<tr>
					<td width="70%"><b>Resolution added by: #firstname# #lastname# on #dateformat(dateTimeCreated, 'mm/dd/yyyy')# at #timeformat(dateTimeCreated, 'hh:mm tt')#</b></td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td>#replace(resolutionText, "#chr(13)#", "<br />", "all")#</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		</cfoutput>
	</cfif>
	<cfif getContactAttempts.recordcount gt 0>
		<tr>
			<td><b>Contact Attempts</b></td>
		</tr>	
		<cfif getContactAttempts.recordcount gt 0>
			<tr>
				<td style="padding:0px">
				<table border="0" cellspacing="0" cellpadding="3">
					<cfset dealerContacted = false>
					<cfoutput query="getcontactattempts">
					<tr>
						<td valign="top">#getcontactattempts.currentrow#.</td>
						<td valign="top">
						<cfswitch expression="#contactAttemptID#">
							<cfcase value="1">First attempt left message for call back <cfif leftMsgWith is "answering machine">on #leftMsgWith#<cfelse>with #leftMsgWithPerson#</cfif> on #dateformat(attemptDateTime, 'mm/dd/yyyy')# at #timeformat(attemptDateTime, 'hh:mm tt')#<br />Comments: #comments#</cfcase>
							<cfcase value="2">Second attempt left message for call back on #leftMsgWith# on #dateformat(attemptDateTime, 'mm/dd/yyyy')# at #timeformat(attemptDateTime, 'hh:mm tt')#<br />Comments: #comments#</cfcase>
							<cfcase value="3">
								<cfset dealerContacted = true>
								Spoke to #spokeTo# on #dateformat(attemptDateTime, 'mm/dd/yyyy')# at #timeformat(attemptDateTime, 'hh:mm tt')#<br />Result: #contactResult#<br />Comments: #comments#
							</cfcase>
							<cfcase value="4">
								<cfset dealerContacted = true>
								<!--- In House Inquiry - Employee Notified --->
								Recording posted to MPower
							</cfcase>
						</cfswitch>
						</td>
					</tr>
					</cfoutput>
					<cfif dealerContacted is false>
					<tr>
						<td colspan="2"><b>Dealer Not Contacted</b></td>
					</tr>
					</cfif>
				</table>
				</td>
			</tr>
		</cfif>
	</cfif>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<cfif getInq.salesFollowUp is 1 and getInq.salesFollowUpComments is not "">
	<tr>
		<td><b>Sales Follow-Up</b></td>
	</tr>
	<tr>
		<td>
		<cfoutput>
		#replace(getInq.salesFollowUpComments, "#chr(13)#", "<br />", "all")#
		</cfoutput>
		</td>
	</tr>
	</cfif>
</table>
</div>
