
<cfsetting showdebugoutput="yes">

<link rel="stylesheet" type="text/css" href="../../styles.css">

<script language="javascript" type="text/javascript">
function viewPrintWin(u) {
	window.open(u,"inqWin","width=600,height=500,scrollbars=1");
}
</script>

<cfquery name="getDepts" datasource="#ds#">
	select * 
	from Admin_Users_Departments
	order by department asc
</cfquery>

<cfparam name="sortby" default="dept">

<cfswitch expression="#sortby#">
	<cfcase value="inqNum"><cfset sort = "order by inquiries_main.inquiryNumber ASC"></cfcase>
	<cfcase value="dept"><cfset sort = "order by toDepartments.department ASC"></cfcase>
	<cfcase value="cat"><cfset sort = "order by inquiries_categories.category ASC"></cfcase>
</cfswitch>

<cfquery name="getInqs" datasource="#ds#">
	select 
	inquiries_main.*, 
	inquiries_categories.category, 
	inquiries_status.status, 
	inquiries_status_history.*, 
	toDepartments.department as toDeptName, 
	fromDepartments.department as fromDeptName, 
	statusUsers.firstname, 
	statusUsers.lastname, 
	inquiries_inquiryIsOptions.* 
	from inquiries_main 
	left join inquiries_categories on inquiries_main.categoryid = inquiries_categories.categoryid
	left join inquiries_status_history on inquiries_main.inquiryid = inquiries_status_history.inquiryid
	left join Admin_Users_Departments as toDepartments on inquiries_status_history.toDepartmentID = toDepartments.departmentid
	left join Admin_Users_Departments as fromDepartments on inquiries_status_history.fromDepartmentID = fromDepartments.departmentid
	left join admin_users as statusUsers on inquiries_status_history.adminuserid = statusUsers.adminuserid
	left join inquiries_inquiryIsOptions on inquiries_main.inquiryIsID = inquiries_inquiryIsOptions.inquiryIsID
	left join inquiries_status on inquiries_status_history.statusid = inquiries_status.statusid
	where 
	inquiries_status_history.isCurrentStatus = 1 and 
	inquiries_status_history.statusid not in (4,5) and 
	1=1
	#sort#
</cfquery>

<cfquery name="getOverdue" datasource="#ds#">
	select 
	inquiries_main.*, 
	inquiries_categories.category, 
	inquiries_status.status, 
	inquiries_status_history.*, 
	toDepartments.department as toDeptName, 
	fromDepartments.department as fromDeptName, 
	statusUsers.firstname, 
	statusUsers.lastname, 
	inquiries_inquiryIsOptions.* 
	from inquiries_main 
	inner join inquiries_categories on inquiries_main.categoryid = inquiries_categories.categoryid
	inner join inquiries_status_history on inquiries_main.inquiryid = inquiries_status_history.inquiryid
	left join Admin_Users_Departments as toDepartments on inquiries_status_history.toDepartmentID = toDepartments.departmentid
	left join Admin_Users_Departments as fromDepartments on inquiries_status_history.fromDepartmentID = fromDepartments.departmentid
	left join admin_users as statusUsers on inquiries_status_history.adminuserid = statusUsers.adminuserid
	left join inquiries_inquiryIsOptions on inquiries_main.inquiryIsID = inquiries_inquiryIsOptions.inquiryIsID
	left join inquiries_status on inquiries_status_history.statusid = inquiries_status.statusid
	where 
	inquiries_status_history.isCurrentStatus = 1 and 
	inquiries_status_history.statusid not in (4,5) and 
	(dateTimeClosed is null and dateTimeOverdue <= #createodbcdatetime(now())#)
	#sort#
</cfquery>

<cfset totalInqs = getInqs.recordcount>

<cfif totalInqs is not 0>
	<cfset totalOverdue = getOverdue.recordcount>
	<cfset totalPercentOverdue = (totalOverdue / totalInqs) * 100>
<cfelse>
	<cfset totalOverdue = 0>
	<cfset totalPercentOverdue = 0>
</cfif>

<div align="center">
<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="left"><a style="text-decoration:underline" href="index.cfm">Return to Reporting Menu</a></td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	<tr>
		<td>
		<table border="0" width="100%" cellspacing="0" cellpadding="5">
			<tr>
				<td class="nopadding">
					<table border="0" cellspacing="0" cellpadding="5">
						<cfoutput>
						<tr>
							<td>
							<a target="_blank" href="overdue_printall.cfm"><img src="../../images/printerfriendlyicon.gif" alt="Print" width="30" height="30" border="0" /></a>							</td>
							<td><a style="text-decoration:underline;" target="_blank" href="overdue_printall.cfm">Print this report</a></td>
							<td>(requires adobe acrobat reader. <a href="/download/adobe_reader.exe" style="text-decoration:underline;">click here to download</a> if you do not have it) </td>
						</tr>
						</cfoutput>
					</table>
				</td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td align="center"><b>Overdue Inquiry Report as of <cfoutput>#dateformat(now(),'mm/dd/yyyy')#</cfoutput></b></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td class="nopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td align="center"><b>Total Inquiries </b></td>
					<td align="center"><b>Total Overdue </b></td>
					<td align="center"><b>Total % Overdue </b></td>
				</tr>
				<cfoutput>
				<tr>
					<td align="center">#totalInqs#</td>
					<td align="center">#totalOverdue#</td>
					<td align="center">#numberformat(totalPercentOverdue)#%</td>
				</tr>
				</cfoutput>
			</table>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td class="nopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td align="left" valign="bottom"><b>Department</b></td>
					<td align="center" valign="bottom"><b>Total <br />
					Inquiries </b></td>
					<td align="center" valign="bottom"><b>Total <br />
					Overdue </b></td>
					<td align="center" valign="bottom"><b>% Overdue<br />
					in Dept. </b></td>
					<td align="center" valign="bottom"><b>% Overdue by Dept.<br />
					out of total inquiries </b></td>
					<td align="center" valign="bottom"><b>% inquiries by dept.<br />
					out of total inquiries</b></td>
				</tr>
				<cfloop query="getDepts">
					<cfset deptTotal = 0>
					<cfset deptOverdue = 0>
					<cfset percentOverdueInDepartment = 0>
					<cfset percentOverdueByDeparment = 0>
					<cfset percentInqsByDepartment = 0>
					
					<cfquery name="getDeptTotal" dbtype="query">
						select * 
						from getInqs
						where toDepartmentID = #getDepts.departmentid#
					</cfquery>
					<cfset deptTotal = getDeptTotal.recordcount>
					
					<cfif deptTotal gt 0>
						<cfquery name="getDeptOverdue" dbtype="query">
							select * 
							from getOverdue 
							where toDepartmentID = #getDepts.departmentid#
						</cfquery>
						<cfset deptOverdue = getDeptOverdue.recordcount>
						
						<cfset percentOverdueInDepartment = (deptOverdue / deptTotal) * 100>
						<cfset percentOverdueByDeparment = (deptOverdue / totalInqs) * 100>
						<cfset percentInqsByDepartment = (deptTotal / totalInqs) * 100>
					<cfelse>
						<cfset deptOverdue = 0>
						<cfset percentOverdueInDepartment = 0>
						<cfset percentOverdueByDeparment = 0>
						<cfset percentInqsByDepartment = 0>
					</cfif>
					<cfoutput>
					<tr>
						<td>#department#</td>
						<td align="center">#deptTotal#</td>
						<td align="center">#deptOverdue#</td>
						<td align="center">#numberformat(percentOverdueInDepartment)#%</td>
						<td align="center">#numberformat(percentOverdueByDeparment)#%</td>
						<td align="center">#numberformat(percentInqsByDepartment)#%</td>
					</tr>
					</cfoutput>
				</cfloop>				
			</table>
		</td>
	</tr>
</table>
<br>
<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td>&nbsp;</td>
		<td align="center"><b>Inquiry # </b></td>
		<td align="center"><b>Dept</b></td>
		<td align="center"><b>Created</b></td>
		<td align="center"><b>Closed</b></td>
		<td align="center"><b>Dealer # </b></td>
		<td align="center"><b>Account # </b></td>
		<td align="center"><b>Category</b></td>
		<td align="left"><b>Status</b></td>
	</tr>
	<cfoutput query="getOverdue">
	<tr>
		<td>#getoverdue.currentrow#.</td>
		<td align="center"><a href="javascript:viewPrintWin('../viewInquiry.cfm?i=#inquiryid#');" style="text-decoration:underline;">#inquiryNumber#</a></td>
		<td align="center">#toDeptName#</td>
		<td align="center">#dateformat(dateTimeCreated,'mm/dd/yyyy')#</td>
		<td align="center">#dateformat(dateTimeClosed,'mm/dd/yyyy')#</td>
		<td align="center">#dealerNumber#</td>
		<td align="center">#rec#&ndash;#acct#</td>
		<td align="center">#category#</td>
		<td align="left">
		#status#
		<cfswitch expression="#statusid#">
			<cfcase value="1"> by </cfcase>
			<cfcase value="3"> from #fromDeptName# to #toDeptName# by </cfcase>
			<cfcase value="4"> by </cfcase>
			<cfdefaultcase> by </cfdefaultcase>
		</cfswitch>
		#firstname# #lastname# at #dateformat(dateTimeUpdated, 'mm/dd/yyyy')# #timeformat(dateTimeUpdated, 'hh:mm tt')#
		</td>
	</tr>
	</cfoutput>
</table>
</div>
