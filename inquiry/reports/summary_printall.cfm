<cfdocument format="pdf" orientation="portrait" scale="75">

<cfquery name="getDepts" datasource="#ds#">
	select * 
	from Admin_Users_Departments
	order by department asc
</cfquery>

<cfset startDate = createodbcdate(sd)>
<cfset endDate = createodbcdate(dateadd("d",1,ed))>

<cfquery name="getInqs" datasource="#ds#">
	select 
	inquiries_main.*, 
	inquiries_categories.category, 
	inquiries_status_history.*, 
	Admin_Users_Departments.*, 
	inquiries_inquiryIsOptions.* 
	from inquiries_main 
	inner join inquiries_categories on inquiries_main.categoryid = inquiries_categories.categoryid
	inner join inquiries_status_history on inquiries_main.inquiryid = inquiries_status_history.inquiryid
	inner join Admin_Users_Departments on inquiries_main.inquiryIsDepartmentID = Admin_Users_Departments.departmentid

	left join inquiries_inquiryIsOptions on inquiries_main.inquiryIsID = inquiries_inquiryIsOptions.inquiryIsID
	where 
	<cfif did is not 0>inquiries_main.inquiryIsDepartmentID = #did# and </cfif>
	inquiries_status_history.isCurrentStatus = 1 and 
	inquiries_status_history.statusid IN (4,5) and 
	(inquiries_main.dateTimeClosed > #startDate# and inquiries_main.dateTimeClosed < #endDate#)
	order by inquiries_main.inquiryNumber ASC
</cfquery>

<cfset totalInqs = getInqs.recordcount>

<cfquery name="getFounded" dbtype="query">
	select * from getInqs
	where inquiryIsID IN (1,3)
</cfquery>
	
<cfif totalInqs is not 0>
	<cfset totalFounded = getFounded.recordcount>
	<cfset totalPercentFounded = (totalFounded / totalInqs) * 100>
<cfelse>
	<cfset totalFounded = 0>
	<cfset totalPercentFounded = 0>
</cfif>

<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center"><b>Closed Inquiry Report Summary</b></td>
	</tr>
	<cfoutput>
	<tr>
		<td align="center"><b>From #dateformat(sd,'mm/dd/yyyy')# to #dateformat(ed,'mm/dd/yyyy')#</b></td>
	</tr>
	</cfoutput>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td class="nopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td align="center"><b>Total Inquiries </b></td>
					<td align="center"><b>Total Founded </b></td>
					<td align="center"><b>Total % Founded </b></td>
				</tr>
				<cfoutput>
				<tr>
					<td align="center">#totalInqs#</td>
					<td align="center">#totalFounded#</td>
					<td align="center">#numberformat(totalPercentFounded)#%</td>
				</tr>
				</cfoutput>
			</table>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td class="nopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td align="left" valign="bottom"><b>Department</b></td>
					<td align="center" valign="bottom"><b>Total <br />Inquiries </b></td>
					<td align="center" valign="bottom"><b>Total <br />Founded </b></td>
					<td align="center" valign="bottom"><b>% Founded<br />in Dept. </b></td>
					<td align="center" valign="bottom"><b>% Founded by Dept.<br />out of total founded </b></td>
					<td align="center" valign="bottom"><b>% inquiries by dept.<br />out of total inquiries </b></td>
				</tr>
				<cfloop query="getDepts">
					<cfquery name="getDeptTotal" dbtype="query">
						select * 
						from getInqs
						where inquiryIsDepartmentID = #getDepts.departmentid#
					</cfquery>
					<cfset deptTotal = getDeptTotal.recordcount>
					
					<cfif deptTotal gt 0>
						<cfquery name="getDeptFounded" dbtype="query">
							select * 
							from getFounded 
							where inquiryIsDepartmentID = #getDepts.departmentid#
						</cfquery>
						<cfset deptFounded = getDeptFounded.recordcount>
						
						<cfset percentFoundedInDepartment = (deptFounded / deptTotal) * 100>
						<cfset percentFoundedByDeparment = (deptFounded / totalInqs) * 100>
						<cfset percentInqsByDepartment = (deptTotal / totalInqs) * 100>
					<cfelse>
						<cfset deptFounded = 0>
						<cfset percentFoundedInDepartment = 0>
						<cfset percentFoundedByDeparment = 0>
						<cfset percentInqsByDepartment = 0>
					</cfif>
					<cfoutput>
					<tr>
						<td>#department#</td>
						<td align="center">#deptTotal#</td>
						<td align="center">#deptFounded#</td>
						<td align="center">#numberformat(percentFoundedInDepartment)#%</td>
						<td align="center">#numberformat(percentFoundedByDeparment)#%</td>
						<td align="center">#numberformat(percentInqsByDepartment)#%</td>
					</tr>
					</cfoutput>
				</cfloop>				
			</table>
		</td>
	</tr>
</table>

<cfdocumentitem type="pagebreak"></cfdocumentitem>

<cfloop query="getInqs">
	<cfset i = getInqs.inquiryid>
	<cf_printInq i="#i#" datasource="#ds#">
	<cfdocumentitem type="footer"><cfoutput>Page #cfdocument.currentpagenumber# of #cfdocument.totalpagecount#</cfoutput></cfdocumentitem>
	<cfdocumentitem type="pagebreak"></cfdocumentitem>
</cfloop>

</cfdocument>