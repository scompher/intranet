<cfdocument format="pdf" orientation="portrait" scale="75">

<cfquery name="getDepts" datasource="#ds#">
	select * 
	from Admin_Users_Departments
	order by department asc
</cfquery>

<cfparam name="sortby" default="dept">

<cfswitch expression="#sortby#">
	<cfcase value="inqNum"><cfset sort = "order by inquiries_main.inquiryNumber ASC"></cfcase>
	<cfcase value="dept"><cfset sort = "order by toDepartments.department ASC"></cfcase>
	<cfcase value="cat"><cfset sort = "order by inquiries_categories.category ASC"></cfcase>
</cfswitch>

<cfquery name="getInqs" datasource="#ds#">
	select 
	inquiries_main.*, 
	inquiries_categories.category, 
	inquiries_status.status, 
	inquiries_status_history.*, 
	toDepartments.department as toDeptName, 
	fromDepartments.department as fromDeptName, 
	statusUsers.firstname, 
	statusUsers.lastname, 
	inquiries_inquiryIsOptions.* 
	from inquiries_main 
	left join inquiries_categories on inquiries_main.categoryid = inquiries_categories.categoryid
	left join inquiries_status_history on inquiries_main.inquiryid = inquiries_status_history.inquiryid
	left join Admin_Users_Departments as toDepartments on inquiries_status_history.toDepartmentID = toDepartments.departmentid
	left join Admin_Users_Departments as fromDepartments on inquiries_status_history.fromDepartmentID = fromDepartments.departmentid
	left join admin_users as statusUsers on inquiries_status_history.adminuserid = statusUsers.adminuserid
	left join inquiries_inquiryIsOptions on inquiries_main.inquiryIsID = inquiries_inquiryIsOptions.inquiryIsID
	left join inquiries_status on inquiries_status_history.statusid = inquiries_status.statusid
	where 
	inquiries_status_history.isCurrentStatus = 1 and 
	inquiries_status_history.statusid not in (4,5) and 
	1=1
</cfquery>

<cfquery name="getOverdue" datasource="#ds#">
	select 
	inquiries_main.*, 
	inquiries_categories.category, 
	inquiries_status.status, 
	inquiries_status_history.*, 
	toDepartments.department as toDeptName, 
	fromDepartments.department as fromDeptName, 
	statusUsers.firstname, 
	statusUsers.lastname, 
	inquiries_inquiryIsOptions.* 
	from inquiries_main 
	inner join inquiries_categories on inquiries_main.categoryid = inquiries_categories.categoryid
	inner join inquiries_status_history on inquiries_main.inquiryid = inquiries_status_history.inquiryid
	left join Admin_Users_Departments as toDepartments on inquiries_status_history.toDepartmentID = toDepartments.departmentid
	left join Admin_Users_Departments as fromDepartments on inquiries_status_history.fromDepartmentID = fromDepartments.departmentid
	left join admin_users as statusUsers on inquiries_status_history.adminuserid = statusUsers.adminuserid
	left join inquiries_inquiryIsOptions on inquiries_main.inquiryIsID = inquiries_inquiryIsOptions.inquiryIsID
	left join inquiries_status on inquiries_status_history.statusid = inquiries_status.statusid
	where 
	inquiries_status_history.isCurrentStatus = 1 and 
	inquiries_status_history.statusid not in (4,5) and 
	(dateTimeClosed is null and dateTimeOverdue <= #createodbcdatetime(now())#)
	order by inquirynumber asc
</cfquery>

<cfset totalInqs = getInqs.recordcount>

<cfif totalInqs is not 0>
	<cfset totalOverdue = getOverdue.recordcount>
	<cfset totalPercentOverdue = (totalOverdue / totalInqs) * 100>
<cfelse>
	<cfset totalOverdue = 0>
	<cfset totalPercentOverdue = 0>
</cfif>

<div align="center">
<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center"><b>Overdue Inquiry Report as of <cfoutput>#dateformat(now(),'mm/dd/yyyy')#</cfoutput></b></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td class="nopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td align="center"><b>Total Inquiries </b></td>
					<td align="center"><b>Total Overdue </b></td>
					<td align="center"><b>Total % Overdue </b></td>
				</tr>
				<cfoutput>
				<tr>
					<td align="center">#totalInqs#</td>
					<td align="center">#totalOverdue#</td>
					<td align="center">#numberformat(totalPercentOverdue)#%</td>
				</tr>
				</cfoutput>
			</table>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td class="nopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td align="left" valign="bottom"><b>Department</b></td>
					<td align="center" valign="bottom"><b>Total <br />
					Inquiries </b></td>
					<td align="center" valign="bottom"><b>Total <br />
					Overdue </b></td>
					<td align="center" valign="bottom"><b>% Overdue<br />
					in Dept. </b></td>
					<td align="center" valign="bottom"><b>% Overdue by Dept.<br />
					out of total inquiries </b></td>
					<td align="center" valign="bottom"><b>% inquiries by dept.<br />
					out of total inquiries</b></td>
				</tr>
				<cfloop query="getDepts">
					<cfset deptTotal = 0>
					<cfset deptOverdue = 0>
					<cfset percentOverdueInDepartment = 0>
					<cfset percentOverdueByDeparment = 0>
					<cfset percentInqsByDepartment = 0>
					
					<cfquery name="getDeptTotal" dbtype="query">
						select * 
						from getInqs
						where toDepartmentID = #getDepts.departmentid#
					</cfquery>
					<cfset deptTotal = getDeptTotal.recordcount>
					
					<cfif deptTotal gt 0>
						<cfquery name="getDeptOverdue" dbtype="query">
							select * 
							from getOverdue 
							where toDepartmentID = #getDepts.departmentid#
						</cfquery>
						<cfset deptOverdue = getDeptOverdue.recordcount>
						
						<cfset percentOverdueInDepartment = (deptOverdue / deptTotal) * 100>
						<cfset percentOverdueByDeparment = (deptOverdue / totalInqs) * 100>
						<cfset percentInqsByDepartment = (deptTotal / totalInqs) * 100>
					<cfelse>
						<cfset deptOverdue = 0>
						<cfset percentOverdueInDepartment = 0>
						<cfset percentOverdueByDeparment = 0>
						<cfset percentInqsByDepartment = 0>
					</cfif>
					<cfoutput>
					<tr>
						<td>#department#</td>
						<td align="center">#deptTotal#</td>
						<td align="center">#deptOverdue#</td>
						<td align="center">#numberformat(percentOverdueInDepartment)#%</td>
						<td align="center">#numberformat(percentOverdueByDeparment)#%</td>
						<td align="center">#numberformat(percentInqsByDepartment)#%</td>
					</tr>
					</cfoutput>
				</cfloop>				
			</table>
		</td>
	</tr>
</table>
<cfdocumentitem type="pagebreak"></cfdocumentitem>

<cfloop query="getOverdue">
	<cfset i = getOverdue.inquiryid>
	<cf_printInq i="#i#" datasource="#ds#">
	<cfdocumentitem type="footer"><cfoutput>Page #cfdocument.currentpagenumber# of #cfdocument.totalpagecount#</cfoutput></cfdocumentitem>
	<cfdocumentitem type="pagebreak"></cfdocumentitem>
</cfloop>
</cfdocument>