
<cfsetting showdebugoutput="yes">

<cfparam name="sd" default="#createodbcdate(now())#">
<cfparam name="ed" default="#createodbcdate(now())#">

<link rel="stylesheet" type="text/css" href="../../styles.css">

<script language="javascript" type="text/javascript">
function viewPrintWin(u) {
	window.open(u,"inqWin","width=600,height=500,scrollbars=1");
}
</script>

<cfquery name="getCats" datasource="#ds#">
	select * 
	from inquiries_categories
	order by category asc
</cfquery>

<cfset startDate = createodbcdate(sd)>
<cfset endDate = createodbcdate(dateadd("d",1,ed))>

<cfquery name="getInqs" datasource="#ds#">
	select 
	inquiries_main.*, 
	inquiries_status.status, 
	inquiries_status_history.*, 
	inquiries_categories.*, 
	statusUsers.firstname, 
	statusUsers.lastname, 
	inquiries_inquiryIsOptions.* 
	from inquiries_main 
	left join inquiries_status_history on inquiries_main.inquiryid = inquiries_status_history.inquiryid
	left join inquiries_categories on inquiries_main.categoryid = inquiries_categories.categoryid
	left join admin_users as statusUsers on inquiries_status_history.adminuserid = statusUsers.adminuserid
	left join inquiries_inquiryIsOptions on inquiries_main.inquiryIsID = inquiries_inquiryIsOptions.inquiryIsID
	left join inquiries_status on inquiries_status_history.statusid = inquiries_status.statusid
	where 
	(inquiries_main.dateTimeCreated > #startDate# and inquiries_main.dateTimeCreated < #endDate#) and 
	inquiries_status_history.isCurrentStatus = 1  
	order by inquiries_main.inquiryNumber ASC
</cfquery>

<cfset totalInqs = getInqs.recordcount>

<div align="center">
<table width="750" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="left"><a style="text-decoration:underline" href="category.cfm">Return to Date Selection</a></td>
	</tr>
	<tr>
		<td align="center"><b>Inquiry Report by Category</b></td>
	</tr>
	<cfoutput>
	<tr>
		<td align="center"><b>From #dateformat(sd,'mm/dd/yyyy')# to #dateformat(ed,'mm/dd/yyyy')#</b></td>
	</tr>
	</cfoutput>
	<tr><td>&nbsp;</td></tr>
	<tr>
		<td class="nopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td align="center"><b>Total Inquiries </b></td>
				</tr>
				<cfoutput>
				<tr>
					<td align="center">#totalInqs#</td>
				</tr>
				</cfoutput>
			</table>
		</td>
	</tr>
	
	<tr>
		<td class="nopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td align="left" valign="bottom"><b>Category</b></td>
					<td align="center" valign="bottom"><b># of inquiries per category</b></td>
					<td align="center" valign="bottom"><b>% inquiries by category<br />
					out of total inquiries</b></td>
				</tr>
				<cfloop query="getCats">
					<cfset catTotal = 0>
					<cfquery name="getInqsByCategory" dbtype="query">
						select * from getInqs
						where categoryid = #getCats.categoryid#
					</cfquery>
					
					<cfset catTotal = getInqsByCategory.recordcount>
					<cfset percentInqsByCategory = 0>
					
					<cfif catTotal gt 0>
						<cfset percentInqsByCategory = (catTotal / totalInqs) * 100>
					<cfelse>
						<cfset percentInqsByCategory = 0>
					</cfif>
					<cfoutput>
					<tr onmouseover="this.style.backgroundColor='##EEEEEE';" onmouseout="this.style.backgroundColor='##FFFFFF';">
						<td>#category#</td>
						<td align="center">#catTotal#</td>
						<td align="center">#numberformat(percentInqsByCategory)#%</td>
					</tr>
					</cfoutput>
				</cfloop>
			</table>
		</td>
	</tr>
</table>
</div>
