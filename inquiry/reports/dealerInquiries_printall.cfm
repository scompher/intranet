<cfsetting showdebugoutput="no">

<cfparam name="dn" default="">
<cfparam name="sd" default="#now()#">
<cfparam name="ed" default="#now()#">

<cfset sd = createodbcdate(sd)>
<cfset ed = createodbcdate(ed)>
<cfset endDate = dateadd("d",1,ed)>
<cfset ed = endDate>

<cfquery name="getInqs" datasource="#ds#">
	select 
	inquiries_main.inquiryid 
	from inquiries_main
	inner join inquiries_status_history on inquiries_main.inquiryid = inquiries_status_history.inquiryid
	where 
	dealernumber = '#dn#' and 
	<!--- (dateTimeCreated > #dateadd("d",-1,sd)# and dateTimeCreated < #ed#) and  --->
	inquiries_status_history.isCurrentStatus = 1
</cfquery>

<cfdocument format="pdf" orientation="portrait" scale="75">
<cfloop query="getInqs">
	<cfset i = getInqs.inquiryid>
	<cf_printInq i="#i#" datasource="#ds#">
	<cfdocumentitem type="footer"><cfoutput>Page #cfdocument.currentpagenumber# of #cfdocument.totalpagecount#</cfoutput></cfdocumentitem>
	<cfdocumentitem type="pagebreak"></cfdocumentitem>
</cfloop>
</cfdocument>
