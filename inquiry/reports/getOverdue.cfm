<!--- 
10/28/2015 : PG : Changed overdue timeframes on all inquiries to 72 hours (from 24) per Sarah Brooks 
--->
<cfsetting showdebugoutput="yes">

<link rel="stylesheet" type="text/css" href="../../styles.css">

<script language="javascript" type="text/javascript">
function viewPrintWin(u) {
	window.open(u,"inqWin","width=600,height=500,scrollbars=1");
}
</script>

<cfquery name="getDepts" datasource="#ds#">
	select * 
	from Admin_Users_Departments
	order by department asc
</cfquery>

<cfparam name="includeClosed" default="0">
<cfparam name="sortby" default="dept">
<cfset startDate = createodbcdate(sd)>
<cfset endDate = createodbcdate(dateadd("d",1,ed))>
<cfset overdue = createodbcdate(dateadd("h",-72,now()))>

<cfswitch expression="#sortby#">
	<cfcase value="inqNum"><cfset sort = "order by inquiries_main.inquiryNumber ASC"></cfcase>
	<cfcase value="dept"><cfset sort = "order by toDepartments.department ASC"></cfcase>
	<cfcase value="cat"><cfset sort = "order by inquiries_categories.category ASC"></cfcase>
</cfswitch>

<cfquery name="getInqs" datasource="#ds#">
	select 
	inquiries_main.*, 
	inquiries_categories.category, 
	inquiries_status.status, 
	inquiries_status_history.*, 
	toDepartments.department as toDeptName, 
	fromDepartments.department as fromDeptName, 
	statusUsers.firstname, 
	statusUsers.lastname, 
	inquiries_inquiryIsOptions.* 
	from inquiries_main 
	left join inquiries_categories on inquiries_main.categoryid = inquiries_categories.categoryid
	left join inquiries_status_history on inquiries_main.inquiryid = inquiries_status_history.inquiryid
	left join Admin_Users_Departments as toDepartments on inquiries_status_history.toDepartmentID = toDepartments.departmentid
	left join Admin_Users_Departments as fromDepartments on inquiries_status_history.fromDepartmentID = fromDepartments.departmentid
	left join admin_users as statusUsers on inquiries_status_history.adminuserid = statusUsers.adminuserid
	left join inquiries_inquiryIsOptions on inquiries_main.inquiryIsID = inquiries_inquiryIsOptions.inquiryIsID
	left join inquiries_status on inquiries_status_history.statusid = inquiries_status.statusid
	where 
	(inquiries_main.dateTimeCreated > #startDate# and inquiries_main.dateTimeCreated < #endDate#) and 
	inquiries_status_history.isCurrentStatus = 1 and 
	<cfif includeClosed is 0>inquiries_status_history.statusid <> 4 and </cfif>
	1=1
	#sort#
</cfquery>

<cfquery name="getOverdue" datasource="#ds#">
	select 
	inquiries_main.*, 
	inquiries_categories.category, 
	inquiries_status.status, 
	inquiries_status_history.*, 
	toDepartments.department as toDeptName, 
	fromDepartments.department as fromDeptName, 
	statusUsers.firstname, 
	statusUsers.lastname, 
	inquiries_inquiryIsOptions.* 
	from inquiries_main 
	inner join inquiries_categories on inquiries_main.categoryid = inquiries_categories.categoryid
	inner join inquiries_status_history on inquiries_main.inquiryid = inquiries_status_history.inquiryid
	left join Admin_Users_Departments as toDepartments on inquiries_status_history.toDepartmentID = toDepartments.departmentid
	left join Admin_Users_Departments as fromDepartments on inquiries_status_history.fromDepartmentID = fromDepartments.departmentid
	left join admin_users as statusUsers on inquiries_status_history.adminuserid = statusUsers.adminuserid
	left join inquiries_inquiryIsOptions on inquiries_main.inquiryIsID = inquiries_inquiryIsOptions.inquiryIsID
	left join inquiries_status on inquiries_status_history.statusid = inquiries_status.statusid
	where 
	(inquiries_main.dateTimeCreated > #startDate# and inquiries_main.dateTimeCreated < #endDate#) and 
	inquiries_status_history.isCurrentStatus = 1 and 
	<cfif includeClosed is 0>inquiries_status_history.statusid <> 4 and </cfif>
	(
	(dateTimeClosed is not null and DATEDIFF(hh,dateTimeCreated,dateTimeClosed) > 72) or 
	(dateTimeClosed is null and DATEDIFF(hh,dateTimeCreated,getdate()) > 72)
	)
	#sort#
</cfquery>

<cfset totalInqs = getInqs.recordcount>

<cfif totalInqs is not 0>
	<cfset totalOverdue = getOverdue.recordcount>
	<cfset totalPercentOverdue = (totalOverdue / totalInqs) * 100>
<cfelse>
	<cfset totalOverdue = 0>
	<cfset totalPercentOverdue = 0>
</cfif>

<div align="center">
<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="left"><a style="text-decoration:underline" href="overdue.cfm">Return to Date Selection</a></td>
	</tr>
	<tr>
		<td align="center"><b>Overdue Inquiry Report Summary</b></td>
	</tr>
	<cfoutput>
	<tr>
		<td align="center"><b>From #dateformat(sd,'mm/dd/yyyy')# to #dateformat(ed,'mm/dd/yyyy')#</b></td>
	</tr>
	</cfoutput>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td class="nopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td align="center"><b>Total Inquiries </b></td>
					<td align="center"><b>Total Overdue </b></td>
					<td align="center"><b>Total % Overdue </b></td>
				</tr>
				<cfoutput>
				<tr>
					<td align="center">#totalInqs#</td>
					<td align="center">#totalOverdue#</td>
					<td align="center">#numberformat(totalPercentOverdue)#%</td>
				</tr>
				</cfoutput>
			</table>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td class="nopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td align="left" valign="bottom"><b>Department</b></td>
					<td align="center" valign="bottom"><b>Total <br />
					Inquiries </b></td>
					<td align="center" valign="bottom"><b>Total <br />
					Overdue </b></td>
					<td align="center" valign="bottom"><b>% Overdue<br />
					in Dept. </b></td>
					<td align="center" valign="bottom"><b>% Overdue by Dept.<br />
					out of total inquiries </b></td>
					<td align="center" valign="bottom"><b>% inquiries by dept.<br />
					out of total inquiries</b></td>
				</tr>
				<cfloop query="getDepts">
					<cfset deptTotal = 0>
					<cfset deptOverdue = 0>
					<cfset percentOverdueInDepartment = 0>
					<cfset percentOverdueByDeparment = 0>
					<cfset percentInqsByDepartment = 0>
					
					<cfquery name="getDeptTotal" dbtype="query">
						select * 
						from getInqs
						where toDepartmentID = #getDepts.departmentid#
					</cfquery>
					<cfset deptTotal = getDeptTotal.recordcount>
					
					<cfif deptTotal gt 0>
						<cfquery name="getDeptOverdue" dbtype="query">
							select * 
							from getOverdue 
							where toDepartmentID = #getDepts.departmentid#
						</cfquery>
						<cfset deptOverdue = getDeptOverdue.recordcount>
						
						<cfset percentOverdueInDepartment = (deptOverdue / deptTotal) * 100>
						<cfset percentOverdueByDeparment = (deptOverdue / totalInqs) * 100>
						<cfset percentInqsByDepartment = (deptTotal / totalInqs) * 100>
					<cfelse>
						<cfset deptOverdue = 0>
						<cfset percentOverdueInDepartment = 0>
						<cfset percentOverdueByDeparment = 0>
						<cfset percentInqsByDepartment = 0>
					</cfif>
					<cfoutput>
					<tr>
						<td>#department#</td>
						<td align="center">#deptTotal#</td>
						<td align="center">#deptOverdue#</td>
						<td align="center">#numberformat(percentOverdueInDepartment)#%</td>
						<td align="center">#numberformat(percentOverdueByDeparment)#%</td>
						<td align="center">#numberformat(percentInqsByDepartment)#%</td>
					</tr>
					</cfoutput>
				</cfloop>				
			</table>
		</td>
	</tr>
</table>
<br>
<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center"><b>Inquiry # </b></td>
		<td align="center"><b>Dept</b></td>
		<td align="center"><b>Created</b></td>
		<td align="center"><b>Closed</b></td>
		<td align="center"><b>Dealer # </b></td>
		<td align="center"><b>Account # </b></td>
		<td align="center"><b>Category</b></td>
		<td align="left"><b>Status</b></td>
	</tr>
	<cfoutput query="getOverdue">
	<tr>
		<td align="center"><a href="javascript:viewPrintWin('../viewInquiry.cfm?i=#inquiryid#');" style="text-decoration:underline;">#inquiryNumber#</a></td>
		<td align="center">#toDeptName#</td>
		<td align="center">#dateformat(dateTimeCreated,'mm/dd/yyyy')#</td>
		<td align="center">#dateformat(dateTimeClosed,'mm/dd/yyyy')#</td>
		<td align="center">#dealerNumber#</td>
		<td align="center">#rec#&ndash;#acct#</td>
		<td align="center">#category#</td>
		<td align="left">
		#status#
		<cfswitch expression="#statusid#">
			<cfcase value="1"> by </cfcase>
			<cfcase value="3"> from #fromDeptName# to #toDeptName# by </cfcase>
			<cfcase value="4"> by </cfcase>
			<cfdefaultcase> by </cfdefaultcase>
		</cfswitch>
		#firstname# #lastname# at #dateformat(dateTimeUpdated, 'mm/dd/yyyy')# #timeformat(dateTimeUpdated, 'hh:mm tt')#
		</td>
	</tr>
	</cfoutput>
</table>
</div>
