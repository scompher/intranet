
<link rel="stylesheet" type="text/css" href="../../styles.css">

<cfparam name="catid" default="0">
<cfparam name="sd" default="#now()#">
<cfparam name="ed" default="#now()#">

<cfset sd = createodbcdate(sd)>
<cfset ed = createodbcdate(ed)>

<script language="javascript" src="../cal2.js">
/*
Xin's Popup calendar script-  Xin Yang (http://www.yxscripts.com/)
Script featured on/available at http://www.dynamicdrive.com/
This notice must stay intact for use
*/
</script>
<script language="javascript" src="../cal_conf2.js"></script>

<script language="javascript" type="text/javascript">
function viewPrintWin(u) {
	window.open(u,"inqWin","width=600,height=500,scrollbars=1");
}
</script>

<cfquery name="getInqTotal" datasource="#ds#">
	select inquiries_main.inquiryid 
	from inquiries_main
	left join inquiries_status_history on inquiries_main.inquiryid = inquiries_status_history.inquiryid
	where 
	(dateTimeCreated > #dateadd("d",-1,sd)# and dateTimeCreated < #ed#) and 
	inquiries_status_history.isCurrentStatus = 1
</cfquery>

<cfquery name="getCats" datasource="#ds#">
	select * 
	from inquiries_categories
	order by category asc
</cfquery>

<cfquery name="getInqs" datasource="#ds#">
	select 
	inquiries_main.inquiryid, 
	inquiries_main.dealerNumber, 
	inquiries_main.inquirynumber, 
	inquiries_main.rec, 
	inquiries_main.acct, 
	inquiries_main.dateTimeCreated,
	inquiries_main.dateTimeClosed,
	Admin_Users_Departments.department, 
	inquiries_categories.category, 
	inquiries_inquiryIsOptions.inquiryIs as conclusion
	
	from inquiries_main 
	inner join inquiries_status_history on inquiries_main.inquiryid = inquiries_status_history.inquiryid
	inner join Admin_Users_Departments on inquiries_status_history.toDepartmentID = Admin_Users_Departments.departmentid
	inner join inquiries_categories on inquiries_main.categoryid = inquiries_categories.categoryid
	inner join inquiries_inquiryIsOptions on inquiries_main.inquiryIsID = inquiries_inquiryIsOptions.inquiryIsID
	
	where 
	<cfif catid is not 0>
		inquiries_main.categoryid = #catid# and 
	</cfif>
	(dateTimeCreated > #dateadd("d",-1,sd)# and dateTimeCreated < #ed#) and 
	inquiries_status_history.isCurrentStatus = 1
</cfquery>

<div align="center">
<table width="750" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center"><b>Inquiries by Category Report</b></td>
	</tr>
	<tr>
		<td align="center">
			<form method="post" action="inquiriesByCategory.cfm" name="mainform">
			<table border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td class="highlightbar"><b>Enter Inquiry Category and Date Generated Range:</b></td>
				</tr>
				<tr>
					<td class="greyrow" style="padding:0px">
						<table border="0" cellpadding="5" cellspacing="0" class="grey">
							<tr>
								<td>Inquiry Category: </td>
								<td>
								<select name="catid" class="normal">
										<option <cfif catid is 0>selected</cfif> value="0">All</option>
									<cfoutput query="getcats">
										<option <cfif getcats.categoryid is catid>selected</cfif> value="#categoryid#">#category#</option>
									</cfoutput>
								</select>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="greyrow" style="padding:0px">
					<cfoutput>
							<table border="0" cellpadding="5" cellspacing="0" class="grey">
								<tr>
									<td>Starting:</td>
									<td>
										<input name="sd" type="text" id="sd" style="width:75px" value="#dateformat(sd, 'mm/dd/yyyy')#" maxlength="10" />
									</td>
									<td> <a style="text-decoration:none;" href="javascript:showCal('startDate');"> <img src="/images/calicon.gif" alt="Select a Date" align="absmiddle" width="20" height="20" border="0" /> </a> </td>
									<td width="5">&nbsp;</td>
									<td>Ending:</td>
									<td>
										<input name="ed" type="text" id="ed" style="width:75px" value="#dateformat(ed, 'mm/dd/yyyy')#" maxlength="10" />
									</td>
									<td><a style="text-decoration:none;" href="javascript:showCal('endDate');"><img src="/images/calicon.gif" alt="Select a Date" align="absmiddle" width="20" height="20" border="0" /></a></td>
								</tr>
							</table>
					</cfoutput>
					</td>
				</tr>
				<tr>
					<td align="center" class="greyrowbottom">
						<input name="btnGetReport" type="submit" class="sidebar" value="Get Report" />
					</td>
				</tr>
			</table>
			</form>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<cfif isdefined("form.btnGetReport")>
	<tr>
		<td>
		There are a total of <cfoutput>#getInqTotal.recordcount#</cfoutput> inquiries between this date range.
		<br />
		<br />
		There are a total <cfoutput>#getInqs.recordcount#</cfoutput> inquiries between this date range for the selected category.
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<!--- 
	<tr>
		<td>
		<table border="0" width="100%" cellspacing="0" cellpadding="5">
			<tr>
				<td class="nopadding">
					<table border="0" cellspacing="0" cellpadding="5">
						<cfoutput>
						<tr>
							<td>
							<a target="_blank" href="summary_printall.cfm?sd=#sd#&ed=#ed#&did=#inquiryisdepartmentid#"><img src="../../images/printerfriendlyicon.gif" alt="Print" width="30" height="30" border="0" /></a></td>
							<td><a style="text-decoration:underline;" target="_blank" href="summary_printall.cfm?sd=#sd#&ed=#ed#&did=#inquiryIsDepartmentID#">Print this report</a></td>
							<td>(this report requires adobe acrobat reader. <a href="/download/adobe_reader.exe" style="text-decoration:underline;">click here to download</a> if you do not have it)</td>
						</tr>
						</cfoutput>
					</table>
				</td>
			</tr>
		</table>
		</td>
	</tr>
	 --->
	</cfif>
	<tr>
		<td style="padding:0px">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td><b>Inquiry # </b></td>
					<td><b>Dealer #</b></td>
					<td><b>Account # </b></td>
					<td><b>Dept.</b></td>
					<td><b>Generated</b></td>
					<td><b>Completed</b></td>
					<td><b>Category</b></td>
					<td><b>Conclusion</b></td>
				</tr>
				<cfoutput query="getInqs">
				<tr>
					<td><a href="javascript:viewPrintWin('../viewInquiry.cfm?i=#inquiryid#');" style="text-decoration:underline;">#inquiryNumber#</a></td>
					<td>#dealerNumber#</td>
					<td>#rec#&ndash;#acct#</td>
					<td>#department#</td>
					<td>#dateformat(dateTimeCreated,'mm/dd/yyyy')#</td>
					<td>#dateformat(dateTimeClosed,'mm/dd/yyyy')#</td>
					<td>#category#</td>
					<td>#conclusion#</td>
				</tr>
				</cfoutput>
			</table>
		</td></tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
</table>
<p class="normal"><a style="text-decoration:underline" href="index.cfm">Return to Reporting Menu</a> </p>
</div>
