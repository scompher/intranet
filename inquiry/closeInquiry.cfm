<!---
12/21/2012 : PG : Added code to publish recording in dealer MPower mailbox
--->



<link rel="stylesheet" type="text/css" href="../styles.css">

<script language="javascript" src="cal2.js">
/*
Xin's Popup calendar script-  Xin Yang (http://www.yxscripts.com/)
Script featured on/available at http://www.dynamicdrive.com/
This notice must stay intact for use
*/
</script>
<script language="javascript" src="cal_conf2.js"></script>

<cfparam name="form.btnCloseInquiry" default="0">
<cfparam name="form.inquiryIsID" default="0">
<cfparam name="form.inquiryIsDepartmentID" default="0">
<cfparam name="form.scheduleFollowUp" default="0">


<cfif isDefined("form.btnCancelClose")>
	<cflocation url="openInquiry.cfm?i=#form.inquiryid#">
</cfif>

<cfif form.btnCloseInquiry is 1>

	<cfquery name="checkSalesFollowUp" datasource="#ds#">
		select * from inquiries_main 
		where inquiryid = #form.inquiryid#
	</cfquery>
	<cfquery name="checkContactAttempt" datasource="#ds#">
		select * from inquiries_contactAttempts
		where inquiryid = #form.inquiryid# and contactResult <> '' 
	</cfquery>
	
	<!--- 12/21/2012 : PG : put recording in MPower mailbox if box is selected --->
	<cfif checkSalesFollowUp.recordingDownload is not 0>
		<!--- get account info --->
		<cfxml variable="xmlRequest">
		<cfoutput>
		<request>
			<command>intranetBasicInfo</command>
			<data>
				<accountNumber>#checkSalesFollowUp.rec#-#checkSalesFollowUp.acct#</accountNumber>
			</data>
		</request>
		</cfoutput>
		</cfxml>
		<cf_copalink command="#xmlRequest#">
		<cfset xResult = xmlparse(result)>
		
		<cfset accountName = xResult.response.data.basicInformation.name.xmlText>
		<cfset address = xResult.response.data.basicInformation.address.xmlText>
		<cfset phone = xResult.response.data.basicInformation.ctvPhone1.xmlText>

		<!--- Attach any .wav files as well --->
		<cfset errlist = "">
		<cfinclude template="convertAttachedRecordings.cfm">
		<cfif trim(errlist) is not "">
		<link rel="stylesheet" type="text/css" href="../styles.css">
		<div align="center">
		<table width="500" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td colspan="2" nowrap class="highlightbar" align="center"><b>An error has occurred</b></td>
			</tr>
			<tr>
				<td colspan="2" class="greyrow" align="center">
				<cfloop list="#errlist#" index="errormessage">
				<cfoutput>
				<b>
				#errormessage#
				</b>
				<br>
				</cfoutput>
				</cfloop>
				</td>
			</tr>
			<tr>
				<td colspan="2" class="greyrowbottom" align="center">
				<a style="text-decoration:underline" href="index.cfm">Click here to return to Inquiry Menu</a>
				</td>
			</tr>
		</table>	
		</div>
		<cfabort>
		</cfif>
		<!--- Attach any .wav files as well --->

		<cfquery name="getRecordings" datasource="#ds#">
			select * from inquiries_recordings where inquiryid = #form.inquiryid#
		</cfquery>
		<cfloop query="getRecordings">
			
			<cfset expirationDate = dateadd("d",30,now())>	
			
			<cfquery name="publishRecording" datasource="copalink">
				insert into dealer_recordings (dealerNumber, accountNumber, inquiryid, recordingDateTime, category, expirationDate, recordingName, dateTimePosted, accountName, address, phone) 
				values ('#checkSalesFollowUp.dealerNumber#', '#checkSalesFollowUp.rec#-#checkSalesFollowUp.acct#', #form.inquiryid#, #createodbcdatetime(getRecordings.recordingDateTime)#, '#getRecordings.condition#', #createodbcdatetime(expirationDate)#, '#recordingName#', getdate(), '#variables.accountName#', '#variables.address#', '#variables.phone#') 
			</cfquery>
		</cfloop>
	</cfif>
	

	
	<!--- if sales follow up selected, show sales follow up message and send e-mail notification. Otherwise continue to main screen --->
	<!--- <cfif checkSalesFollowUp.salesFollowUp is 1 or checkContactAttempt.contactResult is "Dealer Unsatisfied"> --->
	<cfif checkContactAttempt.contactResult is "Dealer Unsatisfied" and trim(checkSalesFollowUp.salesFollowUPComments) is "">

		<!--- 10/23/14 : PG : removed per heather sparks  --->
		<!--- get sales contact information 
		<cfquery name="GetContact" datasource="#ds#">
			select * from inquiries_contacts
			where departmentid = 7 
		</cfquery>
		--->
		<!--- display message 
		<link rel="stylesheet" type="text/css" href="../styles.css">
		<div align="center">
		<table width="500" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td colspan="2" nowrap class="highlightbar" align="center"><b>Sales Follow-up required please notify sales</b></td>
			</tr>
			<tr>
				<td colspan="2" class="greyrow" align="center">
				<cfoutput>
				<b>Sales follow-up is required</b>
				<br>
				<br>
				Please contact #getcontact.firstname# #getcontact.lastname# at extension #getcontact.extention# to notify that a sales follow-up is requested.
				</cfoutput>
				</td>
			</tr>
			<tr>
				<td colspan="2" class="greyrowbottom" align="center">
				<a style="text-decoration:underline" href="index.cfm">Click here to return to Inquiry Menu</a>
				</td>
			</tr>
		</table>	
		</div>
		<!--- send email  --->
		<cfquery name="Getinqnum" datasource="#ds#">
			select inquirynumber from inquiries_main
			where inquiryid = #form.inquiryid#
		</cfquery>
<cfmail from="system.info@copsmonitoring.com" to="ktierno@copsmonitoring.com" subject="Inquiry #getinqnum.inquirynumber# requires a sales follow-up">
Follow the link below to view the inquiry: 

#request.appurl#inquiry/openInquiry.cfm?i=#form.inquiryid#

</cfmail>
		--->
		
		<!--- display message --->
		<link rel="stylesheet" type="text/css" href="../styles.css">
		<div align="center">
		<table width="500" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td colspan="2" nowrap class="highlightbar" align="center"><b>Manager interaction required please notify your department manager</b></td>
			</tr>
			<tr>
				<td colspan="2" class="greyrow" align="center">
				<cfoutput>
				<b>Department manager interaction is required. Please contact your department manager to close this inquiry.</b>
				<br>
				</cfoutput>
				</td>
			</tr>
			<tr>
				<td colspan="2" class="greyrowbottom" align="center">
				<a style="text-decoration:underline" href="index.cfm">Click here to return to Inquiry Menu</a>
				</td>
			</tr>
		</table>	
		</div>
		<cfabort>
	<cfelse>
	
	<cfif form.scheduleFollowUp is 1>
		<cfset statusid = 5>
	<cfelse>
		<cfset statusid = 4>
	</cfif>

	<cfset today = createodbcdatetime(now())>
	<cftransaction>
		<cfquery name="updateStatus" datasource="#ds#">
			begin
				insert into inquiries_status_history (inquiryid, dateTimeUpdated, statusid, adminuserid, isCurrentStatus, fromDepartmentID, toDepartmentID)
				select inquiryid, dateTimeUpdated, statusid, #cookie.adminlogin#, isCurrentStatus, fromDepartmentID, toDepartmentID
				from inquiries_status_history
				where inquiries_status_history.inquiryid = #form.inquiryid# and isCurrentStatus = 1
			end
			begin
				update inquiries_status_history
				set	isCurrentStatus = 0 
				where inquiryid = #form.inquiryid#
			end
			begin
				update inquiries_status_history
				set dateTimeUpdated = #today#, statusid = #statusid#, isCurrentStatus = 1, isRead = 1
				<cfif form.scheduleFollowUp is 1>, followUpDate = '#form.followUpDate#', followUpNote = '#form.followUpNote#'</cfif>
				where inquiryid = #form.inquiryid# and statusHistoryID = (select max(statusHistoryID) from inquiries_status_history where inquiryid = #form.inquiryid#)
			end
			begin
				update inquiries_main
				set inquiryIsID = #form.inquiryIsID#, dateTimeClosed = #today#, inquiryIsDepartmentID = #form.inquiryIsDepartmentID#, reviewed = 0
				where inquiryid = #form.inquiryid#
			end
		</cfquery>
	</cftransaction>
	
		<cflocation url="index.cfm">
	</cfif>
</cfif>

<cfif isDefined("form.btnCancelRecordingPost")>
	<cflocation url="openInquiry.cfm?i=#i#">
</cfif>

<cfif not isDefined("form.btnConfirmRecordingPost")>
	<cfquery name="getInqInfo" datasource="#ds#">
		select * from inquiries_main 
		where inquiryid = #i# 
	</cfquery>
	<cfquery name="checkForRecordings" datasource="#ds#">
		select * from inquiries_recordings
		where inquiryid = #i# 
	</cfquery>
	<cfif getInqInfo.recordingDownload is 1 and checkForRecordings.recordcount gt 0>
		<cfinclude template="confirmRecordingPosting.cfm">
		<cfabort>
	</cfif>
</cfif>

<cfquery name="getInquiryIsOptions" datasource="#ds#">
	select * 
	from inquiries_inquiryIsOptions
	order by inquiryIsOrder
</cfquery>

<cfquery name="getDepartments" datasource="#ds#">
	select * 
	from admin_users_departments
	order by department asc
</cfquery>

<cfquery name="getCurrentStatus" datasource="#ds#">
	select * from inquiries_main 
	where inquiryid = #i# 
</cfquery>

<script language="JavaScript" type="text/javascript">
	function checkform(frm) {

		var today = new Date();
		// var chosenDate = new Date(frm.followUpDate.value);
		var futureDate = new Date();
		futureDate.setDate(today.getDate() + 14);

		if (frm.inquiryIsID.selectedIndex == 0) {alert('Please select the conclusion before closing out the inquiry.'); frm.inquiryIsID.focus(); return false;}
		if (frm.inquiryIsDepartmentID.selectedIndex == 0) {alert('Please select which department this inquiry conclusion pertains to.'); frm.inquiryIsDepartmentID.focus(); return false;}
		/*
		if (frm.scheduleFollowUp.checked) {
			if (frm.followUpDate.value == "") {alert('The follow up date is required.'); frm.followUpDate.focus(); return false;}
			if (frm.followUpNote.value == "") {alert('The follow up note is required.'); frm.followUpNote.focus(); return false;}
			if (chosenDate > futureDate) {alert('The follow up date cannot be more than 2 weeks into the future.'); frm.followUpDate.focus(); return false;}
		}
		*/
		frm.btnCloseInquiry.value = 1;
		frm.submit();
	}
</script>

<form method="post" action="closeInquiry.cfm" name="mainform">
<cfoutput>
<input type="hidden" name="btnCloseInquiry" value="0" />
<input type="hidden" name="inquiryid" value="#i#" />
</cfoutput>
<table width="500" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center" class="highlightbar">Close Inquiry</td>
	</tr>
	<tr>
		<td class="greyrow">&nbsp;</td>
	</tr>
	<tr>
		<td align="center" class="greyrow"><b>Are you sure you wish to close this inquiry?</b></td>
	</tr>
	<tr>
		<td class="greyrow">&nbsp;</td>
	</tr>
	<tr>
		<td class="greyrow" style="padding:0px" align="center">
			<table border="0" cellspacing="0" cellpadding="5" class="grey" width="390">
				<tr>
					<td width="133" nowrap="nowrap">This inquiry is: </td>
					<td width="237">
					<select name="inquiryIsID">
						<option value="0" selected="selected"></option>
						<cfoutput query="getInquiryIsOptions">
							<option <cfif getCurrentStatus.inquiryIsID is getInquiryIsOptions.inquiryIsID>selected</cfif> value="#inquiryIsID#">#inquiryIs#</option>
						</cfoutput>
					</select>
					</td>
				</tr>
				<tr>
					<td nowrap="nowrap">For which department:</td>
					<td>
					<select name="inquiryIsDepartmentID">
						<option value="0"></option>
						<cfoutput query="getDepartments">
							<option <cfif getCurrentStatus.inquiryIsDepartmentID is getDepartments.departmentID>selected</cfif> value="#getDepartments.departmentID#">#getDepartments.department#</option>
						</cfoutput>
					</select>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td align="center" class="greyrow">&nbsp;</td>
	</tr>
	<!--- 
	<tr>
		<td align="center" class="greyrow">
		<table width="390" border="0" cellspacing="0" cellpadding="5" class="grey">
		<tr>
			<td width="20">
				<input type="checkbox" name="scheduleFollowUp" value="1" /></td>
			<td width="350" colspan="2">I would like to schedule a follow up for this inquiry</td>
		</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td class="greyrow" style="padding:0px" align="center">
			<table width="390" border="0" cellpadding="5" cellspacing="0" class="grey">
				<tr>
					<td width="91" nowrap="nowrap">Follow up date:</td>
					<td width="75" align="left">
						<input name="followUpDate" type="text" style="width:75px" value="" maxlength="10" />
					</td>
					<td width="194" align="left"><a style="text-decoration:none;" href="javascript:showCal('FollowUpDate');"><img src="../images/calicon.gif" alt="Select a Date" align="absmiddle" width="20" height="20" border="0" /></a></td>
				</tr>
				<tr>
					<td colspan="3" align="left">NOTE: Follow up date cannot be more than 2 weeks from today </td>
				</tr>
				<tr>
					<td colspan="3" align="left">Follow up comments or notes:</td>
				</tr>
				<tr>
					<td align="left" colspan="3">
					<textarea name="followUpNote" rows="3" style="width:380px;"></textarea>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td class="greyrow">&nbsp;</td>
	</tr>
	 --->
	<tr>
		<td align="center" class="greyrow">
			<input type="button" onclick="checkform(this.form);" class="lightbutton" value="Close Inquiry">
			&nbsp;&nbsp;
			<input name="btnCancelClose" type="submit" class="lightbutton" value="Cancel Close">
		</td>
	</tr>
	<tr>
		<td class="greyrowbottom">&nbsp;</td>
	</tr>
</table>
</form>

