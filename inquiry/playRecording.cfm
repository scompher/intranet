<cfsetting showdebugoutput="no">

<link rel="stylesheet" type="text/css" href="../styles.css">

<cfparam name="url.r" default="">

<cfif findnocase("_",url.r) is not 0>
	<cfset recordingName = listgetat(url.r,listlen(url.r,"\"),"\")>
	<cfset fileLocation = "#copalinkUrl#/recordingmailbox/recordings/#listgetat(recordingName,2,'_')#/#recordingName#">
<cfelse>
	<cfset recordingName = listgetat(url.r,2,"-")>
	<cfset fileLocation = "#copalinkUrl#/recordingmailbox/recordings/#listgetat(url.r,1,'-')#/#recordingName#">
</cfif>

<div align="center">
<table border="0" cellspacing="0" cellpadding="5">
    <tr>
        <td class="highlightbar"><b>Recording Playback</b></td>
    </tr>
	<tr>
		<td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td>
						<audio controls>
							<cfoutput>
								<source src="#fileLocation#" type="audio/mpeg">
							</cfoutput>	
							<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=8,0,0,0" width="315" height="100" id="soundplayer" align="middle">
								<cfoutput>
									<param name="allowScriptAccess" value="sameDomain" />
									<param name="movie" value="soundplayer.swf?soundFile=#fileLocation#" />
									<param name="quality" value="high" />
									<param name="bgcolor" value="##F7F7F7" />
									<embed src="soundplayer.swf?soundFile=#fileLocation#" quality="high" bgcolor="##F7F7F7" width="315" height="100" name="soundplayer" align="middle" allowscriptaccess="sameDomain" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" />
								</cfoutput>
							</object>
						</audio>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</div>


