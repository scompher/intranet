<!--- 03/03/2017 SWC - added an email to the creator when the global is opened as part of the new ticketing system --->


<cfquery name="markCompleted" datasource="#ds#">
	update globalChangeRequest 
	set currentStatus = 'Opened', dateOpened = #createodbcdate(now())#, openedby = #cookie.adminlogin#
	where changeRequestID = #rid#
</cfquery>

<cfquery name="getCreatorID" datasource="#ds#">
	select createdBy from globalChangeRequest 
	where changeRequestID = #rid# 
</cfquery>

<cfquery name="getCreatorInfo" datasource="#ds#">
	select * from admin_users 
	where adminuserid = #getcreatorid.createdBy#
</cfquery>

<cfmail from="technologyrequest@copsmonitoring.com" to="#getCreatorInfo.email#" subject="Your technology request has been opened.">
Technology request ###rid# has been opened by #getsec.firstname# #getsec.lastname#.
</cfmail>


<cflocation url="#cgi.HTTP_REFERER#">

