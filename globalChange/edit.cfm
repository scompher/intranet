<link rel="stylesheet" type="text/css" href="/styles.css">

<cfinclude template="functions.cfm">

<cfparam name="form.status" default="Unread">
<cfparam name="form.dateSubmitted" default="#now()#">
<cfparam name="form.dateRequested" default="">
<cfparam name="form.customerOrDivision" default="">
<cfparam name="form.personRequestingChange" default="">
<cfparam name="form.personRequestingChangeIs" default="">
<cfparam name="form.changeDetails" default="">
<cfparam name="form.changeTypeID" default="0">
<cfparam name="form.changeApprovedBy" default="">
<cfparam name="form.dateCompleted" default="">
<cfparam name="form.changeCompletedBy" default="">
<cfparam name="err" default="">
<cfparam name="fieldErrors" default="">
<cfparam name="form.fileAttachments" default="">
<cfparam name="form.fileAttachment" default="">
<cfparam name="form.removeAttachmentValue" default="">

<cfif trim(form.fileAttachment) is not "">
	<cfset form.btnAttach = 1>
</cfif>

<cfif isdefined("form.btnAttach") and form.fileAttachment is not "">
	<cffile action="upload" filefield="fileAttachment" destination="#request.DirectPath#\globalChange\attachments\" nameconflict="makeunique">
	<cfset savedFile = file.ServerFile>
	<cfset uploadedFile = file.ClientFile>
	<cfset form.fileAttachments = listappend(form.fileAttachments, "#savedFile#:#uploadedFile#")>
</cfif>

<cfif form.removeAttachmentValue is not "">
	<cfset serverFile = listgetat(form.removeAttachmentValue,1,":")>
	<cfset clientFile = listgetat(form.removeAttachmentValue,2,":")>
	<cfif fileExists("#request.DirectPath#\globalChange\attachments\#serverFile#")>
		<cffile action="delete" file="#request.DirectPath#\globalChange\attachments\#serverFile#">
	</cfif>
	<cfquery name="updateAttachmentList" datasource="#ds#">
		delete from globalChangeRequest_attachments 
		where changeRequestID = #rid# and savedFile = '#serverFile#' and clientFile = '#clientFile#' 
	</cfquery>
	<cfset form.fileAttachments = listdeleteat(form.fileAttachments,listfindnocase(form.fileAttachments,form.removeAttachmentValue))>
</cfif>

<cfif isDefined("form.btnSubmitChangeRequest")>
	<!--- validate data --->
	<cfset err = validateForm(form)>

	<cfif trim(err) is "">
		<!--- save data --->
		<cfif updateRequest(form)>
			<cflocation url="index.cfm">		
		</cfif>
	</cfif>
</cfif>

<script type="text/javascript" language="javascript">
function showhide(layer_ref, state){ 
	if(document.all){ //IS IE 4 or 5 (or 6 beta) 
		eval("document.all." +layer_ref+ ".style.display = state"); 
	} 
	if (document.layers) { //IS NETSCAPE 4 or below 
		document.layers[layer_ref].display = state; 
	} 
	if (document.getElementById &&!document.all) { 
		hza = document.getElementById(layer_ref); 
		hza.style.display = state; 
	} 
}
function removeAttachment(serverFile, clientFile) {
	document.mainform.removeAttachmentValue.value = serverFile + ':' + clientFile;
	document.mainform.submit();
}
</script>

<script language="javascript" src="cal2.js">
/*
Xin's Popup calendar script-  Xin Yang (http://www.yxscripts.com/)
Script featured on/available at http://www.dynamicdrive.com/
This notice must stay intact for use
*/
</script>
<script language="javascript" src="cal_conf2.js"></script>

<cfif not isDefined("form.submitted")>
	<cfquery name="getRequest" datasource="#ds#">
		select 
			globalChangeRequest.*, 
			globalChangeRequest_attachments.attachmentid, 
			globalChangeRequest_attachments.savedFile, 
			globalChangeRequest_attachments.clientFile 
		from globalChangeRequest 
			left join globalChangeRequest_attachments on globalChangeRequest.changeRequestID = globalChangeRequest_attachments.changeRequestID 
		where globalChangeRequest.changeRequestID = #rid# 
	</cfquery>
	
	<cfif getRequest.recordcount gt 0>
		<cfset form.status = getRequest.currentStatus>
		<cfset form.dateSubmitted = dateformat(getRequest.dateSubmitted,'mm/dd/yyyy')>
		<cfset form.dateRequested = dateformat(getRequest.dateRequested,'mm/dd/yyyy')>
		<cfset form.customerOrDivision = getRequest.customerOrDivision>
		<cfset form.personRequestingChange = getRequest.personRequestingChange>
		<cfset form.personRequestingChangeIs = getRequest.personRequestingChangeIs>
		<cfset form.changeDetails = getRequest.changeDetails>
		<cfset form.changeTypeID = getRequest.changeTypeID>
		<cfset form.changeApprovedBy = getRequest.changeApprovedBy>
		<cfset form.dateCompleted = getRequest.dateCompleted>
		<cfset form.changeCompletedBy = getRequest.changeCompletedBy>
		<cfoutput query="getRequest" group="changeRequestID">
			<cfoutput>
				<cfset form.fileAttachments = listappend(form.fileAttachments, "#savedFile#:#clientFile#")>
			</cfoutput>
		</cfoutput>
	</cfif>
</cfif>

<cfquery name="getChangeTypes" datasource="#ds#">
	select * from globalChangeRequest_changeTypes
	order by orderID asc, changeType asc
</cfquery>

<div align="center">
<table width="600" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Global Change Request Form</b> </td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<form method="post" action="edit.cfm" enctype="multipart/form-data" name="mainform">
			<input type="hidden" name="submitted" value="1" />
			<input type="hidden" name="fileAttachments" value="<cfoutput>#form.fileAttachments#</cfoutput>">
			<input type="hidden" name="removeAttachmentValue" value="">
			<input type="hidden" name="rid" value="<cfoutput>#rid#</cfoutput>">
			<input type="hidden" name="status" value="<cfoutput>#form.status#</cfoutput>" />
				<cfif trim(err) is not "">
					<cfloop list="#err#" index="e">
						<cfset fieldErrors = listappend(fieldErrors,listgetat(e,1,":"))>
						<cfset m = listgetat(e,2,":")>
						<tr>
							<td style="color:#FF0000; font-weight:bold;"><cfoutput>#m#</cfoutput></td>
						</tr>
					</cfloop>
				</cfif>
				<tr>
					<td><b>Please fill in all fields completely </b></td>
				</tr>
				<tr>
					<td class="nopadding">
						<table width="100%" border="0" cellspacing="0" cellpadding="5">
							<tr>
								<td><b>Attach File(s): (optional)</b></td>
								<td style="padding:0px">
									<table border="0" cellspacing="0" cellpadding="5" class="grey">
										<tr>
											<td>
												<input type="file" name="fileAttachment">
											</td>
											<td>
												<input name="btnAttach" type="submit" class="sidebar" value="Attach File">
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td valign="top"><b>Current Attatchments:</b> </td>
								<td style="padding:0px">
								<cfif fileAttachments is not "">
									<table border="0" cellpadding="5" cellspacing="0" class="grey">
										<cfloop list="#fileAttachments#" index="theFile">
										<cfoutput>
										<cfset serverFile = listgetat(theFile,1,":")>
										<cfset fileName = listgetat(theFile,2,":")>
										<tr>
											<td><a target="_blank" href="/globalChange/attachments/#serverFile#" style="text-decoration:underline;">#fileName#</a></td>
											<td><a href="javascript:removeAttachment('#serverFile#','#fileName#');" style="text-decoration:underline;">[remove]</a></td>
										</tr>
										</cfoutput>
										</cfloop>
									</table>
								<cfelse>
									<table border="0" cellpadding="5" cellspacing="0" class="grey">
										<tr>
											<td colspan="2">None</td>
										</tr>
									</table>
								</cfif>
								&nbsp;	
								</td>
							</tr>
							<tr>
								<td <cfif listfind(fieldErrors,"customerOrDivision")>bgcolor="#FF7777"</cfif> width="32%" nowrap><b>Customer/Division:</b></td>
								<td <cfif listfind(fieldErrors,"customerOrDivision")>bgcolor="#FF7777"</cfif> width="68%">
									<input type="text" name="customerOrDivision" style="width:390px" value="<cfoutput>#customerOrDivision#</cfoutput>">
								</td>
							</tr>
							<tr>
								<td <cfif listfind(fieldErrors,"dateRequested")>bgcolor="#FF7777"</cfif> nowrap><b>Date Requested: </b></td>
								<td <cfif listfind(fieldErrors,"dateRequested")>bgcolor="#FF7777"</cfif> class="nopadding">
									<table border="0" cellpadding="5" cellspacing="0" class="grey">
										<tr>
											<td <cfif listfind(fieldErrors,"dateRequested")>bgcolor="#FF7777"</cfif> >
												<input name="dateRequested" type="text" style="width:75px" value="<cfoutput>#dateRequested#</cfoutput>" maxlength="10" />
											</td>
											<td <cfif listfind(fieldErrors,"dateRequested")>bgcolor="#FF7777"</cfif> ><a style="text-decoration:none;" href="javascript:showCal('DateRequested');"> <img src="../images/calicon.gif" alt="Select a Date" align="absmiddle" width="20" height="20" border="0" /> </a></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td <cfif listfind(fieldErrors,"personRequestingChange")>bgcolor="#FF7777"</cfif> nowrap><b>Person Requesting Change: </b></td>
								<td <cfif listfind(fieldErrors,"personRequestingChange")>bgcolor="#FF7777"</cfif> >
									<input type="text" name="personRequestingChange" style="width:390px" value="<cfoutput>#personRequestingChange#</cfoutput>">
								</td>
							</tr>
							<tr>
								<td <cfif listfind(fieldErrors,"personRequestingChangeIs")>bgcolor="#FF7777"</cfif> valign="top" nowrap>&nbsp;</td>
								<td <cfif listfind(fieldErrors,"personRequestingChangeIs")>bgcolor="#FF7777"</cfif> class="nopadding">
									<table border="0" cellspacing="0" cellpadding="5">
										<tr>
											<td <cfif listfind(fieldErrors,"personRequestingChangeIs")>bgcolor="#FF7777"</cfif> valign="middle">
												<input type="radio" <cfif form.personRequestingChangeIs is "Dealer">checked</cfif> name="personRequestingChangeIs" value="Dealer">
											</td>
											<td <cfif listfind(fieldErrors,"personRequestingChangeIs")>bgcolor="#FF7777"</cfif> valign="middle" style="padding-left:0px">
												Dealer											</td>
											<td <cfif listfind(fieldErrors,"personRequestingChangeIs")>bgcolor="#FF7777"</cfif> valign="middle">&nbsp;</td>
											<td <cfif listfind(fieldErrors,"personRequestingChangeIs")>bgcolor="#FF7777"</cfif>valign="middle">
												<input type="radio" <cfif personRequestingChangeIs is "In-House">checked</cfif> name="personRequestingChangeIs" value="In-House">
											</td>
											<td <cfif listfind(fieldErrors,"personRequestingChangeIs")>bgcolor="#FF7777"</cfif> valign="middle" style="padding-left:0px">
												In-House											</td>
										</tr>
									</table>
								</td>
							</tr>
							
							<tr>
								<td <cfif listfind(fieldErrors,"changeTypeID")>bgcolor="#FF7777"</cfif> valign="top" nowrap="nowrap"><b>Requested Change: </b></td>
								<td <cfif listfind(fieldErrors,"changeTypeID")>bgcolor="#FF7777"</cfif> >
									<select name="changeTypeID" style="width:390px">
										<option value="0"></option>
										<cfoutput query="getChangeTypes">
											<option <cfif form.changeTypeID is getChangeTypes.changeTypeID>selected</cfif> value="#getChangeTypes.changeTypeID#">#getChangeTypes.changeType#</option>
										</cfoutput>
									</select>
								</td>
							</tr>
							<tr>
								<td <cfif listfind(fieldErrors,"changeDetails")>bgcolor="#FF7777"</cfif> valign="top" nowrap><b>Change Details: </b></td>
								<td <cfif listfind(fieldErrors,"changeDetails")>bgcolor="#FF7777"</cfif> >
								<textarea name="changeDetails" style="width:390px" rows="5"><cfoutput>#changeDetails#</cfoutput></textarea>
								</td>
							</tr>
							<tr>
								<td <cfif listfind(fieldErrors,"changeApprovedBy")>bgcolor="#FF7777"</cfif> nowrap><b>Change Approved By: </b></td>
								<td <cfif listfind(fieldErrors,"changeApprovedBy")>bgcolor="#FF7777"</cfif> >
									<input type="text" name="changeApprovedBy" style="width:390px" value="<cfoutput>#changeApprovedBy#</cfoutput>">
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<input name="btnSubmitChangeRequest" type="submit" class="sidebar" value="Update Changes" style="width:200px;">
						<input type="button" class="sidebar" value="Cancel Changes" onClick="document.location = 'index.cfm';">
					</td>
				</tr>
			</form>
			</table>
		</td>
	</tr>
</table>
<br>
<a style="text-decoration:underline;" class="normal" href="/index.cfm">Return to Intranet</a> | 
<a style="text-decoration:underline;" class="normal" href="index.cfm">Return to Current Requests</a>
</div>

