
<!--- function: validate form --->
<cffunction name="validateForm" access="private" returntype="string">
	<cfargument name="form" type="struct" required="yes">
	
	<cfset e = "">
	
	<cfif trim(form.customerOrDivision) is "">
		<cfset e = listappend(e,"customerOrDivision:The customer/division is required.")>
	</cfif>
	<cfif trim(form.personRequestingChange) is "">
		<cfset e = listappend(e,"personRequestingChange:The person requesting the change is required.")>
	</cfif>
	<cfif trim(form.personRequestingChangeIs) is "">
		<cfset e = listappend(e,"personRequestingChangeIs:The person requesting the change type is required. (Dealer or In-House)")>
	</cfif>
	<cfif trim(form.changeDetails) is "">
		<cfset e = listappend(e,"changeDetails:The change details are required.")>
	</cfif>
	<cfif not isdate(form.dateRequested)>
		<cfset e = listappend(e,"dateRequested:The date requested is not a valid date.")>
	</cfif>
	<cfif trim(form.dateCompleted) is not "">
		<cfif not isdate(form.dateCompleted)>
			<cfset e = listappend(e,"dateCompleted:The date completed is not a valid date.")>
		</cfif>
	</cfif>
	<cfif trim(form.dateCompleted) is not "" and trim(form.changeCompletedBy) is "">
		<cfset e = listappend(e,"changeCompletedBy:You must enter who completed the change.")>
	</cfif>
	<cfif form.changeTypeID is 0>
		<cfset e = listappend(e,"changeTypeID:The requested change is required.")>
	</cfif>
	<cfif trim(form.changeApprovedBy) is "">
		<cfset e = listappend(e,"changeApprovedBy:The person approving the change is required.")>
	</cfif>
	
	<cfreturn e>
</cffunction>
<!--- function: validate form --->

<!--- function : save request data --->
<cffunction name="saveRequest" access="private" returntype="boolean">
	<cfargument name="form" type="struct" required="yes">
	
	<cfset saved = true>
	
	<cfquery name="saveChangeRequest" datasource="#ds#">
		declare @ID as int

		insert into globalChangeRequest (dateSubmitted, dateRequested, customerOrDivision, personRequestingChange, personRequestingChangeIs, changeDetails, changeTypeID, changeApprovedBy, currentStatus, createdBy) 
		values (#createOdbcDate(form.dateSubmitted)#, #createOdbcDate(form.dateRequested)#, '#form.customerOrDivision#', '#form.personRequestingChange#', '#form.personRequestingChangeIs#', '#form.changeDetails#', #form.changeTypeID#, '#form.changeApprovedBy#', '#form.status#', #cookie.adminLogin#) 

		set @ID = (select max(changeRequestID) from globalChangeRequest where createdBy = #cookie.adminLogin#)
	
		<cfif trim(form.fileAttachments) IS NOT "">
			<cfloop list="#form.fileAttachments#" index="theFile">
				<cfset savedFile = listgetat(theFile,1,":")>
				<cfset clientFile = listgetat(theFile,2,":")>
				if not exists (select * from globalChangeRequest_attachments where savedFile = '#savedFile#' and clientFile = '#clientFile#' and changeRequestID = @id)
				begin
					insert into globalChangeRequest_attachments (savedFile, clientFile, changeRequestID)
					values ('#savedFile#', '#clientFile#', @ID)
				end
			</cfloop>
		</cfif>

	</cfquery>
	
<!--- email IT to notify --->
<cfmail from="pgregory@copsmonitoring.com" to="technologyrequest@copsmonitoring.com" subject="New Technology Request Submission Notification">
A new global change request has been submitted.

Date Submitted.............#dateformat(form.dateSubmitted,'mm/dd/yyyy')#
Customer...................#customerOrDivision#
Person Requesting Change...#personRequestingChange# (#personRequestingChangeIs#)
Details:
#form.changeDetails#

</cfmail>
	
	<cfreturn saved>
</cffunction>
<!--- function : save request data --->

<!--- function : update request data --->
<cffunction name="updateRequest" access="private" returntype="boolean">
	<cfargument name="form" type="struct" required="yes">
	
	<cfset saved = true>
	
	<cfquery name="updateChangeRequest" datasource="#ds#">
		update globalChangeRequest
		set dateRequested = #createOdbcDate(form.dateRequested)#, customerOrDivision = '#form.customerOrDivision#', personRequestingChange = '#form.personRequestingChange#', personRequestingChangeIs = '#form.personRequestingChangeIs#', changeDetails = '#form.changeDetails#', changeTypeID = #form.changeTypeID#, changeApprovedBy = '#form.changeApprovedBy#' 
		where changeRequestID = #form.rid# 
	</cfquery>
	
	<cfreturn saved>
</cffunction>
<!--- function : update request data --->

