<cfsetting showdebugoutput="no">

<link rel="stylesheet" type="text/css" href="/styles.css">

<style type="text/css">
<!--
.largeText {
	font-size: 14px;
	font-weight: bold;
}
-->
</style>

<cfquery name="getDetails" datasource="#ds#">
	select globalChangeRequest.*, globalChangeRequest_changeTypes.changeType, creatingUsers.firstname + ' ' + creatingUsers.lastname as createdByName, openingUsers.firstname + ' ' + openingUsers.lastname as openedByName, completedUsers.firstname + ' ' + completedUsers.lastname as completedByName  
	from globalChangeRequest
	left join globalChangeRequest_changeTypes on globalChangeRequest.changeTypeID = globalChangeRequest_changeTypes.changeTypeID
	left join admin_users as creatingUsers on globalChangeRequest.createdBy = creatingUsers.adminuserid 
	left join admin_users as openingUsers on globalChangeRequest.openedBy = openingUsers.adminuserid 
	left join admin_users as completedUsers on globalChangeRequest.changecompletedby = completedUsers.adminuserid 
	where globalChangeRequest.changeRequestID = #rid#
</cfquery>

<body leftmargin="0" topmargin="0" rightmargin="0" bottommargin="0">
<cfoutput query="getDetails">
<cfquery name="getAttachments" datasource="#ds#">
	select * from globalChangeRequest_attachments
	where changeRequestID = #rid#
</cfquery>
<table width="540" border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td style="padding:0px" colspan="2">
		<a href="javascript:window.print();"></a>
		<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td><a href="javascript:window.print();"><img src="/images/printerfriendlyicon.gif" alt="Print This Page" border="0" align="absmiddle" />&nbsp;<b>Print this page</b></a></td>
				<td align="center"><a href="javascript:window.close();">Close This Window</a> <a href="javascript:window.close();"><img src="/images/closeWin.gif" alt="Close Window" width="21" height="21" border="0" align="absmiddle" /></a></td>
			</tr>
		</table>
		</td>
	</tr>
	<tr><td colspan="2">&nbsp;</td></tr>
	<tr>
		<td colspan="2" class="linedrow"><span class="largeText">Change Request Detail </span></td>
	</tr>
	<cfif getAttachments.recordcount gt 0>
	<tr>
		<td colspan="2">
		<b>Attachments:</b> (you'll need to view and print separately) <br />
		<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<cfloop query="getAttachments">
				<tr>
					<td><a target="_blank" href="/globalChange/attachments/#savedFile#" style="text-decoration:underline;">#clientFile#</a><br /></td>
				</tr>
			</cfloop>
		</table>
		</td>
	</tr>
	</cfif>
	<tr>
		<td width="129" nowrap="NOWRAP"><b>Request ##: </b></td>
		<td width="385">#changeRequestID#</td>
	</tr>
	<tr>
		<td nowrap="NOWRAP"><b>Date Created: </b></td>
		<td>#dateformat(dateSubmitted,'mm/dd/yyyy')#</td>
	</tr>
	<tr>
		<td nowrap="NOWRAP"><b>Created By: </b></td>
		<td>#createdByName#</td>
	</tr>
	<tr>
		<td nowrap="NOWRAP"><b>Current Status:</b> </td>
		<td>#currentStatus#</td>
	</tr>
	<cfif currentStatus is not "Unread">
	<tr>
		<td nowrap="NOWRAP"><b>Date Opened: </b></td>
		<td>#dateformat(dateOpened,'mm/dd/yyyy')#</td>
	</tr>
	<tr>
		<td nowrap="NOWRAP"><b>Opened By: </b></td>
		<td>#openedByName#</td>
	</tr>
	</cfif>
	<tr>
		<td nowrap="NOWRAP"><b>Customer/Division:</b></td>
		<td>#customerOrDivision#</td>
	</tr>
	<tr>
		<td nowrap="NOWRAP"><b>Date Requested: </b></td>
		<td>#dateformat(dateRequested,'mm/dd/yyyy')#</td>
	</tr>
	<tr>
		<td nowrap="NOWRAP"><b>Person Requesting: </b></td>
		<td>#personRequestingChange# (#personRequestingChangeIs#)</td>
	</tr>
	<tr>
		<td valign="top" nowrap="NOWRAP"><b>Requested Change: </b></td>
		<td valign="top">#changeType#</td>
	</tr>
	<tr>
		<td colspan="2" valign="top" nowrap="nowrap"><b>Change Details: </b></td>
	</tr>
	<tr>
		<td colspan="2">
		#replace(changeDetails,chr(13),"<br />","all")#
		</td>
	</tr>
	<tr>
		<td nowrap="NOWRAP"><b>Approved By: </b></td>
		<td>#changeApprovedBy#</td>
	</tr>
	<cfif currentStatus is "Closed">
	<tr>
		<td nowrap="NOWRAP"><b>Completed By: </b></td>
		<td>#completedByName#</td>
	</tr>
	<tr>
		<td nowrap="NOWRAP"><b>Date Completed: </b></td>
		<td>#dateformat(dateCompleted,'mm/dd/yyyy')#</td>
	</tr>
	<tr>
		<td valign="top" nowrap="NOWRAP"><b>Closing Notes: </b></td>
		<td>#replace(notes, chr(13), "<br>", "all")#</td>
	</tr>
	</cfif>
</table>
</cfoutput>
</body>
