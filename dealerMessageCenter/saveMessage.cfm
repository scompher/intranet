<cfparam name = "form.subject" default = "">
<cfparam name = "form.departmentID" default = "0">
<cfparam name = "form.rec" default = "">
<cfparam name = "form.loc" default = "">
<cfparam name = "form.noteDateTime" default = "">
<cfparam name = "employeeName" default ="">

<cfset dateTime = createodbcdatetime(now())>

<cfset form.noteDateTime = dateTime>

<cfif isDefined("cookie.adminlogin")>
	<cfset loginID = cookie.adminlogin>
	<!--- query to look up departmentID and save the result into form.departmentID --->
	<cfquery name="getDeptID" datasource="#ds#">
		select firstname, lastname, defaultdeptid from admin_users 
		where adminuserid = #cookie.adminlogin# 
	</cfquery>
	<cfset form.employeeName = getDeptID.firstname & " " & getDeptID.lastname> 
	<cfset form.departmentID = getDeptID.defaultDeptID>
<cfelse>
	<cfset loginID = 0>
	<cfset departmentList = 0>
	<cfset form.departmentID = 5>
</cfif>

<cfset form.message = REReplace(form.message,'<[^>]*>','','all')>
<cfset form.message = Replacenocase(form.message,'&nbsp;','','all')>

<cfquery name = "UpdateDb" datasource = "#ds#">
	INSERT INTO DealerMessageCenter_Notes (dealerNumber,receiver,location,subscriberName,subject,notes,employeeID,employeeName,noteDateTime,employeeDepartmentID)
	VALUES ('#form.dealer#',
		'#form.rec#', 
		'#form.loc#',
		'#form.subscriber#',
		'#form.subject#',
		'#form.message#',
		'#loginID#',
		'#form.employeeName#',
		#noteDateTime#,
		#form.departmentID#)
</cfquery>

<!--- get noteID of newly added record --->
<cfquery name="getNoteID" datasource="#ds#">
	select max(noteid) as newNoteID from DealerMessageCenter_Notes 
	where dealerNumber = '#form.dealer#' and receiver = '#form.rec#' and location = '#form.loc#' and noteDateTime = #noteDateTime# 
</cfquery>

<cfset noteID = getNoteID.newNoteID>

<!---send email alert--->
<cfset toList = "dsupport@copsmonitoring.com">
<cfmail from="system.info@copsmonitoring.com" to="#toList#" subject="Dealer Message Notification" username="system" password="V01c3">
This is to notify you of a Dealer Message that needs your attention:

You can view the details here: http://intranet.copsmonitoring.com/dealerMessageCenter/viewApprovalMessage.cfm?noteid=#noteid#
</cfmail>

<cfif (listfind(departmentlist,3) is not 0) or (listfind(departmentlist,1) is not 0)>
	<cflocation url="index.cfm">
<cfelseif departmentlist is 0>
	<div align="center">
	<br />
	Your message has been saved
	<br /><br />
	<a href="/ops/">Return to Main Menu</a>
	</div>
<cfelse>
	<cflocation url="/index.cfm">
</cfif>

