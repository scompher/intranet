<!--- 07/8/2011 : SW : Dealer Message Center Approval --->
<link rel="stylesheet" type="text/css" href="/styles.css">


<cfparam name="form.filterStatus" default="">
<cfparam name="form.filterDealer" default="">
<cfparam name="form.filterSubject" default="">
<cfparam name="form.filterApproved" default="0">
<cfparam name="form.filterBeginDate" default="">
<cfparam name="form.filterEndDate" default="">

<cfset err = "">

<cfif trim(form.filterBeginDate) is not "">
	<cfif not isDate(form.filterBeginDate)>
		<cfset err = listappend(err,"The beginning date must be a valid date.")>
		<cfset form.filterBeginDate = "">
	</cfif>
</cfif>
<cfif trim(form.filterEndDate) is not "">
	<cfif not isDate(form.filterEndDate)>
		<cfset err = listappend(err,"The ending date must be a valid date.")>
		<cfset form.filterEndDate = "">
	</cfif>
</cfif>
<cfif trim(err) is "">
	<cfif trim(form.filterBeginDate) is not "" and trim(form.filterEndDate) is not "">
		<cfif form.filterEndDate lt form.filterBeginDate>
			<cfset err = listappend(err,"The ending date must be after the beginning date.")>
		</cfif>
	</cfif>
</cfif>

<!--- Get Messages and Employee who entered it --->
<cfquery name="GetDealerMessage" datasource="#ds#" >
	SELECT DealerMessageCenter_Notes.*, Admin_Users.firstname, Admin_Users.lastname
	FROM   DealerMessageCenter_Notes 
	LEFT JOIN Admin_Users ON DealerMessageCenter_Notes.employeeID = Admin_Users.adminuserid
	where 
		<cfif trim(form.filterStatus) is not "">
			isRead = #form.filterStatus# and 
		</cfif>
		<cfif trim(form.filterDealer) is not "">
			dealerNumber = '#form.filterDealer#' and 
		</cfif>
		<cfif trim(form.filterSubject) is not "">
			subject = '#form.filterSubject#' and 
		</cfif>
		<cfif trim(form.filterApproved) is not "">
			isApproved = #form.filterApproved# and 
		</cfif>
		<cfif trim(form.filterBeginDate) is not "">
			noteDateTime >= #createodbcdatetime(form.filterBeginDate)# and 
		</cfif>
		<cfif trim(form.filterEndDate) is not "">
			noteDateTime < #createodbcdatetime(form.filterEndDate)# and 
		</cfif>
	copsDeleted = 0 and 
	1=1
	order by DealerMessageCenter_Notes.noteDateTime asc 
</cfquery>

<br />
<div align="center">
<div class="normal" style="width:600px; position:relative;z-index:1;">
	<cfif trim(err) is not "">
		<b class="alert">
		<cfloop list="#err#" index="e">
		<cfoutput>#e#</cfoutput><br />
		</cfloop>
		</b>
	</cfif>
	<cfform method="post" action="index.cfm">
	<table width="600" border="0" cellspacing="0" cellpadding="5">
	    <tr>
            <td class="highlightbar"><b>Dealer Message Center - Filter Controls</b></td>
        </tr>
        <tr>
            <td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td width="16%" nowrap="nowrap"><b>Status:</b></td>
					<td width="84%">
						<cfselect name="filterStatus">
							<option value="">All</option>
							<option <cfif form.filterStatus is 1>selected</cfif> value="1">Read</option>
							<option <cfif form.filterStatus is 0>selected</cfif> value="0">Unread</option>
						</cfselect>
					</td>
				</tr>
				<tr>
					<td nowrap="nowrap"><b>Dealer #: </b></td>
					<td><cfinput type="text" name="filterDealer" value="#form.filterDealer#" maxlength="255" style="width:40px"></td>
				</tr>
				<tr>
					<td nowrap="nowrap"><b>Subject:</b></td>
					<td>
						<cfselect name="filterSubject">	
							<option value="">All</option>
							<option <cfif form.filterSubject is "Alarm Handling"> selected </cfif> value="Alarm Handling">Alarm Handling</option>
							<option <cfif form.filterSubject is "Data Support"> selected </cfif> value="Data Support">Data Support</option>
							<option <cfif form.filterSubject is "Dealer Action Required"> selected </cfif> value="Dealer Action Required">Dealer Action Required</option>
							<option <cfif form.filterSubject is "Dealer Support Request"> selected </cfif> value="Dealer Support Request">Dealer Support Request</option>
							<option <cfif form.filterSubject is "Subscriber Service Request"> selected </cfif> value="Subscriber Service Request">Subscriber Service Request</option>
						</cfselect>
					</td>
				</tr>
				<tr>
					<td nowrap="nowrap"><b>Date Created: </b></td>
					<td class="nopadding">
						<table border="0" cellspacing="0" cellpadding="5">
							<tr>
								<td>Beginning Date:</td>
								<td><cfinput type="datefield" name="filterBeginDate" style="vertical-align:middle; width:75px;" value="#form.filterBeginDate#"> </td>
								<td>&nbsp;</td>
								<td>Ending Date:</td>
								<td><cfinput style="vertical-align:middle; width:75px;" type="datefield" name="filterEndDate" value="#form.filterEndDate#"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td nowrap="nowrap"><b>Approved:</b></td>
					<td>
						<cfselect name="filterApproved">
							<option value="">All</option>
							<option <cfif form.filterApproved is 1>selected</cfif> value="1">Yes</option>
							<option <cfif form.filterApproved is 0>selected</cfif> value="0">No</option>
						</cfselect>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<input name="Submit" type="submit" class="sidebar" value="Apply Filters" />
						<input type="button" onclick="document.location = 'index.cfm';" value="Clear Filter" class="sidebar" />
					</td>
				</tr>
			</table>
			</td>
		</tr>
	</table>
	</cfform>
</div>
	<br />
<div align="center" class="normal" style="position:relative;z-index:0">
    <table width="600" border="0" cellpadding="5" cellspacing="0">
	    <tr>
            <td style="padding-left:0px;"><input type="button" onclick="document.location = 'newMessage.cfm';" value="New Message" /></td>
        </tr>
	    <tr>
            <td class="highlightbar"><b>Dealer Message Center - Maintenance Menu</b></td>
        </tr>
        <tr>
            <td class="greyrowbottomnopadding">
                <table width="100%" border="0" cellpadding="5" cellspacing="0">
                    <tr>
                        <td width="4%" class="linedrowrightcolumn">&nbsp;</td>
                        <td width="11%" nowrap="nowrap" class="linedrowrightcolumn" align="center"><b>Action</b></td>
                        <td width="25%" align="center" nowrap="nowrap" class="linedrowrightcolumn"><b>Created</b></td>
						<td width="25%" align="center" nowrap="nowrap" class="linedrowrightcolumn"><b>Created By</b></td>
                        <td width="25%" nowrap="nowrap" class="linedrowrightcolumn"><b>Dealer #</b></td>
                        <td width="24%" nowrap="nowrap" class="linedrowrightcolumn"><b>Account Number</b></td>
                        <td width="20%" align="center" nowrap="nowrap" class="linedrowrightcolumn"><b>Subject</b></td>
                        <td width="16%" align="center" nowrap="nowrap" class="linedrow"><b>Approved</b></td>
                    </tr>
					<cfoutput query="GetDealerMessage">
						<tr>
						<td align="center" class="linedrowrightcolumn">
						<cfif GetDealerMessage.isRead is 0>
							<img border="0" src="/images/unread.gif" />
						<cfelseif GetDealerMessage.isRead is 1>
							<img border="0" src="/images/read.gif" /></a>
						</cfif>
						</td>
                        <td align="center" nowrap="nowrap" class="linedrowrightcolumn"><a href="editMessage.cfm?noteid=#GetDealerMessage.noteid#"><img src="/images/edit.gif" alt="Edit" width="16" height="16" border="" />&nbsp;</a>
						<a onclick="return confirm('Are you sure you want to delete this?');" href="deleteMessage.cfm?noteid=#GetDealerMessage.noteid#"><img src="/images/delete.gif" alt="Delete" width="16" height="16" border="" />&nbsp;</a>
						<a href="viewApprovalMessage.cfm?noteid=#GetDealerMessage.noteid#"><img src="/images/view.gif" alt="View" width="16" height="16" border=""/></a></td>							
							<td nowrap="nowrap" class="linedrowrightcolumn">#dateformat(noteDateTime,'mm/dd/yyyy')# #timeformat(noteDateTime,'hh:mm tt')#&nbsp;</td>
							<td nowrap="nowrap" class="linedrowrightcolumn"><cfif GetDealerMessage.employeeID is 0>#GetDealerMessage.employeeName#</cfif>#firstname# #lastname#&nbsp;</td>
							<td nowrap="nowrap" class="linedrowrightcolumn">#dealerNumber#&nbsp;</td>
							<td nowrap="nowrap" class="linedrowrightcolumn"><cfif GetDealerMessage.receiver is not "" and GetDealerMessage.location is not "">#GetDealerMessage.receiver#-#GetDealerMessage.location#<cfelse>#GetDealerMessage.receiver##GetDealerMessage.location#</cfif>&nbsp;</td>
							<td nowrap="nowrap" class="linedrowrightcolumn">#subject#&nbsp;</td>
							<td nowrap="nowrap" class="linedrowrightcolumn">
							<cfif GetDealerMessage.isApproved is 0>
								No
							<cfelseif GetDealerMessage.isApproved is 1>
								Yes
							</cfif>
							</td>
						</tr>
					</cfoutput>
                </table>
            </td>
        </tr>
    </table>
	<br />
	<a style="text-decoration:underline;" href="/index.cfm">Return to Intranet</a>
</div>
</div>