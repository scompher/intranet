<!--- 07/8/2011 : SW : Dealer Message Center --->
<link rel="stylesheet" type="text/css" href="/styles.css">

<cfif isDefined("form.btnSave")>
	<cfset url.noteid = form.noteid>
	<cfset markRead = "1">
	<cfif not isDefined("form.alreadyApproved")>
		<cfif form.isApproved is "0">
			<cfset markApproved = "0">
		<cfelse>
			<cfset markApproved = "1">
		</cfif>
	</cfif>
	<cfif form.departmentID is "" or form.departmentID is 0>
		<p class="alert" align="center"><b>Department is required.</b></p>
	<cfelse>
		<cfinclude template="editMessageSave.cfm">
	</cfif>
</cfif>

<cfquery name="GetDealerMessage" datasource="#ds#" >
	SELECT  DealerMessageCenter_Notes.*, Admin_Users_Departments.departmentID, Admin_Users_Departments.department
	FROM    DealerMessageCenter_Notes LEFT JOIN
			Admin_Users_Departments ON DealerMessageCenter_Notes.employeeDepartmentID = Admin_Users_Departments.departmentID
	WHERE noteid = #noteid#
</cfquery>	

<cfquery name="getDepartments" datasource="#ds#">
	select * from admin_users_departments 
	order by department asc 
</cfquery>

<cfparam name = "form.noteid" default = "#noteid#">
<cfparam name = "form.dealerNumber" default = "#GetDealerMessage.dealernumber#">
<cfparam name = "form.rec" default = "#GetDealerMessage.receiver#">
<cfparam name = "form.loc" default = "#GetDealerMessage.location#">
<cfparam name = "form.subscriber" default = "#GetDealerMessage.subscriberName#">
<cfparam name = "form.subject" default = "#GetDealerMessage.subject#">
<cfparam name = "form.departmentID" default = "#GetDealerMessage.employeeDepartmentID#">
<cfparam name = "form.notes" default = "#GetDealerMessage.notes#">
<cfparam name = "form.employeeName" default = "#GetDealerMessage.employeeName#">
<cfparam name = "form.isApproved" default = "#GetDealerMessage.isApproved#">


<div align="center" class="normal">
<cfform method="post" action="editMessage.cfm">
<cfinput type = "hidden" name="noteid" value="#noteid#"/>
<cfinput type = "hidden" name="dealernumber" value="#form.dealernumber#"/>
<cfinput type="hidden" name="departmentID" value="#form.departmentID#">
<cfinput type="hidden" name="employeeName" value="#form.employeeName#">

<table border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td class="highlightbar"><b>Dealer Message Center - Edit Entry</b></td>
	</tr>
	<tr>
	<td class="greyrowbottomnopadding">
	<table border="0" cellpadding="5" cellspacing="0">
	<cfoutput>
	<tr>
		<td nowrap="nowrap"><b>Dealer Number:</b></td>
		<td> 
			<cfoutput>#form.dealerNumber#</cfoutput>
		</td>
	</tr>
	<tr>	
		<td nowrap="nowrap"><b>Subject:</b>&nbsp;<span class="smallred"><b>*</b></span></td>
		<td>
			<cfselect onChange="this.form.submit();" name="subject" multiple="no" required="yes" message="Subject is required">
				<option <cfif form.subject is "Alarm Handling"> selected </cfif> value="Alarm Handling">Alarm Handling</option>
				<option <cfif form.subject is "Data Support"> selected </cfif> value="Data Support">Data Support</option>
				<option <cfif form.subject is "Dealer Action Required"> selected </cfif> value="Dealer Action Required">Dealer Action Required</option>
				<option <cfif form.subject is "Dealer Support Request"> selected </cfif> value="Dealer Support Request">Dealer Support Request</option>
				<option <cfif form.subject is "Subscriber Service Request"> selected </cfif> value="Subscriber Service Request">Subscriber Service Request</option>
			</cfselect>
		</td>
	</tr>
	<cfif trim(form.subject) is not "">
			<cfif trim(form.subject) is "Dealer Support Request">
				<tr>	
					<td nowrap="nowrap"><b>Account Number: </b></td>
					<td>
						<cfinput type="text" maxlength="5" name="rec" value="#form.rec#" style="width:50px; vertical-align:middle;">
						 -   
						<cfinput validate="integer" type="text" maxlength="7" name="loc" id="loc" value="#form.loc#" style="width:50px; vertical-align:middle;">
					</td>
				</tr>
				<tr>
					<td nowrap="nowrap"><b>Subscriber Name:</b></td>
					<td>
						<cfinput type="text" name="subscriber" id="subscriber" value="#form.subscriber#" style="width:300px">
					</td>
				</tr>
			<cfelse>
				<tr>	
					<td nowrap="nowrap"><b>Account Number: </b>&nbsp;<span class="smallred"><b>*</b></span></td>
					<td>
						<cfinput required ="yes" message="Receiver is required" type="text" maxlength="5" name="rec" value="#form.rec#" style="width:50px; vertical-align:middle;">
						 -   
						<cfinput required ="yes" message="Location is required" type="text" maxlength="7" name="loc" id="loc" value="#form.loc#" style="width:50px; vertical-align:middle;">
					</td>
				</tr>
				<tr>
					<td nowrap="nowrap"><b>Subscriber Name:</b>&nbsp;<span class="smallred"><b>*</b></span></td>
					<td>
						<cfinput required="yes" message="Subscriber is required" type="text" name="subscriber" id="subscriber" value="#form.subscriber#" style="width:300px">
					</td>
				</tr>
			</cfif>
			<!--- removed : PG : 08/30/2011
			<cfif GetDealerMessage.employeeName is not "">
				<tr>
				<td nowrap="nowrap"><b>Employee Name:</b></td>
					<td>
						<cfinput required ="yes" message="Employee Name is required" type="text" name="employeeName" id="employeeName" value="#GetDealerMessage.employeeName#">
					</td>
				</tr>
				<tr>
				<td nowrap="nowrap"><b>Employee Department:</b></td>
				<td><cfselect name="departmentID" required="yes" message="Department is required">
						<option value=""></option>
						<cfloop query="getDepartments">
							<option <cfif getDepartments.departmentID is GetDealerMessage.departmentID>selected</cfif> value="#getDepartments.departmentID#">#getDepartments.department#</option>
						</cfloop>
					</cfselect></td>
				</tr>
			</cfif>
			--->
			<tr>
				<td colspan="2" nowrap="nowrap"><b>Message:</b>&nbsp;<span class="smallred"><b>*</b></span></td>
			</tr>
			<tr>
				<td colspan="2" nowrap="nowrap">Enter starts a new paragraph, shift + enter goes to next line</td>
			</tr>
			<tr>
				<td colspan="2">
					<!--- <cftextarea richtext="yes" html="no" toolbar="SpellCheckOnly" name="message" value="#form.notes#" rows="5" required="yes" id="message" style="width:500px" message="Message is required." enabled="no" visable="no"></cftextarea> --->
					<cftextarea name="message" value="#form.notes#" rows="5" required="yes" id="message" style="width:500px" message="Message is required."></cftextarea>
				</td>
			</tr>
			<tr>
				<td nowrap="nowrap"><b>Approval (Y/N):</b></td>
				<td>
				<cfif trim(GetDealerMessage.isApproved) is 1>
					YES
					<cfinput type="hidden" name="alreadyApproved" value="1">
				<cfelse>	
					<cfselect name="isApproved" enabled="No" multiple="no">	
						<option <cfif GetDealerMessage.isApproved is ""> selected </cfif> value=""></option>
						<option <cfif GetDealerMessage.isApproved is 0> selected </cfif> value="0">NO</option>
						<option <cfif GetDealerMessage.isApproved is 1> selected </cfif> value="1">YES</option>			
					</cfselect>
				</cfif>
				</td>
			</tr>
		</cfif>
		<tr>
			<td colspan="2">
			<cfinput type="submit" name="btnSave" id="Save Info" value="Save Updates"> &nbsp;
			</td>
		</tr>
	</cfoutput>
	</table>	
	</td>
	</tr>
</table>
</cfform>

<br>
<a style="text-decoration:underline;" href="index.cfm">Return to Dealer Message List</a>
<br />
</div>
