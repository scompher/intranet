
<cfquery name="GetDealerMessage" datasource="#attributes.datasource#" >
	select DealerMessageCenter_Notes.*, Admin_Users_Departments.department 
	from DealerMessageCenter_Notes 
	left join Admin_Users_Departments on DealerMessageCenter_Notes.employeeDepartmentID = Admin_Users_Departments.departmentID
	WHERE DealerMessageCenter_Notes.noteid = #attributes.i# 
</cfquery>

<link rel="stylesheet" type="text/css" href="/styles.css">

<cfoutput query="GetDealerMessage">
<table width="550" border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td width="149" nowrap="nowrap"><b>Created:</b></td>
		<td width="381">#dateformat(noteDateTime,'mm/dd/yyyy')# #timeformat(noteDateTime,'hh:mm tt')# </td>
	</tr>
	<tr>
		<td nowrap="nowrap"><b>Approved:</b></td>
		<td>
		<cfif isApproved is 1>YES<cfelse>NO</cfif>
		</td>
	</tr>
	<tr>
		<td nowrap="nowrap"><b>Dealer Number:</b></td>
		<td>
		#GetDealerMessage.dealerNumber#		</td>
	</tr>
	<tr>	
		<td nowrap="nowrap"><b>Account Number:</b></td>
		<td>
			#GetDealerMessage.receiver#
			 -
			#GetDealerMessage.location#
		</td>
	</tr>
	<tr>
		<td nowrap="nowrap"><b>Subscriber Name:</b></td>
		<td>
			#GetDealerMessage.subscriberName#		</td>
	</tr>
	<tr>
		<td nowrap="nowrap"><b>Employee Name:</b></td>
		<td>
			#GetDealerMessage.employeeName#		</td>
	</tr>
	<tr>
	<td nowrap="nowrap"><b>Employee Department:</b></td>
		<td>
			#GetDealerMessage.department#		</td>
	</tr>
	<tr>	
		<td nowrap="nowrap"><b>Subject:</b></td>
		<td>
			#GetDealerMessage.subject#		</td>
	</tr>
	<tr>
		<td colspan="2" nowrap="nowrap"><b>Message:</b></td>
	</tr>
	<tr>
		<td colspan="2">
		#GetDealerMessage.notes#		</td>
	</tr>
</table>	
</cfoutput>