<!--- Dealer Message Center Open (Read) Report --->
<cfsetting showdebugoutput="yes">

<cfset dateTimeNow = createodbcdatetime(now())>

<link rel="stylesheet" type="text/css" href="../../styles.css">
<script language="javascript" type="text/javascript">
function viewPrintWin(u) {
	window.open(u,"inqWin","width=600,height=500,scrollbars=1");
}
</script>

<cfquery name="getDealerMsgs" datasource="#ds#">	
	SELECT DealerMessageCenter_Notes.*, Admin_Users_Departments.department 
	from DealerMessageCenter_Notes 
	LEFT JOIN Admin_Users_Departments on DealerMessageCenter_Notes.employeeDepartmentID = Admin_Users_Departments.departmentid 
	WHERE DealerMessageCenter_Notes.copsDeleted = 0 and 
	DealerMessageCenter_Notes.isRead = 1 and
	DealerMessageCenter_Notes.isApproved = 0 and
	1=1
	Order by Admin_Users_Departments.department 
</cfquery>
	
<cfquery name="ctrDealerMsgs" datasource="#ds#">
	SELECT DealerMessageCenter_Notes.*
	from DealerMessageCenter_Notes 
</cfquery>
	
<!---Total Read Messages--->
<cfset totalOpen = 0>
<cfloop query="getDealerMsgs">
	<cfset totalOpen = totalOpen + 1>
</cfloop>

<div align="center">
<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td>
		<table border="0" width="100%" cellspacing="0" cellpadding="5">
			<tr>
				<td class="nopadding">
					<table border="0" cellspacing="0" cellpadding="5">
						<cfoutput>
						<tr>
							<td>
							<a target="_blank" href="openmessage_printall.cfm"><img src="../../images/printerfriendlyicon.gif" alt="Print" width="30" height="30" border="0" /></a></td>
							<td><a style="text-decoration:underline;" target="_blank" href="openmessage_printall.cfm">Print this report</a></td>
							<td>(this report requires adobe acrobat reader. <a href="/download/adobe_reader.exe" style="text-decoration:underline;">click here to download</a> if you do not have it)</td>
						</tr>
						</cfoutput>
					</table>
				</td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td align="center"><b>Dealer Message Center Pending Approval Report as of <cfoutput>#dateformat(now(),'mm/dd/yyyy')#</cfoutput></b></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td class="nopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td align="center"><b>Total Pending Approval Messages </b></td>
				</tr>
				<cfoutput>
				<tr>
					<td align="center">#totalOpen#</td>
				</tr>
				</cfoutput>
			</table>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
</table>

<br>
<table width="100%" border="0" cellspacing="0" cellpadding="5">
    <tr>
        <td>&nbsp;</td>
        <td align="center"><b>View</b></td>
        <td align="center"><b>Dept</b></td>
        <td align="center"><b>Generated</b></td>
        <td align="center"><b>Dealer # </b></td>
        <td align="center"><b>Account # </b></td>
        <td align="center"><b>Subject</b></td>
		 <td align="center"><b>Approved</b></td>
    </tr>
    <cfoutput query="getDealerMsgs">
        <tr>
            <td align="right" width="1%">#getDealerMsgs.currentRow#.</td>
            <td align="center"><a href="javascript:viewPrintWin('../viewMessage.cfm?noteid=#noteid#');" style="text-decoration:underline;">View this</a></td>
            <td align="center">#department#</td>
			<td align="center">#dateformat(noteDateTime,'mm/dd/yyyy')#</td>
			<td align="center">#dealerNumber#</td>
			<td align="center">#receiver#&ndash;#location#</td>
			<td align="center">#subject#</td>
            <td align="center"><cfif getDealerMsgs.isApproved is 0>No<cfelse>Yes</cfif></td>
        </tr>
    </cfoutput>
</table>

<br />
<p class="normal"><a style="text-decoration:underline" href="index.cfm">Return to Reporting Menu</a> </p>
</div>