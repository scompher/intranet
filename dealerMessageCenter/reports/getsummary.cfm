<!--- Dealer Message Center Report Summary Search --->
<script language="javascript" src="cal2.js">
/*
Xin's Popup calendar script-  Xin Yang (http://www.yxscripts.com/)
Script featured on/available at http://www.dynamicdrive.com/
This notice must stay intact for use
*/
</script>
<script language="javascript" src="cal_conf2.js"></script>

<link rel="stylesheet" type="text/css" href="/styles.css">

<cfparam name = "form.dealer" default = "">
<cfparam name = "form.subject" default = "">

<!--- Get Departements --->
<cfquery name="getDepts" datasource="#ds#">
	select * from admin_users_departments
	order by department asc
</cfquery>

<div align="center">
<cfform method="post" action="dealerSummary.cfm" name="mainform">
<table border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Summary Report</b> </td>
	</tr>
	<tr>
		<td class="greyrow"><b>Select Reporting Date Range:</b></td>
	</tr>
	<tr>
		<td class="greyrow" style="padding:0px">
			<cfoutput>
			<table border="0" cellspacing="0" cellpadding="5" class="grey">
				<tr>
					<td>Starting:</td>
					<td class="grey">
						<cfinput name="sd" type="text" id="sd" style="width:75px" value="#dateformat(now(),'mm/dd/yyyy')#" maxlength="10" />
					</td>
					<td class="grey"> <a style="text-decoration:none;" href="javascript:showCal('startDate');"> <img src="/images/calicon.gif" alt="Select a Date" align="absmiddle" width="20" height="20" border="0" /> </a> </td>
					<td width="20" class="grey">&nbsp;</td>
					<td>Ending:</td>
					<td><span class="grey">
						<cfinput name="ed" type="text" id="ed" style="width:75px" value="#dateformat(now(),'mm/dd/yyyy')#" maxlength="10" />
					</span></td>
					<td><a style="text-decoration:none;" href="javascript:showCal('endDate');"><img src="/images/calicon.gif" alt="Select a Date" align="absmiddle" width="20" height="20" border="0" /></a></td>
				</tr>
			</table>
			</cfoutput>
		</td>
	</tr>
	<tr>
		<td class="greyrow" style="padding:0px">
			<table border="0" cellspacing="0" cellpadding="5" class="grey">
				<tr>
					<td>Department:</td>
					<td>
						<select name="departmentID">
							<option value="0">All Departments</option>
							<cfoutput query="getdepts">
								<option value="#departmentid#">#department#</option>
							</cfoutput>
						</select>
					</td>
				</tr>
				<tr>
					<td>Dealer:</td>
					<td>
					<cfinput type="text" name="dealerNumber" id="dealerNumber" value="#form.dealer#">
					</td>
				</tr>
				<tr>
				<td>Subject:</td>
					<td>
					<cfselect enabled="No" name="subject" multiple="no">
						<option <cfif form.subject is ""> selected </cfif> value=""></option>
						<option <cfif form.subject is "Alarm Handling"> selected </cfif> value="Alarm Handling">Alarm Handling</option>
						<option <cfif form.subject is "Data Support"> selected </cfif> value="Data Support">Data Support</option>
						<option <cfif form.subject is "Dealer Action Required"> selected </cfif> value="Dealer Action Required">Dealer Action Required</option>
						<option <cfif form.subject is "Dealer Support Request"> selected </cfif> value="Dealer Support Request">Dealer Support Request</option>
						<option <cfif form.subject is "Subscriber Service Request"> selected </cfif> value="Subscriber Service Request">Subscriber Service Request</option>
					</cfselect>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td class="greyrowbottom">
			<input name="Submit" type="submit" class="sidebar" value="Get Report">
		</td>
	</tr>
</table>
<br />
<p class="normal"><a style="text-decoration:underline" href="index.cfm">Return to Reporting Menu</a> </p>
</cfform>
</div>
