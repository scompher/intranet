<!--- Dealer Message Center Overdue Report --->
<cfsetting showdebugoutput="yes">

<cfset dateTimeNow = createodbcdatetime(now())>

<cfdocument format="pdf" orientation="portrait" scale="100">

<link rel="stylesheet" type="text/css" href="../../styles.css">

<cfquery name="getDealerMsgs" datasource="#ds#">	
	SELECT DealerMessageCenter_Notes.*, Admin_Users_Departments.department 
	from DealerMessageCenter_Notes 
	LEFT JOIN Admin_Users_Departments on DealerMessageCenter_Notes.employeeDepartmentID = Admin_Users_Departments.departmentid 
	WHERE DealerMessageCenter_Notes.copsDeleted = 0 and 
	DealerMessageCenter_Notes.isRead = 0 and
	(
	(noteDateTime is not null and DATEDIFF(hh,noteDateTime,#dateTimeNow#) > 24) 
	)
	and
	1=1
	Order by Admin_Users_Departments.department 
</cfquery>
	
<cfquery name="ctrDealerMsgs" datasource="#ds#">
	SELECT DealerMessageCenter_Notes.*
	from DealerMessageCenter_Notes 
</cfquery>
	
<cfset totalOverdue = 0>
<cfloop query="getDealerMsgs">
	<cfset totalOverdue = totalOverdue + 1>
</cfloop>

<cfif isnumeric(ctrDealerMsgs.recordcount)>
	<cfset totalMsgs = ctrDealerMsgs.recordcount>
<cfelse>
	<cfset totalMsgs = 0>
</cfif>

<cfif totalMsgs is not 0>
	<cfset totalPercentOverdue = (totalOverdue / totalMsgs) * 100>
<cfelse>
	<cfset totalPercentOverdue = 0>
</cfif>

<div align="center">
<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center"><b>Dealer Message Center Overdue Report as of <cfoutput>#dateformat(now(),'mm/dd/yyyy')#</cfoutput></b></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td class="nopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td align="center"><b>Total Messages </b></td>
					<td align="center"><b>Total Overdue </b></td>
					<td align="center"><b>Total % Overdue </b></td>
				</tr>
				<cfoutput>
				<tr>
					<td align="center">#totalMsgs#</td>
					<td align="center">#totalOverdue#</td>
					<td align="center">#numberformat(totalPercentOverdue)#%</td>
				</tr>
				</cfoutput>
			</table>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
</table>

<br>
<table width="100%" border="0" cellspacing="0" cellpadding="5">
    <tr>
        <td>&nbsp;</td>
        <td align="center"><b>Dept</b></td>
        <td align="center"><b>Generated</b></td>
        <td align="center"><b>Dealer # </b></td>
        <td align="center"><b>Account # </b></td>
        <td align="center"><b>Subject</b></td>
    </tr>
    <cfoutput query="getDealerMsgs">
        <tr>
            <td align="right" width="1%">#getDealerMsgs.currentRow#.</td>
            <td align="center">#department#</td>
			<td align="center">#dateformat(noteDateTime,'mm/dd/yyyy')#</td>
			<td align="center">#dealerNumber#</td>
			<td align="center">#receiver#&ndash;#location#</td>
            <td align="center">#subject#</td>
        </tr>
    </cfoutput>
</table>

<cfdocumentitem type="pagebreak"></cfdocumentitem>

<cfloop query="getDealerMsgs">
	<cfset i = getDealerMsgs.noteid>
	<cf_printMessage i="#i#" datasource="#ds#">
	<hr />
	<!--- <cfdocumentitem type="footer"><cfoutput>Page #cfdocument.currentpagenumber# of #cfdocument.totalpagecount#</cfoutput></cfdocumentitem> --->
	<!--- <cfdocumentitem type="pagebreak"></cfdocumentitem> --->
</cfloop>

</cfdocument>
