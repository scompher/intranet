
<cfquery name="getDepts" datasource="#ds#">
	select * 
	from Admin_Users_Departments
	order by department asc
</cfquery>

<cfif trim(url.sd) is not "">
	<cfset url.sd = createodbcdate(url.sd)>
</cfif>
<cfif trim(url.ed) is not "">
	<cfset url.ed = dateadd("d",1,url.ed)>
	<cfset url.ed = createodbcdate(url.ed)>
</cfif>

<cfdocument format="pdf" orientation="portrait" scale="100">

<link rel="stylesheet" type="text/css" href="../../styles.css">

<cfquery name="getDealerMsgs" datasource="#ds#">	
	SELECT DealerMessageCenter_Notes.*, Admin_Users_Departments.department 
	from DealerMessageCenter_Notes 
	LEFT JOIN Admin_Users_Departments on DealerMessageCenter_Notes.employeeDepartmentID = Admin_Users_Departments.departmentid 
	WHERE DealerMessageCenter_Notes.copsDeleted = 0 and 
	<cfif trim(url.sd) is not "">
		noteDateTime >= #url.sd# and 
	</cfif>
	<cfif trim(url.ed) is not "">
		noteDateTime < #url.ed# and 
	</cfif>
	<cfif departmentID is not 0>
		employeeDepartmentID = #departmentID# and
	</cfif>
	<cfif dealerNumber is not "">
		dealerNumber = '#dealerNumber#' and
	</cfif>
	<cfif trim(subject) is not "">
		subject = '#subject#' and
	</cfif>
	1=1
	Order by department 
</cfquery>

<cfset totalApproved = 0>
<cfloop query="getDealerMsgs">
	<cfif isApproved is not 0>
		<cfset totalApproved = totalApproved + 1>
	</cfif>
</cfloop>

<cfif isnumeric(getDealerMsgs.recordcount)>
	<cfset totalMsgs = getDealerMsgs.recordcount>
<cfelse>
	<cfset totalMsgs = 0>
</cfif>

<cfif totalMsgs is not 0>
	<cfset totalPercentApproved = (totalApproved / totalMsgs) * 100>
<cfelse>
	<cfset totalPercentApproved = 0>
</cfif>

<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="left"><a style="text-decoration:underline" href="getsummary.cfm">Return to Date Selection</a></td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	<tr>
		<td>
		<table border="0" width="100%" cellspacing="0" cellpadding="5">
			<tr>
				<td class="nopadding">
					<table border="0" cellspacing="0" cellpadding="5">
						<cfoutput>
						<tr>
							<td>
							<a target="_blank" href="summary_printall.cfm?sd=#sd#&ed=#ed#&did=#departmentID#"><img src="../../images/printerfriendlyicon.gif" alt="Print" width="30" height="30" border="0" /></a></td>
							<td><a style="text-decoration:underline;" target="_blank" href="dealerSummary_printall.cfm?sd=#sd#&ed=#ed#&did=#departmentID#">Print this report</a></td>
							<td>(this report requires adobe acrobat reader. <a href="/download/adobe_reader.exe" style="text-decoration:underline;">click here to download</a> if you do not have it)</td>
						</tr>
						</cfoutput>
					</table>
				</td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td align="center"><b>Dealer Message Center Report Summary</b></td>
	</tr>
	<cfoutput>
	<tr>
		<td align="center"><b>From #dateformat(sd,'mm/dd/yyyy')# to #dateformat(ed,'mm/dd/yyyy')#</b></td>
	</tr>
	</cfoutput>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td class="nopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td align="center"><b>Total Messages </b></td>
					<td align="center"><b>Total Approved </b></td>
					<td align="center"><b>Total % Approved </b></td>
				</tr>
				<cfoutput>
				<tr>
					<td align="center">#totalMsgs#</td>
					<td align="center">#totalApproved#</td>
					<td align="center">#numberformat(totalPercentApproved)#%</td>
				</tr>
				</cfoutput>
			</table>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
</table>

<br>
<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td>&nbsp;</td>
		<td align="center"><b>Generated</b></td>
		<td align="center"><b>Approved</b></td>
		<td align="center"><b>Dealer # </b></td>
		<td align="center"><b>Account # </b></td>
		<td align="center"><b>Subject</b></td>
		<td align="center"><b>Dept</b></td>
	</tr>
	<cfoutput query="getDealerMsgs">
	<tr>
		<td align="right" width="1%">#getDealerMsgs.currentRow#.</td>
		<td align="center">#dateformat(noteDateTime,'mm/dd/yyyy')#</td>
		<td align="center">
		<cfif trim(dateTimeApproved) is not ""> 
		#dateformat(dateTimeApproved,'mm/dd/yyyy')#
		<cfelse>
		&nbsp;
		</cfif>
		</td>
		<td align="center">#dealerNumber#</td>
		<td align="center">#receiver#&ndash;#location#</td>
		<td align="center">#subject#</td>
		<td align="center">#department#</td>
	</tr>
	</cfoutput>
</table>

<cfdocumentitem type="pagebreak"></cfdocumentitem>

<cfloop query="getDealerMsgs">
	<cfset i = getDealerMsgs.noteid>
	<cf_printMessage i="#i#" datasource="#ds#">
	<hr />
	<!--- <cfdocumentitem type="footer"><cfoutput>Page #cfdocument.currentpagenumber# of #cfdocument.totalpagecount#</cfoutput></cfdocumentitem> --->
	<!--- <cfdocumentitem type="pagebreak"></cfdocumentitem> --->
</cfloop>

</cfdocument>