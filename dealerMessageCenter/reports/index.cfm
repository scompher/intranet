<!--- Dealer Message Center Report Menu --->
<link rel="stylesheet" type="text/css" href="../../styles.css">

<div align="center">
<table border="0" cellspacing="0" cellpadding="7">
	<tr>
		<td class="highlightbar" align="center"><b>Dealer Message Center Reporting Menu</b></td>
	</tr>
	<tr>
		<td class="greyrow">1. <a href="getsummary.cfm" style="text-decoration:underline;">Summary Message Report</a></td>
	</tr>
	<tr>
		<td class="greyrow">2. <a href="overdueMessage.cfm" style="text-decoration:underline;">Overdue Message Report</a></td>
	</tr>
	<tr>
		<td class="greyrowbottom">3. <a href="openMessage.cfm" style="text-decoration:underline;">Pending Approval Message Report</a></td>
	</tr>
</table>
<br />
<a href="/index.cfm" class="normal" style="text-decoration:underline;">Return to Intranet Menu</a>
</div>


