
<cfset dateTimeNow = createodbcdatetime(now())>

<cfdocument format="pdf" orientation="portrait" scale="100">

<link rel="stylesheet" type="text/css" href="../../styles.css">

<cfquery name="getDealerMsgs" datasource="#ds#">	
	SELECT DealerMessageCenter_Notes.*, Admin_Users_Departments.department 
	from DealerMessageCenter_Notes 
	LEFT JOIN Admin_Users_Departments on DealerMessageCenter_Notes.employeeDepartmentID = Admin_Users_Departments.departmentid 
	WHERE DealerMessageCenter_Notes.copsDeleted = 0 and 
	DealerMessageCenter_Notes.isRead = 1 and
	1=1
	Order by Admin_Users_Departments.department 
</cfquery>
	
<cfquery name="ctrDealerMsgs" datasource="#ds#">
	SELECT DealerMessageCenter_Notes.*
	from DealerMessageCenter_Notes 
</cfquery>
	
<!---Total Read Messages--->
<cfset totalOpen = 0>
<cfloop query="getDealerMsgs">
	<cfset totalOpen = totalOpen + 1>
</cfloop>

<div align="center">
<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center"><b>Dealer Message Center Open Report as of <cfoutput>#dateformat(now(),'mm/dd/yyyy')#</cfoutput></b></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td class="nopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td align="center"><b>Total Open Messages </b></td>
				</tr>
				<cfoutput>
				<tr>
					<td align="center">#totalOpen#</td>
				</tr>
				</cfoutput>
			</table>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
</table>

<br>
<table width="100%" border="0" cellspacing="0" cellpadding="5">
    <tr>
        <td>&nbsp;</td>
        <td align="center"><b>Dept</b></td>
        <td align="center"><b>Generated</b></td>
        <td align="center"><b>Dealer # </b></td>
        <td align="center"><b>Account # </b></td>
        <td align="center"><b>Subject</b></td>
		 <td align="center"><b>Approved</b></td>
    </tr>
    <cfoutput query="getDealerMsgs">
        <tr>
            <td align="right" width="1%">#getDealerMsgs.currentRow#.</td>
            <td align="center">#department#</td>
			<td align="center">#dateformat(noteDateTime,'mm/dd/yyyy')#</td>
			<td align="center">#dealerNumber#</td>
			<td align="center">#receiver#&ndash;#location#</td>
			<td align="center">#subject#</td>
            <td align="center"><cfif getDealerMsgs.isApproved is 0>No<cfelse>Yes</cfif></td>
        </tr>
    </cfoutput>
</table>

<cfdocumentitem type="pagebreak"></cfdocumentitem>

<cfloop query="getDealerMsgs">
	<cfset i = getDealerMsgs.noteid>
	<cf_printMessage i="#i#" datasource="#ds#">
	<hr />
	<!--- <cfdocumentitem type="footer"><cfoutput>Page #cfdocument.currentpagenumber# of #cfdocument.totalpagecount#</cfoutput></cfdocumentitem> --->
	<!--- <cfdocumentitem type="pagebreak"></cfdocumentitem> --->
</cfloop>

</cfdocument>
