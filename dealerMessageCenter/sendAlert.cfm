
<!--- Dealer Message Center Email Alert --->

<cfquery name="getsubject" datasource="#ds#">
	select subject from DealerMessageCenter_Notes 
	where noteid = #noteid# 
</cfquery>

<cfquery name="GetEmailAccess" datasource="#ds#" >
	SELECT    email
	FROM      DealerMessageCenter_notificationSettings
	WHERE	 (departmentID = #form.departmentID#) and (subject = '' or subject = '#trim(getsubject.subject)#')
</cfquery>

<cfoutput query="GetEmailAccess">
<cfmail from="system.info@copsmonitoring.com" to="#GetEmailAccess.email#" subject="Dealer Message Notification" username="system" password="V01c3">
This is to notify you of a Dealer Message that needs your attention:

You can view the details here: http://192.168.107.10/dealerMessageCenter/viewApprovalMessage.cfm?noteid=#noteid#
</cfmail>
</cfoutput>
