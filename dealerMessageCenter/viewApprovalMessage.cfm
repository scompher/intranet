<!--- 07/26/2011 : SW : Dealer Message Center --->
<cfparam name = "form.isRead" default = "">
<cfset setDateTimeApproved = createodbcdatetime(now())>

<cfif isDefined("form.btnApproval")>
	<cfquery name = "UpdateApproval" datasource = "#ds#">
		UPDATE DealerMessageCenter_Notes
		SET isApproved = #form.approved#,
			isRead = 1,
		<cfif not isDefined("form.alreadyApproved")>
			<cfif form.approved is 1>
				dateTimeApproved = #setDateTimeApproved#
			<cfelse>
				dateTimeApproved = NULL 
			</cfif>
		</cfif>
		WHERE noteid = #noteid#
	</cfquery>

	<cfif not isDefined("form.alreadyApproved")>
		<cfif form.approved is "1">
			<!---  Update Message to Dealer Diary --->
			<cfinvoke 
				 component="dealerMessageCenter.cfc.dealerMessageCenter"
				 method="addToDealerDiary"
				 returnvariable="addToDealerDiaryRet">
					<cfinvokeargument name="noteid" value="#noteid#"/>
					<cfinvokeargument name="departmentID" value="#form.employeeDepartmentID#"/>
			</cfinvoke>
			<!--- When dept is OPS, save into managers log --->
			<cfif form.employeeDepartmentID is 5>
				<cfinvoke 
					 component="dealerMessageCenter.cfc.dealerMessageCenter"
					 method="addToOpsManagersLog"
					 returnvariable="addToManagersLogRet">
						<cfinvokeargument name="noteid" value="#noteid#"/>
						<cfinvokeargument name="departmentID" value="#form.employeeDepartmentID#"/>
				</cfinvoke>
			</cfif>
		</cfif>
	</cfif>
	
	<cflocation url="index.cfm">
</cfif>

<cfif isDefined("form.btnExit")>
    <cfset url.noteid = form.noteid>
		<cflocation url="index.cfm?noteid=#url.noteid#">
</cfif>

<cfquery name="GetDealerMessage" datasource="#ds#" >
	SELECT  DealerMessageCenter_Notes.*, Admin_Users_Departments.departmentID, Admin_Users_Departments.department
	FROM    DealerMessageCenter_Notes LEFT JOIN
			Admin_Users_Departments ON DealerMessageCenter_Notes.employeeDepartmentID = Admin_Users_Departments.departmentID
	WHERE noteid = #noteid#
</cfquery>

<!--- if message is marked as unread, mark as read --->
<cfif GetDealerMessage.isRead is 0 or GetDealerMessage.isRead is 1>
	<cfset markRead = "1">
</cfif>

<link rel="stylesheet" type="text/css" href="/styles.css">

<div align="center" class="normal" >
<cfform method="post" action="viewApprovalMessage.cfm">
<cfinput type = "hidden" name="noteid" value="#url.noteid#"/>
<cfinput type = "hidden" name="employeeDepartmentID" value="#GetDealerMessage.employeeDepartmentID#"/>

<table width="650" border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td class="highlightbar"><b>Dealer Message Center - View Entry</b></td>
	</tr>
	<tr>
	<td class="greyrowbottomnopadding">
	<table border="0" cellpadding="5" cellspacing="0">
		<tr>
			<td width="140" nowrap="nowrap"><b>Dealer Number:</b></td>
		<cfoutput query="GetDealerMessage">
		<td width="134">
			<cfoutput>#GetDealerMessage.dealerNumber#</cfoutput>
		</td>
		</tr>
		<tr>	
			<td nowrap="nowrap"><b>Account Number:</b></td>
				<td>
					<cfif trim(GetDealerMessage.receiver) is not "" and trim(GetDealerMessage.location) is not "">
						<cfoutput>#GetDealerMessage.receiver#</cfoutput>
						 -
						 <cfoutput>#GetDealerMessage.location#</cfoutput>
					 </cfif>&nbsp;
				</td>
		</tr>
		<tr>
			<td nowrap="nowrap"><b>Subscriber Name:</b></td>
			<td>
				<cfoutput>#GetDealerMessage.subscriberName#</cfoutput>
			</td>
		</tr>
		<tr>
		<td nowrap="nowrap"><b>Employee Name:</b></td>
			<td>
				<cfoutput>#GetDealerMessage.employeeName#</cfoutput>
			</td>
		</tr>
		<tr>
		<td nowrap="nowrap"><b>Department:</b></td>
			<td>
				<cfoutput>#GetDealerMessage.department#</cfoutput>
			</td>
		</tr>
		<tr>	
			<td nowrap="nowrap"><b>Subject:</b></td>
			<td>
				<cfoutput>#GetDealerMessage.subject#</cfoutput>
			</td>
		</tr>
		<tr>
			<td colspan="2" nowrap="nowrap"><b>Message:</b></td>
		</tr>
		<tr>
			<td colspan="2">
			<cfoutput>#GetDealerMessage.notes#</cfoutput>
			</td>
		</tr>
		<tr>
			<td nowrap="nowrap"><b>Message Approved (Y/N):</b></td>
			<td width="99">
				<cfif trim(GetDealerMessage.isApproved) is 1>
					YES
					<cfinput type="hidden" name="alreadyApproved" value="1">
				<cfelse>	
					<cfselect enabled="No" name="approved" multiple="no">
						<option <cfif GetDealerMessage.isApproved is 0> selected </cfif> value="0">No</option>
						<option <cfif GetDealerMessage.isApproved is 1> selected </cfif> value="1">Yes</option>
					</cfselect>
				</cfif>
			</td>
		</tr>
		<tr>
			<td colspan="2">
			<cfinput type="submit" name="btnExit" id="Save Info" value="Exit View">&nbsp;
			<cfif trim(GetDealerMessage.isApproved) is 0>
				<cfinput type="submit" name="btnApproval" id="Save Approval" value="Update">
			</cfif>
			</td>
		</tr>
		
	</cfoutput>
	</table>	
	</td>
	</tr>
</table>
</cfform>

<br>
<a style="text-decoration:underline;" href="index.cfm">Return to Dealer Message List</a>
<br />
</div>
