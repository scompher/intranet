<!--- Dealer Message Center Email Maintenance Page --->
<link rel="stylesheet" type="text/css" href="/styles.css">

<cfquery name="GetEmail" datasource="#ds#" >
	SELECT DealerMessageCenter_notificationSettings.*, Admin_Users_Departments.department 
	from DealerMessageCenter_notificationSettings
	left join Admin_Users_Departments on DealerMessageCenter_notificationSettings.departmentID = Admin_Users_Departments.departmentid 
	Order by department 
</cfquery>

<div align="center" class="normal"> 
    <table width="500" border="0" cellpadding="5" cellspacing="0">
    <tr>
        <td class="highlightbar"><b>Dealer Message Center Email Maintenance</b> </td>
    </tr>
    <tr>
        <td class="greyrowbottomnopadding">	
		<table width="100%" border="0" cellpadding="5" cellspacing="0">
			<tr>		
                <td width="60" align="center" nowrap="nowrap" class="linedrowrightcolumn"><b>Action</b></td>
                <td width="170" nowrap="nowrap" class="linedrowrightcolumn"><b>Department</b></td>
				<td width="170" nowrap="nowrap" class="linedrowrightcolumn"><b>Subject</b></td>
                <td width="144" nowrap="nowrap" class="linedrow"><b>Email Address</b></td>
            </tr>
			<cfoutput query="GetEmail">
				<tr>
					<td align="center" class="linedrowrightcolumn">
					<a href="editEmail.cfm?notificationID=#GetEmail.notificationID#"><img border="0" src="/images/edit.gif" alt="Edit" width="16" height="16" />&nbsp;&nbsp;</a>
					<a onclick="return confirm('Are you sure you want to delete this?');" href="deleteEmail.cfm?notificationID=#GetEmail.notificationID#"><img border="0" src="/images/delete.gif" alt="Delete" width="16" height="16" /></a> 
					</td> 
					<td nowrap="nowrap" class="linedrowrightcolumn">#department#&nbsp;</td>
					<td nowrap="nowrap" class="linedrowrightcolumn">
					<cfif trim(subject) is "">All Subjects<cfelse>#subject#&nbsp;</cfif>
					</td>
					<td nowrap="nowrap" class="linedrow">#email#&nbsp;</td>
				</tr>
			</cfoutput>	
        </table>
		</td>
    </tr>
</table>

<cfform method="post" action="newEmail.cfm">
	<table width="500" border="0" cellspacing="5" cellpadding="0">
        <tr>
            <td><cfinput type="submit" name="btnNewEmail" id="Add New Entry" value="Add New Entry"></td>
        
		</tr>
    </table>	
</cfform>

<br>
<a style="text-decoration:underline;" href="/index.cfm">Return to Intranet</a>
</div>


