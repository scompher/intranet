<link rel="stylesheet" type="text/css" href="/styles.css">

<cfparam name = "empSelect" default = "false">
<cfparam name = "form.adminuserid" default = "0">
<cfparam name = "form.email" default = "">
<cfparam name = "form.departmentid" default = "0">
<cfparam name = "employeeEmail" default = "">
<cfparam name = "holdAdminuserid" default = "">

<cfquery name="getDepartments" datasource="#ds#">
	select * from admin_users_departments 
	order by department asc 
</cfquery>

<cfif isDefined("form.btnSelect")>
	<cfset empSelect = true>
</cfif>

<div align="center" class="normal">
<cfform method="post" action="emailAlert.cfm">
<table border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td class="highlightbar"><b>Dealer Message Center - Email Notification</b></td>
	</tr>
	<tr>
	<td class="greyrowbottomnopadding">
	<table border="0" cellpadding="5" cellspacing="0">
		<tr>
		<td width="98" nowrap="nowrap"><b>Employee Email:</b></td>
			<td width="172">
			<cfinput required ="yes" message="Email is required" type="text" maxlength="100" name="email" value="#form.email#" style="width:250px"></td>		
		</tr>
		<tr>
		<td width="98" nowrap="nowrap"><b>Department:</b></td>
			<td width="172">
				<cfselect enabled="No" name="departmentid" multiple="no">
                	<option value="0"></option>
					<cfoutput query="getDepartments">
						<option <cfif getDepartments.departmentid is form.departmentid> selected </cfif> value="#getDepartments.departmentid#">#department#</option>
					</cfoutput>
				</cfselect>
			</td>
		</tr>
		<tr>
			<td colspan="2">
			<cfinput type="submit" name="btnSave" id="Save Email" value="Save Email Selection"> &nbsp;
			</td>
		</tr>
	</table>	
	</td>
	</tr>
</table>
</cfform>
</div>




