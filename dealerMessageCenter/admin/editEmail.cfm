<!--- Dealer Message Center Edit Email page --->
<cfquery name="GetEmail" datasource="#ds#" >
	SELECT * 
	FROM DealerMessageCenter_notificationSettings
	WHERE notificationID = #url.notificationID#
</cfquery>

<cfquery name="getDepartments" datasource="#ds#">
	select * from admin_users_departments 
	order by department asc 
</cfquery>

<link rel="stylesheet" type="text/css" href="/styles.css">

<div align="center" class="normal">

<cfform id="form1" name="form1" method="post" action="editEmail_save.cfm">
<cfinput type = "hidden" name="notificationID" value="#url.notificationID#" />
<table border="0" cellspacing="0" cellpadding="5">
    <tr>
        <td colspan="2" class="highlightbar"><b>Email Maintenance Change Entry</b></td>
    </tr>
	<tr>
		<td class="greyrowbottomnopadding">
	<table border="0" cellpadding="5" cellspacing="0">
	<cfoutput query="GetEmail">
		<tr>
			<td nowrap="nowrap"><b>Email Address:</b></td>
			<td width="275"><cfinput name="email" type="text" value="#GetEmail.email#" required="Yes" validate="email" message="You must enter a valid e-mail address." style="width:300px" maxlength="255"/></td>
		</tr>
		<tr>
			<td nowrap="nowrap"><b>Subject:</b></td>
				<td>
					<cfselect name="subject" multiple="yes" size="5">
				    <option <cfif GetEmail.subject is ""> selected </cfif> value="">All Subjects</option>
					<option <cfif GetEmail.subject is "Alarm Handling"> selected </cfif> value="Alarm Handling">Alarm Handling</option>
					<option <cfif GetEmail.subject is "Data Support"> selected </cfif> value="Data Support">Data Support</option>
					<option <cfif GetEmail.subject is "Dealer Action Required"> selected </cfif> value="Dealer Action Required">Dealer Action Required</option>
					<option <cfif GetEmail.subject is "Subscriber Service Request"> selected </cfif> value="Subscriber Service Request">Subscriber Service Request</option>
					</cfselect>
				</td>
		</tr>
	</cfoutput>
		<tr>
			<td nowrap="nowrap"><b>Department:</b></td>
			<td><select name="departmentID">
					<option value="0"></option>
					<cfoutput query="getDepartments">
						<option <cfif getDepartments.departmentID is GetEmail.departmentID>selected</cfif> value="#departmentID#">#department#</option>
					</cfoutput>
				</select></td>
		</tr>
		<tr>
		<td colspan="2">
			<cfinput type="submit" name="btnSave" id="Save" value="Save Change"> &nbsp;
			<cfinput type="button" onClick="document.location='index.cfm';" name="btnCancel" value="Cancel">
		</td> 
		</tr> 
		
	 </table>
	 </td>
    </tr>
</table>

</cfform>
 <p><br />
        <a href="javascript:history.go(-1);">Return to Previous Page</a> </p>
</div>
