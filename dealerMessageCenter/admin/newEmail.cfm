<!--- Dealer Message Center New Email page --->
<link rel="stylesheet" type="text/css" href="/styles.css">
<cfparam name = "form.email" default = "">
<cfparam name = "form.departmentID" default = "0">
<cfparam name = "form.subject" default="">

<div align="center" class="normal">

<cfif isDefined("form.btnSave")>
	<cfinclude template="newEmail_save.cfm">
</cfif>

<cfquery name="getDepartments" datasource="#ds#">
	select * from admin_users_departments 
	order by department asc 
</cfquery>

<cfform id="form1" name="form1" method="post" action="newEmail.cfm">
<table border="0" cellspacing="0" cellpadding="5">
    <tr>
        <td colspan="2" class="highlightbar"><b>Dealer Message Center Email Maintenance Add New Entry </b></td>
    </tr>
	<tr>
	
	<td class="greyrowbottomnopadding">
	<table border="0" cellpadding="5" cellspacing="0">
		<tr>
			<td nowrap="nowrap"><b>Email Address:</b></td>
			<td><cfinput type="text" name="email" validate="email" value="#form.email#" required="Yes" class="normal" id="email" style="width:300px" maxlength="255" message="You must enter a valid e-mail address."></td>
		</tr>
		<tr>
			<td nowrap="nowrap"><b>Subject:</b></td>
			<td>
			    <cfselect name="subject" multiple="yes" size="5">
					<option <cfif form.subject is ""> selected </cfif> value="">All Subjects</option>
					<option <cfif form.subject is "Alarm Handling"> selected </cfif> value="Alarm Handling">Alarm Handling</option>
					<option <cfif form.subject is "Data Support"> selected </cfif> value="Data Support">Data Support</option>
					<option <cfif form.subject is "Dealer Action Required"> selected </cfif> value="Dealer Action Required">Dealer Action Required</option>
					<option <cfif form.subject is "Subscriber Service Request"> selected </cfif> value="Subscriber Service Request">Subscriber Service Request</option>
                </cfselect>
			</td>
		</tr>
		<tr>
			<td nowrap="nowrap"><b>Department:</b></td>
			<td><select name="departmentID">
					<option value=""></option>
					<cfoutput query="getDepartments">
						<option <cfif getDepartments.department is form.departmentID>selected</cfif> value="#departmentID#">#department#</option>
					</cfoutput>
				</select></td>
		</tr>
		<tr>
		<td colspan="2">
			<cfinput type="submit" name="btnSave" id="Save" value="Save"> &nbsp;
			<cfinput type="button" onClick="document.location='index.cfm';" name="btnCancel" value="Cancel">
		</td> 
		</tr> 
	 </table>
	 </td>
    </tr>

</table>

</cfform>
 <p><br />
 <a href="index.cfm" style="text-decoration:underline">Return to Previous Page</a> </p>
</div>