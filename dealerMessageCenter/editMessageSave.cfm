<cfset setDateTimeApproved = createodbcdatetime(now())>

<cfset form.message = REReplace(form.message,'<[^>]*>','','all')>
<cfset form.message = Replacenocase(form.message,'&nbsp;','','all')>

<cfquery name = "UpdateDb" datasource = "#ds#">
	UPDATE DealerMessageCenter_Notes
	SET receiver = '#form.rec#',
		location = '#form.loc#',
		subscriberName = '#form.subscriber#',
		subject = '#form.subject#',
		notes = '#form.message#',
		employeeName = '#form.employeeName#',
		employeeDepartmentID = '#form.departmentID#',
		<cfif markRead is "1">
			isRead = 1  
		<cfelse>
			isRead = 0  
		</cfif>
		<cfif not isDefined("form.alreadyApproved")>
			<cfif markApproved is "1">
				, isApproved = 1,
				dateTimeApproved = #setDateTimeApproved#
			<cfelse>
				, isApproved = 0,
				dateTimeApproved = NULL
			</cfif>
		</cfif>
	WHERE noteid = #form.noteid#
</cfquery>

<cfif not isDefined("form.alreadyApproved")>
	<cfif markApproved is "1">
		<!---  Update Message to Dealer Diary --->
		<cfinvoke 
			 component="dealerMessageCenter.cfc.dealerMessageCenter"
			 method="addToDealerDiary"
			 returnvariable="addToDealerDiaryRet">
				<cfinvokeargument name="noteid" value="#noteid#"/>
				<cfinvokeargument name="departmentID" value="#departmentID#"/>
		</cfinvoke>
		<!--- When Dept is OPS (5) save into managers log --->
		<cfif form.departmentID is 5>
			<cfinvoke 
				 component="dealerMessageCenter.cfc.dealerMessageCenter"
				 method="addToOpsManagersLog"
				 returnvariable="addToManagersLogRet">
					<cfinvokeargument name="noteid" value="#noteid#"/>
					<cfinvokeargument name="departmentID" value="#departmentID#"/>
			</cfinvoke>
		</cfif>
	</cfif>
</cfif>

<!--- Return back to Main page--->
<cflocation url="index.cfm">

 						
	