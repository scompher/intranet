<!--- 07/8/2011 : SW : Dealer Message Center --->

<link rel="stylesheet" type="text/css" href="/styles.css">
<cfparam name = "form.rec" default = "">
<cfparam name = "form.loc" default = "">
<cfparam name = "form.dealer" default = "">
<cfparam name = "form.subscriber" default = "">
<cfparam name = "form.subject" default = "">
<cfparam name = "form.message" default = "">
<cfparam name = "form.employeeName" default = "">
<cfparam name = "cal.access" default = "">

<cfif isDefined("form.btnSubmit")>
	<!--- <cfset url.logid = form.logid>--->
		<cfinclude template="saveMessage.cfm">
</cfif>

<div align="center" class="normal" >
<cfform method="post" action="newMessage.cfm">
<table border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td class="highlightbar"><b>Dealer Message Center - New Entry</b></td>
	</tr>
	<tr>
	<td class="greyrowbottomnopadding">
	<table border="0" cellpadding="5" cellspacing="0">
		<tr>
			<td width="138" nowrap="nowrap"><b>Dealer Number:</b></td>
			<td width="50"> 
				<cfinput required ="yes" message="Dealer Number is required" type="text" name="dealer" value="#form.dealer#" style="width:50px"></td>
		</tr>	
	<!---	<cfif cal.access = "y">--->
	
		<tr>	
			<td nowrap="nowrap"><b>Account Number: </b></td>
				<td>
					<cfinput required ="yes" message="Receiver is required" type="text" name="rec" value="#form.rec#" style="width:50px">
                     -
                   	<cfinput required ="yes" message="Location is required" type="text" name="loc" id="loc" value="#form.loc#" style="width:50px">
				</td>
		</tr>
		<tr>
		<td nowrap="nowrap"><b>Employee Name:</b></td>
			<td>
			    <cfinput type="text" name="employeeName" id="employeeName" value="#form.employeeName#">
			</td>
		</tr>
		<tr>
			<td nowrap="nowrap"><b>Subscriber Name:</b></td>
			<td>
			    <cfinput required ="yes" message="Subscriber is required" type="text" name="subscriber" id="subscriber" value="#form.subscriber#" style="width:300px">
			</td>
		</tr>
		
		<tr>	
			<td nowrap="nowrap"><b>Subject:</b></td>
			<td>
			    <cfselect enabled="No" name="subject" multiple="no">
                <option <cfif form.subject is ""> selected </cfif> value=""></option>
                <option <cfif form.subject is "Alarm Handling"> selected </cfif> value="Alarm Handling">Alarm Handling</option>
                <option <cfif form.subject is "Data Support"> selected </cfif> value="Data Support">Data Support</option>
                <option <cfif form.subject is "Dealer Action Required"> selected </cfif> value="Dealer Action Required">Dealer Action Required</option>
                <option <cfif form.subject is "Subscriber Service Request"> selected </cfif> value="Subscriber Service Request">Subscriber Service Request</option>
                </cfselect>
			</td>
		</tr>
		<tr>
			<td colspan="2" nowrap="nowrap"><b>Message:</b></td>
		</tr>
		<tr>
			<td colspan="2">
			    <cftextarea name="message" value="#form.message#" rows="5" required="yes" message="Message is required." id="message" style="width:500px"  enabled="no" visable="no"></cftextarea>
			</td>
		</tr>
		<!---</cfif>--->
		<tr>
			<td colspan="2">
			<cfinput type="submit" name="btnSubmit" id="Save Info" value="Submit Form"> &nbsp;
			<cfinput type="button" onClick="document.location='newMessage.cfm';" name="Clear" value="Clear Form">
			</td>
		</tr>
		
	</table>	
	</td>
	</tr>
</table>
</cfform>
</div>

