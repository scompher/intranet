<!--- 07/8/2011 : SW : Dealer Message Center --->

<link rel="stylesheet" type="text/css" href="/styles.css">
<cfparam name = "form.rec" default = "">
<cfparam name = "form.loc" default = "">
<cfparam name = "form.dealer" default = "">
<cfparam name = "form.subscriber" default = "">
<cfparam name = "form.subject" default = "">
<cfparam name = "form.message" default = "">
<cfparam name = "form.employeeName" default = "">
<cfparam name = "calAccess" default = "">
<cfparam name="form.departmentID" default="0">
<cfset err="">

<cfif isDefined("form.btnCheckCAL")>
	<cfinvoke
			 component="dealerMessageCenter.cfc.calAccess"
			 method="getCalAccess"
			 returnvariable="calAccess">
			 <cfinvokeargument name="ds" value="#ds#"/>
			 <cfinvokeargument name="dealerNumber" value="#form.dealer#"/>
	</cfinvoke>
</cfif>

<!---if Clear button is entered, erase data --->
<cfif isDefined("form.btnClear")>
	<cfset form.rec = "">
	<cfset form.loc = "">
	<cfset form.subscriber = "">
	<cfset form.subject = "">
	<cfset form.message = "">
	<cfset form.employeeName = "">	
	<cfset calAccess = true>
</cfif>

<cfif isDefined("form.btnSubmit")>
<!--- ADD VALIDATION FOR DEPT AND SUBJECT--->
		<cfif trim(form.departmentID is 0 or form.departmentID is "") and not isDefined("cookie.adminlogin")>
			<cfset err = listappend(err,"<b>Department is required.</b>","|")>
		</cfif>	
		<cfif trim(form.subject) is "">
		  <cfset err = listappend(err,"<b>Subject is required.</b>", "|")>
		</cfif>
	
		<cfif #err# is not "">
			<cfloop list = "#err#" index = "message" delimiters = "|"> 
				<cfoutput>
				<p class="alert" align="center">#message#</p> 
				</cfoutput>
			</cfloop>
		<cfelse>
			<cfinclude template="saveMessage.cfm">	
		</cfif>
</cfif>

<cfif calAccess is false>
	<p class="alert" align="center"><b>Dealer </b><cfoutput>#form.dealer#</cfoutput><b> does not have Cop-A-Link Access.</b></p>
</cfif>

<cfquery name="getDepartments" datasource="#ds#">
	select * from admin_users_departments 
	order by department asc 
</cfquery>

<div align="center" class="normal" >
<cfform method="post" action="newMessage.cfm">
<cfinput type="hidden" name="calAccess" value="#calAccess#">
<table border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td class="highlightbar"><b>Dealer Message Center - New Entry</b></td>
	</tr>
	<tr>
	<td class="greyrowbottomnopadding">
	<table border="0" cellpadding="5" cellspacing="0">
	<cfif calAccess is false or calAccess is "">
		<tr>
			<td width="138" nowrap="nowrap"><b>Dealer Number:</b></td>
			<td width="50"> 
				<cfinput required ="yes" message="Dealer Number is required" type="text" maxlength="4" name="dealer" value="#form.dealer#" style="width:50px"></td>
		</tr>
	<cfelse>
		<cfinput type = "hidden" name="dealer" value="#form.dealer#">		
	</cfif>	
	<cfif calAccess is true>
	<tr>
			<td width="138" nowrap="nowrap"><b>Dealer Number:</b></td>
			<td width="50"> 
				<cfoutput>#form.dealer#</cfoutput></td>
		</tr>
		<tr>	
			<td nowrap="nowrap"><b>Account Number: </b></td>
				<td>
					<cfinput required ="yes" message="Receiver is required" type="text" maxlength="5" name="rec" value="#form.rec#" style="width:50px">
                     -   
					<cfinput validate="integer" required ="yes" message="Location is required and must be numeric" type="text" maxlength="7" name="loc" id="loc" value="#form.loc#" style="width:50px">
				</td>
		</tr>
		<cfif not isDefined("cookie.adminLogin")>
			<tr>
			<td nowrap="nowrap"><b>Employee Name:</b></td>
				<td>
					<cfinput required="yes" message="Employee Name is required" type="text" name="employeeName" id="employeeName" value="#form.employeeName#">
				</td>
			</tr>
			<tr>
			<td nowrap="nowrap"><b>Employee Department:</b></td>
				<td><cfselect name="departmentID" required="yes" message="Department is required" >
					<option value=""></option>
					<cfoutput query="getDepartments">
						<option <cfif getDepartments.departmentID is form.departmentID>selected</cfif> value="#getDepartments.departmentID#">#getDepartments.department#</option>
					</cfoutput>
				</cfselect></td>
			</tr>
		</cfif>
		<tr>
			<td nowrap="nowrap"><b>Subscriber Name:</b></td>
			<td>
			    <cfinput required="yes" message="Subscriber is required" type="text" name="subscriber" id="subscriber" value="#form.subscriber#" style="width:300px">
			</td>
		</tr>
		<tr>
			<td nowrap="nowrap"><b>Subject:</b></td>
			<td>
			    <cfselect name="subject" multiple="no" required="yes" message="Subject is required">
					<option <cfif form.subject is ""> selected </cfif> value=""></option>
					<option <cfif form.subject is "Alarm Handling"> selected </cfif> value="Alarm Handling">Alarm Handling</option>
					<option <cfif form.subject is "Data Support"> selected </cfif> value="Data Support">Data Support</option>
					<option <cfif form.subject is "Dealer Action Required"> selected </cfif> value="Dealer Action Required">Dealer Action Required</option>
					<option <cfif form.subject is "Subscriber Service Request"> selected </cfif> value="Subscriber Service Request">Subscriber Service Request</option>
                </cfselect>
			</td>
		</tr>
		<tr>
			<td colspan="2" nowrap="nowrap"><b>Message:</b></td>
		</tr>
		<tr>
			<td colspan="2">
			    <cftextarea name="message" value="#form.message#" rows="5" required="yes" message="Message is required." id="message" style="width:500px"  enabled="no" visable="no"></cftextarea>
			</td>
		</tr>
		<tr>
			<td colspan="2">
			<cfinput type="submit" name="btnSubmit" id="Save Info" value="Submit Form"> &nbsp;
			<cfinput type="button" onClick="document.location='/index.cfm';" name="btnClear" id="Clear Form" value="Cancel Message" >
			</td>
		</tr>
	<cfelse>
		<tr>
			<td colspan="2">
			<cfinput type="submit" name="btnCheckCAL" id="Check CAL" value="Check Cop-A-Link Access"> &nbsp;
			</td>
		</tr>
	</cfif>		
	</table>	
	</td>
	</tr>
</table>
</cfform>
<br />
<a style="text-decoration:underline;" class="normal" href="/index.cfm">Return to Intranet</a>
</div>
