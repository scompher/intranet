
<cfquery name="GetDealerMessage" datasource="#ds#" >
	<!---SELECT * from DealerMessageCenter_Notes
	WHERE noteid = #noteid# --->
	SELECT  DealerMessageCenter_Notes.*, Admin_Users_Departments.departmentID, Admin_Users_Departments.department
	FROM    DealerMessageCenter_Notes LEFT JOIN
			Admin_Users_Departments ON DealerMessageCenter_Notes.employeeDepartmentID = Admin_Users_Departments.departmentID
	WHERE noteid = #noteid#
</cfquery>

<cfquery name="getDepartments" datasource="#ds#">
	select * from admin_users_departments 
	order by department asc 
</cfquery>


<link rel="stylesheet" type="text/css" href="/styles.css">

<div align="center" class="normal" >
<cfform method="post" action="viewMessage.cfm">
<cfinput type = "hidden" name="noteid" value="#url.noteid#"/>
<table width="550" border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td class="highlightbar"><b>Dealer Message Center - View Entry</b></td>
	</tr>
	<tr>
	<td class="greyrowbottomnopadding">
	<table border="0" cellpadding="5" cellspacing="0">
		
			<td nowrap="nowrap"><b>Dealer Number:</b></td>
		<cfoutput query="GetDealerMessage">
		<td>
			<cfoutput>#GetDealerMessage.dealerNumber#</cfoutput>
		</td>
		
		<tr>	
			<td nowrap="nowrap"><b>Account Number:</b></td>
				<td>
					<cfif trim(GetDealerMessage.receiver) is not "" and trim(GetDealerMessage.location) is not "">
						<cfoutput>#GetDealerMessage.receiver#</cfoutput>
						 -
						 <cfoutput>#GetDealerMessage.location#</cfoutput>
					 </cfif>&nbsp;
				</td>
		</tr>
		<tr>
			<td nowrap="nowrap"><b>Subscriber Name:</b></td>
			<td>
				<cfoutput>#GetDealerMessage.subscriberName#</cfoutput>
			</td>
		</tr>
		<tr>
		<cfif GetDealerMessage.employeeName is not "">
		<td nowrap="nowrap"><b>Employee Name:</b></td>
			<td>
				<cfoutput>#GetDealerMessage.employeeName#</cfoutput>
			</td>
		</tr>
		<tr>
		<td nowrap="nowrap"><b>Department:</b></td>
			<td>
				<cfoutput>#GetDealerMessage.department#</cfoutput>
			</td>
		</tr>
		</cfif>
		<tr>	
			<td nowrap="nowrap"><b>Subject:</b></td>
			<td>
				<cfoutput>#GetDealerMessage.subject#</cfoutput>
			</td>
		</tr>
		<tr>
			<td colspan="2" nowrap="nowrap"><b>Message:</b></td>
		</tr>
		<tr>
			<td colspan="2">
			<cfoutput>#GetDealerMessage.notes#</cfoutput>
			</td>
		</tr>
	</cfoutput>
	</table>	
	</td>
	</tr>
</table>
</cfform>

</div>
