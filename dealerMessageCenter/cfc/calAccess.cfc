<cfcomponent>
<!--- Check for Copalink Access - verify Dealer --->
<cffunction name="getCalAccess" access="remote" returntype="boolean">
    <cfargument name="dealerNumber" type="string" required="yes">
    <!--- get data from PICK --->

    <cfxml variable="xmlRequest">
    <cfoutput>
        <request>
			<dealerNumber>#dealerNumber#</dealerNumber>
            <command>checkCalAccess</command>
        </request>
    </cfoutput>
    </cfxml>
    <!---Request is data sent from CAL --->
    <cf_copalink command="#xmlRequest#">
    <cfset xResult = xmlparse(result)>
    <!--- Display error messages or Data Returned from PICK --->
    <cfif xresult.response.result.xmlText is "RJ">
		<cfreturn false>	
	<cfelse>
		<cfif xresult.response.data.checkCalAccess.calAccess.xmlText is "YES">
			<cfreturn true>
		<cfelse>
			<cfreturn false>
		</cfif>
    </cfif>
	
</cffunction>
</cfcomponent>
