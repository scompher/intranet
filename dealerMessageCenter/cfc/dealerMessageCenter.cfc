<cfcomponent>
	<cffunction name="addToDealerDiary" access="public" returntype="string">
		<cfargument name="noteid" type="numeric" required="yes">
		<cfargument name="departmentID" type="numeric" required="yes">
		
		<cfif isDefined("cookie.adminlogin")>
			<cfset creatorID = cookie.adminlogin>
		<cfelse>
			<cfset creatorID = 0>
		</cfif>
		
		<!--- add to dealer diary notes --->
		<cfquery name="getNote" datasource="intranet">
			select * from DealerMessageCenter_Notes where noteid = #noteid#
		</cfquery>
		<cfset subject = "A Dealer Message Center Note for account #getnote.receiver#-#getnote.location# was created on #dateformat(getNote.noteDateTime,'mm/dd/yyyy')#">
		<cfset noteText = "<a href=""javascript:viewPrintWin(''http://192.168.107.10/dealerMessageCenter/viewMessage.cfm?noteid=#noteid#'');"">View Full Note</a>#chr(13)##chr(13)##getNote.notes#">
		<cfquery name="insertIntoDiary" datasource="intranet">
			insert into dealerdiary_entries (creatorID, dealernumber, created, content, subject, departmentid, parentid, inquiryNote, inquiryID)
			values (#creatorID#, '#getNote.dealerNumber#', #createodbcdatetime(getNote.noteDateTime)#, '#noteText#', '#subject#', #departmentID#, 0, 1, 0)
		</cfquery>

		<cfreturn "">
	</cffunction>

	<cffunction name="addToOpsManagersLog" access="public" returntype="string">
		<cfargument name="noteid" type="numeric" required="yes">
		
		<cfif isDefined("cookie.adminlogin")>
			<cfset creatorID = cookie.adminlogin>
		<cfelse>
			<cfset creatorID = 0>
		</cfif>
		
		<!--- add to ops managers log notes --->
		<cfquery name="getNote" datasource="intranet">
			select * from DealerMessageCenter_Notes where noteid = #noteid#
		</cfquery>
		<cfquery name="getCreator" datasource="intranet">
			select * from admin_users 
			where adminuserid = #creatorID# 
		</cfquery>
		
		<cfset creatorname = "">
		<cfif getCreator.recordcount is 1>
			<cfset creatorname = "and approved by #getCreator.firstname# #getCreator.lastname#">
		</cfif>

		<cfset form.otherSubject = "A Dealer Message Center Note for account #getnote.receiver#-#getnote.location# was created on #dateformat(getNote.noteDateTime,'mm/dd/yyyy')# #creatorname#">
		<cfset form.note = "<a target=""_blank"" href=""http://192.168.107.10/dealerMessageCenter/viewMessage.cfm?noteid=#noteid#"">View Full Note</a>#chr(13)##chr(13)##getNote.notes#">
		<cfquery name = "InsertDb" datasource = "intranet">
			INSERT INTO managerLog_logEntries(receiver,location,dateTimeEntered,adminUserID,subject,otherSubject,note,firstname,lastname)
			VALUES ('#getNote.receiver#',
					'#getNote.location#',
					#createodbcdatetime(getNote.noteDateTime)#,
					'#creatorID#', 
					'',
					'#form.otherSubject#',
					'#form.note#',
					'#getCreator.firstname#',
					'#getCreator.lastname#')
		</cfquery>


		<cfreturn "">
	</cffunction>
</cfcomponent>


