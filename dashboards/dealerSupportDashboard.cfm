
<link rel="stylesheet" type="text/css" href="/styles.css">

<cfparam name="departmentid" default="3">

<cfset confessionalList = "39,62,70,135,145">
<cfset hideImages = true>
<!--- 
<cfif listfindnocase(confessionalList,cookie.adminlogin) is not 0>
	<cfinclude template="included/dashboardConfessional.cfm">
</cfif>
--->

<style type="text/css">
<!--
.red {
	color: #FF0000;
	font-weight: bold;
}
-->
</style>

<cfquery name="getInqs" datasource="#ds#">
	select 
	inquiries_main.dateTimeCreated, 
	inquiries_main.inquiryID, 
	inquiries_main.inquiryNumber, 
	inquiries_main.dateTimeOverdue, 
	inquiries_status_history.statusid 
	from inquiries_main with (nolock) 
	left join inquiries_status_history on inquiries_main.inquiryid = inquiries_status_history.inquiryid 
	where 
	inquiries_status_history.isCurrentStatus = 1 and inquiries_main.active = 1 and 
	inquiries_status_history.toDepartmentID IN (3) and 
	inquiries_status_history.statusid NOT IN (4,5) and 
	1=1 
</cfquery>

<cfquery name="countNewInqs" datasource="#ds#">
	select count(inquiries_main.inquiryid) as newCount 
	from inquiries_main with (nolock) 
	left join inquiries_status_history on inquiries_main.inquiryid = inquiries_status_history.inquiryid 
	where 
	inquiries_status_history.isCurrentStatus = 1 and inquiries_main.active = 1 and 
	inquiries_status_history.toDepartmentID IN (3) and 
	inquiries_status_history.statusid NOT IN (4,5) and 
	inquiries_status_history.statusid = 1 
</cfquery>

<cfquery name="countOpenInqs" datasource="#ds#">
	select count(inquiries_main.inquiryid) as openCount 
	from inquiries_main with (nolock) 
	left join inquiries_status_history on inquiries_main.inquiryid = inquiries_status_history.inquiryid 
	where 
	inquiries_status_history.isCurrentStatus = 1 and inquiries_main.active = 1 and 
	inquiries_status_history.toDepartmentID IN (3) and 
	inquiries_status_history.statusid NOT IN (4,5) and 
	inquiries_status_history.statusid IN (2,6) 
</cfquery>

<cfquery name="countOverDueInqs" datasource="#ds#">
	select count(inquiries_main.inquiryid) as overdueCount  
	from inquiries_main with (nolock) 
	left join inquiries_status_history on inquiries_main.inquiryid = inquiries_status_history.inquiryid 
	where 
	inquiries_status_history.isCurrentStatus = 1 and inquiries_main.active = 1 and 
	inquiries_status_history.toDepartmentID IN (3) and 
	inquiries_status_history.statusid NOT IN (4,5) and 
	inquiries_main.dateTimeOverdue <= #createodbcdatetime(now())#
</cfquery>

<cfquery name="countNewGCR" datasource="#ds#">
	select count(changeRequestID) as newCount 
	from globalChangeRequest with (nolock) 
	where currentStatus = 'Unread' 
</cfquery>

<cfquery name="countOpenedGCR" datasource="#ds#">
	select count(changeRequestID) as openedCount 
	from globalChangeRequest with (nolock) 
	where currentStatus = 'Opened' 
</cfquery>

<cfset dmcOverdue = dateadd("h",-48,now())>

<cfquery name="dmcGetTotal" datasource="#ds#">
	select distinct count(noteid) as totalCount from DealerMessageCenter_Notes where copsDeleted = 0
</cfquery>

<cfquery name="dmcGetOverDue" datasource="#ds#">
	select distinct count(noteid) as overDueCount from DealerMessageCenter_Notes where noteDateTime <= #createodbcdatetime(dmcOverdue)# and isRead = 0 and copsDeleted = 0
</cfquery>

<cfquery name="dmcGetUnread" datasource="#ds#">
	select distinct count(noteid) as unReadCount from DealerMessageCenter_Notes where isRead = 0 and copsDeleted = 0
</cfquery>

<cfquery name="dmcGetPendingApproval" datasource="#ds#">
	select distinct count(noteid) as pendingCount from DealerMessageCenter_Notes where isRead = 1 and isApproved = 0 and copsDeleted = 0 
</cfquery>

<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Dealer Support Dashboard</b></td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td><b>Dealer Support Inquiries </b> <a href="/inquiry/index.cfm?deptlist=3">(click here to jump to Inquiries)</a> </td>
				</tr>
				<cfoutput>
				<tr>
					<td class="nopadding">
						<table border="0" cellspacing="0" cellpadding="5">
							<tr>
								<td>Total: #getInqs.recordCount#</td>
								<td width="100">&nbsp;</td>
								<td>New: <cfif trim(countNewInqs.newCount) is "">0<cfelse>#countNewInqs.newCount#</cfif></td>
								<td width="100">&nbsp;</td>
								<td>Open: <cfif trim(countOpenInqs.openCount) is "">0<cfelse>#countOpenInqs.openCount#</cfif></td>
								<td width="100">&nbsp;</td>
								<td>Overdue: <cfif trim(countOverDueInqs.overdueCount) is "" or trim(countOverDueInqs.overdueCount) is 0>0<cfelse><span class="red">#countOverDueInqs.overdueCount#</span></cfif></td>
							</tr>
						</table>
					</td>
				</tr>
				</cfoutput>
				<tr>
					<td><b>Global Change Requests</b> <a href="/globalChange/">(click here to jump to global change requests)</a> </td>
				</tr>
				<cfoutput>
				<tr>
					<td class="nopadding">
						<table border="0" cellspacing="0" cellpadding="5">
							<tr>
								<td>New: <cfif trim(countNewGCR.newCount) is "">0<cfelse>#countNewGCR.newCount#</cfif></td>
								<td width="100">&nbsp;</td>
								<td>Open: <cfif trim(countOpenedGCR.openedCount) is "">0<cfelse>#countOpenedGCR.openedCount#</cfif></td>
							</tr>
						</table>
					</td>
				</tr>
				</cfoutput>
				<tr>
					<td><b>Dealer Message Center Messages </b> <a href="/dealerMessageCenter/index.cfm">(click here to jump to Dealer Message Center)</a> </td>
				</tr>
				<cfoutput>
				<tr>
					<td class="nopadding">
						<table border="0" cellspacing="0" cellpadding="5">
							<tr>
								<!---
								<td>Total: #dmcGetTotal.totalCount#</td>
								<td width="100">&nbsp;</td>
								--->
								<td>New: #dmcGetUnread.unReadCount#</td>
								<td width="100">&nbsp;</td>
								<td>Pending Approval: #dmcGetPendingApproval.pendingCount#</td>
								<td width="100">&nbsp;</td>
								<td>Overdue: <cfif trim(dmcGetOverDue.overdueCount) is "" or trim(dmcGetOverDue.overdueCount) is 0>0<cfelse><span class="red">#dmcGetOverDue.overdueCount#</span></cfif></td>
							</tr>
						</table>
					</td>
				</tr>
				</cfoutput>
			</table>
		</td>
	</tr>
</table>
<br />

<script type="text/javascript">
function checkUncheck(frm) {
	if (frm.checkStatus.value == 1) {
		var checkStatus = false;
		frm.checkStatus.value = 0;
	} else {
		var checkStatus = true;
		frm.checkStatus.value = 1;
	}
	if (frm.requestid.length > 1) {
		for (var i=0; i < frm.requestid.length; i++) {
			frm.requestid[i].checked = checkStatus;
		}
	} else {
		frm.requestid.checked = checkStatus;
	}
}
</script>

<cfparam name="SearchCreatedBy" default="">

<cfparam name="sort" default="dateDesc">

<cfswitch expression="#sort#">
	<cfcase value="createdByAsc"><cfset orderby = "createdBy ASC, requestDate ASC"></cfcase>
	<cfcase value="createdByDesc"><cfset orderby = "createdBy DESC, requestDate ASC"></cfcase>
	<cfcase value="dealerNumAsc"><cfset orderby = "dealerNumber ASC, requestDate ASC"></cfcase>
	<cfcase value="dealerNumDesc"><cfset orderby = "dealerNumber DESC, requestDate ASC"></cfcase>
	<cfcase value="dealerNameAsc"><cfset orderby = "dealerName ASC, requestDate ASC"></cfcase>
	<cfcase value="dealerNameDesc"><cfset orderby = "dealerName DESC, requestDate ASC"></cfcase>
	<cfcase value="dateAsc"><cfset orderby = "requestDate ASC"></cfcase>
	<cfcase value="dateDesc"><cfset orderby = "requestDate DESC"></cfcase>
	<cfcase value="requestAsc"><cfset orderby = "requestTypeName ASC, requestDate ASC"></cfcase>
	<cfcase value="requestDesc"><cfset orderby = "requestTypeName DESC, requestDate ASC"></cfcase>
	<cfcase value="statusAsc"><cfset orderby = "requestStatusName ASC, requestDate ASC"></cfcase>
	<cfcase value="statusDesc"><cfset orderby = "requestStatusName DESC, requestDate ASC"></cfcase>
</cfswitch>

<cfquery name="getMyName" datasource="#ds#">
	select firstname + ' ' + lastname as myName
	from admin_users 
	where adminuserid = #cookie.adminlogin# 
</cfquery>
<cfset myName = getMyName.myName>

<cfif searchCreatedBy is "">
	<cfset searchCreatedBy = myName>
</cfif>

<cfif isDefined("form.btnUpdateItems")>
	<cfinclude template="/formTracker/updateItems.cfm">
</cfif>
<cfquery name="getRequestStatus" datasource="#ds#">
	select * from requestTracking_requestStatus 
	order by displayOrder asc 
</cfquery>
<cfquery name="getRequests" datasource="#ds#">
	select requestTracking_requests.*, requestTracking_requestStatus.requestStatusName, requestTracking_requestTypes.requestTypeName 
	from requestTracking_requests 
	left join requestTracking_requestStatus on requestTracking_requests.requestStatus = requestTracking_requestStatus.requestStatusID 
	left join requestTracking_requestTypes on requestTracking_requests.requestType = requestTracking_requestTypes.requestTypeID 
	where 
	requestTracking_requests.departmentid = #departmentid# and 
	requestStatus IN (1,2) and 
	<cfif searchCreatedBy is not "All">
		createdby = '#SearchCreatedBy#' and 
	</cfif>
	1=1 
	order by #orderby#
</cfquery>

<cfquery name="getCreatedBy" datasource="#ds#">
	select distinct createdBy 
	from requestTracking_requests 
	where requestStatus IN (1,2) and departmentid = #departmentid# 
	order by createdBy asc 
</cfquery>

<cfset urlString = "searchCreatedBy=#searchCreatedBy#">

<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Dealer Support Request Tracker</b></td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
		<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<form method="post" action="index.cfm" name="outputForm">
			<input type="hidden" name="checkStatus" value="0" />
			<tr>
				<td colspan="8" class="nopadding">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td colspan="3" class="linedrow"><a href="/formTracker/" style="text-decoration:underline;" class="normal">Click here to go to the Request Tracker</a></td>
					</tr>
					<tr>
						<td class="linedrow" colspan="2">
						<b>Filter By Creator:</b> 
						<select name="SearchCreatedBy" class="normal" style="vertical-align:middle;">
							<option <cfif searchCreatedBy is "All">selected</cfif> value="All">View All</option>
							<cfoutput>
							<option <cfif searchCreatedBy is myName>selected</cfif> value="#myName#">#myName#</option>
							</cfoutput>
							<cfoutput query="getCreatedBy">
								<cfif myName is not createdBy>
									<option <cfif createdBy is searchCreatedBy>selected</cfif> value="#createdBy#">#createdBy#</option> 
								</cfif>
							</cfoutput>
						</select>
						<input name="btnFilterRequests" type="submit" class="sidebar" style="vertical-align:middle;" value="Filter" />
						</td>
						<td class="linedrow"><input onclick="document.location='/formTracker/newRequest.cfm?backpage=/index.cfm';" type="button" class="sidebar" value="Create New Request" /></td>
					</tr>
					<!--- 
					<tr>
						<td class="linedrow">
						<b>Update Checked Items:</b> 
						<select name="requestStatusID" class="normal" style="vertical-align:middle;">
							<option value="0"></option>
							<cfoutput query="getRequestStatus">
								<option value="#requestStatusID#">#requestStatusName#</option>
							</cfoutput>
						</select>
						<input onclick="return confirm('Are you SURE you wish to perform this action? \n Clicking Apply will update the status of all checked items.');" name="btnUpdateItems" type="submit" class="sidebar" style="vertical-align:middle;" value="Apply" />
						</td>
						<td class="linedrow">&nbsp;</td>
						<td class="linedrow"><input onclick="document.location='/formTracker/newRequest.cfm?backpage=/index.cfm';" type="button" class="sidebar" value="Create New Request" /></td>
					</tr>
					--->
				</table>
				</td>
			</tr>
			<cfoutput>
			<tr>
				<td align="center" class="linedrow">&nbsp;</td>
				<!--- 
				<td align="center" class="linedrow"><a href="javascript:checkUncheck(document.outputForm);"><img border="0" src="/images/checkBox_clearbg.gif" alt="Check All/Uncheck All" width="23" height="26" /></a></td>
				--->
				<cfif sort is "createdByAsc"><cfset thisSort="createdByDesc"><cfelse><cfset thisSort = "createdByAsc"></cfif>
				<td class="linedrow"><b><a style="text-decoration:underline;" href="index.cfm?sort=#thisSort#&#urlString#">Created By</a></b></td>
				<cfif sort is "dealerNumAsc"><cfset thisSort="dealerNumDesc"><cfelse><cfset thisSort = "dealerNumAsc"></cfif>
				<td class="linedrow" nowrap="nowrap"><b><a style="text-decoration:underline;" href="index.cfm?sort=#thisSort#&#urlString#">Dealer ##</a></b></td>
				<cfif sort is "dealerNameAsc"><cfset thisSort="dealerNameDesc"><cfelse><cfset thisSort = "dealerNameAsc"></cfif>
				<td class="linedrow"><b><a style="text-decoration:underline;" href="index.cfm?sort=#thisSort#&#urlString#">Dealer Name</a></b></td>
				<cfif sort is "dateAsc"><cfset thisSort="dateDesc"><cfelse><cfset thisSort = "dateAsc"></cfif>
				<td align="center" class="linedrow"><b><a style="text-decoration:underline;" href="index.cfm?sort=#thisSort#&#urlString#">Date Submitted</a></b></td>
				<cfif sort is "requestAsc"><cfset thisSort="requestDesc"><cfelse><cfset thisSort = "requestAsc"></cfif>
				<td class="linedrow"><b><a style="text-decoration:underline;" href="index.cfm?sort=#thisSort#&#urlString#">Request</a></b></td>
				<cfif sort is "statusAsc"><cfset thisSort="statusDesc"><cfelse><cfset thisSort = "statusAsc"></cfif>
				<td class="linedrow"><b><a style="text-decoration:underline;" href="index.cfm?sort=#thisSort#&#urlString#">Status</a></b></td>
			</tr>
			</cfoutput>
			<cfoutput query="getRequests">
			<cfif getRequests.currentRow mod 2 is 0>
				<cfset bgc = "##EEEEEE">
			<cfelse>
				<cfset bgc = "##FFFFFF">
			</cfif>
			<tr>
				<td align="center" bgcolor="#bgc#" class="linedrow">
				<a href="/formTracker/details.cfm?rid=#requestID#&sort=#sort#&#urlString#">
				<img border="0" src="/images/manifyingglass.gif" alt="View Details" width="15" height="15" />
				</a>
				</td>
				<!--- 
				<td align="center" bgcolor="#bgc#" class="linedrow"><input type="checkbox" name="requestid" value="#requestid#" /></td>
				--->
				<td bgcolor="#bgc#" class="linedrow">#createdBy#</td>
				<td bgcolor="#bgc#" class="linedrow">#dealerNumber#</td>
				<td bgcolor="#bgc#" class="linedrow">#dealerName#</td>
				<td align="center" bgcolor="#bgc#" class="linedrow">#dateformat(requestDate,'mm/dd/yyyy')#</td>
				<td bgcolor="#bgc#" class="linedrow">#requestTypeName#</td>
				<td bgcolor="#bgc#" class="linedrow">
				<cfif requestStatusName is "Completed">
					Completed #dateformat(completedDate,'mm/dd/yyyy')#
				<cfelse>
					#requestStatusName#
				</cfif>
				</td>
			</tr>
			</cfoutput>
			</form>
		</table>
		</td>
	</tr>
</table>