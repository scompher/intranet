
<link rel="stylesheet" type="text/css" href="/styles.css">

<cfquery name="getNewPostCount" datasource="#ds#">
	select count(postid) as newPostCount 
	from confessional_posts 
	where 
	<cfif isDefined("cookie.lastConfessionalPostRead")>
		postid > #cookie.lastConfessionalPostRead# and 
	</cfif>
	postDateTime >= #createodbcdate(now())# 
</cfquery>

<script type="text/javascript">
function showAlert() {
	window.focus();
	alert('There are new confessionals!');
}
</script>

<title>Confessional Monitor</title>

<div align="center">
<meta http-equiv="refresh" content="5;url='confessionalMonitor.cfm';" />

<cfif getNewPostCount.newPostCount gt 0>
	<cfif findnocase("firefox",cgi.HTTP_USER_AGENT) is not 0>
		<body onLoad="showAlert();">
	<cfelse>
		<body onLoad="self.focus();">
	</cfif>
<cfelse>
	<body>
</cfif>

<table width="200" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Confessional Monitor</b> </td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding"><table width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td>
				<cfoutput>
				<cfif getNewPostCount.newPostCount gt 0>
				There are <b>#getNewPostCount.newPostCount#</b> new confessions!
				<cfelse>
				No new confessions
				</cfif>
				</cfoutput>
				</td>
			</tr>
			<tr>
				<td><a target="_blank" style="text-decoration:underline;" href="/index.cfm?showConfessional=true">Click here to go to confessional</a> </td>
			</tr>
		</table>
		</td>
	</tr>
</table>
</body>
</div>