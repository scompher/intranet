<cfsetting showdebugoutput="no">

<cfparam name="postLimit" default="100">

<cfquery name="getPrefs" datasource="#ds#">
	select * from communicatorSettings 
	where adminuserid = #cookie.adminlogin# 
</cfquery>

<cfquery name="getPosts" datasource="#ds#">
	select 
	<cfif not isDefined("url.showAllPosts")>
	top #postLimit#  
	</cfif>
	communicator_posts.*, admin_users.firstname + ' ' + admin_users.lastname as posterName, admin_users.photo, admin_users.communicatorDisplayName 
	from communicator_posts 
	left join admin_users on communicator_posts.adminuserid = admin_users.adminuserid 
	where communicator_posts.deleted  = 0 
	order by postid DESC 
</cfquery>

<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<cfoutput query="getPosts">
	<cfif getPosts.currentRow is 1>
		<cfset lastPostID = getPosts.postid>
	</cfif>
	<cfif getPosts.currentRow mod 2 is 0>
		<cfset bgc = "##EEEEEE">
	<cfelse>
		<cfset bgc = "##FFFFFF">
	</cfif>
	<tr>
		<td bgcolor="#bgc#" class="linedrow" valign="top">
		<cfset outputText = postText>
		<!--- <cfif findnocase("#chr(32)##chr(32)#", outputText) is not 0><cfset asciiArt = true><cfelse><cfset asciiArt = false></cfif> --->
		<cfif left(outputText,2) is "#chr(32)##chr(32)#" ><cfset asciiArt = true><cfelse><cfset asciiArt = false></cfif>
		<cfif not isdefined("cookie.hidecommunicatorimages") and not isDefined("variables.hideImages")>
			<cfif trim(photo) is not "">
				<img src="/users/images/#photo#" border="1" hspace="5" <cfif asciiArt is false>align="left"</cfif> />
			</cfif>
		</cfif>
		<cfset dateDifference = dateformat(now(),'mm/dd/yyyy') - dateformat(postDateTime,'mm/dd/yyyy')>
		<cfset postDay = " ">
		<cfif dateDifference is 0>
			<cfset postDay = " today ">
		<cfelseif dateDifference is 1>
			<cfset postDay = " yesterday ">
		<cfelse>
			<cfset postDay = " on " & dateformat(postDateTime,'mm/dd') & " ">
		</cfif>
		<!--- 
		<cfif findnocase("tenerelli",posterName) is not 0>
			<cfquery name="getQuote" datasource="#ds#">
				SELECT TOP 1 quote FROM robinquotes 
				ORDER BY NEWID()
			</cfquery>
			<cfset outputText = trim(getQuote.quote) & "! " & outputText>
		</cfif> 
		--->
		<cfif trim(getposts.communicatorDisplayName) is not "">
			<cfset displayName = getposts.communicatorDisplayName>
		<cfelse>
			<cfset displayName = getposts.posterName>
		</cfif>
		At #timeformat(postDateTime,'hh:mm:ss tt')##postDay# #displayName# 
		<cfif trim(postedUsing) is not "">using #postedUsing#</cfif> wrote: <cfif findnocase(chr(13),postText) is not 0><br /></cfif>
		<cfif postid gt getPrefs.lastcommunicatorPostRead and (getposts.adminuserid is not cookie.adminlogin)>
			<cfset outputText = "<b>" & outputText & "</b>">
		</cfif>
		<cfif trim(smallImageName) is "">
			<cfif asciiArt is true><cfset outputText = "<pre>" & outputText & "</pre"></cfif>
			#outputText#
		<cfelse>
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td>
					<a href="http://www.copalink.com/communicate/attachments/#largeImageName#">
					<img src="http://www.copalink.com/communicate/attachments/#smallImageName#" border="1" style="border-color:##000000" />
					</a>
					</td>
				</tr>
				<tr>
					<td>
					#outputText#
					</td>
				</tr>
			</table>
		</cfif>
		</td>
		<td width="16" nowrap="nowrap" bgcolor="#bgc#" class="linedrow" valign="top">
		<cfif (cookie.adminlogin is getPosts.adminuserid) or (getsec.seclevelid lte 2)>
		<a onclick="return confirm('Are you sure you wish to remove this post?');" href="index.cfm?delpost=true&id=#postid#">
		<img alt="Remove this post" src="../../images/delete.gif" width="16" height="16" border="0" />
		</a>
		<cfelse>
		&nbsp;
		</cfif>
		</td>
	</tr>
	</cfoutput>
</table>

<cfif getPosts.recordcount gt 0>
	<cfcookie name="lastcommunicatorPostRead" value="#variables.lastPostID#" expires="never">
	<cfquery name="updateLastRead" datasource="#ds#">
		update communicatorSettings 
		set lastcommunicatorPostRead = #variables.lastPostID# 
		where adminuserid = #cookie.adminlogin# 
	</cfquery>
</cfif>

