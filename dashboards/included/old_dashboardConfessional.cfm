<!--- stuff to prevent caching of pages --->

	<!--- Client side cache prevention --->
	<meta http-equiv="Expires" content="0">
	
	<!--- Setup our expire times for Netscape and Internet Explorer --->
	<cfoutput>
			<!--- Internet Explorer Date Formate: (Fri, 30 Oct 1998 14:19:41 GMT) --->
			<cfset MSIEtimestamp='#dateformat(now(),"DDD")#,#dateformat(now(),"DD")#
			#dateformat(now(),"Mmm")# #timeformat(now(),"HH:MM:SS")#'>
	
			<!--- Netscape Date Formate: Netscape (Wednesday, Apr 26 2000 17:45:25 PM) --->
			<cfset NETSCAPEtimestamp='#dateformat(now(),"DDDD")#,#dateformat(now(),"MMM")#
			#dateformat(now(),"dd")# #dateformat(now(),"YYY")#
			#timeformat(now(),"HH:MM:SS tt")#'>
	</cfoutput>
	
	<!--- Tell HTTP Header to force expire of page - nocache --->
	<cfif HTTP_USER_AGENT contains "MSIE">
			<cfheader name="Expires" value="<cfoutput>#MSIEtimestamp#</cfoutput>">
			<cfheader name="Pragma" value="no-cache">
			<cfheader name="cache-control" value="no-cache, no-store, must-revalidate">
	<cfelse>
			<cfheader name="Expires" value="<cfoutput>#NETSCAPEtimestamp#</cfoutput>">
			<cfheader name="Pragma" value="no-cache">
			<cfheader name="cache-control" value="no-cache, no-store, must-revalidate">
	</cfif>
	
<!--- stuff to prevent caching of pages --->

<link rel="stylesheet" type="text/css" href="/styles.css">

<script type="text/javascript">
function openConfessionalMonitor(u) {
	window.open(u,"MonitorWin","width=220,height=20,scrollbars=0,status=0,location=0,resizable=0");
}

function openDashboardWindow(u) {
	window.open(u,"dashboardWin","width=250,height=400,scrollbars=1,status=0,location=0,resizable=1");
}
</script>

<cfparam name="postLimit" default="100">

<cfif isDefined("url.delpost")>
	<cfquery name="delPost" datasource="#ds#">
		delete from confessional_posts 
		where postid = #url.id# 
	</cfquery>
</cfif>

<cfif isDefined("form.btnSaveBday")>
	<cfquery name="updateBday" datasource="#ds#">
		update admin_users 
		set birthdate = '#form.birthdate#' 
		where adminuserid = #cookie.adminlogin# 
	</cfquery>
</cfif>

<cfif not isDefined("cookie.lastConfessionalPostRead")>
	<cfcookie name="lastConfessionalPostRead" value="0" expires="never">
</cfif>

<cfquery name="getPrefs" datasource="#ds#">
	select * from confessionalSettings 
	where adminuserid = #cookie.adminlogin# 
</cfquery>
<cfif getPrefs.recordcount is 0>
	<cfquery name="createDefaultPrefs" datasource="#ds#">
		insert into confessionalSettings (adminuserid, lastConfessionalPostRead) 
		values (#cookie.adminLogin#, #cookie.lastConfessionalPostRead#) 
	</cfquery>
	<cfquery name="getPrefs" datasource="#ds#">
		select * from confessionalSettings 
		where adminuserid = #cookie.adminlogin# 
	</cfquery>
</cfif>

<cfif not isNumeric(getPrefs.lastConfessionalPostRead)>
	<cfquery name="updateLastRead" datasource="#ds#">
		update confessionalSettings 
		set lastConfessionalPostRead = #cookie.lastConfessionalPostRead# 
		where adminuserid = #cookie.adminlogin# 
	</cfquery>
	<cfquery name="getPrefs" datasource="#ds#">
		select * from confessionalSettings 
		where adminuserid = #cookie.adminlogin# 
	</cfquery>
<cfelse>
	<cfset cookie.lastConfessionalPostRead = getPrefs.lastConfessionalPostRead>
</cfif>

<cfquery name="getNewPostCount" datasource="#ds#">
	select count(postid) as newPostCount 
	from confessional_posts 
	where postid > #getPrefs.lastConfessionalPostRead# and adminuserid <> #cookie.adminlogin# 
</cfquery>

<cfif isDefined("url.deactivateNotifications")>
	<cfquery name="turnOffNotifications" datasource="#ds#">
		update admin_users 
		set confessionalNotifications = 0 
		where adminuserid = #cookie.adminlogin# 
	</cfquery>
</cfif>

<cfif isDefined("url.activateNotifications")>
	<cfquery name="turnOnNotifications" datasource="#ds#">
		update admin_users 
		set confessionalNotifications = 1 
		where adminuserid = #cookie.adminlogin# 
	</cfquery>
</cfif>

<cfif isDefined("url.hideConfessional")>
	<cfcookie name="hideconfessional" value="true" expires="never">
</cfif>

<cfif isDefined("url.showConfessional")>
	<cfcookie name="hideconfessional" expires="now">
	<meta http-equiv="refresh" content="0;url=index.cfm" />
</cfif>

<cfif isDefined("url.hideConfessionalImages")>
	<cfcookie name="hideconfessionalimages" value="true" expires="never">
</cfif>

<cfif isDefined("url.showConfessionalImages")>
	<cfcookie name="hideconfessionalimages" expires="now">
	<meta http-equiv="refresh" content="0;url=index.cfm" />
</cfif>

<cfif isDefined("form.btnSaveConfession")>
	<cfif trim(form.postText) is not "">
		<cfquery name="postItem" datasource="#ds#">
			insert into confessional_posts (postText, postDateTime, adminuserid) 
			values ('#form.postText#', #createodbcdatetime(now())#, #cookie.adminlogin#) 
		</cfquery>
		<cfquery name="getPosterName" datasource="#ds#">
			select * from admin_users 
			where adminuserid = #cookie.adminlogin# 
		</cfquery>

		<cfinvoke 
			component="copalink.confess.confessionalFunctions" 
			method="SendNotifications"
			returnvariable="sendResult">
			<cfinvokeargument name="adminLogin" value="#cookie.adminlogin#"/>
			<cfinvokeargument name="postText" value="#form.postText#"/>
		</cfinvoke>

		<cflocation url="index.cfm">
	</cfif>
</cfif>

<cfquery name="getPosts" datasource="#ds#">
	select 
	<cfif not isDefined("url.showAllPosts")>
	top #postLimit#  
	</cfif>
	confessional_posts.*, admin_users.firstname + ' ' + admin_users.lastname as posterName, admin_users.photo, admin_users.confessionalDisplayName 
	from confessional_posts 
	left join admin_users on confessional_posts.adminuserid = admin_users.adminuserid 
	order by postid DESC 
</cfquery>

<cfquery name="getTotalPostCount" datasource="#ds#">
	select count(postid) as totalPosts 
	from confessional_posts 
</cfquery>

<cfquery name="checkNotificationStatus" datasource="#ds#">
	select confessionalNotifications 
	from admin_users 
	where adminuserid = #cookie.adminlogin# 
</cfquery>

<!--- calc bday timer --->
<cfquery name="getBday" datasource="#ds#">
	select birthdate from admin_users 
	where adminuserid = #cookie.adminlogin# 
</cfquery>
<cfif isDate(getBday.birthdate)>
	<cfif dateformat(getbday.birthdate,'mm/dd') gt dateformat(now(),'mm/dd')>
		<cfset bday = dateformat(getbday.birthdate,'mm/dd') & "/" & dateformat(now(),'yyyy')>
	<cfelse>
		<cfset bday = dateformat(getbday.birthdate,'mm/dd') & "/" & evaluate(dateformat(now(),'yyyy') + 1)>
	</cfif>
	<cfset daysToBday = datediff("d",dateformat(now(), 'mm/dd/yyyy'),bday)>
<cfelse>
	<cfset daysToBday = -1>
</cfif>
<!--- calc bday timer --->

<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar" style="padding:0px">
		<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td width="1%" nowrap="nowrap"><b>Dashboard Confessional</b> </td>
				<td>
				<cfoutput>
				There are 
				<cfif getNewPostCount.newPostCount gt 0>
				<b>#getNewPostCount.newPostCount#</b> 
				<cfelse>
				#getNewPostCount.newPostCount#
				</cfif>
				new confessions
				</cfoutput>
				&nbsp;
				<a href="javascript:openDashboardWindow('/specialUserContent/confessionalWindow/index.cfm');">[Pop Out Confessional]</a>
				</td>
			</tr>
			<tr>
				<td colspan="2">
				<cfif not isdefined("cookie.hideConfessional")>
				<a style="text-decoration:underline" href="index.cfm?hideConfessional=true">Hide Confessional</a>
				<cfelse>
				<a style="text-decoration:underline" href="index.cfm?showConfessional=true">Show Confessional</a> 
				</cfif>
				 | 
				<cfif not isDefined("variables.hideImages")>
					<cfif not isdefined("cookie.hideConfessionalImages")>
					<a style="text-decoration:underline" href="index.cfm?hideConfessionalImages=true">Turn Images Off</a> (currently ON)
					<cfelse>
					<a style="text-decoration:underline" href="index.cfm?showConfessionalImages=true">Turn Images On</a> (currently OFF)
					</cfif>
				</cfif>
				 | 
				<a style="text-decoration:underline" href="/dashboards/included/manageNotifications.cfm">Manage Notifications</a>
				<!--- 
				<cfif checkNotificationStatus.confessionalNotifications is 1>
				<a style="text-decoration:underline" href="index.cfm?deactivateNotifications=true">Turn Notifications Off</a> (currently ON)
				<cfelse>
				<a style="text-decoration:underline" href="index.cfm?activateNotifications=true">Turn Notifications On</a> (currently OFF)
				</cfif>
				---> 
				 | 
				 <a style="text-decoration:underline;" href="javascript:openConfessionalMonitor('/dashboards/included/confessionalMonitor.cfm');">Open Monitor</a>
				</td>
			</tr>
		</table>
		</td>
	</tr>
	<cfif not isDefined("cookie.hideConfessional")>
	<tr>
		<td class="greyrowbottomnopadding">
		<table border="0" cellspacing="0" cellpadding="5">
			<cfif daysToBday gt -1>
				<cfif daysToBday lte 30>
				<tr>
					<td><b>There are <span style="color:#FF0000"><cfoutput>#daysToBday#</cfoutput></span> days left until your birthday!</b></td>
				</tr>
				</cfif>
			<cfelse>
			<tr>
				<td colspan="3"><b>Birthday Countdown Timer</b></td>
			</tr>
			<form method="post" action="index.cfm">
			<tr>
				<td><b>Enter Birth Date:</b></td>
				<td><input type="text" name="birthdate" style="width:75px;" value="mm/dd/yyyy" onclick="this.select();" /></td>
				<td><input type="submit" name="btnSaveBday" value="Start Timer!" class="sidebarsmall" /></td>
			</tr>
			</form>
			</cfif>
		</table>
		<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<form method="post" action="index.cfm">
			<tr>
				<td class="linedrow">
				<b>Confess your sins!:</b><br />
				<textarea name="postText" style="width:100%; vertical-align:middle; margin-top:.5em; margin-bottom:.5em;" rows="3"></textarea><br />
				<input name="btnSaveConfession" type="submit" class="sidebar" value="Post Confession" style="vertical-align:middle;">
				</td>
			</tr>
			</form>
			<!--- news item 
			<tr>
				<td class="linedrow">
				<span class="alert">
				<b style="color:#DF0000">NEWS FLASH: YOU CAN NOW GO MOBILE! WWW.COPALINK.COM/CONFESS ALSO RECEIVE ALERTS VIA SMS!</b>
				</span>
				</td>
			</tr>
			 news item --->
			<cfset urlString = "?s=1">
			<cfif isDefined("hideImages")>
				<cfset urlString = urlString & "&hideImages=#hideImages#">
			</cfif>
			<cfif isDefined("postlimit")>
				<cfset urlString = urlString & "&postlimit=#postlimit#">
			</cfif>
			<cfif getPosts.recordcount gt 0>
			<tr><td class="linedrow"><b>What others have confessed:</b></td></tr>
			<tr>
				<td class="nopadding">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<cfoutput query="getPosts">
					<cfif getPosts.currentRow is 1>
						<cfset lastPostID = getPosts.postid>
					</cfif>
					<cfif getPosts.currentRow mod 2 is 0>
						<cfset bgc = "##EEEEEE">
					<cfelse>
						<cfset bgc = "##FFFFFF">
					</cfif>
					<tr>
						<td bgcolor="#bgc#" class="linedrow" valign="top">
						<cfset outputText = postText>
						<!--- <cfif findnocase("#chr(32)##chr(32)#", outputText) is not 0><cfset asciiArt = true><cfelse><cfset asciiArt = false></cfif> --->
						<cfif left(outputText,2) is "#chr(32)##chr(32)#" ><cfset asciiArt = true><cfelse><cfset asciiArt = false></cfif>
						<cfif not isdefined("cookie.hideconfessionalimages") and not isDefined("variables.hideImages")>
							<cfif trim(photo) is not "">
								<img src="/users/images/#photo#" border="1" hspace="5" <cfif asciiArt is false>align="left"</cfif> />
							</cfif>
						</cfif>
						<cfset dateDifference = dateformat(now(),'mm/dd/yyyy') - dateformat(postDateTime,'mm/dd/yyyy')>
						<cfset postDay = " ">
						<cfif dateDifference is 0>
							<cfset postDay = " today ">
						<cfelseif dateDifference is 1>
							<cfset postDay = " yesterday ">
						<cfelse>
							<cfset postDay = " on " & dateformat(postDateTime,'mm/dd') & " ">
						</cfif>
						<!--- 
						<cfif findnocase("tenerelli",posterName) is not 0>
							<cfquery name="getQuote" datasource="#ds#">
								SELECT TOP 1 quote FROM robinquotes 
								ORDER BY NEWID()
							</cfquery>
							<cfset outputText = trim(getQuote.quote) & "! " & outputText>
						</cfif> 
						--->
						<cfif trim(getposts.confessionalDisplayName) is not "">
							<cfset displayName = getposts.confessionalDisplayName>
						<cfelse>
							<cfset displayName = getposts.posterName>
						</cfif>
						At #timeformat(postDateTime,'hh:mm:ss tt')##postDay# #displayName# 
						<cfif trim(postedUsing) is not "">using #postedUsing#</cfif> wrote: <cfif findnocase(chr(13),postText) is not 0><br /></cfif>
						<cfif postid gt getPrefs.lastConfessionalPostRead and (getposts.adminuserid is not cookie.adminlogin)>
							<cfset outputText = "<b>" & outputText & "</b>">
						</cfif>
						<cfif trim(smallImageName) is "">
							<cfif asciiArt is true><cfset outputText = "<pre>" & outputText & "</pre"></cfif>
							#outputText#
						<cfelse>
							<table width="100%" border="0" cellspacing="0" cellpadding="5">
								<tr>
									<td>
									<a href="http://www.copalink.com/confess/attachments/#largeImageName#">
									<img src="http://www.copalink.com/confess/attachments/#smallImageName#" border="1" style="border-color:##000000" />
									</a>
									</td>
								</tr>
								<tr>
									<td>
									#outputText#
									</td>
								</tr>
							</table>
						</cfif>
						</td>
						<td width="16" nowrap="nowrap" bgcolor="#bgc#" class="linedrow" valign="top">
						<cfif cookie.adminlogin is getPosts.adminuserid>
						<a onclick="return confirm('Are you sure you wish to remove your post?');" href="index.cfm?delpost=true&id=#postid#">
						<img alt="Remove this post" src="../../images/delete.gif" width="16" height="16" border="0" />
						</a>
						<cfelse>
						&nbsp;
						</cfif>
						</td>
					</tr>
					</cfoutput>
				</table>
				</td>
			</tr>
			</cfif>
			<cfif getTotalPostCount.totalPosts gte postLimit>
			<tr>
				<td>
				<cfif not isDefined("url.showAllPosts")>
				<a href="index.cfm?showAllPosts=true">[Show All Posts]</a>
				<cfelse>
				<a href="index.cfm">[Hide Extra Posts]</a>
				</cfif>
				</td>
			</tr>
			</cfif>
			<cfif getPosts.recordcount gt 0>
				<cfcookie name="lastConfessionalPostRead" value="#variables.lastPostID#" expires="never">
				<cfquery name="updateLastRead" datasource="#ds#">
					update confessionalSettings 
					set lastConfessionalPostRead = #variables.lastPostID# 
					where adminuserid = #cookie.adminlogin# 
				</cfquery>
			</cfif>
		</table>
		</td>
	</tr>
	</cfif>
</table>
<br />

