<link rel="stylesheet" type="text/css" href="../../styles.css">

<cfparam name="mobileNumber" default="">
<cfparam name="mobileNotifications" default="0">
<cfparam name="emailNotifications" default="0">
<cfparam name="emailAddress" default="">
<cfparam name="carrierExtension" default="">

<cfif isdefined("form.submitted")>

	<cfquery name="updatePrefs" datasource="#ds#">
		update communicatorSettings 
		set mobileNotifications = #mobileNotifications#, emailNotifications = #emailNotifications# 
		where adminuserid = #cookie.adminlogin# 
	</cfquery>

</cfif>

<cfif isDefined("form.btnSaveNumber")>
	<cfset form.mobileNumber = replace(form.mobileNumber,"(","","all")>
	<cfset form.mobileNumber = replace(form.mobileNumber,")","","all")>
	<cfset form.mobileNumber = replace(form.mobileNumber,"-","","all")>
	<cfset form.mobileNumber = replace(form.mobileNumber," ","","all")>
	<cfquery name="updatePrefs" datasource="#ds#">
		update communicatorSettings 
		set mobileNumber = '#form.mobileNumber#', carrierExtension = '#form.carrierExtension#' 
		where adminuserid = #cookie.adminlogin# 
	</cfquery>
	<cflocation url="manageCommunicatorNotifications.cfm">
</cfif>

<cfif isDefined("form.btnSaveEmail")>
	<cfquery name="updatePrefs" datasource="#ds#">
		update communicatorSettings 
		set emailAddress = '#form.emailAddress#' 
		where adminuserid = #cookie.adminlogin# 
	</cfquery>
	<cflocation url="manageCommunicatorNotifications.cfm">
</cfif>

<cfquery name="getPrefs" datasource="#ds#">
	select * from communicatorSettings 
	where adminuserid = #cookie.adminlogin# 
</cfquery>
<cfset mobileNumber = getPrefs.mobileNumber>
<cfset mobileNotifications = getPrefs.mobileNotifications>
<cfset emailNotifications = getPrefs.emailNotifications>
<cfset emailAddress = getPrefs.emailAddress>
<cfset carrierExtension = getPrefs.carrierExtension>

<div align="center">
<table width="400" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar" align="center"><b>Manage Notifications</b></td>
	</tr>
	<cfform method="post" action="manageCommunicatorNotifications.cfm">
	<input type="hidden" name="submitted" value="1" />
	<tr>
		<td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td width="8%">
						<input <cfif emailNotifications is 1>checked</cfif> onclick="this.form.submit();" type="checkbox" name="emailNotifications" value="1" />
					</td>
					<td width="92%">Send me E-Mail Notifications </td>
				</tr>
				<cfif isDefined("form.btnChangeEmail")>
					<tr>
						<td>&nbsp;</td>
						<td>
							<input name="emailAddress" type="text" size="30" maxlength="50" />
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>
							<input type="submit" name="btnSaveEmail" value="Save E-Mail" />
						</td>
					</tr>
				<cfelse>
					<cfif emailNotifications is not 0>
					<tr>
						<td>&nbsp;</td>
						<td>Current email: <cfoutput>#emailAddress#</cfoutput></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>
							<input type="submit" name="btnChangeEmail" value="Change E-Mail" />
						</td>
					</tr>
					</cfif>
				</cfif>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>
						<input onclick="this.form.submit();" <cfif mobileNotifications is not 0>checked</cfif> type="checkbox" name="mobileNotifications" value="1" />
					</td>
					<td>Send me SMS Notifications</td>
				</tr>
				<cfif isDefined("form.btnChangeNumber")>
					<tr>
						<td>&nbsp;</td>
						<td>
							<cfinput name="mobileNumber" type="text" size="20" maxlength="20" value="#mobileNumber#" />
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>
							<select name="carrierExtension">
								<option <cfif carrierExtension is "">selected</cfif> value="">Select Carrier</option>
								<option <cfif carrierExtension is "@vtext.com">selected</cfif> value="@vtext.com">Verizon Wireless</option>
								<option <cfif carrierExtension is "@messaging.sprintpcs.com">selected</cfif> value="@messaging.sprintpcs.com">Sprint</option>
								<option <cfif carrierExtension is "@txt.att.net">selected</cfif> value="@txt.att.net">AT&amp;T</option>
								<option <cfif carrierExtension is "@tmomail.net">selected</cfif> value="@tmomail.net">T-Mobile</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>
							<input type="submit" name="btnSaveNumber" value="Save Number" />
						</td>
					</tr>
				<cfelse>
					<cfif mobileNotifications is 1>
					<tr>
						<td>&nbsp;</td>
						<td>
						Current number: <cfoutput>#mobileNumber#</cfoutput> with 
						<cfswitch expression="#carrierExtension#">
							<cfcase value="@vtext.com">Verizon Wireless</cfcase>
							<cfcase value="@messaging.sprintpcs.com">Sprint</cfcase>
							<cfcase value="@txt.att.net">AT&amp;T</cfcase>
							<cfcase value="@tmomail.net">T-Mobile</cfcase>
						</cfswitch>
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>
							<input type="submit" name="btnChangeNumber" value="Change Number or Carrier" />
						</td>
					</tr>
					</cfif>
				</cfif>
			</table>
		</td>
	</tr>
	</cfform>
	<tr><td>&nbsp;</td></tr>
	<tr>
		<td align="center"><a href="/index.cfm">Return to communicator</a></td>
	</tr>
</table>
</div>