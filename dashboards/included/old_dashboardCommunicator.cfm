
<script src="/jQuery/jquery-1.11.1.js"></script>

<!--- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> --->

<script>
$(document).ready(function(){
	//$.ajaxSetup({cache: false}); // IE bug 
	$("#showposts").load("/dashboards/included/getPosts.cfm");
	setInterval(function(){
		$("#showposts").load("/dashboards/included/getPosts.cfm");
	},3000);
});
</script>

<!--- stuff to prevent caching of pages --->

<!--- Client side cache prevention --->
<meta http-equiv="Expires" content="0">

<!--- Setup our expire times for Netscape and Internet Explorer --->
<cfoutput>
		<!--- Internet Explorer Date Formate: (Fri, 30 Oct 1998 14:19:41 GMT) --->
		<cfset MSIEtimestamp='#dateformat(now(),"DDD")#,#dateformat(now(),"DD")#
		#dateformat(now(),"Mmm")# #timeformat(now(),"HH:MM:SS")#'>

		<!--- Netscape Date Formate: Netscape (Wednesday, Apr 26 2000 17:45:25 PM) --->
		<cfset NETSCAPEtimestamp='#dateformat(now(),"DDDD")#,#dateformat(now(),"MMM")#
		#dateformat(now(),"dd")# #dateformat(now(),"YYY")#
		#timeformat(now(),"HH:MM:SS tt")#'>
</cfoutput>

<!--- Tell HTTP Header to force expire of page - nocache --->
<cfif HTTP_USER_AGENT contains "MSIE">
		<cfheader name="Expires" value="<cfoutput>#MSIEtimestamp#</cfoutput>">
		<cfheader name="Pragma" value="no-cache">
		<cfheader name="cache-control" value="no-cache, no-store, must-revalidate">
<cfelse>
		<cfheader name="Expires" value="<cfoutput>#NETSCAPEtimestamp#</cfoutput>">
		<cfheader name="Pragma" value="no-cache">
		<cfheader name="cache-control" value="no-cache, no-store, must-revalidate">
</cfif>

<!--- stuff to prevent caching of pages --->

<link rel="stylesheet" type="text/css" href="/styles.css">

<script type="text/javascript">
function opencommunicatorMonitor(u) {
	window.open(u,"MonitorWin","width=250,height=50,scrollbars=0,status=0,location=0,resizable=1");
}

function openDashboardWindow(u) {
	window.open(u,"dashboardWin","width=250,height=400,scrollbars=1,status=0,location=0,resizable=1");
}
</script>

<cfparam name="postLimit" default="30">

<cfif isDefined("url.delpost")>
	<cfquery name="delPost" datasource="#ds#">
		update communicator_posts 
		set deleted = 1 
		where postid = #url.id# 
	</cfquery>
</cfif>

<cfif isDefined("form.btnSaveBday")>
	<cfquery name="updateBday" datasource="#ds#">
		update admin_users 
		set birthdate = '#form.birthdate#' 
		where adminuserid = #cookie.adminlogin# 
	</cfquery>
</cfif>

<cfif not isDefined("cookie.lastcommunicatorPostRead")>
	<cfcookie name="lastcommunicatorPostRead" value="0" expires="never">
</cfif>

<cfquery name="getPrefs" datasource="#ds#">
	select * from communicatorSettings 
	where adminuserid = #cookie.adminlogin# 
</cfquery>
<cfif getPrefs.recordcount is 0>
	<cfquery name="createDefaultPrefs" datasource="#ds#">
		insert into communicatorSettings (adminuserid, lastcommunicatorPostRead) 
		values (#cookie.adminLogin#, #cookie.lastcommunicatorPostRead#) 
	</cfquery>
	<cfquery name="getPrefs" datasource="#ds#">
		select * from communicatorSettings 
		where adminuserid = #cookie.adminlogin# 
	</cfquery>
</cfif>

<cfif not isNumeric(getPrefs.lastcommunicatorPostRead)>
	<cfquery name="updateLastRead" datasource="#ds#">
		update communicatorSettings 
		set lastcommunicatorPostRead = #cookie.lastcommunicatorPostRead# 
		where adminuserid = #cookie.adminlogin# 
	</cfquery>
	<cfquery name="getPrefs" datasource="#ds#">
		select * from communicatorSettings 
		where adminuserid = #cookie.adminlogin# 
	</cfquery>
<cfelse>
	<cfset cookie.lastcommunicatorPostRead = getPrefs.lastcommunicatorPostRead>
</cfif>

<cfquery name="getNewPostCount" datasource="#ds#">
	select count(postid) as newPostCount 
	from communicator_posts 
	where postid > #getPrefs.lastcommunicatorPostRead# and adminuserid <> #cookie.adminlogin# 
</cfquery>

<cfif isDefined("url.deactivateNotifications")>
	<cfquery name="turnOffNotifications" datasource="#ds#">
		update admin_users 
		set communicatorNotifications = 0 
		where adminuserid = #cookie.adminlogin# 
	</cfquery>
</cfif>

<cfif isDefined("url.activateNotifications")>
	<cfquery name="turnOnNotifications" datasource="#ds#">
		update admin_users 
		set communicatorNotifications = 1 
		where adminuserid = #cookie.adminlogin# 
	</cfquery>
</cfif>

<cfif isDefined("url.hidecommunicator")>
	<cfcookie name="hidecommunicator" value="true" expires="never">
</cfif>

<cfif isDefined("url.showcommunicator")>
	<cfcookie name="hidecommunicator" expires="now">
	<meta http-equiv="refresh" content="0;url=index.cfm" />
</cfif>

<cfif isDefined("url.hidecommunicatorImages")>
	<cfcookie name="hidecommunicatorimages" value="true" expires="never">
</cfif>

<cfif isDefined("url.showcommunicatorImages")>
	<cfcookie name="hidecommunicatorimages" expires="now">
	<meta http-equiv="refresh" content="0;url=index.cfm" />
</cfif>

<cfif isDefined("form.btnSaveConfession")>
	<cfif trim(form.postText) is not "">
		<cfquery name="postItem" datasource="#ds#">
			insert into communicator_posts (postText, postDateTime, adminuserid) 
			values ('#form.postText#', #createodbcdatetime(now())#, #cookie.adminlogin#) 
		</cfquery>
		<cfquery name="getPosterName" datasource="#ds#">
			select * from admin_users 
			where adminuserid = #cookie.adminlogin# 
		</cfquery>

		<cfinvoke 
			component="copalink.communicate.communicatorFunctions" 
			method="SendNotifications"
			returnvariable="sendResult">
			<cfinvokeargument name="adminLogin" value="#cookie.adminlogin#"/>
			<cfinvokeargument name="postText" value="#form.postText#"/>
		</cfinvoke>

		<cflocation url="index.cfm">
	</cfif>
</cfif>

<cfquery name="getPosts" datasource="#ds#">
	select 
	<cfif not isDefined("url.showAllPosts")>
	top #postLimit#  
	</cfif>
	communicator_posts.*, admin_users.firstname + ' ' + admin_users.lastname as posterName, admin_users.photo, admin_users.communicatorDisplayName 
	from communicator_posts 
	left join admin_users on communicator_posts.adminuserid = admin_users.adminuserid 
	order by postid DESC 
</cfquery>

<cfquery name="getTotalPostCount" datasource="#ds#">
	select count(postid) as totalPosts 
	from communicator_posts 
</cfquery>

<cfquery name="checkNotificationStatus" datasource="#ds#">
	select communicatorNotifications 
	from admin_users 
	where adminuserid = #cookie.adminlogin# 
</cfquery>

<!--- calc bday timer --->
<cfquery name="getBday" datasource="#ds#">
	select birthdate from admin_users 
	where adminuserid = #cookie.adminlogin# 
</cfquery>
<cfif isDate(getBday.birthdate)>
	<cfif dateformat(getbday.birthdate,'mm/dd') gt dateformat(now(),'mm/dd')>
		<cfset bday = dateformat(getbday.birthdate,'mm/dd') & "/" & dateformat(now(),'yyyy')>
	<cfelse>
		<cfset bday = dateformat(getbday.birthdate,'mm/dd') & "/" & evaluate(dateformat(now(),'yyyy') + 1)>
	</cfif>
	<cfset daysToBday = datediff("d",dateformat(now(), 'mm/dd/yyyy'),bday)>
<cfelse>
	<cfset daysToBday = -1>
</cfif>
<!--- calc bday timer --->

<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar" style="padding:0px">
		<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td width="1%" nowrap="nowrap"><b>Operations Communicator</b> </td>
				<!--- 
				<td>
				<cfoutput>
				There are 
				<cfif getNewPostCount.newPostCount gt 0>
				<b>#getNewPostCount.newPostCount#</b> 
				<cfelse>
				#getNewPostCount.newPostCount#
				</cfif>
				new posts
				</cfoutput>
				&nbsp;
				<a href="javascript:openDashboardWindow('/specialUserContent/communicatorWindow/index.cfm');">[Pop Out communicator]</a>
				</td>
				 --->
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2">
				<cfif not isdefined("cookie.hidecommunicator")>
				<a style="text-decoration:underline" href="index.cfm?hidecommunicator=true">Hide communicator</a>
				<cfelse>
				<a style="text-decoration:underline" href="index.cfm?showcommunicator=true">Show communicator</a> 
				</cfif>
				 | 
				<cfif not isDefined("variables.hideImages")>
					<cfif not isdefined("cookie.hidecommunicatorImages")>
					<a style="text-decoration:underline" href="index.cfm?hidecommunicatorImages=true">Turn Images Off</a> (currently ON)
					<cfelse>
					<a style="text-decoration:underline" href="index.cfm?showcommunicatorImages=true">Turn Images On</a> (currently OFF)
					</cfif>
				</cfif>
				 <!--- | ---> 
				<!--- <a style="text-decoration:underline" href="/dashboards/included/manageCommunicatorNotifications.cfm">Manage Notifications</a> --->
				<!--- 
				<cfif checkNotificationStatus.communicatorNotifications is 1>
				<a style="text-decoration:underline" href="index.cfm?deactivateNotifications=true">Turn Notifications Off</a> (currently ON)
				<cfelse>
				<a style="text-decoration:underline" href="index.cfm?activateNotifications=true">Turn Notifications On</a> (currently OFF)
				</cfif>
				---> 

				<a style="text-decoration:underline;" href="javascript:opencommunicatorMonitor('/dashboards/included/communicatorMonitor.cfm');">Open Monitor</a>

				</td>
			</tr>
		</table>
		</td>
	</tr>
	<cfif not isDefined("cookie.hidecommunicator")>
	<tr>
		<td class="greyrowbottomnopadding">
		<!---
		<table border="0" cellspacing="0" cellpadding="5">
			<cfif daysToBday gt -1>
				<cfif daysToBday lte 30>
				<tr>
					<td><b>There are <span style="color:#FF0000"><cfoutput>#daysToBday#</cfoutput></span> days left until your birthday!</b></td>
				</tr>
				</cfif>
			<cfelse>
			<tr>
				<td colspan="3"><b>Birthday Countdown Timer</b></td>
			</tr>
			<form method="post" action="index.cfm">
			<tr>
				<td><b>Enter Birth Date:</b></td>
				<td><input type="text" name="birthdate" style="width:75px;" value="mm/dd/yyyy" onclick="this.select();" /></td>
				<td><input type="submit" name="btnSaveBday" value="Start Timer!" class="sidebarsmall" /></td>
			</tr>
			</form>
			</cfif>
		</table>
		--->
		<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<form method="post" action="index.cfm">
			<tr>
				<td class="linedrow">
				<b>Post Updates:</b><br />
				<textarea name="postText" style="width:100%; vertical-align:middle; margin-top:.5em; margin-bottom:.5em;" rows="3"></textarea><br />
				<input name="btnSaveConfession" type="submit" class="sidebar" value="Post Update" style="vertical-align:middle;">
				</td>
			</tr>
			</form>
			<!--- news item 
			<tr>
				<td class="linedrow">
				<span class="alert">
				<b style="color:#DF0000">NEWS FLASH: YOU CAN NOW GO MOBILE! WWW.COPALINK.COM/CONFESS ALSO RECEIVE ALERTS VIA SMS!</b>
				</span>
				</td>
			</tr>
			 news item --->
			<cfset urlString = "?s=1">
			<cfif isDefined("hideImages")>
				<cfset urlString = urlString & "&hideImages=#hideImages#">
			</cfif>
			<cfif isDefined("postlimit")>
				<cfset urlString = urlString & "&postlimit=#postlimit#">
			</cfif>
			<!--- <cfif getPosts.recordcount gt 0> --->
			<tr><td class="linedrow"><b>What others have posted:</b></td></tr>
			<tr>
				<td class="nopadding">
				<div id="showposts">
				<!--- show posts here --->
				</div>
				</td>
			</tr>
			<!--- </cfif> --->
			<cfif getTotalPostCount.totalPosts gte postLimit>
			<tr>
				<td>
				<cfif not isDefined("url.showAllPosts")>
				<a href="index.cfm?showAllPosts=true">[Show All Posts]</a>
				<cfelse>
				<a href="index.cfm">[Hide Extra Posts]</a>
				</cfif>
				</td>
			</tr>
			</cfif>
<!--- 			<cfif getPosts.recordcount gt 0>
				<cfcookie name="lastcommunicatorPostRead" value="#variables.lastPostID#" expires="never">
				<cfquery name="updateLastRead" datasource="#ds#">
					update communicatorSettings 
					set lastcommunicatorPostRead = #variables.lastPostID# 
					where adminuserid = #cookie.adminlogin# 
				</cfquery>
			</cfif> --->
		</table>
		</td>
	</tr>
	</cfif>
</table>
<br />

