<cfsetting showdebugoutput="no">

<link rel="stylesheet" type="text/css" href="/styles.css">

<style>
BODY {margin:0px} 
</style>

<cfquery name="getNewPostCount" datasource="#ds#">
	select count(postid) as newPostCount 
	from communicator_posts 
	where 
	<cfif isDefined("cookie.lastcommunicatorPostRead")>
		postid > #cookie.lastcommunicatorPostRead# and 
	</cfif>
	postDateTime >= #createodbcdate(now())# and deleted = 0
</cfquery>

<script type="text/javascript">
function showAlert() {
	window.focus();
	alert('There are new communicators!');
}
</script>

<title>communicator Monitor</title>

<div align="center">
<meta http-equiv="refresh" content="5;url='communicatorMonitor.cfm';" />

<cfif getNewPostCount.newPostCount gt 0>
	<cfif findnocase("firefox",cgi.HTTP_USER_AGENT) is not 0>
		<body onLoad="showAlert();">
	<cfelse>
		<body onLoad="self.focus();">
	</cfif>
<cfelse>
	<body>
</cfif>
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td height="20px;" class="highlightbar"><b>Communicator Monitor</b> </td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding"><table width="100%" height="100%" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td>
				<cfoutput>
				<cfif getNewPostCount.newPostCount gt 0>
				There are <b>#getNewPostCount.newPostCount#</b> new posts!
				<cfelse>
				No new posts
				</cfif>
				</cfoutput>
				</td>
			</tr>
			<tr>
				<td><a target="_blank" style="text-decoration:underline;" href="/index.cfm?showcommunicator=true">Click here to go to communicator</a> </td>
			</tr>
		</table>
		</td>
	</tr>
</table>
</body>
</div>