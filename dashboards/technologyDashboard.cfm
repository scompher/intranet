
<link rel="stylesheet" type="text/css" href="/styles.css">
<style type="text/css">
<!--
.red {
	color: #FF0000;
	font-weight: bold;
}
-->
</style>

<cfquery name="getInqs" datasource="#ds#">
	select 
	inquiries_main.dateTimeCreated, 
	inquiries_main.inquiryID, 
	inquiries_main.inquiryNumber, 
	inquiries_main.dateTimeOverdue, 
	inquiries_status_history.statusid 
	from inquiries_main with (nolock) 
	left join inquiries_status_history on inquiries_main.inquiryid = inquiries_status_history.inquiryid 
	where 
	inquiries_status_history.isCurrentStatus = 1 and inquiries_main.active = 1 and 
	inquiries_status_history.toDepartmentID IN (1) and 
	inquiries_status_history.statusid NOT IN (4,5) and 
	1=1 
</cfquery>

<cfquery name="countNewInqs" datasource="#ds#">
	select count(inquiries_main.inquiryid) as newCount 
	from inquiries_main with (nolock) 
	left join inquiries_status_history on inquiries_main.inquiryid = inquiries_status_history.inquiryid 
	where 
	inquiries_status_history.isCurrentStatus = 1 and inquiries_main.active = 1 and 
	inquiries_status_history.toDepartmentID IN (1) and 
	inquiries_status_history.statusid NOT IN (4,5) and 
	inquiries_status_history.statusid = 1 
</cfquery>

<cfquery name="countOpenInqs" datasource="#ds#">
	select count(inquiries_main.inquiryid) as openCount 
	from inquiries_main with (nolock) 
	left join inquiries_status_history on inquiries_main.inquiryid = inquiries_status_history.inquiryid 
	where 
	inquiries_status_history.isCurrentStatus = 1 and inquiries_main.active = 1 and 
	inquiries_status_history.toDepartmentID IN (1) and 
	inquiries_status_history.statusid NOT IN (4,5) and 
	inquiries_status_history.statusid IN (2,6) 
</cfquery>

<cfquery name="countOverDueInqs" datasource="#ds#">
	select count(inquiries_main.inquiryid) as overdueCount  
	from inquiries_main with (nolock) 
	left join inquiries_status_history on inquiries_main.inquiryid = inquiries_status_history.inquiryid 
	where 
	inquiries_status_history.isCurrentStatus = 1 and inquiries_main.active = 1 and 
	inquiries_status_history.toDepartmentID IN (1) and 
	inquiries_status_history.statusid NOT IN (4,5) and 
	inquiries_main.dateTimeOverdue <= #createodbcdatetime(now())#
</cfquery>

<cfquery name="countNewGCR" datasource="#ds#">
	select count(changeRequestID) as newCount 
	from globalChangeRequest with (nolock) 
	where currentStatus = 'Unread' 
</cfquery>

<cfquery name="countOpenedGCR" datasource="#ds#">
	select count(changeRequestID) as openedCount 
	from globalChangeRequest with (nolock) 
	where currentStatus = 'Opened' 
</cfquery>

<cfquery name="countNewTT" datasource="#ds#">
	select count(ticketid) as newCount 
	from troubleTicket_tickets with (nolock) 
	where currentStatusID = 0 
</cfquery>

<cfquery name="countOpenedTT" datasource="#ds#">
	select count(ticketid) as openedCount 
	from troubleTicket_tickets with (nolock) 
	where currentStatusID = 1
</cfquery>

<cfquery name="countOverdueTT" datasource="#ds#">
	select count(ticketid) as overdueCount 
	from troubleTicket_tickets with (nolock) 
	where currentStatusID <> 2 and estimatedCompletionDate < #createodbcdate(now())# 
</cfquery>

<cfset today = now()>
<cfset overduePickupDate = dateadd("h",-48,today)>
<cfquery name="countOverduePickupTT" datasource="#ds#">
	select count(ticketid) as overdueCount 
	from troubleTicket_tickets with (nolock) 
	where currentStatusID = 0 and dateTimeReported <= #createodbcdatetime(overduePickupDate)#
</cfquery>

<cfquery name="countTotalTT" datasource="#ds#">
	select count(ticketid) as totalCount 
	from troubleTicket_tickets with (nolock) 
	where currentStatusID <> 2 
</cfquery>

<cfif cookie.adminlogin is 1>
	<cfquery name="getPrivateLabelTotals" datasource="copalink_cobrand">
		select 
		(select count(*) from site_main where live = 1) as totalCount, 
		(select count(*) from site_main where inDevelopment = 1) as devCount,  
		(select count(*) from site_main where inDevelopment = 1 and dateTimeReceived <= dateadd(day,-7,getdate())) as overdueCount 
	</cfquery>
</cfif>

<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Technology Dashboard</b></td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td><b>Technology Inquiries </b> <a href="/inquiry/index.cfm?deptlist=1">(click here to jump to Inquiries)</a> </td>
				</tr>
				<cfoutput>
				<tr>
					<td class="linedrow">
						<table border="0" cellspacing="0" cellpadding="5">
							<tr>
								<td>Total: #getInqs.recordCount#</td>
								<td width="100">&nbsp;</td>
								<td>New: <cfif trim(countNewInqs.newCount) is "">0<cfelse>#countNewInqs.newCount#</cfif></td>
								<td width="100">&nbsp;</td>
								<td>Open: <cfif trim(countOpenInqs.openCount) is "">0<cfelse>#countOpenInqs.openCount#</cfif></td>
								<td width="100">&nbsp;</td>
								<td>Overdue: <cfif trim(countOverDueInqs.overdueCount) is "" or trim(countOverDueInqs.overdueCount) is 0>0<cfelse><span class="red">#countOverDueInqs.overdueCount#</span></cfif></td>
							</tr>
						</table>
					</td>
				</tr>
				</cfoutput>
				<tr>
					<td><b>Global Change Requests</b> <a href="/globalChange/">(click here to jump to global change requests)</a> </td>
				</tr>
				<cfoutput>
				<tr>
					<td class="linedrow">
						<table border="0" cellspacing="0" cellpadding="5">
							<tr>
								<td>New: <cfif trim(countNewGCR.newCount) is "">0<cfelse>#countNewGCR.newCount#</cfif></td>
								<td width="100">&nbsp;</td>
								<td>Open: <cfif trim(countOpenedGCR.openedCount) is "">0<cfelse>#countOpenedGCR.openedCount#</cfif></td>
							</tr>
						</table>
					</td>
				</tr>
				</cfoutput>
				<tr>
					<td><b>Equipment Trouble Tickets</b> <a href="/troubleTickets/tickets.cfm">(click here to jump to equipment tracking)</a></td>
				</tr>
				<cfoutput>
				<tr>
					<td class="linedrow">
						<table border="0" cellspacing="0" cellpadding="5">
							<tr>
								<td>Total: #countTotalTT.totalCount#</td>
								<td width="50">&nbsp;</td>
								<td>New: <cfif trim(countNewTT.newCount) is "">0<cfelse>#countNewTT.newCount#</cfif></td>
								<td width="50">&nbsp;</td>
								<td>Open: <cfif trim(countOpenedTT.openedCount) is "">0<cfelse>#countOpenedTT.openedCount#</cfif></td>
								<td width="50">&nbsp;</td>
								<td>Overdue: <cfif trim(countOverdueTT.overdueCount) is "" or trim(countOverdueTT.overdueCount) is 0>0<cfelse><span class="red">#countOverdueTT.overdueCount#</span></cfif></td>
								<td width="50">&nbsp;</td>
								<td>Overdue Pickup: <cfif trim(countOverduePickupTT.overdueCount) is "" or trim(countOverduePickupTT.overdueCount) is 0>0<cfelse><span class="red">#countOverduePickupTT.overdueCount#</span></cfif></td>
							</tr>
						</table>
					</td>
				</tr>
				</cfoutput>

				<cfif cookie.adminlogin is 1>
					<tr>
						<td><b>Private Label Sites</b> <a href="/privatelabel/index.cfm">(click here to jump to private label)</a></td>
					</tr>
					<cfoutput>
					<tr>
						<td class="nopadding">
							<table border="0" cellspacing="0" cellpadding="5">
								<tr>
									<td>Live: #getPrivateLabelTotals.totalCount#</td>
									<td width="50">&nbsp;</td>
									<td>In Development: #getPrivateLabelTotals.devCount#</td>
									<td width="50">&nbsp;</td>
									<td>Overdue: <cfif trim(getPrivateLabelTotals.overdueCount) is "" or trim(getPrivateLabelTotals.overdueCount) is 0>0<cfelse><span class="red">#getPrivateLabelTotals.overdueCount#</span></cfif></td>
									<td width="50">&nbsp;</td>
								</tr>
							</table>
						</td>
					</tr>
					</cfoutput>
				</cfif>

			</table>
		</td>
	</tr>
</table>


