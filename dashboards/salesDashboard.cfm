<link rel="stylesheet" type="text/css" href="/styles.css">

<cfparam name="departmentid" default="7">

<script type="text/javascript">
function checkUncheck(frm) {
	if (frm.checkStatus.value == 1) {
		var checkStatus = false;
		frm.checkStatus.value = 0;
	} else {
		var checkStatus = true;
		frm.checkStatus.value = 1;
	}
	if (frm.requestid.length > 1) {
		for (var i=0; i < frm.requestid.length; i++) {
			frm.requestid[i].checked = checkStatus;
		}
	} else {
		frm.requestid.checked = checkStatus;
	}
}
</script>

<cfparam name="SearchCreatedBy" default="">

<cfparam name="sort" default="dateDesc">

<cfswitch expression="#sort#">
	<cfcase value="createdByAsc"><cfset orderby = "createdBy ASC, requestDate ASC"></cfcase>
	<cfcase value="createdByDesc"><cfset orderby = "createdBy DESC, requestDate ASC"></cfcase>
	<cfcase value="dealerNumAsc"><cfset orderby = "dealerNumber ASC, requestDate ASC"></cfcase>
	<cfcase value="dealerNumDesc"><cfset orderby = "dealerNumber DESC, requestDate ASC"></cfcase>
	<cfcase value="dealerNameAsc"><cfset orderby = "dealerName ASC, requestDate ASC"></cfcase>
	<cfcase value="dealerNameDesc"><cfset orderby = "dealerName DESC, requestDate ASC"></cfcase>
	<cfcase value="dateAsc"><cfset orderby = "requestDate ASC"></cfcase>
	<cfcase value="dateDesc"><cfset orderby = "requestDate DESC"></cfcase>
	<cfcase value="requestAsc"><cfset orderby = "requestTypeName ASC, requestDate ASC"></cfcase>
	<cfcase value="requestDesc"><cfset orderby = "requestTypeName DESC, requestDate ASC"></cfcase>
	<cfcase value="statusAsc"><cfset orderby = "requestStatusName ASC, requestDate ASC"></cfcase>
	<cfcase value="statusDesc"><cfset orderby = "requestStatusName DESC, requestDate ASC"></cfcase>
</cfswitch>

<cfquery name="getMyName" datasource="#ds#">
	select firstname + ' ' + lastname as myName
	from admin_users 
	where adminuserid = #cookie.adminlogin# 
</cfquery>
<cfset myName = getMyName.myName>

<cfif searchCreatedBy is "">
	<cfset searchCreatedBy = myName>
</cfif>

<cfif isDefined("form.btnUpdateItems")>
	<cfinclude template="/formTracker/updateItems.cfm">
</cfif>
<cfquery name="getRequestStatus" datasource="#ds#">
	select * from requestTracking_requestStatus 
	order by displayOrder asc 
</cfquery>
<cfquery name="getRequests" datasource="#ds#">
	select requestTracking_requests.*, requestTracking_requestStatus.requestStatusName, requestTracking_requestTypes.requestTypeName 
	from requestTracking_requests 
	left join requestTracking_requestStatus on requestTracking_requests.requestStatus = requestTracking_requestStatus.requestStatusID 
	left join requestTracking_requestTypes on requestTracking_requests.requestType = requestTracking_requestTypes.requestTypeID 
	where 
	requestTracking_requests.departmentid = #departmentid# and 
	requestStatus IN (1,2) and 
	<cfif searchCreatedBy is not "All">
		createdby = '#SearchCreatedBy#' and 
	</cfif>
	1=1 
	order by #orderby#
</cfquery>

<cfquery name="getCreatedBy" datasource="#ds#">
	select distinct createdBy 
	from requestTracking_requests 
	where requestStatus IN (1,2) and departmentid = #departmentid# 
	order by createdBy asc 
</cfquery>

<cfset urlString = "searchCreatedBy=#searchCreatedBy#">

<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Sales Request Tracker</b></td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
		<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<form method="post" action="index.cfm" name="outputForm">
			<input type="hidden" name="checkStatus" value="0" />
			<tr>
				<td colspan="8" class="nopadding">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td colspan="3" class="linedrow"><a href="/formTracker/" style="text-decoration:underline;" class="normal">Click here to go to the Request Tracker</a></td>
					</tr>
					<tr>
						<td class="linedrow" colspan="2">
						<b>Filter By Creator:</b> 
						<select name="SearchCreatedBy" class="normal" style="vertical-align:middle;">
							<option <cfif searchCreatedBy is "All">selected</cfif> value="All">View All</option>
							<cfoutput>
							<option <cfif searchCreatedBy is myName>selected</cfif> value="#myName#">#myName#</option>
							</cfoutput>
							<cfoutput query="getCreatedBy">
								<cfif myName is not createdBy>
									<option <cfif createdBy is searchCreatedBy>selected</cfif> value="#createdBy#">#createdBy#</option> 
								</cfif>
							</cfoutput>
						</select>
						<input name="btnFilterRequests" type="submit" class="sidebar" style="vertical-align:middle;" value="Filter" />
						</td>
						<td class="linedrow"><input onclick="document.location='/formTracker/newRequest.cfm?backpage=/index.cfm';" type="button" class="sidebar" value="Create New Request" /></td>
					</tr>
					<!--- 
					<tr>
						<td class="linedrow">
						<b>Update Checked Items:</b> 
						<select name="requestStatusID" class="normal" style="vertical-align:middle;">
							<option value="0"></option>
							<cfoutput query="getRequestStatus">
								<option value="#requestStatusID#">#requestStatusName#</option>
							</cfoutput>
						</select>
						<input onclick="return confirm('Are you SURE you wish to perform this action? \n Clicking Apply will update the status of all checked items.');" name="btnUpdateItems" type="submit" class="sidebar" style="vertical-align:middle;" value="Apply" />
						</td>
						<td class="linedrow">&nbsp;</td>
						<td class="linedrow"><input onclick="document.location='/formTracker/newRequest.cfm?backpage=/index.cfm';" type="button" class="sidebar" value="Create New Request" /></td>
					</tr>
					--->
				</table>
				</td>
			</tr>
			<cfoutput>
			<tr>
				<td align="center" class="linedrow">&nbsp;</td>
				<!--- 
				<td align="center" class="linedrow"><a href="javascript:checkUncheck(document.outputForm);"><img border="0" src="/images/checkBox_clearbg.gif" alt="Check All/Uncheck All" width="23" height="26" /></a></td>
				--->
				<cfif sort is "createdByAsc"><cfset thisSort="createdByDesc"><cfelse><cfset thisSort = "createdByAsc"></cfif>
				<td class="linedrow"><b><a style="text-decoration:underline;" href="index.cfm?sort=#thisSort#&#urlString#">Created By</a></b></td>
				<cfif sort is "dealerNumAsc"><cfset thisSort="dealerNumDesc"><cfelse><cfset thisSort = "dealerNumAsc"></cfif>
				<td class="linedrow" nowrap="nowrap"><b><a style="text-decoration:underline;" href="index.cfm?sort=#thisSort#&#urlString#">Dealer ##</a></b></td>
				<cfif sort is "dealerNameAsc"><cfset thisSort="dealerNameDesc"><cfelse><cfset thisSort = "dealerNameAsc"></cfif>
				<td class="linedrow"><b><a style="text-decoration:underline;" href="index.cfm?sort=#thisSort#&#urlString#">Dealer Name</a></b></td>
				<cfif sort is "dateAsc"><cfset thisSort="dateDesc"><cfelse><cfset thisSort = "dateAsc"></cfif>
				<td align="center" class="linedrow"><b><a style="text-decoration:underline;" href="index.cfm?sort=#thisSort#&#urlString#">Date Submitted</a></b></td>
				<cfif sort is "requestAsc"><cfset thisSort="requestDesc"><cfelse><cfset thisSort = "requestAsc"></cfif>
				<td class="linedrow"><b><a style="text-decoration:underline;" href="index.cfm?sort=#thisSort#&#urlString#">Request</a></b></td>
				<cfif sort is "statusAsc"><cfset thisSort="statusDesc"><cfelse><cfset thisSort = "statusAsc"></cfif>
				<td class="linedrow"><b><a style="text-decoration:underline;" href="index.cfm?sort=#thisSort#&#urlString#">Status</a></b></td>
			</tr>
			</cfoutput>
			<cfoutput query="getRequests">
			<cfif getRequests.currentRow mod 2 is 0>
				<cfset bgc = "##EEEEEE">
			<cfelse>
				<cfset bgc = "##FFFFFF">
			</cfif>
			<tr>
				<td align="center" bgcolor="#bgc#" class="linedrow">
				<a href="/formTracker/details.cfm?rid=#requestID#&sort=#sort#&#urlString#">
				<img border="0" src="/images/manifyingglass.gif" alt="View Details" width="15" height="15" />
				</a>
				</td>
				<!--- 
				<td align="center" bgcolor="#bgc#" class="linedrow"><input type="checkbox" name="requestid" value="#requestid#" /></td>
				--->
				<td bgcolor="#bgc#" class="linedrow">#createdBy#</td>
				<td bgcolor="#bgc#" class="linedrow">#dealerNumber#</td>
				<td bgcolor="#bgc#" class="linedrow">#dealerName#</td>
				<td align="center" bgcolor="#bgc#" class="linedrow">#dateformat(requestDate,'mm/dd/yyyy')#</td>
				<td bgcolor="#bgc#" class="linedrow">#requestTypeName#</td>
				<td bgcolor="#bgc#" class="linedrow">
				<cfif requestStatusName is "Completed">
					Completed #dateformat(completedDate,'mm/dd/yyyy')#
				<cfelse>
					#requestStatusName#
				</cfif>
				</td>
			</tr>
			</cfoutput>
			</form>
		</table>
		</td>
	</tr>
</table>
