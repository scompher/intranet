
<!--- get linkfiles --->
<cfquery name="getinfo" datasource="#ds#">
	select * from migrationportal_links
	where dealerid = #dealerid#
</cfquery>

<cfloop query="getinfo">
	<cfset filepath = "e:\websites\intranet\migration\files">
	<cfif trim(getinfo.attachment) is not "">
		<cfset theFile = "#filepath#\#getinfo.attachment#">
		<cfif fileExists("#theFile#")>
			<cffile action="delete" file="#theFile#">
		</cfif>
	</cfif>
</cfloop>

<!--- delete info --->
<cfquery name="deleteInfo" datasource="#ds#">
	begin
		delete from migrationportal_dealers
		where dealerid = #dealerid#
	end
	begin
		delete from migrationportal_links
		where dealerid = #dealerid#
	end
</cfquery>

<cflocation url="index.cfm">

