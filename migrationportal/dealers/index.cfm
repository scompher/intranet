
<link rel="stylesheet" type="text/css" href="../../styles.css">

<cfquery name="getdealers" datasource="#ds#">
	select migrationportal_dealers.*, admin_users.firstname, admin_users.lastname, migrationportal_links.linkName, migrationportal_links.linkURL, migrationportal_links.attachment, migrationportal_links.linkid
	from migrationportal_dealers 
	left join admin_users on migrationportal_dealers.acctmanagerid = admin_users.adminuserid
	left join migrationportal_links on migrationportal_dealers.dealerid = migrationportal_links.dealerid
	order by migrationportal_dealers.dealerid asc
</cfquery>

<div align="center" class="normal">
<table border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Sales portal dealer maintenance</b></td>
	</tr>
	<form method="post" action="add.cfm">
	<tr>
		<td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td align="center"><b>Action</b></td>
					<td><b>Dealer #</b></td>
					<td><b>Dealer Name </b></td>
					<td><b>Passcode</b></td>
					<td align="center"><b>Number of Visits</b></td>
					<td align="center"><b>Expires</b></td>
					<td align="center"><b>Account Manager</b> </td>
					<td align="left"><b>Links</b></td>
				</tr>
				<cfoutput query="getdealers" group="dealerid">
				<cfset daysToExpire = datediff("d",dateformat(now(),'mm/dd/yyyy'), expirationDate)>
				<cfif daysToExpire lte 2>
				<tr style="color:##DF262B; font-weight:bold;">
				<cfelseif daysToExpire is 3>
				<tr style="font-weight:bold;">
				<cfelse>
				<tr>
				</cfif>
					<td valign="top" align="center" class="linedrow" style="color:##000000; font-weight:normal">
					<a href="edit.cfm?dealerid=#dealerid#">edit</a> | 
					<a href="javascript: if (confirm('Are you sure you wish to delete this dealer?')) document.location='delete.cfm?dealerid=#dealerid#';">delete</a>
					</td>
					<td align="center" valign="top" class="linedrow">#dealernumber#</td>
					<td valign="top" class="linedrow">#dealername#</td>
					<td valign="top" class="linedrow">#passcode#</td>
					<td align="center" valign="top" class="linedrow">#numberOfVisits#</td>
					<td align="center" valign="top" class="linedrow">#dateformat(expirationDate, 'mm/dd/yyyy')# <cfif daysToExpire is 3>!</cfif></td>
					<td align="center" valign="top" class="linedrow">#firstname# #lastname#</td>
					<td valign="top" class="linedrow" style="padding:0px">
					<table width="100%" border="0" cellspacing="0" cellpadding="3">
						<cfoutput>
						<cfif trim(linkURL) is not ""><cfset linkto = linkURL><cfelse><cfset linkto = "#copalinkURL#/migration/files/#attachment#"></cfif>
						<tr>
							<td><a style="text-decoration:underline;" target="_blank" href="#linkto#">#linkName#</a></td>
						</tr>
						</cfoutput>
					</table>
					</td>
				</tr>
				</cfoutput>
				<tr>
					<td colspan="6">
						<input style="height:20px" name="Submit" type="submit" class="sidebar" value="Add Dealer">
					</td>
				</tr>
			</table>
		</td>
	</tr>
	</form>
</table>
<br />
<a href="/">Return to Intranet</a>
</div>
