using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Xml;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.IO.Compression;
using System.Text;
using System.Data;

[WebService(Namespace = "com.cops.uccaudits")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]


public class UccAuditServices : System.Web.Services.WebService
{
    string connectionString208 = ConfigurationManager.ConnectionStrings["LydiaBilling208"].ConnectionString;

    string connectionString20 = ConfigurationManager.ConnectionStrings["LydiaBilling"].ConnectionString;

    public UccAuditServices()
    {
    }


    //Copies the service code table from .20 > .208
    [Obsolete ("This method is no longer needed.")]
    [WebMethod]
    public bool CopyServiceConversion()
    {
        //11/1/17 - SC & CM
        //The LydiaBilling db on .20 is no longer used.  The d/e pages for maintaining the service conversion table are now updating the .208 db directly so this is no longer needed.
        //Copied data for the final time on 11/1/17
        return true;

        /*
        List<String> statements = new List<string>();
        string values = "";

        using (SqlConnection connection = new SqlConnection(connectionString20))
        {
            string sql = "SELECT ID, Vendor, Item, Description, ServiceCode, Ignore, VendorID FROM serviceConversion WHERE serviceCode IS NOT NULL";            
            using (SqlCommand cmd = new SqlCommand(sql, connection))
            {
                connection.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                int n = 0;

                while (reader.Read())
                {

                    if (n > 0)
                        values += ", ";

                    n = n + 1;
                    values += string.Concat("(", reader.GetInt32(reader.GetOrdinal("ID")), ",");

                    int cid = reader.GetOrdinal("Vendor");
                    values += string.Concat(reader.IsDBNull(cid) ? "NULL" : SQuoteTrim(reader.GetString(cid)), ",");

                    cid = reader.GetOrdinal("Item");
                    values += string.Concat(reader.IsDBNull(cid) ? "NULL" : SQuoteTrim(reader.GetString(cid)), ",");

                    cid = reader.GetOrdinal("Description");
                    values += string.Concat(reader.IsDBNull(cid) ? "NULL" : SQuoteTrim(reader.GetString(cid)), ",");

                    values += string.Concat(reader.GetInt32(reader.GetOrdinal("ServiceCode")), ",");

                    if (reader.IsDBNull(reader.GetOrdinal("Ignore")))
                    {
                        values += string.Concat("NULL", ",");
                    }
                    else
                    {
                        values += string.Concat(reader.GetBoolean(reader.GetOrdinal("Ignore")) == true ? 1 : 0, ",");
                    }

                    cid = reader.GetOrdinal("VendorID");
                    values += string.Concat(reader.IsDBNull(cid) ? "NULL" : reader.GetInt32(cid).ToString(), ")");

                    if (n == 800)
                    {
                        values = "INSERT INTO serviceConversion  VALUES " + values;
                        statements.Add(values);
                        values = "";
                        n = 0;
                    }

                }
            }
            
        }

        values = "INSERT INTO serviceConversion VALUES " + values;
        statements.Add(values);
            
        using (SqlConnection connection2 = new SqlConnection(connectionString208))
        {
            connection2.Open();
            using (SqlTransaction transaction = connection2.BeginTransaction())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = connection2;
                cmd.Transaction = transaction;
                cmd.CommandText = "TRUNCATE TABLE serviceConversion";
                cmd.ExecuteNonQuery();

                foreach (String s in statements)
                {
                    cmd.CommandText = s;
                    cmd.ExecuteNonQuery();
                }

                transaction.Commit();
            }
        }
        
        return true;
        */
    }

    [WebMethod]
    public DataSet AccountLookUp(string account)
    {
        string sql = string.Concat("EXEC csp_GetInfoFromTransmitter '", account, "'");
        return GetDataSet(sql);
    }

    [WebMethod]
    public void Initialize(string system)
    {
        string sql = "";

        switch (system.ToLower())
        {
            case "alarmnet":
                sql = "TRUNCATE TABLE tblAlarmNet";
                break;
            case "alarm.com":
                sql = "TRUNCATE TABLE tblAlarmCom";
                break;
            case "connect 24":
            case "connect24":
                sql = "TRUNCATE TABLE tblConnect24";
                break;
            case "telguard":
                sql = "TRUNCATE TABLE tblTelGuard";
                break;
            case "uplink":
                sql = "TRUNCATE TABLE tblUplink";
                break;
            default:
                throw new InvalidOperationException(system + " is not defined");
        }

        using (SqlConnection connection = new SqlConnection(connectionString208))
        {
            connection.Open();
            using (SqlCommand cmd = new SqlCommand(sql, connection))            
            {
                cmd.ExecuteNonQuery();
            }
        }
    }

    [WebMethod]
    public void Upload(string val)
    {
      var bt = System.Convert.FromBase64String(val);
      string sql = Unzip(bt);
       
       using (SqlConnection connection = new SqlConnection(connectionString208))
       {
            connection.Open();
            using (SqlCommand cmd = new SqlCommand(sql, connection))
            {
                cmd.ExecuteNonQuery();
            }
       }
    }
    
    [WebMethod]
    public DataSet GetMissingAccounts(string system)
    {
        string sql = "";
        
        switch (system.ToLower())
        {
          case "alarmnet":
            sql = "EXEC csp_AlarmNet_Missing";
            break;
          case "alarm.com":
            sql = "EXEC csp_AlarmCom_Missing";
                break;
        case "connect 24":
        case "connect24":
            sql = "EXEC csp_Connect24_Missing";
            break;
          case "telguard":
            sql = "EXEC csp_TelGuard_Missing";
            break;
          case "uplink":
            sql = "EXEC csp_Uplink_Missing";
            break;              
          default:
            throw new InvalidOperationException(system + " is not defined");
        }
        
        return GetDataSet(sql);
    }
    
    [WebMethod]
    public DataSet GetCancelledAccounts(string system)
    {       
       string sql = "";
        
        switch (system.ToLower())
        {
          case "alarmnet":
            sql = "EXEC csp_AlarmNet_CancelledServices";
            break;
          case "alarm.com":
            sql = "EXEC csp_AlarmCom_CancelledServices";
            break;
            case "connect 24":
            case "connect24":
            sql = "EXEC csp_Connect24_CancelledServices";
            break;
          case "telguard":
            sql = "EXEC csp_TelGuard_CancelledServices";
            break;
          case "uplink":
            sql = "EXEC csp_Uplink_CancelledServices";
            break;              
          default:
            throw new InvalidOperationException(system + " is not defined");
        }
        
        return GetDataSet(sql);
    }    
    
    [WebMethod]
    public DataSet GetMissingServices(string system, bool reportOnly)        
    {       
       string sql = "";
        
        switch (system.ToLower())
        {
          case "alarmnet":
            sql = "EXEC csp_AlarmNet_MissingServiceCodes";
            break;
          case "alarm.com":
            sql = "EXEC csp_AlarmCom_MissingServiceCodes";            
            break;
          case "connect 24":
          case "connect24":
            sql = "EXEC csp_Connect24_MissingServiceCodes";            
            break;
          case "telguard":
            sql = "EXEC csp_TelGuard_MissingServiceCodes";            
            break;
          case "uplink":                
            sql = "EXEC csp_Uplink_MissingServiceCodes";            
            break;              
          default:
            throw new InvalidOperationException(system + " is not defined");
        }
        
        if (reportOnly == true)
          sql += " @ReportOnly=1";
        
        return GetDataSet(sql);
    }   
    
    [WebMethod]
    public DataSet GetNotOnBill(string system)        
    {       
       string sql = "";
        
        switch (system.ToLower())
        {
          case "alarmnet":
            sql = "EXEC csp_AlarmNet_NotOnBill";
            break;
          case "alarm.com":
            sql = "EXEC csp_AlarmCom_NotOnBill";
            break;
            case "connect 24":
            case "connect24":
            sql = "EXEC csp_Connect24_NotOnBill";
            break;
          case "telguard":
            sql = "EXEC csp_TelGuard_NotOnBill";
            break;
          case "uplink":
            sql = "EXEC csp_Uplink_NotOnBill";
            break;              
          default:
            throw new InvalidOperationException(system + " is not defined");
        }
        
        return GetDataSet(sql);
    }

    [WebMethod]
    public DataSet GetNotOnBillCancelled(string system)
    {
        string sql = "";

        switch (system.ToLower())
        {
            case "alarmnet":
                sql = "EXEC csp_AlarmNet_NotOnBill_Cancelled";
                break;
            case "alarm.com":
                sql = "EXEC csp_AlarmCom_NotOnBill_Cancelled";
                break;
            case "connect 24":
            case "connect24":
                sql = "EXEC csp_Connect24_NotOnBill_Cancelled";
                break;
            case "telguard":
                sql = "EXEC csp_TelGuard_NotOnBill_Cancelled";
                break;
            case "uplink":
                sql = "EXEC csp_Uplink_NotOnBill_Cancelled";
                break;
            default:
                throw new InvalidOperationException(system + " is not defined");
        }

        return GetDataSet(sql);
    }

    [WebMethod]
    public void DeleteNotOnBill(string system)
    {
        string sql = "";
        switch (system.ToLower())
        {
            case "alarmnet":
                sql = "EXEC csp_AlarmNet_NotOnBill_Delete";
                break;
            case "alarm.com":
                sql = "EXEC csp_AlarmCom_NotOnBill_Delete";
                break;
            case "connect 24":
            case "connect24":
                sql = "EXEC csp_Connect24_NotOnBill_Delete";
                break;
            case "telguard":
                sql = "EXEC csp_TelGuard_NotOnBill_Delete";
                break;
            case "uplink":
                sql = "EXEC csp_Uplink_NotOnBill_Delete";
                break;
            default:
                throw new InvalidOperationException(system + " is not defined");
        }

        using (SqlConnection connection = new SqlConnection(connectionString208))
        {
            connection.Open();
            using (SqlCommand cmd = new SqlCommand(sql, connection))
            {
                cmd.ExecuteNonQuery();
            }
        }
    }



    private DataSet GetDataSet(string sql)
    {
        DataSet dSet = new DataSet();
        using (SqlConnection connection = new SqlConnection(connectionString208))
        {
          connection.Open();
          using (SqlDataAdapter adapter = new SqlDataAdapter(sql, connection))
          {
            adapter.SelectCommand.CommandTimeout = 300;
            adapter.Fill(dSet,"Results");
          }
        }
        return dSet;
    }
    
    
    private string Unzip(byte[] bytes)
    {
        using (var msi = new MemoryStream(bytes))
        using (var mso = new MemoryStream())
        {
            using (var gs = new GZipStream(msi, CompressionMode.Decompress))
            {
                gs.CopyTo(mso);
            }
            return Encoding.UTF8.GetString(mso.ToArray());
        }
    }

    public static string SQuoteTrim(object val)
    {
        try
        {
            string buf = Convert.ToString(val);
            return string.Concat("'", buf.Trim().Replace("'", "''"), "'");
        }
        catch
        {

        }
        return string.Concat("'", val, "'");

    }


}