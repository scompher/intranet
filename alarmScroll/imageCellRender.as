﻿import mx.core.UIComponent;
class imageCellRender extends UIComponent {
	var img:MovieClip;
	var url:String;
	var getDataLabel:Function;
	function IconCellRenderer() {
		
	}
	function createChildren(Void):Void {
		size();
	}
	// the setValue function is called  when the component
	// is first drawn and whenever a user interacts with the datagrid
	function setValue(str:String, item:Object, sel:Boolean):Void {
		img._visible = (item[getDataLabel()] != undefined);
		// do not render a cell if the data label is undefined
		if(item[getDataLabel()] != undefined){
		// do not re-draw cell if data label is unchanged
		if(item[getDataLabel()] != this.url){
		// load the appropriate jpg into our img MovieClip instance
			var imgName = "";
			
			if (item[getDataLabel()] == "B") imgName = "btnPolice.gif";
			if (item[getDataLabel()] == "F") imgName = "btnFire.gif";
			if (item[getDataLabel()] == "M") imgName = "btnMedical.gif";
			if (item[getDataLabel()] == "L") imgName = "btnLowLevel.gif";
			if (item[getDataLabel()] == "S") imgName = "btnSupervisory.gif";
			if (item[getDataLabel()] == "RA") imgName = "btnRunner.gif";

			img.loadMovie(imgName);
	// if the user interacts with a cell we need to see if the image needs to be redrawn
	// if this.url is unchanged this code tells the datagrid not to redraw
			this.onEnterFrame = function(){
	//make sure that the image has fully loaded
	//set this.url to image name returned for the corresponding cell
	//so that we can check if data label has changed
				if(img.getBytesLoaded() >= img.getBytesTotal()) {
					this.url = item[getDataLabel()];
				}
				}
				}
			}
		}
// use the size function to postion our empty MovieClip img
	function size(Void) : Void
	{
		// img._y = -48;
		// img._x = 40;
		img._y = 3;
		img._x = 5;
	}	
}
