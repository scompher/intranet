<cfsetting showdebugoutput="no">

<style>
TABLE {font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#FFFFFF;}
A.menunav{font-size : 8pt; color : FFFFFF; font-family : Verdana, sans; text-decoration : none; font-weight : normal;}
A.menunav_yellow{font-size : 8pt; color : FFFF99; font-family : Verdana, sans; text-decoration : none; font-weight : normal;}
A.menunav:hover{font-size : 8pt; color : C0C0C0; font-family : Verdana, sans; text-decoration : none; font-weight : normal;}
A.link{font-size : 9pt; color : b9b9ff; font-family : Verdana, sans; text-decoration : none; font-weight : normal;}
A.link:hover{font-size : 9pt; color : C0C0C0; font-family : Verdana, sans; text-decoration : none; font-weight : normal;}
.rollovertype{font-size : 16px; color : #FFFFFF; font-family : Verdana, sans; text-decoration : none; font-weight : bold;}
.footer{font-size : 8pt; color : #C0C0C0; font-family : Verdana, sans; text-decoration : none; font-weight : normal;}
.regtext{font-size : 8pt; color : #000000; font-family : Verdana, sans; text-decoration : none; font-weight : normal;}
.textlink{font-size : 8pt; color : #000099; font-family : Verdana, sans; text-decoration : none; font-weight : normal;}
.hdrtext{font-size : 12pt; color : #CCCCFF; font-family : Verdana, sans; text-decoration : none; font-weight : 900;}
.subhdrtext{font-size : 10pt; color : #FFFFFF; font-family : Verdana, sans; text-decoration : none; font-weight : 900;}
.formtext{font-size : 9pt; color : #FFFFFF; font-family : Verdana, sans; text-decoration : none; font-weight : normal;}
.formtextblack{font-size : 9pt; color : #000000; font-family : Verdana, sans; text-decoration : none; font-weight : normal;}
.formtextyellow{font-size : 9pt; font-family : Verdana, sans; text-decoration : none; font-weight : normal; 	color: #FFFF00;}
.formtextgreen{font-size : 9pt; font-family : Verdana, sans; text-decoration : none; font-weight : normal; 	color: #00CC00;}
.formtextyellowsmall{font-size : 11px; font-family : Verdana, sans; text-decoration : none; font-weight : normal; 	color: #FFFF00;}
.formtextgreensmall{font-size : 11px; font-family : Verdana, sans; text-decoration : none; font-weight : normal; 	color: #00CC00;}
.formtextbold{font-size : 9pt; color : #FFFFFF; font-family : Verdana, sans; text-decoration : none; font-weight : bold;}
.alerttext{font-size : 10pt; color : #cc0033; font-family : Verdana, sans; text-decoration : none; font-weight : 900;}
.printhdrtext{font-size : 9pt; color : #000000; font-family : Verdana, sans; text-decoration : none; font-weight : 900;}
.printtext{font-size : 7pt; color : #000000; font-family : Verdana, sans; text-decoration : none; font-weight : normal;}
.printtextbold{font-size : 7pt; color : #000000; font-family : Verdana, sans; text-decoration : none; font-weight : bold;}
.small{font-size : 8pt; color : #000000; font-family : Verdana, sans; text-decoration : none; font-weight : normal;}
.menutxt{font-size : 8pt; color : #FFFFFF; font-family : Verdana, sans; text-decoration : none; font-weight : normal;}
.buttontxt{font-size : 9pt; color : #000000; font-family : Verdana, sans; text-decoration : none; font-weight : normal;}
.menu {font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #DDDDDD; text-decoration: none;}
.menulink {font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #DDDDDD; text-decoration: none;}
.menulinkdark {font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; text-decoration: none;}
.alerttextlarge {font-family:Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold; color:#FFFFFF; border:solid thin #FFFFFF; background-color:#FF0000; padding:5px; text-shadow:1px 1px #000000;}
.bluebutton {
	border: 3px double #999999;
	border-top-color: #CCCCCC;
	border-left-color: #CCCCCC;
	padding: 0.25em;
	background-color:#9FA4C4;
	background-image: url(images/background_form_element.gif);
	background-repeat: repeat-x;
	color: #333333;
	font-size: 75%;
	font-weight: bold;
	font-family: Verdana, Helvetica, Arial, sans-serif;
	font-size:10px; 
	width:170px;
}
.button {
	border: 3px double #999999;
	border-top-color: #CCCCCC;
	border-left-color: #CCCCCC;
	padding: 0.25em;
	background-color: #EEEEEE;
	background-image: url(images/background_form_element.gif);
	background-repeat: repeat-x;
	color: #333333;
	font-size: 75%;
	font-weight: bold;
	font-family: Verdana, Helvetica, Arial, sans-serif;
	font-size:10px
}
.nopadding {padding:0px}
.required {color:#FF0000;}
.linedRowBlack {border-bottom:1px #000000 solid; color:#000000;}
</style>

<link rel="stylesheet" href="/jqwidgets/jqwidgets/styles/jqx.base.css" type="text/css" />
<link rel="stylesheet" href="/jqwidgets/jqwidgets/styles/jqx.darkblue.css" type="text/css" />
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/themes/smoothness/jquery-ui.css" />
<link rel="stylesheet" href="rowcolors.css" type="text/css"/>
<link rel="stylesheet" type="text/css" href="/styles.css">
<cfinclude template="/jqueryincludes.cfm">
<script type="text/javascript" src="jqwidgets/jqxcore.js"></script>
<script type="text/javascript" src="jqwidgets/jqxdata.js"></script> 
<script type="text/javascript" src="jqwidgets/jqxbuttons.js"></script>
<script type="text/javascript" src="jqwidgets/jqxscrollbar.js"></script>
<script type="text/javascript" src="jqwidgets/jqxmenu.js"></script>
<script type="text/javascript" src="jqwidgets/jqxcheckbox.js"></script>
<script type="text/javascript" src="jqwidgets/jqxlistbox.js"></script>
<script type="text/javascript" src="jqwidgets/jqxdropdownlist.js"></script>
<script type="text/javascript" src="jqwidgets/jqxgrid.js"></script>
<script type="text/javascript" src="jqwidgets/jqxgrid.sort.js"></script> 
<script type="text/javascript" src="jqwidgets/jqxgrid.pager.js"></script> 
<script type="text/javascript" src="jqwidgets/jqxgrid.selection.js"></script> 
<script type="text/javascript" src="jqwidgets/jqxgrid.edit.js"></script>
<script type="text/javascript" src="jqwidgets/jqxgrid.columnsresize.js"></script>
<script type="text/javascript" src="jqwidgets/jqxdata.export.js"></script> 
<script type="text/javascript" src="jqwidgets/jqxgrid.export.js"></script>
<script type="text/javascript" src="jqwidgets/jqxgrid.filter.js"></script>
<script type="text/javascript" src="functions.js"></script>
<cfapplication name="scrollingList" sessionmanagement="Yes">

<style>
.jqx-grid-empty-cell {background-color:#000066; color:#FFFFFF;}
.jqx-grid-column-header {background-color:#bebfd2;}
.jqx-grid-pager {background-color:#bebfd2;}
.jqx-grid-cell {border-color:#bebfd2;}
.jqx-column-resize-line {border-color:#000000;}
.jqx-grid-cell-selected {background-color:#bebfd2;}
</style><br>

<div style="background-color:000066;" id="dialog"></div>

<cfparam name="hoursBack" default="0">
<cfparam name="alarmType" default="">

<cfif isDefined("btnGoBack")>
	<!--- <cfset Session.startDateTime = CreateODBCDateTime( dateAdd('h','-#hoursBack#',now()) )>  --->
	<cfset backTime = dateadd('h','-#hoursBack#',now())>
	<cfset Session.startDateTime = CreateODBCDateTime(backTime)>
</cfif>

<cfset session.dealerNumber = form.dealerNumber>
<cfif not isDefined("Session.startDateTime") or isDefined("form.btnClearData")>
	<cfset hoursBack = 0>
	<!--- <cfset Session.startDateTime = CreateODBCDateTime(now())> --->
	<cfset Session.startDateTime = "#dateformat(now(),'mm/dd/yyyy')# #timeformat(now(),'HH:mm:ss')#">
</cfif>

<cfif not isDefined("session.alarmType")>
	<cfset session.alarmType = form.alarmType>
<cfelse>
	<cfset session.alarmType = form.alarmType>
</cfif>

<form method="post" action="index2.cfm">
<cfoutput>
<input type="hidden" name="dealerNumber" value="#form.dealerNumber#">
<input type="hidden" name="alarmType" value="#form.alarmType#">
</cfoutput>

<cfoutput>
<table border="0" width="100%" style="color:white; font-family:Arial, Helvetica, sans-serif; font-size:14px;">
	<tr>
		<td width="35%">
		Go back 
		<select name="hoursBack">
			<cfloop from="0" to="24" index="i">
				<option <cfif hoursBack is i>selected</cfif> value="#i#">#i#</option>
			</cfloop>
		</select>
		hours 
		<input type="submit" name="btnGoBack" value="Go">&nbsp;<input type="submit" name="btnClearData" value="Clear Data">
		</td>
		<td align="left">Showing Alarms from #dateformat(session.startdatetime,'mm/dd/yyyy')# #timeformat(session.startdatetime,'hh:mm:ss tt')# Eastern </td>
	</tr>
</table>
</cfoutput>
</p>
</form>

<body class="body">	

	<button class="dashbutton" id="showdashboard">Open/Close Dashboard</button>
		<div id="dashboard" style="width:600px;">
			<input id="button" type="button" value="Auto Resize Columns" />
			<input type="button" value="Export to Excel" id='excelExport' />
			<input type="button" value="Export to CSV" id='csvExport' />
			<input type="button" value="Export to PDF" id='pdfExport' />
			<div id="jqxlistbox"></div>	
		</div>
		<div class="table" id="jqxgrid">	
        </div>
     </div>
<br>
</body>
