<!---<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/themes/smoothness/jquery-ui.css" />
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js"></script>--->
<cfsetting showdebugoutput="no">
<link rel="stylesheet" href="/jqwidgets/jqwidgets/styles/jqx.base.css" type="text/css" />
<link rel="stylesheet" href="/jqwidgets/jqwidgets/styles/jqx.darkblue.css" type="text/css" />
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/themes/smoothness/jquery-ui.css" />
<link rel="stylesheet" href="rowcolors.css" type="text/css"/>
<link rel="stylesheet" type="text/css" href="/styles.css">
<cfinclude template="/jqueryincludes.cfm">
<script type="text/javascript" src="jqwidgets/jqxcore.js"></script>
<script type="text/javascript" src="jqwidgets/jqxdata.js"></script> 
<script type="text/javascript" src="jqwidgets/jqxbuttons.js"></script>
<script type="text/javascript" src="jqwidgets/jqxscrollbar.js"></script>
<script type="text/javascript" src="jqwidgets/jqxmenu.js"></script>
<script type="text/javascript" src="jqwidgets/jqxcheckbox.js"></script>
<script type="text/javascript" src="jqwidgets/jqxlistbox.js"></script>
<script type="text/javascript" src="jqwidgets/jqxdropdownlist.js"></script>
<script type="text/javascript" src="jqwidgets/jqxgrid.js"></script>
<script type="text/javascript" src="jqwidgets/jqxgrid.sort.js"></script> 
<script type="text/javascript" src="jqwidgets/jqxgrid.pager.js"></script> 
<script type="text/javascript" src="jqwidgets/jqxgrid.selection.js"></script> 
<script type="text/javascript" src="jqwidgets/jqxgrid.edit.js"></script>
<script type="text/javascript" src="jqwidgets/jqxgrid.columnsresize.js"></script>
<script type="text/javascript" src="jqwidgets/jqxdata.export.js"></script> 
<script type="text/javascript" src="jqwidgets/jqxgrid.export.js"></script>
<script type="text/javascript" src="jqwidgets/jqxgrid.filter.js"></script>
<script type="text/javascript" src="functions.js"></script>
<cfapplication name="scrollingList" 
sessionmanagement="Yes">
	<cfset session.dealerNumber = form.dealerNumber>
	<cfif not isDefined("Session.startDateTime") or isDefined("form.btnClearData")>
		<cfset Session.startDateTime = CreateODBCDateTime(now())>
	</cfif>
<style>
.jqx-grid-empty-cell {background-color:#000066; color:#FFFFFF;}
.jqx-grid-column-header {background-color:#bebfd2;}
.jqx-grid-pager {background-color:#bebfd2;}
.jqx-grid-cell {border-color:#bebfd2;}
.jqx-column-resize-line {border-color:#000000;}
.jqx-grid-cell-selected {background-color:#bebfd2;}
</style><br>


<div style="background-color:000066;" id="dialog"></div>
<form method="post" action="indexSC.cfm">
<cfoutput>
<input type="hidden" name="dealerNumber" value="#form.dealerNumber#">
</cfoutput>
<input type="submit" name="btnClearData" value="Clear Data">
</form>
<body class="body">	

	<button class="dashbutton" id="showdashboard">Open/Close Dashboard</button>
		<div id="dashboard" style="width:600px;">
			<input id="button" type="button" value="Auto Resize Columns" />
			<input type="button" value="Export to Excel" id='excelExport' />
			<input type="button" value="Export to CSV" id='csvExport' />
			<input type="button" value="Export to PDF" id='pdfExport' />
			<div id="jqxlistbox"></div>	
		</div>
		<div class="table" id="jqxgrid">	
        </div>
     </div>
<br>
/body>
