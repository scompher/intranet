
<!--- reset session --->
<cflock scope="Session" timeout="5" type="exclusive"> 
	<cfset structclear(session)> 
</cflock>
<cfcookie name="CFID" value="empty" expires="NOW">
<cfcookie name="CFTOKEN" value="empty" expires="NOW">
<!--- reset session --->

<cfparam name="dealernumber" default="">
<cfparam name="type" default="1,2,3">

<cfset session.alarmType = "">
<cfset session.dealerNumber = "">

<link rel="stylesheet" type="text/css" href="/styles.css">

<body onLoad="document.mainform.dealerNumber.focus();">
<div align="center" class="normal">
<form method="post" action="index2.cfm" name="mainform">
	<table border="0" cellspacing="0" cellpadding="5">
		<cfif isdefined("url.invalidLogin")>
		<tr align="center">
			<td colspan="2" style="font-weight:bold; color:#FF0000;">Invalid Dealer Number or Passcode<br><br></td>
		</tr>
		</cfif>
		<tr>
			<td colspan="2" align="center" valign="top" nowrap bgcolor="#FFFFFF" class="header">Scrolling List of Alarms</td>
			</tr>
		<tr>
			<td align="right" valign="top" nowrap class="greyrowleft">&nbsp;</td>
			<td nowrap class="greyrowright">&nbsp;</td>
		</tr>
		<tr>
			<td align="right" nowrap class="greyrowleft">Dealer Number: </td>
			<td nowrap class="greyrowright">
				<input type="text" name="dealerNumber" value="<cfoutput>#dealernumber#</cfoutput>">
			</td>
		</tr>
		<input type="hidden" name="passcode" value="99999">
		<!--- 
		<tr>
			<td align="right" nowrap class="greyrowleft">Passcode:</td>
			<td nowrap class="greyrowright">
				<input type="text" name="passcode" value="">
			</td>
		</tr>
		--->
		<tr>
			<td align="right" valign="top" nowrap class="greyrowleft">Options:</td>
			<td nowrap class="greyrowright" style="padding:0px">
				<table border="0" cellpadding="3" cellspacing="0" class="grey">
					
					<tr>
						<td>
							<input name="alarmType" type="checkbox" value="1" <cfif listfind(type,1) is not 0>checked</cfif> >
						</td>
						<td>Dispatcher Handled Alarms</td>
					</tr>
					<tr>
						<td>
							<input name="alarmType" type="checkbox" value="2" <cfif listfind(type,2) is not 0>checked</cfif> >
						</td>
						<td>Logged In Alarms</td>
					</tr>
					<tr>
						<td>
							<input name="alarmType" type="checkbox" value="3" <cfif listfind(type,3) is not 0>checked</cfif> >
						</td>
						<td>Test Results</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="2" align="center" class="greyrowbottom">
				<input name="Submit" type="submit" class="sidebar" value="View Scrolling Alarms" style="padding:3px" />
			</td>
		</tr>
	</table>
</form>
<br>
<a href="/index.cfm" style="text-decoration:underline;">Return to Intranet Menu</a>
</div>
</body>
