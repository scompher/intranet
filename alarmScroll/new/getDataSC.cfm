
<cfsetting showdebugoutput="no">
<cfheader name="Content-Type" value="application/json">
<cfapplication name="scrollingList" sessionmanagement="Yes">

<cfset dhflag = false>
<cfset alarmtypes = "">

<cfif listfind(session.alarmType,1) is not 0><cfset dhflag = true></cfif>
<cfif listfind(session.alarmType,2) is not 0><cfset alarmtypes = listappend(alarmtypes,5)></cfif>
<cfif listfind(session.alarmType,3) is not 0><cfset alarmtypes = listappend(alarmtypes,6)></cfif>

<!--- <cfoutput> --->
<cfquery name="limitResults" datasource="copalink_production">
	select top 1000 * 
	from recent_alarms with (nolock)
	where 1=1 
	and dealernumber like '#session.dealerNumber#%'
	<cfif findnocase("{ts",session.startDateTime) is not 0>
		and dateTimeSaved >= #session.startDateTime# 
	<cfelse>
		and dateTimeSaved >= '#session.startDateTime#' 
	</cfif>
	and (
		<cfif dhflag is true>dispatcherHandled = 'Y'</cfif>
		<cfif dhflag is true and alarmtypes is not ""> or </cfif>
		<cfif alarmtypes is not "">status IN (#alarmtypes#)</cfif> 
	) 
	order by alarmid desc   
</cfquery>
<!--- </cfoutput> --->

[<cfloop query="limitResults">

<cfset newName = trim(limitResults.name)>
<cfset newName = replace(newName,"#chr(34)#","\#chr(34)#","all")>

<cfset newAddress = limitResults.address>
<cfset newAddress = replace(newAddress,"#chr(34)#","\#chr(34)#","all")>

<cfset newCode = limitResults.alarmCode>
<cfset newCode = replace(newCode,"\","\#chr(92)#","all")>

<cfset newCondition = limitResults.condition>
<cfset newCondition = replace(newCondition,"#chr(34)#","\#chr(34)#","all")>

<!--- 
cfset newName = replace(newName,"'","\'","all")>
<cfset newName = replace(newName,"#chr(34)#","\#chr(34)#","all")>
<cfset newName = replace(newName,"#chr(9)#","","all")>

<cfset newAddress = replace(newAddress,"'","\'","all")>
<cfset newAddress = replace(newAddress,"#chr(34)#","\#chr(34)#","all")>
<cfset newAddress = replace(newAddress,"#chr(9)#","","all")>

<cfset newCode = replace(newCode,"'","\'","all")>
<cfset newCode = replace(newCode,"#chr(34)#","\#chr(34)#","all")>
<cfset newCode = replace(newCode,"#chr(9)#","","all")>


<cfset newCondition = replace(newCondition,"'","\'","all")>
<cfset newCondition = replace(newCondition,"#chr(34)#","\#chr(34)#","all")>
<cfset newCondition = replace(newCondition,"#chr(9)#","","all")>
--->

<cfoutput>
{
<cfswitch expression="#limitResults.legend#">
	<cfcase value="B">
		"btn": "police.png",
	</cfcase>
	<cfcase value="F">
		"btn": "fire.png",
	</cfcase>
	<cfcase value="M">
		"btn": "medical.png",
	</cfcase>
	<cfcase value="L">
		"btn": "lowLevel.png",
	</cfcase>
	<cfcase value="S">
		"btn": "supervisory.png",
	</cfcase>
	<cfcase value="RA">
		"btn": "runaway.png",
	</cfcase>
</cfswitch>
"id": "#trim(limitResults.alarmid)#",
"name": "#newName#",
"dealer": "#trim(limitResults.dealernumber)#",
"acct": "#trim(limitResults.accountnumber)#",
"partition": "#trim(limitResults.partition)#",
"received": "#trim(limitResults.dateReceived)# #trim(limitResults.timeReceived)#",
"timezone": "#trim(limitResults.timezone)#",
"code": "#newCode#",
"zone": "#trim(limitResults.alarmzone)#",
"condition": "#newCondition#",
"address": "#newAddress#",
"dispatcherhandled": "#trim(limitResults.dispatcherHandled)#",
"status": "#trim(limitResults.status)#",
"legend": "#trim(limitResults.legend)#",
"incident": "#trim(limitResults.incidentNumber)#"
}
<cfif limitResults.currentRow is not limitResults.recordcount>,</cfif>
</cfoutput></cfloop>]

<!--- <cfoutput>#session.startDateTime#</cfoutput> --->