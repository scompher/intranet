$(document).ready(function(){
	//toggle dashboard
	$( "#dashboard" ).hide();
	$( "#showdashboard" ).click(function() {
		$( "#dashboard" ).toggle("blind",{direction:"down"},500);
	});
	
	//logic for incident pop-up
	$(function() {
		$( "#dialog" ).dialog({
		  height: 750,
		  width: 650,
		  autoOpen: false,
		  show: {
			effect: "blind",
			duration: 500
		  },
		  hide: {
			effect: "blind",
			duration: 500
		  }
		});
	});
	
	$("#jqxgrid").bind('rowdoubleclick', function (event) {
    	var row = event.args.rowindex;
    	var data = $("#jqxgrid").jqxGrid('getrowdata', row);
		$("#dialog").load("viewdetail.cfm", 
			{
				incidentno: data.incident,
				pc: data.passcode,
				dn: data.dealer,
				name: data.name
			}
		); 
		$( "#dialog" ).dialog( "open" );
	});
	
	//image renderer for column 1 images
	var imagerenderer = function (row, datafield, value) {
		if (value != "") {
		 return '<img style="margin-left: 5px;" height="25" width="25" src="images/' + value + '"/>';
	   }
	}
	
	//logic for row colors
	var cellclass = function (row, columnfield, value) {
		var checkdisp = $('#jqxgrid').jqxGrid('getcellvalue', row, 'dispatcherhandled');
		var checkstatus = $('#jqxgrid').jqxGrid('getcellvalue', row, 'status');
		var checkcode = $('#jqxgrid').jqxGrid('getcellvalue', row, 'code');
		var checklegend = $('#jqxgrid').jqxGrid('getcellvalue', row, 'legend');
		if (checkstatus == "6") {
			return 'greenClass';
		} else if (checkcode == "DNT" || checkcode == "DNR") {
			return 'purpleClass';
		} else if (checklegend == "RA") {
			return 'yellowClass';
		} else if (checkdisp == "Y") {
			if (checklegend == "S") {
				return 'orangeClass';
			} else {
				return 'redClass';
			}
		} else {
			return 'blueClass';
		}			
	}
	
	//grid auto refresh
	var refreshInterval = setInterval(function () {
		$("#jqxgrid").jqxGrid("updatebounddata","data");
	}, 7000);
	
	//export button logic
	$("#excelExport").jqxButton();
	$("#csvExport").jqxButton();         
	$("#pdfExport").jqxButton();
	$("#excelExport").click(function () {
	$("#jqxgrid").jqxGrid('exportdata', 'xls', 'jqxGrid');           
	});  
	$("#csvExport").click(function () {
		$("#jqxgrid").jqxGrid('exportdata', 'csv', 'jqxGrid');
	});  
	$("#pdfExport").click(function () {
		$("#jqxgrid").jqxGrid('exportdata', 'pdf', 'jqxGrid');
	});
	
	//column hide/show checklist
	var listSource = [{ label: 'Dealer', value: 'dealer', checked: true }, { label: 'Acct #', value: 'acct', checked: true }, { label: 'Partition', value: 'partition', checked: true }, { label: 'Name', value: 'name', checked: true }, { label: 'Received Date Time', value: 'received', checked: true}, { label: 'Time Zone', value: 'timezone', checked: true}, { label: 'Code', value: 'code', checked: true}, { label: 'Zone', value: 'zone', checked: true}, { label: 'Condition', value: 'condition', checked: true}, { label: 'Address', value: 'address', checked: true}];
		$("#jqxlistbox").jqxListBox({ source: listSource, width: 200, height: 200,  checkboxes: true });
		$("#jqxlistbox").on('checkChange', function (event) {
			$("#jqxgrid").jqxGrid('beginupdate');
			if (event.args.checked) {
				$("#jqxgrid").jqxGrid('showcolumn', event.args.value);
			}
			else {
				$("#jqxgrid").jqxGrid('hidecolumn', event.args.value);
			}
		$("#jqxgrid").jqxGrid('endupdate');
	});
		
	//Autosize column button
	$("#button").jqxButton();
    $("#button").click(function () {
    	$("#jqxgrid").jqxGrid('autoresizecolumns');
    });
	
	//column resize logic
	$("#jqxgrid").on('columnresized', function (event) {
		var column = event.args.columntext;
		var newwidth = event.args.newwidth
		var oldwidth = event.args.oldwidth;
		$("#eventlog").text("Column: " + column + ", " + "New Width: " + newwidth + ", Old Width: " + oldwidth);
	});
	
	//grid source mapping
	url='getdataSC.cfm'
	var source =
	{
		datatype: "json",
		datafields: [
			{ name: 'btn' },
			{ name: 'name' },
			{ name: 'dealer' },
			{ name: 'address' },
			{ name: 'acct' },
			{ name: 'partition', type:'int' },
			{ name: 'received' },
			{ name: 'timezone' },
			{ name: 'code'},
			{ name: 'zone'},
			{ name: 'condition' },
			{ name: 'legend' },
			{ name: 'status' },
			{ name: 'dispatcherhandled' },
			{ name: 'incident' },
			{ name: 'passcode' }
		],
		url: url,
		id: 'id'
	};
	
	//data adaptor for source
	var dataAdapter = new $.jqx.dataAdapter(source, {
		downloadComplete: function (data, status, xhr) { },
		loadComplete: function (data) { },
		loadError: function (xhr, status, error) { }
	});
	
			
 	//actual grid logic
	$("#jqxgrid").jqxGrid(
	{
		width: '100%',
		height: '75%',
		source: dataAdapter,
		filterable: true,
		showfilterrow: true,
		columnsresize: true,
		columns: [
			{ text: '', datafield: 'btn', cellsrenderer: imagerenderer, minwidth:35, cellclassname: cellclass },
			{ text: 'Dealer', datafield: 'dealer', cellclassname: cellclass, width:60, minwidth:60},
			{ text: 'Acct #', datafield: 'acct', cellclassname: cellclass, width:100, minwidth:100 },
			{ text: 'Partition', datafield: 'partition', cellclassname: cellclass, width:75, minwidth:75 },
			{ text: 'Name', datafield: 'name', cellclassname: cellclass, width:200, minwidth:200 },
			{ text: 'Received', datafield: 'received', cellclassname: cellclass, width:150, minwidth:150 },
			{ text: 'Time Zone', datafield: 'timezone', cellclassname: cellclass, width:100, minwidth:100 },
			{ text: 'D.H.', datafield: 'dispatcherhandled', cellclassname: cellclass, width:50, minwidth:50 },
			{ text: 'Code', datafield: 'code', cellclassname: cellclass, width:50, minwidth:50 },
			{ text: 'Zone', datafield: 'zone', cellclassname: cellclass, width:50, minwidth:50 },
			{ text: 'Condition', datafield: 'condition', cellclassname: cellclass },
			{ text: 'Address', datafield: 'address', cellclassname: cellclass }
		]
	});
	
	
});