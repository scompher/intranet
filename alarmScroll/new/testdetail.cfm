<link rel="stylesheet" type="text/css" href="/styles.css">
<body bgcolor="#000066">

Detail not available in this version

</body>
<cfabort>

<cfset incidentNo = '5T*1000384'>
<cfif ParameterExists(INCIDENTNO)>
	<cfset dn='9999'>
	<cfset pc='44455'>
	<cfset xmlRequest = "<request><dealerNumber>#xmlformat(trim(dn))#</dealerNumber><dealerPasscode>#xmlformat(trim(pc))#</dealerPasscode><command>getIncidentDetail</command><data><incidentNumber>#xmlformat(trim(incidentNo))#</incidentNumber></data></request>">
	
	<cf_copalink command="#trim(xmlRequest)#">
	
	<cfset xResult = xmlparse(trim(result))>

	<cfif xResult.response.result.xmltext is "RJ">
		<cfset returnPage = "index.cfm">
		<cfset heading = "C.O.P.S. Monitoring - Individual Account Alarm History">
		<cfset errorMessages = xResult.response.data.messages>
		<cfinclude template="/copalink/templates/showXmlError.cfm">
		<cfabort>
	</cfif>

	<cfif isDefined("xResult.response.data.results.item")>
	
		<cfset item = xResult.response.data.results.item>
	
		<cfset IncidentNumber = incidentNo>
		<cfset IncidentDate = item.dateReceived.xmlText>
		<cfset IncidentTime = item.timeReceived.xmlText>
		<cfset IncidentCondition = item.incidentCondition.xmlText>
		<cfset IncidentPasscode = item.ctvGivenPasscode.xmlText>
		<cfset IncidentName = item.ctvGivenName.xmlText>
		<cfset IncidentAcctNumber = item.accountNumber.xmlText>
		<cfset IncidentCode = item.incidentCode.xmlText>
		<cfset IncidentZone = item.incidentZone.xmlText>
		<cfset Incidentevent = item.event.xmlText>
		<cfset IncidentRestore = item.restore.xmlText>
		<cfset authorities = item.authorities>
		<cfset respondingParties = item.respondingParties>
		<cfset alarmCompanies = item.alarmCompanies>
		<cfset verificationLog = item.verificationLog>	
		<cfset attemptsLog = item.attemptsLog>
		<cfset IncidentAltInstruct = item.specialInstructions.xmlText>
		<cfset IncidentZoneHandling = item.zoneInstructions.xmlText>
		<cfset IncidentDefault = item.defaultUsed.xmlText>
		<cfset IncidentNCFcode = item.NCFUsed.xmlText>
		<cfset AdditionalInfo = item.comments.xmlText>
		<cfset address = item.originalDispatchAddress.xmlText>
		<cfset city = item.city.xmlText>
		<cfset state = item.state.xmlText>
		<cfset zip = item.zipCode.xmlText>
		<cfset IncidentTimeProc = item.timeProcessed.xmlText>
		<cfset IncidentDateProc = item.dateProcessed.xmlText>
		<cfset ani = item.ani.xmlText>
		<cfset partition = item.partition.xmlText>
		<cfset comments = item.comments>
		<cfset originalCodeZone = item.originalCodeZone.xmlText>
		<cfset handlingFoundOn = item.handlingFoundOn.xmlText>
		<cfset ncfUsed = item.ncfUsed.xmlText>
		<cfset accountname = item.accountname.xmlText>
		
		<cfoutput>
		<table align="center" width="635" cellpadding="0" cellspacing="4" border="0"><tr><td align="center">
		<table width="500" cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td>
					<table width="100%" cellpadding="2" cellspacing="0" border="1">
						<tr>
							<td width="33%" valign="top">
								<span class="formtextbold">Date:</span>
								<br><span class="formtext">#IncidentDate#</span>
							</td>
							<td width="33%" valign="top">
								<span class="formtextbold">Time Received:</span>
								<br><span class="formtext"><cfif len(incidentTimeProc) GTE 3>#left(IncidentTime,len(IncidentTime) - 3)#</cfif></span>
							</td>
							<td width="33%" valign="top">
								<span class="formtextbold">Time Processed:</span>
								<br><span class="formtext"><cfif len(IncidentTime) GTE 3>#left(incidentTimeProc,len(incidentTimeProc) - 3)#</cfif></span>
							</td>
						</tr>
						<tr>
							<td valign="top">
								<span class="formtextbold">Caller ID/ANI:</span><br>
								<span class="formtext"><cfif isnumeric(trim(ani)) is true and len(trim(ani)) is 10>#left(ani,3)#-#mid(ani,4,3)#-#right(ani,4)#<cfelse>#ani#</cfif></span>
							</td>
							<td colspan="2" valign="top">
								<span class="formtextbold">Partition:</span><br>
								<span class="formtext">#partition#</span>
							</td>
						</tr>
						<tr>
							<td colspan="3" valign="top">
								<span class="formtextbold">Condition:</span>
								<br><span class="formtext">#IncidentCondition#</span>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<table width="100%" cellpadding="2" cellspacing="0" border="1">
						<tr>
							<td width="50%" valign="top">
								<span class="formtextbold">Account Number:</span>
								<br><span class="formtext">#IncidentAcctNumber#</span>
							</td>
							<td width="50%" valign="top">
								<span class="formtextbold">Account Name:</span>
								<br><span class="formtext">#AccountName#</span>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			
			<tr>
				<td>
					<table width="100%" cellpadding="2" cellspacing="0" border="1">
						<tr>
							<td width="50%" valign="top">
								<span class="formtextbold">Address:</span>
								<br><span class="formtext">#address#</span>
							</td>
							<td width="50%" valign="top">
								<span class="formtextbold">City:</span>
								<br><span class="formtext">#city#</span>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<table width="100%" cellpadding="2" cellspacing="0" border="1">
						<tr>
							<td width="50%" valign="top">
								<span class="formtextbold">State:</span>
								<br><span class="formtext">#state#</span>
							</td>
							<td width="50%" valign="top">
								<span class="formtextbold">Zip:</span>
								<br><span class="formtext">#zip#</span>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<table width="100%" cellpadding="2" cellspacing="0" border="1">
						<tr>
							<td width="50%" valign="top">
								<span class="formtextbold">Passcode Given:</span>
								<br><span class="formtext">#incidentPasscode#</span>
							</td>
							<td width="50%" valign="top">
								<span class="formtextbold">Name:</span>
								<br><span class="formtext">#IncidentName#</span>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<table width="100%" cellpadding="2" cellspacing="0" border="1">
						<tr>
							<td width="25%" valign="top">
								<span class="formtextbold">Code:</span>					
								<br><span class="formtext">#incidentcode#</span>
							</td>
							<td width="25%" valign="top">
								<span class="formtextbold">Zone/User:</span>
								<br><span class="formtext">#incidentZone#</span>
							</td>
							<td width="25%" valign="top">
								<span class="formtextbold">Event:</span>
								<br><span class="formtext">#IncidentEvent#</span>
							</td>
							<td width="25%" valign="top">
								<span class="formtextbold">Restore:</span>
								<br><span class="formtext">#IncidentRestore#</span>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<cfif isDefined("authorities.authority")>
			<cfloop from="1" to="#arraylen(authorities.authority)#" index="i">
			<cfset auth = authorities.authority[i]>
			<tr>
				<td>
					<table width="100%" cellpadding="2" cellspacing="0" border="1">
						<tr>
							<td width="66%" valign="top">
								<span class="formtextbold">Authority:</span>						
								<br><span class="formtext">#auth.name.xmlText#</span>
							</td>
							<td width="33%" valign="top">
								<span class="formtextbold">Authority Time:</span>
								<br>
								<span class="formtext">#auth.time.xmlText#</span>
							</td>
							</tr>
					</table>
				</td>
			</tr>
			</cfloop>
			</cfif>
			<cfif isDefined("respondingParties.respondingParty")>
			<cfloop from="1" to="#arraylen(respondingParties.respondingParty)#" index="i">
			<cfset rp = respondingParties.respondingParty[i]>
			<tr>
				<td>
					<table width="100%" cellpadding="2" cellspacing="0" border="1">
						<tr>
							<td width="66%" valign="top">
								<span class="formtextbold">Responding Party</span>
								<br><span class="formtext">#rp.name.xmlText#</span>
							</td>
							<td width="33%" valign="top">
								<span class="formtextbold">Responding Party Time:</span>
								<br><span class="formtext">#rp.time.xmlText#</span>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			</cfloop>
			</cfif>
			<cfif isDefined("alarmCompanies.alarmCompany")>
			<cfloop from="1" to="#arraylen(alarmCompanies.alarmCompany)#" index="i">
			<cfset alarmCo = alarmCompanies.alarmCompany[i]>
			<tr>
				<td>
					<table width="100%" cellpadding="2" cellspacing="0" border="1">
						<tr>
							<td width="66%" valign="top">
								<span class="formtextbold">Alarm Co.:</span>	
								<br><span class="formtext">#alarmCo.name.xmlText#</span>
							</td>	
							<td width="33%" valign="top">
								<span class="formtextbold">Alarm Co. Time:</span>
								<br><span class="formtext">#alarmCo.time.xmlText#</span>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			</cfloop>
			</cfif>
			<tr>
				<td>
					<table width="100%" cellpadding="2" cellspacing="0" border="1">
						<tr>
							<td valign="top">
								<span class="formtextbold">Special Instructions:</span>
								<br><span class="formtext">#IncidentAltInstruct#</span>
							</td>
							<td valign="top">
								<span class="formtextbold">Zone Handling?</span>
								<br><span class="formtext">#IncidentZoneHandling#</span>
							</td>
							<td valign="top">
								<span class="formtextbold">Default</span>
								<br><span class="formtext">#incidentdefault#</span>
							</td>
						</tr>
						<tr>
							<td colspan="3" valign="top">
								<span class="formtextbold">NCF:</span>
								<br>
								#ncfUsed#
								<br />
								<span class="formtextbold">Original Code Zone</span>
								<br />
								<span class="formtext">#originalCodeZone#</span>
								<br />
								<span class="formtextbold">Code Zone Received</span>
								<br />
								<span class="formtext">#IncidentNCFcode#</span>
								<br />
								<span class="formtextbold">Handling Found On:</span>
								<br />
								<span class="formtext">#handlingFoundOn#</span>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td align="center">
					<br><span class="subhdrtext">Verification Log</span><br /><br />
				</td>
			</tr>
			<tr>
				<td>
					<table width="100%" cellpadding="0" cellspacing="0" border="0">
						<cfif isdefined("verificationLog.verification")>
						<cfloop from="1" to="#arraylen(verificationLog.verification)#" index="x">
							<cfset VerLogEntry = verificationLog.verification[x]>
							<tr>
								<td>
								<table width="100%" cellpadding="2" cellspacing="0" border="1" class="formtext">
									<tr>
									<td>
										<span class="formtextbold">Person Called:</span><br />
										#verLogEntry.ctvName.xmlText#
									</td>
									<td>
										<span class="formtextbold">Date/Time Called:</span><br />
										#verLogEntry.ctvDate.xmlText# #verLogEntry.ctvTime.xmlText#
									</td>
									<td>
										<span class="formtextbold">Number Dialed:</span><br />
										#verLogEntry.ctvPhone.xmlText#
									</td>
									<td>
										<span class="formtextbold">Result:</span><br />
										#verLogEntry.ctvResult.xmlText#
									</td>
									<td>
										<span class="formtextbold">Misc:</span><br />
										#verLogEntry.ctvMisc.xmlText#
									</td>
									</tr>
								</table>
								</td>
							</tr>
						</cfloop>
						</cfif>
					</table>
				</td>
			</tr>
			<tr>
				<td align="center">
					<br><span class="subhdrtext">Attempts Log</span>
				</td>
			</tr>
			<tr>
				<td>
					<cfif isDefined("attemptsLog.attempt")>
					<table width="100%" cellpadding="2" cellspacing="0" border="1" class="formtext">
						<cfloop from="1" to="#arraylen(attemptsLog.attempt)#" index="x">
							<cfset tempattempt = attemptsLog.attempt[x]>
							<tr>
								<td>
								<b>Person Called:</b><br />
								#tempAttempt.attemptName.xmlText#
								</td>
								<td>
								<b>Code:</b><br />
								#tempAttempt.attemptResult.xmlText#
								</td>
								<td>
								<b>Time:</b><br />
								#tempAttempt.attemptTime.xmlText#
								</td>
								<td>
								<b>Misc:</b><br />
								#tempAttempt.attemptMisc.xmlText#
								</td>
							</tr>
						</cfloop>
					</table>
					</cfif>
				</td>
			</tr>
			<tr><td>&nbsp;</td></tr>
			<cfif isDefined("comments.comment")>
			<tr>
				<td>
					<table width="100%" cellpadding="2" cellspacing="0" border="1">
						<tr>
							<td class="nopadding">
							<span class="subhdrtext">Additional Information:</span><br>
								<table width="100%" border="0" cellspacing="0" cellpadding="3">
									<cfloop from="1" to="#arraylen(comments.comment)#" index="i">
									<cfset currentComment = comments.comment[i].xmlText>
									<tr>
										<td><cfoutput>#currentComment#</cfoutput></td>
									</tr>
									</cfloop>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			</cfif>
			</table>
			</td></tr></table>
		</cfoutput>
		
	</cfif>
<cfelse>
	incident detail not existing
</cfif>
</body>