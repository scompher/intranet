

<cfsetting showdebugoutput="no">
<cfheader name="Content-Type" value="application/json">
<cfapplication name="scrollingList" sessionmanagement="Yes">

<cfset dhflag = false>
<cfset alarmtypes = "">

<!--- <cfoutput> --->
<cfquery name="limitResults" datasource="copalink_production">
	select top 500 * 
	from recent_alarms with (nolock)
	where 1=1 
	and dealernumber like 'H%'
	order by alarmid desc   
</cfquery>
<!--- </cfoutput> --->


[<cfoutput query="limitResults">
{
<cfswitch expression="#limitResults.legend#">
	<cfcase value="B">
		"btn": "police.png",
	</cfcase>
	<cfcase value="F">
		"btn": "fire.png",
	</cfcase>
	<cfcase value="M">
		"btn": "medical.png",
	</cfcase>
	<cfcase value="L">
		"btn": "lowLevel.png",
	</cfcase>
	<cfcase value="S">
		"btn": "supervisory.png",
	</cfcase>
	<cfcase value="RA">
		"btn": "runaway.png",
	</cfcase>
</cfswitch>
"id": "#trim(limitResults.alarmid)#",
"name": "#trim(replace(limitResults.name,"'","\'","all"))#",
"dealer": "#trim(limitResults.dealernumber)#",
"acct": "#trim(limitResults.accountnumber)#",
"partition": "#trim(limitResults.partition)#",
"received": "#trim(limitResults.dateReceived)# #trim(limitResults.timeReceived)#",
"timezone": "#trim(limitResults.timezone)#",
"code": "#trim(replace(limitResults.alarmcode,"\","\\","all"))#",
"zone": "#trim(limitResults.alarmzone)#",
"condition": "#trim(replace(limitResults.condition,"'","\'","all"))#",
"address": "#trim(replace(limitResults.address,"'","\'","all"))#",
"dispatcherhandled": "#trim(limitResults.dispatcherHandled)#",
"status": "#trim(limitResults.status)#",
"legend": "#trim(limitResults.legend)#",
"incident": "#trim(limitResults.incidentNumber)#"
}
<cfif limitResults.currentRow is not limitResults.recordcount>,</cfif>
</cfoutput>]
--->

<!--- <cfoutput>#session.startDateTime#</cfoutput> --->