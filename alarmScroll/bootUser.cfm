

<cfparam name="form.dealernumber" default="">
<cfparam name="form.passcode" default="">
<cfparam name="msg" default="">

<link rel="stylesheet" type="text/css" href="../styles.css">

<cfif isDefined("form.btnBootUser")>

	<cfquery name="checkForLogin" datasource="copalink">
		select * from scrolling_alarms_dealer_lookup
		where  dealerNumber = '#form.dealerNumber#' and passcode = '#form.passcode#'
	</cfquery>	

	<cfif checkForLogin.recordcount gt 0>

		<cfset todayDate = createodbcdate(now())>
		<cfset tomorrowDate = createodbcdate(dateadd("d",1,todayDate))>
		<cfset todayDateTime = createodbcdatetime(now())>

		<cfquery name="delAlarms" datasource="copalink">
			set deadlock_priority -1
			delete from scrolling_alarms where dealerNumber = '#form.dealerNumber#' and passcode = '#form.passcode#'
		</cfquery>
		<cfquery name="delLookup" datasource="copalink">
			set deadlock_priority -1
			delete from scrolling_alarms_dealer_lookup where dealerNumber = '#form.dealerNumber#' and passcode = '#form.passcode#'
		</cfquery>
		<cfquery name="delMobile" datasource="copalink">
			set deadlock_priority -1
			delete from scrolling_alarms_mobile where dealerNumber = '#form.dealerNumber#' and passcode = '#form.passcode#'
		</cfquery>
		<cfquery name="updateTracking" datasource="copalink">
			update scrolling_alarms_tracking 
			set dateTimeEnded = #todayDateTime#
			where (dateTimeStarted >= #todayDate# and dateTimeStarted < #tomorrowDate#) and dealerNumber = '#form.dealerNumber#' and passcode = '#form.passcode#' and dateTimeEnded is null
		</cfquery>
	
		<cfset msg = "Dealer has been logged out of scrolling list successfully">
	
	<cfelse>
	
		<cfset msg = "Dealer was not logged into scrolling list">
	
	</cfif>

</cfif>

<body onLoad="if (document.mainform.dealerNumber.value == '') {document.mainform.dealerNumber.focus();} else {document.mainform.passcode.focus();}">
<div align="center" class="normal">
<form method="post" action="bootUser.cfm" name="mainform">
	<table border="0" cellspacing="0" cellpadding="5">
		<cfif trim(msg) is not "">
		<cfoutput>
		<tr align="center">
			<td colspan="2" style="font-weight:bold; color:##FF0000;">#msg#<br><br></td>
		</tr>
		</cfoutput>
		</cfif>
		<tr>
			<td colspan="2" align="center" valign="top" nowrap bgcolor="#FFFFFF" class="header">Scrolling List of Alarms - Log User Out </td>
			</tr>
		<tr>
			<td colspan="2" valign="top" nowrap class="greyrow">Enter Dealer # &amp; Passcode to be logged out of scrolling list </td>
			</tr>
		<tr>
			<td align="right" nowrap class="greyrowleft">Dealer Number: </td>
			<td nowrap class="greyrowright">
				<input tabindex="1" onKeyUp="if (this.value.length == 4) {this.form.passcode.focus();}" type="text" name="dealerNumber" value="<cfoutput>#form.dealernumber#</cfoutput>">			</td>
		</tr>
		<tr>
			<td align="right" nowrap class="greyrowleft">Passcode:</td>
			<td nowrap class="greyrowright">
				<input tabindex="2" type="text" name="passcode" value="<cfoutput>#form.passcode#</cfoutput>">
			</td>
		</tr>
		
		<tr>
			<td colspan="2" align="center" class="greyrowbottom">
				<input tabindex="3" name="btnBootUser" type="submit" class="sidebar" value="Log User Out" style="padding:3px" />
			</td>
		</tr>
	</table>
</form>
<br>
<a href="/index.cfm" style="text-decoration:underline;">Return to Intranet Menu</a>
</div>
</body>
