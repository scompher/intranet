<link rel="stylesheet" type="text/css" href="../../styles.css">

<cfparam name="form.linkName" default="">
<cfparam name="form.linKURL" default="">
<cfparam name="form.linkFile" default="">

<cfset errormsg = "">
	
<cfif isdefined("Form.btnFinished")>
	<cflocation url="index.cfm">
</cfif>

<cfif isdefined("form.btnAddLink")>

	<cfif trim(form.linkName) is ""><cfset errormsg = listappend(errormsg, "The link name is required.")></cfif>
	<cfif trim(form.linkURL) is "" and trim(form.linkFile) IS ""><cfset errormsg = listappend(errormsg, "The link URL or File is required.")></cfif>
	
	<cfif trim(errormsg) is "">
		<cfif findnocase("http://", form.linkURL) is 0><cfset form.linkURL = "http://" & form.linkURL></cfif>
		<cfquery name="saveLink" datasource="#ds#">
			begin
				insert into migrationportal_links (dealerid, linkName, linkURL)
				values (#form.dealerid#, '#form.linkName#', '#form.linkURL#')
			end
			begin
				select max(linkid) as maxid from migrationportal_links
				where dealerid = #form.dealerid# and linkName = '#form.linkName#' and linkURL = '#form.linkURL#'
			end
		</cfquery>
		<cfset linkid = saveLink.maxid>
		<cfif form.linkFile is not "">
			<cf_savefile dealerid="#dealerid#" formfield="linkFile" action="new">
			<cfset savedAttachment = savedFileName>
			<cfquery name="updateFileName" datasource="#ds#">
				update migrationportal_links
				set attachment = '#savedAttachment#', linkURL = ''
				where linkid = #linkid# 
			</cfquery>
		</cfif>
		
		<cfset form.linkName = "">
		<cfset form.linkURL = "">
		<cfset form.linkFile = "">
	</cfif>
</cfif>

<cfif isdefined("form.btnEditLink")>
	<cfif trim(form.linkName) is ""><cfset errormsg = listappend(errormsg, "The link name is required.")></cfif>
	<cfif trim(form.linkURL) is "" and trim(form.linkFile) IS ""><cfset errormsg = listappend(errormsg, "The link URL or File is required.")></cfif>
	
	<cfif trim(errormsg) is "">
		<cfif findnocase("http://", form.linkURL) is 0><cfset form.linkURL = "http://" & form.linkURL></cfif>
		<cfquery name="saveLink" datasource="#ds#">
			update migrationportal_links 
			set dealerid = #form.dealerid#, linkName = '#form.linkName#', linkURL = '#form.linkURL#'
			where linkid = #linkid#
		</cfquery>
		<cfif form.linkFile is not "">
			<cf_savefile dealerid="#dealerid#" formfield="linkFile" action="new">
			<cfset savedAttachment = savedFileName>
			<cfquery name="updateFileName" datasource="#ds#">
				update migrationportal_links
				set attachment = '#savedAttachment#'
				where dealerid = #dealerid#
			</cfquery>
		</cfif>
		<cfset form.linkName = "">
		<cfset form.linkURL = "">
		<cfset form.linkFile = "">
	</cfif>
</cfif>

<cfif isdefined("url.action")>
	<cfswitch expression="#url.action#">
		<cfcase value="edit">
			<cfquery name="getLink" datasource="#ds#">
				select * from migrationportal_links
				where linkid = #linkid#
			</cfquery>
			<cfif getLink.recordcount gt 0>
				<cfset form.linkName = getLink.linkName>
				<cfset form.linkURL = getLink.linkURL>
				<cfset form.linkFile = getLink.attachment>
				<cfset editflag = true>
			</cfif>
		</cfcase>
		<cfcase value="delete">
			<cfquery name="deleteLink" datasource="#ds#">
				begin
					select attachment from migrationportal_links
					where linkid = #linkid#
				end
				begin
					delete from migrationportal_links
					where linkid = #linkid#
				end
			</cfquery>
			<cfif deleteLink.attachment is not "">
				<cfset filepath = "e:\websites\intranet\migration\files">
				<cfif fileExists("#filepath#\#deleteLink.attachment#")>
					<cffile action="delete" file="#filepath#\#deleteLink.attachment#">
				</cfif>
			</cfif>
		</cfcase>
	</cfswitch>
</cfif>

<!--- get links --->
<cfquery name="getLinks" datasource="#ds#">
	select * from migrationportal_links
	where dealerid = #dealerid#
</cfquery>

<div align="center">
<table width="400" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Manage Links</b></td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<cfif trim(errormsg) is not "">
				<tr>
					<td colspan="2" style="color:#FF0000">
					<cfloop list="#errormsg#" index="err">
					<cfoutput>
					<b>#err#</b><br />
					</cfoutput>
					</cfloop>
					</td>
				</tr>
				</cfif>
				<tr>
					<td colspan="2" nowrap>Link name is required. You can have it point to a URL <b>OR</b> attach a file </td>
				</tr>
				<form method="post" action="manageLinks.cfm" enctype="multipart/form-data">
				<cfoutput>
				<input type="hidden" name="dealerid" value="#dealerid#" />
				</cfoutput>
				<cfoutput>
				<tr>
					<td width="20%" nowrap>Link Name: </td>
					<td width="80%">
						<input name="linkName" type="text" id="linkName" style="width:300px" maxlength="255" value="#form.linkName#">
					</td>
				</tr>
				<tr>
					<td nowrap>Link URL: </td>
					<td>
						<input name="linkUrl" type="text" id="linkUrl" style="width:300px" maxlength="255" value="#form.linkURL#">
					</td>
				</tr>
				<tr>
					<td nowrap>Linked File: </td>
					<td>
						<input name="linkFile" type="file" id="linkFile">
					</td>
				</tr>
				</cfoutput>
				<tr>
					<td colspan="2" valign="top" nowrap="nowrap">
						<cfif isdefined("editflag")>
							<input type="hidden" name="linkid" value="<cfoutput>#linkid#</cfoutput>" />
							<input name="btnEditLink" type="submit" class="sidebar" value="Update Link" />
						<cfelse>
							<input name="btnAddLink" type="submit" class="sidebar" value="Add Link" />
						</cfif>
					</td>
					</tr>
				<tr>
					<td valign="top" nowrap="nowrap">Custom Links: </td>
					<td class="nopadding">
						<table border="0" cellpadding="5" cellspacing="0" class="grey">
							<cfoutput query="getLinks">
							<tr>
								<td>#linkName#</td>
								<td>
								<a href="manageLinks.cfm?linkid=#linkid#&dealerid=#dealerid#&action=edit"><img src="../../images/edit.gif" width="16" height="16" border="0" /></a>
								<a href="manageLinks.cfm?linkid=#linkid#&dealerid=#dealerid#&action=delete"><img src="../../images/delete.gif" width="16" height="16" border="0" /></a>
								</td>
							</tr>
							</cfoutput>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2" valign="top" nowrap="nowrap">
						<input name="btnFinished" type="submit" class="sidebar" value="Finished" />
					</td>
					</tr>
				</form>
			</table>
		</td>
	</tr>
</table>

</div>
