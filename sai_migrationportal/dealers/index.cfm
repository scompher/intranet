
<link rel="stylesheet" type="text/css" href="../../styles.css">

<cfparam name="s" default="1">
<cfparam name="currentSort" default="">
<cfparam name="sb" default="dnum asc"> 
<cfparam name="sort" default="">

<cfset sortby = sb>

<cfset start = s>
<cfset maxrows = 50>

<cfswitch expression="#sb#">
	<cfcase value="dnum asc">
		<cfset sort = "dealernumber asc">
	</cfcase>
	<cfcase value="dnum desc">
		<cfset sort = "dealernumber desc">
	</cfcase>
	<cfcase value="dname asc">
		<cfset sort = "dealername asc">
	</cfcase>
	<cfcase value="dname desc">
		<cfset sort = "dealername desc">
	</cfcase>
	<cfcase value="saidnum asc">
		<cfset sort = "saidealernumber asc">
	</cfcase>
	<cfcase value="saidnum desc">
		<cfset sort = "saidealernumber desc">
	</cfcase>
	<cfcase value="numvisits asc">
			<cfset sort = "numberOfVisits asc">
	</cfcase>
	<cfcase value="numvisits desc">
		<cfset sort = "numberOfVisits desc">
	</cfcase>
	<cfcase value="expires asc">
		<cfset sort = "expirationDate asc">
	</cfcase>
	<cfcase value="expires desc">
		<cfset sort = "expirationDate desc">
	</cfcase>
	<cfcase value="acctmanager asc">
		<cfset sort = "lastname asc, lastname asc">
	</cfcase>
	<cfcase value="acctmanager desc">
		<cfset sort = "lastname desc, lastname desc">
	</cfcase>
</cfswitch>

<cfquery name="getdealers" datasource="#ds#">
	select saiportal_dealers.*, admin_users.firstname, admin_users.lastname, saiportal_links.linkName, saiportal_links.linkURL, saiportal_links.attachment, saiportal_links.linkid
	from saiportal_dealers 
	left join admin_users on saiportal_dealers.acctmanagerid = admin_users.adminuserid
	left join saiportal_links on saiportal_dealers.dealerid = saiportal_links.dealerid
	<cfif trim(sort) is not "">
	order by #sort# 
	</cfif>
</cfquery>

<div align="center" class="normal">
<table border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>SAI Migration portal dealer maintenance</b></td>
	</tr>
	<form method="post" action="add.cfm">
	<tr>
		<td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<cfoutput>
				<tr>
					<td align="center"><b>Action</b></td>
					<td>
					<cfif sb is "dnum asc">
						<cfset sortby = "dnum desc">
					<cfelse>
						<cfset sortby = "dnum asc">
					</cfif>
					<a style="text-decoration:underline;" href="index.cfm?sb=#sortby#"><b>Dealer ##</b></a>
					</td>
					<td>
					<cfif sb is "saidnum asc">
						<cfset sortby = "saidnum desc">
					<cfelse>
						<cfset sortby = "saidnum asc">
					</cfif>
					<a style="text-decoration:underline;" href="index.cfm?sb=#sortby#"><b>SAI Dealer ##</b></a>
					</td>
					<td>
					<cfif sb is "dname asc">
						<cfset sortby = "dname desc">
					<cfelse>
						<cfset sortby = "dname asc">
					</cfif>
					<a style="text-decoration:underline;" href="index.cfm?sb=#sortby#"><b>Dealer Name </b></a>
					</td>
					<td align="center">
					<cfif sb is "numvisits asc">
						<cfset sortby = "numvisits desc">
					<cfelse>
						<cfset sortby = "numvisits asc">
					</cfif>
					<a style="text-decoration:underline;" href="index.cfm?sb=#sortby#"><b>Number of Visits</b></a>
					</td>
					<td align="center">
					<cfif sb is "expires asc">
						<cfset sortby = "expires desc">
					<cfelse>
						<cfset sortby = "expires asc">
					</cfif>
					<a style="text-decoration:underline;" href="index.cfm?sb=#sortby#"><b>Expires</b></a>
					</td>
					<td align="center">
					<cfif sb is "acctmanager asc">
						<cfset sortby = "acctmanager desc">
					<cfelse>
						<cfset sortby = "acctmanager asc">
					</cfif>
					<a style="text-decoration:underline;" href="index.cfm?sb=#sortby#"><b>Account Manager</b></a>
					</td>
					<td align="left"><b>Links</b></td>
				</tr>
				</cfoutput>
				<cfoutput query="getdealers" group="dealerid" startrow="#start#" maxrows="#maxrows#">
				<cfset daysToExpire = datediff("d",dateformat(now(),'mm/dd/yyyy'), expirationDate)>
				<cfif daysToExpire lte 2>
				<tr>
				</cfif>
					<td valign="top" align="center" class="linedrow" style="color:##000000; font-weight:normal">
					#getdealers.currentrow#. 
					<a href="edit.cfm?dealerid=#dealerid#">edit</a> | 
					<a href="javascript: if (confirm('Are you sure you wish to delete this dealer?')) document.location='delete.cfm?dealerid=#dealerid#';">delete</a>					</td>
					<td align="center" valign="top" class="linedrow">#dealernumber#</td>
					<td align="center" valign="top" class="linedrow">#saiDealerNumber#</td>
					<td valign="top" class="linedrow">#dealername#</td>
					<td align="center" valign="top" class="linedrow">#numberOfVisits#</td>
					<td align="center" valign="top" class="linedrow">#dateformat(expirationDate, 'mm/dd/yyyy')# <cfif daysToExpire is 3>!</cfif></td>
					<td align="center" valign="top" class="linedrow">#firstname# #lastname#</td>
					<td valign="top" class="linedrow" style="padding:0px">
					<table width="100%" border="0" cellspacing="0" cellpadding="3">
						<cfoutput>
						<cfif trim(linkURL) is not ""><cfset linkto = linkURL><cfelse><cfset linkto = "#copalinkURL#/migration/files/#attachment#"></cfif>
						<tr>
							<td><a style="text-decoration:underline;" target="_blank" href="#linkto#">#linkName#</a></td>
						</tr>
						</cfoutput>
					</table>
					</td>
				</tr>
				</cfoutput>
				<tr>
					<td colspan="8">
					<cfset next = start + maxrows>
					<cfset prev = start - maxrows>
					<cfoutput>
					<cfif prev gt 0>
					<a href="index.cfm?s=#prev#&sb=#sb#"><b>Previous Page</b></a>
					</cfif>
					<cfif getdealers.recordcount gt next>
					<cfif prev gt 0> | </cfif>
					<a href="index.cfm?s=#next#&sb=#sb#"><b>Next page</b></a>
					</cfif>
					</cfoutput>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	</form>
</table>
<br />
<a href="/">Return to Intranet</a>
</div>
