<link rel="stylesheet" type="text/css" href="../../styles.css">

<cfsetting requesttimeout="300">

<cfif isdefined("form.addDealer")>
	
	<cftry>

	<cfquery name="saveDealerInfo" datasource="#ds#">
		begin
			insert into migrationportal_dealers (acctmanagerid, dealerName, dealerNumber, passcode, expirationDate)
			values (#form.acctmanagerid#, '#form.dealerName#', '#form.dealerNumber#', '#form.passcode#', #createodbcdate(form.expirationDate)#)
		end
		begin
			select max(dealerid) as maxid from migrationportal_dealers where dealername = '#form.dealername#' and dealerNumber = '#form.dealerNumber#' and passcode = '#form.passcode#' 
		end
	</cfquery>
	<cfset dealerid = saveDealerInfo.maxid>

<!--- send welcome email 
<cfquery name="getAM" datasource="#ds#">
	select * from admin_Users
	where adminuserid = #form.acctmanagerid#
</cfquery>

<cfmail from="#getAM.email#" to="#form.dealerNumber#" subject="Your C.O.P.S. Monitoring custom web portal is now available!" username="copalink@copsmonitoring.com" password="copsmoncal">
Attn: #form.dealerName#

Your custom web portal is now available!  You will find useful information about the company - including a corporate profile, a breakdown of services offered, details on our industry-leading COP-A-Link dealer (and subscriber) access options, and any customized presentations / information prepared for your organization.

To access the portal, simply click on the link below (if you are unable to click on the link, copy and paste into your browser website address field) and enter the e-mail address and pass code contained in this e-mail.  Please note that you may access this before #form.expirationDate#.  If you have any questions, please do not hesitate to contact #getAM.firstname# #getAM.lastname#, your account manager.

Thank you for your interest in C.O.P.S. Monitoring, and we look forward to hearing from you!

Your link: https://copalink.copsmonitoring.com/sales

Your e-mail: #form.dealerNumber#

Your pass code: #form.passcode#

Expiration Date: #form.expirationDate#
</cfmail>
--->
	<cflocation url="manageLinks.cfm?dealerid=#dealerid#">
	
	<cfcatch type="any">
		<div align="center">
		<table border="0" cellspacing="0" cellpadding="5" width="500">
			<tr>
				<td class="highlightbar"><b>Add A Dealer </b></td>
			</tr>
			<tr>
				<td class="greyrowbottomnopadding">
				<table width="100%" border="0" cellspacing="0" cellpadding="5" class="grey">
					<tr>
						<td align="center">
						<br />
						An error has occurred while adding this dealer.
						<br />
						<br />
						<a style="text-decoration:underline;" href="javascript:history.go(-1);">Click here to go back and try again</a>
						<br />
						<br />
						<cfoutput>
						#cfcatch.Message#
						<br />
						<br />
						#cfcatch.Detail#
						<br />
						<br />
						</cfoutput>
						</td>
					</tr>
				</table>
				</td>
			</tr>
		</table>
		</div>
		<cfabort>
	</cfcatch>
	</cftry>

</cfif>

<script language="javascript" src="../cal2.js">
/*
Xin's Popup calendar script-  Xin Yang (http://www.yxscripts.com/)
Script featured on/available at http://www.dynamicdrive.com/
This notice must stay intact for use
*/
</script>
<script language="javascript" src="../cal_conf2.js"></script>

<cfparam name="form.dealerName" default="">
<cfparam name="form.dealerNumber" default="">
<cfparam name="form.passcode" default="">
<cfparam name="form.expirationDate" default="5/31/2008">
<cfparam name="form.dealerid" default="0">

<cfquery name="getAcctManagers" datasource="#ds#">
	select * 
	from admin_users
	inner join admin_users_departments_lookup on admin_users.adminuserid = admin_users_departments_lookup.adminuserid
	inner join admin_users_departments on admin_users_departments_lookup.departmentid = admin_users_departments.departmentid 
	where admin_users_departments.department = 'Sales' and admin_users.active <> 0
</cfquery>

<script type="text/javascript">
	function checkForm(frm) {
		if (frm.dealerName.value == '') {alert('The dealer name is required.'); frm.dealerName.focus(); return false;}
		if (frm.dealerNumber.value == '') {alert('The dealer number is required.'); frm.dealerNumber.focus(); return false;}
		if (frm.passcode.value == '') {alert('The passcode is required.'); frm.passcode.focus(); return false;}
		if (frm.expirationDate.value == '') {alert('The expiration date is required.'); frm.expirationDate.focus(); return false;}
		frm.submit();
	}
</script>

<div align="center">
<br>
<form method="post" action="add.cfm" name="mainform" enctype="multipart/form-data">
<input type="hidden" name="addDealer" value="1" />
<table border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Step 1: Add A Dealer </b></td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellpadding="5" cellspacing="0" class="grey">
				<tr>
					<td nowrap="nowrap">Account Manager:</td>
					<td>
					<select name="acctmanagerid">
						<option value="0"></option>
						<cfoutput query="getAcctManagers">
							<option value="#adminuserid#" <cfif getsec.seclevelid lte 2 and cookie.adminlogin is adminuserid>selected</cfif> >#firstname# #lastname#</option>
						</cfoutput>
					</select>
					</td>
				</tr>
				<tr>
					<td nowrap="nowrap">Dealer Name: </td>
					<td>
						<input name="dealerName" type="text" style="width:300px" value="<cfoutput>#form.dealerName#</cfoutput>" />
					</td>
				</tr>
				<tr>
					<td nowrap="nowrap">Dealer Number:</td>
					<td>
						<input name="dealerNumber" type="text" style="width:300px" value="<cfoutput>#form.dealerNumber#</cfoutput>" />
					</td>
				</tr>
				<tr>
					<td nowrap="nowrap">Portal Passcode: </td>
					<td>
						<input name="passcode" type="text" style="width:100px" value="<cfoutput>#form.passcode#</cfoutput>" />
					</td>
				</tr>
				<tr>
					<td nowrap="nowrap">Expiration Date: </td>
					<td class="nopadding">
						<table border="0" cellpadding="5" cellspacing="0" class="grey">
							<tr>
								<td>
									<input name="expirationDate" type="text" id="expirationDate" style="width:75px" value="<cfoutput>#form.expirationDate#</cfoutput>" />
								</td>
								<td align="center"><a style="text-decoration:none;" href="javascript:showCal('ExpirationDate');"> <img src="/images/calicon.gif" alt="Select a Date" align="absmiddle" width="20" height="20" border="0" /> </a></td>
							</tr>
						</table>
					</td>
				</tr>
				
				<tr>
					<td colspan="2">
						<input style="height:20px" type="button" class="sidebar" value="Continue" onclick="checkForm(this.form);" />
						<input style="height:20px" type="button" class="sidebar" value="Cancel" onclick="document.location='index.cfm';" />
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</form>
<br>
</div>
