
<cfabort>
<cfquery name="getDealer" datasource="#ds#">
	select * from salesportal_dealers
	where dealerid = #url.did#
</cfquery>

<cfif getDealer.recordcount gt 0>

<!--- send welcome email --->
<cfquery name="getAM" datasource="#ds#">
	select * from admin_Users
	where adminuserid = #getDealer.acctmanagerid#
</cfquery>

<cfmail from="#getAM.email#" to="#getDealer.dealeremail#" subject="Your C.O.P.S. Monitoring custom web portal is now available!" username="copalink@copsmonitoring.com" password="copsmoncal">
Attn: #getDealer.dealerName#

Your custom web portal is now available!  You will find useful information about the company - including a corporate profile, a breakdown of services offered, details on our industry-leading COP-A-Link dealer (and subscriber) access options, and any customized presentations / information prepared for your organization.

To access the portal, simply click on the link below (if you are unable to click on the link, copy and paste into your browser website address field) and enter the e-mail address and pass code contained in this e-mail.  Please note that you may access this before #getDealer.expirationDate#.  If you have any questions, please do not hesitate to contact #getAM.firstname# #getAM.lastname#, your account manager.

Thank you for your interest in C.O.P.S. Monitoring, and we look forward to hearing from you!

Your link: https://copalink.copsmonitoring.com/sales

Your e-mail: #getDealer.dealerEmail#

Your pass code: #getDealer.passcode#

Expiration Date: #getDealer.expirationDate#
</cfmail>

sent

<cfelse>

dealer not found

</cfif>

