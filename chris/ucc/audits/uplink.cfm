<cfset today = dateformat(Now(),'mm/dd/yyyy')>
<cfset serviceAddTime = createodbcdatetime(now())>
<cfset starttime = Now()>
<cfset outputFilePath = "#request.DirectPath#\chris\ucc\audits\uplink">
<cfsetting requesttimeout="30000">

<!--- delete old files --->
<cffile action="delete" file="#outputFilePath#/uplinkmissing.csv">
<cffile action="delete" file="#outputFilePath#/uplinkserviceadded.csv">
<cffile action="delete" file="#outputFilePath#/uplinknotonbillcancelled.csv">
<cffile action="delete" file="#outputFilePath#/uplinknotonbill.csv">
<cffile action="delete" file="#outputFilePath#/uplinkonbillcancelleducc.csv">

<!--- create headers --->
<cffile action="append" file="#outputFilePath#/uplinkmissing.csv"
output='Account Number On Bill-Missing from UCC,Account Name,OOS category,UCC Dealer Number, Dealer Name,Bill Code Desc,Bill Code,Charge'
addnewline="yes">
<cffile action="append" file="#outputFilePath#/uplinkserviceadded.csv"
output='Account Number on Bill-Service Added to UCC,Account Name,OOS category,UCC Dealer Number, Dealer Name,Bill Code Desc,Bill Code,Charge'
addnewline="yes">
<cffile action="append" file="#outputFilePath#/uplinknotonbillcancelled.csv"
output='Canceled UCC Account Number-Not on bill,Account Name,OOS category,UCC Dealer Number, Dealer Name,Bill Code Desc,Bill Code,Charge'
addnewline="yes">
<cffile action="append" file="#outputFilePath#/uplinknotonbill.csv"
output='UCC account number missing from bill,Account Name,OOS category,UCC Dealer Number,Dealer Name,Bill Code Desc,Bill Code,Charge'
addnewline="yes">
<cffile action="append" file="#outputFilePath#/uplinkonbillcancelleducc.csv"
output='UCC account number on bill- cancelled in UCC,Account Name,OOS category,UCC Dealer Number,Dealer Name,Bill Code Desc,Bill Code,Charge'
addnewline="yes">

<cfoutput>
	<!--- build service code list--->
	<cfquery name='getuplinkServices' datasource="lydiabilling">
		select serviceCode
		from serviceConversion
		where vendor = 'Uplink'
	</cfquery>
	<cfset serviceList = quotedValueList(getuplinkServices.serviceCode)>
	
	<!--- grab the bill --->
	<cfquery name="getBill" datasource="lydiabilling">
		select * from uplink
		join serviceconversion
		on uplink.serviceplan = serviceconversion.item
		and ignore is NULL
		and serviceconversion.vendor = 'Uplink'
	</cfquery>

	<!--- this part takes the bill and makes sure the accounts are at UCC --->
	<cfloop query="getBill">
		<cfquery name="getAccountInfo" datasource="uccbilling">
			select 
			stages.site.SiteNum 'siteNum',
			stages.site.SiteName 'siteName',
			stages.Device.OOSStartDate 'oosDate',
			stages.Transmitter.TransmitterCode 'accountNum',
			stages.SiteContractItem.ContractItem 'serviceCode',
			stages.SiteGroupLink.SiteGroupNum 'dealerNum',
			stages.SiteGroup.name 'dealerName',
			stages.Device.DevNum 'devNum',
			stages.Device.OOSCat
			from stages.SiteContractItem
			join stages.device
			on stages.device.devnum = stages.sitecontractitem.devnum
			join stages.transmitter
			on stages.device.devnum = stages.transmitter.devnum
			join stages.site
			on stages.sitecontractitem.sitenum = stages.site.sitenum
			join stages.SiteGroupLink
			on stages.site.SiteNum = stages.sitegrouplink.SiteNum
			join stages.SiteGroup
			on stages.SiteGroup.SiteGroupNum = stages.SiteGroupLink.SiteGroupNum
			where 
			stages.Transmitter.TransmitterCode = '#trim(getBill.automationID)#'
		</cfquery>
		<cfif getAccountInfo.recordCount is 0 and trim(getBill.automationID) is not ''>
			<cfquery name="checkEntireDB" datasource="uccbilling">
				select 
				stages.site.SiteNum 'siteNum',
				stages.site.SiteName 'siteName',
				stages.Transmitter.TransmitterCode 'accountNum'
				from stages.Site
				join Stages.device on stages.site.SiteNum = stages.device.SiteNum
				join stages.Transmitter on stages.device.DevNum = stages.Transmitter.DevNum
				where stages.Transmitter.TransmitterCode = '#getBill.automationID#'
			</cfquery>
			<cfif checkEntireDB.recordCount is 0>
				<cfset SubscriberName = '#getBill.name#'>
				<cffile action="append" file="#outputFilePath#/uplinkmissing.csv"
				output='#getBill.automationID#,#SubscriberName#,N/A,N/A,N/A,#getBill.servicePlan#,#getBill.servicePlan#,'
				addnewline="yes">
			<cfelse>
				<cfif getAccountInfo.oosDate is ''  and getAccountInfo.siteNum is not "" and getAccountInfo.siteNum is not "0" >
					<cfquery name="doesAccountHaveService" dbtype="query">
						select *
						from getAccountInfo
						where 
						serviceCode = '#getBill.servicePlan#'
					</cfquery>
					<!---
					<cfquery name="addService" datasource="uccbilling">
					insert into stages.sitecontractitem
					values ('#getAccountInfo.siteNum#','#getAccountInfo.devNum#','#getBill.serviceCode#',#serviceAddTime#,'')
					</cfquery>
					--->
					<cffile action="append" file="#outputFilePath#/uplinkserviceadded.csv"
					output='#checkEntireDB.accountNum#,#replace(checkEntireDB.siteName,","," ","all")#,Active,#getAccountInfo.dealerNum#,#getAccountInfo.dealerName#,#getBill.servicePlan#,#getBill.serviceCode#,'
					addnewline="yes">
				</cfif>
			</cfif>
		</cfif>
		<cfif getAccountInfo.recordCount gt 0>
			<cfquery name="doesAccountHaveService" dbtype="query">
				select *
				from getAccountInfo
				where 
				serviceCode = '#getBill.serviceCode#'
			</cfquery>
			<cfif doesAccountHaveService.recordCount is 0   and getAccountInfo.siteNum is not "" and getAccountInfo.siteNum is not "0">
				<cfif getAccountInfo.oosDate is ''>
					<!---
					<cfquery name="addService" datasource="uccbilling">
					insert into stages.sitecontractitem
					values ('#getAccountInfo.siteNum#','#getAccountInfo.devNum#','#getBill.serviceCode#',#serviceAddTime#,'')
					</cfquery>
					--->
					<cffile action="append" file="#outputFilePath#/uplinkserviceadded.csv"
					output='#getAccountInfo.accountNum#,#replace(getAccountInfo.siteName,","," ","all")#,Active,#getAccountInfo.dealerNum#,#getAccountInfo.dealerName#,#getBill.servicePlan#,#getBill.serviceCode#,'
					addnewline="yes">
				<cfelse>
					<cffile action="append" file="#outputFilePath#/uplinkonbillcancelleducc.csv"
					output='#getAccountInfo.accountNum#,#replace(getAccountInfo.siteName,","," ","all")#,#getAccountInfo.oosCat# - #getAccountInfo.oosDate#,#getAccountInfo.dealerNum#,#getAccountInfo.dealerName#,#getBill.servicePlan#,#getBill.serviceCode#,'
					addnewline="yes">
				</cfif>
			<cfelse>
				<cfif getAccountInfo.oosDate is not "">
					<cffile action="append" file="#outputFilePath#/uplinkonbillcancelleducc.csv"
					output='#getAccountInfo.accountNum#,#replace(getAccountInfo.siteName,","," ","all")#,#getAccountInfo.oosCat# - #getAccountInfo.oosDate#,#getAccountInfo.dealerNum#,#getAccountInfo.dealerName#,#getBill.servicePlan#,#getBill.serviceCode#,'
					addnewline="yes">
				</cfif>
			</cfif>
		</cfif>
	</cfloop>

	<!--- get accounts with a telular service code--->
	<cfquery name="getAccounts" datasource="uccbilling">
		select
		stages.site.SiteNum 'siteNum',
		stages.site.SiteName 'siteName',
		stages.Device.OOSStartDate 'oosDate',
		stages.Transmitter.TransmitterCode 'accountNum',
		stages.SiteContractItem.ContractItem 'serviceCode',
		stages.SiteGroupLink.SiteGroupNum 'dealerNum',
		stages.SiteGroup.name 'dealerName',
		stages.Device.DevNum 'devNum',
		stages.Device.OOSCat
		from stages.SiteContractItem
		join stages.device
		on stages.device.devnum = stages.sitecontractitem.devnum
		join stages.transmitter
		on stages.device.devnum = stages.transmitter.devnum
		join stages.site
		on stages.sitecontractitem.sitenum = stages.site.sitenum
		join stages.SiteGroupLink
		on stages.site.SiteNum = stages.sitegrouplink.SiteNum
		join stages.SiteGroup
		on stages.SiteGroup.SiteGroupNum = stages.SiteGroupLink.SiteGroupNum
		where contractitem in (#preservesinglequotes(serviceList)#)
	</cfquery>

	<!--- This part takes all accounts on UCC and checks to see if they are on the bill --->
	<cfloop query="getAccounts">
		<cfquery name="checkBill" dbtype="query">
			select *
			from getBill
			where automationid = '#getAccounts.accountNum#'
		</cfquery>
		<cfif checkBill.recordcount is 0>
			<cfif trim(getAccounts.oosDate) is not ''>
				<cffile action="append" file="#outputFilePath#/uplinknotonbillcancelled.csv"
				output='#getAccounts.accountNum#,#replace(getAccounts.siteName,","," ","all")#,#getAccounts.oosCat# - #getAccounts.oosDate#,#getAccounts.dealerNum#,#getAccounts.dealerName#,N/A,N/A,N/A'
				addnewline="yes">
			<cfelse>
				<cffile action="append" file="#outputFilePath#/uplinknotonbill.csv"
				output='#getAccounts.accountNum#,#replace(getAccounts.siteName,","," ","all")#,Active,#getAccounts.dealerNum#,#getAccounts.dealerName#,N/A,#getAccounts.serviceCode#,N/A,N/A'
				addnewline="yes">
			</cfif>
			<!---
			<cfquery name="removeService" datasource="uccbilling">
				delete
				from stages.sitecontractitem
				where siteNum = '#getAccounts.siteNum#'
				and contractItem = '#getAccounts.serviceCode#'
			</cfquery>
			--->
		</cfif>
	</cfloop>

	<!--- mail results to Kat--->
	<!---
	<cfmail from="reports@copsmonitoring.com" to="ktallman@copsmonitoring.com,tgolding@teamucc.com" subject="uplink Audit"  username="copalink@copsmonitoring.com" password="copsmoncal">
		<cfmailparam file="#outputFilePath#/uplinknotonbill.csv">
		<cfmailparam file="#outputFilePath#/uplinknotonbillcancelled.csv">
		<cfmailparam file="#outputFilePath#/uplinkserviceadded.csv">
		<cfmailparam file="#outputFilePath#/uplinkmissing.csv">
		<cfmailparam file="#outputFilePath#/uplinkonbillcancelleducc.csv">
	</cfmail>
	--->
		Audit is complete. Results have been emailed.
</cfoutput>
