<cfset today = dateformat(Now(),'mm/dd/yyyy')>
<cfset serviceAddTime = createodbcdatetime(now())>
<cfset starttime = Now()>
<cfset outputFilePath = "#request.DirectPath#\ucc\audits\alarmnet">
<cfsetting requesttimeout="30000">

<!--- delete old files --->
<cffile action="delete" file="#outputFilePath#/alarmnetmissing.csv">
<cffile action="delete" file="#outputFilePath#/alarmnetserviceadded.csv">
<cffile action="delete" file="#outputFilePath#/alarmnetnotonbillcancelled.csv">
<cffile action="delete" file="#outputFilePath#/alarmnetnotonbill.csv">
<cffile action="delete" file="#outputFilePath#/alarmnetonbillcancelleducc.csv">

<!--- create headers --->
<cffile action="append" file="#outputFilePath#/alarmnetmissing.csv"
output='Account Number On Bill-Missing from UCC,Account Name,OOS category,UCC Dealer Number, Dealer Name,Bill Code Desc,Bill Code,Charge'
addnewline="yes">
<cffile action="append" file="#outputFilePath#/alarmnetserviceadded.csv"
output='Account Number on Bill-Service Added to UCC,Account Name,OOS category,UCC Dealer Number, Dealer Name,Bill Code Desc,Bill Code,Charge'
addnewline="yes">
<cffile action="append" file="#outputFilePath#/alarmnetnotonbillcancelled.csv"
output='Canceled UCC Account Number-Not on bill,Account Name,OOS category,UCC Dealer Number, Dealer Name,Bill Code Desc,Bill Code,Charge'
addnewline="yes">
<cffile action="append" file="#outputFilePath#/alarmnetnotonbill.csv"
output='UCC account number missing from bill,Account Name,OOS category,UCC Dealer Number,Dealer Name,Bill Code Desc,Bill Code,Charge'
addnewline="yes">
<cffile action="append" file="#outputFilePath#/alarmnetonbillcancelleducc.csv"
output='UCC account number on bill- cancelled in UCC,Account Name,OOS category,UCC Dealer Number,Dealer Name,Bill Code Desc,Bill Code,Charge'
addnewline="yes">

<cfoutput>
	<!--- build service code list--->
	<cfquery name='getalarmnetServices' datasource="lydiabilling">
		select serviceCode
		from serviceConversion
		where vendor = 'Alarmnet'
	</cfquery>
	<cfset serviceList = quotedValueList(getalarmnetServices.serviceCode)>
	
	<!--- grab the bill --->
	<cfquery name="getBill" datasource="lydiabilling">
		select * from alarmnet
		join serviceconversion
		on alarmnet.itemnum = serviceconversion.item
		where billed = 'Yes'
		and ignore is NULL
		and serviceconversion.vendor = 'AlarmNet'
	</cfquery>

	<!--- this part takes the bill and makes sure the accounts are at UCC --->
	<cfloop query="getBill">
		<cfquery name="getAccountInfo" datasource="uccbilling">
			select 
			stages.site.SiteNum 'siteNum',
			stages.site.SiteName 'siteName',
			stages.Device.OOSStartDate 'oosDate',
			stages.Transmitter.TransmitterCode 'accountNum',
			stages.SiteContractItem.ContractItem 'serviceCode',
			stages.Sitegrouplink.SiteGroupNum 'dealerNum',
			stages.SiteGroup.name 'dealerName',
			stages.Device.DevNum 'devNum',
			stages.Device.OOSCat
			from stages.SiteContractItem
			join stages.device
			on stages.device.devnum = stages.sitecontractitem.devnum
			join stages.transmitter
			on stages.device.devnum = stages.transmitter.devnum
			join stages.site
			on stages.sitecontractitem.sitenum = stages.site.sitenum
			join stages.Sitegrouplink
			on stages.site.SiteNum = stages.sitegrouplink.SiteNum
			join stages.SiteGroup
			on stages.SiteGroup.SiteGroupNum = stages.Sitegrouplink.SiteGroupNum
			where 
			stages.Transmitter.TransmitterCode = '#trim(getBill.automationID)#'
		</cfquery>
		<cfif getAccountInfo.recordCount is 0 and trim(getBill.automationID) is not ''>
			<cfquery name="checkEntireDB" datasource="uccbilling">
				select 
				stages.site.SiteNum 'siteNum',
				stages.site.SiteName 'siteName',
				stages.Transmitter.TransmitterCode 'accountNum'
				from stages.Site
				join Stages.device on stages.site.SiteNum = stages.device.SiteNum
				join stages.Transmitter on stages.device.DevNum = stages.Transmitter.DevNum
				where stages.Transmitter.TransmitterCode = '#getBill.automationID#'
			</cfquery>
			<cfif checkEntireDB.recordCount is 0>
				<cfset SubscriberName = '#getBill.automationID#'>
			<cffile action="append" file="#outputFilePath#/alarmnetmissing.csv"
			output='#getBill.automationID#,#SubscriberName#,N/A,N/A,N/A,#getBill.svcdesc#,#getBill.itemnum#,'
			addnewline="yes">
			<cfelse>
				<cfif getAccountInfo.oosDate is ''>
					<cfquery name="doesAccountHaveService" dbtype="query">
						select *
						from getAccountInfo
						where 
						serviceCode = '#getBill.servicePlan#'
					</cfquery>
					<cfquery name="addService" datasource="uccbilling">
					insert into stages.sitecontractitem
					values ('#getAccountInfo.siteNum#','#getAccountInfo.devNum#','#getBill.serviceCode#',#serviceAddTime#)
					</cfquery>
				<cffile action="append" file="#outputFilePath#/alarmnetserviceadded.csv"
				output='#getAccountInfo.accountNum#,#replace(getAccountInfo.siteName,","," ","all")#,Active,#getAccountInfo.dealerNum#,#getAccountInfo.dealerName#,#getBill.svcdesc#,#getBill.itemnum#,'
				addnewline="yes">
				</cfif>
			</cfif>
		</cfif>
		<cfif getAccountInfo.recordCount gt 0>
			<cfquery name="doesAccountHaveService" dbtype="query">
				select *
				from getAccountInfo
				where 
				serviceCode = '#getBill.serviceCode#'
			</cfquery>
			<cfif doesAccountHaveService.recordCount is 0>
				<cfif getAccountInfo.oosDate is ''>
					<cfquery name="addService" datasource="uccbilling">
					insert into stages.sitecontractitem
					values ('#getAccountInfo.siteNum#','#getAccountInfo.devNum#','#getBill.serviceCode#',#serviceAddTime#)
					</cfquery>
					<cffile action="append" file="#outputFilePath#/alarmnetserviceadded.csv"
				output='#getAccountInfo.accountNum#,#replace(getAccountInfo.siteName,","," ","all")#,Active,#getAccountInfo.dealerNum#,#getAccountInfo.dealerName#,#getBill.svcdesc#,#getBill.itemnum#,'
				addnewline="yes">
				<cfelse>
					<cffile action="append" file="#outputFilePath#/alarmnetonbillcancelleducc.csv"
					output='#getAccountInfo.accountNum#,#replace(getAccountInfo.siteName,","," ","all")#,#getAccountInfo.oosCat# - #getAccountInfo.oosDate#,#getAccountInfo.dealerNum#,#getAccountInfo.dealerName#,#getBill.svcdesc#,#getBill.itemnum#,'
					addnewline="yes">
				</cfif>
			<cfelse>
				<cfif getAccountInfo.oosDate is not "">
						<cffile action="append" file="#outputFilePath#/alarmnetonbillcancelleducc.csv"
					output='#getAccountInfo.accountNum#,#replace(getAccountInfo.siteName,","," ","all")#,#getAccountInfo.oosCat# - #getAccountInfo.oosDate#,#getAccountInfo.dealerNum#,#getAccountInfo.dealerName#,#getBill.svcdesc#,#getBill.itemnum#,'
					addnewline="yes">
				</cfif>
			</cfif>
		</cfif>
	</cfloop>

	<!--- get accounts with a telular service code--->
	<cfquery name="getAccounts" datasource="uccbilling">
		select
		stages.site.SiteNum 'siteNum',
		stages.site.SiteName 'siteName',
		stages.Device.OOSStartDate 'oosDate',
		stages.Transmitter.TransmitterCode 'accountNum',
		stages.SiteContractItem.ContractItem 'serviceCode',
		stages.Sitegrouplink.SiteGroupNum 'dealerNum',
		stages.SiteGroup.name 'dealerName',
		stages.Device.DevNum 'devNum',
		stages.Device.OOSCat
		from stages.SiteContractItem
		join stages.device
		on stages.device.devnum = stages.sitecontractitem.devnum
		join stages.transmitter
		on stages.device.devnum = stages.transmitter.devnum
		join stages.site
		on stages.sitecontractitem.sitenum = stages.site.sitenum
		join stages.Sitegrouplink
		on stages.site.SiteNum = stages.sitegrouplink.SiteNum
		join stages.SiteGroup
		on stages.SiteGroup.SiteGroupNum = stages.Sitegrouplink.SiteGroupNum
		where contractitem in (#preservesinglequotes(serviceList)#)
	</cfquery>

	<!--- This part takes all accounts on UCC and checks to see if they are on the bill --->
	<cfloop query="getAccounts">
		<cfquery name="checkBill" dbtype="query">
			select *
			from getBill
			where automationid = '#getAccounts.accountNum#'
		</cfquery>
		<cfif checkBill.recordcount is 0>
			<cfif trim(getAccounts.oosDate) is not ''>
				<cffile action="append" file="#outputFilePath#/alarmnetnotonbillcancelled.csv"
					output='#getAccounts.accountNum#,#replace(getAccounts.siteName,","," ","all")#,#getAccounts.oosCat# - #getAccounts.oosDate#,#getAccounts.dealerNum#,#getAccounts.dealerName#,N/A,N/A,N/A'
					addnewline="yes">
			<cfelse>
				<cffile action="append" file="#outputFilePath#/alarmnetnotonbill.csv"
					output='#getAccounts.accountNum#,#replace(getAccounts.siteName,","," ","all")#,Active,#getAccounts.dealerNum#,#getAccounts.dealerName#,N/A,#getAccounts.serviceCode#,N/A,N/A'
					addnewline="yes">
			</cfif>
			<cfquery name="removeService" datasource="uccbilling">
				delete
				from stages.sitecontractitem
				where siteNum = '#getAccounts.siteNum#'
				and contractItem = '#getAccounts.serviceCode#'
			</cfquery>
		</cfif>
	</cfloop>

	<!--- mail results to Kat--->
	<cfmail from="reports@copsmonitoring.com" to="ktallman@copsmonitoring.com,tgolding@teamucc.com" subject="alarmnet Audit"  username="copalink@copsmonitoring.com" password="copsmoncal">
		<cfmailparam file="#outputFilePath#/alarmnetnotonbill.csv">
		<cfmailparam file="#outputFilePath#/alarmnetnotonbillcancelled.csv">
		<cfmailparam file="#outputFilePath#/alarmnetserviceadded.csv">
		<cfmailparam file="#outputFilePath#/alarmnetmissing.csv">
		<cfmailparam file="#outputFilePath#/alarmnetonbillcancelleducc.csv">
	</cfmail>
		Audit is complete. Results have been emailed.
</cfoutput>
