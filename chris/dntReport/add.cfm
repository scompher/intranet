<!--- LOAD JAVA --->
<script src="/chris/dntReport/script/dntAddJava.js"></script>

<!--- SET PARAMETERS --->
<cfparam name='dealerList' default=''>
<cfparam name='emailList' default=''>
<cfparam name='frequency' default=''>
<cfparam name='day' default=''>
<cfparam name='reportName' default=''>
<cfparam name='activeDate' default=''>
<cfparam name='rptName' default=''>
<cfparam name='actDate' default=''>
<cfset activeDate=dateFormat(now(),"mm/dd/yyyy")>
<cfoutput>
<br><br>

<!--- DISPLAY INSTRUCTIONS --->
<table width=90% align='center'>
	<tr><td>
		<b style='font-size: 14px;'>Adding New Report...</b>
		<p style='font-size: 12px;'>-Enter new dealer number or e-mail in the appropriate field.</p>
		<p style='font-size: 12px;'>-Click on one or more Dealer/E-mail and use [Backspace] or [Delete] to remove them from the list. (You can also remove a Dealer/E-mail by re-entering it into the appropriate field)</p>
		<p style='font-size: 12px;'>-Reports are set to be 'active' the day they are created, but this can be changed by clicking on the Date field and entering or choosing a date from the calender.
	</td></tr>
</table>
<br>

<!--- DISPLAY INPUT FIELDS --->
<table width=90% align='center'>
	
	<!--- DEALER/E-MAIL INPUT --->	
	<tr>
		<td><b style='font-size: 12px;'>Dealer ##: </b></td>
		<td width='13px'></td>
		<td align='left'><b style='font-size: 12px;'>E-Mail: </b></td>
	</tr><tr>
		<td><input id='dealer' type='text'></td>
		<td colspan=2 align='right'><input id='email' type='text'></td>
    </tr><tr>
    	<td><div id='dlrBox' name='dlrBox'></div></td>
    	<td colspan=2 align='right'><div id='emlBox' name='emlBox'></div></td>
	</tr>
</table>
	
	
<table width=90% align='center'>
	<!--- REPORT NAME/ACTIVATION DATE INPUTS --->	
	<tr>
		<td><b style='font-size: 12px;'>Report Name:</b></td>
		<td colspan=3><input id='reportName' type='text'></td>
		<td><b style='font-size: 12px;'>Activation Date:</b></td>
		<td align='right'><input id='activeDate' type='text' value='#activeDate#'></td>
	</tr><tr>
		<td><b style='font-size: 12px;'>Frequency:</b></td>
		<td><div id='freqDrop'></td>
		<td><b style='font-size: 12px;'>Day:</b></td>
		<td><div id='dayDrop'></td>
		<td></td>
		
		<td>
		<!--- HIDDEN FORM FOR SUBMISSION --->
		<form id="inputForm" method="post" action="query/save.cfm">
    		<input type='input' id='dealerList' name='dealerList' value='#dealerList#'>
    		<input type='input' id='emailList'  name='emailList'  value='#emailList#'>
    		<input type='input' id='actDate'    name='actDate'    value='#actDate#'>
    		<input type='input' id='rptName'    name='rptName'    value='#rptName#'>
    		<input type='input' id='frequency'  name='frequency'  value='#frequency#'>
    		<input type='input' id='day'        name='day'        value='#day#'>
			<td align='right'><input id="submit" type='button' value='Submit'></td>
		</form>
		</td>
	</tr>
	
	<!--- SPACER --->	
	<tr><td colspan='7'><br></td></tr>
	
	<!--- DISPLAY ERROR MESSAGE --->	
	<tr><td align='right' colspan='7'><div id='errorContent' text='FF0000'><b id='error'></b></div></td></tr>
	
	<!--- SPACER --->	
	<tr><td colspan='7'><br></td></tr>
</table>	

<!--- TEST FIELD --->
<div id='addTest'></div>
</cfoutput>