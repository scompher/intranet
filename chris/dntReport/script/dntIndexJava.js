$(document).ready(function () {
		
	/* CREATE TABBED WINDOWS */
    $('#dntTabs').jqxTabs({ width: 900, theme: 'energyblue' });
    $('#dntTabs').jqxTabs('disableAt', 2);
    $('#dntTabs').bind('selected', function (event) {
        var item = event.args.item;
        var title = $('#dntTabs').jqxTabs('getTitleAt', item);
    });
        
    /* KEEP EDIT TAB DISABLED EXCEPT WHEN IN USE */
 	$('#dntTabs').on('tabclick', function (event) {
     	var tabCLicked = event.args.item;
     	if ( tabCLicked != 2 ) { $('#dntTabs').jqxTabs('disableAt', 2) };
    });
});