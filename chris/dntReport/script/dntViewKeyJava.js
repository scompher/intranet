/* KEY LOGIC FOR VIEW.CFM */
document.addEventListener("keydown", keyLogic);
function keyLogic() {	
			
	// Only allow function on View Tab.
	var selectedItem = $('#dntTabs').jqxTabs('selectedItem'); 
	if ( selectedItem == 0 ) {
	
	// Make sure row is selected...
	var thisIndex = $('#reportTable').jqxGrid('getselectedrowindex');
	if ( thisIndex != -1 ) {
		
		// Get key pressed.
   		var thisKey = event.which;
       	
    	// Enter Key Logic
   		if ( thisKey == 13 ) {		
   			
   			// Grab info from selected row and head to edit tab.
   			$("#dlrBoxEdit").jqxListBox('clear');
    		$("#emlBoxEdit").jqxListBox('clear');
    		var rowIndex = thisIndex;
    		var edID = $('#reportTable').jqxGrid('getcellvalue', thisIndex, 'hiddenID');
			var edName = $('#reportTable').jqxGrid('getcellvalue', thisIndex, 'reportName');
			var edDlr = $('#reportTable').jqxGrid('getcellvalue', thisIndex, 'dealers');
			var edEml = $('#reportTable').jqxGrid('getcellvalue', thisIndex, 'emails');
    		edDlr = edDlr.split("<br>");
    		edEml = edEml.split("<br>");
    		$('#dntTabs').jqxTabs('enableAt', 2);
    		$('#dntTabs').jqxTabs('select', 2);
    		$("#editID").val(edID) 
    		$("#reportNameEdit").val(edName) 
			var dLen = edDlr.length;
    		for (i = 0; i < dLen; i++) { 
    			var dValue = edDlr[i];
				$("#dlrBoxEdit").jqxListBox('insertAt', dValue, -1 ); 
			};
			var eLen = edEml.length;
    		for (i = 0; i < eLen; i++) { 
    			var eValue = edEml[i];
				$("#emlBoxEdit").jqxListBox('insertAt', eValue, -1 ); 
			};
			$('#dealerEdit').jqxInput('focus');
				
		// Delete/Backspace key logic.
   		} else if ( thisKey == 8 || thisKey == 46 ) {
   			
   			// Grab selected report ID and deactivate that report.
     		var selID = $('#reportTable').jqxGrid('getcellvalue', thisIndex, 'hiddenID');
			var selName = $('#reportTable').jqxGrid('getcellvalue', thisIndex, 'reportName');
    		var r = confirm('Are you sure you want to DEACTIVATE "'+selName+'"?');
    		if (r == true) {
        		$("#selID").val(selID) 
        		document.getElementById("deactivateForm").submit();
    		};
    	};
    	
    // ... or refocus.
    } else {
		$('#reportTable').jqxInput('focus');
    }; 
    };
};