<link rel="stylesheet" type="text/css" href="/styles.css">
<link rel="stylesheet" href="/jquery-ui-1.11.4/jquery-ui.min.css" />
<link rel="stylesheet" href="/jquery-ui-1.11.4/jquery-ui.theme.min.css"/>
<link rel="stylesheet" href="/jqwidgets/jqwidgets/styles/jqx.base.css" type="text/css"> 
<link rel="stylesheet" href="/jqwidgets/jqwidgets/styles/jqx.energyblue.css" type="text/css">
<script type="text/javascript" src="/jqwidgets/scripts/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="/jquery-ui-1.11.4/jquery-ui.min.js"></script>
<script type="text/javascript" src="/jqwidgets/jqwidgets/jqxcore.js"></script>
<script type="text/javascript" src="/jqwidgets/jqwidgets/jqxbuttons.js"></script>
<script type="text/javascript" src="/jqwidgets/jqwidgets/jqxdata.js"></script>
<script type="text/javascript" src="/jqwidgets/jqwidgets/jqxdatatable.js"></script>
<script type="text/javascript" src="/jqwidgets/jqwidgets/jqxdropdownlist.js"></script>
<script type="text/javascript" src="/jqwidgets/jqwidgets/jqxgrid.js"></script>
<script type="text/javascript" src="/jqwidgets/jqwidgets/jqxgrid.filter.js"></script>
<script type="text/javascript" src="/jqwidgets/jqwidgets/jqxgrid.pager.js"></script>
<script type="text/javascript" src="/jqwidgets/jqwidgets/jqxgrid.selection.js"></script>
<script type="text/javascript" src="/jqwidgets/jqwidgets/jqxinput.js"></script>
<script type="text/javascript" src="/jqwidgets/jqwidgets/jqxlistbox.js"></script>
<script type="text/javascript" src="/jqwidgets/jqwidgets/jqxmenu.js"></script>
<script type="text/javascript" src="/jqwidgets/jqwidgets/jqxscrollbar.js"></script>
<script type="text/javascript" src="/jqwidgets/jqwidgets/jqxtabs.js"></script>