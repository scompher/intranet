$(document).ready(function () {
				
	$("#dealerEdit").jqxInput({
		placeHolder: 'Dealer #',
		height: 25, 
		width: 74, 
		minLength: 1, 
		maxLength: 4, 
		searchMode: 'none',
		theme: 'energyblue' 
	});
	
	
	$("#emailEdit").jqxInput({
		placeHolder: 'E-mail',
		height: 25, 
		width: 673, 
		minLength: 1, 
		maxLength: 70, 
		theme: 'energyblue' 
	});
	
	
	/* CREATE REPORT NAME INPUT */
	$("#reportNameEdit").jqxInput({
		placeHolder: 'Report Name',
		height: 25, 
		width: 400, 
		minLength: 1, 
		maxLength: 16, 
		disabled: true,
		theme: 'energyblue' 
	});
		
		
	/* CREATE START DATE INPUT */
	$("#activeDateEdit").jqxInput({
		placeHolder: 'Activation Date',
		height: 25, 
		width: 110, 
		minLength: 1, 
		maxLength: 10, 
		theme: 'energyblue' 
	});
		
		
	/* CREATE SUBMIT BUTTON */
       $("#submitEdit").jqxButton({ 
		height: 25, 
		width: 70, 
		theme: 'energyblue' 
	});
		 
		 
	/* CREATE DEALER LIST BOX*/
	var dlrSource = [];
    $("#dlrBoxEdit").jqxListBox({ 
       	source: dlrSource,
       	height: 250,  
       	width: 80, 
       	multiple: true,
       	theme: 'energyblue' 
    });
		 
		 
	/* CREATE E-MAIL LIST BOX*/
	var emlSource = [];
    $("#emlBoxEdit").jqxListBox({ 
       	source: emlSource,
       	height: 250,  
       	width: 679, 
       	multiple: true,
       	theme: 'energyblue' 
    });

		
	/* GIVE DEALER INPUT INITIAL FOCUS */
	$('#dealerEdit').jqxInput('focus');     

	
	/* UPDATE LIST OF DEALERS TO ADD */
	$("#dealerEdit").keydown(function(event) { 
		
		// Get key pressed.
		$('#errorEdit').html('');
   		var thisKey = event.which;
       	
       	// Enter Key Logic
   		if ( thisKey == 13 ) {
   			
   			// Get input and make uppercase.
   			var value = $('#dealerEdit').val();
			value = value.toUpperCase();
			
			// Make sure a value has been entered...
			if (value != '') {
				
				// Get current dealer list.		
   				var dlrList = [];
				var dlrItems = $("#dlrBoxEdit").jqxListBox('getItems'); 
   				for (var i = 0; i < dlrItems.length; i++) { dlrList[i] = dlrItems[i].label };		
				var dlrIndex = dlrList.indexOf(value)
					
				// If the value is not on the list, add it...
				if ( dlrIndex < 0) {
					$("#dlrBoxEdit").jqxListBox('insertAt', value, -1 ); 
											
				// ...otherwise remove it. 
				} else {
					$("#dlrBoxEdit").jqxListBox('removeAt', dlrIndex ); 
				};
					
				// Clear input field.
				$('#dealerEdit').val('');
					
			// ...or...
			} else {
				
				// ...Make sure a dealer list exits.
   				var dlrList = [];
				var dlrItems = $("#dlrBoxEdit").jqxListBox('getItems'); 
   				for (var i = 0; i < dlrItems.length; i++) { dlrList[i] = dlrItems[i].label };
				if ( dlrList != '' ) {
						
					// Make sure an e-mail list exists.
   					var emlList = [];
					var emlItems = $("#emlBoxEdit").jqxListBox('getItems'); 
   					for (var i = 0; i < emlItems.length; i++) { emlList[i] = emlItems[i].label };
						
					// If so, submit...
					if ( emlList != '' ) {
						$('#submitEdit').jqxButton('focus');
							
					// ...otherwise return Error.
					} else {
						$('#emailEdit').jqxInput('focus');
						$('#errorEdit').html('*Error: Submission requires at least 1 E-mail!');
					};
				
				// ...otherwise return Error.	
				}  else  {
					$('#errorEdit').html('*Error: Submission requires at least 1 Dealer!');
       			};
			};
			
		// Down arrow key logic.
   		} else if ( thisKey == 40 ) {
			$('#dlrBoxEdit').jqxListBox('focus');
   		};
	});
		
		
	/* UPDATE LIST OF E-MAILS TO ADD */
	$("#emailEdit").keydown(function(event){ 
		
		// Get key pressed.
		$('#errorEdit').html('');
   		var thisKey = event.which;
        	
       	// Enter key logic.
   		if ( thisKey == 13 ) {
    			
   			// Get input and make uppercase.
   			var value = $('#emailEdit').val();
			value = value.toUpperCase();
			
			// Make sure a value has been entered...
			if (value != '') {
					
				// Validate e-mail...
				var atCheck = value.includes('@');
				var prCheck = value.includes('.');
				if (atCheck == true && prCheck == true) {
						
					// Get current E-mail list.
   					var emlList = [];
					var emlItems = $("#emlBoxEdit").jqxListBox('getItems'); 
   					for (var i = 0; i < emlItems.length; i++) { emlList[i] = emlItems[i].label };		
					var emlIndex = emlList.indexOf(value)
					
					// If the value is not on the list, add it...
					if ( emlIndex < 0) {
						$("#emlBoxEdit").jqxListBox('insertAt', value, -1 ); 
					
					// ...otherwise remove it. 
					} else {
						$("#emlBoxEdit").jqxListBox('removeAt', emlIndex ); 
					};
					
				// ...or return an Error.
				} else {
					$('#errorEdit').html('*Error: Invalid E-mail!');
				};
					
				// Clear input field.
				$('#emailEdit').val('');
					
			// ...or...
			} else {
				
				// ...make sure e-mail list exists....
   				var emlList = [];
				var emlItems = $("#emlBoxEdit").jqxListBox('getItems'); 
  				for (var i = 0; i < emlItems.length; i++) { emlList[i] = emlItems[i].label };
				if ( emlList != '' ) {
						
					// Make sure a dealer list exists.
   					var dlrList = [];
					var dlrItems = $("#dlrBoxEdit").jqxListBox('getItems'); 
   					for (var i = 0; i < dlrItems.length; i++) { dlrList[i] = dlrItems[i].label };
						
					// If so, submit...
					if ( dlrList != '' ) {
						$('#submitEdit').jqxButton('focus');
						
					// ...otherwise return Error.
					} else {
						$('#dealerEdit').jqxInput('focus');
						$('#errorEdit').html('*Error: Submission requires at least 1 Dealer!');
					};
				
				// ...otherwise return Error.	
				}  else  {
					$('#errorEdit').html('*Error: Submission requires at least 1 E-mail!');
       			};
			};
   		
		// Down arrow key logic.
   		} else if ( thisKey == 40 ) {
			$('#emlBoxEdit').jqxListBox('focus');
   		};
	});
		
				
	/* DEALER BOX CONTROLS */	
	$('#dlrBoxEdit').on('select', function (row) {
   		var rowArgs = row.args;
   		if (rowArgs) {
			$('#dlrBoxEdit').keydown(function (event) {
       			var dlrIndex = rowArgs.index;
       				
       			// Get key pressed.
				$('#errorEdit').html('');
   				var thisKey = event.which;
       	
       			// Delete or backspace key logic.
   				if ( thisKey == 8 || thisKey == 46 ) {
   					var dlrItems = $("#dlrBoxEdit").jqxListBox('getSelectedItems'); 
   					var dlrLen = dlrItems.length;
   					if (dlrLen > 0) {
       					for (var i = dlrLen-1; i >= 0 ; i--) {
							$("#dlrBoxEdit").jqxListBox('removeAt', dlrItems[i].index ); 
							$("#dlrBoxEdit").jqxListBox('unselectIndex', dlrItems[i].index ); 
       					};
    				};
    			};
			});
    	};
	});
		
				
	/* E-MAIL BOX CONTROLS */	
	$('#emlBoxEdit').on('select', function (row) {
   		var rowArgs = row.args;
   		if (rowArgs) {
			$('#emlBoxEdit').keydown(function (event) {
       			var emlIndex = rowArgs.index;
        				
       			// Get key pressed.
				$('#error').html('');
   				var thisKey = event.which;
        	
       			// Delete or backspace key logic.
   				if ( thisKey == 8 || thisKey == 46 ) {
 					var emlItems = $("#emlBoxEdit").jqxListBox('getSelectedItems'); 
   					var emlLen = emlItems.length;
   					if (emlLen > 0) {
       					for (var i = emlLen-1; i >= 0 ; i--) {
							$("#emlBoxEdit").jqxListBox('removeAt', emlItems[i].index ); 
							$("#emlBoxEdit").jqxListBox('unselectIndex', emlItems[i].index ); 
       					};
   					};
   				};
			});
   		};
	});
		
		
	/* START-DATE CALENDER */
	$( function() {
		$( "#activeDateEdit" ).datepicker();
	} );
		
		
	/* SUBMIT BUTTON LOGIC */
	$("#submitEdit").on('click', function(event){ 
			
   		// Get dealer and email lists
   		var emlList = [];
   		var dlrList = [];
   		var reportName = $("#reportNameEdit").val()
   		var activeDate = $("#activeDateEdit").val();
		var emlItems = $("#emlBoxEdit").jqxListBox('getItems'); 
  		for (var i = 0; i < emlItems.length; i++) { emlList[i] = emlItems[i].label };
		var dlrItems = $("#dlrBoxEdit").jqxListBox('getItems'); 
   		for (var i = 0; i < dlrItems.length; i++) { dlrList[i] = dlrItems[i].label };
				
		// If dealer list is missing, return error and set focus to dealer...
		submitCheck: {
		if ( dlrList == '' ) {
			$('#dealerEdit').jqxInput('focus');
			$('#errorEdit').html('*Error: Submission requires at least 1 Dealer!');
			break submitCheck;
				
		// ...or if e-mail list is missing, return error and set focus to e-mail...	
		} else if ( emlList == '' ) {
			$('#emailEdit').jqxInput('focus');
			$('#errorEdit').html('*Error: Submission requires at least 1 E-mail!');
			break submitCheck;
					
		// ...otherwise continue.
		} else {
			if ( activeDate != '' ) {
				if ( activeDate.length != 8 && activeDate.length != 10 ) {
					$('#activeDateEdit').jqxInput('focus');
					$('#errorEdit').html('*Error: Active date incorrect format!');
				} else if ( activeDate[2] != '/' || activeDate[5] != '/') {
					$('#activeDateEdit').jqxInput('focus');
					$('#errorEdit').html('*Error: Active date incorrect format!');
				} else {
					$('#errorEdit').html('*Form SUBMITTED!');
					$('#dealerEdit').jqxInput('focus');
					$('#dealerListEdit').val(dlrList);
					$('#emailListEdit').val(emlList);
					$('#frequencyEdit').val('Daily');
					$('#dayEdit').val('');
					$('#rptNameEdit').val(reportName);
					$('#actDateEdit').val(activeDate);
					document.getElementById('editForm').submit();
				};
			} else {
					$('#activeDateEdit').jqxInput('focus');
					$('#errorEdit').html('*Error: Active date required!');
			};
		}};
	});		
});