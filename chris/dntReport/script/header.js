$(document).ready(function() {	
    	
	/* CUSTOM BUTTON ANIMATION IN */
    $("img").mouseenter(function(event) {
		var eventID = event.target.id
		if (eventID == "homeButton") {
          	$("#homeButton").attr("src","/dntReport/images/btnMPower2.png")
		}
		if (eventID == "logOutButton") {
           	$("#logOutButton").attr("src","/dntReport/images/btnLogOut2.png")
		}
    });
        
    /* CUSTOM BUTTON ANIMATION OUT */
    $("img").mouseleave(function(event) {
		var eventID = event.target.id
		if (eventID == "homeButton") {
       		$("#homeButton").attr("src","/dntReport/images/btnMPower1.png")
		}
		if (eventID == "logOutButton") {
           	$("#logOutButton").attr("src","/dntReport/images/btnLogOut1.png")
		}
    });
}); 