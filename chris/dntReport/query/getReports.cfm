<!--- GET ALL ACTIVE REPORTS --->
<cfquery name='getReports' dataSource='#ds#'>
	select *
	from dntReport_Main
	where active = 1
</cfquery>

<!--- LOAD EMPTY ARRAYS --->
<cfset currentIDs=[]>
<cfset currentReports=[]>
<cfset currentFrequencies=[]>
<cfset currentDays=[]>
<cfset currentDealers=[]>
<cfset currentEmails=[]>

<!--- GET DATA FROM ACTIVE REPORTS --->
<cfset idCnt=0>
<cfloop query='getReports'>
	<cfset idCnt+=1>
	<cfset currentIDs[#idCnt#]='#getReports.ID#'>
	<cfset currentReports[#idCnt#]='#getReports.reportName#'>
	<cfset currentFrequencies[#idCnt#]='#getReports.frequency#'>
	<cfset currentDays[#idCnt#]='#getReports.day#'>
	
	<!--- GET DEALER INFO --->
	<cfquery name='getDealers' dataSource='#ds#'>
		select *
		from dntReport_Dealer
		where reportID = #getReports.id#
	</cfquery>
	<cfset dlrCnt=0>
	<cfset currentDealers[#idCnt#]=''>	
	<cfloop query='getDealers'>
		<cfset dlrCnt+=1>
		<cfif dlrCnt NEQ 1><cfset currentDealers[#idCnt#]&=','></cfif>
		<cfset currentDealers[#idCnt#]&='#getDealers.dealer#'>	
	</cfloop>
	
	<!--- GET E-MAIL INFO --->
	<cfquery name='getEmails' dataSource='#ds#'>
		select *
		from dntReport_Email
		where reportID = #getReports.id#
	</cfquery>
	<cfset emlCnt=0>
	<cfset currentEmails[#idCnt#]=''>
	<cfloop query='getEmails'>
		<cfset emlCnt+=1>
		<cfif emlCnt NEQ 1><cfset currentEmails[#idCnt#]&=','></cfif>
		<cfset currentEmails[#idCnt#]&='#getEmails.email#'>
	</cfloop>
</cfloop>

<!--- CONVERT ARRAY TO ":" SEPERATED LIST --->
<cfscript>
	idsList=ArrayToList(currentIDs,':');
	frqList=ArrayToList(currentFrequencies,':');
	dayList=ArrayToList(currentDays,':');
	rptList=ArrayToList(currentReports,':');
	dlrList=ArrayToList(currentDealers,':');
	emlList=ArrayToList(currentEmails,':');
</cfscript>