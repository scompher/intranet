<!--- START FILE AND SET TITLE --->
<html>
<head>
	<title>COPS Monitoring</title>
</head>

<!--- LOAD SCRIPT DATA --->
<cfinclude template="/dntReport/script/java.cfm">

<!--- LOAD CSS DATA  --->
<link rel="stylesheet" href="/dntreport/script/styles.css">

<!--- LOAD HEADER JAVA --->
<script src="/dntReport/script/header.js"></script>

<!--- SET BODY --->
<body bgcolor="#F0F0F0" text="#0F0F0F" leftmargin=0 topmargin=0 marginheight=0 marginwidth=0>
	
	<!--- DISPLAY RETURN LOGO --->
	<table padding=0 border=0 width="100%"><tr>
		<td width="20px"></td>
		<td><divHeaderRow>
				<divHeaderCell>
					<div  class ='tooltip'>
						<a href="/"><img id="copsButton" src="/dntReport/image/cops-logo.jpg" style="vertical-align:top"></a>
						<span class="tooltiptext">C.O.P.S</span>
					</div>
				</divHeaderCell>
		</divHeaderRow></td>
		<td width="20px"></td>
	</tr></table>
	
	<!--- DISPLAY LINE BREAK --->
	<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>
		<td width="20px"></td>
		<td><hr></td>
		<td width="20px"></td>
	</tr></table>
	<!--- LEAVE OPEN FOR CALLING PAGE --->