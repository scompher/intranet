	<br>
	<!--- DISPLAY LINE BREAK --->
	<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>
		<td width="20px"></td>
		<td><hr></td>
		<td width="20px"></td>
	</tr></table>
	<!--- DISPLAY COPYRIGHT ---->
	<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>
		<td width="20px"></td>
		<td align="center">
			<span class="footer">Copyright &copy; <cfoutput>#dateformat(now(),'YYYY')#</cfoutput>, COPS Monitoring</span>
			<br><br>
		</td>
		<td width="20px"></td>
	</tr></table>
</body>
</html>