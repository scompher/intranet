<!--- LOAD JAVA --->
<script src="/dntReport/script/dntEditJava.js"></script>

<!--- SET PARAMETERS --->
<cfparam name='editID' default=''>
<cfparam name='dealerListEdit' default=''>
<cfparam name='emailListEdit' default=''>
<cfparam name='frequencyEdit' default=''>
<cfparam name='dayEdit' default=''>
<cfparam name='reportNameEdit' default=''>
<cfparam name='activeDateEdit' default=''>
<cfparam name='rptNameEdit' default=''>
<cfparam name='actDateEdit' default=''>
<cfset activeDateEdit=dateFormat(now(),"mm/dd/yyyy")>
<cfoutput>
<br><br>

<!--- DISPLAY INSTRUCTIONS --->
<table width=90% align='center'>
	<tr><td>
		<b style='font-size: 14px;'>Editing Existing Report...</b>
		<p style='font-size: 12px;'>-Enter new dealer number or e-mail in the appropriate field.</p>
		<p style='font-size: 12px;'>-Click on one or more Dealer/E-mail and use [Backspace] or [Delete] to remove them from the list. (You can also remove a Dealer/E-mail by re-entering it into the appropriate field)</p>
		<p style='font-size: 12px;'>-Reports are set to be 'active' the day they are created, but this can be changed by clicking on the Date field and entering or choosing a date from the calender.
	</td></tr>
</table>
<br>

<!--- DISPLAY INPUT FIELDS --->
<table width=90% align='center'>
	
	<!--- DEALER/E-MAIL INPUT --->	
	<tr>
		<td><b style='font-size: 12px;'>Dealer ##: </b></td>
		<td width='13px'></td>
		<td align='left'><b style='font-size: 12px;'>E-Mail: </b></td>
	</tr><tr>
		<td><input id='dealerEdit' type='text'></td>
		<td colspan=2 align='right'><input id='emailEdit' type='text'></td>
    </tr><tr>
    	<td><div id='dlrBoxEdit' name='dlrBoxEdit'></div></td>
    	<td colspan=2 align='right'><div id='emlBoxEdit' name='emlBoxEdit'></div></td>
	</tr>
</table>
	
	
<table width=90% align='center'>
	<!--- REPORT NAME/ACTIVATION DATE INPUTS --->	
	<tr>
		<td><b style='font-size: 12px;'>Report Name:</b></td>
		<td colspan=3><input id='reportNameEdit' type='text'></td>
		<td><b style='font-size: 12px;'>Activation Date:</b></td>
		<td align='right'><input id='activeDateEdit' type='text' value='#activeDate#'></td>
	</tr><tr>
		<td><b style='font-size: 12px;'>Frequency:</b></td>
		<td><div id='freqDropEdit'></td>
		<td><b style='font-size: 12px;'>Day:</b></td>
		<td><div id='dayDropEdit'></td>
		<td></td>
		
		<!--- HIDDEN FORM FOR SUBMISSION --->
		<form id="editForm" name="editForm" method="post" action="query/update.cfm">    		
			<input type='hidden' name='editID'         id='editID' 		   value='#editID#'>
    		<input type='hidden' name='dealerListEdit' id='dealerListEdit' value='#dealerListEdit#'>
    		<input type='hidden' name='emailListEdit'  id='emailListEdit'  value='#emailListEdit#'>
    		<input type='hidden' name='actDateEdit'    id='actDateEdit'    value='#actDateEdit#'>
    		<input type='hidden' name='rptNameEdit'    id='rptNameEdit'    value='#rptNameEdit#'>
    		<input type='hidden' name='frequencyEdit'  id='frequencyEdit'  value='#frequencyEdit#'>
    		<input type='hidden' name='dayEdit'        id='dayEdit'        value='#dayEdit#'>
			<td align='right'><input id='submitEdit' type='submit' value='Save'></td>
		</form>
	</tr>
	
	<!--- SPACER --->	
	<tr><td colspan='7'><br></td></tr>
	
	<!--- DISPLAY ERROR MESSAGE --->	
	<tr><td align='right' colspan='7'><div id='errorContentEdit' text='FF0000'><b id='error'></b></div></td></tr>
	
	<!--- SPACER --->	
	<tr><td colspan='7'><br></td></tr>
</table>	

<!--- TEST FIELD --->
<div id='editTest'></div>
</cfoutput>