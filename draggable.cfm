

<cfquery name="getitems" datasource="#request.odbc_datasource#">
select Admin_Security_Sections.*, Admin_Security_Items.*
	from Admin_Security_Sections
	inner join admin_security_items on admin_security_sections.sectionid = admin_security_items.sectionid
	inner join admin_security_lookup on admin_security_items.itemid = admin_security_lookup.itemid
	where admin_security_lookup.adminuserid = #cookie.adminlogin#
	order by admin_security_sections.sectionname asc, admin_security_items.itemname asc
</cfquery>
<cfquery name="getSections" dbtype="query">
	select distinct sectionid from getitems 
</cfquery>
<cfset itemsPerColumn = ceiling(getsections.recordcount / 5)>


<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Intranet</title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/themes/smoothness/jquery-ui.css" />
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js"></script>
	<style>
	body {
	min-width: 520px;
	}
	.column {
	width: 300px;
	float: left;
	padding-bottom: 100px;
	}
	.portlet {
	margin: 0 1em 1em 0;
	padding: 0.3em;
	}
	.portlet-header {
	padding: 0.2em 0.3em;
	margin-bottom: 0.5em;
	position: relative;
	font-size:0.75em;
	}
	.portlet-toggle {
	position: absolute;
	top: 50%;
	right: 0;
	margin-top: -8px;
	}
	.portlet-content {
	padding: 0.4em;
	font-size:0.75em;
	}
	.portlet-placeholder {
	border: 1px dotted black;
	margin: 0 1em 1em 0;
	height: 50px;
	}
	</style>
	<script>
	$(function() {
	$( ".column" ).sortable({
	  connectWith: ".column",
	  handle: ".portlet-header",
	  cancel: ".portlet-toggle",
	  placeholder: "portlet-placeholder ui-corner-all"
	});
	
	$( ".portlet" )
	  .addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" )
	  .find( ".portlet-header" )
		.addClass( "ui-widget-header ui-corner-all" )
		.prepend( "<span class='ui-icon ui-icon-minusthick portlet-toggle'></span>");
	
	$( ".portlet-toggle" ).click(function() {
	  var icon = $( this );
	  icon.toggleClass( "ui-icon-minusthick ui-icon-plusthick" );
	  icon.closest( ".portlet" ).find( ".portlet-content" ).toggle();
	});
	});
	</script>
</head>

<body>

<cfloop from="1" to="5" index="c">
<cfset startNum = (c * 5) - 4>
<div class="column">
<cfoutput query="getitems" group="sectionid" startrow="#startNum#" maxrows="#itemsPerColumn#">
  <div class="portlet">
    <div class="portlet-header">#getitems.sectionname#</div>
    <div class="portlet-content">
	<cfoutput>
	<a href="#getitems.itemURL#">#getitems.itemName#</a><br>
	</cfoutput>
	</div>
  </div>
</cfoutput>
</div> 
</cfloop>
 
</body>
</html>


<!--- 
<div class="column">
 
  <div class="portlet">
    <div class="portlet-header">Shopping</div>
    <div class="portlet-content">Lorem ipsum dolor sit amet, consectetuer adipiscing elit</div>
  </div>
 
</div>
 
<div class="column">
 
  <div class="portlet">
    <div class="portlet-header">Links</div>
    <div class="portlet-content">Lorem ipsum dolor sit amet, consectetuer adipiscing elit</div>
  </div>
 
  <div class="portlet">
    <div class="portlet-header">Images</div>
    <div class="portlet-content">Lorem ipsum dolor sit amet, consectetuer adipiscing elit</div>
  </div>
 
</div> --->
 
