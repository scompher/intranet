<link rel="stylesheet" type="text/css" href="../styles.css">

<cfif isdefined("form.btnDonate")>
	<cfif trim(form.name) is not "" and trim(form.department) is not "" and trim(form.donating) is not "">
		<cfquery name="addDonation" datasource="#ds#">
			insert into helpingfamily (name, department, donating, inputdate)
			values ('#form.name#', '#form.department#', '#form.donating#', #createodbcdatetime(now())#)
		</cfquery>
	</cfif>
</cfif>

<cfquery name="getDonations" datasource="#ds#">
	select * from helpingfamily order by inputdate desc
</cfquery>

<div align="center" class="normal">
<table width="625" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center"><img src="bells.gif" width="250" height="213"><br />
			<br />
		</td>
	</tr>
	<tr>
		<td align="center">
		<a style="text-decoration:underline" href="/index.cfm">Return to Intranet Homepage</a><br /><br />
		</td>
	</tr>
	<tr>
		<td class="highlightbar"><b>C.O.P.S. Helping Families!</b></td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5" class="grey">
				<tr>
					<td nowrap="nowrap"><b>Please submit what items you will be donating. Below is a list of items being donated by others: </b><br /><br />(please fill out name, department, and items being donated)</td>
				</tr>
				<tr>
					<td class="nopadding">
						<cfif not isdefined("form.btnDonate")>
						<table border="0" cellpadding="5" cellspacing="0" class="grey">
							<form method="post" action="inputform.cfm">
							<tr>
								<td nowrap><b>Your Name: </b></td>
								<td>
									<input name="name" type="text" id="name" maxlength="100" style="width:400px" >
								</td>
							</tr>
							<tr>
								<td nowrap="nowrap"><b>Your Department: </b></td>
								<td>
									<input name="department" type="text" id="name2" maxlength="100" style="width:400px">
								</td>
							</tr>
							<tr>
								<td colspan="2"><b>What items you will be donating: </b></td>
								</tr>
							<tr>
								<td colspan="2">
									<textarea name="donating" rows="5" id="donating" style="width:490px"></textarea>
								</td>
								</tr>
							<tr>
								<td colspan="2">
									<input name="btnDonate" type="submit" class="sidebar" id="btnDonate" value="Submit Items" onClick="return confirm('Are you sure you wish to submit? You cannot go back and change once you do.');">
								</td>
							</tr>
							<tr>
								<td colspan="2">&nbsp;</td>
							</tr>
							</form>
						</table>
						<cfelse>
						<table border="0" cellpadding="5" cellspacing="0" class="grey" width="100%">
							<tr>
								<td align="center">
									<font size="+1"><br>
									<br>
									<br>
								Thank you for your donations!
								<br>
								<br>
								<br>
								<br>
									</font></td>
							</tr>
						</table>
						</cfif>
					</td>
				</tr>
				<tr>
					<td class="linedrow"><b>Items currently being donated:</b> </td>
				</tr>
				
				<tr>
					<td class="nopadding">
						<table width="100%" border="0" cellpadding="5" cellspacing="0" class="grey">
							<cfif getdonations.recordcount gt 0>
							<cfoutput query="getdonations">
							<tr>
								<td width="10%" valign="top" nowrap class="linedrow">#name# from #department#:</td>
								<td valign="top" class="linedrow">
								#replace(donating, chr(13), "<br>", "all")#
								</td>
							</tr>
							</cfoutput>
							<cfelse>
							<tr>
								<td>Nobody has listed anything yet, you can be the first!</td>
							</tr>
							</cfif>
						</table>
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<br>
<a style="text-decoration:underline" href="/index.cfm">Return to Intranet Homepage</a>
</div>
