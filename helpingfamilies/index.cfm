
<link rel="stylesheet" type="text/css" href="../styles.css">

<div align="center">
<table width="500" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center"><img src="bells.gif" width="250" height="213" /></td>
	</tr>
	<tr>
		<td>
			<p><b>This year COPS Monitoring will be sponsoring a family  from Williamstown. <br>
				<br>
				If you like to contribute please see Danielle Buce.  <br>
				<br>
				They have 5 children and their wish list is the following:</b></p>
			<p><b>Jimmy Jr. (Age 18)  Clothes and boots<br>
				Shoe size 11, shirt size Large, Jeans 34-32 size <br>
				Jessica (Age 15) Clothes<br>
				Size 12 or Large in Juniors<br>
				Ashley (Age 14) Clothes<br>
				Size 8 or Large in Juniors<br>
				Amanda (Age 10) Clothes, makeup and girl toys (barbies, bratz,etc)<br>
				Size 14-16 or Large in Girls<br>
				Brianna (Age 8) Clothes, makeup and girl toys (barbies, bratz,etc)<br>
				Size 7-8 or Medium in Girls</b></p>
			<p><b>We will be taking cash donations for extra toys and a Shop Rite gift card.  I will also be collecting nonperishable food donations for a food basket. Please make all contributions by Friday, December 21.</b></p>
			<p><b>Hopefully we can make this family&rsquo;s holiday a special one!!!</b><br>
				<br>
				<b>Please <a style="text-decoration:underline" href="inputform.cfm">click here</a> to see what others are donating, and fill out the form to list what you will be donating. </b></p>
		</td>
	</tr>
</table>
</div>
