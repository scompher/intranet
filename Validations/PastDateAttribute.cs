﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

/// <summary>
/// Validates that the date is not in the past
/// </summary>
namespace Intranet.Validations
{
    public class PastDateAttribute : ValidationAttribute
    {

        public override bool IsValid(object value)
        {
            DateTime d = Convert.ToDateTime(value);
            return d > DateTime.Now;

        }
    }
}