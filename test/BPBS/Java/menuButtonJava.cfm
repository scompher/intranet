<script type="text/javascript">
    $(document).ready(function() {	
    	
		/* CUSTOM BUTTON ANIMATION IN */
        $("img").mouseenter(function(event) {
			var eventID = event.target.id
			if (eventID == "homeButton") {
            	$("#homeButton").attr("src","/test/BPBS/images/btnMPower2.png")
			}
			if (eventID == "logOutButton") {
            	$("#logOutButton").attr("src","/test/BPBS/images/btnLogOut2.png")
			}
        });
        
	    /* CUSTOM BUTTON ANIMATION OUT */
        $("img").mouseleave(function(event) {
			var eventID = event.target.id
			if (eventID == "homeButton") {
            	$("#homeButton").attr("src","/test/BPBS/images/btnMPower1.png")
			}
			if (eventID == "logOutButton") {
            	$("#logOutButton").attr("src","/test/BPBS/images/btnLogOut1.png")
			}
        });
    });   
</script>