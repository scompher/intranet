<script type="text/javascript">
	$(document).ready(function () {
    		
    		var reportID = $('#idsList').val().split(':');
        	var frequency = $('#frqList').val().split(':');
        	var day = $('#dayList').val().split(':');
        	var dealers = $('#dlrList').val().split(':');
        	var emails = $('#emlList').val().split(':');
            
        	var z = 0
        	var data = [];
			var maxID = reportID.length;
        	for (var x = 0; x < maxID; x++) {
        		var row = {};
            	row["hiddenID"] = reportID[x];
            	row["reportID"] = reportID[x];
            	row["frequency"] = frequency[x];
            	row["day"] = day[x];
           		var theseDlr = dealers[x].split(',');
            	var theseEml = emails[x].split(',');
				var maxDlr = theseDlr.length;
				var maxEml = theseEml.length;
				var maxCnt = maxDlr;
				if (maxEml > maxCnt) { maxCnt = maxEml };
				for (var y = 0; y < maxCnt; y++) {
					if (y > 0) { 
               			var row = {}
            			row["hiddenID"] = reportID[x];
                		row['reportID'] = '' 
                		row["frequency"] = ''
                		row["day"] = ''
               		};
					if (y+1 <= maxDlr) { row['dealers'] = theseDlr[y] } else { row['dealers'] = '' };
					if (y+1 <= maxEml) { row['emails'] = theseEml[y] } else { row['emails'] = '' };
					data[z] = row;
					z++
				};
        	};
            
       		var source = {
        		localdata: data,
        		datatype: "array"
        	};
            
        	var dataAdapter = new $.jqx.dataAdapter(source, {
        		loadComplete: function (data) { },
        		loadError: function (xhr, status, error) { }      
        	});
           
        	$("#reportTable").jqxGrid( {
        		source: dataAdapter,
        		width: 810,
        		pageable: true,
        		pagesize: 15,
        		autoheight: true,
        		selectionmode: 'multiplerows',
           	 	theme: 'energyblue',
            	columns: [
            		{ text: 'hiddenID', datafield: 'hiddenID', hidden: true },
            		{ text: 'ID', datafield: 'reportID', width: 40, cellsalign: 'right' },
            		{ text: 'Frequency', datafield: 'frequency', width: 80, cellsalign: 'right' },
            		{ text: 'Day', datafield: 'day', width: 110, cellsalign: 'right' },
           			{ text: 'Dealers', datafield: 'dealers', width: 60, cellsalign: 'right' },
                	{ text: 'E-mails', datafield: 'emails', width: 520, cellsalign: 'left' }
           		]
    		});
    		
    		
    		$("#reportTable").on('rowdoubleclick', function(event) {
    			$("#dlrBoxEdit").jqxListBox('clear');
    			$("#emlBoxEdit").jqxListBox('clear');
    			var rowIndex = event.args.rowindex;
     			var edID = $('#reportTable').jqxGrid('getcellvalue', rowIndex, 'hiddenID');
				var edFreq = $('#reportTable').jqxGrid('getcellvalue', rowIndex, 'frequency');
				var edDay = $('#reportTable').jqxGrid('getcellvalue', rowIndex, 'day');
				var edDlr = '';
				var edEml = '';
				var rows = $('#reportTable').jqxGrid('getrows');
				var rLen = rows.length;
				for (i = 0, h = 0; i < rLen; i++) {
					var thisID = $('#reportTable').jqxGrid('getcellvalue', i, 'hiddenID'); 
					if (thisID == edID) { 
						var currDlr = $('#reportTable').jqxGrid('getcellvalue', i, 'dealers');
						if (h > 0 && currDlr != '' ) {edDlr += ','};
						edDlr += currDlr;
						var currEml = $('#reportTable').jqxGrid('getcellvalue', i, 'emails');
						if (h > 0 && currEml != '' ) {edEml += ','};
						edEml += currEml;
						h++;
					};
    			};
    			edDlr = edDlr.split(",");
    			edEml = edEml.split(",");
     			$('#dntTabs').jqxTabs('enableAt', 2);
     			$('#dntTabs').jqxTabs('select', 2);
     			$("#editID").val(edID) 
				var dLen = edDlr.length;
     			for (i = 0; i < dLen; i++) { 
     				var dValue = edDlr[i];
					$("#dlrBoxEdit").jqxListBox('insertAt', dValue, -1 ); 
				};
				var eLen = edEml.length;
     			for (i = 0; i < eLen; i++) { 
     				var eValue = edEml[i];
					$("#emlBoxEdit").jqxListBox('insertAt', eValue, -1 ); 
				};
				$('#dealerEdit').jqxInput('focus');
    		});
    		
    		$("#clearButton").jqxButton({ width: '150', height: '25'});
    		
	});
</script>