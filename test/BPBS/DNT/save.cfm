<cfoutput>
	<cfset dealerList='#form.dealerList#'>
	<cfif dealerList is not ''>
		<cfif form.frequency EQ 'Daily'>
			<cfset form.day=''>
		</cfif>
		<cfquery name="saveMain" datasource="#ds#">
			insert into dntReport_Main ( frequency, day, active, actDate )
			values ( '#form.frequency#', '#form.day#', 1, '#form.actDate#' )
		</cfquery> 
					
		<cfquery name="getReportID" datasource="#ds#">
			select max(id)
			as id
			from dntReport_Main
		</cfquery> 

		<cfset currentID='#getReportID.id#'>

		<cfloop list='#form.dealerList#' index='currentDealer'>					
			<cfquery name="saveDealer" datasource="#ds#">
				insert into dntReport_Dealer ( reportID, dealer )
				values ( #currentID#, '#currentDealer#' )
			</cfquery> 
		</cfloop>

		<cfloop list='#form.emailList#' index='currentEmail'>					
			<cfquery name="saveEmail" datasource="#ds#">
				insert into dntReport_Email ( reportID, email )
				values ( #currentID#, '#currentEmail#' )
			</cfquery> 
		</cfloop>
	</cfif>
</cfoutput>
<cflocation url="index.cfm" >