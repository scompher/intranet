<cfinclude template="/test/BPBS/Java/dntEditJava.cfm">
<cfparam name='dealerListEdit' default=''>
<cfparam name='emailListEdit' default=''>
<cfparam name='frequencyEdit' default=''>
<cfparam name='dayEdit' default=''>
<cfparam name='actDateEdit' default=''>
<cfparam name='activeDateEdit' default=''>
<cfparam name='editID' default=''>
<cfset activeDateEdit=dateFormat(now(),"mm/dd/yyyy")>
<cfoutput>
<br><br>

<table width=90% align='center'>
	<tr><td>
		<p>-Enter new dealer number or e-mail in the appropriate field.</p>
		<p>-Click on one or more Dealer/E-mail and use [Backspace] or [Delete] to remove them from the list. (You can also remove a Dealer/E-mail by re-entering it into the appropriate field)</p>
		<p>-Reports can be set to occur Daily(default), Weekly or Monthly. (If report runs Weekly or Monthly, make sure to set the day of the week for the report to run on. (Sunday by default))</p>
		<p>-Reports are set to be 'active' the day they are created, but this can be changed by clicking on the Date field and entering or choosing a date from the calender.
	</td></tr>
</table>
<br><br>

<table width=90% align='center'>
	<tr>
		<td width='15%'><b>Dealer ##: </b></td>
		<td width='15%'><b>E-Mail: </b></td>
		<td></td>
		<td></td>
	</tr>
	
	<tr>
		<td><input id='dealerEdit' type='text'></td>
		<td colspan='3'><input id='emailEdit' type='text'></td>
    </tr>
    
	<form name="editForm" method="post" action="editSave.cfm">
    	<tr>
    		<td><div id='dlrBoxEdit' name='dlrBoxEdit'></div></td>
    		<td colspan='3'><div id='emlBoxEdit' name='emlBoxEdit'></div></td>
    		<input type='hidden' name='editID' id='editID' value='#editID#'>
    		<input type='hidden' name='dealerListEdit' id='dealerListEdit' value='#dealerListEdit#'>
    		<input type='hidden' name='emailListEdit' id='emailListEdit' value='#emailListEdit#'>
		</tr>
	
		<tr><td colspan='3'><br></td></tr>
		
		<tr>
    		<td><div id='freqDropEdit'></div></td>
			<td><div id='dayDropEdit'></div></td>
			<td><div align='right'><input id='activeDateEdit' type='text' value='#activeDateEdit#'></div></td>
			<td align='right'><input id='submitEdit' type='submit' value='Submit'></td>
    		<input type='hidden' name='frequencyEdit' id='frequencyEdit' value='#frequencyEdit#'>
    		<input type='hidden' name='dayEdit' id='dayEdit' value='#dayEdit#'>
    		<input type='hidden' name='actDateEdit' id='actDateEdit' value='#actDateEdit#'>
		</tr>
		
		<tr>
			<td align='right' colspan='4'><div id='errorContentEdit' text='FF0000'><b id='errorEdit'></b></div></td>
		</tr>
    </form>
</table>
</cfoutput>