<cfoutput>
	<cfset dealerListEdit='#dealerListEdit#'>
	<cfif dealerListEdit is not ''>
		<cfif form.frequencyEdit EQ 'Daily'>
			<cfset form.dayEdit=''>
		</cfif>
		<cfquery name="saveMain" datasource="#ds#">
			update dntReport_Main 
			set frequency = '#form.frequencyEdit#', day = '#form.dayEdit#', active = 1, actDate = '#form.actDateEdit#' 
			where id = #editID#
		</cfquery> 
		
		<cfquery name='deleteDealer' datasource='#ds#'>
			delete from dntReport_Dealer
			where reportID = #editID#
		</cfquery>
		<cfloop list='#dealerListEdit#' index='currentDealer'>					
			<cfquery name="saveDealer" datasource="#ds#">
				insert into dntReport_Dealer ( reportID, dealer )
				values ( #editID#, '#currentDealer#' )
			</cfquery> 
		</cfloop>
		
		<cfquery name='deleteEmail' datasource='#ds#'>
			delete from dntReport_Email
			where reportID = #editID#
		</cfquery>
		<cfloop list='#emailListEdit#' index='currentEmail'>					
			<cfquery name="saveEmail" datasource="#ds#">
				insert into dntReport_Email ( reportID, email )
				values ( #editID#, '#currentEmail#' )
			</cfquery> 
		</cfloop>
	</cfif>
</cfoutput>
<cflocation url="index.cfm" >