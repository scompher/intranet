<cfinclude template="/test/BPBS/Java/dntViewJava.cfm"><cfquery name='getReports' dataSource='#ds#'>
	select *
	from dntReport_Main
	where active = 1
</cfquery>

<cfset currentIDs=[]>
<cfset currentFreqs=[]>
<cfset currentDays=[]>
<cfset currentDealers=[]>
<cfset currentEmails=[]>

<cfset idCnt=0>
<cfloop query='getReports'>
	<cfset idCnt+=1>
	<cfset currentIDs[#idCnt#]='#getReports.ID#'>
	<cfset currentFreqs[#idCnt#]='#getReports.Frequency#'>
	<cfset currentDays[#idCnt#]='#getReports.Day#'>
	
	<cfquery name='getDealers' dataSource='#ds#'>
		select *
		from dntReport_Dealer
		where reportID = #getReports.id#
	</cfquery>
	<cfset dlrCnt=0>
	<cfset currentDealers[#idCnt#]=''>	
	<cfloop query='getDealers'>
		<cfset dlrCnt+=1>
		<cfif dlrCnt NEQ 1><cfset currentDealers[#idCnt#]&=','></cfif>
		<cfset currentDealers[#idCnt#]&='#getDealers.dealer#'>	
	</cfloop>
	
	<cfquery name='getEmails' dataSource='#ds#'>
		select *
		from dntReport_Email
		where reportID = #getReports.id#
	</cfquery>
	<cfset emlCnt=0>
	<cfset currentEmails[#idCnt#]=''>
	<cfloop query='getEmails'>
		<cfset emlCnt+=1>
		<cfif emlCnt NEQ 1><cfset currentEmails[#idCnt#]&=','></cfif>
		<cfset currentEmails[#idCnt#]&='#getEmails.email#'>
	</cfloop>
</cfloop>

<cfscript>
	idsList=ArrayToList(currentIDs,':');
	frqList=ArrayToList(currentFreqs,':');
	dayList=ArrayToList(currentDays,':');
	dlrList=ArrayToList(currentDealers,':');
	emlList=ArrayToList(currentEmails,':');
</cfscript>

<cfoutput>
	<form name='hiddenForm' id='hiddenForm' method='post'>
    	<input type='hidden' name='idsList' id='idsList' value='#idsList#'>
    	<input type='hidden' name='frqList' id='frqList' value='#frqList#'>
    	<input type='hidden' name='dayList' id='dayList' value='#dayList#'>
    	<input type='hidden' name='dlrList' id='dlrList' value='#dlrList#'>
    	<input type='hidden' name='emlList' id='emlList' value='#emlList#'>
	</form>
	
	<br><br>
	<table width=90% align='center'><tr>
		<td><b id='test'>Viewing all reports....</b></td>
		<td align='right'><a href='clear.cfm'><input type='button' name='clearButton' id='clearButton' value='!!!Clear All Records!!!'></a></td>
	</tr></table>
	
	<br>
	<table width=90% align='center'><tr><td><div id="reportTable"></div></td></tr></table>
</cfoutput>