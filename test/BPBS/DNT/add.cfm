<cfinclude template="/test/BPBS/Java/dntAddJava.cfm">
<cfparam name='dealerList' default=''>
<cfparam name='emailList' default=''>
<cfparam name='frequency' default=''>
<cfparam name='day' default=''>
<cfparam name='actDate' default=''>
<cfparam name='activeDate' default=''>
<cfset activeDate=dateFormat(now(),"mm/dd/yyyy")>
<cfoutput>
<br><br>

<table width=90% align='center'>
	<tr><td>
		<p>-Enter new dealer number or e-mail in the appropriate field.</p>
		<p>-Click on one or more Dealer/E-mail and use [Backspace] or [Delete] to remove them from the list. (You can also remove a Dealer/E-mail by re-entering it into the appropriate field)</p>
		<p>-Reports can be set to occur Daily(default), Weekly or Monthly. (If report runs Weekly or Monthly, make sure to set the day of the week for the report to run on. (Sunday by default))</p>
		<p>-Reports are set to be 'active' the day they are created, but this can be changed by clicking on the Date field and entering or choosing a date from the calender.
	</td></tr>
</table>
<br><br>

<table width=90% align='center'>
	<tr>
		<td width='15%'><b>Dealer ##: </b></td>
		<td width='15%'><b>E-Mail: </b></td>
		<td></td>
		<td></td>
	</tr>
	
	<tr>
		<td><input id='dealer' type='text'></td>
		<td colspan='3'><input id='email' type='text'></td>
    </tr>
    
	<form name="inputForm" method="post" action="save.cfm">
    	<tr>
    		<td><div id='dlrBox' name='dlrBox'></div></td>
    		<td colspan='3'><div id='emlBox' name='emlBox'></div></td>
    		<input type='hidden' name='dealerList' id='dealerList' value='#dealerList#'>
    		<input type='hidden' name='emailList' id='emailList' value='#emailList#'>
		</tr>
	
		<tr><td colspan='3'><br></td></tr>
		
		<tr>
    		<td><div id='freqDrop'></div></td>
			<td><div id='dayDrop'></div></td>
			<td><div align='right'><input id='activeDate' type='text' value='#activeDate#'></div></td>
			<td align='right'><input id='submit' type='submit' value='Submit'></td>
    		<input type='hidden' name='frequency' id='frequency' value='#frequency#'>
    		<input type='hidden' name='day' id='day' value='#day#'>
    		<input type='hidden' name='actDate' id='actDate' value='#actDate#'>
		</tr>
		
		<tr>
			<td align='right' colspan='4'><div id='errorContent' text='FF0000'><b id='error'></b></div></td>
		</tr>
    </form>
</table>
</cfoutput>