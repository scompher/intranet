<cfset today = dateformat(Now(),'mm/dd/yyyy')>
<cfset serviceAddTime = createodbcdatetime(now())>
<cfset starttime = Now()>
<cfset outputFilePath = "#request.DirectPath#\test\steve\telguard">
<cfsetting requesttimeout="30000">

<!--- delete old files --->
<cffile action="delete" file="#outputFilePath#/telguardmissing.csv">
<cffile action="delete" file="#outputFilePath#/telguardserviceadded.csv">
<cffile action="delete" file="#outputFilePath#/telguardnotonbillcancelled.csv">
<cffile action="delete" file="#outputFilePath#/telguardnotonbill.csv">
<cffile action="delete" file="#outputFilePath#/telguardonbillcancelleducc.csv">

<!--- create headers --->
<cffile action="append" file="#outputFilePath#/telguardmissing.csv"
output='Account Number On Bill-Missing from UCC,Account Name,OOS category,UCC Dealer Number, Dealer Name,Bill Code Desc,Bill Code,Charge'
addnewline="yes">
<cffile action="append" file="#outputFilePath#/telguardserviceadded.csv"
output='Account Number on Bill-Service Added to UCC,Account Name,OOS category,UCC Dealer Number, Dealer Name,Bill Code Desc,Bill Code,Charge'
addnewline="yes">
<cffile action="append" file="#outputFilePath#/telguardnotonbillcancelled.csv"
output='Canceled UCC Account Number-Not on bill,Account Name,OOS category,UCC Dealer Number, Dealer Name,Bill Code Desc,Bill Code,Charge'
addnewline="yes">
<cffile action="append" file="#outputFilePath#/telguardnotonbill.csv"
output='UCC account number missing from bill,Account Name,OOS category,UCC Dealer Number,Dealer Name,Bill Code Desc,Bill Code,Charge'
addnewline="yes">
<cffile action="append" file="#outputFilePath#/telguardonbillcancelleducc.csv"
output='UCC account number on bill- cancelled in UCC,Account Name,OOS category,UCC Dealer Number,Dealer Name,Bill Code Desc,Bill Code,Charge'
addnewline="yes">

<cfoutput>
	<!--- build service code list--->
	<cfquery name='gettelguardServices' datasource="lydiabilling">
		select serviceCode
		from serviceConversion
		where vendor = 'Telguard'
	</cfquery>
	<cfset serviceList = quotedValueList(gettelguardServices.serviceCode)>
	
	<!--- grab the bill --->
	<cfquery name="getBill" datasource="lydiabilling">
		select * from telguard
		join serviceconversion
		on telguard.billcode = serviceconversion.item
		and ignore is NULL
	</cfquery>


	<!--- this part takes the bill and makes sure the accounts are at UCC --->
	<cfloop query="getBill">
		<cfquery name="getAccountInfo" datasource="lydiabilling">
			select 
			stages.site.SiteNum 'siteNum',
			stages.site.SiteName 'siteName',
			stages.Device.OOSStartDate 'oosDate',
			stages.Transmitter.TransmitterCode 'accountNum',
			stages.SiteContractItem.ContractItem 'serviceCode',
			stages.SiteGroupLink.SiteGroupNum 'dealerNum',
			stages.SiteGroup.name 'dealerName',
			stages.Device.DevNum 'devNum',
			stages.Device.OOSCat
			from stages.SiteContractItem
			join stages.device
			on stages.device.devnum = stages.sitecontractitem.devnum
			join stages.transmitter
			on stages.device.devnum = stages.transmitter.devnum
			join stages.site
			on stages.sitecontractitem.sitenum = stages.site.sitenum
			join stages.SiteGroupLink
			on stages.site.SiteNum = stages.sitegrouplink.SiteNum
			join stages.SiteGroup
			on stages.SiteGroup.SiteGroupNum = stages.SiteGroupLink.SiteGroupNum
			where 
			stages.Transmitter.TransmitterCode = '#getBill.automationID#'
		</cfquery>
		<cfif getAccountInfo.recordCount is 0 and trim(getBill.automationID) is not ''>
			<cffile action="append" file="#outputFilePath#/telguardmissing.csv"
			output='#getBill.automationID#,#getBill.SubscriberName#,N/A,N/A,N/A,#getBill.RatePlanName#,#getBill.serviceCode#,#getBill.MonthlyCharge#'
			addnewline="yes">
		</cfif>
		<cfif getAccountInfo.recordCount gt 0>
			<cfquery name="doesAccountHaveService" dbtype="query">
				select *
				from getAccountInfo
				where 
				serviceCode = '#getBill.serviceCode#'
			</cfquery>
			<cfif doesAccountHaveService.recordCount is 0>
				<cfif getAccountInfo.oosDate is ''>
					<cfquery name="addService" datasource="lydiabilling">
					insert into stages.sitecontractitem
					values ('#getAccountInfo.siteNum#','#getAccountInfo.devNum#','#getBill.serviceCode#',#serviceAddTime#)
					</cfquery>
					<cffile action="append" file="#outputFilePath#/telguardserviceadded.csv"
					output='#getAccountInfo.accountNum#,#getAccountInfo.siteName#,Active,#getAccountInfo.dealerNum#,#getAccountInfo.dealerName#,#getBill.RatePlanName#,#getBill.serviceCode#,#getBill.MonthlyCharge#'
					addnewline="yes">
				<cfelse>
					<cffile action="append" file="#outputFilePath#/telguardonbillcancelleducc.csv"
					output='#getAccountInfo.accountNum#,#getAccountInfo.siteName#,#getAccountInfo.oosCat# - #getAccountInfo.oosDate#,#getAccountInfo.dealerNum#,#getAccountInfo.dealerName#,#getBill.RatePlanName#,#getBill.serviceCode#,#getBill.MonthlyCharge#'
					addnewline="yes">
				</cfif>
			</cfif>
		</cfif>
	</cfloop>

	<!--- get accounts with a telular service code--->
	<cfquery name="getAccounts" datasource="lydiaBilling">
		select
		stages.site.SiteNum 'siteNum',
		stages.site.SiteName 'siteName',
		stages.Device.OOSStartDate 'oosDate',
		stages.Transmitter.TransmitterCode 'accountNum',
		stages.SiteContractItem.ContractItem 'serviceCode',
		stages.SiteGroupLink.SiteGroupNum 'dealerNum',
		stages.SiteGroup.name 'dealerName',
		stages.Device.DevNum 'devNum',
		stages.Device.OOSCat
		from stages.SiteContractItem
		join stages.device
		on stages.device.devnum = stages.sitecontractitem.devnum
		join stages.transmitter
		on stages.device.devnum = stages.transmitter.devnum
		join stages.site
		on stages.sitecontractitem.sitenum = stages.site.sitenum
		join stages.SiteGroupLink
		on stages.site.SiteNum = stages.sitegrouplink.SiteNum
		join stages.SiteGroup
		on stages.SiteGroup.SiteGroupNum = stages.SiteGroupLink.SiteGroupNum
		where contractitem in (#preservesinglequotes(serviceList)#)
	</cfquery>

	<!--- This part takes all accounts on UCC and checks to see if they are on the bill --->
	<cfloop query="getAccounts">
		<cfquery name="checkBill" dbtype="query">
			select *
			from getBill
			where automationid = '#getAccounts.accountNum#'
		</cfquery>
		<cfif checkBill.recordcount is 0>
			<cfif trim(getAccounts.oosDate) is not ''>
				<cffile action="append" file="#outputFilePath#/telguardnotonbillcancelled.csv"
					output='#getAccounts.accountNum#,#getAccounts.siteName#,#getAccounts.oosCat# - #getAccounts.oosDate#,#getAccounts.dealerNum#,#getAccounts.dealerName#,N/A,N/A,N/A'
					addnewline="yes">
			<cfelse>
				<cffile action="append" file="#outputFilePath#/telguardnotonbill.csv"
					output='#getAccounts.accountNum#,#getAccounts.siteName#,Active,#getAccounts.dealerNum#,#getAccounts.dealerName#,N/A,#getAccounts.serviceCode#,N/A,N/A'
					addnewline="yes">
			</cfif>
			<cfquery name="removeService" datasource="lydiabilling">
				delete
				from stages.sitecontractitem
				where siteNum = '#getAccounts.siteNum#'
				and contractItem = '#getAccounts.serviceCode#'
			</cfquery>
		</cfif>
	</cfloop>

	<!--- mail results to Kat--->
	<cfmail from="reports@copsmonitoring.com" to="ktallman@copsmonitoring.com" subject="Telguard Audit"  username="copalink@copsmonitoring.com" password="copsmoncal">
		<cfmailparam file="#outputFilePath#/telguardnotonbill.csv">
		<cfmailparam file="#outputFilePath#/telguardnotonbillcancelled.csv">
		<cfmailparam file="#outputFilePath#/telguardserviceadded.csv">
		<cfmailparam file="#outputFilePath#/telguardmissing.csv">
		<cfmailparam file="#outputFilePath#/telguardonbillcancelleducc.csv">
	</cfmail>
		Audit is complete. Results have been emailed.
</cfoutput>
