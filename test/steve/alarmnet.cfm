<cfset today = dateformat(Now(),'mm/dd/yyyy')>
<cfset starttime = Now()>
<cfset outputFilePath = "#request.DirectPath#\test\steve\alarmnet"><br />
<cfset rightNow = createodbcdatetime(now())>
<cfsetting requesttimeout="30000">
<!---<cffile action="delete" file="#outputFilePath#/alarmnet.csv">--->
<cfoutput>
<cfquery name="checkServices" datasource="lydiabilling">
	select * from alarmnet
	left outer join serviceConversion
	on alarmnet.ItemNum = serviceConversion.item
	where serviceConversion.item is NULL
</cfquery>
<cfif checkServices.recordcount is not 0>
		<cfloop query="checkServices">
			<cffile action="append" file="#outputFilePath#/alarnetmissingservices.csv"
			output='#checkServices.svcDesc#'
			addnewline="yes">
		</cfloop>
		<cfabort>
</cfif>
	<cfquery name='getAlarmnetServices' datasource="lydiabilling">
		select serviceCode
		from serviceConversion
		where vendor = 'AlarmNet'
	</cfquery>
	<cfset serviceList = quotedValueList(getAlarmnetServices.serviceCode)>

	<cfquery name="getAccounts" datasource="lydiabilling">
		select *
		from stages.SiteContractItem
		join stages.device
		on stages.device.devnum = stages.sitecontractitem.devnum
		join stages.transmitter
		on stages.device.devnum = stages.transmitter.devnum
		join stages.site
		on stages.sitecontractitem.sitenum = stages.site.sitenum
		where contractitem in (#preservesinglequotes(serviceList)#)
	</cfquery>
	<cfquery name="getBill" datasource="lydiabilling">
		select * from alarmnet
		join serviceconversion
		on alarmnet.itemnum = serviceconversion.item
		where billed = 'Yes'
		and ignore is NULL
</cfquery>
<cfloop query="getBill">
	<cfquery name="getRow" dbtype="query">
	select *
	from getAccounts
	where transmittercode = '#trim(getBill.automationID)#'
	and contractitem = '#getbill.serviceCode#'
	</cfquery>
	<cfif getRow.recordCount is 0>
		<cfquery name="getCurrentAccount" datasource="lydiabilling">
			select *
			from stages.transmitter
			join stages.device
			on stages.transmitter.devnum = stages.device.devnum
			join stages.site
			on stages.device.sitenum = stages.site.sitenum
			where transmittercode = '#trim(getBill.automationID)#'
		</cfquery>
		<cfif getCurrentAccount.recordcount is 0>
			<cffile action="append" file="#outputFilePath#/alarmnetmissing.csv"
			output='#getBill.automationID#,#getBill.serviceCode#'
			addnewline="yes">
		<cfelse>
			<cfquery name="addService" datasource="lydiabilling">
				insert into stages.sitecontractitem
				values ('#getCurrentAccount.siteNum#','#getCurrentAccount.devNum#','#getBill.servicecode#',#rightNow#)
			</cfquery>
			<cffile action="append" file="#outputFilePath#/alarmnetserviceadded.csv"
			output='#getBill.automationID#,#getBill.serviceCode#'
			addnewline="yes">
		</cfif>
	</cfif>
</cfloop>
<cfloop query="getAccounts">
	<cfquery name="checkBill" dbtype="query">
		select *
		from getBill
		where automationid = '#getAccounts.transmittercode#'
	</cfquery>
	<cfif checkBill.recordcount is 0>
		<cfif trim(getAccounts.oosstartdate) is not ''>
			<cffile action="append" file="#outputFilePath#/alarmnetnotonbillcancelled.csv"
			output='#getAccounts.transmittercode#,#getAccounts.contractItem#'
			addnewline="yes">
		<cfelse>
			<cffile action="append" file="#outputFilePath#/alarmnetnotonbill.csv"
			output='#getAccounts.transmittercode#,#getAccounts.contractItem#'
			addnewline="yes">
		</cfif>
	</cfif>
</cfloop>
</cfoutput>