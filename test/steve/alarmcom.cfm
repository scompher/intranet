<cfset today = dateformat(Now(),'mm/dd/yyyy')>
<cfset serviceAddTime = createodbcdatetime(now())>
<cfset starttime = Now()>
<cfset outputFilePath = "#request.DirectPath#\test\steve\alarmcom">
<cfsetting requesttimeout="30000">

<!--- delete old files --->
<cffile action="delete" file="#outputFilePath#/alarmcommissing.csv">
<cffile action="delete" file="#outputFilePath#/alarmcomserviceadded.csv">
<cffile action="delete" file="#outputFilePath#/alarmcomnotonbillcancelled.csv">
<cffile action="delete" file="#outputFilePath#/alarmcomnotonbill.csv">
<cffile action="delete" file="#outputFilePath#/alarmcomonbillcancelleducc.csv">

<!--- create headers --->
<cffile action="append" file="#outputFilePath#/alarmcommissing.csv"
output='Account Number On Bill-Missing from UCC,Account Name,OOS category,UCC Dealer Number, Dealer Name,Bill Code Desc,Bill Code,Charge'
addnewline="yes">
<cffile action="append" file="#outputFilePath#/alarmcomserviceadded.csv"
output='Account Number on Bill-Service Added to UCC,Account Name,OOS category,UCC Dealer Number, Dealer Name,Bill Code Desc,Bill Code,Charge'
addnewline="yes">
<cffile action="append" file="#outputFilePath#/alarmcomnotonbillcancelled.csv"
output='Canceled UCC Account Number-Not on bill,Account Name,OOS category,UCC Dealer Number, Dealer Name,Bill Code Desc,Bill Code,Charge'
addnewline="yes">
<cffile action="append" file="#outputFilePath#/alarmcomnotonbill.csv"
output='UCC account number missing from bill,Account Name,OOS category,UCC Dealer Number,Dealer Name,Bill Code Desc,Bill Code,Charge'
addnewline="yes">
<cffile action="append" file="#outputFilePath#/alarmcomonbillcancelleducc.csv"
output='UCC account number on bill- cancelled in UCC,Account Name,OOS category,UCC Dealer Number,Dealer Name,Bill Code Desc,Bill Code,Charge'
addnewline="yes">

<cfoutput>
	<!--- build service code list--->
	<cfquery name='getalarmcomServices' datasource="lydiabilling">
		select serviceCode
		from serviceConversion
		where vendor = 'Alarm.com'
	</cfquery>
	<cfset serviceList = quotedValueList(getalarmcomServices.serviceCode)>
	
	<!--- grab the bill --->
	<cfquery name="getBill" datasource="lydiabilling">
		select * from alarmcom
		join serviceconversion
		on alarmcom.ratePlanName = serviceconversion.item
		where ignore is NULL
		and chargeType != 'Activation'
		and chargeType != 'Service'
	</cfquery>


	<!--- this part takes the bill and makes sure the accounts are at UCC --->
	<cfloop query="getBill">
		<cfquery name="getAccountInfo" datasource="lydiabilling">
			select 
			stages.site.SiteNum 'siteNum',
			stages.site.SiteName 'siteName',
			stages.Device.OOSStartDate 'oosDate',
			stages.Transmitter.TransmitterCode 'accountNum',
			stages.SiteContractItem.ContractItem 'serviceCode',
			stages.SiteGroupLink.SiteGroupNum 'dealerNum',
			stages.SiteGroup.name 'dealerName',
			stages.Device.DevNum 'devNum',
			stages.Device.OOSCat
			from stages.SiteContractItem
			join stages.device
			on stages.device.devnum = stages.sitecontractitem.devnum
			join stages.transmitter
			on stages.device.devnum = stages.transmitter.devnum
			join stages.site
			on stages.sitecontractitem.sitenum = stages.site.sitenum
			join stages.SiteGroupLink
			on stages.site.SiteNum = stages.sitegrouplink.SiteNum
			join stages.SiteGroup
			on stages.SiteGroup.SiteGroupNum = stages.SiteGroupLink.SiteGroupNum
			where 
			stages.Transmitter.TransmitterCode = '#getBill.automationID#'
		</cfquery>
		<cfif getAccountInfo.recordCount is 0 and trim(getBill.automationID) is not ''>
			<cfset SubscriberName = '#getBill.firstname# #getBill.lastname#'>
			<cffile action="append" file="#outputFilePath#/alarmcommissing.csv"
			output='#getBill.automationID#,#SubscriberName#,N/A,N/A,N/A,#getBill.RatePlanName#,#getBill.serviceCode#,#getBill.MonthlyCharge#'
			addnewline="yes">
		</cfif>
		<cfif getAccountInfo.recordCount gt 0>
			<cfquery name="doesAccountHaveService" dbtype="query">
				select *
				from getAccountInfo
				where 
				serviceCode = '#getBill.serviceCode#'
			</cfquery>
			<cfif doesAccountHaveService.recordCount is 0>
				<cfif getAccountInfo.oosDate is ''>
					<cfquery name="addService" datasource="lydiabilling">
					insert into stages.sitecontractitem
					values ('#getAccountInfo.siteNum#','#getAccountInfo.devNum#','#getBill.serviceCode#',#serviceAddTime#)
					</cfquery>
					<cffile action="append" file="#outputFilePath#/alarmcomserviceadded.csv"
					output='#getAccountInfo.accountNum#,#replace(getAccountInfo.siteName,","," ","all")#,Active,#getAccountInfo.dealerNum#,#getAccountInfo.dealerName#,#getBill.RatePlanName#,#getBill.serviceCode#,#getBill.MonthlyCharge#'
					addnewline="yes">
				<cfelse>
					<cffile action="append" file="#outputFilePath#/alarmcomonbillcancelleducc.csv"
					output='#getAccountInfo.accountNum#,#replace(getAccountInfo.siteName,","," ","all")#,#getAccountInfo.oosCat# - #getAccountInfo.oosDate#,#getAccountInfo.dealerNum#,#getAccountInfo.dealerName#,#getBill.RatePlanName#,#getBill.serviceCode#,#getBill.MonthlyCharge#'
					addnewline="yes">
				</cfif>
			</cfif>
		</cfif>
	</cfloop>

	<!--- get accounts with a telular service code--->
	<cfquery name="getAccounts" datasource="lydiaBilling">
		select
		stages.site.SiteNum 'siteNum',
		stages.site.SiteName 'siteName',
		stages.Device.OOSStartDate 'oosDate',
		stages.Transmitter.TransmitterCode 'accountNum',
		stages.SiteContractItem.ContractItem 'serviceCode',
		stages.SiteGroupLink.SiteGroupNum 'dealerNum',
		stages.SiteGroup.name 'dealerName',
		stages.Device.DevNum 'devNum',
		stages.Device.OOSCat
		from stages.SiteContractItem
		join stages.device
		on stages.device.devnum = stages.sitecontractitem.devnum
		join stages.transmitter
		on stages.device.devnum = stages.transmitter.devnum
		join stages.site
		on stages.sitecontractitem.sitenum = stages.site.sitenum
		join stages.SiteGroupLink
		on stages.site.SiteNum = stages.sitegrouplink.SiteNum
		join stages.SiteGroup
		on stages.SiteGroup.SiteGroupNum = stages.SiteGroupLink.SiteGroupNum
		where contractitem in (#preservesinglequotes(serviceList)#)
	</cfquery>

	<!--- This part takes all accounts on UCC and checks to see if they are on the bill --->
	<cfloop query="getAccounts">
		<cfquery name="checkBill" dbtype="query">
			select *
			from getBill
			where automationid = '#getAccounts.accountNum#'
		</cfquery>
		<cfif checkBill.recordcount is 0>
			<cfif trim(getAccounts.oosDate) is not ''>
				<cffile action="append" file="#outputFilePath#/alarmcomnotonbillcancelled.csv"
					output='#getAccounts.accountNum#,#replace(getAccounts.siteName,","," ","all")#,#getAccounts.oosCat# - #getAccounts.oosDate#,#getAccounts.dealerNum#,#getAccounts.dealerName#,N/A,N/A,N/A'
					addnewline="yes">
			<cfelse>
				<cffile action="append" file="#outputFilePath#/alarmcomnotonbill.csv"
					output='#getAccounts.accountNum#,#replace(getAccounts.siteName,","," ","all")#,Active,#getAccounts.dealerNum#,#getAccounts.dealerName#,N/A,#getAccounts.serviceCode#,N/A,N/A'
					addnewline="yes">
			</cfif>
			<cfquery name="removeService" datasource="lydiabilling">
				delete
				from stages.sitecontractitem
				where siteNum = '#getAccounts.siteNum#'
				and contractItem = '#getAccounts.serviceCode#'
			</cfquery>
		</cfif>
	</cfloop>

	<!--- mail results to Kat--->
	<cfmail from="reports@copsmonitoring.com" to="ktallman@copsmonitoring.com" subject="alarmcom Audit"  username="copalink@copsmonitoring.com" password="copsmoncal">
		<cfmailparam file="#outputFilePath#/alarmcomnotonbill.csv">
		<cfmailparam file="#outputFilePath#/alarmcomnotonbillcancelled.csv">
		<cfmailparam file="#outputFilePath#/alarmcomserviceadded.csv">
		<cfmailparam file="#outputFilePath#/alarmcommissing.csv">
		<cfmailparam file="#outputFilePath#/alarmcomonbillcancelleducc.csv">
	</cfmail>
		Audit is complete. Results have been emailed.
</cfoutput>
