<cfset today = dateformat(Now(),'mm/dd/yyyy')>
<cfset starttime = Now()>
<cfset outputFilePath = "#request.DirectPath#\test\steve\uplink"><br />
<cfset rightNow = createodbcdatetime(now())>
<cfsetting requesttimeout="30000">
<!---<cffile action="delete" file="#outputFilePath#/uplink.csv">--->
<cfoutput>
	<cfquery name='getuplinkServices' datasource="lydiabilling">
		select serviceCode
		from serviceConversion
		where vendor = 'UpLink'
	</cfquery>
	<cfset serviceList = quotedValueList(getuplinkServices.serviceCode)>

	<cfquery name="getAccounts" datasource="lydiaBilling">
		select *
		from stages.SiteContractItem
		join stages.device
		on stages.device.devnum = stages.sitecontractitem.devnum
		join stages.transmitter
		on stages.device.devnum = stages.transmitter.devnum
		join stages.site
		on stages.sitecontractitem.sitenum = stages.site.sitenum
		where contractitem in (#preservesinglequotes(serviceList)#)
	</cfquery>

	<cfquery name="getBill" datasource="lydiabilling">
		select * from uplink
		join serviceconversion
		on uplink.serviceplan = serviceconversion.item
		and ignore is NULL
</cfquery>

<cfloop query="getBill">
	<cfquery name="getRow" dbtype="query">
	select *
	from getAccounts
	where transmittercode = '#getBill.automationID#'
	and contractitem = '#getbill.serviceCode#'
	</cfquery>
	<cfif getRow.recordCount is 0 and trim(getBill.automationID) is not ''>
		<cfquery name="getCurrentAccount" datasource="lydiabilling">
			select *
			from stages.transmitter
			join stages.device
			on stages.transmitter.devnum = stages.device.devnum
			join stages.site
			on stages.device.sitenum = stages.site.sitenum
			where transmittercode = '#trim(getBill.automationID)#'
		</cfquery>
		<cfif getCurrentAccount.recordcount is 0>
			<cffile action="append" file="#outputFilePath#/uplinkmissing.csv"
			output='#getBill.automationID#,#getBill.serviceCode#'
			addnewline="yes">
		<cfelse>
			<cfif get
			<cfquery name="addService" datasource="lydiabilling">
				insert into stages.sitecontractitem
				values ('#getCurrentAccount.siteNum#','#getCurrentAccount.devNum#','#getBill.servicecode#',#rightNow#)
			</cfquery>
			<cffile action="append" file="#outputFilePath#/uplinkserviceadded.csv"
			output='#getBill.automationID#,#getBill.serviceCode#'
			addnewline="yes">
		</cfif>
	</cfif>
</cfloop>
<cfloop query="getAccounts">
	<cfquery name="checkBill" dbtype="query">
		select *
		from getBill
		where automationid = '#getAccounts.transmittercode#'
	</cfquery>
	<cfif checkBill.recordcount is 0>
		<cfif trim(getAccounts.oosstartdate) is not ''>
			<cffile action="append" file="#outputFilePath#/uplinknotonbillcancelled.csv"
			output='#getAccounts.transmittercode#,#getAccounts.contractItem#'
			addnewline="yes">
		<cfelse>
			<cffile action="append" file="#outputFilePath#/uplinknotonbill.csv"
			output='#getAccounts.transmittercode#,#getAccounts.contractItem#'
			addnewline="yes">
		</cfif>
	</cfif>
</cfloop>
</cfoutput>