
<cfif isDefined("form.Submit")>
	<cfif trim(form.auditSheet) is not "">
		<cffile action="upload" filefield="auditSheet" destination="#request.directpath#\test\steve\" nameconflict="overwrite">
		<cfset fileName = CFFILE.serverfile>
		<cfset auditName = listGetAt(CFFILE.serverfile,1,'.')>
		<cfoutput>#auditName#
			<cfquery name="truncateTable" datasource="lydiabilling">
				truncate table #auditName#
			</cfquery>
			<cfspreadsheet action="read" src="#request.directpath#\test\steve\#fileName#" query="readSheet" headerrow="1">
				<cfset metadata = getMetadata(readSheet)>
				<cfset colList = "">
				<cfloop index="col" array="#metadata#">
					<cfset colList = listAppend(colList, col.name)>
				</cfloop>
			<cfloop query="readSheet" startrow="2">
			<cfset insertRow = ''>
				<cfloop index='c' list="#colList#"><br />
				<cfset rowcell = replace(readSheet[c][currentRow],',','','all')>
				<cfset rowcell = replace(rowcell,"'",'',"all")>
				<cfset insertRow = listAppend(insertRow,#rowcell#)>
				</cfloop>
				<cfset insertRow = replace(insertRow,",","','",'all')>
				<cfset insertRow = "'"&#insertRow#&"'">
				<cfif trim(insertRow) is not "''">
				<cfquery name="SaveRow" datasource="lydiabilling">
					insert into #auditname#
					values(#preservesinglequotes(insertRow)#)
				</cfquery>
				</cfif>
			</cfloop>
		</cfoutput>
	</cfif><br />
<cflocation url="#auditname#.cfm">
</cfif>

<link rel="stylesheet" type="text/css" href="../styles.css">
<div align="center">
<table width="400" border="0" cellspacing="0" cellpadding="5">
    <tr>
        <td class="highlightbar"><b>Upload Audit</b></td>
    </tr>
    <tr>
        <td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td><b>Upload audit:</b></td>
				</tr>
				<tr>
					<td>The name of the file has to be the name of the service being audited</td>
				</tr>
				<tr>
					<td>(alarmcom,alarmnet,uplink,c24,telguard).xlsx</td>
				</tr>
				<form method="post" action="upload.cfm" enctype="multipart/form-data">
				<tr>
				    <td>
				        <input type="file" name="auditSheet" style="width:300px; background-color:#FFFFFF;">
				    </td>
			    </tr>
				<tr>
				    <td>
				        <input name="Submit" type="submit" class="sidebar" value="Upload">
				    </td>
			    </tr>
				</form>
			</table>
		</td>
    </tr>
</table>
<br>
<a style="text-decoration:underline;" class="normal" href="/index.cfm">Return to Intranet</a>
</div>