<cfset today = dateformat(Now(),'mm/dd/yyyy')>
<cfset starttime = Now()>
<cfset outputFilePath = "#request.DirectPath#\test\steve\c24"><br />
<cfsetting requesttimeout="30000">
<!---<cffile action="delete" file="#outputFilePath#/c24.csv">--->
<cfoutput>
	<cfquery name='getc24Services' datasource="lydiabilling">
		select serviceCode
		from serviceConversion
		where vendor = 'Connect24'
	</cfquery>
	<cfset serviceList = quotedValueList(getc24Services.serviceCode)>

	<cfquery name="getAccounts" datasource="lydiaBilling">
		select *
		from stages.SiteContractItem
		join stages.device
		on stages.device.devnum = stages.sitecontractitem.devnum
		join stages.transmitter
		on stages.device.devnum = stages.transmitter.devnum
		join stages.site
		on stages.sitecontractitem.sitenum = stages.site.sitenum
		where contractitem in (#preservesinglequotes(serviceList)#)
	</cfquery>

	<cfquery name="getBill" datasource="lydiabilling">
		select * from c24
		join serviceconversion
		on c24.Description = serviceconversion.item
		and ignore is NULL
</cfquery>

<cfloop query="getBill">
	<cfquery name="getRow" dbtype="query">
	select *
	from getAccounts
	where transmittercode = '#getBill.automationID#'
	and contractitem = '#getbill.serviceCode#'
	</cfquery>
	<cfif getRow.recordCount is 0>
		<cfquery name="getCurrentAccount" datasource="lydiabilling">
			select *
			from stages.transmitter
			where transmittercode = '#getBill.automationID#'
		</cfquery>
		<cfif getCurrentAccount.recordcount is 0 and trim(getBill.automationID) is not ''>
			<cffile action="append" file="#outputFilePath#/c24missing.csv"
			output='#getBill.automationID#,#getBill.serviceCode#'
			addnewline="yes">
		<cfelse>
			<cffile action="append" file="#outputFilePath#/c24serviceadded.csv"
			output='#getBill.automationID#,#getBill.serviceCode#'
			addnewline="yes">
		</cfif>
	</cfif>
</cfloop>
<cfloop query="getAccounts">
	<cfquery name="checkBill" dbtype="query">
		select *
		from getBill
		where automationid = '#getAccounts.transmittercode#'
	</cfquery>
	<cfif checkBill.recordcount is 0>
		<cfif trim(getAccounts.oosstartdate) is not ''>
			<cffile action="append" file="#outputFilePath#/c24notonbillcancelled.csv"
			output='#getAccounts.transmittercode#,#getAccounts.contractItem#'
			addnewline="yes">
		<cfelse>
			<cffile action="append" file="#outputFilePath#/c24notonbill.csv"
			output='#getAccounts.transmittercode#,#getAccounts.contractItem#'
			addnewline="yes">
		</cfif>
	</cfif>
</cfloop>
</cfoutput>