
<link rel="stylesheet" type="text/css" href="/styles.css">

<cfparam name="rid" default="0">
<cfparam name="backpage" default="">
<cfparam name="urlString" default="">
<cfparam name="markCompleted" default="0">
<cfparam name="returnURL" default="">

<cfif trim(returnURL) is "">
	<cfif trim(cgi.HTTP_REFERER) is not "">
		<cfset returnURL = listgetat(cgi.HTTP_REFERER,listlen(cgi.HTTP_REFERER, "\"),"\")>
		<cfset returnURL = listgetat(returnURL,1,"?")>
		<cfset returnURL = returnURL & "?" & cgi.QUERY_STRING>
	<cfelse>
		<cfset returnURL = "##">
	</cfif>
</cfif> 

<cfif markCompleted is 1>

	<cfquery name="updatecompleted" datasource="#ds#">
		update requestTracking_requests 
		set completedDate = #createodbcdatetime(now())#, requestStatus = 3 
		where requestid = #rid# 
	</cfquery>
	
	<!--- insert into dealer diary --->
	<cfquery name="getRequest" datasource="#ds#">
		select requestTracking_requests.*, requestTracking_requestStatus.requestStatusName, requestTracking_requestTypes.requestTypeName, requestTracking_submittedTo.submittedToName 
		from requestTracking_requests 
		left join requestTracking_requestStatus on requestTracking_requests.requestStatus = requestTracking_requestStatus.requestStatusID 
		left join requestTracking_requestTypes on requestTracking_requests.requestType = requestTracking_requestTypes.requestTypeID 
		left join requestTracking_submittedTo on requestTracking_requests.submittedTo = requestTracking_submittedTo.submittedToID 
		where requestTracking_requests.requestID = #rid# 
	</cfquery>
	<cfquery name="getcomments" datasource="#ds#">
		select requestTracking_comments.*, admin_users.firstname + ' ' + admin_users.lastname as creator 
		from requestTracking_comments 
		left join admin_users on requestTracking_comments.adminuserid = admin_users.adminuserid 
		where requestid = #rid# 
		order by commentDateTime asc 
	</cfquery>
	<cfset subject = "Request: #getRequest.requestTypeName#">
	<cfset noteText = "<a href=""javascript:viewRequestDetails('/formTracker/details.cfm?rid=#rid#');"">View Request Details</a>#chr(13)##chr(13)#Requested By: #getRequest.requestedBy##chr(13)#Requestor Name: #getRequest.requestorName##chr(13)#Completed Date: #dateformat(getRequest.completedDate,'mm/dd/yyyy')##chr(13)#">
	<cfset noteText = noteText & "#chr(13)# Comments: " & chr(13)>
	<cfloop query="getcomments">
		<cfset noteText = noteText & "#dateformat(commentDateTime, 'mm/dd/yyyy')# #timeformat(commentDateTime, 'hh:mm tt')# by #creator# #chr(13)##replace(comment, chr(13), "<br />", "all")# #chr(13)# #chr(13)#">
	</cfloop>
	<cfquery name="insertIntoDiary" datasource="#ds#">
		insert into dealerdiary_entries (creatorID, dealernumber, created, content, subject, departmentid, parentid, inquiryNote)
		values (#cookie.adminlogin#, '#getRequest.dealerNumber#', #createodbcdatetime(getRequest.completedDate)#, '#noteText#', '#subject#', #getsec.defaultdeptid#, 0, 0)
	</cfquery>

	<cflocation url="#returnURL#">
</cfif>

<cfif isDefined("form.btnUpdateComment")>
	<cfquery name="insertComment" datasource="#ds#">
		update requestTracking_comments 
		set comment = '#comment#' 
		where commentid = #cid# 
	</cfquery>
</cfif>

<cfif isDefined("form.btnSaveComment")>
	<cfquery name="insertComment" datasource="#ds#">
		if not exists (select commentid from requestTracking_comments where requestid = #rid# and comment like '#comment#' and adminuserid = #cookie.adminlogin#)
		begin
			insert into requestTracking_comments (requestid, comment, commentDateTime, adminuserid) 
			values (#rid#, '#comment#', #createOdbcDateTime(now())#, #cookie.adminlogin#)
		end
	</cfquery>
</cfif>

<cfquery name="getRequest" datasource="#ds#">
	select requestTracking_requests.*, requestTracking_requestStatus.requestStatusName, requestTracking_requestTypes.requestTypeName, requestTracking_submittedTo.submittedToName 
	from requestTracking_requests 
	left join requestTracking_requestStatus on requestTracking_requests.requestStatus = requestTracking_requestStatus.requestStatusID 
	left join requestTracking_requestTypes on requestTracking_requests.requestType = requestTracking_requestTypes.requestTypeID 
	left join requestTracking_submittedTo on requestTracking_requests.submittedTo = requestTracking_submittedTo.submittedToID 
	where requestTracking_requests.requestID = #rid# 
</cfquery>

<cfquery name="getcomments" datasource="#ds#">
	select requestTracking_comments.*, admin_users.firstname + ' ' + admin_users.lastname as creator 
	from requestTracking_comments 
	left join admin_users on requestTracking_comments.adminuserid = admin_users.adminuserid 
	where requestid = #rid# 
	order by commentDateTime asc 
</cfquery>

<div align="center">
<table width="750" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Request Details</b> </td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
		<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<cfoutput query="getRequest">
			<tr>
				<td nowrap><b>Status:</b></td>
				<td>
				#getRequest.requestStatusName# 
				<cfif requestStatus is 3>on #dateformat(getRequest.completedDate,'mm/dd/yyyy')#</cfif>
				</td>
			</tr>
			<cfif getRequest.requestStatus is not 3>
				<form method="post" action="details.cfm">
				<input type="hidden" name="rid" value="<cfoutput>#rid#</cfoutput>" />
				<input type="hidden" name="returnURL" value="<cfoutput>#returnURL#</cfoutput>" />
				<tr>
					<td colspan="2">
					<input type="checkbox" name="markCompleted" value="1" style="vertical-align:middle" onclick="if (confirm('Are you sure you wish to mark this item as completed?')) {this.form.submit();} else {this.form.markCompleted.checked = false;}" />
					<b>Mark this item as completed</b>
					</td>
				</tr>
				</form>
			</cfif>
			<tr>
				<td width="19%" nowrap><b>Created By: </b></td>
				<td width="81%"><cfoutput>#createdBy#</cfoutput></td>
			</tr>
			<tr>
				<td nowrap><b>Requested By: </b></td>
				<td>
				#requestedBy#</td>
			</tr>
			<tr>
				<td nowrap><b>Requestor Name: </b></td>
				<td>#requestorName#</td>
			</tr>
			<tr>
				<td nowrap><b>Dealer Number: </b></td>
				<td>
				#dealerNumber#</td>
			</tr>
			<tr>
				<td nowrap><b>Dealer Name: </b></td>
				<td>#dealerName#</td>
			</tr>
			<tr>
				<td nowrap><b>Contact:</b></td>
				<td>#contactPerson#</td>
			</tr>
			<tr>
				<td nowrap><b>Date:</b></td>
				<td>
				#dateformat(requestDate,'mm/dd/yyyy')#</td>
			</tr>
			<tr>
				<td nowrap><b>Request:</b></td>
				<td>#requestTypeName#</td>
			</tr>
			<tr>
				<td nowrap valign="top"><b>Comments:</b></td>
				<td class="nopadding">
					<table border="0" cellspacing="0" cellpadding="3">
						<cfloop query="getComments">
						<tr>
							<td <cfif getcomments.currentrow is 1>class="linedtop"</cfif> nowrap="nowrap" valign="top">
							#dateformat(commentDateTime, 'mm/dd/yyyy')# #timeformat(commentDateTime, 'hh:mm tt')# by #creator#
							</td>
						</tr>
						<tr>
							<td class="linedrow" valign="top">
							#replace(comment, chr(13), "<br />", "all")#
							<cfif (cookie.adminlogin is getcomments.adminuserid) and (getRequest.requestStatus is not 3)>
							<br />
							<a style="text-decoration:underline;" href="details.cfm?editcomment=true&cid=#commentid#&rid=#rid#&returnURL=#urlencodedformat(returnURL)#">[edit this comment]</a>
							</cfif>
							</td>
						</tr>
						</cfloop>
					</table>
				</td>
			</tr>
			<tr>
				<td nowrap><b>Submitted To: </b></td>
				<td>
				#submittedToName#</td>
			</tr>
			<tr>
				<td nowrap><b>Name/Vendor:</b></td>
				<td>#nameOrVendor#</td>
			</tr>
			</cfoutput>
			<form method="post" action="details.cfm">
			<input type="hidden" name="rid" value="<cfoutput>#rid#</cfoutput>" />
			<input type="hidden" name="returnURL" value="<cfoutput>#returnURL#</cfoutput>" />
			<cfif not isDefined("form.btnAddComment") and not isdefined("url.editcomment")>
				<cfif getRequest.requestStatus is not 3>
				<tr>
					<td colspan="2"><input name="btnAddComment" type="submit" class="sidebar" value="Add A Comment" /></td>
				</tr>
				</cfif>
			<cfelse>
				<cfif isDefined("form.btnAddComment")>
				<tr>
					<td colspan="2"><textarea name="comment" style="width:485px;" rows="5"></textarea></td>
				</tr>
				<tr>
					<td colspan="2">
					<input name="btnSaveComment" type="submit" class="sidebar" value="Save Comment" />
					<input name="btnCancelComment" type="submit" class="sidebar" value="Cancel" />
					</td>
				</tr>
				<cfelseif isDefined("url.editcomment")>
				<cfquery name="getComment" datasource="#ds#">
					select * from requestTracking_comments 
					where commentid = #cid# 
				</cfquery>
				<tr>
					<td colspan="2"><textarea name="comment" style="width:485px;" rows="5"><cfoutput>#getcomment.comment#</cfoutput></textarea></td>
				</tr>
				<tr>
					<td colspan="2">
					<input type="hidden" name="cid" value="<cfoutput>#cid#</cfoutput>" />
					<input name="btnUpdateComment" type="submit" class="sidebar" value="Save Comment" />
					<input name="btnCancelComment" type="submit" class="sidebar" value="Cancel" />
					</td>
				</tr>			
				</cfif>
			</cfif>
			</form>
		</table>
		</td>
	</tr>
</table>
<cfif trim(returnURL) is not "##">
	<br />
	<!--- <a class="normal" style="text-decoration:underline;" href="<cfoutput>#returnURL#</cfoutput>?<cfoutput>#urlString#</cfoutput>">Return to Previous Page</a> --->
	<a class="normal" style="text-decoration:underline;" href="<cfoutput>#returnURL#</cfoutput>">Return to Previous Page</a>
</cfif>
</div>


