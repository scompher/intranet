
<link rel="stylesheet" type="text/css" href="/styles.css">

<cfset deptList = valuelist(getSec.departmentid)>

<cfquery name="getCats" datasource="#ds#">
	select * from requestTracking_requestTypes 
	<!--- where departmentid = #getsec.defaultdeptid#  --->
	where departmentid IN (#deptList#) 
	order by requestTypeName asc 
</cfquery>

<div align="center">
<table width="600" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Categories</b></td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
		<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td colspan="2" class="linedrowrightcolumn"><a href="add.cfm" style="text-decoration:underline;">Add a new Category</a> </td>
			</tr>
			<tr>
				<td width="12%" align="center" class="linedrow"><b>Action</b></td>
				<td width="88%" class="linedrow"><b>Category</b></td>
			</tr>
			<cfoutput query="getCats">
			<cfif getCats.currentRow mod 2 is 0>
				<cfset bgc = "##EEEEEE">
			<cfelse>
				<cfset bgc = "##FFFFFF">
			</cfif>
			<tr>
				<td bgcolor="#bgc#" align="center">
				<a href="edit.cfm?rid=#requesttypeid#"><img border="0" src="/images/edit.gif" width="16" height="16"></a>
				<!--- 
				&nbsp;&nbsp;
				<a onclick="return confirm('Are you sure you wish to delete this category?');" href="del.cfm?rid=#requesttypeid#"><img border="0" src="/images/delete.gif" width="16" height="16"></a>
				--->
				</td>
				<td bgcolor="#bgc#">#requestTypeName#</td>
			</tr>
			</cfoutput>
		</table>
		</td>
	</tr>
</table>
<br>
<a class="normal" style="text-decoration:underline;" href="/index.cfm">Return to Intranet</a>
</div>
