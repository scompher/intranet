
<link rel="stylesheet" type="text/css" href="/styles.css">
	
<cfif isDefined("form.btnUpdateCat")>
	
	<cfquery name="insertCat" datasource="#ds#">
		update requestTracking_requestTypes 
		set requestTypeName = '#form.catName#'
		where requestTypeID = #rid# 
	</cfquery>
	
	<cflocation url="index.cfm">
	
<cfelse>
	
	<cfquery name="getCat" datasource="#ds#">
		select * from requestTracking_requestTypes 
		where requestTypeID = #rid# 
	</cfquery>
	
	<div align="center">
	<table border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td class="highlightbar"><b>Edit a Category</b></td>
		</tr>
		<tr>
			<td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<form method="post" action="edit.cfm">
				<cfoutput>
				<input type="hidden" name="rid" value="#rid#" />
				</cfoutput>
				<tr>
					<td><b>Category Name:</b></td>
					<td><input name="catName" type="text" maxlength="100" style="width:300px" value="<cfoutput>#getCat.requestTypeName#</cfoutput>" /></td>
				</tr>
				<tr>
					<td colspan="2"><input name="btnUpdateCat" type="submit" class="sidebar" value="Update Category" /></td>
				</tr>
				</form>
			</table>
			</td>
		</tr>
	</table>
	<br />
	<a style="text-decoration:underline;" class="normal" href="index.cfm">Return</a>
	</div>

</cfif>
