
<cfif isDefined("form.btnAddCat")>
	
	<cfquery name="insertCat" datasource="#ds#">
		insert into requestTracking_requestTypes (requestTypeName, displayOrder, departmentid) 
		values ('#form.catName#', 1, #getsec.defaultdeptid#) 
	</cfquery>
	
	<cflocation url="index.cfm">
	
<cfelse>
	
	<link rel="stylesheet" type="text/css" href="/styles.css">
	
	<div align="center">
	<table border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td class="highlightbar"><b>Add a Category</b></td>
		</tr>
		<tr>
			<td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<form method="post" action="add.cfm">
				<tr>
					<td><b>Category Name:</b></td>
					<td><input name="catName" type="text" maxlength="100" style="width:300px" /></td>
				</tr>
				<tr>
					<td colspan="2"><input name="btnAddCat" type="submit" class="sidebar" value="Add Category" /></td>
				</tr>
				</form>
			</table>
			</td>
		</tr>
	</table>
	<br />
	<a style="text-decoration:underline;" class="normal" href="index.cfm">Return</a>
	</div>

</cfif>
