
<cfparam name="mainpage" default="">
<cfparam name="requestedBy" default="">
<cfparam name="requestorName" default="">
<cfparam name="dealerNumber" default="">
<cfparam name="dealerName" default="">
<cfparam name="btnDealerLookup" default="0">
<cfparam name="contactPerson" default="">
<cfparam name="requestDate" default="#dateformat(now(),'mm/dd/yyyy')#">
<cfparam name="requestType" default="0">
<cfparam name="comments" default="">
<cfparam name="requestStatus" default="0">
<cfparam name="submittedTo" default="0">
<cfparam name="nameOrVendor" default="">

<link rel="stylesheet" type="text/css" href="/styles.css">

<div align="center">
<table width="500" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Request Saved</b></td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
		<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<form method="post" action="<cfoutput>#mainpage#</cfoutput>">
			<cfoutput>
			<input type="hidden" name="requestedBy" value="#requestedBy#" />
			<input type="hidden" name="requestorName" value="#requestorName#" />
			<input type="hidden" name="dealerNumber" value="#dealerNumber#" />
			<input type="hidden" name="dealerName" value="#dealerName#" />
			<input type="hidden" name="contactPerson" value="#contactPerson#" />
			<input type="hidden" name="requestDate" value="#requestDate#" />
			<input type="hidden" name="backpage" value="#backpage#" />
			</cfoutput>
			<tr>
				<td align="center"><b style="font-family:Arial, Helvetica, sans-serif; font-size:14px;">Your request has been saved. </b></td>
			</tr>
			<tr>
				<td align="center"><b>Would you like to create another request for this same dealer? </b></td>
			</tr>
			<tr>
				<td align="center">
				<input name="btnYes" type="submit" class="sidebar" value="Yes">
				&nbsp;
				<input name="btnNo" type="button" class="sidebar" value="No" onclick="document.location='<cfoutput>#backpage#</cfoutput>';">
				</td>
			</tr>
			</form>
		</table>
		</td>
	</tr>
</table>
</div>

