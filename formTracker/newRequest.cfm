
<link rel="stylesheet" type="text/css" href="/styles.css">

<cffunction name="validateForm" access="private" output="no" returntype="string">
	<cfargument type="struct" name="form" required="yes">
	<cfset err = "">
	
		<cfif trim(requestedBy) is "">
			<cfset err = listappend(err,"Requested by is required.")>
		</cfif>
		<cfif trim(requestorName) is "">
			<cfset err = listappend(err,"The requestor name is required.")>
		</cfif>
		<cfif trim(dealerNumber) is "">
			<cfset err = listappend(err,"The dealer number is required.")>
		</cfif>
		<cfif trim(dealerName) is "">
			<cfset err = listappend(err,"The dealer name is required.")>
		</cfif>
		<cfif trim(contactPerson) is "">
			<cfset err = listappend(err,"The contact person is required.")>
		</cfif>
		<cfif trim(requestDate) is "">
			<cfset err = listappend(err,"The request date is required.")>
		</cfif>
		<cfif trim(requestType) is 0>
			<cfset err = listappend(err,"The request type is required.")>
		</cfif>
		<cfif trim(requestStatus) is 0>
			<cfset err = listappend(err,"The request status is required.")>
		</cfif>
		<cfif trim(submittedTo) is 0>
			<cfset err = listappend(err,"Who the request is submitted to is required.")>
		</cfif>
	<cfreturn err>
</cffunction>

<cfparam name="requestedBy" default="">
<cfparam name="requestorName" default="">
<cfparam name="dealerNumber" default="">
<cfparam name="dealerName" default="">
<cfparam name="btnDealerLookup" default="0">
<cfparam name="contactPerson" default="">
<cfparam name="requestDate" default="#dateformat(now(),'mm/dd/yyyy')#">
<cfparam name="requestType" default="0">
<cfparam name="comments" default="">
<cfparam name="requestStatus" default="0">
<cfparam name="submittedTo" default="0">
<cfparam name="nameOrVendor" default="">
<cfparam name="err" default="">
<cfparam name="backpage" default="index.cfm">
<cfparam name="departmentid" default="#getsec.defaultdeptid#">

<cfif isDefined("form.btnSaveRequest")>
	<cfset err = validateForm(form)>
	<cfif trim(err) is "">
		<cfquery name="saveData" datasource="#ds#">
			insert into requestTracking_requests (requestedBy, requestorName, dealerNumber, dealerName, contactPerson, requestDate, requestType, requestStatus, submittedTo, nameOrVendor, createdBy, departmentid) 
			values ('#requestedBy#', '#requestorName#', '#dealerNumber#', '#dealerName#', '#contactPerson#', #createodbcdate(requestDate)#, #requestType#, #requestStatus#, #submittedTo#, '#nameOrVendor#', '#createdBy#', #getsec.defaultdeptid#)
		</cfquery>
		<cfif trim(comments) is not "">
			<cfquery name="getID" datasource="#ds#">
				select max(requestid) as rid 
				from requestTracking_requests 
				where createdBy = '#createdBy#' 
			</cfquery>
			<cfset rid = getid.rid>
			<cfquery name="insertComment" datasource="#ds#">
				insert into requestTracking_comments (requestid, comment, commentDateTime, adminuserid) 
				values (#rid#, '#comments#', #createOdbcDateTime(now())#, #cookie.adminlogin#)
			</cfquery>
		</cfif>
		
		<!--- if request is marked completed --->
		<cfif requestStatus is 3>
			<!--- insert into dealer diary --->
			<cfquery name="getRequest" datasource="#ds#">
				select requestTracking_requests.*, requestTracking_requestStatus.requestStatusName, requestTracking_requestTypes.requestTypeName, requestTracking_submittedTo.submittedToName 
				from requestTracking_requests 
				left join requestTracking_requestStatus on requestTracking_requests.requestStatus = requestTracking_requestStatus.requestStatusID 
				left join requestTracking_requestTypes on requestTracking_requests.requestType = requestTracking_requestTypes.requestTypeID 
				left join requestTracking_submittedTo on requestTracking_requests.submittedTo = requestTracking_submittedTo.submittedToID 
				where requestTracking_requests.requestID = #rid# 
			</cfquery>
			<cfquery name="getcomments" datasource="#ds#">
				select requestTracking_comments.*, admin_users.firstname + ' ' + admin_users.lastname as creator 
				from requestTracking_comments 
				left join admin_users on requestTracking_comments.adminuserid = admin_users.adminuserid 
				where requestid = #rid# 
				order by commentDateTime asc 
			</cfquery>
			<cfset subject = "Request: #getRequest.requestTypeName#">
			<cfset noteText = "<a href=""javascript:viewRequestDetails('/formTracker/details.cfm?rid=#rid#');"">View Request Details</a>#chr(13)##chr(13)#Requested By: #getRequest.requestedBy##chr(13)#Requestor Name: #getRequest.requestorName##chr(13)#Completed Date: #dateformat(getRequest.completedDate,'mm/dd/yyyy')##chr(13)#">
			<cfset noteText = noteText & "#chr(13)# Comments: " & chr(13)>
			<cfloop query="getcomments">
				<cfset noteText = noteText & "#dateformat(commentDateTime, 'mm/dd/yyyy')# #timeformat(commentDateTime, 'hh:mm tt')# by #creator# #chr(13)##replace(comment, chr(13), "<br />", "all")# #chr(13)# #chr(13)#">
			</cfloop>
			<cfquery name="insertIntoDiary" datasource="#ds#">
				insert into dealerdiary_entries (creatorID, dealernumber, created, content, subject, departmentid, parentid, inquiryNote)
				values (#cookie.adminlogin#, '#getRequest.dealerNumber#', #createodbcdatetime(getRequest.completedDate)#, '#noteText#', '#subject#', #getsec.defaultdeptid#, 0, 0)
			</cfquery>
		</cfif>
		
		<cfset mainpage = "newRequest.cfm">
		<cfinclude template="saveRequest.cfm">
		<cfabort>
	</cfif>
</cfif>

<cfif btnDealerLookup is 1>
	<cfquery name="lookupDealer" datasource="#ds#">
		select * from dealerdiary_dealers 
		where dealernumber = '#dealerNumber#' 
	</cfquery>
	<cfset dealerNumber = ucase(dealerNumber)>
	<cfif lookupDealer.recordcount gt 0>
		<cfset dealerName = lookupDealer.dealername>
		<cfset contactPerson = lookupDealer.contact>
		<cfset foundFlag = true>
	<cfelse>
		<cfset dealerName = "">
		<cfset contactPerson = "">
		<cfset foundFlag = false>
	</cfif>
</cfif>

<cfset deptList = valuelist(getSec.departmentid)>

<cfquery name="getRequestTypes" datasource="#ds#">
	select * from requestTracking_requestTypes 
	where departmentid IN (#deptList#) 
	order by requestTypeName asc 
</cfquery>

<cfquery name="getSubmittedTo" datasource="#ds#">
	select * from requestTracking_submittedTo 
	order by submittedToName asc 
</cfquery>

<cfquery name="getRequestStatus" datasource="#ds#">
	select * from requestTracking_requestStatus 
	order by displayOrder asc 
</cfquery>

<cfquery name="getCreatedBy" datasource="#ds#">
	select * from admin_users 
	where adminuserid = #cookie.adminlogin# 
</cfquery>
<cfif getCreatedBy.recordcount gt 0>
	<cfset createdBy = getCreatedBy.firstname & " " & getCreatedBy.lastname>
<cfelse>
	<cfset createdby = "">
</cfif>

<div align="center">
<table border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Enter new request</b> </td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
		<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<cfif trim(err) is not "">
			<tr>
				<td colspan="2" class="nopadding">
					<table width="100%" border="0" cellspacing="0" cellpadding="3">
						<cfloop list="#err#" index="e">
						<tr>
							<td class="alert">
							<cfoutput>#e#</cfoutput>
							</td>
						</tr>
						</cfloop>
					</table>
				</td>
			</tr>
			</cfif>
			<form method="post" action="newRequest.cfm">
			<input type="hidden" name="createdBy" value="<cfoutput>#createdBy#</cfoutput>">
			<input type="hidden" name="backpage" value="<cfoutput>#backpage#</cfoutput>">
			<tr>
				<td width="19%" nowrap><b>Created By: </b></td>
				<td width="81%"><cfoutput>#createdBy#</cfoutput></td>
			</tr>
			<tr>
				<td nowrap><b>Requested By: </b></td>
				<td class="nopadding">
				<table border="0" cellspacing="0" cellpadding="3">
					<tr>
						<td><input name="requestedBy" <cfif requestedBy is "Dealer">checked</cfif> type="radio" value="Dealer"></td>
						<td>Dealer</td>
						<td width="20">&nbsp;</td>
						<td><input name="requestedBy" <cfif requestedBy is "In House Request">checked</cfif> type="radio" value="In House Request"></td>
						<td>In House Request </td>
					</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td nowrap><b>Requestor Name: </b></td>
				<td><input type="text" name="requestorName" style="width:300px;" value="<cfoutput>#requestorName#</cfoutput>"></td>
			</tr>
			<tr>
				<td nowrap><b>Dealer Number: </b></td>
				<td>
				<input onkeyup="if (this.value.length == this.maxLength) {this.form.btnDealerLookup.value = 1; this.form.submit();}" type="text" name="dealerNumber" style="width:45px; vertical-align:middle;" maxlength="4" value="<cfoutput>#dealerNumber#</cfoutput>">
				<input type="hidden" name="btnDealerLookup" value="0" />
				<cfif btnDealerLookup is 1>
					<cfif foundFlag is false>
					<span class="alert">
					&nbsp;Dealer not found 
					</span>
					</cfif>
				</cfif>
				</td>
			</tr>
			<tr>
				<td nowrap><b>Dealer Name: </b></td>
				<td><input type="text" name="dealerName" style="width:300px;" value="<cfoutput>#dealerName#</cfoutput>"></td>
			</tr>
			<tr>
				<td nowrap><b>Contact:</b></td>
				<td><input type="text" name="contactPerson" style="width:300px;" value="<cfoutput>#contactPerson#</cfoutput>"></td>
			</tr>
			<tr>
				<td nowrap><b>Date:</b></td>
				<td>
				<input name="requestDate" type="text" maxlength="10" style="vertical-align:middle; width:75px;" value="<cfoutput>#requestDate#</cfoutput>">
				<img src="/images/calIcon.gif" align="absmiddle">
				</td>
			</tr>
			<tr>
				<td nowrap><b>Request:</b></td>
				<td>
				<select name="requestType">
					<option value="0"></option>
					<cfoutput query="getRequestTypes">
						<cfif requestTypeName is not "Other">
							<option <cfif requestType is getRequestTypes.requestTypeID>selected</cfif> value="#requestTypeID#">#requestTypeName#</option>
						</cfif>
					</cfoutput>
					<cfoutput query="getRequestTypes">
						<cfif requestTypeName is "Other">
							<option <cfif requestType is getRequestTypes.requestTypeID>selected</cfif> value="#requestTypeID#">#requestTypeName#</option>
						</cfif>
					</cfoutput>
				</select>
				</td>
			</tr>
			<tr>
				<td nowrap valign="top"><b>Comments:</b></td>
				<td><textarea name="comments" rows="4" style="width:300px;"><cfoutput>#comments#</cfoutput></textarea></td>
			</tr>
			<tr>
				<td nowrap><b>Status:</b></td>
				<td>
				<select name="requestStatus">
					<option value=""></option>
					<cfoutput query="getRequestStatus">
						<option <cfif requestStatus is getRequestStatus.requestStatusID>selected</cfif> value="#requestStatusID#">#requestStatusName#</option>
					</cfoutput>
				</select>
				</td>
			</tr>
			<tr>
				<td nowrap><b>Submitted To: </b></td>
				<td>
				<select name="submittedTo">
					<option value="0"></option>
					<cfoutput query="getSubmittedTo">
						<option <cfif submittedTo is getSubmittedTo.submittedToID>selected</cfif> value="#submittedToID#">#submittedToName#</option>
					</cfoutput>
				</select>
				</td>
			</tr>
			<tr>
				<td nowrap><b>Name/Vendor:</b></td>
				<td><input type="text" name="nameOrVendor" style="width:300px;"></td>
			</tr>
			<tr>
				<td colspan="2">
				<input name="btnSaveRequest" type="submit" class="highlightbar" value="Save Request">
				<input name="btnCancel" type="button" onclick="document.location='<cfoutput>#backpage#</cfoutput>';" class="highlightbar" value="Cancel">
				</td>
			</tr>
			</form>
		</table>
		</td>
	</tr>
</table>
</div>

