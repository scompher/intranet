
<link rel="stylesheet" type="text/css" href="/styles.css">

<cfif isDefined("form.btnUpdateItems")>
	<cfinclude template="updateItems.cfm">
</cfif>

<script type="text/javascript">
function checkUncheck(frm) {
	if (frm.checkStatus.value == 1) {
		var checkStatus = false;
		frm.checkStatus.value = 0;
	} else {
		var checkStatus = true;
		frm.checkStatus.value = 1;
	}
	if (frm.requestid.length > 1) {
		for (var i=0; i < frm.requestid.length; i++) {
			frm.requestid[i].checked = checkStatus;
		}
	} else {
		frm.requestid.checked = checkStatus;
	}
}
</script>

<cfparam name="searchRequestStatusID" default="1,2">
<cfparam name="searchDealerNumber" default="">
<cfparam name="SearchCreatedBy" default="">
<cfparam name="SearchSubmittedTo" default="0">
<cfparam name="sort" default="dateDesc">
<cfparam name="departmentid" default="#valuelist(getSec.departmentid)#">

<cfswitch expression="#sort#">
	<cfcase value="createdByAsc"><cfset orderby = "createdBy ASC, requestDate ASC"></cfcase>
	<cfcase value="createdByDesc"><cfset orderby = "createdBy DESC, requestDate ASC"></cfcase>
	<cfcase value="dealerNumAsc"><cfset orderby = "dealerNumber ASC, requestDate ASC"></cfcase>
	<cfcase value="dealerNumDesc"><cfset orderby = "dealerNumber DESC, requestDate ASC"></cfcase>
	<cfcase value="dealerNameAsc"><cfset orderby = "dealerName ASC, requestDate ASC"></cfcase>
	<cfcase value="dealerNameDesc"><cfset orderby = "dealerName DESC, requestDate ASC"></cfcase>
	<cfcase value="dateAsc"><cfset orderby = "requestDate ASC"></cfcase>
	<cfcase value="dateDesc"><cfset orderby = "requestDate DESC"></cfcase>
	<cfcase value="requestAsc"><cfset orderby = "requestTypeName ASC, requestDate ASC"></cfcase>
	<cfcase value="requestDesc"><cfset orderby = "requestTypeName DESC, requestDate ASC"></cfcase>
	<cfcase value="statusAsc"><cfset orderby = "requestStatusName ASC, requestDate ASC"></cfcase>
	<cfcase value="statusDesc"><cfset orderby = "requestStatusName DESC, requestDate ASC"></cfcase>
	<cfcase value="submittedToAsc"><cfset orderby = "submittedToName DESC, requestDate ASC"></cfcase>
	<cfcase value="submittedToDesc"><cfset orderby = "submittedToName DESC, requestDate ASC"></cfcase>
</cfswitch>

<cfquery name="getRequests" datasource="#ds#">
	select requestTracking_requests.*, requestTracking_requestStatus.requestStatusName, requestTracking_requestTypes.requestTypeName, requestTracking_submittedTo.submittedToName 
	from requestTracking_requests 
	left join requestTracking_requestStatus on requestTracking_requests.requestStatus = requestTracking_requestStatus.requestStatusID 
	left join requestTracking_requestTypes on requestTracking_requests.requestType = requestTracking_requestTypes.requestTypeID 
	left join requestTracking_submittedTo on requestTracking_requests.submittedTo = requestTracking_submittedTo.submittedToID
	where 
	requestTracking_requests.departmentid IN (#departmentid#) and 
	<cfif SearchRequestStatusID is not 0>
		requestStatus IN (#SearchRequestStatusID#) and 
	</cfif>
	<cfif trim(searchDealerNumber) is not "">
		dealerNumber = '#searchDealerNumber#' and 
	</cfif>
	<cfif trim(SearchCreatedBy) is not "">
		createdBy = '#SearchCreatedBy#' and 
	</cfif>
	<cfif trim(SearchSubmittedTo) is not 0>
		submittedTo = '#SearchSubmittedTo#' and 
	</cfif>	
	1=1 
	order by #orderby# 
</cfquery>

<cfquery name="getRequestStatus" datasource="#ds#">
	select * from requestTracking_requestStatus 
	order by displayOrder asc 
</cfquery>

<cfquery name="getCreatedBy" datasource="#ds#">
	select distinct createdBy 
	from requestTracking_requests 
	where departmentid IN (#departmentid#)  
	order by createdBy asc 
</cfquery>

<cfquery name="getSubmittedTo" datasource="#ds#">
	select * from requestTracking_submittedTo 
	order by submittedToName asc 
</cfquery>

<cfset urlString = "searchRequestStatusID=#searchRequestStatusID#&searchDealerNumber=#searchDealerNumber#&SearchCreatedBy=#SearchCreatedBy#">

<div align="center">
<table width="400" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Search for Requests</b></td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
		<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td colspan="2"><b>Search by the following criteria</b> </td>
			</tr>
			<form method="post" action="index.cfm">
			<tr>
				<td width="18%" nowrap="nowrap">Status:</td>
				<td width="82%">
					<select name="SearchRequestStatusID" class="normal">
						<option value="1,2">All Pending Status</option>
						<cfoutput query="getRequestStatus">
							<option <cfif getRequestStatus.requestStatusID is SearchRequestStatusID>selected</cfif> value="#requestStatusID#">#requestStatusName#</option>
						</cfoutput>
					</select>
				</td>
			</tr>
			<tr>
				<td nowrap="nowrap">Dealer Number: </td>
				<td><input type="text" name="searchDealerNumber" style="width:45px;" maxlength="4" value="<cfoutput>#searchDealerNumber#</cfoutput>"></td>
			</tr>
			<tr>
				<td nowrap="nowrap">Created By: </td>
				<td>
					<select name="SearchCreatedBy" class="normal">
						<option value="">Anyone</option>
						<cfoutput query="getCreatedBy">
							<option <cfif getCreatedBy.createdBy is SearchCreatedBy>selected</cfif> value="#createdBy#">#createdBy#</option>
						</cfoutput>
					</select>
				</td>
			</tr>
			<tr>
				<td nowrap="nowrap">Submitted To: </td>
				<td>
					<select name="SearchSubmittedTo">
					<option value="0"></option>
					<cfoutput query="getSubmittedTo">
						<option <cfif SearchSubmittedTo is getSubmittedTo.submittedToID>selected</cfif> value="#submittedToID#">#submittedToName#</option>
					</cfoutput>
					</select>
				</td>
			</tr>
			<tr>
				<td colspan="2">
				<input name="btnSearch" type="submit" class="sidebar" value="Search Now" />
				<input name="btnNewSearch" type="button" class="sidebar" value="New Search" onclick="document.location = 'index.cfm'" />
				</td>
			</tr>
			</form>
		</table>
		</td>
	</tr>
</table>
<br />
<table border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Dealer Support Request Tracker</b> </td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
		<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<form method="post" action="" name="outputForm">
			<input type="hidden" name="checkStatus" value="0" />
			<tr>
				<td colspan="10" class="nopadding">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td class="linedrow">
						<b>Update Checked Items:</b> 
						<select name="requestStatusID" class="normal" style="vertical-align:middle;">
							<option value="0"></option>
							<cfoutput query="getRequestStatus">
								<option value="#requestStatusID#">#requestStatusName#</option>
							</cfoutput>
						</select>
						<input name="btnUpdateItems" onclick="return confirm('Are you SURE you wish to perform this action? \n Clicking Apply will update the status of all checked items.');" type="submit" class="sidebar" style="vertical-align:middle;" value="Apply" />
						</td>
						<td class="linedrow">&nbsp;</td>
						<td class="linedrow"><input onclick="document.location='newRequest.cfm';" type="button" class="sidebar" value="Create New Request" /></td>
					</tr>
				</table>
				</td>
			</tr>
			<cfoutput>
			<tr>
				<td align="center" class="linedrow">&nbsp;</td>
				<td align="center" class="linedrow">&nbsp;</td>
				<td align="center" class="linedrow"><a href="javascript:checkUncheck(document.outputForm);"><img border="0" src="/images/checkBox_clearbg.gif" alt="Check All/Uncheck All" width="23" height="26" /></a></td>
				<cfif sort is "createdByAsc"><cfset thisSort="createdByDesc"><cfelse><cfset thisSort = "createdByAsc"></cfif>
				<td class="linedrow"><b><a style="text-decoration:underline;" href="index.cfm?sort=#thisSort#&#urlString#">Created By</a></b></td>
				<cfif sort is "dealerNumAsc"><cfset thisSort="dealerNumDesc"><cfelse><cfset thisSort = "dealerNumAsc"></cfif>
				<td class="linedrow" nowrap="nowrap"><b><a style="text-decoration:underline;" href="index.cfm?sort=#thisSort#&#urlString#">Dealer ##</a></b></td>
				<cfif sort is "dealerNameAsc"><cfset thisSort="dealerNameDesc"><cfelse><cfset thisSort = "dealerNameAsc"></cfif>
				<td class="linedrow"><b><a style="text-decoration:underline;" href="index.cfm?sort=#thisSort#&#urlString#">Dealer Name</a></b></td>
				<cfif sort is "dateAsc"><cfset thisSort="dateDesc"><cfelse><cfset thisSort = "dateAsc"></cfif>
				<td align="center" class="linedrow"><b><a style="text-decoration:underline;" href="index.cfm?sort=#thisSort#&#urlString#">Date Submitted</a></b></td>
				<cfif sort is "requestAsc"><cfset thisSort="requestDesc"><cfelse><cfset thisSort = "requestAsc"></cfif>
				<td class="linedrow"><b><a style="text-decoration:underline;" href="index.cfm?sort=#thisSort#&#urlString#">Request</a></b></td>
				<cfif sort is "submittedToAsc"><cfset thisSort="submittedToDesc"><cfelse><cfset thisSort = "submittedToAsc"></cfif>
				<td class="linedrow"><b><a style="text-decoration:underline;" href="index.cfm?sort=#thisSort#&#urlString#">Submitted To</a></b></td>
				<cfif sort is "statusAsc"><cfset thisSort="statusDesc"><cfelse><cfset thisSort = "statusAsc"></cfif>
				<td class="linedrow"><b><a style="text-decoration:underline;" href="index.cfm?sort=#thisSort#&#urlString#">Status</a></b></td>
			</tr>
			</cfoutput>
			<cfoutput query="getRequests">
			<cfif getRequests.currentRow mod 2 is 0>
				<cfset bgc = "##EEEEEE">
			<cfelse>
				<cfset bgc = "##FFFFFF">
			</cfif>
			<tr>
				<td align="center" bgcolor="#bgc#" class="linedrow">#getRequests.currentRow#.</td>
				<td align="center" bgcolor="#bgc#" class="linedrow"><a href="details.cfm?rid=#requestID#&sort=#sort#&#urlString#"><img border="0" src="/images/manifyingglass.gif" alt="View Details" width="15" height="15" /></a></td>
				<td align="center" bgcolor="#bgc#" class="linedrow"><input type="checkbox" name="requestid" value="#requestid#" /></td>
				<td bgcolor="#bgc#" class="linedrow">#createdBy#</td>
				<td bgcolor="#bgc#" class="linedrow">#dealerNumber#</td>
				<td bgcolor="#bgc#" class="linedrow">#dealerName#</td>
				<td align="center" bgcolor="#bgc#" class="linedrow">#dateformat(requestDate,'mm/dd/yyyy')#</td>
				<td bgcolor="#bgc#" class="linedrow">#requestTypeName#</td>
				<td bgcolor="#bgc#" class="linedrow">#submittedToName#</td>
				<td bgcolor="#bgc#" class="linedrow">
				<cfif requestStatusName is "Completed">
					Completed #dateformat(completedDate,'mm/dd/yyyy')# 
				<cfelse>
					#requestStatusName#
				</cfif>
				</td>
			</tr>
			</cfoutput>
			</form>
		</table>
		</td>
	</tr>
</table>
<br />
<a class="normal" style="text-decoration:underline;" href="/index.cfm">Return to Intranet</a>
</div>
