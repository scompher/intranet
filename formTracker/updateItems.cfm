
<cfparam name="requestid" default="0">

<cfloop list="#requestid#" index="rid">
	<cfquery name="updateStatus" datasource="#ds#">
		update requestTracking_requests 
		set <cfif requestStatusID is 3>completedDate = #createodbcdatetime(now())#, </cfif>requestStatus = #requestStatusID# 
		where requestid = #rid# 
	</cfquery>
</cfloop>

