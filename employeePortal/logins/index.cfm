
<link rel="stylesheet" type="text/css" href="/styles.css">

<cfparam name="form.filter_employeeID" default="">
<cfparam name="form.filter_employeeName" default="">
<cfparam name="form.filter_lockedOut" default="0">
<cfparam name="form.filter_pwExpired" default="0">
<cfparam name="form.filter_site" default="">

<cfquery name="getEmployees" datasource="#ds#">
	select * 
	from employeePortalEmployees 
	where active = 1 and 
	<cfif trim(form.filter_employeeID) is not "">employeeID like '%#form.filter_employeeID#%' and </cfif>
	<cfif trim(form.filter_employeeName) is not "">lastname like '%#form.filter_employeeName#%' and </cfif>
	<cfif form.filter_lockedOut is 1>isLockedOut = 1 and </cfif>
	<cfif form.filter_pwExpired is 1>passwordExpired = 1 and </cfif>
	<cfif form.filter_site is not "">site = '#form.filter_site#' and </cfif>
	1 = 1
	order by lastname asc, firstname asc 
</cfquery>

<div align="center">

<cfform method="post" action="index.cfm">
<table width="800" border="0" cellspacing="0" cellpadding="5">
    <tr>
        <td class="highlightbar"><b>Employee Portal - Filter</b></td>
    </tr>
    <tr>
        <td class="greyrowbottomnopadding">
			<table border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td colspan="3">
					<b>Filter By:</b>					</td>
				</tr>
				<tr>
					<td>
						Employee #: 
						<cfinput name="filter_employeeID" type="text" maxlength="5" style="width:50px; vertical-align:middle;" value="#form.filter_employeeID#" />&nbsp;					</td>
					<td>
						Employee Last Name:
						<cfinput name="filter_employeeName" type="text" maxlength="50" style="width:200px; vertical-align:middle;" value="#filter_employeeName#" />&nbsp;					</td>
					<td>
						Site:
						<cfselect name="filter_site" style="vertical-align:middle;">
							<option value=""></option>
							<option <cfif form.filter_site is "NJ">selected</cfif> value="NJ">Williamstown, NJ</option>
							<option <cfif form.filter_site is "TX">selected</cfif> value="TX">Lewisville, TX</option>
							<option <cfif form.filter_site is "AZ">selected</cfif> value="AZ">Scottsdale, AZ</option>
							<option <cfif form.filter_site is "FL">selected</cfif> value="FL">Boca Raton, FL</option>
							<option <cfif form.filter_site is "TN">selected</cfif> value="TN">Nashville, TN</option>
							<option <cfif form.filter_site is "MD">selected</cfif> value="MD">Hunt Valley, MD</option>
						</cfselect>
					</td>
				</tr>
				<tr>
				    <td> Locked Out:
						<cfif filter_lockedOut is 1>
							<cfinput type="checkbox" name="filter_lockedOut" value="1" checked="yes" style="vertical-align:middle;"/>
						<cfelse>
							<cfinput type="checkbox" name="filter_lockedOut" value="1" style="vertical-align:middle;" />
						</cfif>
&nbsp;					</td>
				    <td> Password Expired:
						<cfif filter_pwExpired is 1>
							<cfinput type="checkbox" name="filter_pwExpired" value="1" checked="yes" style="vertical-align:middle;"/>
						<cfelse>
							<cfinput type="checkbox" name="filter_pwExpired" value="1" style="vertical-align:middle;" />
						</cfif>
&nbsp;					</td>
				    <td>&nbsp;</td>
				    </tr>
				<tr>
					<td colspan="3">
					<cfinput type="submit" name="btnFilter" value="Apply Filters" class="sidebar">
					<cfinput type="button" name="btnClear" value="Clear Filters" class="sidebar" onClick="document.location = 'index.cfm';">
					</td>
				</tr>
			</table>
		</td>
    </tr>
</table>
</cfform>

<br />

<table width="800" border="0" cellspacing="0" cellpadding="5">
    <tr>
        <td class="highlightbar"><b>Employee Portal - Employees</b></td>
    </tr>
    <tr>
        <td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<cfif getEmployees.recordcount gt 1>
				<tr>
				    <td align="center" nowrap class="linedrowrightcolumn"><input onclick="document.location='add.cfm';" type="button" value="Add New" class="sidebarsmall" /></td>
				    <td  colspan="6" class="linedrow">&nbsp;</td>
			    </tr>
				</cfif>
				<tr>
					<td width="14%" align="center" nowrap class="linedrowrightcolumn"><b>Action</b></td>
					<td width="12%" nowrap="nowrap" class="linedrowrightcolumn"><b>Employee # </b></td>
				    <td width="36%" nowrap="nowrap" class="linedrowrightcolumn"><b>Employee Name </b></td>
				    <td width="5%" nowrap="nowrap" class="linedrowrightcolumn"><b>Site</b></td>
				    <td width="13%" nowrap="nowrap" class="linedrowrightcolumn"><b>Username</b></td>
				    <td width="14%" nowrap="nowrap" class="linedrowrightcolumn"><b>Locked Out </b></td>
				    <td width="11%" nowrap="nowrap" class="linedrow"><b>PW Expired</b></td>
				</tr>
				<cfoutput query="getEmployees">
				<tr>
				    <td align="center" nowrap class="linedrowrightcolumn">
					<a href="edit.cfm?eid=#employeeLoginID#">[edit]</a>
					<a onclick="return confirm('Are you sure you wish to delete this employee portal login?');" href="delete.cfm?eid=#employeeLoginID#">[delete]</a>					</td>
					<td nowrap="nowrap" class="linedrowrightcolumn">#employeeID#</td>
				    <td nowrap="nowrap" class="linedrowrightcolumn">#firstname# #lastname#</td>
				    <td nowrap="nowrap" class="linedrowrightcolumn">#site#</td>
				    <td nowrap="nowrap" class="linedrowrightcolumn">#username#</td>
				    <td nowrap="nowrap" class="linedrowrightcolumn">
				        <cfif isLockedOut is 1>YES<cfelse>NO</cfif></td>
				    <td nowrap="nowrap" class="linedrow">
				        <cfif passwordExpired is 1>YES<cfelse>NO</cfif></td>
			    </tr>
				</cfoutput>
				<tr>
				    <td align="center" nowrap class="linedrowrightcolumn"><input onclick="document.location='add.cfm';" type="button" value="Add New" class="sidebarsmall" /></td>
				    <td  colspan="6" class="linedrow">&nbsp;</td>
			    </tr>
			</table>
		</td>
    </tr>
</table>
<br />
<a class="normal" style="text-decoration:underline;" href="/index.cfm">Return to Intranet</a>
<br />
<br />
</div>

