
<cffunction name="validatePassword" access="private" returntype="boolean">
	<cfargument type="string" name="password" required="yes">
	
	<cfset isValid = true>
	<cfset hasLetters = false>
	<cfset hasNumbers = false>
	
	<cfloop from="1" to="#len(trim(password))#" index="c">
		<cfset currentChar = mid(password,c,1)>
		<cfset aVal = asc(currentChar)>
		<cfif (aVal gte 48 and aVal lte 57) or (aVal gte 65 and aVal lte 90) or (aVal gte 97 and aVal lte 122)>
			<cfif (aVal gte 48 and aVal lte 57)>
				<cfset hasNumbers = true>
			</cfif>
			<cfif (aVal gte 65 and aVal lte 90) or (aVal gte 97 and aVal lte 122)>
				<cfset hasLetters = true>
			</cfif>
		<cfelse>
			<cfset isValid = false>
			<cfbreak>
		</cfif>
	</cfloop>
	
	<cfif isValid>
		<cfif hasLetters is false or hasNumbers is false><cfset isValid = false></cfif>
	</cfif>
	
	<cfif isValid>
		<cfset pwLen = len(trim(form.password))>
		<cfif pwLen lt 6 or pwLen gt 10>
			<cfset isValid = false>
		</cfif>
	</cfif>
	
	<cfreturn isValid>
</cffunction>

<link rel="stylesheet" type="text/css" href="/styles.css">

<div align="center">

<cfparam name="form.employeeNumber" default="">
<cfparam name="form.firstName" default="">
<cfparam name="form.lastName" default="">
<cfparam name="form.username" default="">
<cfparam name="form.password" default="">
<cfparam name="form.confirmPassword" default="">
<cfparam name="form.active" default="0">
<cfparam name="form.site" default="">
<cfparam name="err" default="">

<cfif isDefined("form.btnAddEmployee")>
	<cfif trim(form.password) is "">
		<cfset err = listappend(err,"The password is required and must be between 6 to 10 letters and numbers.")>
	<cfelse>
		<cfif trim(form.password) is not trim(form.confirmPassword)>
			<cfset err = listappend(err,"The password and confirming password do not match.")>
		</cfif>
		<cfif validatePassword(form.password) is false>
			<cfset err = listappend(err,"The password must consist of 6 to 10 letters and numbers.")>
		</cfif>
		<cfquery name="checkDup" datasource="#ds#">
			select * from employeePortalEmployees where employeeID = '#trim(form.employeeNumber)#' 
		</cfquery>
		<cfif checkDup.recordcount gt 0>
			<cfset err = listappend(err,"That employee number already exists.")>
		</cfif>
	</cfif>
	<cfif trim(err) is "">
		<cfset pwExpires = dateadd('d',-1,now())>
		<cfquery name="saveEmployee" datasource="#ds#">
			insert into employeePortalEmployees (employeeID, firstName, lastName, username, password, active, site, expiresDateTime, passwordExpired) 
			values (#form.employeeNumber#, '#form.firstName#', '#form.lastName#', '#form.employeeNumber#', '#form.password#', 1, '#form.site#', #createodbcdatetime(pwExpires)#, 1) 
		</cfquery>

		<table width="400" border="0" cellspacing="0" cellpadding="5">
			<tr>
		        <td class="highlightbar"><b>Employee Portal - Add Employee</b></td>
			</tr>
			<tr>
				<td class="greyrowbottomnopadding">
					<table width="100%" border="0" cellspacing="0" cellpadding="5">
						<tr>
							<td align="center"><b>Employee saved successfully</b><br /><br /><a href="index.cfm">Click Here to return to Employee Listing</a></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>

		<cfabort>
	</cfif>
</cfif>

<cfif not isDefined("form.submitted")>
	<cfset form.active = 1>
</cfif>

<cfif trim(err) is not "">
<span class="alert">
<cfloop list="#err#" index="e">
<cfoutput>
<b style="font-size:14px;">#e#</b><br />
</cfoutput>
</cfloop>
</span>
<br />
</cfif>

<cfform method="post" action="add.cfm">
<cfinput type="hidden" name="submitted" value="1">
<table border="0" cellspacing="0" cellpadding="5">
    <tr>
        <td class="highlightbar"><b>Employee Portal - Add Employee</b></td>
    </tr>
    <tr>
        <td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td width="21%" nowrap>Employee Number: </td>
				    <td width="79%">
				        <cfinput type="text" name="employeeNumber" maxlength="5" style="width:50px;" value="#form.employeeNumber#" required="yes" validate="integer" message="The employee number is required.">
				    </td>
				</tr>
				<tr>
				    <td nowrap>First Name: </td>
				    <td>
				        <cfinput type="text" name="firstName" style="width:200px;" value="#form.firstName#" required="yes" message="The first name is required.">
				    </td>
			    </tr>
				<tr>
				    <td nowrap>Last Name: </td>
				    <td>
				        <cfinput type="text" name="lastName" style="width:200px;" value="#form.lastName#" required="yes" message="The last name is required.">
				    </td>
			    </tr>
				<tr>
				    <td nowrap>Location:</td>
				    <td>
					<cfselect name="site" required="yes" message="Please select a site.">
						<option value="">Please select a site</option>
						<option <cfif form.site is "NJ">selected</cfif> value="NJ">Williamstown, NJ</option>
						<option <cfif form.site is "TX">selected</cfif> value="TX">Lewisville, TX</option>
						<option <cfif form.site is "AZ">selected</cfif> value="AZ">Scottsdale, AZ</option>
						<option <cfif form.site is "FL">selected</cfif> value="FL">Boca Raton, FL</option>
						<option <cfif form.site is "TN">selected</cfif> value="TN">Nashville, TN</option>
						<option <cfif form.site is "MD">selected</cfif> value="MD">Hunt Valley, MD</option>
					</cfselect>
					</td>
				</tr>
				<!---
				<tr>
				    <td nowrap>Username:</td>
				    <td>
				        <cfinput type="text" name="username" style="width:200px;" value="#form.username#" required="yes" message="The username is required.">
				    </td>
			    </tr>
				--->
				<tr>
				    <td nowrap>Password:</td>
				    <td>
				        <cfinput type="password" name="password" style="width:200px;" value="#form.password#">
				    </td>
			    </tr>
				<tr>
				    <td nowrap>Confirm Password:</td>
				    <td>
				        <cfinput type="password" name="confirmPassword" style="width:200px;" value="#form.confirmPassword#">
				    </td>
			    </tr>
				<tr>
				    <td colspan="2">
				        <cfinput name="btnAddEmployee" type="submit" class="sidebar" id="btnAddEmployee" value="Add Employee">
				        <cfinput onClick="document.location = 'index.cfm';" name="btnCancel" type="button" class="sidebar" id="btnCancel" value="Cancel">
				    </td>
			    </tr>
			</table>
		</td>
    </tr>
</table>

</cfform>

</div>

