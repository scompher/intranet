﻿
    pageContentLoad(
    {
    "currentPage":{"item":"page22", "num":"22"},
    "keywords":"C.O.P.S., Employee, Handbook, September, 2013:, Page, 21, of, 22, Downloaded, or, printed, pages, may, be, outdated., If, you, downloaded, this, page,, please, check, the, intranet, to, ensure, have, most, recent, version., Company, Departments, Accounting, Among, other, responsibilities, Department:, , Conducts, credit, references, Maintains, hold, list, Administers, dealer, billing, Answers, invoice, questions, Performs, collections, accounts, payable, Handles, all, company, financial, matters, Data, Entry, Enters, temporary, and, new, subscriber, account, information, into, computer, Proofreads, that, has, been, entered, in, Makes, changes, at, dealer’s, request, Sets, up, for, open, /, close, reports, Dealer, Support, inquiries, than, accounting, concerning, past, alarm, history, administrative, Human, Resources, Resources:, Processes, payroll, benefits, Mail, Room, Room:, Opens, incoming, mail, Purchases, office, supplies, contracts, brochures, outgoing, shipping, general, filing, file, search, seizures, vehicles, Operations, Operations:, activity, receivers, Assists, subscriber/dealers, with, related, Sales, &, Marketing, Marketing:, Is, responsible, advertising,, promotions,, marketing,, trade, show, Follows, on, sales, sales-related, issues, existing, customers, Technology, computers,, terminals,, printers, computerized, automation, system, phone, lines, MPower, COP-A-FAX, EMAIL, special, receiver, equipment, technical, problems, Provides, training, support, Building",
    "fullLink":"../../../index.html",
    "height":"792",
    "width":" 612",
    "isWide":"False",
    "bookWidth":"612",
    "bookHeight":"792",

    "download":[{}
      ,{"pdfPublication":{"url":"../common/downloads_c167bf08/COPS_BULLETin.pdf", "size":"1.16 MB"}}
    
      ,{"PdfPage":{"url":"../common/downloads_c167bf08/page0022.pdf", "size":"321.23 KB"}}
     ],

    
    
    "substrate":{"background":"url(../common/page-substrates/page0022.png)",
    "backgroundSize":"contain", "backgroundColor":"#FFFFFF"},
    
    "leftTool":{"innerText":"21","src":"page21.html"},
    
    "rightTool":{"innerText":" 23","src":"page23.html"},
    
    "content":[{}
    
    ,{"className":"div_items", "top":"759","left":"181","width":"214","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font1","text":"C.O.P.S. Employee Handbook September 2013:  Page" 
    }}
  
    ,{"className":"div_items", "top":"759","left":"398","width":"10","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font2","text":"21" 
    }}
  
    ,{"className":"div_items", "top":"759","left":"410","width":"8","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font1","text":"of" 
    }}
  
    ,{"className":"div_items", "top":"759","left":"421","width":"10","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font2","text":"22" 
    }}
  
    ,{"className":"div_items", "top":"770","left":"36","width":"545","height":"10","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font3","text":"Downloaded or printed pages may be outdated.  If you printed or downloaded this page, please check the C.O.P.S. intranet to ensure you have the most recent version." 
    }}
  
    ,{"className":"div_items", "top":"34","left":"232","width":"148","height":"16","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font8","text":"Company Departments" 
    }}
  
    ,{"className":"div_items", "top":"78","left":"41","width":"57","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font10","text":"Accounting" 
    }}
  
    ,{"className":"div_items", "top":"91","left":"41","width":"199","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Among other responsibilities the Accounting" 
    }}
  
    ,{"className":"div_items", "top":"104","left":"41","width":"58","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Department:" 
    }}
  
    ,{"className":"div_items", "top":"117","left":"59","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"118","left":"79","width":"120","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Conducts credit references" 
    }}
  
    ,{"className":"div_items", "top":"131","left":"59","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"132","left":"79","width":"111","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Maintains credit hold list" 
    }}
  
    ,{"className":"div_items", "top":"145","left":"59","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"146","left":"79","width":"114","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Administers dealer billing" 
    }}
  
    ,{"className":"div_items", "top":"158","left":"59","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"159","left":"79","width":"118","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Answers invoice questions" 
    }}
  
    ,{"className":"div_items", "top":"172","left":"59","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"173","left":"79","width":"91","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Performs collections" 
    }}
  
    ,{"className":"div_items", "top":"186","left":"59","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"187","left":"79","width":"124","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Maintains accounts payable" 
    }}
  
    ,{"className":"div_items", "top":"200","left":"59","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"201","left":"79","width":"170","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Handles all company financial matters" 
    }}
  
    ,{"className":"div_items", "top":"224","left":"41","width":"54","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font10","text":"Data Entry" 
    }}
  
    ,{"className":"div_items", "top":"237","left":"41","width":"196","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Among other responsibilities the Data Entry" 
    }}
  
    ,{"className":"div_items", "top":"250","left":"41","width":"58","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Department:" 
    }}
  
    ,{"className":"div_items", "top":"263","left":"59","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"264","left":"77","width":"206","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Enters temporary and new subscriber account" 
    }}
  
    ,{"className":"div_items", "top":"277","left":"77","width":"137","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"information into the computer" 
    }}
  
    ,{"className":"div_items", "top":"290","left":"59","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"291","left":"77","width":"194","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Proofreads all account information that has" 
    }}
  
    ,{"className":"div_items", "top":"305","left":"77","width":"135","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"been entered in the computer" 
    }}
  
    ,{"className":"div_items", "top":"317","left":"59","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"318","left":"77","width":"200","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Makes changes to subscriber accounts at the" 
    }}
  
    ,{"className":"div_items", "top":"332","left":"77","width":"72","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"dealer’s request" 
    }}
  
    ,{"className":"div_items", "top":"345","left":"59","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"346","left":"77","width":"183","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Sets up accounts for open / close reports" 
    }}
  
    ,{"className":"div_items", "top":"369","left":"41","width":"77","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font10","text":"Dealer Support" 
    }}
  
    ,{"className":"div_items", "top":"382","left":"41","width":"216","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Among other responsibilities the Dealer Support" 
    }}
  
    ,{"className":"div_items", "top":"395","left":"41","width":"58","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Department:" 
    }}
  
    ,{"className":"div_items", "top":"408","left":"59","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"409","left":"77","width":"176","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Handles subscriber and dealer inquiries" 
    }}
  
    ,{"className":"div_items", "top":"422","left":"59","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"423","left":"77","width":"197","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Handles request for computer reports other" 
    }}
  
    ,{"className":"div_items", "top":"436","left":"77","width":"72","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"than accounting" 
    }}
  
    ,{"className":"div_items", "top":"449","left":"59","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"450","left":"77","width":"181","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Handles dealer inquiries concerning past" 
    }}
  
    ,{"className":"div_items", "top":"463","left":"77","width":"59","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"alarm history" 
    }}
  
    ,{"className":"div_items", "top":"476","left":"59","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"477","left":"77","width":"191","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Maintains Company administrative reports" 
    }}
  
    ,{"className":"div_items", "top":"500","left":"41","width":"91","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font10","text":"Human Resources" 
    }}
  
    ,{"className":"div_items", "top":"513","left":"41","width":"232","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Among other responsibilities the Human Resources:" 
    }}
  
    ,{"className":"div_items", "top":"526","left":"59","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"527","left":"77","width":"77","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Processes payroll" 
    }}
  
    ,{"className":"div_items", "top":"540","left":"59","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"541","left":"77","width":"136","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Administers company benefits" 
    }}
  
    ,{"className":"div_items", "top":"68","left":"315","width":"54","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font10","text":"Mail Room" 
    }}
  
    ,{"className":"div_items", "top":"81","left":"315","width":"200","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Among other responsibilities the Mail Room:" 
    }}
  
    ,{"className":"div_items", "top":"94","left":"333","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"95","left":"351","width":"107","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Opens all incoming mail" 
    }}
  
    ,{"className":"div_items", "top":"107","left":"333","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"108","left":"351","width":"112","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Purchases office supplies" 
    }}
  
    ,{"className":"div_items", "top":"121","left":"333","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"122","left":"351","width":"196","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Handles all dealer request for contracts and" 
    }}
  
    ,{"className":"div_items", "top":"136","left":"351","width":"45","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"brochures" 
    }}
  
    ,{"className":"div_items", "top":"148","left":"333","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"149","left":"351","width":"172","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Handles all outgoing mail and shipping" 
    }}
  
    ,{"className":"div_items", "top":"162","left":"333","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"163","left":"351","width":"102","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Performs general filing" 
    }}
  
    ,{"className":"div_items", "top":"176","left":"333","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"177","left":"351","width":"142","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Handles file search and seizures" 
    }}
  
    ,{"className":"div_items", "top":"190","left":"333","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"191","left":"351","width":"128","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Maintains Company vehicles" 
    }}
  
    ,{"className":"div_items", "top":"214","left":"315","width":"56","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font10","text":"Operations" 
    }}
  
    ,{"className":"div_items", "top":"227","left":"315","width":"184","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Among other responsibilities Operations:" 
    }}
  
    ,{"className":"div_items", "top":"240","left":"333","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"241","left":"351","width":"112","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Handles all alarm activity" 
    }}
  
    ,{"className":"div_items", "top":"253","left":"333","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"254","left":"351","width":"116","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Maintains alarm receivers" 
    }}
  
    ,{"className":"div_items", "top":"267","left":"333","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"268","left":"351","width":"201","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Assists subscriber/dealers with alarm related" 
    }}
  
    ,{"className":"div_items", "top":"282","left":"351","width":"53","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"information" 
    }}
  
    ,{"className":"div_items", "top":"305","left":"315","width":"91","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font10","text":"Sales & Marketing" 
    }}
  
    ,{"className":"div_items", "top":"318","left":"315","width":"242","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Among other responsibilities the Sales and Marketing:" 
    }}
  
    ,{"className":"div_items", "top":"331","left":"333","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"332","left":"351","width":"189","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Is responsible for advertising, promotions," 
    }}
  
    ,{"className":"div_items", "top":"345","left":"351","width":"161","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"marketing, and trade show inquiries" 
    }}
  
    ,{"className":"div_items", "top":"358","left":"333","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"359","left":"351","width":"139","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Follows up on new dealer sales" 
    }}
  
    ,{"className":"div_items", "top":"372","left":"333","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"373","left":"351","width":"177","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Handles sales-related issues for existing" 
    }}
  
    ,{"className":"div_items", "top":"386","left":"351","width":"47","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"customers" 
    }}
  
    ,{"className":"div_items", "top":"409","left":"315","width":"59","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font10","text":"Technology" 
    }}
  
    ,{"className":"div_items", "top":"422","left":"315","width":"200","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Among other responsibilities the Technology" 
    }}
  
    ,{"className":"div_items", "top":"436","left":"315","width":"58","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Department:" 
    }}
  
    ,{"className":"div_items", "top":"448","left":"333","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"449","left":"351","width":"178","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Maintains all computers, terminals, and" 
    }}
  
    ,{"className":"div_items", "top":"463","left":"351","width":"35","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"printers" 
    }}
  
    ,{"className":"div_items", "top":"476","left":"333","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"477","left":"351","width":"181","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Maintains the computerized automation" 
    }}
  
    ,{"className":"div_items", "top":"490","left":"351","width":"32","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"system" 
    }}
  
    ,{"className":"div_items", "top":"503","left":"333","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"504","left":"351","width":"99","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Maintains phone lines" 
    }}
  
    ,{"className":"div_items", "top":"517","left":"333","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"518","left":"351","width":"85","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Maintains MPower" 
    }}
  
    ,{"className":"div_items", "top":"530","left":"333","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"531","left":"351","width":"134","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Maintains COP-A-FAX / EMAIL" 
    }}
  
    ,{"className":"div_items", "top":"544","left":"333","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"545","left":"351","width":"176","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Processes special request for computer" 
    }}
  
    ,{"className":"div_items", "top":"558","left":"351","width":"33","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"reports" 
    }}
  
    ,{"className":"div_items", "top":"571","left":"333","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"572","left":"351","width":"135","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Maintains receiver equipment" 
    }}
  
    ,{"className":"div_items", "top":"585","left":"333","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"586","left":"351","width":"171","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Assists dealer with technical problems" 
    }}
  
    ,{"className":"div_items", "top":"599","left":"333","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"600","left":"351","width":"175","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Provides technical training and support" 
    }}
  
    ,{"className":"div_items", "top":"612","left":"333","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"613","left":"351","width":"83","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Maintains Building" 
    }}
      
    ]
})
 	