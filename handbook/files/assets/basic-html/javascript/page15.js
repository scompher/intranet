﻿
    pageContentLoad(
    {
    "currentPage":{"item":"page15", "num":"15"},
    "keywords":"C.O.P.S., Employee, Handbook, September, 2013:, Page, 14, of, 22, Downloaded, or, printed, pages, may, be, outdated., If, you, downloaded, this, page,, please, check, the, intranet, to, ensure, have, most, recent, version., , Job, Protection, Upon, Return, Work:, With, limited, exceptions,, any, eligible, employee, who, takes, leave, is, entitled, restored, his, her, old, job, an, equivalent, position, with, pay,, benefits,, and, other, terms, conditions, employment., No, employment, benefits, that, accrued, before, date, will, lost., However,, not, accrual, seniority, would, occurred, during, period, leave., Finally,, FMLA, provides, employer, deny, restoration, certain, “highly, compensated, employees.”, Further, information, concerning, exception, in, general, obtained, from, Department, Manager., Leave, for, Maternity, This, policy, only, available, when, a, location, does, qualify, guidelines, per, federal, state, requirements., A, pregnant, has, worked, Company, minimum, 12, months, 1,250, hours, preceding, utilize, Monitoring, Leave., provides:, 4, weeks, unpaid, birth, child, 6, after, vaginal, (a, total, 10, leave), 8, Cesarean, leave)., An, returns, maternity, same, The, must, request, human, resources, on, approved, form., President, approve, Employment, terminated, period., Absence, points, by, continue, pay, insurance, premiums, were, paid, prior, forfeit, right, receive, wellness, bonus, and/or, incentive, if, applicable,, (other, than, earned, commissions, sales, staff), month(s), return, ID, badge, Corby, required, exhaust, all, vacation, time, unless, prohibited, guidelines., Any, runs, concurrently, Vacation/Holiday, Benefits, Vacation, Policy, —, All, Full-Time, Employees, After, one, (1), year, full-time, 75, vacation., AFTER, five, (5), years, 112.5, written, your, supervisor, submitted, at, least, 30, days, requested, time., writing., Otherwise, should, consider, denied., Vacations, are, permitted, between, December, January, 2, those, employees, tenure, less, 5, 22., more, (as, 22), period,, subject, normal, herein., As, request,, it, granted, first, come,, serve, basis., two, off, frame,, then, based, seniority., week, consecutive, years.",
    "fullLink":"../../../index.html",
    "height":"792",
    "width":" 612",
    "isWide":"False",
    "bookWidth":"612",
    "bookHeight":"792",

    "download":[{}
      ,{"pdfPublication":{"url":"../common/downloads_c167bf08/COPS_BULLETin.pdf", "size":"1.16 MB"}}
    
      ,{"PdfPage":{"url":"../common/downloads_c167bf08/page0015.pdf", "size":"323.99 KB"}}
     ],

    
    
    "substrate":{"background":"url(../common/page-substrates/page0015.png)",
    "backgroundSize":"contain", "backgroundColor":"#FFFFFF"},
    
    "leftTool":{"innerText":"14","src":"page14.html"},
    
    "rightTool":{"innerText":" 16","src":"page16.html"},
    
    "content":[{}
    
    ,{"className":"div_items", "top":"759","left":"181","width":"214","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font1","text":"C.O.P.S. Employee Handbook September 2013:  Page" 
    }}
  
    ,{"className":"div_items", "top":"759","left":"398","width":"10","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font2","text":"14" 
    }}
  
    ,{"className":"div_items", "top":"759","left":"410","width":"8","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font1","text":"of" 
    }}
  
    ,{"className":"div_items", "top":"759","left":"421","width":"10","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font2","text":"22" 
    }}
  
    ,{"className":"div_items", "top":"770","left":"36","width":"545","height":"10","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font3","text":"Downloaded or printed pages may be outdated.  If you printed or downloaded this page, please check the C.O.P.S. intranet to ensure you have the most recent version." 
    }}
  
    ,{"className":"div_items", "top":"33","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"34","left":"90","width":"171","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font9","text":"Job Protection Upon Return to Work:" 
    }}
  
    ,{"className":"div_items", "top":"34","left":"264","width":"294","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"With limited exceptions, any eligible employee who takes leave is" 
    }}
  
    ,{"className":"div_items", "top":"47","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"entitled to be restored to his or her old job or to an equivalent position with equivalent pay, benefits," 
    }}
  
    ,{"className":"div_items", "top":"61","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"and other terms and conditions of employment.  No employment benefits that accrued before the date" 
    }}
  
    ,{"className":"div_items", "top":"74","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"of leave will be lost.  However, an employee is not entitled to an accrual of any seniority or employment" 
    }}
  
    ,{"className":"div_items", "top":"88","left":"90","width":"275","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"benefits that would have occurred during the period of leave." 
    }}
  
    ,{"className":"div_items", "top":"113","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Finally, FMLA provides that an employer may deny job restoration to certain “highly compensated employees.”" 
    }}
  
    ,{"className":"div_items", "top":"126","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Further information concerning this exception and FMLA in general may be obtained from the Department" 
    }}
  
    ,{"className":"div_items", "top":"140","left":"54","width":"43","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Manager." 
    }}
  
    ,{"className":"div_items", "top":"163","left":"54","width":"99","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font10","text":"Leave for Maternity" 
    }}
  
    ,{"className":"div_items", "top":"176","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"This policy is only available when a location does not qualify for FMLA guidelines per federal and state" 
    }}
  
    ,{"className":"div_items", "top":"190","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"requirements.  A pregnant employee that has worked for the Company a minimum of 12 months and has" 
    }}
  
    ,{"className":"div_items", "top":"203","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"worked a minimum of 1,250 hours in the preceding 12 months is eligible to utilize C.O.P.S. Monitoring Maternity" 
    }}
  
    ,{"className":"div_items", "top":"217","left":"54","width":"29","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Leave." 
    }}
  
    ,{"className":"div_items", "top":"240","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"This leave provides:  4 weeks unpaid leave before the birth of the child and 6 weeks unpaid leave after the" 
    }}
  
    ,{"className":"div_items", "top":"253","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"vaginal birth of the child (a total of 10 weeks unpaid leave) or 8 weeks unpaid leave after the Cesarean birth of" 
    }}
  
    ,{"className":"div_items", "top":"267","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"the child (a total of 12 weeks unpaid leave).  An employee who returns from maternity leave is entitled to be" 
    }}
  
    ,{"className":"div_items", "top":"280","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"restored to the same or equivalent job with equivalent pay, benefits and other terms and conditions of" 
    }}
  
    ,{"className":"div_items", "top":"294","left":"54","width":"60","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"employment." 
    }}
  
    ,{"className":"div_items", "top":"316","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"317","left":"90","width":"387","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"The employee must request leave from human resources on a C.O.P.S. approved form." 
    }}
  
    ,{"className":"div_items", "top":"330","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"331","left":"90","width":"249","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"The Company President must approve Maternity Leave." 
    }}
  
    ,{"className":"div_items", "top":"344","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"345","left":"90","width":"222","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Employment is not terminated during this period." 
    }}
  
    ,{"className":"div_items", "top":"358","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"359","left":"90","width":"318","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Absence points will not be accrued by the employee during this period." 
    }}
  
    ,{"className":"div_items", "top":"371","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"372","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"The employee must continue to pay any insurance premiums that were paid by the employee prior to" 
    }}
  
    ,{"className":"div_items", "top":"386","left":"90","width":"93","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"the Maternity Leave." 
    }}
  
    ,{"className":"div_items", "top":"399","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"400","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"The employee is not entitled to an accrual of any seniority or employment benefits that would have" 
    }}
  
    ,{"className":"div_items", "top":"413","left":"90","width":"161","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"occurred during the period of leave." 
    }}
  
    ,{"className":"div_items", "top":"426","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"427","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Employee will forfeit the right to receive her wellness bonus and/or any other incentive pay, if" 
    }}
  
    ,{"className":"div_items", "top":"440","left":"90","width":"461","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"applicable, (other than earned commissions for sales staff) during the month(s) of the Maternity Leave." 
    }}
  
    ,{"className":"div_items", "top":"453","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"454","left":"90","width":"311","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Employee must return her ID badge and Corby during period of leave." 
    }}
  
    ,{"className":"div_items", "top":"477","left":"54","width":"502","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Employee is required to exhaust all accrued vacation time during the leave unless prohibited by state guidelines." 
    }}
  
    ,{"className":"div_items", "top":"491","left":"54","width":"337","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Any accrued paid vacation time runs concurrently with the maternity leave." 
    }}
  
    ,{"className":"div_items", "top":"520","left":"221","width":"170","height":"16","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font8","text":"Vacation/Holiday Benefits" 
    }}
  
    ,{"className":"div_items", "top":"546","left":"54","width":"216","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font10","text":"Vacation Policy — All Full-Time Employees" 
    }}
  
    ,{"className":"div_items", "top":"559","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"560","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"After one (1) year of full-time employment you will receive 75 hours paid vacation.  AFTER five (5) years" 
    }}
  
    ,{"className":"div_items", "top":"573","left":"90","width":"302","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"of full-time employment you will receive 112.5 hours paid vacation." 
    }}
  
    ,{"className":"div_items", "top":"586","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"587","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"A written vacation request to your supervisor must be submitted at least 30 days prior to your" 
    }}
  
    ,{"className":"div_items", "top":"600","left":"90","width":"112","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"requested vacation time." 
    }}
  
    ,{"className":"div_items", "top":"613","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"614","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"The request must be approved by your supervisor in writing.  Otherwise you should consider your" 
    }}
  
    ,{"className":"div_items", "top":"628","left":"90","width":"126","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"request for vacation denied." 
    }}
  
    ,{"className":"div_items", "top":"640","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"641","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Vacations are not permitted between December 22 and January 2 for those employees with tenure of" 
    }}
  
    ,{"className":"div_items", "top":"655","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"less than 5 years on December 22.  Employees with tenure of 5 years or more (as of December 22) may" 
    }}
  
    ,{"className":"div_items", "top":"668","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"request vacation time during this period, subject to the guidelines of the normal vacation request policy" 
    }}
  
    ,{"className":"div_items", "top":"682","left":"90","width":"32","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"herein." 
    }}
  
    ,{"className":"div_items", "top":"694","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"695","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"As with any vacation request, it will be granted on a first come, first serve basis.  If two or more" 
    }}
  
    ,{"className":"div_items", "top":"709","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"employees request vacation time off during the same time frame, it will then be granted based on" 
    }}
  
    ,{"className":"div_items", "top":"722","left":"90","width":"370","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"seniority.  An employee may or may not be granted this week in consecutive years." 
    }}
      
    ]
})
 	