﻿
    pageContentLoad(
    {
    "currentPage":{"item":"page20", "num":"20"},
    "keywords":"C.O.P.S., Employee, Handbook, September, 2013:, Page, 19, of, 22, Downloaded, or, printed, pages, may, be, outdated., If, you, downloaded, this, page,, please, check, the, intranet, to, ensure, have, most, recent, version., A., PERSONAL, POSTINGS, , Personal, Internet, postings, should, not, disclose, any, information, that, is, confidential, proprietary, third, party, has, disclosed, C.O.P.S.., make, respond, comments, on, aspect, C.O.P.S.’s, business, policy, issue, in, which, involved, and, responsibility,, must, clearly, identify, yourself, as, a, employee, your, blog, site(s), include, following, disclaimer:, “the, views, expressed, post, are, mine, do, necessarily, reflect, C.O.P.S.”, In, addition,, employees, circulate, they, know, written, by, other, without, permission, informing, recipient, author, posting, employee., definition, personal, point, view,, view, However,, it, important, communicate, manner, disrespectful, demeaning., Because, legally, responsible, for, postings,, subject, liability, if, posts, found, defamatory,, harassing,, violation, applicable, law., You, also, liable, copyrighted, (music,, videos,, text,, etc.), belonging, parties., All, above, mentioned, prohibited, under, policy., When, neither, claim, nor, imply, speaking, behalf,, unless, authorized, writing, Executive, Director, so., posting,, refer, work, done, provide, link, website,, required, disclaimer, reasonably, prominent, place:, C.O.P.S..”, Your, internet, logos, trademarks,, respect, copyright,, privacy,, fair, use,, financial, disclosure,, laws., Managers, who, about, online, special, responsibility., By, virtue, their, position,, statements, misperceived, expressions, policies, opinions, even, with, disclaimer., As, such,, managers, thoughtful, consider, whether, thoughts, misconstrued, position., B., BLOGS, Blogs, blogs, requiring, corporate, approval, our, industry., Only, company’s, logo., links, back, web, destinations., legal, stating, all, author,, guest, visitors, those, organization., Respect, disclosure, audience., Never, use, ethnic, slurs,, insults,, obscenity,, etc.,, show, proper, consideration, individual, privacy, topics, considered, objectionable, inflammatory., Correcting, mistakes, —, Be, first, correct, own, mistakes,, don’t, alter, previous, indicating, Posting, high, quality, basic, spellchecking., Ensure, blogging, activity, does, interfere, commitments., forming, sponsored, blog,, obtain, pre-approval, from, President., Every, effort, made, e-mail, address., Please, contact, HR, details, obtaining, an, Any, complaints, received, site, referred, supervisor, disposition., request, avoid, certain, subjects, withdraw, Blog, believes, doing, so, will, help, compliance, laws,, including, securities, regulations., reserves, right, remove, posted, comment, deemed, sole, discretion, inappropriate, topic, discussed, uses, language., particular, communications, Blog.",
    "fullLink":"../../../index.html",
    "height":"792",
    "width":" 612",
    "isWide":"False",
    "bookWidth":"612",
    "bookHeight":"792",

    "download":[{}
      ,{"pdfPublication":{"url":"../common/downloads_c167bf08/COPS_BULLETin.pdf", "size":"1.16 MB"}}
    
      ,{"PdfPage":{"url":"../common/downloads_c167bf08/page0020.pdf", "size":"218.35 KB"}}
     ],

    
    
    "substrate":{"background":"url(../common/page-substrates/page0020.png)",
    "backgroundSize":"contain", "backgroundColor":"#FFFFFF"},
    
    "leftTool":{"innerText":"19","src":"page19.html"},
    
    "rightTool":{"innerText":" 21","src":"page21.html"},
    
    "content":[{}
    
    ,{"className":"div_items", "top":"759","left":"181","width":"214","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font1","text":"C.O.P.S. Employee Handbook September 2013:  Page" 
    }}
  
    ,{"className":"div_items", "top":"759","left":"398","width":"10","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font2","text":"19" 
    }}
  
    ,{"className":"div_items", "top":"759","left":"410","width":"8","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font1","text":"of" 
    }}
  
    ,{"className":"div_items", "top":"759","left":"421","width":"10","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font2","text":"22" 
    }}
  
    ,{"className":"div_items", "top":"770","left":"36","width":"545","height":"10","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font3","text":"Downloaded or printed pages may be outdated.  If you printed or downloaded this page, please check the C.O.P.S. intranet to ensure you have the most recent version." 
    }}
  
    ,{"className":"div_items", "top":"34","left":"72","width":"10","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font9","text":"A." 
    }}
  
    ,{"className":"div_items", "top":"34","left":"90","width":"98","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font9","text":"PERSONAL POSTINGS" 
    }}
  
    ,{"className":"div_items", "top":"46","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"47","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Personal Internet postings should not disclose any information that is confidential or proprietary to" 
    }}
  
    ,{"className":"div_items", "top":"61","left":"90","width":"318","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"C.O.P.S. or to any third party that has disclosed information to C.O.P.S.." 
    }}
  
    ,{"className":"div_items", "top":"74","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"75","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"If you make or respond to comments on any aspect of C.O.P.S.’s business or any policy issue in which" 
    }}
  
    ,{"className":"div_items", "top":"88","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"C.O.P.S. is involved and in which you have responsibility, you must clearly identify yourself as a C.O.P.S." 
    }}
  
    ,{"className":"div_items", "top":"101","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"employee in your postings or blog site(s) and include the following disclaimer: “the views expressed on" 
    }}
  
    ,{"className":"div_items", "top":"115","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"this post are mine and do not necessarily reflect the views of C.O.P.S.” In addition, C.O.P.S. employees" 
    }}
  
    ,{"className":"div_items", "top":"128","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"should not circulate postings they know are written by other C.O.P.S. employees without permission and" 
    }}
  
    ,{"className":"div_items", "top":"142","left":"90","width":"381","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"without informing the recipient that the author of the posting is a C.O.P.S. employee." 
    }}
  
    ,{"className":"div_items", "top":"154","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"155","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Personal Internet postings by definition reflect your personal point of view, not necessarily the point of" 
    }}
  
    ,{"className":"div_items", "top":"169","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"view of C.O.P.S. However, it is important to communicate in a manner that is not disrespectful or" 
    }}
  
    ,{"className":"div_items", "top":"182","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"demeaning.  Because you are legally responsible for your postings, you may be subject to liability if your" 
    }}
  
    ,{"className":"div_items", "top":"196","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"posts are found defamatory, harassing, or in violation of any other applicable law.  You may also be" 
    }}
  
    ,{"className":"div_items", "top":"209","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"liable if you make postings which include confidential or copyrighted information (music, videos, text," 
    }}
  
    ,{"className":"div_items", "top":"222","left":"90","width":"452","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"etc.) belonging to third parties.  All of the above mentioned postings are prohibited under this policy." 
    }}
  
    ,{"className":"div_items", "top":"235","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"236","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"When posting your point of view, you should neither claim nor imply you are speaking on C.O.P.S.’s" 
    }}
  
    ,{"className":"div_items", "top":"250","left":"90","width":"385","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"behalf, unless you are authorized in writing by a C.O.P.S.’s Executive Director to do so." 
    }}
  
    ,{"className":"div_items", "top":"262","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"263","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"If you identify yourself as a C.O.P.S. employee on any Internet posting, refer to the work done by" 
    }}
  
    ,{"className":"div_items", "top":"277","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"C.O.P.S. or provide a link on a C.O.P.S. website, you are required to include the following disclaimer in a" 
    }}
  
    ,{"className":"div_items", "top":"290","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"reasonably prominent place: “the views expressed on this post are mine and do not necessarily reflect" 
    }}
  
    ,{"className":"div_items", "top":"304","left":"90","width":"99","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"the views of C.O.P.S..”" 
    }}
  
    ,{"className":"div_items", "top":"316","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"317","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Your personal internet postings may not include C.O.P.S.’s logos or trademarks, and should respect" 
    }}
  
    ,{"className":"div_items", "top":"331","left":"90","width":"331","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"copyright, privacy, fair use, financial disclosure, and other applicable laws." 
    }}
  
    ,{"className":"div_items", "top":"344","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"345","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Managers who post information about C.O.P.S. online have a special responsibility.  By virtue of their" 
    }}
  
    ,{"className":"div_items", "top":"358","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"position, statements they make about C.O.P.S. may be misperceived as expressions of C.O.P.S.’s policies" 
    }}
  
    ,{"className":"div_items", "top":"371","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"or opinions even with a disclaimer.  As such, managers should be thoughtful and consider whether their" 
    }}
  
    ,{"className":"div_items", "top":"385","left":"90","width":"336","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"personal thoughts may be misconstrued as statements of C.O.P.S. position." 
    }}
  
    ,{"className":"div_items", "top":"404","left":"72","width":"9","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font9","text":"B." 
    }}
  
    ,{"className":"div_items", "top":"404","left":"90","width":"69","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font9","text":"C.O.P.S. BLOGS" 
    }}
  
    ,{"className":"div_items", "top":"417","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"418","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"C.O.P.S. Blogs are blogs requiring corporate approval in which employees may blog about C.O.P.S. and" 
    }}
  
    ,{"className":"div_items", "top":"431","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"our industry.  Only C.O.P.S. Blogs may include the company’s logo.  C.O.P.S. Blogs may also include links" 
    }}
  
    ,{"className":"div_items", "top":"445","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"back to C.O.P.S. web destinations.  All C.O.P.S. Blogs must include a legal disclaimer stating that all posts" 
    }}
  
    ,{"className":"div_items", "top":"458","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"by the author, guest author and visitors reflect personal thoughts and opinions which are not necessarily" 
    }}
  
    ,{"className":"div_items", "top":"472","left":"90","width":"115","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"those of the organization." 
    }}
  
    ,{"className":"div_items", "top":"485","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"486","left":"90","width":"266","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Respect all copyright, fair use, and financial disclosure laws." 
    }}
  
    ,{"className":"div_items", "top":"498","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"499","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Respect your audience.  Never use ethnic slurs, personal insults, obscenity, etc., and show proper" 
    }}
  
    ,{"className":"div_items", "top":"513","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"consideration for individual privacy and for topics that may be considered objectionable or" 
    }}
  
    ,{"className":"div_items", "top":"526","left":"90","width":"64","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"inflammatory." 
    }}
  
    ,{"className":"div_items", "top":"539","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"540","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Correcting mistakes — Be the first to correct your own mistakes, and don’t alter previous posts without" 
    }}
  
    ,{"className":"div_items", "top":"553","left":"90","width":"148","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"indicating that you have done so." 
    }}
  
    ,{"className":"div_items", "top":"566","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"567","left":"90","width":"278","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Posting should be high quality and include basic spellchecking." 
    }}
  
    ,{"className":"div_items", "top":"580","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"581","left":"90","width":"348","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Ensure that blogging activity does not interfere with your work commitments." 
    }}
  
    ,{"className":"div_items", "top":"604","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"In forming any C.O.P.S. sponsored blog, you must obtain pre-approval from the President.  Every effort should" 
    }}
  
    ,{"className":"div_items", "top":"618","left":"54","width":"486","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"be made to use a C.O.P.S. e-mail address.  Please contact the HR Director for details on obtaining an address." 
    }}
  
    ,{"className":"div_items", "top":"641","left":"54","width":"461","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Any complaints received on a C.O.P.S. sponsored site should be referred to a supervisor for disposition." 
    }}
  
    ,{"className":"div_items", "top":"665","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"C.O.P.S. may request that you avoid certain subjects or withdraw certain posts from a C.O.P.S. Blog if it believes" 
    }}
  
    ,{"className":"div_items", "top":"678","left":"54","width":"425","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"that doing so will help ensure compliance with applicable laws, including securities regulations." 
    }}
  
    ,{"className":"div_items", "top":"701","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"C.O.P.S. reserves the right to remove any posted comment on C.O.P.S. Blog site(s) that is deemed in C.O.P.S.’s" 
    }}
  
    ,{"className":"div_items", "top":"715","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"sole discretion to be inappropriate for the topic discussed or uses inappropriate language.  C.O.P.S. also reserves" 
    }}
  
    ,{"className":"div_items", "top":"728","left":"54","width":"279","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"the right to post particular communications on a C.O.P.S. Blog." 
    }}
      
    ]
})
 	