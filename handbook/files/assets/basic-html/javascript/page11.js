﻿
    pageContentLoad(
    {
    "currentPage":{"item":"page11", "num":"11"},
    "keywords":"C.O.P.S., Employee, Handbook, September, 2013:, Page, 10, of, 22, Downloaded, or, printed, pages, may, be, outdated., If, you, downloaded, this, page,, please, check, the, intranet, to, ensure, have, most, recent, version., Distribution, Paychecks, , Please, with, Human, Resource, Department, get, a, listing, scheduled, paydays., All, payroll, is, processed, in, NJ, facility., Employees, that, do, not, work, at, facility, will, receive, their, paychecks, via, US, Postal, Service, through, direct, deposit., Direct, deposit, encouraged, for, all, employees, regardless, location, they, convenience., holiday, falls, on, payday,, distributed, following, business, day., deposits, also, delayed, one, day, holidays., Paycheck, errors, should, reported, your, supervisor, once, and, corrected, next, paycheck., Performance, Review, General, Office, You, two, performance, reviews, year,, mid-term, second, an, annual, review., Any, applicable, pay, increase, determined, during, Operations, either, per, evaluated, company’s, Gradation, program., Those, see, Manager, help, them, review, Booklet, more, details., Applicable, raises, effective, period, evaluation, date., Resignations, To, leave, employment, Company, good, standing,, we, request, give, least, (2), weeks’, notice., Prior, leaving,, requested, meet, Management, exit, interview., considerate, co-workers, appropriate, employee, resigns, quits, employment,, Monitoring, policy, allows, re-hire, eligibility, after, years, separation.",
    "fullLink":"../../../index.html",
    "height":"792",
    "width":" 612",
    "isWide":"False",
    "bookWidth":"612",
    "bookHeight":"792",

    "download":[{}
      ,{"pdfPublication":{"url":"../common/downloads_c167bf08/COPS_BULLETin.pdf", "size":"1.16 MB"}}
    
      ,{"PdfPage":{"url":"../common/downloads_c167bf08/page0011.pdf", "size":"357.21 KB"}}
     ],

    
    
    "substrate":{"background":"url(../common/page-substrates/page0011.png)",
    "backgroundSize":"contain", "backgroundColor":"#FFFFFF"},
    
    "leftTool":{"innerText":"10","src":"page10.html"},
    
    "rightTool":{"innerText":" 12","src":"page12.html"},
    
    "content":[{}
    
    ,{"className":"div_items", "top":"759","left":"181","width":"214","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font1","text":"C.O.P.S. Employee Handbook September 2013:  Page" 
    }}
  
    ,{"className":"div_items", "top":"759","left":"398","width":"10","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font2","text":"10" 
    }}
  
    ,{"className":"div_items", "top":"759","left":"410","width":"8","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font1","text":"of" 
    }}
  
    ,{"className":"div_items", "top":"759","left":"421","width":"10","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font2","text":"22" 
    }}
  
    ,{"className":"div_items", "top":"770","left":"36","width":"545","height":"10","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font3","text":"Downloaded or printed pages may be outdated.  If you printed or downloaded this page, please check the C.O.P.S. intranet to ensure you have the most recent version." 
    }}
  
    ,{"className":"div_items", "top":"40","left":"223","width":"165","height":"16","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font8","text":"Distribution of Paychecks" 
    }}
  
    ,{"className":"div_items", "top":"61","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"62","left":"90","width":"399","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Please check with the Human Resource Department to get a listing of scheduled paydays." 
    }}
  
    ,{"className":"div_items", "top":"75","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"76","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"All payroll is processed in the NJ facility.  Employees that do not work at the NJ facility will receive their" 
    }}
  
    ,{"className":"div_items", "top":"89","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"paychecks via US Postal Service or through direct deposit.  Direct deposit is encouraged for all" 
    }}
  
    ,{"className":"div_items", "top":"103","left":"90","width":"321","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"employees regardless of the location that they work in for convenience." 
    }}
  
    ,{"className":"div_items", "top":"116","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"117","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"If a holiday falls on a payday, paychecks will be distributed the following business day.  Direct deposits" 
    }}
  
    ,{"className":"div_items", "top":"130","left":"90","width":"226","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"may also be delayed one business day on holidays." 
    }}
  
    ,{"className":"div_items", "top":"143","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"144","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Paycheck errors should be reported to your supervisor at once and will be corrected in your next" 
    }}
  
    ,{"className":"div_items", "top":"157","left":"90","width":"44","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"paycheck." 
    }}
  
    ,{"className":"div_items", "top":"187","left":"206","width":"201","height":"16","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font8","text":"Employee Performance Review" 
    }}
  
    ,{"className":"div_items", "top":"213","left":"54","width":"129","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font10","text":"General Office Employees" 
    }}
  
    ,{"className":"div_items", "top":"226","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"You will receive two performance reviews a year, one is a mid-term and the second will be an annual review." 
    }}
  
    ,{"className":"div_items", "top":"240","left":"54","width":"327","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Any applicable pay increase will be determined during the annual review." 
    }}
  
    ,{"className":"div_items", "top":"263","left":"54","width":"114","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font10","text":"Operations Employees" 
    }}
  
    ,{"className":"div_items", "top":"276","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Operations employees will receive either two performance reviews per year, or be evaluated through the" 
    }}
  
    ,{"className":"div_items", "top":"289","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"company’s Gradation program.  Those employees evaluated through Gradation may see their Manager to help" 
    }}
  
    ,{"className":"div_items", "top":"303","left":"54","width":"235","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"them review the Gradation Booklet for more details." 
    }}
  
    ,{"className":"div_items", "top":"327","left":"54","width":"459","height":"11","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font12","text":"Applicable raises for all employees will be effective on the next pay period following the evaluation date." 
    }}
  
    ,{"className":"div_items", "top":"356","left":"264","width":"83","height":"16","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font8","text":"Resignations" 
    }}
  
    ,{"className":"div_items", "top":"377","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"378","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"To leave employment with the Company in good standing, we request that you give at least two (2)" 
    }}
  
    ,{"className":"div_items", "top":"391","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"weeks’ notice.  Prior to leaving, you may be requested to meet with Management for an exit interview." 
    }}
  
    ,{"className":"div_items", "top":"405","left":"90","width":"313","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Please be considerate of your co-workers and give appropriate notice." 
    }}
  
    ,{"className":"div_items", "top":"418","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"419","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"If an employee resigns or quits employment, C.O.P.S. Monitoring policy allows for re-hire eligibility after" 
    }}
  
    ,{"className":"div_items", "top":"432","left":"90","width":"108","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"two years of separation." 
    }}
      
    ]
})
 	