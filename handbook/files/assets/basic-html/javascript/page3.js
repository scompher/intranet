﻿
    pageContentLoad(
    {
    "currentPage":{"item":"page3", "num":"3"},
    "keywords":"C.O.P.S., Employee, Handbook, September, 2013:, Page, 2, of, 22, Downloaded, or, printed, pages, may, be, outdated., If, you, downloaded, this, page,, please, check, the, intranet, to, ensure, have, most, recent, version., The, President’s, Message, Welcome, Monitoring., We’re, glad, with, our, company., As, President,, I, would, like, one, first, congratulate, on, your, new, position., will, soon, see,, Monitoring, can, a, very, exciting, place, work., environment, fast, paced, and, challenging., Our, Mission, Statement, should, help, realize, vision., encourage, set, both, personal, professional, goals, high., great, confidence, in, ability, achieve, them,, while, using, good, judgment., Looking, forward, meeting, you,, James, McMullen, OUR, MISSION, STATEMENT, “To, industry, leader, providing, highest, quality, alarm, monitoring, services, through, caring, professionals, supported, by, leading, edge, technology.”",
    "fullLink":"../../../index.html",
    "height":"792",
    "width":" 612",
    "isWide":"False",
    "bookWidth":"612",
    "bookHeight":"792",

    "download":[{}
      ,{"pdfPublication":{"url":"../common/downloads_c167bf08/COPS_BULLETin.pdf", "size":"1.16 MB"}}
    
      ,{"PdfPage":{"url":"../common/downloads_c167bf08/page0003.pdf", "size":"563.93 KB"}}
     ],

    
    
    "substrate":{"background":"url(../common/page-substrates/page0003.png)",
    "backgroundSize":"contain", "backgroundColor":"#FFFFFF"},
    
    "leftTool":{"innerText":"2","src":"page2.html"},
    
    "rightTool":{"innerText":" 4","src":"page4.html"},
    
    "content":[{}
    
    ,{"className":"div_items", "top":"759","left":"183","width":"214","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font1","text":"C.O.P.S. Employee Handbook September 2013:  Page" 
    }}
  
    ,{"className":"div_items", "top":"759","left":"400","width":"5","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font2","text":"2" 
    }}
  
    ,{"className":"div_items", "top":"759","left":"408","width":"8","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font1","text":"of" 
    }}
  
    ,{"className":"div_items", "top":"759","left":"418","width":"10","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font2","text":"22" 
    }}
  
    ,{"className":"div_items", "top":"770","left":"36","width":"545","height":"10","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font3","text":"Downloaded or printed pages may be outdated.  If you printed or downloaded this page, please check the C.O.P.S. intranet to ensure you have the most recent version." 
    }}
  
    ,{"className":"div_items", "top":"119","left":"193","width":"225","height":"22","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font4","text":"The President’s Message" 
    }}
  
    ,{"className":"div_items", "top":"192","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font31","text":"Welcome to C.O.P.S. Monitoring.  We’re glad to have you with our company.  As President, I would like to be one" 
    }}
  
    ,{"className":"div_items", "top":"212","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font31","text":"of the first to congratulate you on your new position.  As you will soon see, C.O.P.S. Monitoring can be a very" 
    }}
  
    ,{"className":"div_items", "top":"232","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font31","text":"exciting place to work.  The environment can be fast paced and challenging.  Our Mission Statement should help" 
    }}
  
    ,{"className":"div_items", "top":"252","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font31","text":"you realize our vision.  I encourage you to set both your personal and professional goals high. I have great" 
    }}
  
    ,{"className":"div_items", "top":"272","left":"54","width":"314","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font31","text":"confidence in your ability to achieve them, while using good judgment." 
    }}
  
    ,{"className":"div_items", "top":"306","left":"233","width":"145","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font31","text":"Looking forward to meeting you," 
    }}
  
    ,{"className":"div_items", "top":"360","left":"269","width":"75","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font15","text":"James McMullen" 
    }}
  
    ,{"className":"div_items", "top":"374","left":"239","width":"134","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font31","text":"President, C.O.P.S. Monitoring" 
    }}
  
    ,{"className":"div_items", "top":"497","left":"244","width":"133","height":"14","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font32","text":"OUR MISSION STATEMENT" 
    }}
  
    ,{"className":"div_items", "top":"525","left":"85","width":"449","height":"14","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font33","text":"“To be the industry leader in providing the highest quality alarm monitoring services through" 
    }}
  
    ,{"className":"div_items", "top":"543","left":"148","width":"324","height":"14","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font33","text":"caring professionals supported by the leading edge of technology.”" 
    }}
      
    ]
})
 	