﻿
    pageContentLoad(
    {
    "currentPage":{"item":"page6", "num":"6"},
    "keywords":"C.O.P.S., Employee, Handbook, September, 2013:, Page, 5, of, 22, Downloaded, or, printed, pages, may, be, outdated., If, you, downloaded, this, page,, please, check, the, intranet, to, ensure, have, most, recent, version., Building, Access, and, Parking, , is, provided, for, employees, in, employee, designated, spots,, unless, otherwise, specified., Employees, should, enter/exit, building, through, entrance,, otherwise., To, enter,, hold, your, access, control, device, up, reader, next, door., The, door, can, then, opened, if, are, authorized, enter, that, All, visitors, required, wear, a, company, issued, identification, badge, clearly, visible, others, whenever, facility., ID, badges, will, also, serve, as, time, card, hourly, employees., DO, NOT, allow, anyone, any, access-controlled, they, demonstrated, possess, valid, badge., security, safety, responsibility, everyone., Handicapped, Handicap, parking, available, at, all, facilities., bathrooms, C.O.P.S, Lockers, Where, available,, locker, on, their, first, day, employment., You, supplied, with, confidential, combination., This, property, subject, search, company’s, discretion.",
    "fullLink":"../../../index.html",
    "height":"792",
    "width":" 612",
    "isWide":"False",
    "bookWidth":"612",
    "bookHeight":"792",

    "download":[{}
      ,{"pdfPublication":{"url":"../common/downloads_c167bf08/COPS_BULLETin.pdf", "size":"1.16 MB"}}
    
      ,{"PdfPage":{"url":"../common/downloads_c167bf08/page0006.pdf", "size":"319.96 KB"}}
     ],

    
    
    "substrate":{"background":"url(../common/page-substrates/page0006.png)",
    "backgroundSize":"contain", "backgroundColor":"#FFFFFF"},
    
    "leftTool":{"innerText":"5","src":"page5.html"},
    
    "rightTool":{"innerText":" 7","src":"page7.html"},
    
    "content":[{}
    
    ,{"className":"div_items", "top":"759","left":"183","width":"214","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font1","text":"C.O.P.S. Employee Handbook September 2013:  Page" 
    }}
  
    ,{"className":"div_items", "top":"759","left":"400","width":"5","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font2","text":"5" 
    }}
  
    ,{"className":"div_items", "top":"759","left":"408","width":"8","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font1","text":"of" 
    }}
  
    ,{"className":"div_items", "top":"759","left":"418","width":"10","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font2","text":"22" 
    }}
  
    ,{"className":"div_items", "top":"770","left":"36","width":"545","height":"10","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font3","text":"Downloaded or printed pages may be outdated.  If you printed or downloaded this page, please check the C.O.P.S. intranet to ensure you have the most recent version." 
    }}
  
    ,{"className":"div_items", "top":"34","left":"215","width":"182","height":"16","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font8","text":"Building Access and Parking" 
    }}
  
    ,{"className":"div_items", "top":"55","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"56","left":"90","width":"451","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Parking is provided for C.O.P.S. employees in employee designated spots, unless otherwise specified." 
    }}
  
    ,{"className":"div_items", "top":"75","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"76","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Employees should enter/exit the building through the employee entrance, unless designated otherwise." 
    }}
  
    ,{"className":"div_items", "top":"89","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"To enter, hold your access control device up to the reader next to the door.  The door can then be" 
    }}
  
    ,{"className":"div_items", "top":"103","left":"90","width":"310","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"opened if you are authorized to enter the building through that door." 
    }}
  
    ,{"className":"div_items", "top":"122","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"123","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"All employees and visitors are required to wear a company issued identification badge that is clearly" 
    }}
  
    ,{"className":"div_items", "top":"136","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"visible to others whenever you are in the C.O.P.S. facility.  Employee ID badges will also serve as a time" 
    }}
  
    ,{"className":"div_items", "top":"149","left":"90","width":"120","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"card for hourly employees." 
    }}
  
    ,{"className":"div_items", "top":"168","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"169","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"DO NOT allow anyone to enter the building or any access-controlled door unless they have" 
    }}
  
    ,{"className":"div_items", "top":"183","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"demonstrated that they possess a valid access control device and identification badge.  Building security" 
    }}
  
    ,{"className":"div_items", "top":"196","left":"90","width":"241","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"and employee safety is the responsibility of everyone." 
    }}
  
    ,{"className":"div_items", "top":"226","left":"241","width":"131","height":"16","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font8","text":"Handicapped Access" 
    }}
  
    ,{"className":"div_items", "top":"247","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"248","left":"90","width":"233","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Handicap parking is available at all C.O.P.S. facilities." 
    }}
  
    ,{"className":"div_items", "top":"267","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"268","left":"90","width":"254","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Handicap bathrooms are available at all C.O.P.S facilities." 
    }}
  
    ,{"className":"div_items", "top":"298","left":"247","width":"118","height":"16","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font8","text":"Employee Lockers" 
    }}
  
    ,{"className":"div_items", "top":"319","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"320","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Where available, employees are issued a locker on their first day of employment.  You will be supplied" 
    }}
  
    ,{"className":"div_items", "top":"334","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"with a confidential locker combination.  This locker is the property of the company and is subject to" 
    }}
  
    ,{"className":"div_items", "top":"347","left":"90","width":"158","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"search at the company’s discretion." 
    }}
      
    ]
})
 	