﻿
    pageContentLoad(
    {
    "currentPage":{"item":"page4", "num":"4"},
    "keywords":"C.O.P.S., Employee, Handbook, September, 2013:, Page, 3, of, 22, Downloaded, or, printed, pages, may, be, outdated., If, you, downloaded, this, page,, please, check, the, intranet, to, ensure, have, most, recent, version., Monitoring:, What, do, we, do?, Central, Office, Processing, Services, There, are, thousands, independent, alarm, companies, throughout, country, who, experts, when, it, comes, selling,, installing,, and, servicing, systems., However,, monitoring, these, systems, 24, hours, a, day, requires, significantly, different, area, experience, expertise, –, which, is, where, come, in., Monitoring, (sometimes, referred, herein, as, “company”), wholesale, Station, that, provides, world, class, dealer, services, for, over, 4,000, with, 900,000, subscribers, across, country., The, company, (, Dealer, ), gives, each, system, Subscriber, an, account, number, programs, panel, contact, our, receiving, equipment, using, phone, line,, internet, connection,, wirelessly, specialized, cellular, radio, connection, needs, assistance., By, combining, receiver, ID, programmed, into, panel,, given, unique, receiver-account, number., All, information, necessary, monitor, entered, database, (Generations™)., This, includes, like, Subscriber’s, address,, telephone, numbers,, passcodes,, points, protection,, other, important, information., Most, dealers, enter, themselves, MPower,, Advanced, Access, &, Alarm, Database, Management, Software., some, fax, mail, Data, Entry, department, they, them., When, assistance,, will, us,, take, actions, such, calling, see, if, need, emergency, dispatch, authorities, (i.e., police/fire, department),, notify, people, and/or, associated, Subscribers’, account., job, here, rewarding!, Not, only, entrusted, property, Subscribers, value, most,, also, protecting, Dealers’, reputation, company., After, installed, on, average,, interact, more, frequently, than, Dealers., How, represent, Dealers, part, whether, succeed., We, successful, prosper., Now, can, why, say…",
    "fullLink":"../../../index.html",
    "height":"792",
    "width":" 612",
    "isWide":"False",
    "bookWidth":"612",
    "bookHeight":"792",

    "download":[{}
      ,{"pdfPublication":{"url":"../common/downloads_c167bf08/COPS_BULLETin.pdf", "size":"1.16 MB"}}
    
      ,{"PdfPage":{"url":"../common/downloads_c167bf08/page0004.pdf", "size":"333.77 KB"}}
     ],

    
    
    "substrate":{"background":"url(../common/page-substrates/page0004.png)",
    "backgroundSize":"contain", "backgroundColor":"#FFFFFF"},
    
    "leftTool":{"innerText":"3","src":"page3.html"},
    
    "rightTool":{"innerText":" 5","src":"page5.html"},
    
    "content":[{}
    
    ,{"className":"div_items", "top":"759","left":"183","width":"214","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font1","text":"C.O.P.S. Employee Handbook September 2013:  Page" 
    }}
  
    ,{"className":"div_items", "top":"759","left":"400","width":"5","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font2","text":"3" 
    }}
  
    ,{"className":"div_items", "top":"759","left":"408","width":"8","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font1","text":"of" 
    }}
  
    ,{"className":"div_items", "top":"759","left":"418","width":"10","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font2","text":"22" 
    }}
  
    ,{"className":"div_items", "top":"770","left":"36","width":"545","height":"10","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font3","text":"Downloaded or printed pages may be outdated.  If you printed or downloaded this page, please check the C.O.P.S. intranet to ensure you have the most recent version." 
    }}
  
    ,{"className":"div_items", "top":"35","left":"142","width":"329","height":"22","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font4","text":"C.O.P.S. Monitoring: What do we do?" 
    }}
  
    ,{"className":"div_items", "top":"192","left":"197","width":"218","height":"18","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font30","text":"Central Office Processing Services" 
    }}
  
    ,{"className":"div_items", "top":"223","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"There are thousands of independent alarm companies throughout the country who are experts when it comes" 
    }}
  
    ,{"className":"div_items", "top":"239","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"to selling, installing, and servicing alarm systems.  However, monitoring these systems 24 hours a day requires a" 
    }}
  
    ,{"className":"div_items", "top":"254","left":"54","width":"378","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"significantly different area of experience and expertise – which is where we come in." 
    }}
  
    ,{"className":"div_items", "top":"280","left":"54","width":"91","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font9","text":"C.O.P.S. Monitoring" 
    }}
  
    ,{"className":"div_items", "top":"280","left":"147","width":"291","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"(sometimes referred to herein as the “company”) is a wholesale" 
    }}
  
    ,{"className":"div_items", "top":"280","left":"442","width":"116","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font9","text":"alarm monitoring Central" 
    }}
  
    ,{"className":"div_items", "top":"295","left":"54","width":"33","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font9","text":"Station" 
    }}
  
    ,{"className":"div_items", "top":"295","left":"89","width":"134","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"that provides the world class" 
    }}
  
    ,{"className":"div_items", "top":"295","left":"226","width":"81","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font9","text":"alarm monitoring" 
    }}
  
    ,{"className":"div_items", "top":"295","left":"310","width":"18","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"and" 
    }}
  
    ,{"className":"div_items", "top":"295","left":"331","width":"69","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font9","text":"dealer services" 
    }}
  
    ,{"className":"div_items", "top":"295","left":"402","width":"156","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"for over 4,000 independent alarm" 
    }}
  
    ,{"className":"div_items", "top":"311","left":"54","width":"273","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"companies with over 900,000 subscribers across the country." 
    }}
  
    ,{"className":"div_items", "top":"336","left":"54","width":"96","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"The alarm company (" 
    }}
  
    ,{"className":"div_items", "top":"336","left":"150","width":"30","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font9","text":"Dealer" 
    }}
  
    ,{"className":"div_items", "top":"336","left":"180","width":"123","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":") gives each alarm system (" 
    }}
  
    ,{"className":"div_items", "top":"336","left":"303","width":"48","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font9","text":"Subscriber" 
    }}
  
    ,{"className":"div_items", "top":"336","left":"351","width":"207","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":") an account number and programs the panel" 
    }}
  
    ,{"className":"div_items", "top":"352","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"to contact our receiving equipment using the phone line, internet connection, or wirelessly using a specialized" 
    }}
  
    ,{"className":"div_items", "top":"367","left":"54","width":"297","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"cellular or radio connection when the Subscriber needs assistance." 
    }}
  
    ,{"className":"div_items", "top":"392","left":"54","width":"84","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"By combining the" 
    }}
  
    ,{"className":"div_items", "top":"392","left":"144","width":"37","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font9","text":"receiver" 
    }}
  
    ,{"className":"div_items", "top":"392","left":"183","width":"254","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"ID that the alarm panel is programmed to and the" 
    }}
  
    ,{"className":"div_items", "top":"392","left":"443","width":"78","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font9","text":"account number" 
    }}
  
    ,{"className":"div_items", "top":"392","left":"524","width":"34","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"that is" 
    }}
  
    ,{"className":"div_items", "top":"408","left":"54","width":"280","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"programmed into the panel, each Subscriber is given a unique" 
    }}
  
    ,{"className":"div_items", "top":"408","left":"337","width":"77","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font9","text":"receiver-account" 
    }}
  
    ,{"className":"div_items", "top":"408","left":"417","width":"141","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"number.  All of the information" 
    }}
  
    ,{"className":"div_items", "top":"423","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"necessary to monitor the account is entered into our database (Generations™).  This includes information like" 
    }}
  
    ,{"className":"div_items", "top":"439","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"the Subscriber’s address, telephone numbers, passcodes, and points of protection, and other important" 
    }}
  
    ,{"className":"div_items", "top":"454","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"information.  Most dealers enter this information themselves over the internet using MPower, our Advanced" 
    }}
  
    ,{"className":"div_items", "top":"470","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Dealer Access & Alarm Database Management Software.  However, some dealers fax or mail the Subscriber" 
    }}
  
    ,{"className":"div_items", "top":"485","left":"54","width":"357","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"information to our Data Entry department and they enter the account for them." 
    }}
  
    ,{"className":"div_items", "top":"511","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"When the Subscriber requires assistance, the alarm system will contact us, which is when we take the necessary" 
    }}
  
    ,{"className":"div_items", "top":"526","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"actions such as calling the Subscriber to see if they need emergency assistance, dispatch the authorities (i.e." 
    }}
  
    ,{"className":"div_items", "top":"541","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"police/fire department), and calling to notify the people that the Subscriber and/or Dealer have associated with" 
    }}
  
    ,{"className":"div_items", "top":"557","left":"54","width":"112","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"the Subscribers’ account." 
    }}
  
    ,{"className":"div_items", "top":"582","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"The job we do here is rewarding!  Not only are we entrusted with the people and property that Subscribers" 
    }}
  
    ,{"className":"div_items", "top":"598","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"value most, we are also entrusted with protecting our Dealers’ reputation and company.  After the alarm" 
    }}
  
    ,{"className":"div_items", "top":"613","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"systems are installed – on average, we interact with Subscribers more frequently than our Dealers. How we" 
    }}
  
    ,{"className":"div_items", "top":"629","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"represent our Dealers is an important part of whether our Dealers succeed. We are only successful when our" 
    }}
  
    ,{"className":"div_items", "top":"644","left":"54","width":"218","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Dealers prosper.   Now you can see why we say…" 
    }}
      
    ]
})
 	