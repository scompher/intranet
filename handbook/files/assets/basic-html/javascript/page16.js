﻿
    pageContentLoad(
    {
    "currentPage":{"item":"page16", "num":"16"},
    "keywords":"C.O.P.S., Employee, Handbook, September, 2013:, Page, 15, of, 22, Downloaded, or, printed, pages, may, be, outdated., If, you, downloaded, this, page,, please, check, the, intranet, to, ensure, have, most, recent, version., , Full-time, employees, take, no, more, than, 5, contiguous, days, off, at, one, time., a, holiday, falls, within, week,, then, will, count, as, five, allowed, off., Hourly, compensated, for, up, 37.5, hours, unused, vacation, time, their, renewal, date., Compensation, based, on, rate, pay, in, effect, day, before, Any, excess, forfeited., (This, does, not, apply, salaried, employees)., Vacation, Policy, —, All, Part-Time, Employees, Part-time, are, eligible, minimum, (2, days), paid, after, year, part-time, employment., AFTER, (5), years, employment, receive, 22.5, (3, vacation., Based, actual, worked,, employee, earn, additional, above, outlined, above., has, worked, between, 1,000, and, 1,199, hours,, anniversary, date, date,, they, entitled, 7.5, hours., 1,200, been, earned, top, Holiday, Full-Time, holidays, only, if, work, scheduled, holiday., personal, do, qualify, working,, unless, taking, full, week, The, working, must, completed., Operation, Dispatchers, required, HOLIDAYS, that, fall, normal, day., YOU, MUST, shift, your, covered, by, making, prior, arrangements, with, manager., Personal, Time, full-time, This, renews, every, thereafter., Please, give, much, advance, notice, possible, when, request, Your, approved, writing, supervisor., use, WILL, LOSE, IT., accrue, year-to-year., is, available, employees., Company, Paid, Holidays, New, Year’s, Day, Labor, Memorial, Thanksgiving, Independence, Christmas, When, Saturday,, general, office, preceding, Friday., Sunday,, succeeding, Monday., Pay, differentials, Operations, hourly, regular, following, differential, listed, below:, Eve, (Pay, starts, 6:00, p.m.), Double, (Holiday), half",
    "fullLink":"../../../index.html",
    "height":"792",
    "width":" 612",
    "isWide":"False",
    "bookWidth":"612",
    "bookHeight":"792",

    "download":[{}
      ,{"pdfPublication":{"url":"../common/downloads_c167bf08/COPS_BULLETin.pdf", "size":"1.16 MB"}}
    
      ,{"PdfPage":{"url":"../common/downloads_c167bf08/page0016.pdf", "size":"325.86 KB"}}
     ],

    
    
    "substrate":{"background":"url(../common/page-substrates/page0016.png)",
    "backgroundSize":"contain", "backgroundColor":"#FFFFFF"},
    
    "leftTool":{"innerText":"15","src":"page15.html"},
    
    "rightTool":{"innerText":" 17","src":"page17.html"},
    
    "content":[{}
    
    ,{"className":"div_items", "top":"759","left":"181","width":"214","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font1","text":"C.O.P.S. Employee Handbook September 2013:  Page" 
    }}
  
    ,{"className":"div_items", "top":"759","left":"398","width":"10","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font2","text":"15" 
    }}
  
    ,{"className":"div_items", "top":"759","left":"410","width":"8","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font1","text":"of" 
    }}
  
    ,{"className":"div_items", "top":"759","left":"421","width":"10","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font2","text":"22" 
    }}
  
    ,{"className":"div_items", "top":"770","left":"36","width":"545","height":"10","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font3","text":"Downloaded or printed pages may be outdated.  If you printed or downloaded this page, please check the C.O.P.S. intranet to ensure you have the most recent version." 
    }}
  
    ,{"className":"div_items", "top":"33","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"34","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Full-time employees may take no more than 5 contiguous days off at one time.  If a holiday falls within" 
    }}
  
    ,{"className":"div_items", "top":"47","left":"90","width":"324","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"the week, then the holiday will count as one of the five allowed days off." 
    }}
  
    ,{"className":"div_items", "top":"60","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"61","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Hourly employees may be compensated for up to 37.5 hours of unused vacation at the time of their" 
    }}
  
    ,{"className":"div_items", "top":"74","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"renewal date.  Compensation will be based on the rate of pay in effect the day before the renewal date." 
    }}
  
    ,{"className":"div_items", "top":"88","left":"90","width":"390","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Any excess unused hours will be forfeited.  (This does not apply to salaried employees)." 
    }}
  
    ,{"className":"div_items", "top":"111","left":"54","width":"219","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font10","text":"Vacation Policy — All Part-Time Employees" 
    }}
  
    ,{"className":"div_items", "top":"124","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"125","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Part-time employees are eligible for a minimum of 15 hours (2 days) paid vacation after one year of" 
    }}
  
    ,{"className":"div_items", "top":"138","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"part-time employment.  AFTER five (5) years of part-time employment you will receive a minimum of" 
    }}
  
    ,{"className":"div_items", "top":"151","left":"90","width":"150","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"22.5 hours (3 days) paid vacation." 
    }}
  
    ,{"className":"div_items", "top":"164","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"165","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Based on actual hours worked, a part-time employee may earn additional vacation hours above the" 
    }}
  
    ,{"className":"div_items", "top":"179","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"minimum outlined above.  If a part-time employee has worked between 1,000 and 1,199 hours," 
    }}
  
    ,{"className":"div_items", "top":"192","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"between their anniversary date and renewal date, they will be entitled to 7.5 additional hours.  If more" 
    }}
  
    ,{"className":"div_items", "top":"205","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"than 1,200 hours has been worked they will have earned 15 hours additional on top of the minimum" 
    }}
  
    ,{"className":"div_items", "top":"219","left":"90","width":"69","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"vacation hours." 
    }}
  
    ,{"className":"div_items", "top":"242","left":"54","width":"195","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font10","text":"Holiday Policy — Full-Time Employees" 
    }}
  
    ,{"className":"div_items", "top":"255","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"256","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Full-time employees will be paid for holidays only if they work their scheduled day before and after the" 
    }}
  
    ,{"className":"div_items", "top":"269","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"holiday.  Vacation or personal days do not qualify as working, unless taking a full week before or after as" 
    }}
  
    ,{"className":"div_items", "top":"283","left":"90","width":"228","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"vacation.  The full working day must be completed." 
    }}
  
    ,{"className":"div_items", "top":"306","left":"54","width":"249","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font10","text":"Holiday Policy — Full-Time Operation Employees" 
    }}
  
    ,{"className":"div_items", "top":"318","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"319","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Dispatchers are required to work on HOLIDAYS that fall on their normal working day.  YOU MUST work" 
    }}
  
    ,{"className":"div_items", "top":"333","left":"90","width":"422","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"the shift unless you have your shift covered by making prior arrangements with your manager." 
    }}
  
    ,{"className":"div_items", "top":"356","left":"54","width":"212","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font10","text":"Personal Time — All Full-Time Employees" 
    }}
  
    ,{"className":"div_items", "top":"368","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"369","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Employees are eligible for 22.5 hours (3 days) of personal time after one year of full-time employment." 
    }}
  
    ,{"className":"div_items", "top":"382","left":"90","width":"154","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"This renews every year thereafter." 
    }}
  
    ,{"className":"div_items", "top":"395","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"396","left":"90","width":"360","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Please give as much advance notice as possible when you request personal time." 
    }}
  
    ,{"className":"div_items", "top":"409","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"410","left":"90","width":"274","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Your request must be approved in writing by your supervisor." 
    }}
  
    ,{"className":"div_items", "top":"423","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"424","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"If you do not use your personal time by your renewal date, YOU WILL LOSE IT.  Personal time does not" 
    }}
  
    ,{"className":"div_items", "top":"437","left":"90","width":"90","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"accrue year-to-year." 
    }}
  
    ,{"className":"div_items", "top":"450","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"451","left":"90","width":"241","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Personal time is only available to full-time employees." 
    }}
  
    ,{"className":"div_items", "top":"474","left":"54","width":"119","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font10","text":"Company Paid Holidays" 
    }}
  
    ,{"className":"div_items", "top":"499","left":"126","width":"72","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font9","text":"New Year’s Day" 
    }}
  
    ,{"className":"div_items", "top":"499","left":"234","width":"46","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font9","text":"Labor Day" 
    }}
  
    ,{"className":"div_items", "top":"499","left":"342","width":"65","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font9","text":"Memorial Day" 
    }}
  
    ,{"className":"div_items", "top":"523","left":"126","width":"79","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font9","text":"Thanksgiving Day" 
    }}
  
    ,{"className":"div_items", "top":"523","left":"234","width":"85","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font9","text":"Independence Day" 
    }}
  
    ,{"className":"div_items", "top":"523","left":"342","width":"66","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font9","text":"Christmas Day" 
    }}
  
    ,{"className":"div_items", "top":"545","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"546","left":"90","width":"416","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"When the holiday falls on a Saturday, the general office will have off on the preceding Friday." 
    }}
  
    ,{"className":"div_items", "top":"559","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"560","left":"90","width":"423","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"When the holiday falls on a Sunday, the general office will have off on the succeeding Monday." 
    }}
  
    ,{"className":"div_items", "top":"584","left":"205","width":"203","height":"16","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font8","text":"Pay differentials for Operations" 
    }}
  
    ,{"className":"div_items", "top":"606","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"All hourly Operations employees scheduled to work a holiday will be paid for their regular hours based on the" 
    }}
  
    ,{"className":"div_items", "top":"620","left":"54","width":"153","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"following differential listed below:" 
    }}
  
    ,{"className":"div_items", "top":"642","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"643","left":"90","width":"229","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"New Year’s Eve (Pay differential starts at 6:00 p.m.)" 
    }}
  
    ,{"className":"div_items", "top":"643","left":"378","width":"57","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Double Time" 
    }}
  
    ,{"className":"div_items", "top":"656","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"657","left":"90","width":"112","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"New Year’s Day (Holiday)" 
    }}
  
    ,{"className":"div_items", "top":"657","left":"378","width":"57","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Double Time" 
    }}
  
    ,{"className":"div_items", "top":"670","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"671","left":"90","width":"106","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Memorial Day (Holiday)" 
    }}
  
    ,{"className":"div_items", "top":"671","left":"378","width":"69","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Time and a half" 
    }}
  
    ,{"className":"div_items", "top":"683","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"684","left":"90","width":"126","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Independence Day (Holiday)" 
    }}
  
    ,{"className":"div_items", "top":"684","left":"378","width":"69","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Time and a half" 
    }}
  
    ,{"className":"div_items", "top":"697","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"698","left":"90","width":"88","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Labor Day (Holiday)" 
    }}
  
    ,{"className":"div_items", "top":"698","left":"378","width":"69","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Time and a half" 
    }}
  
    ,{"className":"div_items", "top":"711","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"712","left":"90","width":"120","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Thanksgiving Day (Holiday)" 
    }}
  
    ,{"className":"div_items", "top":"712","left":"378","width":"69","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Time and a half" 
    }}
  
    ,{"className":"div_items", "top":"725","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"726","left":"90","width":"224","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Christmas Eve (Pay differential starts at 6:00 p.m.)" 
    }}
  
    ,{"className":"div_items", "top":"726","left":"378","width":"57","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Double Time" 
    }}
  
    ,{"className":"div_items", "top":"738","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"739","left":"90","width":"107","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Christmas Day (Holiday)" 
    }}
  
    ,{"className":"div_items", "top":"739","left":"378","width":"57","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Double Time" 
    }}
      
    ]
})
 	