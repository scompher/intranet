﻿
    pageContentLoad(
    {
    "currentPage":{"item":"page7", "num":"7"},
    "keywords":"C.O.P.S., Employee, Handbook, September, 2013:, Page, 6, of, 22, Downloaded, or, printed, pages, may, be, outdated., If, you, downloaded, this, page,, please, check, the, intranet, to, ensure, have, most, recent, version., Personnel, Policies, and, Practices, Clocking, In/Out, , Hourly, Employees, Are, Expected, To, Use, Company, Time, Clocks, Properly:, The, time, clock, is, Company’s, method, used, track, attendance, punctuality, (not, personal, watches,, cell, phones,, other, means)., mandatory, for, clocking, in, out, shift,, authorized, breaks,, away, from, work, non-work, related, reasons, (these, scenarios, are, detailed, below)., Failure, use, will, result, disciplinary, action, including, points, (as, outlined, handbook)., Policy, Employees:, employees, required, no, more, than, 14, minutes, before, start, their, shift., expected, at, station, ready, by, scheduled, begin, paid, Out:, When, end, a, shift, there, 9, minute, grace, period, an, employee’s, that, allows, enough, appropriately, out., Any, beyond, require, approval, supervisor., Breaks, For, Authorized, Breaks:, company, provides, meal, breaks, qualified, (see, guidelines, take, break, help, they, refreshed, during, It, responsibility, periods, so, return, on, avoid, action., Late, assigned, if, employee, clocks, 2, time., not, first, last, hour, ▪, Should, department, get, busy,, asked, break., worked., permitted, leave, grounds, any, without, supervisor, manager, permission., permission, given, grounds,, longer, considered, working, company., Dispatchers, alarm, operations, room, manager’s, Other, There, throughout, shifts., encouraged, prior, break,, after, handle, issues, (i.e., restroom,, calls)., emergency, arises, must, time,, need, abuse, exception, contrary, employer’s, rules, subject, as, well, reduction, pay, based, actual, hours, Break, Summary, Scheduled, Shift, Meal, Less, 4, None, up, 10, 8, 30, uninterrupted,, unpaid, 37",
    "fullLink":"../../../index.html",
    "height":"792",
    "width":" 612",
    "isWide":"False",
    "bookWidth":"612",
    "bookHeight":"792",

    "download":[{}
      ,{"pdfPublication":{"url":"../common/downloads_c167bf08/COPS_BULLETin.pdf", "size":"1.16 MB"}}
    
      ,{"PdfPage":{"url":"../common/downloads_c167bf08/page0007.pdf", "size":"415.66 KB"}}
     ],

    
    
    "substrate":{"background":"url(../common/page-substrates/page0007.png)",
    "backgroundSize":"contain", "backgroundColor":"#FFFFFF"},
    
    "leftTool":{"innerText":"6","src":"page6.html"},
    
    "rightTool":{"innerText":" 8","src":"page8.html"},
    
    "content":[{}
    
    ,{"className":"div_items", "top":"759","left":"183","width":"214","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font1","text":"C.O.P.S. Employee Handbook September 2013:  Page" 
    }}
  
    ,{"className":"div_items", "top":"759","left":"400","width":"5","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font2","text":"6" 
    }}
  
    ,{"className":"div_items", "top":"759","left":"408","width":"8","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font1","text":"of" 
    }}
  
    ,{"className":"div_items", "top":"759","left":"418","width":"10","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font2","text":"22" 
    }}
  
    ,{"className":"div_items", "top":"770","left":"36","width":"545","height":"10","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font3","text":"Downloaded or printed pages may be outdated.  If you printed or downloaded this page, please check the C.O.P.S. intranet to ensure you have the most recent version." 
    }}
  
    ,{"className":"div_items", "top":"35","left":"157","width":"298","height":"22","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font4","text":"Personnel Policies and Practices" 
    }}
  
    ,{"className":"div_items", "top":"73","left":"255","width":"102","height":"16","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font8","text":"Clocking In/Out" 
    }}
  
    ,{"className":"div_items", "top":"95","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"96","left":"90","width":"353","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font26","text":"Hourly Employees Are Expected To Use Company Time Clocks Properly:" 
    }}
  
    ,{"className":"div_items", "top":"96","left":"451","width":"107","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"The time clock is the" 
    }}
  
    ,{"className":"div_items", "top":"109","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Company’s method used to track attendance and punctuality (not personal watches, cell phones, or" 
    }}
  
    ,{"className":"div_items", "top":"123","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"other means). Use of the time clock is mandatory for clocking in and out for shift, authorized breaks, and" 
    }}
  
    ,{"className":"div_items", "top":"136","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"time away from work for non-work related reasons (these scenarios are detailed below).  Failure to use" 
    }}
  
    ,{"className":"div_items", "top":"149","left":"90","width":"414","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"the time clock will result in disciplinary action including points (as outlined in the handbook)." 
    }}
  
    ,{"className":"div_items", "top":"162","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"163","left":"90","width":"136","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font26","text":"Policy for Hourly Employees:" 
    }}
  
    ,{"className":"div_items", "top":"163","left":"235","width":"323","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Hourly employees are required to clock in no more than 14 minutes" 
    }}
  
    ,{"className":"div_items", "top":"177","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"before the start of their shift. Employees are expected to be at their work station and ready to work by" 
    }}
  
    ,{"className":"div_items", "top":"190","left":"90","width":"429","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"the scheduled begin time of their shift.  Employees will be paid from the start time of their shift." 
    }}
  
    ,{"className":"div_items", "top":"203","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"204","left":"90","width":"61","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font26","text":"Clocking Out:" 
    }}
  
    ,{"className":"div_items", "top":"204","left":"157","width":"400","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"When clocking out at the end of a shift there is a 9 minute grace period from the end of" 
    }}
  
    ,{"className":"div_items", "top":"217","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"an employee’s shift that allows enough time to appropriately clock out.  Any time beyond that will" 
    }}
  
    ,{"className":"div_items", "top":"231","left":"90","width":"179","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"require the approval of their supervisor." 
    }}
  
    ,{"className":"div_items", "top":"260","left":"283","width":"45","height":"16","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font8","text":"Breaks" 
    }}
  
    ,{"className":"div_items", "top":"282","left":"90","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"283","left":"108","width":"140","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font26","text":"Policy For Authorized Breaks:" 
    }}
  
    ,{"className":"div_items", "top":"283","left":"258","width":"300","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"The company provides meal breaks to qualified employees (see" 
    }}
  
    ,{"className":"div_items", "top":"296","left":"108","width":"450","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"guidelines below). Employees are required to take their break to help ensure they are refreshed" 
    }}
  
    ,{"className":"div_items", "top":"310","left":"108","width":"450","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"during their shift.  It is the employee’s responsibility to track break periods so they return on time to" 
    }}
  
    ,{"className":"div_items", "top":"323","left":"108","width":"450","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"avoid disciplinary action.  Late points will be assigned if the employee clocks in more than 2 minutes" 
    }}
  
    ,{"className":"div_items", "top":"337","left":"108","width":"449","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"beyond the break time.  Breaks are not scheduled during the first or last hour of an employee’s shift." 
    }}
  
    ,{"className":"div_items", "top":"350","left":"126","width":"4","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font19","text":"▪" 
    }}
  
    ,{"className":"div_items", "top":"350","left":"144","width":"414","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Should an employee’s department get busy, they may be asked to return to work from their" 
    }}
  
    ,{"className":"div_items", "top":"363","left":"144","width":"243","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"break. The employee will be paid for the time worked." 
    }}
  
    ,{"className":"div_items", "top":"377","left":"126","width":"4","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font19","text":"▪" 
    }}
  
    ,{"className":"div_items", "top":"377","left":"144","width":"414","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Hourly employees are not permitted to leave the Company grounds at any time without" 
    }}
  
    ,{"className":"div_items", "top":"390","left":"144","width":"414","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"supervisor or manager permission. If permission is given to leave company grounds, the" 
    }}
  
    ,{"className":"div_items", "top":"404","left":"144","width":"316","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"employee will no longer be considered to be working for the company." 
    }}
  
    ,{"className":"div_items", "top":"417","left":"126","width":"4","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font19","text":"▪" 
    }}
  
    ,{"className":"div_items", "top":"417","left":"144","width":"414","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Dispatchers are not to leave the alarm operations room at any time without their shift" 
    }}
  
    ,{"className":"div_items", "top":"431","left":"144","width":"101","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"manager’s permission." 
    }}
  
    ,{"className":"div_items", "top":"443","left":"90","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"444","left":"108","width":"63","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font26","text":"Other Breaks:" 
    }}
  
    ,{"className":"div_items", "top":"444","left":"176","width":"347","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"There are no other scheduled break periods throughout shifts. Employees are" 
    }}
  
    ,{"className":"div_items", "top":"458","left":"108","width":"446","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"encouraged to use time prior to shift, during meal break, or after shift to handle any personal issues" 
    }}
  
    ,{"className":"div_items", "top":"471","left":"108","width":"447","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"(i.e. restroom, personal calls).  If an emergency arises and an employee must leave their work at any" 
    }}
  
    ,{"className":"div_items", "top":"485","left":"108","width":"431","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"other time, they will need to get the permission of their supervisor.   Any abuse of this exception" 
    }}
  
    ,{"className":"div_items", "top":"498","left":"108","width":"420","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"break time is contrary to employer’s rules and will be subject to disciplinary action as well as a" 
    }}
  
    ,{"className":"div_items", "top":"512","left":"108","width":"213","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"reduction in pay based on actual hours worked." 
    }}
  
    ,{"className":"div_items", "top":"535","left":"54","width":"171","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font10","text":"Hourly Employee Break Summary" 
    }}
  
    ,{"className":"div_items", "top":"548","left":"77","width":"71","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font27","text":"Scheduled Shift" 
    }}
  
    ,{"className":"div_items", "top":"548","left":"262","width":"80","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font27","text":"Authorized Break" 
    }}
  
    ,{"className":"div_items", "top":"548","left":"397","width":"100","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font27","text":"Authorized Meal time" 
    }}
  
    ,{"className":"div_items", "top":"562","left":"77","width":"78","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Less than 4 hours" 
    }}
  
    ,{"className":"div_items", "top":"562","left":"262","width":"24","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"None" 
    }}
  
    ,{"className":"div_items", "top":"562","left":"397","width":"24","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"None" 
    }}
  
    ,{"className":"div_items", "top":"576","left":"77","width":"96","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"4 hours up to 6 hours" 
    }}
  
    ,{"className":"div_items", "top":"576","left":"262","width":"68","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"10 minute paid" 
    }}
  
    ,{"className":"div_items", "top":"576","left":"397","width":"24","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"None" 
    }}
  
    ,{"className":"div_items", "top":"590","left":"77","width":"158","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"6 hours up to and including 8 hours" 
    }}
  
    ,{"className":"div_items", "top":"590","left":"262","width":"24","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"None" 
    }}
  
    ,{"className":"div_items", "top":"590","left":"397","width":"148","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"30 minute uninterrupted, unpaid" 
    }}
  
    ,{"className":"div_items", "top":"604","left":"77","width":"39","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"10 hours" 
    }}
  
    ,{"className":"div_items", "top":"604","left":"262","width":"24","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"None" 
    }}
  
    ,{"className":"div_items", "top":"604","left":"397","width":"148","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"37 minute uninterrupted, unpaid" 
    }}
      
    ]
})
 	