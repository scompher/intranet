﻿
    pageContentLoad(
    {
    "currentPage":{"item":"page9", "num":"9"},
    "keywords":"C.O.P.S., Employee, Handbook, September, 2013:, Page, 8, of, 22, Downloaded, or, printed, pages, may, be, outdated., If, you, downloaded, this, page,, please, check, the, intranet, to, ensure, have, most, recent, version., Attendance, Point, Table, OCCURRENCE, Base, Points, OTHER, POINTS, Lateness, clocks, in, any, time, after, start, their, scheduled, shift., 1, The, employee, will, initially, receive, full, lateness, point., After, first, 15, minutes, they, an, additional, ¼, An, point, issued, for, every, hour, thereafter., Partial, Absence, who, reports, work, and, leaves, before, completed, 0, a, each, that, leave, early., , Call, out, with, at, least, four, hours’, notice:, calls, entire, shift, hours, more, prior, time., less, than, 1.5, No, Call,, Show:, does, not, call, report, within, TBD, Cause, Termination, Example, Scenarios, Based, on, 7.5-hour, Description, Occurrence, Assessed, 30, late., 1¼, (T), 2, 3, late, 1¾, giving, 4, notice, (A), without, end, 1(A), arrives, Any, day,, fails, duty, (A)=, =, Tardy",
    "fullLink":"../../../index.html",
    "height":"792",
    "width":" 612",
    "isWide":"False",
    "bookWidth":"612",
    "bookHeight":"792",

    "download":[{}
      ,{"pdfPublication":{"url":"../common/downloads_c167bf08/COPS_BULLETin.pdf", "size":"1.16 MB"}}
    
      ,{"PdfPage":{"url":"../common/downloads_c167bf08/page0009.pdf", "size":"418.05 KB"}}
     ],

    
    
    "substrate":{"background":"url(../common/page-substrates/page0009.png)",
    "backgroundSize":"contain", "backgroundColor":"#FFFFFF"},
    
    "leftTool":{"innerText":"8","src":"page8.html"},
    
    "rightTool":{"innerText":" 10","src":"page10.html"},
    
    "content":[{}
    
    ,{"className":"div_items", "top":"759","left":"183","width":"214","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font1","text":"C.O.P.S. Employee Handbook September 2013:  Page" 
    }}
  
    ,{"className":"div_items", "top":"759","left":"400","width":"5","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font2","text":"8" 
    }}
  
    ,{"className":"div_items", "top":"759","left":"408","width":"8","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font1","text":"of" 
    }}
  
    ,{"className":"div_items", "top":"759","left":"418","width":"10","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font2","text":"22" 
    }}
  
    ,{"className":"div_items", "top":"770","left":"36","width":"545","height":"10","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font3","text":"Downloaded or printed pages may be outdated.  If you printed or downloaded this page, please check the C.O.P.S. intranet to ensure you have the most recent version." 
    }}
  
    ,{"className":"div_items", "top":"33","left":"248","width":"115","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font20","text":"Attendance Point Table" 
    }}
  
    ,{"className":"div_items", "top":"74","left":"167","width":"57","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font21","text":"OCCURRENCE" 
    }}
  
    ,{"className":"div_items", "top":"74","left":"296","width":"48","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font21","text":"Base Points" 
    }}
  
    ,{"className":"div_items", "top":"74","left":"403","width":"62","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font21","text":"OTHER POINTS" 
    }}
  
    ,{"className":"div_items", "top":"87","left":"113","width":"36","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font22","text":"Lateness" 
    }}
  
    ,{"className":"div_items", "top":"118","left":"113","width":"164","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font23","text":"Employee clocks in any time after the" 
    }}
  
    ,{"className":"div_items", "top":"130","left":"113","width":"117","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font23","text":"start of their scheduled shift." 
    }}
  
    ,{"className":"div_items", "top":"136","left":"317","width":"5","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font23","text":"1" 
    }}
  
    ,{"className":"div_items", "top":"100","left":"361","width":"146","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font23","text":"The employee will initially receive 1" 
    }}
  
    ,{"className":"div_items", "top":"112","left":"361","width":"146","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font23","text":"full lateness point. After the first 15" 
    }}
  
    ,{"className":"div_items", "top":"124","left":"361","width":"147","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font23","text":"minutes they will receive an" 
    }}
  
    ,{"className":"div_items", "top":"136","left":"361","width":"76","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font23","text":"additional ¼ point." 
    }}
  
    ,{"className":"div_items", "top":"161","left":"361","width":"147","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font23","text":"An additional ¼ point will be issued" 
    }}
  
    ,{"className":"div_items", "top":"173","left":"361","width":"104","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font23","text":"for every hour thereafter." 
    }}
  
    ,{"className":"div_items", "top":"186","left":"113","width":"64","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font22","text":"Partial Absence" 
    }}
  
    ,{"className":"div_items", "top":"198","left":"113","width":"164","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font23","text":"Employee who reports to work and" 
    }}
  
    ,{"className":"div_items", "top":"211","left":"113","width":"164","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font23","text":"leaves before they have completed their" 
    }}
  
    ,{"className":"div_items", "top":"223","left":"113","width":"64","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font23","text":"scheduled shift." 
    }}
  
    ,{"className":"div_items", "top":"211","left":"317","width":"5","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font23","text":"0" 
    }}
  
    ,{"className":"div_items", "top":"198","left":"361","width":"147","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font23","text":"The employee will receive a ¼ point" 
    }}
  
    ,{"className":"div_items", "top":"211","left":"361","width":"143","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font23","text":"for each hour that they leave early." 
    }}
  
    ,{"className":"div_items", "top":"236","left":"113","width":"35","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font22","text":"Absence" 
    }}
  
    ,{"className":"div_items", "top":"248","left":"113","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"249","left":"122","width":"156","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font21","text":"Call out with at least four hours’" 
    }}
  
    ,{"className":"div_items", "top":"262","left":"122","width":"29","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font21","text":"notice:" 
    }}
  
    ,{"className":"div_items", "top":"262","left":"154","width":"124","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font23","text":"Employee calls out for entire" 
    }}
  
    ,{"className":"div_items", "top":"274","left":"122","width":"156","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font23","text":"shift at least four hours or more prior" 
    }}
  
    ,{"className":"div_items", "top":"286","left":"122","width":"97","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font23","text":"to scheduled start time." 
    }}
  
    ,{"className":"div_items", "top":"267","left":"317","width":"5","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font23","text":"1" 
    }}
  
    ,{"className":"div_items", "top":"298","left":"112","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"300","left":"122","width":"156","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font21","text":"Call out with less than four hours’" 
    }}
  
    ,{"className":"div_items", "top":"312","left":"122","width":"29","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font21","text":"notice:" 
    }}
  
    ,{"className":"div_items", "top":"312","left":"156","width":"122","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font23","text":"Employee calls out for entire" 
    }}
  
    ,{"className":"div_items", "top":"324","left":"122","width":"156","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font23","text":"shift less than four hours prior to" 
    }}
  
    ,{"className":"div_items", "top":"337","left":"122","width":"86","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font23","text":"scheduled start time." 
    }}
  
    ,{"className":"div_items", "top":"318","left":"313","width":"13","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font24","text":"1.5" 
    }}
  
    ,{"className":"div_items", "top":"349","left":"112","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"351","left":"122","width":"75","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font21","text":"No Call, No Show:" 
    }}
  
    ,{"className":"div_items", "top":"351","left":"199","width":"79","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font23","text":"Employee does not" 
    }}
  
    ,{"className":"div_items", "top":"363","left":"122","width":"156","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font23","text":"call or report to work within the first" 
    }}
  
    ,{"className":"div_items", "top":"375","left":"122","width":"156","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font23","text":"hour of the start time of their" 
    }}
  
    ,{"className":"div_items", "top":"387","left":"122","width":"64","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font23","text":"scheduled shift." 
    }}
  
    ,{"className":"div_items", "top":"368","left":"311","width":"16","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font24","text":"TBD" 
    }}
  
    ,{"className":"div_items", "top":"368","left":"361","width":"90","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font24","text":"Cause for Termination" 
    }}
  
    ,{"className":"div_items", "top":"414","left":"197","width":"228","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font21","text":"Example Scenarios Based on a 7.5-hour scheduled shift" 
    }}
  
    ,{"className":"div_items", "top":"427","left":"113","width":"108","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font22","text":"Description of Occurrence" 
    }}
  
    ,{"className":"div_items", "top":"427","left":"438","width":"66","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font22","text":"Points Assessed" 
    }}
  
    ,{"className":"div_items", "top":"439","left":"113","width":"247","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font23","text":"Employee calls in before start of shift will be 30 minutes late." 
    }}
  
    ,{"className":"div_items", "top":"439","left":"459","width":"27","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font23","text":"1¼  (T)" 
    }}
  
    ,{"className":"div_items", "top":"452","left":"113","width":"230","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font23","text":"Employee clocks in 2 minutes after the start of their shift" 
    }}
  
    ,{"className":"div_items", "top":"452","left":"463","width":"18","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font23","text":"1 (T)" 
    }}
  
    ,{"className":"div_items", "top":"465","left":"113","width":"246","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font23","text":"Employee calls in before the start of shift will be 3 hours late" 
    }}
  
    ,{"className":"div_items", "top":"465","left":"460","width":"25","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font23","text":"1¾ (T)" 
    }}
  
    ,{"className":"div_items", "top":"478","left":"113","width":"165","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font23","text":"Employee calls out giving 4 hours’ notice" 
    }}
  
    ,{"className":"div_items", "top":"478","left":"463","width":"19","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font23","text":"1 (A)" 
    }}
  
    ,{"className":"div_items", "top":"490","left":"113","width":"199","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font23","text":"Employee calls out without giving 4 hours’ notice" 
    }}
  
    ,{"className":"div_items", "top":"490","left":"459","width":"27","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font23","text":"1.5 (A)" 
    }}
  
    ,{"className":"div_items", "top":"503","left":"113","width":"200","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font23","text":"Employee leaves 4 hours before end of their shift" 
    }}
  
    ,{"className":"div_items", "top":"503","left":"464","width":"17","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font23","text":"1(A)" 
    }}
  
    ,{"className":"div_items", "top":"516","left":"113","width":"176","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font23","text":"Employee arrives 4 hours late for their shift" 
    }}
  
    ,{"className":"div_items", "top":"516","left":"463","width":"18","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font23","text":"2 (T)" 
    }}
  
    ,{"className":"div_items", "top":"528","left":"113","width":"206","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font23","text":"Any day, employee fails to call in or report for duty" 
    }}
  
    ,{"className":"div_items", "top":"528","left":"464","width":"16","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font23","text":"TBD" 
    }}
  
    ,{"className":"div_items", "top":"541","left":"398","width":"112","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"(A)= Absence  (T) = Tardy" 
    }}
      
    ]
})
 	