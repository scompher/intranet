﻿
tocList = [
    
      {"tag":"ul", "id" : "TocList","content":[{}
        
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName", "href":"page1.html","text": "Front Cover"},{"tag":"a", "className": "fontPageNum", "href":"page1.html","text": "1"}]}

    
    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName", "href":"page2.html","text": "Table of Contents"},{"tag":"a", "className": "fontPageNum", "href":"page2.html","text": "2"}]}

    
    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName", "href":"page3.html","text": "The President’s Message"},{"tag":"a", "className": "fontPageNum", "href":"page3.html","text": "3"}]}

    
    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName", "href":"page4.html","text": "C.O.P.S. Monitoring: What do we do?"},{"tag":"a", "className": "fontPageNum", "href":"page4.html","text": "4"}]}

    
    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName", "href":"page5.html","text": "General Company Policies and Practices"},{"tag":"a", "className": "fontPageNum", "href":"page5.html","text": "5"}]}

    
    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName tocChild", "href":"page5.html","text": "Introduction"},{"tag":"a", "className": "fontPageNum", "href":"page5.html","text": "5"}]}


    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName tocChild", "href":"page5.html","text": "At-Will Employment"},{"tag":"a", "className": "fontPageNum", "href":"page5.html","text": "5"}]}


    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName tocChild", "href":"page5.html","text": "Equal Opportunity Employment"},{"tag":"a", "className": "fontPageNum", "href":"page5.html","text": "5"}]}


    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName tocChild", "href":"page5.html","text": "On-the-job involvement with drugs, controlled substances, and/or alcohol"},{"tag":"a", "className": "fontPageNum", "href":"page5.html","text": "5"}]}


    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName tocChild", "href":"page5.html","text": "Drug Testing & Criminal Background Checks"},{"tag":"a", "className": "fontPageNum", "href":"page5.html","text": "5"}]}


    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName tocChild", "href":"page6.html","text": "Building Access and Parking"},{"tag":"a", "className": "fontPageNum", "href":"page6.html","text": "6"}]}


    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName tocChild", "href":"page6.html","text": "Handicapped Access"},{"tag":"a", "className": "fontPageNum", "href":"page6.html","text": "6"}]}


    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName tocChild", "href":"page6.html","text": "Employee Lockers"},{"tag":"a", "className": "fontPageNum", "href":"page6.html","text": "6"}]}


    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName", "href":"page7.html","text": "Personnel Policies and Practices"},{"tag":"a", "className": "fontPageNum", "href":"page7.html","text": "7"}]}

    
    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName tocChild", "href":"page7.html","text": "Clocking In/Out"},{"tag":"a", "className": "fontPageNum", "href":"page7.html","text": "7"}]}


    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName tocChild", "href":"page7.html","text": "Breaks"},{"tag":"a", "className": "fontPageNum", "href":"page7.html","text": "7"}]}


    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName tocChild", "href":"page7.html","text": "Hourly Employee Break Summary"},{"tag":"a", "className": "fontPageNum", "href":"page7.html","text": "7"}]}


    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName tocChild", "href":"page8.html","text": "How to use the time clock"},{"tag":"a", "className": "fontPageNum", "href":"page8.html","text": "8"}]}


    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName tocChild", "href":"page8.html","text": "Attendance / Lateness Policy"},{"tag":"a", "className": "fontPageNum", "href":"page8.html","text": "8"}]}


    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName tocChild", "href":"page8.html","text": "Lateness / Absenteeism / Partial Absence"},{"tag":"a", "className": "fontPageNum", "href":"page8.html","text": "8"}]}


    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName tocChild", "href":"page9.html","text": "Attendance Point Table"},{"tag":"a", "className": "fontPageNum", "href":"page9.html","text": "9"}]}


    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName tocChild", "href":"page10.html","text": "General Dress Code"},{"tag":"a", "className": "fontPageNum", "href":"page10.html","text": "10"}]}


    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName tocChild", "href":"page10.html","text": "General Office"},{"tag":"a", "className": "fontPageNum", "href":"page10.html","text": "10"}]}


    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName tocChild", "href":"page10.html","text": "Dispatchers Working In Central"},{"tag":"a", "className": "fontPageNum", "href":"page10.html","text": "10"}]}


    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName tocChild", "href":"page10.html","text": "Company Meetings"},{"tag":"a", "className": "fontPageNum", "href":"page10.html","text": "10"}]}


    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName tocChild", "href":"page11.html","text": "Distribution of Paychecks"},{"tag":"a", "className": "fontPageNum", "href":"page11.html","text": "11"}]}


    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName tocChild", "href":"page11.html","text": "Employee Performance Review"},{"tag":"a", "className": "fontPageNum", "href":"page11.html","text": "11"}]}


    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName tocChild", "href":"page11.html","text": "General Office Employees"},{"tag":"a", "className": "fontPageNum", "href":"page11.html","text": "11"}]}


    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName tocChild", "href":"page11.html","text": "Operations Employees"},{"tag":"a", "className": "fontPageNum", "href":"page11.html","text": "11"}]}


    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName tocChild", "href":"page11.html","text": "Resignations"},{"tag":"a", "className": "fontPageNum", "href":"page11.html","text": "11"}]}


    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName", "href":"page12.html","text": "Employee Benefits"},{"tag":"a", "className": "fontPageNum", "href":"page12.html","text": "12"}]}

    
    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName tocChild", "href":"page12.html","text": "Full-Time Employee Benefits"},{"tag":"a", "className": "fontPageNum", "href":"page12.html","text": "12"}]}


    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName tocChild", "href":"page12.html","text": "Medical Insurance"},{"tag":"a", "className": "fontPageNum", "href":"page12.html","text": "12"}]}


    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName tocChild", "href":"page12.html","text": "Life and Accidental Death & Dismemberment (AD&D) Insurance"},{"tag":"a", "className": "fontPageNum", "href":"page12.html","text": "12"}]}


    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName tocChild", "href":"page12.html","text": "Disability Insurance"},{"tag":"a", "className": "fontPageNum", "href":"page12.html","text": "12"}]}


    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName tocChild", "href":"page13.html","text": "401(K) Plan"},{"tag":"a", "className": "fontPageNum", "href":"page13.html","text": "13"}]}


    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName tocChild", "href":"page13.html","text": "Tuition Reimbursement"},{"tag":"a", "className": "fontPageNum", "href":"page13.html","text": "13"}]}


    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName tocChild", "href":"page13.html","text": "Bereavement Pay"},{"tag":"a", "className": "fontPageNum", "href":"page13.html","text": "13"}]}


    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName tocChild", "href":"page13.html","text": "Jury Duty"},{"tag":"a", "className": "fontPageNum", "href":"page13.html","text": "13"}]}


    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName tocChild", "href":"page13.html","text": "Family and Medical Leave Policy"},{"tag":"a", "className": "fontPageNum", "href":"page13.html","text": "13"}]}


    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName tocChild", "href":"page15.html","text": "Leave for Maternity"},{"tag":"a", "className": "fontPageNum", "href":"page15.html","text": "15"}]}


    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName tocChild", "href":"page15.html","text": "Vacation/Holiday Benefits"},{"tag":"a", "className": "fontPageNum", "href":"page15.html","text": "15"}]}


    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName tocChild", "href":"page15.html","text": "Vacation Policy — All Full-Time Employees"},{"tag":"a", "className": "fontPageNum", "href":"page15.html","text": "15"}]}


    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName tocChild", "href":"page16.html","text": "Vacation Policy — All Part-Time Employees"},{"tag":"a", "className": "fontPageNum", "href":"page16.html","text": "16"}]}


    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName tocChild", "href":"page16.html","text": "Holiday Policy — Full-Time Employees"},{"tag":"a", "className": "fontPageNum", "href":"page16.html","text": "16"}]}


    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName tocChild", "href":"page16.html","text": "Holiday Policy — Full-Time Operation Employees"},{"tag":"a", "className": "fontPageNum", "href":"page16.html","text": "16"}]}


    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName tocChild", "href":"page16.html","text": "Personal Time — All Full-Time Employees"},{"tag":"a", "className": "fontPageNum", "href":"page16.html","text": "16"}]}


    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName tocChild", "href":"page16.html","text": "Company Paid Holidays"},{"tag":"a", "className": "fontPageNum", "href":"page16.html","text": "16"}]}


    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName tocChild", "href":"page16.html","text": "Pay differentials for Operations"},{"tag":"a", "className": "fontPageNum", "href":"page16.html","text": "16"}]}


    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName", "href":"page17.html","text": "Additional Guidelines and Information"},{"tag":"a", "className": "fontPageNum", "href":"page17.html","text": "17"}]}

    
    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName tocChild", "href":"page17.html","text": "Severe Weather Procedures"},{"tag":"a", "className": "fontPageNum", "href":"page17.html","text": "17"}]}


    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName tocChild", "href":"page17.html","text": "Guidelines Prohibiting Harassment"},{"tag":"a", "className": "fontPageNum", "href":"page17.html","text": "17"}]}


    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName tocChild", "href":"page18.html","text": "Safeguarding Confidential Information"},{"tag":"a", "className": "fontPageNum", "href":"page18.html","text": "18"}]}


    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName tocChild", "href":"page18.html","text": "Surveillance Cameras and Recording of Telephone Lines"},{"tag":"a", "className": "fontPageNum", "href":"page18.html","text": "18"}]}


    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName tocChild", "href":"page18.html","text": "Guidelines (Internet/Computer Network/Telecommunications)"},{"tag":"a", "className": "fontPageNum", "href":"page18.html","text": "18"}]}


    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName tocChild", "href":"page18.html","text": "The Internet"},{"tag":"a", "className": "fontPageNum", "href":"page18.html","text": "18"}]}


    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName tocChild", "href":"page19.html","text": "Electronic Mail"},{"tag":"a", "className": "fontPageNum", "href":"page19.html","text": "19"}]}


    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName tocChild", "href":"page19.html","text": "Cell Phone/Electronic devices"},{"tag":"a", "className": "fontPageNum", "href":"page19.html","text": "19"}]}


    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName tocChild", "href":"page19.html","text": "Computer Network and Telecommunications"},{"tag":"a", "className": "fontPageNum", "href":"page19.html","text": "19"}]}


    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName tocChild", "href":"page19.html","text": "Internet Postings Policy"},{"tag":"a", "className": "fontPageNum", "href":"page19.html","text": "19"}]}


    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName tocChild", "href":"page21.html","text": "Disciplinary Action"},{"tag":"a", "className": "fontPageNum", "href":"page21.html","text": "21"}]}


    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName tocChild", "href":"page22.html","text": "Company Departments"},{"tag":"a", "className": "fontPageNum", "href":"page22.html","text": "22"}]}


    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName tocChild", "href":"page22.html","text": "Accounting"},{"tag":"a", "className": "fontPageNum", "href":"page22.html","text": "22"}]}


    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName tocChild", "href":"page22.html","text": "Data Entry"},{"tag":"a", "className": "fontPageNum", "href":"page22.html","text": "22"}]}


    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName tocChild", "href":"page22.html","text": "Dealer Support"},{"tag":"a", "className": "fontPageNum", "href":"page22.html","text": "22"}]}


    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName tocChild", "href":"page22.html","text": "Human Resources"},{"tag":"a", "className": "fontPageNum", "href":"page22.html","text": "22"}]}


    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName tocChild", "href":"page22.html","text": "Mail Room"},{"tag":"a", "className": "fontPageNum", "href":"page22.html","text": "22"}]}


    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName tocChild", "href":"page22.html","text": "Operations"},{"tag":"a", "className": "fontPageNum", "href":"page22.html","text": "22"}]}


    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName tocChild", "href":"page22.html","text": "Sales & Marketing"},{"tag":"a", "className": "fontPageNum", "href":"page22.html","text": "22"}]}


    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName tocChild", "href":"page22.html","text": "Technology"},{"tag":"a", "className": "fontPageNum", "href":"page22.html","text": "22"}]}


    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName", "href":"page23.html","text": "Understanding and acknowledgement of the company employee handbook"},{"tag":"a", "className": "fontPageNum", "href":"page23.html","text": "23"}]}

    
    
    ,{"tag":"li","content":[{"tag":"a", "className": "fontPageName", "href":"page24.html","text": "Back Cover"},{"tag":"a", "className": "fontPageNum", "href":"page24.html","text": "24"}]}

    
    
      ]}
    
    ]
  