﻿
    pageContentLoad(
    {
    "currentPage":{"item":"page13", "num":"13"},
    "keywords":"C.O.P.S., Employee, Handbook, September, 2013:, Page, 12, of, 22, Downloaded, or, printed, pages, may, be, outdated., If, you, downloaded, this, page,, please, check, the, intranet, to, ensure, have, most, recent, version., 401(K), Plan, , The, qualifications, for, entry, into, plan:, ▪, employee, must, 21., employed, by, Company, minimum, twelve, (12), months., 1,000, hours, been, worked, within, those, Once, these, are, met,, is, eligible, enter, plan, on, first, day, subsequent, month., Upon, plan,, any, contributions, designated, that, year., will, make, a, matching, contribution, equal, 50%, 8%, contribute, plan., You, receive, Company’s, contributions., pre-tax, weekly, payroll, deductions., has, three, (3), years, they, 100%, vested, in, Tuition, Reimbursement, offers, pre-approved, tuition, reimbursement, benefit, all, full-time, and, part-time, employees., To, eligible,, apply, prior, beginning, school, semester, seeking, for., considered, post-secondary, institutions, course, study, company, related, where, grade, ′C′, better, achieved., Details, concerning, available, from, Human, Resources, Department., Bereavement, Pay, Immediate, Family, Member, (Mother,, Father,, Stepmother,, Stepfather,, Spouse,, Daughter,, Son,, Sister,, Brother,, Mother/Father–in-Law,, Sister/, Brother-in-Law):, Three, Paid, Days., Other, (Grandmother,, Grandfather,, Grandchildren,, Aunt,, Uncle,, Niece,, Nephew,, 1st, Cousin):, One, (1), Day., Jury, Duty, Please, forward, your, summons, supervisor, as, soon, get, it., pay, up, five, (5), days, Duty., MUST, un-cashed, endorsed, order, paid., Medical, Leave, Policy, Act, (“FMLA”),, requires, employers, 50, more, employees, provide, with, weeks, leave, during, month, period, certain, reasons, including, birth,, adoption,, placement, child;, deal, an, employee’s, own, serious, health, conditions, immediate, family, member., Eligibility, :, All, who, completed, at, least, months, service, 1,250, preceding, period., Reasons, Leave:, An, entitled, following, reasons:, Due, birth, adoption, child, foster, care, spouse,, parent, condition;, condition, renders, him, her, incapable, performing, functions, his, job., Definition, A, “serious, condition”, defined, law, illness,, injury,, impairment,, physical, mental, involves, Inpatient, hospital,, hospice,, residential, medical, facility, (2), Or, continuing, treatment, provider., Duration:, Eligible, workweeks",
    "fullLink":"../../../index.html",
    "height":"792",
    "width":" 612",
    "isWide":"False",
    "bookWidth":"612",
    "bookHeight":"792",

    "download":[{}
      ,{"pdfPublication":{"url":"../common/downloads_c167bf08/COPS_BULLETin.pdf", "size":"1.16 MB"}}
    
      ,{"PdfPage":{"url":"../common/downloads_c167bf08/page0013.pdf", "size":"433.24 KB"}}
     ],

    
    
    "substrate":{"background":"url(../common/page-substrates/page0013.png)",
    "backgroundSize":"contain", "backgroundColor":"#FFFFFF"},
    
    "leftTool":{"innerText":"12","src":"page12.html"},
    
    "rightTool":{"innerText":" 14","src":"page14.html"},
    
    "content":[{}
    
    ,{"className":"div_items", "top":"759","left":"181","width":"214","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font1","text":"C.O.P.S. Employee Handbook September 2013:  Page" 
    }}
  
    ,{"className":"div_items", "top":"759","left":"398","width":"10","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font2","text":"12" 
    }}
  
    ,{"className":"div_items", "top":"759","left":"410","width":"8","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font1","text":"of" 
    }}
  
    ,{"className":"div_items", "top":"759","left":"421","width":"10","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font2","text":"22" 
    }}
  
    ,{"className":"div_items", "top":"770","left":"36","width":"545","height":"10","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font3","text":"Downloaded or printed pages may be outdated.  If you printed or downloaded this page, please check the C.O.P.S. intranet to ensure you have the most recent version." 
    }}
  
    ,{"className":"div_items", "top":"33","left":"54","width":"61","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font10","text":"401(K) Plan" 
    }}
  
    ,{"className":"div_items", "top":"46","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"47","left":"90","width":"183","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"The qualifications for entry into the plan:" 
    }}
  
    ,{"className":"div_items", "top":"60","left":"104","width":"4","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font19","text":"▪" 
    }}
  
    ,{"className":"div_items", "top":"60","left":"117","width":"119","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"The employee must be 21." 
    }}
  
    ,{"className":"div_items", "top":"74","left":"104","width":"4","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font19","text":"▪" 
    }}
  
    ,{"className":"div_items", "top":"74","left":"117","width":"370","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Employee must be employed by the Company for minimum of twelve (12) months." 
    }}
  
    ,{"className":"div_items", "top":"87","left":"104","width":"4","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font19","text":"▪" 
    }}
  
    ,{"className":"div_items", "top":"87","left":"117","width":"314","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"1,000 hours must have been worked within those twelve (12) months." 
    }}
  
    ,{"className":"div_items", "top":"100","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"101","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Once these qualifications are met, the employee is eligible to enter the plan on the first day of the" 
    }}
  
    ,{"className":"div_items", "top":"114","left":"90","width":"87","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"subsequent month." 
    }}
  
    ,{"className":"div_items", "top":"127","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"128","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Upon entry into the plan, the employee is eligible for any Company contributions designated for that" 
    }}
  
    ,{"className":"div_items", "top":"141","left":"90","width":"22","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"year." 
    }}
  
    ,{"className":"div_items", "top":"154","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"155","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"The Company will make a matching contribution equal to 50% of the first 8% you contribute to the plan." 
    }}
  
    ,{"className":"div_items", "top":"168","left":"90","width":"368","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"You must contribute to the plan to receive the Company’s matching contributions." 
    }}
  
    ,{"className":"div_items", "top":"181","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"182","left":"90","width":"300","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"The employee contributions are pre-tax weekly payroll deductions." 
    }}
  
    ,{"className":"div_items", "top":"195","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"196","left":"90","width":"464","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Once the Company has employed the employee for three (3) years they will be 100% vested in the plan." 
    }}
  
    ,{"className":"div_items", "top":"219","left":"54","width":"122","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font10","text":"Tuition Reimbursement" 
    }}
  
    ,{"className":"div_items", "top":"232","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"The Company offers a pre-approved tuition reimbursement benefit for all full-time and part-time employees.  To" 
    }}
  
    ,{"className":"div_items", "top":"246","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"be eligible, you must be employed and apply for tuition reimbursement prior to the beginning of the school" 
    }}
  
    ,{"className":"div_items", "top":"259","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"semester that you are seeking reimbursement for.  Tuition reimbursement is considered for post-secondary" 
    }}
  
    ,{"className":"div_items", "top":"273","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"institutions in a course of study that is company related and where a grade of ′C′ or better is achieved.  Details" 
    }}
  
    ,{"className":"div_items", "top":"286","left":"54","width":"347","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"concerning this benefit are available from the Human Resources Department." 
    }}
  
    ,{"className":"div_items", "top":"309","left":"54","width":"89","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font10","text":"Bereavement Pay" 
    }}
  
    ,{"className":"div_items", "top":"322","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"323","left":"90","width":"128","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font28","text":"Immediate Family Member" 
    }}
  
    ,{"className":"div_items", "top":"323","left":"223","width":"335","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"(Mother, Father, Stepmother, Stepfather, Spouse, Daughter, Son, Sister," 
    }}
  
    ,{"className":"div_items", "top":"336","left":"90","width":"366","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Brother, Mother/Father–in-Law, and Sister/ Brother-in-Law):  Three (3) Paid Days." 
    }}
  
    ,{"className":"div_items", "top":"349","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"350","left":"90","width":"101","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font28","text":"Other Family Member" 
    }}
  
    ,{"className":"div_items", "top":"350","left":"194","width":"364","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"(Grandmother, Grandfather, Grandchildren, Aunt, Uncle, Niece, Nephew, and 1st" 
    }}
  
    ,{"className":"div_items", "top":"363","left":"90","width":"119","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Cousin):  One (1) Paid Day." 
    }}
  
    ,{"className":"div_items", "top":"387","left":"54","width":"48","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font10","text":"Jury Duty" 
    }}
  
    ,{"className":"div_items", "top":"399","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"400","left":"90","width":"375","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Please forward your summons for Jury Duty to your supervisor as soon as you get it." 
    }}
  
    ,{"className":"div_items", "top":"413","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"414","left":"90","width":"239","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"The Company will pay up to five (5) days of Jury Duty." 
    }}
  
    ,{"className":"div_items", "top":"427","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"428","left":"90","width":"446","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"You MUST forward your un-cashed endorsed Jury Duty check to your supervisor in order to be paid." 
    }}
  
    ,{"className":"div_items", "top":"451","left":"54","width":"163","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font10","text":"Family and Medical Leave Policy" 
    }}
  
    ,{"className":"div_items", "top":"463","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"464","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"The Family and Medical Leave Act (“FMLA”), requires employers of 50 or more employees to provide" 
    }}
  
    ,{"className":"div_items", "top":"478","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"employees with up to twelve (12) weeks of leave during any twelve (12) month period for certain" 
    }}
  
    ,{"className":"div_items", "top":"491","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"reasons including birth, adoption, or placement of a child; and to deal with an employee’s own serious" 
    }}
  
    ,{"className":"div_items", "top":"504","left":"90","width":"266","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"health conditions or those of an immediate family member." 
    }}
  
    ,{"className":"div_items", "top":"517","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"518","left":"90","width":"42","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font9","text":"Eligibility" 
    }}
  
    ,{"className":"div_items", "top":"518","left":"132","width":"3","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font29","text":":" 
    }}
  
    ,{"className":"div_items", "top":"518","left":"138","width":"420","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"All employees who have completed at least twelve (12) months of service and a minimum of" 
    }}
  
    ,{"className":"div_items", "top":"532","left":"90","width":"268","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"1,250 hours during the preceding twelve (12) month period." 
    }}
  
    ,{"className":"div_items", "top":"544","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"545","left":"90","width":"86","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font9","text":"Reasons for Leave:" 
    }}
  
    ,{"className":"div_items", "top":"545","left":"178","width":"261","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"An employee is entitled to leave for the following reasons:" 
    }}
  
    ,{"className":"div_items", "top":"559","left":"104","width":"4","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font19","text":"▪" 
    }}
  
    ,{"className":"div_items", "top":"559","left":"122","width":"118","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Due to the birth of a child;" 
    }}
  
    ,{"className":"div_items", "top":"572","left":"104","width":"4","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font19","text":"▪" 
    }}
  
    ,{"className":"div_items", "top":"572","left":"122","width":"353","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Due to the adoption of a child or placement with an employee of a foster child;" 
    }}
  
    ,{"className":"div_items", "top":"586","left":"104","width":"4","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font19","text":"▪" 
    }}
  
    ,{"className":"div_items", "top":"586","left":"122","width":"334","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"To care for a spouse, child or parent who has a serious health condition; or" 
    }}
  
    ,{"className":"div_items", "top":"599","left":"104","width":"4","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font19","text":"▪" 
    }}
  
    ,{"className":"div_items", "top":"599","left":"122","width":"436","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Due to the employee’s own serious health condition that renders him or her incapable of" 
    }}
  
    ,{"className":"div_items", "top":"613","left":"122","width":"188","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"performing the functions of his or her job." 
    }}
  
    ,{"className":"div_items", "top":"636","left":"54","width":"46","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font9","text":"Definition" 
    }}
  
    ,{"className":"div_items", "top":"636","left":"100","width":"3","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font29","text":":" 
    }}
  
    ,{"className":"div_items", "top":"636","left":"105","width":"453","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"A “serious health condition” is defined by the law as an illness, injury, impairment, or physical or" 
    }}
  
    ,{"className":"div_items", "top":"649","left":"54","width":"136","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"mental condition that involves" 
    }}
  
    ,{"className":"div_items", "top":"663","left":"90","width":"329","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"(1) Inpatient care in a hospital, hospice, or residential medical care facility" 
    }}
  
    ,{"className":"div_items", "top":"676","left":"90","width":"241","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"(2) Or continuing treatment by a health care provider." 
    }}
  
    ,{"className":"div_items", "top":"700","left":"54","width":"44","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font9","text":"Duration:" 
    }}
  
    ,{"className":"div_items", "top":"700","left":"100","width":"458","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Eligible employees are entitled to twelve (12) workweeks of leave during any twelve (12) month" 
    }}
  
    ,{"className":"div_items", "top":"713","left":"54","width":"32","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"period." 
    }}
      
    ]
})
 	