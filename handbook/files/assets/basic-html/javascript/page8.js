﻿
    pageContentLoad(
    {
    "currentPage":{"item":"page8", "num":"8"},
    "keywords":"C.O.P.S., Employee, Handbook, September, 2013:, Page, 7, of, 22, Downloaded, or, printed, pages, may, be, outdated., If, you, downloaded, this, page,, please, check, the, intranet, to, ensure, have, most, recent, version., How, use, time, clock, Shifts:, CLOCK, IN:, SWIPE, CARD/PRESS, GREEN, BUTTON, OUT:, CARD/, PRESS, RED, Meal, Breaks:, 4, 6, Authorized, Break, any, other, away, from, an, employee’s, work, station/area, for, non-work, related, reasons:, 1, 3, Attendance, /, Lateness, Policy, and, lateness, are, tracked, separately, by, six, (6), month, intervals., Accumulated, points, reset, zero, (0), at, end, each, respective, interval., Two, suspensions, within, twelve, (12), months, will, result, in, termination., Be, considerate, your, co-workers, our, customers, being, on, time., Absenteeism, Partial, Absence, Full-time:, 5, th, point:, st, written, warning, 2, nd, rd, one-day, decision, making, suspension, 8, Termination, Part-time:, Notes:, 1., employee, 2., When, third, (3rd), is, issued,, suspended, without, pay, next, working, day., (Suspension, not, encompass, day, before,, during,, after, a, holiday), 3., Failure, in/out, one-half, (½), point., 4., separately., 5., All, no, call/no, shows, cause, 6., changes, their, status, full, part, cut, half, unless, they, already, been, issued, warning,, then, would, taken, matching, that, occurrence., 7., Part, increased, occurrence, offenses, were, previously., 8., lateness,, partial, absence,, absences, shall, charged, against, (i), available, accrued, Personal, Leave, Time,, (ii), Vacation, Time, (collectively,, “Leave, Time”)., there, all, late, pay., Points, as, detailed, following, page:",
    "fullLink":"../../../index.html",
    "height":"792",
    "width":" 612",
    "isWide":"False",
    "bookWidth":"612",
    "bookHeight":"792",

    "download":[{}
      ,{"pdfPublication":{"url":"../common/downloads_c167bf08/COPS_BULLETin.pdf", "size":"1.16 MB"}}
    
      ,{"PdfPage":{"url":"../common/downloads_c167bf08/page0008.pdf", "size":"307.63 KB"}}
     ],

    
    
    "substrate":{"background":"url(../common/page-substrates/page0008.png)",
    "backgroundSize":"contain", "backgroundColor":"#FFFFFF"},
    
    "leftTool":{"innerText":"7","src":"page7.html"},
    
    "rightTool":{"innerText":" 9","src":"page9.html"},
    
    "content":[{}
    
    ,{"className":"div_items", "top":"759","left":"183","width":"214","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font1","text":"C.O.P.S. Employee Handbook September 2013:  Page" 
    }}
  
    ,{"className":"div_items", "top":"759","left":"400","width":"5","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font2","text":"7" 
    }}
  
    ,{"className":"div_items", "top":"759","left":"408","width":"8","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font1","text":"of" 
    }}
  
    ,{"className":"div_items", "top":"759","left":"418","width":"10","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font2","text":"22" 
    }}
  
    ,{"className":"div_items", "top":"770","left":"36","width":"545","height":"10","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font3","text":"Downloaded or printed pages may be outdated.  If you printed or downloaded this page, please check the C.O.P.S. intranet to ensure you have the most recent version." 
    }}
  
    ,{"className":"div_items", "top":"33","left":"54","width":"129","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font10","text":"How to use the time clock" 
    }}
  
    ,{"className":"div_items", "top":"46","left":"90","width":"28","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Shifts:" 
    }}
  
    ,{"className":"div_items", "top":"46","left":"198","width":"45","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"CLOCK IN:" 
    }}
  
    ,{"className":"div_items", "top":"46","left":"270","width":"161","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"SWIPE CARD/PRESS GREEN BUTTON" 
    }}
  
    ,{"className":"div_items", "top":"60","left":"198","width":"54","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"CLOCK OUT:" 
    }}
  
    ,{"className":"div_items", "top":"60","left":"270","width":"151","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"SWIPE CARD/ PRESS RED BUTTON" 
    }}
  
    ,{"className":"div_items", "top":"87","left":"90","width":"58","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Meal Breaks:" 
    }}
  
    ,{"className":"div_items", "top":"87","left":"198","width":"54","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"CLOCK OUT:" 
    }}
  
    ,{"className":"div_items", "top":"87","left":"270","width":"96","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"SWIPE CARD/PRESS 4" 
    }}
  
    ,{"className":"div_items", "top":"100","left":"198","width":"45","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"CLOCK IN:" 
    }}
  
    ,{"className":"div_items", "top":"100","left":"270","width":"96","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"SWIPE CARD/PRESS 6" 
    }}
  
    ,{"className":"div_items", "top":"127","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Authorized Break or any other time away from an employee’s work station/area for non-work related" 
    }}
  
    ,{"className":"div_items", "top":"140","left":"90","width":"38","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"reasons:" 
    }}
  
    ,{"className":"div_items", "top":"154","left":"198","width":"54","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"CLOCK OUT:" 
    }}
  
    ,{"className":"div_items", "top":"154","left":"270","width":"96","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"SWIPE CARD/PRESS 1" 
    }}
  
    ,{"className":"div_items", "top":"167","left":"198","width":"45","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"CLOCK IN:" 
    }}
  
    ,{"className":"div_items", "top":"167","left":"270","width":"96","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"SWIPE CARD/PRESS 3" 
    }}
  
    ,{"className":"div_items", "top":"197","left":"213","width":"186","height":"16","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font8","text":"Attendance / Lateness Policy" 
    }}
  
    ,{"className":"div_items", "top":"219","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Attendance and lateness are tracked separately by six (6) month intervals.  Accumulated points are reset to zero" 
    }}
  
    ,{"className":"div_items", "top":"233","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"(0) at the end of each respective six (6) month interval. Two suspensions within twelve (12) months will result in" 
    }}
  
    ,{"className":"div_items", "top":"246","left":"54","width":"378","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"termination. Be considerate of your co-workers and our customers by being on time." 
    }}
  
    ,{"className":"div_items", "top":"265","left":"54","width":"210","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font10","text":"Lateness / Absenteeism / Partial Absence" 
    }}
  
    ,{"className":"div_items", "top":"281","left":"90","width":"44","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font9","text":"Full-time:" 
    }}
  
    ,{"className":"div_items", "top":"295","left":"90","width":"6","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"5" 
    }}
  
    ,{"className":"div_items", "top":"293","left":"96","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font25","text":"th" 
    }}
  
    ,{"className":"div_items", "top":"295","left":"104","width":"27","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"point:" 
    }}
  
    ,{"className":"div_items", "top":"295","left":"162","width":"6","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"1" 
    }}
  
    ,{"className":"div_items", "top":"293","left":"168","width":"5","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font25","text":"st" 
    }}
  
    ,{"className":"div_items", "top":"295","left":"175","width":"72","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"written warning" 
    }}
  
    ,{"className":"div_items", "top":"308","left":"90","width":"6","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"6" 
    }}
  
    ,{"className":"div_items", "top":"306","left":"96","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font25","text":"th" 
    }}
  
    ,{"className":"div_items", "top":"308","left":"104","width":"27","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"point:" 
    }}
  
    ,{"className":"div_items", "top":"308","left":"162","width":"6","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"2" 
    }}
  
    ,{"className":"div_items", "top":"306","left":"168","width":"7","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font25","text":"nd" 
    }}
  
    ,{"className":"div_items", "top":"308","left":"177","width":"72","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"written warning" 
    }}
  
    ,{"className":"div_items", "top":"322","left":"90","width":"6","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"7" 
    }}
  
    ,{"className":"div_items", "top":"320","left":"96","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font25","text":"th" 
    }}
  
    ,{"className":"div_items", "top":"322","left":"104","width":"27","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"point:" 
    }}
  
    ,{"className":"div_items", "top":"322","left":"162","width":"6","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"3" 
    }}
  
    ,{"className":"div_items", "top":"320","left":"168","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font25","text":"rd" 
    }}
  
    ,{"className":"div_items", "top":"322","left":"176","width":"257","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"written warning and one-day decision making suspension" 
    }}
  
    ,{"className":"div_items", "top":"335","left":"90","width":"6","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"8" 
    }}
  
    ,{"className":"div_items", "top":"333","left":"96","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font25","text":"th" 
    }}
  
    ,{"className":"div_items", "top":"335","left":"104","width":"27","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"point:" 
    }}
  
    ,{"className":"div_items", "top":"335","left":"162","width":"55","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Termination" 
    }}
  
    ,{"className":"div_items", "top":"352","left":"90","width":"46","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font9","text":"Part-time:" 
    }}
  
    ,{"className":"div_items", "top":"365","left":"90","width":"6","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"3" 
    }}
  
    ,{"className":"div_items", "top":"363","left":"96","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font25","text":"rd" 
    }}
  
    ,{"className":"div_items", "top":"365","left":"104","width":"27","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"point:" 
    }}
  
    ,{"className":"div_items", "top":"365","left":"162","width":"6","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"1" 
    }}
  
    ,{"className":"div_items", "top":"363","left":"168","width":"5","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font25","text":"st" 
    }}
  
    ,{"className":"div_items", "top":"365","left":"175","width":"72","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"written warning" 
    }}
  
    ,{"className":"div_items", "top":"378","left":"90","width":"6","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"4" 
    }}
  
    ,{"className":"div_items", "top":"376","left":"96","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font25","text":"th" 
    }}
  
    ,{"className":"div_items", "top":"378","left":"104","width":"27","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"point:" 
    }}
  
    ,{"className":"div_items", "top":"378","left":"162","width":"6","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"2" 
    }}
  
    ,{"className":"div_items", "top":"376","left":"168","width":"7","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font25","text":"nd" 
    }}
  
    ,{"className":"div_items", "top":"378","left":"177","width":"72","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"written warning" 
    }}
  
    ,{"className":"div_items", "top":"392","left":"90","width":"6","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"5" 
    }}
  
    ,{"className":"div_items", "top":"390","left":"96","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font25","text":"th" 
    }}
  
    ,{"className":"div_items", "top":"392","left":"104","width":"27","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"point:" 
    }}
  
    ,{"className":"div_items", "top":"392","left":"162","width":"6","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"3" 
    }}
  
    ,{"className":"div_items", "top":"390","left":"168","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font25","text":"rd" 
    }}
  
    ,{"className":"div_items", "top":"392","left":"176","width":"257","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"written warning and one-day decision making suspension" 
    }}
  
    ,{"className":"div_items", "top":"405","left":"90","width":"6","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"6" 
    }}
  
    ,{"className":"div_items", "top":"403","left":"96","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font25","text":"th" 
    }}
  
    ,{"className":"div_items", "top":"405","left":"104","width":"27","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"point:" 
    }}
  
    ,{"className":"div_items", "top":"405","left":"162","width":"55","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Termination" 
    }}
  
    ,{"className":"div_items", "top":"423","left":"54","width":"29","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Notes:" 
    }}
  
    ,{"className":"div_items", "top":"436","left":"72","width":"8","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"1." 
    }}
  
    ,{"className":"div_items", "top":"436","left":"108","width":"358","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Two suspensions within twelve (12) months will result in employee termination." 
    }}
  
    ,{"className":"div_items", "top":"452","left":"72","width":"8","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"2." 
    }}
  
    ,{"className":"div_items", "top":"452","left":"108","width":"450","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"When the third (3rd) written warning is issued, the employee will be suspended without pay on the" 
    }}
  
    ,{"className":"div_items", "top":"466","left":"108","width":"80","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"next working day." 
    }}
  
    ,{"className":"div_items", "top":"466","left":"193","width":"337","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font9","text":"(Suspension will not encompass the day before, during, or after a holiday)" 
    }}
  
    ,{"className":"div_items", "top":"482","left":"72","width":"8","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"3." 
    }}
  
    ,{"className":"div_items", "top":"482","left":"108","width":"240","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Failure to clock in/out will result in one-half (½) point." 
    }}
  
    ,{"className":"div_items", "top":"499","left":"72","width":"8","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"4." 
    }}
  
    ,{"className":"div_items", "top":"499","left":"108","width":"232","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Absence and lateness points are tracked separately." 
    }}
  
    ,{"className":"div_items", "top":"515","left":"72","width":"8","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"5." 
    }}
  
    ,{"className":"div_items", "top":"515","left":"108","width":"208","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"All no call/no shows are cause for termination." 
    }}
  
    ,{"className":"div_items", "top":"532","left":"72","width":"8","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"6." 
    }}
  
    ,{"className":"div_items", "top":"532","left":"108","width":"450","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"When an employee changes their status from full time to part time their points are cut in half unless" 
    }}
  
    ,{"className":"div_items", "top":"545","left":"108","width":"450","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"they have already been issued a warning, then they would be taken to the points matching that" 
    }}
  
    ,{"className":"div_items", "top":"559","left":"108","width":"53","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"occurrence." 
    }}
  
    ,{"className":"div_items", "top":"575","left":"72","width":"8","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"7." 
    }}
  
    ,{"className":"div_items", "top":"575","left":"108","width":"450","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"When an employee changes their status from Part time to full time their points are increased to the" 
    }}
  
    ,{"className":"div_items", "top":"588","left":"108","width":"262","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"occurrence matching the offenses they were at previously." 
    }}
  
    ,{"className":"div_items", "top":"605","left":"72","width":"8","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"8." 
    }}
  
    ,{"className":"div_items", "top":"605","left":"108","width":"450","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"All lateness, partial absence, and absences shall be charged against (i) available accrued Personal" 
    }}
  
    ,{"className":"div_items", "top":"618","left":"108","width":"450","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Leave Time, and (ii) available accrued Vacation Leave Time (collectively, “Leave Time”).  If there is no" 
    }}
  
    ,{"className":"div_items", "top":"632","left":"108","width":"450","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"available accrued Leave Time, all absences and late time shall be without pay.  Points will be charged" 
    }}
  
    ,{"className":"div_items", "top":"645","left":"108","width":"450","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"against the employee for all lateness, partial absence, and absences as detailed on the following" 
    }}
  
    ,{"className":"div_items", "top":"659","left":"108","width":"25","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"page:" 
    }}
      
    ]
})
 	