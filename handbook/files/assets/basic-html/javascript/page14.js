﻿
    pageContentLoad(
    {
    "currentPage":{"item":"page14", "num":"14"},
    "keywords":"C.O.P.S., Employee, Handbook, September, 2013:, Page, 13, of, 22, Downloaded, or, printed, pages, may, be, outdated., If, you, downloaded, this, page,, please, check, the, intranet, to, ensure, have, most, recent, version., An, employee’s, available, accrued, vacation, and, sick, leave,, if, any,, must, used, substituted, for, part, twelve, (12), week, leave, period., personal, Once, any, such, paid, is, up,, remainder, weeks, will, unpaid., , Intermittent, Leave, Reduced, Schedule:, Where, taken, birth, placement, a, child, adoption, foster, care,, not, intermittently, on, reduced, schedule, unless, employee, Company, agree, otherwise., However,, where, care, family, member, due, own, serious, health, condition,, when, medically, necessary., In, cases,, Company’s, agreement, required., Notice:, necessity, foreseeable, expected, child,, provide, at, least, 30, days, notice, intention, take, leave., date, requires, begin, in, less, than, days,, as, soon, practicable., (1), member’s, condition, (2), based, planned, medical, treatment,, must:, ▪, Give, (or, practicable,, treatment, starts, days.), Make, reasonable, effort, so, unduly, disrupt, operations, Company,, subject, approval, provider., need, unforeseeable,, required, Required, Certification:, Any, request, supported, by, certification, The, copy, timely, manner., contain:, began;, probable, duration, condition;, appropriate, facts, regarding, spouse,, parent,, statement, that, needed, an, estimate, amount, time, continue;, unable, perform, functions, his, her, job;, case, intermittent, dates, given, treatment., require, later, recertifications, basis., addition,, report, status, periodically, concerning, return, work., Further,, taking, obtain, he, she, able, resume, Continuation, Medical, Coverage:, maintain, coverage, under, group, plan, who, FMLA., continued, same, level, conditions, would, been, provided, no, had, taken., fails, work, after, period, expires,, recover, premium, during, Exceptions, exist, does, work:, continuation,, recurrence,, onset, immediate, otherwise, entitle, other, circumstances, beyond, control, employee., either, exception, applies,, effect, provider",
    "fullLink":"../../../index.html",
    "height":"792",
    "width":" 612",
    "isWide":"False",
    "bookWidth":"612",
    "bookHeight":"792",

    "download":[{}
      ,{"pdfPublication":{"url":"../common/downloads_c167bf08/COPS_BULLETin.pdf", "size":"1.16 MB"}}
    
      ,{"PdfPage":{"url":"../common/downloads_c167bf08/page0014.pdf", "size":"216.70 KB"}}
     ],

    
    
    "substrate":{"background":"url(../common/page-substrates/page0014.png)",
    "backgroundSize":"contain", "backgroundColor":"#FFFFFF"},
    
    "leftTool":{"innerText":"13","src":"page13.html"},
    
    "rightTool":{"innerText":" 15","src":"page15.html"},
    
    "content":[{}
    
    ,{"className":"div_items", "top":"759","left":"181","width":"214","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font1","text":"C.O.P.S. Employee Handbook September 2013:  Page" 
    }}
  
    ,{"className":"div_items", "top":"759","left":"398","width":"10","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font2","text":"13" 
    }}
  
    ,{"className":"div_items", "top":"759","left":"410","width":"8","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font1","text":"of" 
    }}
  
    ,{"className":"div_items", "top":"759","left":"421","width":"10","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font2","text":"22" 
    }}
  
    ,{"className":"div_items", "top":"770","left":"36","width":"545","height":"10","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font3","text":"Downloaded or printed pages may be outdated.  If you printed or downloaded this page, please check the C.O.P.S. intranet to ensure you have the most recent version." 
    }}
  
    ,{"className":"div_items", "top":"34","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"An employee’s available accrued vacation and sick leave, if any, must be used and substituted for part of the" 
    }}
  
    ,{"className":"div_items", "top":"47","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"twelve (12) week leave period.  An employee’s available accrued personal leave may be used and substituted for" 
    }}
  
    ,{"className":"div_items", "top":"60","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"part of the twelve (12) week leave period.  Once any such paid leave is used up, the remainder of the twelve (12)" 
    }}
  
    ,{"className":"div_items", "top":"74","left":"54","width":"134","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"weeks of leave will be unpaid." 
    }}
  
    ,{"className":"div_items", "top":"97","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"98","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Intermittent Leave or Reduced Leave Schedule: Where leave is taken for the birth or placement of a" 
    }}
  
    ,{"className":"div_items", "top":"111","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"child for adoption or foster care, the leave may not be taken intermittently or on a reduced leave" 
    }}
  
    ,{"className":"div_items", "top":"124","left":"90","width":"294","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"schedule unless the employee and the Company agree otherwise." 
    }}
  
    ,{"className":"div_items", "top":"144","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"However, where leave is taken to care for a sick family member or due to the employee’s own serious" 
    }}
  
    ,{"className":"div_items", "top":"157","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"health condition, leave may be taken intermittently or on a reduced schedule when medically necessary." 
    }}
  
    ,{"className":"div_items", "top":"171","left":"90","width":"252","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"In such cases, the Company’s agreement is not required." 
    }}
  
    ,{"className":"div_items", "top":"189","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"190","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Notice: Where the necessity for leave is foreseeable due to the expected birth or placement of a child," 
    }}
  
    ,{"className":"div_items", "top":"204","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"the employee must provide at least 30 days notice of the employee’s intention to take leave.  If the date" 
    }}
  
    ,{"className":"div_items", "top":"217","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"of birth or placement requires leave to begin in less than 30 days, the employee must provide such" 
    }}
  
    ,{"className":"div_items", "top":"231","left":"90","width":"131","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"notice as soon as practicable." 
    }}
  
    ,{"className":"div_items", "top":"250","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"251","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Where the necessity for leave (1) is due to a family member’s or the employee’s own serious health" 
    }}
  
    ,{"className":"div_items", "top":"264","left":"90","width":"404","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"condition and (2) is foreseeable based on planned medical treatment, the employee must:" 
    }}
  
    ,{"className":"div_items", "top":"277","left":"112","width":"4","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font19","text":"▪" 
    }}
  
    ,{"className":"div_items", "top":"277","left":"126","width":"432","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Give at least 30 days notice (or notice as soon as practicable, if treatment starts in less than 30" 
    }}
  
    ,{"className":"div_items", "top":"291","left":"126","width":"26","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"days.)" 
    }}
  
    ,{"className":"div_items", "top":"304","left":"112","width":"4","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font19","text":"▪" 
    }}
  
    ,{"className":"div_items", "top":"304","left":"126","width":"432","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Make a reasonable effort to schedule the treatment so as not to unduly disrupt the operations" 
    }}
  
    ,{"className":"div_items", "top":"318","left":"126","width":"305","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"of the Company, subject to the approval of the health care provider." 
    }}
  
    ,{"className":"div_items", "top":"331","left":"112","width":"4","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font19","text":"▪" 
    }}
  
    ,{"className":"div_items", "top":"331","left":"126","width":"377","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Where the need for leave is unforeseeable, notice is required as soon as practicable." 
    }}
  
    ,{"className":"div_items", "top":"357","left":"72","width":"106","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font9","text":"Required Certification:" 
    }}
  
    ,{"className":"div_items", "top":"357","left":"184","width":"374","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Any leave request based on a family member’s or the employee’s own serious" 
    }}
  
    ,{"className":"div_items", "top":"370","left":"72","width":"486","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"health condition must be supported by certification by a health care provider.  The employee must provide a" 
    }}
  
    ,{"className":"div_items", "top":"383","left":"72","width":"412","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"copy of the certification to the Company in a timely manner.  The certification must contain:" 
    }}
  
    ,{"className":"div_items", "top":"397","left":"112","width":"4","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font19","text":"▪" 
    }}
  
    ,{"className":"div_items", "top":"397","left":"126","width":"200","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"The date the serious health condition began;" 
    }}
  
    ,{"className":"div_items", "top":"410","left":"112","width":"4","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font19","text":"▪" 
    }}
  
    ,{"className":"div_items", "top":"410","left":"126","width":"177","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"The probable duration of the condition;" 
    }}
  
    ,{"className":"div_items", "top":"424","left":"112","width":"4","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font19","text":"▪" 
    }}
  
    ,{"className":"div_items", "top":"424","left":"126","width":"244","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"The appropriate medical facts regarding the condition;" 
    }}
  
    ,{"className":"div_items", "top":"437","left":"112","width":"4","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font19","text":"▪" 
    }}
  
    ,{"className":"div_items", "top":"437","left":"126","width":"432","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"If the leave is based on care of a spouse, child, or parent, a statement that the employee is" 
    }}
  
    ,{"className":"div_items", "top":"451","left":"126","width":"423","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"needed to provide the care and an estimate of the amount of time that the need will continue;" 
    }}
  
    ,{"className":"div_items", "top":"464","left":"112","width":"4","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font19","text":"▪" 
    }}
  
    ,{"className":"div_items", "top":"464","left":"126","width":"432","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"If the leave is based on the employee’s own serious health condition, a statement that the" 
    }}
  
    ,{"className":"div_items", "top":"477","left":"126","width":"295","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"employee is unable to perform the functions of his or her job; and" 
    }}
  
    ,{"className":"div_items", "top":"491","left":"112","width":"4","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font19","text":"▪" 
    }}
  
    ,{"className":"div_items", "top":"491","left":"126","width":"432","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"In the case of intermittent leave or leave on a reduced schedule for planned medical treatment," 
    }}
  
    ,{"className":"div_items", "top":"504","left":"126","width":"370","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"the dates the treatment is expected to be given and the duration of the treatment." 
    }}
  
    ,{"className":"div_items", "top":"523","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"524","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"The Company may require the employee to provide later recertifications on a reasonable basis.  In" 
    }}
  
    ,{"className":"div_items", "top":"537","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"addition, an employee on leave must report on his or her status periodically and concerning his or her" 
    }}
  
    ,{"className":"div_items", "top":"551","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"intention to return to work.  Further, an employee taking leave due to his or her own serious health" 
    }}
  
    ,{"className":"div_items", "top":"564","left":"90","width":"376","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"condition must obtain medical certification that he or she is able to resume to work." 
    }}
  
    ,{"className":"div_items", "top":"583","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"584","left":"90","width":"162","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font9","text":"Continuation of Medical Coverage:" 
    }}
  
    ,{"className":"div_items", "top":"584","left":"254","width":"304","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"The Company will maintain coverage under any group health plan" 
    }}
  
    ,{"className":"div_items", "top":"597","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"for any employee who is taking family or medical leave under the FMLA.  The coverage will be continued" 
    }}
  
    ,{"className":"div_items", "top":"611","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"for the duration of the leave, at the same level and under the same conditions coverage would have" 
    }}
  
    ,{"className":"div_items", "top":"624","left":"90","width":"188","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"been provided if no leave had been taken." 
    }}
  
    ,{"className":"div_items", "top":"644","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"However, if the employee fails to return to work after the period of leave expires, the Company may" 
    }}
  
    ,{"className":"div_items", "top":"657","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"recover the premium the Company paid for coverage during the leave period.  Exceptions exist where" 
    }}
  
    ,{"className":"div_items", "top":"671","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"the employee does not return to work: (1) due to the continuation, recurrence, or onset of a serious" 
    }}
  
    ,{"className":"div_items", "top":"684","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"health condition of an immediate family member or the employee that would otherwise entitle the" 
    }}
  
    ,{"className":"div_items", "top":"697","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"employee to take leave or (2) due to other circumstances beyond the control of the employee.  If either" 
    }}
  
    ,{"className":"div_items", "top":"711","left":"90","width":"388","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"exception applies, certification to that effect by a health care provider will be required." 
    }}
      
    ]
})
 	