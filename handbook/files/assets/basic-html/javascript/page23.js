﻿
    pageContentLoad(
    {
    "currentPage":{"item":"page23", "num":"23"},
    "keywords":"C.O.P.S., Employee, Handbook, September, 2013:, Page, 22, of, Downloaded, or, printed, pages, may, be, outdated., If, you, downloaded, this, page,, please, check, the, intranet, to, ensure, have, most, recent, version., Understanding, and, acknowledgement, company, employee, handbook, Please, read, following, statements, sign, below., &, Acknowledging, Access, COMPANY, EMPLOYEE, HANDBOOK, *, I, been, provided, access, review, a, copy, Handbook., understand, that, except, for, employment, at-will, policy,, policies, benefits, described, in, it, are, subject, change, at, sole, discretion, The, Company, any, time., also, acknowledge, that,, policy, employment,, terms, conditions, with, modified, without, cause, notice, No, implied, contract, concerning, employment-, related, decision, term, condition, can, established, by, other, statement,, conduct,, practice., At, Will, Employment, further, my, is, will,, neither, nor, entered, into, regarding, duration, employment., am, free, terminate, time,, reason., Likewise,, has, right, otherwise, discipline,, transfer,, promote, demote, me, reason,, Company., enter, an, specified, period, make, agreement, contrary, written, approval, from, President., Confidential, Information, aware, during, course, confidential, information, will, made, available, me,, instance,, customer, lists,, dealer, subscriber, information,, marketing, strategies,, pricing, information., proprietary, critical, success, must, not, given, out, used, outside, Company’s, premises, non-Company, employees., In, event, termination, whether, voluntary, involuntary,, hereby, agree, retain,, utilize, exploit, individual, company., Additional, you,, see, your, manager, more, Employee’s, Printed, Name, Position, Signature, Date, Monitoring, online, t, http://copsmonitoring.com/employeehandbook, ., You, using, ID, Passcode, completion, training., A, Human, Resources, Office",
    "fullLink":"../../../index.html",
    "height":"792",
    "width":" 612",
    "isWide":"False",
    "bookWidth":"612",
    "bookHeight":"792",

    "download":[{}
      ,{"pdfPublication":{"url":"../common/downloads_c167bf08/COPS_BULLETin.pdf", "size":"1.16 MB"}}
    
      ,{"PdfPage":{"url":"../common/downloads_c167bf08/page0023.pdf", "size":"427.74 KB"}}
     ],

    
    
    "substrate":{"background":"url(../common/page-substrates/page0023.png)",
    "backgroundSize":"contain", "backgroundColor":"#FFFFFF"},
    
    "leftTool":{"innerText":"22","src":"page22.html"},
    
    "rightTool":{"innerText":" 24","src":"page24.html"},
    
    "content":[{}
    
    ,{"className":"div_items", "top":"759","left":"181","width":"214","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font1","text":"C.O.P.S. Employee Handbook September 2013:  Page" 
    }}
  
    ,{"className":"div_items", "top":"759","left":"398","width":"10","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font2","text":"22" 
    }}
  
    ,{"className":"div_items", "top":"759","left":"410","width":"8","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font1","text":"of" 
    }}
  
    ,{"className":"div_items", "top":"759","left":"421","width":"10","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font2","text":"22" 
    }}
  
    ,{"className":"div_items", "top":"770","left":"36","width":"545","height":"10","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font3","text":"Downloaded or printed pages may be outdated.  If you printed or downloaded this page, please check the C.O.P.S. intranet to ensure you have the most recent version." 
    }}
  
    ,{"className":"div_items", "top":"35","left":"103","width":"406","height":"22","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font4","text":"Understanding and acknowledgement of the" 
    }}
  
    ,{"className":"div_items", "top":"58","left":"168","width":"276","height":"22","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font4","text":"company employee handbook" 
    }}
  
    ,{"className":"div_items", "top":"104","left":"187","width":"238","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Please read the following statements and sign below." 
    }}
  
    ,{"className":"div_items", "top":"144","left":"54","width":"407","height":"14","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font13","text":"Understanding & Acknowledging Access to the COMPANY EMPLOYEE HANDBOOK" 
    }}
  
    ,{"className":"div_items", "top":"141","left":"461","width":"4","height":"10","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font14","text":"*" 
    }}
  
    ,{"className":"div_items", "top":"168","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"I have been provided access to read and review a copy of the C.O.P.S. Employee Handbook. I understand that" 
    }}
  
    ,{"className":"div_items", "top":"181","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"except for the employment at-will policy, the policies and benefits described in it are subject to change at the" 
    }}
  
    ,{"className":"div_items", "top":"195","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"sole discretion of The Company at any time.  I also acknowledge that, except for the policy of at-will" 
    }}
  
    ,{"className":"div_items", "top":"208","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"employment, terms and conditions of employment with The Company may be modified at the sole discretion of" 
    }}
  
    ,{"className":"div_items", "top":"222","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"The Company with or without cause or notice at any time.  No implied contract concerning any employment-" 
    }}
  
    ,{"className":"div_items", "top":"235","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"related decision or term and condition of employment can be established by any other statement, conduct," 
    }}
  
    ,{"className":"div_items", "top":"249","left":"54","width":"80","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"policy or practice." 
    }}
  
    ,{"className":"div_items", "top":"272","left":"54","width":"92","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font9","text":"At Will Employment" 
    }}
  
    ,{"className":"div_items", "top":"285","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"I further understand that my employment is at will, and neither The Company nor I have entered into a contract" 
    }}
  
    ,{"className":"div_items", "top":"299","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"regarding the duration of my employment.  I am free to terminate my employment with The Company at any" 
    }}
  
    ,{"className":"div_items", "top":"312","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"time, with or without reason.  Likewise, The Company has the right to terminate my employment or otherwise" 
    }}
  
    ,{"className":"div_items", "top":"326","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"discipline, transfer, promote or demote me at any time, with or without reason, at the discretion of The" 
    }}
  
    ,{"className":"div_items", "top":"339","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Company.  No employee of The Company can enter into an employment contract for a specified period of time," 
    }}
  
    ,{"className":"div_items", "top":"352","left":"54","width":"428","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"or make any agreement contrary to this policy without the written approval from the President." 
    }}
  
    ,{"className":"div_items", "top":"376","left":"54","width":"113","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font9","text":"Confidential Information" 
    }}
  
    ,{"className":"div_items", "top":"389","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"I am aware that during the course of my employment confidential information will be made available to me, for" 
    }}
  
    ,{"className":"div_items", "top":"403","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"instance, customer lists, dealer and subscriber information, marketing strategies, pricing policies and other" 
    }}
  
    ,{"className":"div_items", "top":"416","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"related information.  I understand that this information is proprietary and critical to the success of The Company" 
    }}
  
    ,{"className":"div_items", "top":"430","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"and must not be given out or used outside of The Company’s premises or with non-Company employees.  In the" 
    }}
  
    ,{"className":"div_items", "top":"443","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"event of termination of employment, whether voluntary or involuntary, I hereby agree not to retain, utilize or" 
    }}
  
    ,{"className":"div_items", "top":"457","left":"54","width":"277","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"exploit this information with any other individual or company." 
    }}
  
    ,{"className":"div_items", "top":"480","left":"102","width":"408","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font15","text":"Additional benefits may be available to you, please see your manager for more information." 
    }}
  
    ,{"className":"div_items", "top":"552","left":"54","width":"154","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Employee’s Printed Name Position" 
    }}
  
    ,{"className":"div_items", "top":"624","left":"54","width":"139","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Employee’s Signature and Date" 
    }}
  
    ,{"className":"div_items", "top":"672","left":"45","width":"6","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font16","text":"*" 
    }}
  
    ,{"className":"div_items", "top":"672","left":"50","width":"292","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"The C.O.P.S. Monitoring Employee Handbook is available online a" 
    }}
  
    ,{"className":"div_items", "top":"672","left":"342","width":"4","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","content":{
    "tag":"a", "href":"http://copsmonitoring.com/employeehandbook","text":"t",
    "useGlobal":"True","hightLightColor":"rgba(,,,)","target":""}

    }}
  
    ,{"className":"div_items", "top":"672","left":"348","width":"216","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font17","content":{
    "tag":"a", "href":"http://copsmonitoring.com/employeehandbook","text":"http://copsmonitoring.com/employeehandbook",
    "useGlobal":"True","hightLightColor":"rgba(,,,)","target":""}

    }}
  
    ,{"className":"div_items", "top":"672","left":"564","width":"3","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","content":{
    "tag":"a", "href":"http://copsmonitoring.com/employeehandbook","text":".",
    "useGlobal":"True","hightLightColor":"rgba(,,,)","target":""}

    }}
  
    ,{"className":"div_items", "top":"688","left":"60","width":"493","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"You can access the handbook using your Employee ID and Passcode given to you at the completion of training." 
    }}
  
    ,{"className":"div_items", "top":"703","left":"129","width":"355","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"A printed copy of the handbook is also available at the Human Resources Office" 
    }}
      
    ]
})
 	