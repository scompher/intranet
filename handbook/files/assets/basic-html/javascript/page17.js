﻿
    pageContentLoad(
    {
    "currentPage":{"item":"page17", "num":"17"},
    "keywords":"C.O.P.S., Employee, Handbook, September, 2013:, Page, 16, of, 22, Downloaded, or, printed, pages, may, be, outdated., If, you, downloaded, this, page,, please, check, the, intranet, to, ensure, have, most, recent, version., Additional, Guidelines, and, Information, Severe, Weather, Procedures, , an, employee, is, expected, absent, late,, he/she, should, call, their, immediate, supervisor, as, soon, possible, report, status., Employees, who, out, do, not, show, for, shift, will, receive, one, absence, point., Vacation, time, taken, make, up, missed, hours, due, absence,, those, employees, with, accrued, hours., (hourly, salaried), vacation, available, paid, work, after, scheduled, start, charged, a, lateness, point,, unless, management, subsequently, waives, point, at, discretion., Casual, dress, acceptable, all, work., road, conditions, permit,, attempt, shuttle, to/from, home., However,, no, guarantees, can, made, that, picked, number, factors, include, severity, conditions,, day,, proximity, office,, etc., For, are, shuttled,, makes, guarantee, before,, home, conclusion, of,, one’s, shift., requesting, following, priority:, ▪, Essential, (operations/dispatchers), highest, priority, when, necessary., General, office, support, staff, considered, only, if, operations, fully, staffed,, general, meet, arranged, location, where, essential, shuttled., appreciates, employee’s, individual, efforts, commute, in, less, than, ideal., we, ask, common, sense, caution, used, during, hazardous, conditions., Prohibiting, Harassment, The, Company, committed, maintaining, environment, free, discrimination, harassment., In, keeping, commitment,, tolerate, harassment, by, anyone,, including, any, supervisor,, co-worker,, vendor,, client,, customer, Company., consists, unwelcome, conduct,, whether, verbal,, physical,, visual,, based, upon, person’s, protected, status,, such, sex,, color,, race,, ancestry,, religion,, national, origin,, age,, physical, handicap,, medical, condition,, disability,, marital, veteran, citizenship, other, group, harassing, conduct, affects, tangible, job, benefits,, interferes, unreasonably, individual’s, performance,, creates, intimidating, environment., Sexual, violation, state, federal, law., Unwelcome, sexual, advances,, request, favors,, visual, on, sex, constitute, (1), submission, explicit, implicit, term, condition, employment,, (2), rejection, basis, employment, decision,, (3), has, purpose, effect, interfering, working, propositions,, innuendo,, suggestive, comments,, sexually, oriented, “kidding”, “teasing,”, “practical, jokes,”, about, gender-specific, traits,, foul, obscene, language, gestures,, displays, material,, contact,, patting,, pinching,, brushing, against, another’s, body., All, responsible, helping, avoid, feel, experienced, witnessed, harassment,, notify, President, (800-367-8801), CEO, (212-223-4242), Monitoring, immediately., forbids, retaliation, anyone, complains, provides, information, complaint., Company’s, policy, investigate, complaints, thoroughly, promptly., To, fullest, extent, practical,, keep, terms, resolution, confidential., investigation, confirms, occurred,, take, corrective, action,, discipline, termination, appropriate.",
    "fullLink":"../../../index.html",
    "height":"792",
    "width":" 612",
    "isWide":"False",
    "bookWidth":"612",
    "bookHeight":"792",

    "download":[{}
      ,{"pdfPublication":{"url":"../common/downloads_c167bf08/COPS_BULLETin.pdf", "size":"1.16 MB"}}
    
      ,{"PdfPage":{"url":"../common/downloads_c167bf08/page0017.pdf", "size":"323.61 KB"}}
     ],

    
    
    "substrate":{"background":"url(../common/page-substrates/page0017.png)",
    "backgroundSize":"contain", "backgroundColor":"#FFFFFF"},
    
    "leftTool":{"innerText":"16","src":"page16.html"},
    
    "rightTool":{"innerText":" 18","src":"page18.html"},
    
    "content":[{}
    
    ,{"className":"div_items", "top":"759","left":"181","width":"214","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font1","text":"C.O.P.S. Employee Handbook September 2013:  Page" 
    }}
  
    ,{"className":"div_items", "top":"759","left":"398","width":"10","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font2","text":"16" 
    }}
  
    ,{"className":"div_items", "top":"759","left":"410","width":"8","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font1","text":"of" 
    }}
  
    ,{"className":"div_items", "top":"759","left":"421","width":"10","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font2","text":"22" 
    }}
  
    ,{"className":"div_items", "top":"770","left":"36","width":"545","height":"10","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font3","text":"Downloaded or printed pages may be outdated.  If you printed or downloaded this page, please check the C.O.P.S. intranet to ensure you have the most recent version." 
    }}
  
    ,{"className":"div_items", "top":"35","left":"129","width":"355","height":"22","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font4","text":"Additional Guidelines and Information" 
    }}
  
    ,{"className":"div_items", "top":"69","left":"216","width":"180","height":"16","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font8","text":"Severe Weather Procedures" 
    }}
  
    ,{"className":"div_items", "top":"91","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"92","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"If an employee is expected to be absent or late, he/she should call their immediate supervisor as soon as" 
    }}
  
    ,{"className":"div_items", "top":"105","left":"90","width":"135","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"possible to report their status." 
    }}
  
    ,{"className":"div_items", "top":"118","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"119","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Employees who call out or do not show for their shift will receive one absence point.  Vacation time will" 
    }}
  
    ,{"className":"div_items", "top":"132","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"be taken to make up missed hours due to absence, for those employees with accrued hours.  Employees" 
    }}
  
    ,{"className":"div_items", "top":"146","left":"90","width":"427","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"(hourly and salaried) who do not have vacation time available will not be paid for missed hours." 
    }}
  
    ,{"className":"div_items", "top":"159","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"160","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Employees who report to work after the scheduled start of their shift will be charged a lateness point," 
    }}
  
    ,{"className":"div_items", "top":"173","left":"90","width":"313","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"unless management subsequently waives the point at their discretion." 
    }}
  
    ,{"className":"div_items", "top":"186","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"187","left":"90","width":"288","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Casual dress is acceptable for all employees who report to work." 
    }}
  
    ,{"className":"div_items", "top":"199","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"200","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"If road conditions permit, C.O.P.S. may attempt to shuttle employees to/from their home.  However, no" 
    }}
  
    ,{"className":"div_items", "top":"214","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"guarantees can be made that an employee will be picked up due to a number of factors that include" 
    }}
  
    ,{"className":"div_items", "top":"227","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"severity of road conditions, time of day, proximity to office, etc.  For those employees that are shuttled," 
    }}
  
    ,{"className":"div_items", "top":"241","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"C.O.P.S. makes no guarantee that the employee will be picked up before, or taken home at the" 
    }}
  
    ,{"className":"div_items", "top":"254","left":"90","width":"411","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"conclusion of, one’s shift.  Employees requesting a shuttle will receive the following priority:" 
    }}
  
    ,{"className":"div_items", "top":"268","left":"108","width":"4","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font19","text":"▪" 
    }}
  
    ,{"className":"div_items", "top":"268","left":"126","width":"432","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Essential employees (operations/dispatchers) will receive the highest priority when a shuttle is" 
    }}
  
    ,{"className":"div_items", "top":"281","left":"126","width":"47","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"necessary." 
    }}
  
    ,{"className":"div_items", "top":"294","left":"108","width":"4","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font19","text":"▪" 
    }}
  
    ,{"className":"div_items", "top":"294","left":"129","width":"429","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"General office and support staff employees will be considered for shuttle only if operations is" 
    }}
  
    ,{"className":"div_items", "top":"308","left":"126","width":"432","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"fully staffed, or a general office employee can meet at an arranged location where an essential" 
    }}
  
    ,{"className":"div_items", "top":"321","left":"126","width":"122","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"employee is to be shuttled." 
    }}
  
    ,{"className":"div_items", "top":"345","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"C.O.P.S. appreciates an employee’s individual efforts to commute to/from work in conditions that are less than" 
    }}
  
    ,{"className":"div_items", "top":"358","left":"54","width":"421","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"ideal.  However, we ask that common sense and caution be used during hazardous conditions." 
    }}
  
    ,{"className":"div_items", "top":"382","left":"193","width":"226","height":"16","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font8","text":"Guidelines Prohibiting Harassment" 
    }}
  
    ,{"className":"div_items", "top":"403","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"404","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"The Company is committed to maintaining a work environment that is free of discrimination and" 
    }}
  
    ,{"className":"div_items", "top":"418","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"harassment.  In keeping with this commitment, we will not tolerate harassment of C.O.P.S. employees" 
    }}
  
    ,{"className":"div_items", "top":"431","left":"90","width":"414","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"by anyone, including any supervisor, co-worker, vendor, client, or customer of the Company." 
    }}
  
    ,{"className":"div_items", "top":"444","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"445","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Harassment consists of unwelcome conduct, whether verbal, physical, or visual, that is based upon a" 
    }}
  
    ,{"className":"div_items", "top":"458","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"person’s protected status, such as sex, color, race, ancestry, religion, national origin, age, physical" 
    }}
  
    ,{"className":"div_items", "top":"472","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"handicap, medical condition, disability, marital status, veteran status, citizenship status, or other" 
    }}
  
    ,{"className":"div_items", "top":"485","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"protected group status.  The Company will not tolerate harassing conduct that affects tangible job" 
    }}
  
    ,{"className":"div_items", "top":"499","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"benefits, that interferes unreasonably with an individual’s work performance, or that creates an" 
    }}
  
    ,{"className":"div_items", "top":"512","left":"90","width":"142","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"intimidating work environment." 
    }}
  
    ,{"className":"div_items", "top":"525","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"526","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Sexual harassment is a violation of state and federal law.  Unwelcome sexual advances, request for" 
    }}
  
    ,{"className":"div_items", "top":"539","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"sexual favors, and other physical, verbal, or visual conduct based on sex constitute sexual harassment" 
    }}
  
    ,{"className":"div_items", "top":"553","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"when (1) submission to the conduct is an explicit or implicit term or condition of employment, (2)" 
    }}
  
    ,{"className":"div_items", "top":"566","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"submission to or rejection of the conduct is used as the basis for an employment decision, or (3) the" 
    }}
  
    ,{"className":"div_items", "top":"580","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"conduct has the purpose or effect of unreasonably interfering with an individual’s working environment." 
    }}
  
    ,{"className":"div_items", "top":"593","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Sexual harassment may include explicit sexual propositions, sexual innuendo, suggestive comments," 
    }}
  
    ,{"className":"div_items", "top":"606","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"sexually oriented “kidding” or “teasing,” “practical jokes,” about gender-specific traits, foul or obscene" 
    }}
  
    ,{"className":"div_items", "top":"620","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"language or gestures, displays of foul or obscene printed or visual material, and physical contact, such as" 
    }}
  
    ,{"className":"div_items", "top":"633","left":"90","width":"239","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"patting, pinching, or brushing against another’s body." 
    }}
  
    ,{"className":"div_items", "top":"646","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"647","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"All Company employees are responsible for helping to ensure that we avoid harassment.  If you feel that" 
    }}
  
    ,{"className":"div_items", "top":"660","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"you have experienced or witnessed harassment, you are to notify the President (800-367-8801) or CEO" 
    }}
  
    ,{"className":"div_items", "top":"674","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"(212-223-4242) of C.O.P.S. Monitoring immediately.  The Company forbids retaliation against anyone" 
    }}
  
    ,{"className":"div_items", "top":"687","left":"90","width":"356","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"that complains of harassment or provides information with any such complaint." 
    }}
  
    ,{"className":"div_items", "top":"700","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"701","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"The Company’s policy is to investigate all such complaints thoroughly and promptly.  To the fullest" 
    }}
  
    ,{"className":"div_items", "top":"714","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"extent practical, the Company will keep complaints and the terms of their resolution confidential.  If an" 
    }}
  
    ,{"className":"div_items", "top":"728","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"investigation confirms that harassment has occurred, the Company will take corrective action, including" 
    }}
  
    ,{"className":"div_items", "top":"741","left":"90","width":"410","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"such discipline up to and including immediate termination of employment as is appropriate." 
    }}
      
    ]
})
 	