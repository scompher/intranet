﻿
    pageContentLoad(
    {
    "currentPage":{"item":"page5", "num":"5"},
    "keywords":"C.O.P.S., Employee, Handbook, September, 2013:, Page, 4, of, 22, Downloaded, or, printed, pages, may, be, outdated., If, you, downloaded, this, page,, please, check, the, intranet, to, ensure, have, most, recent, version., General, Company, Policies, and, Practices, Introduction, This, handbook, is, only, a, source, general, information, concerning, Company’s, policies,, procedures,, rules, for, employees, does, not, include, every, situation, that, could, occur, in, work, place., contains, no, promise, any, kind, intended, as, contract, employment., The, absolutely, free, change,, add, eliminate, policies, working, conditions, contained, at, time, without, notice, consultation, with, agreement, employee., At-Will, Employment, There, employment, between, its, employees., All, “at-will”, entered, into, voluntarily., Each, employee, has, absolute, right, terminate, (“quit”), good, cause, prior, notice,, (“fire”), notice., Equal, Opportunity, provides, equal, opportunities, all, applicants, We, comply, federal,, state, local, laws,, regulations, guidelines, regard, non-discrimination, against, job, on, basis, race,, religion,, color,, age,, sex,, national, origin,, veteran, status, non-job-related, handicap., include,, but, are, limited, to,, recruiting,, hiring,, training,, promotion,, transfer,, rates, pay, benefits., Employees, encouraged, bring, complaints, policy, promptly, attention, President, (800-367-8801), CEO, (212-223-4242), immediately., forbids, retaliation, anyone, complains, violation, opportunity, connection, such, complaint., On-the-job, involvement, drugs,, controlled, substances,, and/or, alcohol, , Smoking, permitted, anywhere, building., use,, sale,, distribution,, transfer, possession, sub-stances, premises, (including,, limitation,, parking, lots,, vehicles,, personal, vehicles, used, business, parked, premises,, recreational, areas), environment, prohibited., “Work, environment”, includes,, situations, where, an, representing, Company,, whether, customer, call, participating, meeting, off, sites., Law, enforcement, agencies, notified, drug, activities, and,, upon, notification,, substances, will, turned, over, same., cooperate, subsequent, investigation, illegal, result, criminal, prosecution., consumption, purposes, sponsored, social, events, meetings, (e.g.,, Picnic,, Holiday, Party,, dealer, meetings), can, take, place, if, authorization, from, use, writing., prohibited, reporting, work,, returning, being, under, influence, impaired, by, alcohol., Drug, Testing, &, Criminal, Background, Checks, As, Security, we, reserve, perform, testing, background, checks, protection, our, customers",
    "fullLink":"../../../index.html",
    "height":"792",
    "width":" 612",
    "isWide":"False",
    "bookWidth":"612",
    "bookHeight":"792",

    "download":[{}
      ,{"pdfPublication":{"url":"../common/downloads_c167bf08/COPS_BULLETin.pdf", "size":"1.16 MB"}}
    
      ,{"PdfPage":{"url":"../common/downloads_c167bf08/page0005.pdf", "size":"322.91 KB"}}
     ],

    
    
    "substrate":{"background":"url(../common/page-substrates/page0005.png)",
    "backgroundSize":"contain", "backgroundColor":"#FFFFFF"},
    
    "leftTool":{"innerText":"4","src":"page4.html"},
    
    "rightTool":{"innerText":" 6","src":"page6.html"},
    
    "content":[{}
    
    ,{"className":"div_items", "top":"759","left":"183","width":"214","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font1","text":"C.O.P.S. Employee Handbook September 2013:  Page" 
    }}
  
    ,{"className":"div_items", "top":"759","left":"400","width":"5","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font2","text":"4" 
    }}
  
    ,{"className":"div_items", "top":"759","left":"408","width":"8","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font1","text":"of" 
    }}
  
    ,{"className":"div_items", "top":"759","left":"418","width":"10","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font2","text":"22" 
    }}
  
    ,{"className":"div_items", "top":"770","left":"36","width":"545","height":"10","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font3","text":"Downloaded or printed pages may be outdated.  If you printed or downloaded this page, please check the C.O.P.S. intranet to ensure you have the most recent version." 
    }}
  
    ,{"className":"div_items", "top":"49","left":"123","width":"367","height":"22","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font4","text":"General Company Policies and Practices" 
    }}
  
    ,{"className":"div_items", "top":"81","left":"265","width":"82","height":"16","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font8","text":"Introduction" 
    }}
  
    ,{"className":"div_items", "top":"103","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font18","text":"This handbook is only a source of general information concerning the Company’s policies, procedures, and rules" 
    }}
  
    ,{"className":"div_items", "top":"116","left":"54","width":"386","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font18","text":"for employees and does not include every situation that could occur in the work place." 
    }}
  
    ,{"className":"div_items", "top":"143","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font18","text":"This handbook contains no promise of any kind and is not intended as a contract of employment.  The Company" 
    }}
  
    ,{"className":"div_items", "top":"157","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font18","text":"is absolutely free to change, add to or eliminate the policies and working conditions contained in the handbook" 
    }}
  
    ,{"className":"div_items", "top":"170","left":"54","width":"399","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font18","text":"at any time without notice and without consultation with or agreement of any employee." 
    }}
  
    ,{"className":"div_items", "top":"196","left":"241","width":"131","height":"16","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font8","text":"At-Will Employment" 
    }}
  
    ,{"className":"div_items", "top":"218","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"There is no contract of employment between the Company and its employees.  All employment is “at-will” and" 
    }}
  
    ,{"className":"div_items", "top":"231","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"entered into voluntarily.  Each employee has the absolute right to terminate employment with the Company" 
    }}
  
    ,{"className":"div_items", "top":"245","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"(“quit”) at any time with or without good cause and without prior notice, and the Company has the absolute" 
    }}
  
    ,{"className":"div_items", "top":"258","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"right to terminate employment of any employee (“fire”) at any time with or without good cause and without" 
    }}
  
    ,{"className":"div_items", "top":"272","left":"54","width":"55","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"prior notice." 
    }}
  
    ,{"className":"div_items", "top":"297","left":"204","width":"205","height":"16","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font8","text":"Equal Opportunity Employment" 
    }}
  
    ,{"className":"div_items", "top":"325","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"The Company provides equal employment opportunities to all employees and applicants for employment.  We" 
    }}
  
    ,{"className":"div_items", "top":"339","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"comply with federal, state and local laws, regulations and guidelines with regard to non-discrimination against" 
    }}
  
    ,{"className":"div_items", "top":"352","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"job applicants or employees on the basis of race, religion, color, age, sex, national origin, veteran status and" 
    }}
  
    ,{"className":"div_items", "top":"366","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"non-job-related handicap.  Equal opportunities include, but are not limited to, recruiting, hiring, training," 
    }}
  
    ,{"className":"div_items", "top":"379","left":"54","width":"208","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"promotion, transfer, rates of pay and benefits." 
    }}
  
    ,{"className":"div_items", "top":"405","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Employees are encouraged to bring any complaints concerning this policy promptly to the attention of the" 
    }}
  
    ,{"className":"div_items", "top":"418","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"President (800-367-8801) or CEO (212-223-4242) of the Company immediately.  The Company forbids retaliation" 
    }}
  
    ,{"className":"div_items", "top":"431","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"against anyone that complains of violation of equal employment opportunity or provides information in" 
    }}
  
    ,{"className":"div_items", "top":"445","left":"54","width":"164","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"connection with any such complaint." 
    }}
  
    ,{"className":"div_items", "top":"471","left":"69","width":"475","height":"16","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font8","text":"On-the-job involvement with drugs, controlled substances, and/or alcohol" 
    }}
  
    ,{"className":"div_items", "top":"492","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"493","left":"90","width":"229","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Smoking is not permitted anywhere in the building." 
    }}
  
    ,{"className":"div_items", "top":"506","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"507","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"The use, sale, distribution, transfer or possession of drugs, controlled sub-stances and/or alcohol on" 
    }}
  
    ,{"className":"div_items", "top":"520","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Company premises (including, without limitation, parking lots, Company vehicles, personal vehicles used" 
    }}
  
    ,{"className":"div_items", "top":"534","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"in Company business or parked on Company premises, recreational areas) or in any Company work" 
    }}
  
    ,{"className":"div_items", "top":"547","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"environment is prohibited.  “Work environment” includes, but is not limited to, situations where an" 
    }}
  
    ,{"className":"div_items", "top":"561","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"employee is representing the Company, whether on a customer call or participating in a business" 
    }}
  
    ,{"className":"div_items", "top":"574","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"meeting off Company premises or work sites.  Law enforcement agencies may be notified of any drug" 
    }}
  
    ,{"className":"div_items", "top":"587","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"activities and, upon notification, all substances will be turned over to same.  The Company will" 
    }}
  
    ,{"className":"div_items", "top":"601","left":"90","width":"456","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"cooperate in any subsequent investigation and any illegal activities may result in criminal prosecution." 
    }}
  
    ,{"className":"div_items", "top":"614","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"615","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"The consumption of alcohol on Company premises or in any Company work environment for purposes" 
    }}
  
    ,{"className":"div_items", "top":"628","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"of Company sponsored social events and business meetings (e.g., Company Picnic, Holiday Party, dealer" 
    }}
  
    ,{"className":"div_items", "top":"642","left":"90","width":"461","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"meetings) can only take place if prior authorization from the President or CEO for such use is in writing." 
    }}
  
    ,{"className":"div_items", "top":"654","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"655","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"All employees are prohibited from reporting to work, returning to work, or being at work under the" 
    }}
  
    ,{"className":"div_items", "top":"669","left":"90","width":"322","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"influence of or impaired by drugs, controlled substances and/or alcohol." 
    }}
  
    ,{"className":"div_items", "top":"694","left":"163","width":"286","height":"16","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font8","text":"Drug Testing & Criminal Background Checks" 
    }}
  
    ,{"className":"div_items", "top":"716","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"717","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"As a Security Company, we reserve the right to perform drug testing and criminal background checks on" 
    }}
  
    ,{"className":"div_items", "top":"730","left":"90","width":"296","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"all employees for the protection of our customers and employees." 
    }}
      
    ]
})
 	