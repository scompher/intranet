﻿
    pageContentLoad(
    {
    "currentPage":{"item":"page18", "num":"18"},
    "keywords":"C.O.P.S., Employee, Handbook, September, 2013:, Page, 17, of, 22, Downloaded, or, printed, pages, may, be, outdated., If, you, downloaded, this, page,, please, check, the, intranet, to, ensure, have, most, recent, version., Safeguarding, Confidential, Information, The, policy, Company, is, preserve, and, protect, about, our, dealers, their, subscribers, as, well, operating,, pricing, business, practices, Company., Such, information, should, not, disclosed, any, person, other, than, persons, in, engaged, providing, services, who, otherwise, a, legitimate, need, know., For, purpose,, all, obtained, from, relating, identifying, terms, contractual, relationship, with, Company,, financial, operating, information,, customer, prospect, list, deemed, confidential., A, breach, Company’s, confidentiality, will, considered, grounds, for, dismissal., duty, continues, after, an, employee, no, longer, associated, agree, that, even, termination, use, purpose., In, order, implement, safeguard, Information,, following, observed:, , All, personnel, refrain, discussing, affairs, anyone, those, within, “need, know”, able, serve, dealer, subscriber’s, needs., left, exposed, public, view., Computer, terminals, “logged, in”, computer, system, when, user, away, his/her, desk, workstation., Good, judgment, care, must, exercised, at, times, avoid, unauthorized, improper, disclosures., Envelopes, folders, marked, “personal”, “confidential”, only, opened, by, addressee., Surveillance, Cameras, Recording, Telephone, Lines, protection, its, employees,, locations, surveillance, cameras, located, on, outside, front, rear, building,, inside, throughout, building., recorded, telephone, lines, Operations, room, areas, necessary., Recordings, used, training, purposes., Consult, your, Supervisor, more, information., Guidelines, (Internet/Computer, Network/Telecommunications), effort, clarify, position, Internet,, network,, telecommunications, while, utilizing, property,, we, developed, guidelines, employees:, electronic, mail,, telecommunication, are, company, assets., entered, into,, stored, within,, removed, temporarily, property., Therefore, files,, e-mail,, voicemail, NOT, private., reserves, right, investigate, monitor, such, employee,, without, notice., Outside, disks,, USB/thumb, drives,, mobile, phones,, iPods,, mp3, players, and/or, files, permitted, viewed, connected, computers, unless, scanned, Technology, Department., Internet, Exploring, done, before, work, hours, during, lunch, breaks., Using, appropriate, sites, purposes, unrestricted, long, it’s, reasonable., Although, virus, programs, installed,, downloading, caution, express, permission, department., display, transmission, sexually-explicit, images,, messages, cartoons,, contains, ethnic, slurs,, racial, epithets,, anything, perceived, harassment, disparagement",
    "fullLink":"../../../index.html",
    "height":"792",
    "width":" 612",
    "isWide":"False",
    "bookWidth":"612",
    "bookHeight":"792",

    "download":[{}
      ,{"pdfPublication":{"url":"../common/downloads_c167bf08/COPS_BULLETin.pdf", "size":"1.16 MB"}}
    
      ,{"PdfPage":{"url":"../common/downloads_c167bf08/page0018.pdf", "size":"360.03 KB"}}
     ],

    
    
    "substrate":{"background":"url(../common/page-substrates/page0018.png)",
    "backgroundSize":"contain", "backgroundColor":"#FFFFFF"},
    
    "leftTool":{"innerText":"17","src":"page17.html"},
    
    "rightTool":{"innerText":" 19","src":"page19.html"},
    
    "content":[{}
    
    ,{"className":"div_items", "top":"759","left":"181","width":"214","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font1","text":"C.O.P.S. Employee Handbook September 2013:  Page" 
    }}
  
    ,{"className":"div_items", "top":"759","left":"398","width":"10","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font2","text":"17" 
    }}
  
    ,{"className":"div_items", "top":"759","left":"410","width":"8","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font1","text":"of" 
    }}
  
    ,{"className":"div_items", "top":"759","left":"421","width":"10","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font2","text":"22" 
    }}
  
    ,{"className":"div_items", "top":"770","left":"36","width":"545","height":"10","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font3","text":"Downloaded or printed pages may be outdated.  If you printed or downloaded this page, please check the C.O.P.S. intranet to ensure you have the most recent version." 
    }}
  
    ,{"className":"div_items", "top":"34","left":"182","width":"248","height":"16","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font8","text":"Safeguarding Confidential Information" 
    }}
  
    ,{"className":"div_items", "top":"56","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"The policy of The Company is to preserve and protect Confidential Information about our dealers and their" 
    }}
  
    ,{"className":"div_items", "top":"69","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"subscribers as well the operating, pricing and business practices of The Company.  Such information should not" 
    }}
  
    ,{"className":"div_items", "top":"83","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"be disclosed to any person other than to persons in The Company engaged in providing services of who" 
    }}
  
    ,{"className":"div_items", "top":"96","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"otherwise have a legitimate need to know.  For this purpose, any and all information obtained from dealers" 
    }}
  
    ,{"className":"div_items", "top":"110","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"relating to their subscribers and all information identifying The Company dealers and the terms of their" 
    }}
  
    ,{"className":"div_items", "top":"123","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"contractual relationship with The Company, The Company financial and operating information, business terms" 
    }}
  
    ,{"className":"div_items", "top":"137","left":"54","width":"256","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"and any customer or prospect list is deemed confidential." 
    }}
  
    ,{"className":"div_items", "top":"160","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"A breach of The Company’s confidentiality policy will be considered grounds for dismissal.  The duty to preserve" 
    }}
  
    ,{"className":"div_items", "top":"173","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Confidential Information continues after an employee is no longer associated with The Company and as an" 
    }}
  
    ,{"className":"div_items", "top":"187","left":"54","width":"477","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"employee you agree that even after termination you will not use Confidential information for any purpose." 
    }}
  
    ,{"className":"div_items", "top":"210","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"In order to implement The Company’s policy and to safeguard Confidential Information, the following should be" 
    }}
  
    ,{"className":"div_items", "top":"224","left":"54","width":"44","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"observed:" 
    }}
  
    ,{"className":"div_items", "top":"237","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"238","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"All personnel should refrain from discussing the business or affairs of our dealers or the subscribers with" 
    }}
  
    ,{"className":"div_items", "top":"251","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"anyone other than those persons within The Company who have a “need to know” in order to be able to" 
    }}
  
    ,{"className":"div_items", "top":"264","left":"90","width":"172","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"serve the dealer or subscriber’s needs." 
    }}
  
    ,{"className":"div_items", "top":"277","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"278","left":"90","width":"299","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Confidential Information should not be left exposed to public view." 
    }}
  
    ,{"className":"div_items", "top":"291","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"292","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Computer terminals should not be left “logged in” to the computer system when the user is away from" 
    }}
  
    ,{"className":"div_items", "top":"305","left":"90","width":"126","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"his/her desk or workstation." 
    }}
  
    ,{"className":"div_items", "top":"318","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"319","left":"90","width":"458","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Good judgment and care must be exercised at all times to avoid unauthorized or improper disclosures." 
    }}
  
    ,{"className":"div_items", "top":"332","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"333","left":"90","width":"441","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Envelopes or folders marked “personal” or “confidential” should only be opened by the addressee." 
    }}
  
    ,{"className":"div_items", "top":"362","left":"126","width":"360","height":"16","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font8","text":"Surveillance Cameras and Recording of Telephone Lines" 
    }}
  
    ,{"className":"div_items", "top":"384","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"385","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"For the protection of the Company and its employees, all locations have surveillance cameras located on" 
    }}
  
    ,{"className":"div_items", "top":"398","left":"90","width":"346","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"the outside front and rear of the building, and inside throughout the building." 
    }}
  
    ,{"className":"div_items", "top":"411","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"412","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"For the protection of the Company and its employees, all locations have recorded telephone lines in the" 
    }}
  
    ,{"className":"div_items", "top":"426","left":"90","width":"304","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Operations room and other areas deemed necessary.  Recordings" 
    }}
  
    ,{"className":"div_items", "top":"427","left":"398","width":"160","height":"11","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font12","text":"may be used for training purposes." 
    }}
  
    ,{"className":"div_items", "top":"439","left":"90","width":"207","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Consult your Supervisor for more information." 
    }}
  
    ,{"className":"div_items", "top":"469","left":"102","width":"409","height":"16","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font8","text":"Guidelines (Internet/Computer Network/Telecommunications)" 
    }}
  
    ,{"className":"div_items", "top":"491","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"In an effort to clarify the Company’s position on the use of the Internet, computer network, and" 
    }}
  
    ,{"className":"div_items", "top":"504","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"telecommunications while utilizing Company property, we have developed the following guidelines for all" 
    }}
  
    ,{"className":"div_items", "top":"518","left":"54","width":"52","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"employees:" 
    }}
  
    ,{"className":"div_items", "top":"541","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"The computer network, electronic mail, and telecommunication system are considered company assets.  All" 
    }}
  
    ,{"className":"div_items", "top":"555","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"information entered into, stored within, or removed temporarily is considered company property.  Therefore" 
    }}
  
    ,{"className":"div_items", "top":"568","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Internet, computer files, e-mail, and voicemail use is NOT private.  C.O.P.S. reserves the right to investigate and" 
    }}
  
    ,{"className":"div_items", "top":"582","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"monitor such use by any employee, with or without notice.  Outside disks, USB/thumb drives, mobile phones," 
    }}
  
    ,{"className":"div_items", "top":"595","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"iPods, mp3 players and/or files are not permitted to be viewed or connected to C.O.P.S. computers unless" 
    }}
  
    ,{"className":"div_items", "top":"609","left":"54","width":"202","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"scanned by C.O.P.S. Technology Department." 
    }}
  
    ,{"className":"div_items", "top":"632","left":"54","width":"64","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font10","text":"The Internet" 
    }}
  
    ,{"className":"div_items", "top":"644","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"645","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Exploring the Internet should be done before or after work hours or during lunch breaks.  Using" 
    }}
  
    ,{"className":"div_items", "top":"659","left":"90","width":"359","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"appropriate sites for business purposes is unrestricted as long as it’s reasonable." 
    }}
  
    ,{"className":"div_items", "top":"671","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"672","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Although we have virus protection programs installed, downloading from the Internet should be done" 
    }}
  
    ,{"className":"div_items", "top":"686","left":"90","width":"366","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"with caution and only with the express permission of the Technology department." 
    }}
  
    ,{"className":"div_items", "top":"705","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"706","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"The display or transmission of sexually-explicit images, messages or cartoons, or any transmission that" 
    }}
  
    ,{"className":"div_items", "top":"719","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"contains ethnic slurs, racial epithets, or anything that may be perceived as harassment or disparagement" 
    }}
      
    ]
})
 	