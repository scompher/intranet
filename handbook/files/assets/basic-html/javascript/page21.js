﻿
    pageContentLoad(
    {
    "currentPage":{"item":"page21", "num":"21"},
    "keywords":"C.O.P.S., Employee, Handbook, September, 2013:, Page, 20, of, 22, Downloaded, or, printed, pages, may, be, outdated., If, you, downloaded, this, page,, please, check, the, intranet, to, ensure, have, most, recent, version., Any, sponsored, internet, posting, sites, that, pre-existed, policy, must, identified, and, a, file, maintained, with, HR, Director, site, names,, addresses,, who, is, currently, maintaining, site., This, document, updated, annually., C., ALL, POSTINGS, , member, news, media, blogger, contacts, an, employee, about, Internet, concerns, business, C.O.P.S.,, refer, person, President, at, postings, should, not, violate, any, other, applicable, including, those, set, forth, in, Handbook., shall, liable,, under, circumstances,, for, errors,, omissions,, loss, damages, claimed, incurred, due, posting., reserves, right, suspend,, modify,, withdraw, Postings, Policy,, are, responsible, regularly, reviewing, terms, Policy., Disciplinary, Action, Individuals, provisions, subject, disciplinary, action, up, termination, employment.",
    "fullLink":"../../../index.html",
    "height":"792",
    "width":" 612",
    "isWide":"False",
    "bookWidth":"612",
    "bookHeight":"792",

    "download":[{}
      ,{"pdfPublication":{"url":"../common/downloads_c167bf08/COPS_BULLETin.pdf", "size":"1.16 MB"}}
    
      ,{"PdfPage":{"url":"../common/downloads_c167bf08/page0021.pdf", "size":"317.71 KB"}}
     ],

    
    
    "substrate":{"background":"url(../common/page-substrates/page0021.png)",
    "backgroundSize":"contain", "backgroundColor":"#FFFFFF"},
    
    "leftTool":{"innerText":"20","src":"page20.html"},
    
    "rightTool":{"innerText":" 22","src":"page22.html"},
    
    "content":[{}
    
    ,{"className":"div_items", "top":"759","left":"181","width":"214","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font1","text":"C.O.P.S. Employee Handbook September 2013:  Page" 
    }}
  
    ,{"className":"div_items", "top":"759","left":"398","width":"10","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font2","text":"20" 
    }}
  
    ,{"className":"div_items", "top":"759","left":"410","width":"8","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font1","text":"of" 
    }}
  
    ,{"className":"div_items", "top":"759","left":"421","width":"10","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font2","text":"22" 
    }}
  
    ,{"className":"div_items", "top":"770","left":"36","width":"545","height":"10","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font3","text":"Downloaded or printed pages may be outdated.  If you printed or downloaded this page, please check the C.O.P.S. intranet to ensure you have the most recent version." 
    }}
  
    ,{"className":"div_items", "top":"34","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Any C.O.P.S. sponsored internet posting sites that pre-existed this policy must be identified and a file maintained" 
    }}
  
    ,{"className":"div_items", "top":"47","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"with the HR Director with site names, site addresses, and who is currently maintaining the site.  This document" 
    }}
  
    ,{"className":"div_items", "top":"60","left":"54","width":"120","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"must be updated annually." 
    }}
  
    ,{"className":"div_items", "top":"86","left":"72","width":"9","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font9","text":"C." 
    }}
  
    ,{"className":"div_items", "top":"86","left":"90","width":"65","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font9","text":"ALL POSTINGS" 
    }}
  
    ,{"className":"div_items", "top":"99","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"100","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"If a member of the news media or blogger contacts an employee about an Internet posting that" 
    }}
  
    ,{"className":"div_items", "top":"113","left":"90","width":"384","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"concerns the business of C.O.P.S., please refer that person to the President at C.O.P.S." 
    }}
  
    ,{"className":"div_items", "top":"126","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"127","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Internet postings should not violate any other applicable policy of C.O.P.S., including those set forth in" 
    }}
  
    ,{"className":"div_items", "top":"140","left":"90","width":"151","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"the C.O.P.S. Employee Handbook." 
    }}
  
    ,{"className":"div_items", "top":"153","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"154","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"C.O.P.S. shall not be liable, under any circumstances, for any errors, omissions, loss or damages claimed" 
    }}
  
    ,{"className":"div_items", "top":"167","left":"90","width":"224","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"or incurred due to any employee Internet posting." 
    }}
  
    ,{"className":"div_items", "top":"180","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"181","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"C.O.P.S. reserves the right to suspend, modify, or withdraw this Internet Postings Policy, and you are" 
    }}
  
    ,{"className":"div_items", "top":"195","left":"90","width":"342","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"responsible for regularly reviewing the terms of this Internet Postings Policy." 
    }}
  
    ,{"className":"div_items", "top":"218","left":"54","width":"97","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font10","text":"Disciplinary Action" 
    }}
  
    ,{"className":"div_items", "top":"231","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Individuals who violate the provisions of this policy may be subject to disciplinary action up to and including" 
    }}
  
    ,{"className":"div_items", "top":"244","left":"54","width":"127","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"termination of employment." 
    }}
      
    ]
})
 	