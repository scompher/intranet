﻿
    pageContentLoad(
    {
    "currentPage":{"item":"page2", "num":"2"},
    "keywords":"C.O.P.S., Employee, Handbook, September, 2013:, Page, 1, of, 22, Downloaded, or, printed, pages, may, be, outdated., If, you, downloaded, this, page,, please, check, the, intranet, to, ensure, have, most, recent, version., Table, Contents, Welcome!, The, President’s, Message, ........................................................................................................................2, Monitoring:, What, do, we, do?........................................................................................................................3, General, Company, Policies, and, Practices, ...................................................................................................................4, Introduction............................................................................................................................................................4, At-Will, Employment, ...............................................................................................................................................4, Equal, Opportunity, Employment.............................................................................................................................4, On-the-job, involvement, with, drugs,, controlled, substances,, and/or, alcohol, ........................................................4, Drug, Testing, &, Criminal, Background, Checks, .........................................................................................................4, Building, Access, Parking, ...................................................................................................................................5, Handicapped, Access...............................................................................................................................................5, Lockers...................................................................................................................................................5, Personnel, ................................................................................................................................6, Clocking, In/Out.......................................................................................................................................................6, Breaks, .....................................................................................................................................................................6, Attendance, /, Lateness, Policy, .................................................................................................................................7, Dress, Code, ................................................................................................................................................9, Meetings.................................................................................................................................................9, Distribution, Paychecks, ...................................................................................................................................., 10, Performance, Review, .........................................................................................................................., Resignations, ........................................................................................................................................................, Benefits..................................................................................................................................................., 11, Full-Time, Benefits, ..............................................................................................................................., Vacation/Holiday, Benefits..................................................................................................................................., 14, Pay, differentials, for, Operations, 15, Additional, Guidelines, Information, ..................................................................................................................., 16, Severe, Weather, Procedures................................................................................................................................, Prohibiting, Harassment, ....................................................................................................................., Safeguarding, Confidential, Information..............................................................................................................., 17, Surveillance, Cameras, Recording, Telephone, Lines..................................................................................., (Internet/Computer, Network/Telecommunications), ......................................................................., Internet, Postings, ......................................................................................................................................., 18, Departments, 21, Understanding, acknowledgement, company, employee, handbook.......................................................",
    "fullLink":"../../../index.html",
    "height":"792",
    "width":" 612",
    "isWide":"False",
    "bookWidth":"612",
    "bookHeight":"792",

    "download":[{}
      ,{"pdfPublication":{"url":"../common/downloads_c167bf08/COPS_BULLETin.pdf", "size":"1.16 MB"}}
    
      ,{"PdfPage":{"url":"../common/downloads_c167bf08/page0002.pdf", "size":"306.60 KB"}}
     ],

    
    
    "substrate":{"background":"url(../common/page-substrates/page0002.png)",
    "backgroundSize":"contain", "backgroundColor":"#FFFFFF"},
    
    "leftTool":{"innerText":"1","src":"page1.html"},
    
    "rightTool":{"innerText":" 3","src":"page3.html"},
    
    "content":[{}
    
    ,{"className":"div_items", "top":"759","left":"183","width":"214","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font1","text":"C.O.P.S. Employee Handbook September 2013:  Page" 
    }}
  
    ,{"className":"div_items", "top":"759","left":"400","width":"5","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font2","text":"1" 
    }}
  
    ,{"className":"div_items", "top":"759","left":"408","width":"8","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font1","text":"of" 
    }}
  
    ,{"className":"div_items", "top":"759","left":"418","width":"10","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font2","text":"22" 
    }}
  
    ,{"className":"div_items", "top":"770","left":"36","width":"545","height":"10","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font3","text":"Downloaded or printed pages may be outdated.  If you printed or downloaded this page, please check the C.O.P.S. intranet to ensure you have the most recent version." 
    }}
  
    ,{"className":"div_items", "top":"35","left":"226","width":"160","height":"22","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font4","text":"Table of Contents" 
    }}
  
    ,{"className":"div_items", "top":"81","left":"54","width":"49","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Welcome! " 
    }}
  
    ,{"className":"div_items", "top":"81","left":"103","width":"455","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","content":{
    "tag":"a", "href":"page3.html","text":"  The President’s Message ........................................................................................................................2",
    "useGlobal":"True","hightLightColor":"rgba(,,,)","target":""}

    }}
  
    ,{"className":"div_items", "top":"106","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","content":{
    "tag":"a", "href":"page4.html","text":"C.O.P.S. Monitoring: What do we do?........................................................................................................................3",
    "useGlobal":"True","hightLightColor":"rgba(,,,)","target":""}

    }}
  
    ,{"className":"div_items", "top":"132","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","content":{
    "tag":"a", "href":"page5.html","text":"General Company Policies and Practices ...................................................................................................................4",
    "useGlobal":"True","hightLightColor":"rgba(,,,)","target":""}

    }}
  
    ,{"className":"div_items", "top":"150","left":"65","width":"493","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","content":{
    "tag":"a", "href":"page5.html","text":"Introduction............................................................................................................................................................4",
    "useGlobal":"True","hightLightColor":"rgba(,,,)","target":""}

    }}
  
    ,{"className":"div_items", "top":"169","left":"65","width":"493","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","content":{
    "tag":"a", "href":"page5.html","text":"At-Will Employment ...............................................................................................................................................4",
    "useGlobal":"True","hightLightColor":"rgba(,,,)","target":""}

    }}
  
    ,{"className":"div_items", "top":"187","left":"65","width":"493","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","content":{
    "tag":"a", "href":"page5.html","text":"Equal Opportunity Employment.............................................................................................................................4",
    "useGlobal":"True","hightLightColor":"rgba(,,,)","target":""}

    }}
  
    ,{"className":"div_items", "top":"206","left":"65","width":"493","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","content":{
    "tag":"a", "href":"page5.html","text":"On-the-job involvement with drugs, controlled substances, and/or alcohol ........................................................4",
    "useGlobal":"True","hightLightColor":"rgba(,,,)","target":""}

    }}
  
    ,{"className":"div_items", "top":"224","left":"65","width":"493","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","content":{
    "tag":"a", "href":"page5.html","text":"Drug Testing & Criminal Background Checks .........................................................................................................4",
    "useGlobal":"True","hightLightColor":"rgba(,,,)","target":""}

    }}
  
    ,{"className":"div_items", "top":"242","left":"65","width":"493","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","content":{
    "tag":"a", "href":"page6.html","text":"Building Access and Parking ...................................................................................................................................5",
    "useGlobal":"True","hightLightColor":"rgba(,,,)","target":""}

    }}
  
    ,{"className":"div_items", "top":"261","left":"65","width":"493","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","content":{
    "tag":"a", "href":"page6.html","text":"Handicapped Access...............................................................................................................................................5",
    "useGlobal":"True","hightLightColor":"rgba(,,,)","target":""}

    }}
  
    ,{"className":"div_items", "top":"279","left":"65","width":"493","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","content":{
    "tag":"a", "href":"page6.html","text":"Employee Lockers...................................................................................................................................................5",
    "useGlobal":"True","hightLightColor":"rgba(,,,)","target":""}

    }}
  
    ,{"className":"div_items", "top":"305","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","content":{
    "tag":"a", "href":"page7.html","text":"Personnel Policies and Practices ................................................................................................................................6",
    "useGlobal":"True","hightLightColor":"rgba(,,,)","target":""}

    }}
  
    ,{"className":"div_items", "top":"323","left":"65","width":"493","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","content":{
    "tag":"a", "href":"page7.html","text":"Clocking In/Out.......................................................................................................................................................6",
    "useGlobal":"True","hightLightColor":"rgba(,,,)","target":""}

    }}
  
    ,{"className":"div_items", "top":"342","left":"65","width":"493","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","content":{
    "tag":"a", "href":"page7.html","text":"Breaks .....................................................................................................................................................................6",
    "useGlobal":"True","hightLightColor":"rgba(,,,)","target":""}

    }}
  
    ,{"className":"div_items", "top":"360","left":"65","width":"493","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","content":{
    "tag":"a", "href":"page8.html","text":"Attendance / Lateness Policy .................................................................................................................................7",
    "useGlobal":"True","hightLightColor":"rgba(,,,)","target":""}

    }}
  
    ,{"className":"div_items", "top":"378","left":"65","width":"493","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","content":{
    "tag":"a", "href":"page10.html","text":"General Dress Code ................................................................................................................................................9",
    "useGlobal":"True","hightLightColor":"rgba(,,,)","target":""}

    }}
  
    ,{"className":"div_items", "top":"397","left":"65","width":"493","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","content":{
    "tag":"a", "href":"page10.html","text":"Company Meetings.................................................................................................................................................9",
    "useGlobal":"True","hightLightColor":"rgba(,,,)","target":""}

    }}
  
    ,{"className":"div_items", "top":"415","left":"65","width":"493","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","content":{
    "tag":"a", "href":"page11.html","text":"Distribution of Paychecks .................................................................................................................................... 10",
    "useGlobal":"True","hightLightColor":"rgba(,,,)","target":""}

    }}
  
    ,{"className":"div_items", "top":"434","left":"65","width":"493","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","content":{
    "tag":"a", "href":"page11.html","text":"Employee Performance Review .......................................................................................................................... 10",
    "useGlobal":"True","hightLightColor":"rgba(,,,)","target":""}

    }}
  
    ,{"className":"div_items", "top":"452","left":"65","width":"493","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","content":{
    "tag":"a", "href":"page11.html","text":"Resignations ........................................................................................................................................................ 10",
    "useGlobal":"True","hightLightColor":"rgba(,,,)","target":""}

    }}
  
    ,{"className":"div_items", "top":"478","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","content":{
    "tag":"a", "href":"page12.html","text":"Employee Benefits................................................................................................................................................... 11",
    "useGlobal":"True","hightLightColor":"rgba(,,,)","target":""}

    }}
  
    ,{"className":"div_items", "top":"496","left":"65","width":"493","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","content":{
    "tag":"a", "href":"page12.html","text":"Full-Time Employee Benefits ............................................................................................................................... 11",
    "useGlobal":"True","hightLightColor":"rgba(,,,)","target":""}

    }}
  
    ,{"className":"div_items", "top":"515","left":"65","width":"493","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","content":{
    "tag":"a", "href":"page15.html","text":"Vacation/Holiday Benefits................................................................................................................................... 14",
    "useGlobal":"True","hightLightColor":"rgba(,,,)","target":""}

    }}
  
    ,{"className":"div_items", "top":"533","left":"65","width":"493","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","content":{
    "tag":"a", "href":"page16.html","text":"Pay differentials for Operations .......................................................................................................................... 15",
    "useGlobal":"True","hightLightColor":"rgba(,,,)","target":""}

    }}
  
    ,{"className":"div_items", "top":"558","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","content":{
    "tag":"a", "href":"page17.html","text":"Additional Guidelines and Information ................................................................................................................... 16",
    "useGlobal":"True","hightLightColor":"rgba(,,,)","target":""}

    }}
  
    ,{"className":"div_items", "top":"577","left":"65","width":"493","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","content":{
    "tag":"a", "href":"page17.html","text":"Severe Weather Procedures................................................................................................................................ 16",
    "useGlobal":"True","hightLightColor":"rgba(,,,)","target":""}

    }}
  
    ,{"className":"div_items", "top":"595","left":"65","width":"493","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","content":{
    "tag":"a", "href":"page17.html","text":"Guidelines Prohibiting Harassment ..................................................................................................................... 16",
    "useGlobal":"True","hightLightColor":"rgba(,,,)","target":""}

    }}
  
    ,{"className":"div_items", "top":"614","left":"65","width":"493","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","content":{
    "tag":"a", "href":"page18.html","text":"Safeguarding Confidential Information............................................................................................................... 17",
    "useGlobal":"True","hightLightColor":"rgba(,,,)","target":""}

    }}
  
    ,{"className":"div_items", "top":"632","left":"65","width":"493","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","content":{
    "tag":"a", "href":"page18.html","text":"Surveillance Cameras and Recording of Telephone Lines................................................................................... 17",
    "useGlobal":"True","hightLightColor":"rgba(,,,)","target":""}

    }}
  
    ,{"className":"div_items", "top":"650","left":"65","width":"493","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","content":{
    "tag":"a", "href":"page18.html","text":"Guidelines (Internet/Computer Network/Telecommunications) ....................................................................... 17",
    "useGlobal":"True","hightLightColor":"rgba(,,,)","target":""}

    }}
  
    ,{"className":"div_items", "top":"669","left":"65","width":"493","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","content":{
    "tag":"a", "href":"page19.html","text":"Internet Postings Policy ....................................................................................................................................... 18",
    "useGlobal":"True","hightLightColor":"rgba(,,,)","target":""}

    }}
  
    ,{"className":"div_items", "top":"687","left":"65","width":"493","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","content":{
    "tag":"a", "href":"page22.html","text":"Company Departments ....................................................................................................................................... 21",
    "useGlobal":"True","hightLightColor":"rgba(,,,)","target":""}

    }}
  
    ,{"className":"div_items", "top":"713","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","content":{
    "tag":"a", "href":"page23.html","text":"Understanding and acknowledgement of the company employee handbook....................................................... 22",
    "useGlobal":"True","hightLightColor":"rgba(,,,)","target":""}

    }}
      
    ]
})
 	