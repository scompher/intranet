﻿
    pageContentLoad(
    {
    "currentPage":{"item":"page10", "num":"10"},
    "keywords":"C.O.P.S., Employee, Handbook, September, 2013:, Page, 9, of, 22, Downloaded, or, printed, pages, may, be, outdated., If, you, downloaded, this, page,, please, check, the, intranet, to, ensure, have, most, recent, version., General, Dress, Code, Formal, business, attire, required, on, any, day, determined, by, management, (including, neck, ties, for, men)., Business, dress, is, from, Labor, Day, through, Memorial, Day., casual, permitted, Day,, unless, otherwise, management., Please, take, pride, in, your, job, presenting, a, neat, and, professional, appearance, at, all, times., Office, (Labor, Day):, , Pants:, Dress,, pleated,, cuffs, worn, over, shoes,, loose, styling,, pull-on, pants, with, pockets., Skirts:, Plain, skirts,, split, skirts, must, come, past, mid-thigh, when, standing., Shirts:, Button-up, shirts;, blouses, (other, than, material/styles)., Stockings, women., Shoes:, Good, condition,, flat, medium, heel, preferred;, open-toe, shoes, stockings., No, canvas,, flip-flops,, stilettos,, work, boots., Sneakers, are, acceptable, Fridays., athletic, jogging, attire., jeans, denim, material, (except, Friday)., shorts., spandex, form, fitting, slacks, skirts., Casual, (Memorial, Male, employees, wear, collared, shirt, (ties, not, required)., Khaki, acceptable., Capri, that, rest, below, knee., required., Midriffs, covered., preferred., Dispatchers, Working, In, Central, Some, branch, offices, provide, uniform, shirts, ties;, sweaters, provided, based, availability., receive, their, uniforms, after, 90, days., do, will, follow, general, office, code, until, such, time, as, provided., authorized,, locations, subject, an, alternate, code., see, Supervisor, more, information., To, accompany, navy, black, Shoes, should, low, heel,, women, Company, Meetings, meetings, held, evening,, typically, two, times, year., Hourly, paid, 1.5, hours, attending., All, working, attend.",
    "fullLink":"../../../index.html",
    "height":"792",
    "width":" 612",
    "isWide":"False",
    "bookWidth":"612",
    "bookHeight":"792",

    "download":[{}
      ,{"pdfPublication":{"url":"../common/downloads_c167bf08/COPS_BULLETin.pdf", "size":"1.16 MB"}}
    
      ,{"PdfPage":{"url":"../common/downloads_c167bf08/page0010.pdf", "size":"318.91 KB"}}
     ],

    
    
    "substrate":{"background":"url(../common/page-substrates/page0010.png)",
    "backgroundSize":"contain", "backgroundColor":"#FFFFFF"},
    
    "leftTool":{"innerText":"9","src":"page9.html"},
    
    "rightTool":{"innerText":" 11","src":"page11.html"},
    
    "content":[{}
    
    ,{"className":"div_items", "top":"759","left":"183","width":"214","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font1","text":"C.O.P.S. Employee Handbook September 2013:  Page" 
    }}
  
    ,{"className":"div_items", "top":"759","left":"400","width":"5","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font2","text":"9" 
    }}
  
    ,{"className":"div_items", "top":"759","left":"408","width":"8","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font1","text":"of" 
    }}
  
    ,{"className":"div_items", "top":"759","left":"418","width":"10","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font2","text":"22" 
    }}
  
    ,{"className":"div_items", "top":"770","left":"36","width":"545","height":"10","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font3","text":"Downloaded or printed pages may be outdated.  If you printed or downloaded this page, please check the C.O.P.S. intranet to ensure you have the most recent version." 
    }}
  
    ,{"className":"div_items", "top":"34","left":"243","width":"125","height":"16","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font8","text":"General Dress Code" 
    }}
  
    ,{"className":"div_items", "top":"56","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Formal business attire may be required on any day determined by management (including neck ties for men)." 
    }}
  
    ,{"className":"div_items", "top":"69","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Business dress is required from Labor Day through Memorial Day.  Business casual dress is permitted from" 
    }}
  
    ,{"className":"div_items", "top":"83","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Memorial Day through Labor Day, unless otherwise determined by management.  Please take pride in your job" 
    }}
  
    ,{"className":"div_items", "top":"96","left":"54","width":"278","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"by presenting a neat and professional appearance at all times." 
    }}
  
    ,{"className":"div_items", "top":"119","left":"54","width":"72","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font10","text":"General Office" 
    }}
  
    ,{"className":"div_items", "top":"133","left":"72","width":"226","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Business Dress (Labor Day through Memorial Day):" 
    }}
  
    ,{"className":"div_items", "top":"145","left":"90","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"146","left":"108","width":"384","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Pants: Dress, pleated, cuffs worn over shoes, loose styling, pull-on pants with pockets." 
    }}
  
    ,{"className":"div_items", "top":"159","left":"90","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"160","left":"108","width":"319","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Skirts: Plain skirts, split skirts must come past mid-thigh when standing." 
    }}
  
    ,{"className":"div_items", "top":"173","left":"90","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"174","left":"108","width":"328","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Shirts: Button-up dress shirts; blouses (other than casual material/styles)." 
    }}
  
    ,{"className":"div_items", "top":"187","left":"90","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"188","left":"108","width":"159","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Stockings must be worn by women." 
    }}
  
    ,{"className":"div_items", "top":"200","left":"90","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"201","left":"108","width":"450","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Shoes: Good condition, flat or medium heel is preferred; open-toe shoes must be worn with" 
    }}
  
    ,{"className":"div_items", "top":"215","left":"108","width":"44","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"stockings." 
    }}
  
    ,{"className":"div_items", "top":"228","left":"90","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"228","left":"108","width":"384","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"No casual canvas, flip-flops, stilettos, work boots.  Sneakers are acceptable on Fridays." 
    }}
  
    ,{"className":"div_items", "top":"241","left":"90","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"242","left":"108","width":"126","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"No athletic or jogging attire." 
    }}
  
    ,{"className":"div_items", "top":"255","left":"90","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"256","left":"108","width":"238","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"No jeans or denim material (except on casual Friday)." 
    }}
  
    ,{"className":"div_items", "top":"269","left":"90","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"270","left":"108","width":"46","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"No shorts." 
    }}
  
    ,{"className":"div_items", "top":"283","left":"90","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"283","left":"108","width":"188","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"No spandex or form fitting slacks or skirts." 
    }}
  
    ,{"className":"div_items", "top":"307","left":"72","width":"258","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Business Casual Dress (Memorial Day through Labor Day):" 
    }}
  
    ,{"className":"div_items", "top":"320","left":"90","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"321","left":"108","width":"279","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Male employees must wear a collared shirt (ties not required)." 
    }}
  
    ,{"className":"div_items", "top":"333","left":"90","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"334","left":"108","width":"123","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Khaki pants are acceptable." 
    }}
  
    ,{"className":"div_items", "top":"347","left":"90","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"348","left":"108","width":"166","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Capri pants that rest below the knee." 
    }}
  
    ,{"className":"div_items", "top":"361","left":"90","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"362","left":"108","width":"121","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Stockings are not required." 
    }}
  
    ,{"className":"div_items", "top":"375","left":"90","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"376","left":"108","width":"115","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Midriffs must be covered." 
    }}
  
    ,{"className":"div_items", "top":"388","left":"90","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"389","left":"108","width":"252","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Shoes: Good condition, flat or medium heel is preferred." 
    }}
  
    ,{"className":"div_items", "top":"402","left":"90","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"403","left":"108","width":"220","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"No casual canvas, flip-flops, stilettos, work boots." 
    }}
  
    ,{"className":"div_items", "top":"426","left":"54","width":"160","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font10","text":"Dispatchers Working In Central" 
    }}
  
    ,{"className":"div_items", "top":"439","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"440","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Some branch offices provide Dispatchers with uniform shirts and ties; sweaters are provided based on" 
    }}
  
    ,{"className":"div_items", "top":"453","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"availability.  Dispatchers receive their uniforms after 90 days.  Dispatchers that do not have uniforms will" 
    }}
  
    ,{"className":"div_items", "top":"467","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"follow the general office dress code until such time as a uniform is provided.  If authorized, Dispatchers" 
    }}
  
    ,{"className":"div_items", "top":"480","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"in branch locations may be subject to an alternate dress code.  Please see your Supervisor for more" 
    }}
  
    ,{"className":"div_items", "top":"493","left":"90","width":"56","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"information." 
    }}
  
    ,{"className":"div_items", "top":"506","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"507","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"To accompany the uniform shirts please wear navy or black dress pants or skirts.  Shoes should have a" 
    }}
  
    ,{"className":"div_items", "top":"521","left":"90","width":"230","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"low or flat heel, and women should wear stockings." 
    }}
  
    ,{"className":"div_items", "top":"550","left":"245","width":"122","height":"16","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font8","text":"Company Meetings" 
    }}
  
    ,{"className":"div_items", "top":"572","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"573","left":"90","width":"313","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Company meetings are held in the evening, typically two times a year." 
    }}
  
    ,{"className":"div_items", "top":"586","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"587","left":"90","width":"228","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Hourly employees are paid 1.5 hours for attending." 
    }}
  
    ,{"className":"div_items", "top":"599","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"600","left":"90","width":"224","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"All employees not working are required to attend." 
    }}
      
    ]
})
 	