﻿
    pageContentLoad(
    {
    "currentPage":{"item":"page19", "num":"19"},
    "keywords":"C.O.P.S., Employee, Handbook, September, 2013:, Page, 18, of, 22, Downloaded, or, printed, pages, may, be, outdated., If, you, downloaded, this, page,, please, check, the, intranet, to, ensure, have, most, recent, version., others, based, on, their, race,, national, origin,, sex,, sexual, orientation,, age,, disability,, religious, political, beliefs, is, NOT, permitted, before,, during,, after, business, hours., , Please, mindful, leaving, open, web, sites, with, visitors, in, your, area., Monitoring, has, right, and, capability, monitor, Internet, email, usage, by, each, user, our, system., However, goal, that, employee, will, make, unnecessary., At, C.O.P.S.,, we, privilege, e-mail, use, it, important, treat, as, a, valuable, asset, for, mutual, benefit., Electronic, Mail, mail, used, purposes., While, personal, electronic, permitted,, kept, minimum., Personal, should, sent, received, seldom, possible, brief, possible., No, one, solicit,, promote, advertise, any, organization,, product, service, through, anywhere, else, company, premises, during, work, Cell, Phone/Electronic, devices, cell, phone, not, building, unless, are, break, designated, areas., This, includes, using, Bluetooth, texting, at, desk, station., take, measures, appropriate, parties, need, an, Emergency, Contact, number, hours, Central, Station, number., Absolutely, no, taking, pictures, recordings, phone,, other, devices,, while, building., Any, violation, policy, considered, “incident”, subject, termination, legal, action., Computer, Network, Telecommunications, The, password, feature, networks, telecommunications, systems, foundation, maintaining, confidentiality, company’s, telecommunication, Passwords,, system, telephone, numbers,, similar, information, disseminated, public, must, retained, confidential, user., For, privacy, reasons,, employees, attempt, gain, access, another, employee’s, mailbox, voice, without, express, permission., revised, time, sees, fit., Postings, Policy, With, rise, new, media, next, generation, communications, tools,, way, which, can, communicate, internally, externally, continues, evolve., creates, opportunities, communication, collaboration,, also, responsibilities, employees., applies, who, following:, Multi-media, social, networking, websites, such, MySpace,, Twitter,, Facebook,, Yahoo!, Groups, YouTube, Blogs, (Both, external, C.O.P.S.), Wikis, Wikipedia, site, where, text, posted, All, these, activities, referred, “Internet, postings”, respects, participate, reasons, non-working, time., same, time,, understand, what, recommended,, expected,, required, when, discuss, topics, relate, whether, own, aware, result, disciplinary, action, up, including, termination., Common, sense, best, guide, if, decide, post, relating, unsure, about, particular, posting,, contact, HR, Director, guidance., instance,, writing, responsibility,, wish, sure, manager, comfortable",
    "fullLink":"../../../index.html",
    "height":"792",
    "width":" 612",
    "isWide":"False",
    "bookWidth":"612",
    "bookHeight":"792",

    "download":[{}
      ,{"pdfPublication":{"url":"../common/downloads_c167bf08/COPS_BULLETin.pdf", "size":"1.16 MB"}}
    
      ,{"PdfPage":{"url":"../common/downloads_c167bf08/page0019.pdf", "size":"323.29 KB"}}
     ],

    
    
    "substrate":{"background":"url(../common/page-substrates/page0019.png)",
    "backgroundSize":"contain", "backgroundColor":"#FFFFFF"},
    
    "leftTool":{"innerText":"18","src":"page18.html"},
    
    "rightTool":{"innerText":" 20","src":"page20.html"},
    
    "content":[{}
    
    ,{"className":"div_items", "top":"759","left":"181","width":"214","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font1","text":"C.O.P.S. Employee Handbook September 2013:  Page" 
    }}
  
    ,{"className":"div_items", "top":"759","left":"398","width":"10","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font2","text":"18" 
    }}
  
    ,{"className":"div_items", "top":"759","left":"410","width":"8","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font1","text":"of" 
    }}
  
    ,{"className":"div_items", "top":"759","left":"421","width":"10","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font2","text":"22" 
    }}
  
    ,{"className":"div_items", "top":"770","left":"36","width":"545","height":"10","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font3","text":"Downloaded or printed pages may be outdated.  If you printed or downloaded this page, please check the C.O.P.S. intranet to ensure you have the most recent version." 
    }}
  
    ,{"className":"div_items", "top":"34","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"of others based on their race, national origin, sex, sexual orientation, age, disability, religious or political" 
    }}
  
    ,{"className":"div_items", "top":"47","left":"90","width":"286","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"beliefs is NOT permitted before, during, or after business hours." 
    }}
  
    ,{"className":"div_items", "top":"66","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"67","left":"90","width":"311","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Please be mindful of leaving open web sites with visitors in your area." 
    }}
  
    ,{"className":"div_items", "top":"86","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"C.O.P.S. Monitoring has the right and capability to monitor Internet and email usage by each user on our" 
    }}
  
    ,{"className":"div_items", "top":"100","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"system.  However our goal is that the employee will make this unnecessary.  At C.O.P.S., we have the" 
    }}
  
    ,{"className":"div_items", "top":"113","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"privilege of Internet and e-mail use and it is important that we treat it as a valuable asset for our mutual" 
    }}
  
    ,{"className":"div_items", "top":"126","left":"90","width":"35","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"benefit." 
    }}
  
    ,{"className":"div_items", "top":"150","left":"54","width":"76","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font10","text":"Electronic Mail" 
    }}
  
    ,{"className":"div_items", "top":"164","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"165","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Electronic mail is to be used for business purposes.  While personal electronic mail is permitted, it is to" 
    }}
  
    ,{"className":"div_items", "top":"180","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"be kept to a minimum.  Personal electronic mail should be sent or received as seldom as possible and be" 
    }}
  
    ,{"className":"div_items", "top":"196","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"as brief as possible.  No one may solicit, promote or advertise any organization, product or service" 
    }}
  
    ,{"className":"div_items", "top":"211","left":"90","width":"436","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"through the use of electronic mail or anywhere else on the company premises during work hours." 
    }}
  
    ,{"className":"div_items", "top":"237","left":"54","width":"151","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font10","text":"Cell Phone/Electronic devices" 
    }}
  
    ,{"className":"div_items", "top":"249","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"250","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Personal cell phone usage is not permitted in the building unless you are on your break in designated" 
    }}
  
    ,{"className":"div_items", "top":"263","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"break areas.  This includes using your Bluetooth or texting at your desk or station.  Please take measures" 
    }}
  
    ,{"className":"div_items", "top":"277","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"to ensure any appropriate parties that may need to have an Emergency Contact number for you during" 
    }}
  
    ,{"className":"div_items", "top":"290","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"work hours have the Central Station number.  Absolutely no taking of pictures or recordings with your" 
    }}
  
    ,{"className":"div_items", "top":"304","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"cell phone, or other devices, is permitted while in the building.  Any violation of this policy will be" 
    }}
  
    ,{"className":"div_items", "top":"317","left":"90","width":"308","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"considered an “incident” and subject to termination and legal action." 
    }}
  
    ,{"className":"div_items", "top":"340","left":"54","width":"227","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font10","text":"Computer Network and Telecommunications" 
    }}
  
    ,{"className":"div_items", "top":"353","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"354","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"The password feature of C.O.P.S. Monitoring networks and telecommunications systems is the" 
    }}
  
    ,{"className":"div_items", "top":"367","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"foundation for maintaining the confidentiality of the company’s telecommunication system.  Passwords," 
    }}
  
    ,{"className":"div_items", "top":"381","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"system telephone numbers, and similar information may not be disseminated to the public and must be" 
    }}
  
    ,{"className":"div_items", "top":"394","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"retained as confidential information by the user.  For privacy reasons, employees may not attempt to" 
    }}
  
    ,{"className":"div_items", "top":"408","left":"90","width":"452","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"gain access to another employee’s personal e-mail mailbox or voice mail without express permission." 
    }}
  
    ,{"className":"div_items", "top":"431","left":"90","width":"309","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"This policy may be revised at any time as C.O.P.S. Monitoring sees fit." 
    }}
  
    ,{"className":"div_items", "top":"461","left":"229","width":"154","height":"16","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font8","text":"Internet Postings Policy" 
    }}
  
    ,{"className":"div_items", "top":"483","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"With the rise of new media and next generation communications tools, the way in which C.O.P.S. employees can" 
    }}
  
    ,{"className":"div_items", "top":"496","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"communicate internally and externally continues to evolve.  While this creates new opportunities for" 
    }}
  
    ,{"className":"div_items", "top":"510","left":"54","width":"416","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"communication and collaboration, it also creates new responsibilities for C.O.P.S. employees." 
    }}
  
    ,{"className":"div_items", "top":"533","left":"54","width":"327","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"This Internet Postings Policy applies to employees who use the following:" 
    }}
  
    ,{"className":"div_items", "top":"556","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"557","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Multi-media and social networking websites such as MySpace, Twitter, Facebook, Yahoo! Groups and" 
    }}
  
    ,{"className":"div_items", "top":"570","left":"90","width":"40","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"YouTube" 
    }}
  
    ,{"className":"div_items", "top":"583","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"584","left":"90","width":"252","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Blogs (Both C.O.P.S. Blogs and Blogs external to C.O.P.S.)" 
    }}
  
    ,{"className":"div_items", "top":"597","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"598","left":"90","width":"306","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Wikis such as Wikipedia and any other site where text can be posted" 
    }}
  
    ,{"className":"div_items", "top":"621","left":"54","width":"320","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"All of these activities are referred to as “Internet postings” in this Policy" 
    }}
  
    ,{"className":"div_items", "top":"645","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"C.O.P.S. respects your right to participate in these media for personal reasons in your non-working time.  At the" 
    }}
  
    ,{"className":"div_items", "top":"658","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"same time, it is important for you to understand what is recommended, expected, and required of you when" 
    }}
  
    ,{"className":"div_items", "top":"672","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"you discuss topics that relate to C.O.P.S. whether at work or on your own time.  Please be aware that violation" 
    }}
  
    ,{"className":"div_items", "top":"685","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"of this policy may result in disciplinary action up to and including termination.  Common sense is the best guide" 
    }}
  
    ,{"className":"div_items", "top":"698","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"if you decide to post information in any way relating to C.O.P.S. If you are unsure about any particular posting," 
    }}
  
    ,{"className":"div_items", "top":"712","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"please contact the HR Director for guidance.  For instance, if you are writing about C.O.P.S. business where you" 
    }}
  
    ,{"className":"div_items", "top":"725","left":"54","width":"472","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"have responsibility, you may wish to make sure your manager is comfortable with your taking that action." 
    }}
      
    ]
})
 	