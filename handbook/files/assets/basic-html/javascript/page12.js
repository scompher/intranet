﻿
    pageContentLoad(
    {
    "currentPage":{"item":"page12", "num":"12"},
    "keywords":"C.O.P.S., Employee, Handbook, September, 2013:, Page, 11, of, 22, Downloaded, or, printed, pages, may, be, outdated., If, you, downloaded, this, page,, please, check, the, intranet, to, ensure, have, most, recent, version., Benefits, Full-Time, Employees, are, considered, full, time, when, they, consistently, work, 37.5, hours, a, week, for, term, their, employment., Otherwise,, employee, is, PART, TIME, employee., Full, employees, who, do, not, lose, benefits, (if, applicable)., meet, definition, employment, as, described, in, section, Medical, Insurance, 180, days, after, first, day, month, following, your, full-time, employment,, eligible, enroll, medical, coverage., Example:, hired, January, 15th, will, on, August, 1st., , It, YOUR, responsibility, notify, Human, Resources, Department, that, want, enrolled, into, Company’s, coverage, program, at, least, three, (3), weeks, prior, eligibility, date., Once, notified,, provide, with, an, insurance, handbook, and, application., A, contribution, towards, premium, required, certain, levels, This, deducted, out, pay,, fall, under, company’s, Section, 125, plan, tax, purposes., Please, Resource, department, inquire, about, respective, amounts, from, pay, relating, benefits., employee’s, status, changes, part, currently, has, benefits,, terminated, given, option, receiving, COBRA., time,, wait, order, Life, Accidental, Death, &, Dismemberment, (AD&D), twice, annual, salary, (up, maximum, $75,000), provided, no, cost, An, benefit, one, hundred, eighty, (180), one’s, note, exceeding, $50,000, reported, income, Form, W-2., additional, death, paid, portion, policy, accidental, death., The, equal, life, paid., request, enrollment, form, Resources., Information, concerning, AD&D, insurance,, including, available, through, contributions,, contained, handbooks, which, Department., Disability, Depending, State, mandates, company, policy,, short-term, long-term, disability, provided., For, more, details,, see, whether, contact, pamphlets",
    "fullLink":"../../../index.html",
    "height":"792",
    "width":" 612",
    "isWide":"False",
    "bookWidth":"612",
    "bookHeight":"792",

    "download":[{}
      ,{"pdfPublication":{"url":"../common/downloads_c167bf08/COPS_BULLETin.pdf", "size":"1.16 MB"}}
    
      ,{"PdfPage":{"url":"../common/downloads_c167bf08/page0012.pdf", "size":"322.31 KB"}}
     ],

    
    
    "substrate":{"background":"url(../common/page-substrates/page0012.png)",
    "backgroundSize":"contain", "backgroundColor":"#FFFFFF"},
    
    "leftTool":{"innerText":"11","src":"page11.html"},
    
    "rightTool":{"innerText":" 13","src":"page13.html"},
    
    "content":[{}
    
    ,{"className":"div_items", "top":"759","left":"181","width":"214","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font1","text":"C.O.P.S. Employee Handbook September 2013:  Page" 
    }}
  
    ,{"className":"div_items", "top":"759","left":"398","width":"10","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font2","text":"11" 
    }}
  
    ,{"className":"div_items", "top":"759","left":"410","width":"8","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font1","text":"of" 
    }}
  
    ,{"className":"div_items", "top":"759","left":"421","width":"10","height":"12","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font2","text":"22" 
    }}
  
    ,{"className":"div_items", "top":"770","left":"36","width":"545","height":"10","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font3","text":"Downloaded or printed pages may be outdated.  If you printed or downloaded this page, please check the C.O.P.S. intranet to ensure you have the most recent version." 
    }}
  
    ,{"className":"div_items", "top":"35","left":"221","width":"171","height":"22","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font4","text":"Employee Benefits" 
    }}
  
    ,{"className":"div_items", "top":"73","left":"213","width":"185","height":"16","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font8","text":"Full-Time Employee Benefits" 
    }}
  
    ,{"className":"div_items", "top":"95","left":"54","width":"122","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Employees are considered" 
    }}
  
    ,{"className":"div_items", "top":"95","left":"181","width":"40","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font9","text":"full time" 
    }}
  
    ,{"className":"div_items", "top":"95","left":"224","width":"334","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"when they consistently work 37.5 hours a week for the term of their" 
    }}
  
    ,{"className":"div_items", "top":"111","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"employment.  Otherwise, the employee is considered a PART TIME employee.  Full time employees who do not" 
    }}
  
    ,{"className":"div_items", "top":"126","left":"54","width":"396","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"consistently work 37.5 hours a week may lose full time employee benefits (if applicable)." 
    }}
  
    ,{"className":"div_items", "top":"152","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Full time employees who do not consistently meet the definition of full time employment may lose benefits as" 
    }}
  
    ,{"className":"div_items", "top":"167","left":"54","width":"174","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"described in this section (if applicable)." 
    }}
  
    ,{"className":"div_items", "top":"192","left":"54","width":"93","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font10","text":"Medical Insurance" 
    }}
  
    ,{"className":"div_items", "top":"206","left":"54","width":"504","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"180 days after the first day of the month following your full-time employment, you are eligible to enroll for" 
    }}
  
    ,{"className":"div_items", "top":"219","left":"54","width":"478","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"medical coverage.  Example: Full time employees hired January 15th will be eligible to enroll on August 1st." 
    }}
  
    ,{"className":"div_items", "top":"242","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"243","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"It is YOUR responsibility to notify the Human Resources Department that you want to be enrolled into" 
    }}
  
    ,{"className":"div_items", "top":"256","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"the Company’s medical coverage program at least three (3) weeks prior to your eligibility date.  Once" 
    }}
  
    ,{"className":"div_items", "top":"270","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"notified, the Human Resources Department will provide you with an insurance handbook and" 
    }}
  
    ,{"className":"div_items", "top":"283","left":"90","width":"52","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"application." 
    }}
  
    ,{"className":"div_items", "top":"296","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"297","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"A contribution towards your medical insurance premium may be required for certain levels of medical" 
    }}
  
    ,{"className":"div_items", "top":"310","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"coverage.  This contribution will be deducted out of your pay, and may fall under the company’s Section" 
    }}
  
    ,{"className":"div_items", "top":"324","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"125 plan for tax purposes.  Please check with the Human Resource department to inquire about" 
    }}
  
    ,{"className":"div_items", "top":"337","left":"90","width":"379","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"respective amounts that will be deducted from your pay relating to medical benefits." 
    }}
  
    ,{"className":"div_items", "top":"350","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"351","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"If an employee’s status changes from full time to part time and the employee currently has medical" 
    }}
  
    ,{"className":"div_items", "top":"364","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"benefits, their medical coverage will be terminated and they will be given the option of receiving" 
    }}
  
    ,{"className":"div_items", "top":"378","left":"90","width":"34","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"COBRA." 
    }}
  
    ,{"className":"div_items", "top":"390","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"391","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"If an employee’s status changes from part time to full time, the employee will have to wait the full 180" 
    }}
  
    ,{"className":"div_items", "top":"405","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"days after the first day of the month following full-time employment, in order to be eligible for medical" 
    }}
  
    ,{"className":"div_items", "top":"418","left":"90","width":"44","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"coverage." 
    }}
  
    ,{"className":"div_items", "top":"441","left":"54","width":"322","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font10","text":"Life and Accidental Death & Dismemberment (AD&D) Insurance" 
    }}
  
    ,{"className":"div_items", "top":"454","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"455","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Life insurance at twice an employee’s annual salary (up to a maximum of $75,000) is provided at no cost" 
    }}
  
    ,{"className":"div_items", "top":"468","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"to the employee.  An employee is eligible for this benefit one hundred eighty (180) days after the first" 
    }}
  
    ,{"className":"div_items", "top":"482","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"day of the month following one’s full-time employment.  Please note that insurance coverage exceeding" 
    }}
  
    ,{"className":"div_items", "top":"495","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"$50,000 will be reported as income on the employee’s annual Form W-2.  An additional death benefit is" 
    }}
  
    ,{"className":"div_items", "top":"509","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"paid under the Accidental death portion of the policy for an accidental death.  The Accidental death" 
    }}
  
    ,{"className":"div_items", "top":"522","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"benefit is equal to the life insurance benefit paid.  It is your responsibility to request an enrollment form" 
    }}
  
    ,{"className":"div_items", "top":"535","left":"90","width":"108","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"from Human Resources." 
    }}
  
    ,{"className":"div_items", "top":"548","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"549","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Information concerning life and AD&D insurance, including additional levels of coverage that may be" 
    }}
  
    ,{"className":"div_items", "top":"563","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"available through employee contributions, is contained in benefit handbooks which are available from" 
    }}
  
    ,{"className":"div_items", "top":"576","left":"90","width":"153","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"the Human Resource Department." 
    }}
  
    ,{"className":"div_items", "top":"599","left":"54","width":"102","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font10","text":"Disability Insurance" 
    }}
  
    ,{"className":"div_items", "top":"612","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"613","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Depending on certain State mandates and company policy, short-term and long-term disability may be" 
    }}
  
    ,{"className":"div_items", "top":"626","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"provided.  For more details, and to see whether or not you are eligible for disability insurance, please" 
    }}
  
    ,{"className":"div_items", "top":"640","left":"90","width":"190","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"contact the Human Resource Department." 
    }}
  
    ,{"className":"div_items", "top":"652","left":"72","width":"6","height":"9","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font11","text":"" 
    }}
  
    ,{"className":"div_items", "top":"653","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Information concerning disability insurance, including additional levels of coverage that may be available" 
    }}
  
    ,{"className":"div_items", "top":"667","left":"90","width":"468","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"through employee contributions, is contained in benefit pamphlets which are available from the Human" 
    }}
  
    ,{"className":"div_items", "top":"680","left":"90","width":"106","height":"13","zIndex":"0",
    
    "content":{"tag":"span", "className":"fontPosition font5","text":"Resources Department." 
    }}
      
    ]
})
 	