<cfsetting requesttimeout="1200">

<link rel="stylesheet" type="text/css" href="../styles.css">

<cfparam name="dealerList" default="">
<cfparam name="err" default="">

<cfif isDefined("form.btnAddDealer")>
	<cfquery name="checkSAIDealer" datasource="sai">
		select inst_no from sai.sai_installer
		where inst_no = '#form.dealernumber#'
	</cfquery>
	<cfif checkSAIDealer.recordcount is 0>
		<cfset err = "Dealer number #form.dealernumber# was not found in the SAI database.">
	<cfelse>
		<cfif listfindnocase(dealerList,form.dealerNumber) is 0>
			<cfset dealerList = listAppend(dealerList,form.dealernumber)>
		</cfif>
	</cfif>
</cfif>

<cfif isDefined("form.btnDelete.x")>
	<cfset dealerList = listDeleteAt(dealerList,listfindnocase(dealerList,form.removeDealer))>
</cfif>

<div align="center" class="normal">
<cfif trim(err) is not "">
	<cfoutput><b class="alert">#err#</b><br /><br /></cfoutput>
</cfif>
<table border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>SAI Dealer Import </b></td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
			<form method="post" action="index.cfm">
			<input type="hidden" name="dealerList" value="<cfoutput>#dealerList#</cfoutput>" />
			<input type="hidden" name="removeDealer" value="" />
			<table border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td><b>Add Dealer Number to Process:  </b></td>
					<td>
						<input name="dealernumber" type="text" id="dealernumber" size="20" maxlength="15">
					</td>
					<td>
						<input name="btnAddDealer" type="submit" class="normal" value="Add Dealer">
					</td>
				</tr>
				<cfif trim(dealerlist) is not "">
				<tr>
					<td colspan="3" class="nopadding">
						<table width="100%" border="0" cellspacing="0" cellpadding="5">
							<tr>
								<td colspan="2" class="highlightbartopbottomborderonly"><b>Currently Added Dealers</b></td>
							</tr>
							<cfloop list="#dealerList#" index="dn">
							<cfoutput>
							<tr>
								<td width="3%"><input onclick="this.form.removeDealer.value = #dn#;" type="image" name="btnDelete" src="/images/delete.gif" alt="Remove Item" /></td>
								<td width="97%">#dn#</td>
							</tr>
							</cfoutput>
							</cfloop>
						</table>
					</td>
				</tr>
				</cfif>
				<tr>
					<td colspan="3">
						<input name="btnProcess" type="submit" class="normal" value="Process Dealers" onclick="this.value='Processing, please wait...'">
					</td>
				</tr>
			</table>
			</form>
		</td>
	</tr>
</table>

<cfif isDefined("form.btnProcess")>
	<cfloop list="#dealerList#" index="dn">
		<cfinclude template="processDealer.cfm">
	</cfloop>
	<br>
	<table width="700" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td class="highlightbar"><b>Finished Processing, Pick up data files Below (right-click and choose &quot;save-as&quot; to download) </b></td>
		</tr>
		<tr>
			<td class="greyrowbottomnopadding">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<cfloop list="#dealerList#" index="dn">
					<cfoutput>
					<tr>
						<td class="linedrowrightcolumn"><b>#dn#</b></td>
						<td class="linedrowrightcolumn"><a style="text-decoration:underline;" href="dealerExport/#dn#.account.txt">account.txt</a></td>
						<td class="linedrowrightcolumn"><a style="text-decoration:underline;" href="dealerExport/#dn#.dispatch.txt">dispatch.txt</a></td>
						<td class="linedrowrightcolumn"><a style="text-decoration:underline;" href="dealerExport/#dn#.passcard.txt">passcard.txt</a></td>
						<td class="linedrowrightcolumn"><a style="text-decoration:underline;" href="dealerExport/#dn#.scheduledEvents.txt">scheduledEvents.txt</a></td>
						<td class="linedrowrightcolumn"><a style="text-decoration:underline;" href="dealerExport/#dn#.schedule.txt">schedule.txt</a></td>
						<td class="linedrowrightcolumn"><a style="text-decoration:underline;" href="dealerExport/#dn#.zone.txt">zone.txt</a></td>
					</tr>
					</cfoutput>
					</cfloop>
				</table>
			</td>
		</tr>
	</table>
</cfif>
<br />
<a class="normal" style="text-decoration:underline;" href="/index.cfm">Return to Intranet</a>
</div>
