
<cfset rootdir = replace(cgi.PATH_TRANSLATED, "index.cfm", "", "all")>
<cfset filedir = "dealerExport">
<cfset currentdir = "#rootdir#\#filedir#">
<!--- 
<cfif directoryExists("#currentdir#")>
	<cfdirectory action="delete" directory="#currentdir#" recurse="yes">
</cfif>
<cfdirectory action="create" directory="#currentdir#">
 --->
<!--- process sai_account.txt --->
<cfquery name="getAccounts" datasource="sai">
	select  
	sai.sai_account.cs_no, name, addr1, addr2, city, state, addr_key, phone1, ext1, phone2, ext2, maploc, pd_agency_no, fd_agency_no, md_agency_no, pt_agency_no, inst_no, guards, keynum, ul_code, name_key, zip_code, cs_loc_no, gd_loc_no, ss_loc_no, sec_cs_no, red_flag, wo_no, mail_freq, map_type, udf1, udf2,  
	CONVERT(VARCHAR(10), start_date, 101) AS start_date,   
	ati, sec_ati, system_type, entry_delay, exit_delay, special, br_no, br_site_no, time_zone, time_zone_mins, dst_grp, site_cs_no, type, udf3, udf4, telco, account_type, num_key, service_type, local, orig_cs, orig_sec, del_date, del_flag, host_id, hol1, hol2, hol3, hol4, hol5, hol6, hol7, hol8, hol9, hol10, hol11, hol12, hol13, hol14, hol15, hol16, hol17, hol18, hol19, hol20, Medical_Agency_No, Medical_Agency_name, Medical_Agency_phone1, Fire_Agency_No, Fire_Agency_name, Fire_Agency_phone1, Police_Agency_No, Police_Agency_name, Police_Agency_phone1, Patrol_Agency_No, Patrol_Agency_name, Patrol_Agency_phone1, county, country, phone_panel, cs_bill_id,   
	CONVERT(VARCHAR(10), sai.sai_out_of_service.oos_date, 101) AS out_of_service_date  
	from sai.sai_account   
	left join sai.sai_out_of_service on sai.sai_account.cs_no = sai.sai_out_of_service.cs_no  
	where sai.sai_account.cs_no in (select cs_no from sai.sai_account where inst_no in (#dn#)) 
	order by sai.sai_account.cs_no asc
</cfquery>
<cfset headerlist = "cs_no,name,addr1,addr2,city,state,addr_key,phone1,ext1,phone2,ext2,maploc,pd_agency_no,fd_agency_no,md_agency_no,pt_agency_no,inst_no,guards,keynum,ul_code,name_key,zip_code,cs_loc_no,gd_loc_no,ss_loc_no,sec_cs_no,red_flag,wo_no,mail_freq,map_type,udf1,udf2,start_date,ati,sec_ati,system_type,entry_delay,exit_delay,special,br_no,br_site_no,time_zone,time_zone_mins,dst_grp,site_cs_no,type,udf3,udf4,telco,account_type,num_key,service_type,local,orig_cs,orig_sec,del_date,del_flag,host_id,hol1,hol2,hol3,hol4,hol5,hol6,hol7,hol8,hol9,hol10,hol11,hol12,hol13,hol14,hol15,hol16,hol17,hol18,hol19,hol20,Medical_Agency_No,Medical_Agency_name,Medical_Agency_phone1,Fire_Agency_No,Fire_Agency_name,Fire_Agency_phone1,Police_Agency_No,Police_Agency_name,Police_Agency_phone1,Patrol_Agency_No,Patrol_Agency_name,Patrol_Agency_phone1,county,country,phone_panel,cs_bill_id">
<cfx_query2excel file="#currentdir#\#dn#.account.txt" headings="#headerlist#" queryFields="#headerlist#" query="#getAccounts#" delimiter="#chr(9)#">
<!--- process sai_account.txt --->

<!--- process sai_dispatch.txt --->
<cfquery name="getDispatch" datasource="sai">
	select cs_no,page_no, 
	CONVERT(VARCHAR(10), eff_date, 101) AS eff_date,  
	CONVERT(VARCHAR(10), exp_date, 101) AS exp_date,   
	zdline1,zdline2,zdline3,zdline4,zdline5,zdline6,zdline7,zdline8,zdline9,zdline10,zdline11,zdline12, 
	local  
	from sai.SAI_Dispatch_page  
	where sai.SAI_Dispatch_page.cs_no in (select cs_no from sai.sai_account where inst_no in (#dn#))  
	order by cs_no asc
</cfquery>
<cfset headerlist = "cs_no,page_no,eff_date,exp_date,zdline1,zdline2,zdline3,zdline4,zdline5,zdline6,zdline7,zdline8,zdline9,zdline10,zdline11,zdline12,eff_date,local">
<cfx_query2excel file="#currentdir#\#dn#.dispatch.txt" headings="#headerlist#" queryFields="#headerlist#" query="#getDispatch#" delimiter="#chr(9)#">
<!--- process sai_dispatch.txt --->

<!--- process sai_passcard.txt --->
<cfquery name="getPasscard" datasource="sai">
	select  
	cs_no, seqno, type, passcard, name, long_name, paslevel_no, exp_date, userid, call_list, relationship, local, phone1, ext1, note1, phone2, ext2, note2, phone3, ext3, note3, phone4, ext4, note4, phone5, ext5, note5, new_seqno, ifline1, ifline2, ifline3, ifline4, ifline5, ifline6, ifline7, ifline8  
	from sai.sai_passcard  
	where sai.sai_passcard.cs_no in (select cs_no from sai.sai_account where inst_no in (#dn#)) 
	order by cs_no asc
</cfquery>
<cfset headerlist = "cs_no,seqno,type,passcard,name,long_name,paslevel_no,exp_date,userid,call_list,relationship,local,phone1,ext1,note1,phone2,ext2,note2,phone3,ext3,note3,phone4,ext4,note4,phone5,ext5,note5,new_seqno,ifline1,ifline2,ifline3,ifline4,ifline5,ifline6,ifline7,ifline8">
<cfx_query2excel file="#currentdir#\#dn#.passcard.txt" headings="#headerlist#" queryFields="#headerlist#" query="#getPasscard#" delimiter="#chr(9)#">
<!--- process sai_passcard.txt --->

<!--- process sai_schedule.txt --->
<cfquery name="getSchedule" datasource="sai">
	select  
	cs_no, sched_no, open_event_no, close_event_no, eff_date, exp_date, verify_early_open_flag, verify_late_open_flag, verify_early_close_flag, verify_late_close_flag, early_open_window, late_open_window, early_close_window, late_close_window, late_open_process, late_close_process, comment, old_exp_date, packed_flag, local, mon_open1, mon_close1, mon_open2, mon_close2, mon_open3, mon_close3, mon_open4, mon_close4, mon_open5, mon_close5, mon_open6, mon_close6, tue_open1, tue_close1, tue_open2, tue_close2, tue_open3, tue_close3, tue_open4, tue_close4, tue_open5, tue_close5, tue_open6, tue_close6, wed_open1, wed_close1, wed_open2, wed_close2, wed_open3, wed_close3, wed_open4, wed_close4, wed_open5, wed_close5, wed_open6, wed_close6, thu_open1, thu_close1, thu_open2, thu_close2, thu_open3, thu_close3, thu_open4, thu_close4, thu_open5, thu_close5, thu_open6, thu_close6, fri_open1, fri_close1, fri_open2, fri_close2, fri_open3, fri_close3, fri_open4, fri_close4, fri_open5, fri_close5, fri_open6, fri_close6, sat_open1, sat_close1, sat_open2, sat_close2, sat_open3, sat_close3, sat_open4, sat_close4, sat_open5, sat_close5, sat_open6, sat_close6, sun_open1, sun_close1, sun_open2, sun_close2, sun_open3, sun_close3, sun_open4, sun_close4, sun_open5, sun_close5, sun_open6, sun_close6  
	from sai.SAI_Schedule  
	where sai.SAI_Schedule.cs_no in (select cs_no from sai.sai_account where inst_no in (#dn#))  
	order by cs_no asc
</cfquery>
<cfset headerlist = "cs_no,sched_no,open_event_no,close_event_no,eff_date,exp_date,verify_early_open_flag,verify_late_open_flag,verify_early_close_flag,verify_late_close_flag,early_open_window,late_open_window,early_close_window,late_close_window,late_open_process,late_close_process,comment,old_exp_date,packed_flag,local,mon_open1,mon_close1,mon_open2,mon_close2,mon_open3,mon_close3,mon_open4,mon_close4,mon_open5,mon_close5,mon_open6,mon_close6,tue_open1,tue_close1,tue_open2,tue_close2,tue_open3,tue_close3,tue_open4,tue_close4,tue_open5,tue_close5,tue_open6,tue_close6,wed_open1,wed_close1,wed_open2,wed_close2,wed_open3,wed_close3,wed_open4,wed_close4,wed_open5,wed_close5,wed_open6,wed_close6,thu_open1,thu_close1,thu_open2,thu_close2,thu_open3,thu_close3,thu_open4,thu_close4,thu_open5,thu_close5,thu_open6,thu_close6,fri_open1,fri_close1,fri_open2,fri_close2,fri_open3,fri_close3,fri_open4,fri_close4,fri_open5,fri_close5,fri_open6,fri_close6,sat_open1,sat_close1,sat_open2,sat_close2,sat_open3,sat_close3,sat_open4,sat_close4,sat_open5,sat_close5,sat_open6,sat_close6,sun_open1,sun_close1,sun_open2,sun_close2,sun_open3,sun_close3,sun_open4,sun_close4,sun_open5,sun_close5,sun_open6,sun_close6">
<cfx_query2excel file="#currentdir#\#dn#.schedule.txt" headings="#headerlist#" queryFields="#headerlist#" query="#getSchedule#" delimiter="#chr(9)#">
<!--- process sai_schedule.txt --->

<!--- process sai_scheduledEvents.txt --->
<cfquery name="getScheduledEvents" datasource="sai">
	select dealernumber, event, description, param1, param2, param3, param4, param5
	from sai.SAI_SCHEDULEDEVENTS 
	where dealernumber = #dn#
</cfquery>
<cfset headerlist = "dealernumber,event,description,param1,param2,param3,param4,param5">
<cfx_query2excel file="#currentdir#\#dn#.scheduledEvents.txt" headings="#headerlist#" queryFields="#headerlist#" query="#getScheduledEvents#" delimiter="#chr(9)#">
<!--- process sai_scheduledEvents.txt --->

<!--- process sai_zone.txt --->
<cfquery name="getZones" datasource="sai">
	select cs_no, page_no, line_no, zone, event_no, zd_page_no, restore_flag, secondary_flag, red_flag, global_zd, sched_no, comment, global_dispatch, local, descr  
	from sai.sai_zone  
	where sai.sai_zone.cs_no in (select cs_no from sai.sai_account where inst_no in (#dn#))  
	order by cs_no asc
</cfquery>
<cfset headerlist = "cs_no,page_no,line_no,zone,event_no,zd_page_no,restore_flag,secondary_flag,red_flag,global_zd,sched_no,comment,global_dispatch,local,descr">
<cfx_query2excel file="#currentdir#\#dn#.zone.txt" headings="#headerlist#" queryFields="#headerlist#" query="#getZones#" delimiter="#chr(9)#">
<!--- process sai_zone.txt --->

