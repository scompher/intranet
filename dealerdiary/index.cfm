<style type="text/css">
<!--
.header {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #FFFFFF;
	background-color: #000066;
}
-->
</style>

<cfparam name="dn" default="">
<cfparam name="companyName" default="">
<cfparam name="phoneNumber" default="">
<cfparam name="contactName" default="">

<cfif isDefined("form.btnGetDealer")>
	<cfif trim(dn) is "">
		<cfquery name="getdlr" datasource="#ds#">
			select * from dealerdiary_dealers 
			where 1=1 
			<cfif trim(companyName) is not "">
				and dealername like '%#companyName#%' 
			</cfif>
			<cfif trim(phoneNumber) is not "">
				and primaryphone like '%#phoneNumber#%' 
			</cfif>
			<cfif trim(contactName) is not "">
				and contact like '%#contactName#%' 
			</cfif>
			order by dealername asc 
		</cfquery>
		
		<div align="center" class="normal">
		<table border="1" cellpadding="5" cellspacing="0">
			<tr>
				<td align="center" bgcolor="FFFFCC" class="header"><b>Dealer Diary Menu</b></td>
			</tr>
			<tr>
				<td align="center">
					<cfif getdlr.recordcount gt 0>
						<table border="1" cellspacing="0" cellpadding="2">
							<tr>
								<td><b>Dealer Number</b></td>
								<td><b>Company Name</b></td>
								<td><b>Contact</b></td>
								<td><b>Phone</b></td>
							</tr>
							<cfoutput query="getdlr">
							<tr>
								<td><a href="getdealer.cfm?dn=#dealernumber#">#dealernumber#</a></td>
								<td><a href="getdealer.cfm?dn=#dealernumber#">#dealername#</a></td>
								<td>#contact#</td>
								<td>#primaryphone#</td>
							</tr>
							</cfoutput>
						</table>
					<cfelse>
						<p>There are no dealers found with that criteria</p>
					</cfif>
				</td>
			</tr>
		</table>
		<br>
		<a href="index.cfm">Return to lookup</a>
		</div>
	<cfelse>
		<cflocation url="getdealer.cfm?dn=#dn#">
	</cfif>
	<cfabort>
</cfif>


<div align="center" class="normal">
<Form method="post" action="index.cfm">
<table border="1" cellpadding="5" cellspacing="0">
	<tr>
		<td align="center" bgcolor="FFFFCC" class="header"><b>Dealer Diary Menu</b></td>
	</tr>
	<tr>
		<td align="center">
			<table border="0" cellspacing="0" cellpadding="2">
				<tr>
					<td colspan="2" align="center"><b>Look up by any of the following, partial matches as well:</b><br /><br /></td>
				</tr>
				<tr>
					<td><b>Look up by Dealer Number:&nbsp;</b></td>
					<td><input name="dn" type="text" class="normal" style="width:50px;" maxlength="5">&nbsp;</td>
				</tr>
				<tr>
					<td><b>Look up by Company Name:&nbsp;</b></td>
					<td><input name="companyName" type="text" class="normal" style="width:200px;">&nbsp;</td>
				</tr>
				<tr>
					<td><b>Look up by Phone Number:&nbsp;</b></td>
					<td><input name="phoneNumber" type="text" class="normal" style="width:130px;">&nbsp;</td>
				</tr>
				<tr>
					<td><b>Look up by Contact Name:&nbsp;</b></td>
					<td><input name="contactName" type="text" class="normal" style="width:200px;">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="2">
					<input class="normal" type="submit" name="btnGetDealer" value="View Notes" />
					</td>
				</tr>
        	</table>
		</td>
	</tr>
	<!--- 
	<tr>
		<td align="center"><a href="dealers/index.cfm">Dealer Administration</a> </td>
	</tr>
	--->
</table>
</Form>
<br>
<a href="/index.cfm">Return to Intranet Menu</a>
</div>

