<cfparam name="form.publicEntry" default="0">

<cfif isDefined("form.btnAddNote")>

	<cfset edited = createodbcdatetime(now())>

	<cfif trim(urldecode(oldnote)) is not trim(form.content)>
		<cfset form.approved = 0>
	</cfif>

	<cfset oldNoteText = urldecode(oldnote)>
	<cfset oldNoteText = replace(oldNoteText, "'", "''", "all")>

	<!--- save archive--->
	<cfquery name="addarchive" datasource="#ds#">
		insert into dealerdiary_entries_archive (entryid, oldcontent, edited)
		values (#form.id#, '#oldNoteText#', #edited#)
	</cfquery>

	<!--- save note --->
	<cfquery name="updatenote" datasource="#ds#">
		update dealerdiary_entries 
		set content = '#form.content#', edited = #edited#, subject='#form.subject#', accountNumber = '#form.accountNumber#', publicEntry = #form.publicEntry#, approved = #form.approved# 
		where entryid = #form.id#
	</cfquery>
	
	<!--- refresh opener window --->
	<cfoutput>
	<script language="JavaScript">
	<!--
	var theLocation = self.opener.location;
	if (theLocation.indexOf("review") != 0) {
		self.opener.location="index.cfm?1=1#urlstring#";	
	} else {
		self.opener.location="getdealer.cfm?dn=#form.dn##urlstring#";	
	}
	self.close();
	//-->
	</script> 
	</cfoutput>

<cfelse>

	<script language="JavaScript" type="text/JavaScript">
	function checkform(frm) {
		if (frm.departmentid.selectedIndex == 0) {alert('Please choose a department.'); frm.departmentid.focus(); return false;}
		if (frm.subject.value == "") {alert('Please enter the subject of the note.'); frm.subject.focus(); return false;}
		if (frm.content.value == "") {alert('Please enter the body of the note.'); frm.content.focus(); return false;}
		
		frm.submit();
	}
	</script>
	
	<cfquery name="getnote" datasource="#ds#">
		select * from dealerdiary_entries
		where entryid = #id#
	</cfquery>
	
	<cfquery name="getuser" datasource="#ds#">
		select * from admin_users where adminuserid = #cookie.adminlogin#
	</cfquery>

	<cfquery name="getdepts" datasource="#ds#">
		select Admin_Users_Departments.*
		from Admin_Users_Departments
		inner join Admin_Users_Departments_Lookup on Admin_Users_Departments.departmentid = Admin_Users_Departments_Lookup.departmentid
		where Admin_Users_Departments_Lookup.adminuserid = #cookie.adminlogin#
		order by department asc
	</cfquery>
	
	<body onLoad="document.forms[0].content.focus();">
	<div align="center">
		<form method="post" action="editnote.cfm">
		<cfoutput>
		<input type="hidden" name="dn" value="#getnote.dealernumber#">
		<input type="hidden" name="id" value="#id#">
		<input type="hidden" name="oldnote" value="#urlencodedformat(getnote.content)#">
		<input type="hidden" name="urlstring" value="#urlstring#">
		<input type="hidden" name="approved" value="#getnote.approved#">
		</cfoutput>
		<table border="0" cellpadding="3">
			<cfoutput>
			<tr>
				<td><b>#getuser.firstname# #getuser.lastname#</b></td>
			</tr>
			</cfoutput>
			<tr>
				<td>
				<b>Department:</b><br>
				<select name="departmentid" class="normal">
					<option value="0"></option>
				<cfloop query="getdepts">
				<cfoutput>
					<option value="#departmentid#" <cfif getnote.departmentid IS getdepts.departmentid>selected</cfif> >#department#</option>
				</cfoutput>
				</cfloop>
				</select>
				</td>
			</tr>
			<cfoutput query="getnote">
			<tr>
				<td>
				<b>Account Number:</b> (If applicable)<br>
				<input type="text" name="accountNumber" style="width:90px" maxlength="12" value="#getnote.accountNumber#">
				</td>
			</tr>
			<tr>
				<td>
				<b>Subject:</b><br>
				<input type="text" name="subject" style="width:500px" maxlength="255" value="#getnote.subject#"></td>
			</tr>
			<tr>
				<td>
				<b>Body:</b><br>
				<textarea name="content" style="width:500px" rows="7" class="normal">#getnote.content#</textarea></td>
			</tr>
			<tr>
				<td valign="middle">
				<input <cfif getnote.publicEntry is 1>checked</cfif> type="checkbox" name="publicEntry" value="1" style="vertical-align:middle">
				<b>Make this note publicly viewable on COP-A-LINK</b><br>
				(note: public notes must be approved before showing up on COP-A-LINK)
				</td>
			</tr>
			</cfoutput>
			<tr>
				<td>
				<input name="btnAddNote" type="submit" value="Update Note">
				<input onClick="self.close();" type="button" value="Close">
				</td>
			</tr>
		</table>
		</form>
	</div>
	</body>
	
</cfif>
