<style type="text/css">
<!--
.header {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #FFFFFF;
	background-color: #000066;
}
-->
</style>

<script type="text/javascript">
function viewPrintWin(u) {
	window.open(u,"inqWin","width=600,height=500,scrollbars=1");
}
</script>

<script type="text/javascript">
function viewRequestDetails(theLink) {
	window.open(theLink,"RequestDetails","width=800,height=450,scrollbars=1");
}
</script>

<cfquery name="getnoteinfo" datasource="#ds#">
	select dealerdiary_entries.trackerComments, dealerdiary_entries.entryid, dealerdiary_entries.dealernumber, dealerdiary_entries.parentid, dealerdiary_entries.created, dealerdiary_entries.edited, dealerdiary_entries.content, dealerdiary_entries.subject, dealerdiary_entries.departmentid, admin_users.firstname, admin_users.lastname, admin_users.adminuserid
	from dealerdiary_entries
	left join admin_users on dealerdiary_entries.creatorid = admin_users.adminuserid
	where dealerdiary_entries.entryid = #id#
</cfquery>

<cfif getnoteinfo.trackerComments is 1>
<cfquery name="getTrackerComments" datasource="#ds#">
	select *
	from dealerdiary_comments
	where dealerdiaryid = #id#
</cfquery>
</cfif>

<cfif getnoteinfo.parentid IS NOT 0>
	<cfset id = getnoteinfo.parentid>
</cfif>

<div align="center" class="normal">
<input type="button" value="Close Window" onclick="self.close();">
<br>
<br>
<table width="500" border="1" cellspacing="0" cellpadding="5">
	<cfoutput query="getnoteinfo">
	<tr>
		<td colspan="2" bgcolor="FFFFCC" class="header">
		<b>#getnoteinfo.firstname# #getnoteinfo.lastname# : Created #dateformat(created, 'mm/dd/yyyy')# #timeformat(created, 'hh:mm tt')#
		<cfif trim(getnoteinfo.edited) IS NOT ""> : Edited #dateformat(edited, 'mm/dd/yyyy')# #timeformat(edited, 'hh:mm tt')# : 
		<a style="color:FFFFFF" href="notehist.cfm?id=#getnoteinfo.entryid#">Click to View Edit History</a>
		</cfif></b>&nbsp;
		<cfif isdefined("cookie.adminlogin")><cfif getnoteinfo.adminuserid IS cookie.adminlogin><a style="color:FFFFFF" href="editnote.cfm?id=#entryid#&urlstring=#urlencodedformat(urlstring)#">[Edit Note]</a></cfif></cfif>
		</td>
	</tr>
	<cfif getnoteinfo.trackerComments is 0 or getnoteinfo.trackerComments is ''>
<!---		<tr>
			<td colspan="2">Account Number: #accountNumber#</td>
		</tr>--->
	
		<tr>
			<td colspan="2">#replace(getnoteinfo.content,"#chr(13)#","<br>","all")#</td>
		</tr>
		<cfelse>
		<cfloop query="getTrackerComments">
		<cfset commentDate = dateformat(getTrackerComments.date,'MM/DD/YYYY')>
		<tr>
			<td colspan="2">#commentDate# - #replace(getTrackerComments.comment,"#chr(10)#","<br>","all")#</td>
		</tr>
		</cfloop>
	</cfif>
<!---	<cfif publicEntry is 0>--->
		<tr>
			<td colspan="2" align="center"><a href="reply.cfm?pid=#id#&did=#getnoteinfo.departmentid#&dn=#getnoteinfo.dealernumber#&urlstring=#urlencodedformat(urlstring)#"><b>Add a reply</b></a></td>
		</tr>
<!---	<cfelse>
		<tr>
			<td colspan="2" align="center">This note is marked for public viewing</td>
		</tr>
		<tr>
			<td colspan="2" align="center">
			<cfif approved is 1>
				<cfquery name="getApprovedBy" datasource="#ds#">
					select * from admin_users 
					where adminuserid = #getnoteinfo.approvedby# 
				</cfquery>
				This note was approved by #getapprovedby.firstname# #getapprovedby.lastname# on #dateformat(dateTimeApproved,'mm/dd/yyyy')# #timeformat(dateTimeApproved,'hh:mm tt')# 
			<cfelse>
				This note has not been approved yet
			</cfif>
			</td>
		</tr>
	</cfif>
--->
</table>
	</cfoutput>
<br>
</div>
