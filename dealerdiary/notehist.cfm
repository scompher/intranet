<style type="text/css">
<!--
.header {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #FFFFFF;
	background-color: #000066;
}
-->
</style>

<cfquery name="getnotearchive" datasource="#ds#">
	select dealerdiary_entries_archive.*, dealerdiary_entries.entryid
	from dealerdiary_entries_archive
	inner join dealerdiary_entries on dealerdiary_entries_archive.entryid = dealerdiary_entries.entryid
	where dealerdiary_entries_archive.entryid = #id#
	order by dealerdiary_entries_archive.edited DESC
</cfquery>

<div align="center" class="normal">
<input type="button" value="Close Window" onClick="self.close();">
<br>
<br>
<table width="100%" border="1" cellpadding="5" cellspacing="0">
	<cfoutput query="getnotearchive">
	<tr>
		<td bgcolor="FFFFCC" class="header"><B>#dateformat(edited, 'mm/dd/yyyy')# #timeformat(edited, 'hh:mm tt')#</B></td>
	</tr>
	<tr>
		<td>#oldcontent#</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	</cfoutput>
</table>
<br>
<a href="javascript:history.go(-1);">Go Back</a>
</div>
