
<cfquery name="getInqs" datasource="#ds#">
	select * 
	from inquiries_main 
</cfquery>

<cfloop query="getInqs">
	<cfquery name="getOtherInfo" datasource="#ds#">
		select top 1 * from inquiries_status_history
		where inquiryid = #getinqs.inquiryid# 
	</cfquery>

	<cfset subject = "Inquiry #getInqs.inquiryNumber# was created on #dateformat(getInqs.dateTimeCreated,'mm/dd/yyyy')#">
	<cfset inqText = "<a href=""javascript:viewPrintWin(''/inquiry/viewInquiry.cfm?i=#getInqs.inquiryid#'');"">View/Print Full Inquiry</a>#chr(13)##chr(13)##getInqs.inquiryText#">
	
	<cfquery name="insertIntoDiary" datasource="#ds#">
		insert into dealerdiary_entries (creatorID, dealernumber, created, content, subject, departmentid, parentid, inquiryNote)
		values (#getOtherInfo.adminuserid#, '#getInqs.dealerNumber#', #createodbcdatetime(getInqs.dateTimeCreated)#, '#inqText#', '#subject#', #getOtherInfo.fromDepartmentID#, 0, 1)
	</cfquery>
	
</cfloop>

Finito