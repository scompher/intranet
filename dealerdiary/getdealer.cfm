<script src="/jQuery/jquery-2.1.3.min.js"></script>
<script src="/jquery-ui-1.11.4/jquery-ui.min.js"></script>
<link rel="stylesheet" href="/jquery-ui-1.11.4/jquery-ui.min.css" />
<link rel="stylesheet" href="/jquery-ui-1.11.4/jquery-ui.theme.min.css"/>
  <script>
  $(function() {
    $( "#newDateInactive" ).datepicker();
  });
  </script>

<cfparam name="dn" default="">

<style type="text/css">
<!--
.header {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #FFFFFF;
	background-color: #000066;
}
-->
</style>

<cfoutput>
<script language="JavaScript" type="text/JavaScript">
function addnote(urlstring) {
	notewin = window.open("addnote.cfm?dn=#dn#&urlstring=" + urlstring,"notewin","width=550,height=400");
	notewin.focus();
}
function viewnote(id,urlstring) {
	notewin = window.open("noteview.cfm?id=" + id + "&urlstring=" + urlstring,"notewin","width=625,height=425,scrollbars=yes,resizable=yes");
	notewin.focus();
}
function notifyacctmanager(dn) {
	notifywin = window.open("notifyacctmanager.cfm?dn=" + dn,"notifywin","width=600,height=500,resizable=yes,scrollbars=yes");
	notifywin.focus();
}
function printnotes(idlist) {
	printwin = window.open("printnotes.cfm?ids=" + idlist,"printwin","width=630,height=450,resizable=yes,scrollbars=yes");
	printwin.focus();	
}
function editnotation(id) {
	notewin = window.open("editnotation.cfm?id=" + id, "notewin","width=550,height=400");
	notewin.focus();
}
</script>
</cfoutput>

<!--- get sec level --->
<cfquery name="getsec" datasource="#ds#">
	select admin_users.*, admin_users_departments_lookup.*
	from admin_users 
	inner join admin_users_departments_lookup on admin_users.adminuserid = admin_users_departments_lookup.adminuserid
	where admin_users.adminuserid = #cookie.adminlogin#
</cfquery>

<cfif isdefined("cookie.adminlogin")>
	<cfquery name="getuser" datasource="#ds#">
		select * from admin_users where adminuserid = #cookie.adminlogin#
	</cfquery>
</cfif>

<cfset urlstring = "">
<cfif isdefined("deptid")><cfset urlstring = urlstring & "&deptid=#deptid#"></cfif>
<cfif isdefined("searchtype")><cfset urlstring = urlstring & "&searchtype=#searchtype#"></cfif>
<cfif isdefined("startdate")><cfset urlstring = urlstring & "&startdate=#startdate#"></cfif>
<cfif isdefined("enddate")><cfset urlstring = urlstring & "&enddate=#enddate#"></cfif>
<cfif isdefined("searchphrase")><cfset urlstring = urlstring & "&searchphrase=#searchphrase#"></cfif>

<cfparam name="sort" default="date_desc">
<cfif isdefined("cookie.adminlogin")>
	<!--- if mark taylor, default to all depts --->
	<cfif cookie.adminlogin is 98 or cookie.adminlogin is 1>
		<cfparam name="deptid" default="0">
	<cfelse>
		<cfparam name="deptid" default="#getuser.defaultdeptid#">
	</cfif>
<cfelse>
	<cfparam name="deptid" default="0">
</cfif>
<cfparam name="searchtype" default="AND">

<cfswitch expression="#sort#">
	<cfcase value="name_asc"><cfset orderby = "admin_users.lastname asc"></cfcase>
	<cfcase value="name_desc"><cfset orderby = "admin_users.lastname desc"></cfcase>
	<cfcase value="subject_asc"><cfset orderby = "dealerdiary_entries.subject asc"></cfcase>
	<cfcase value="subject_desc"><cfset orderby = "dealerdiary_entries.subject desc"></cfcase>
	<cfcase value="date_asc"><cfset orderby = "dealerdiary_entries.created asc"></cfcase>
	<cfcase value="date_desc"><cfset orderby = "dealerdiary_entries.created desc"></cfcase>
	<cfcase value="dept_asc"><cfset orderby = "Admin_Users_Departments.department asc"></cfcase>
	<cfcase value="dept_desc"><cfset orderby = "Admin_Users_Departments.department desc"></cfcase>
</cfswitch>

<cfif isdefined("updateDateInactive")>
	<cfquery name="updateInactive" datasource="#ds#">
		update dealerdiary_dealers
		set dateInactive = '#newDateInactive#'
		where dealerNumber = '#dn#'
	</cfquery>
</cfif>

<cfquery name="getdealer" datasource="#ds#">
	select *
	from dealerdiary_dealers
	where dealerdiary_dealers.dealernumber = '#dn#'
</cfquery>
<cfquery name="lastMpowerLogin" datasource="copalink">
	select top 1 *
	from dealer_logins
	where dealerNumber = '#dn#'
	order by loggedin desc
</cfquery>
<cfif lastMpowerLogin.recordcount gt 0>
	<cfset lastLogin = "#DateFormat(lastMpowerLogin.loggedin,"MM/dd/yyyy")# at #TimeFormat(lastMpowerLogin.loggedin,"hh:mm:ss tt")#">
<cfelse>
	<cfset lastLogin = "No recent login found.">
</cfif>
<script language="javascript" src="cal2.js">
/*
Xin's Popup calendar script-  Xin Yang (http://www.yxscripts.com/)
Script featured on/available at http://www.dynamicdrive.com/
This notice must stay intact for use
*/
</script>
<script language="javascript" src="cal_conf2.js"></script>

<script language="JavaScript" type="text/JavaScript">
function applyFilters(frm) {
	if ((frm.startdate.value != "") && (frm.enddate.value != "")) {
		var sdate = new Date(frm.startdate.value);
		var edate = new Date(frm.enddate.value);
		if (sdate > edate) {alert('The starting date cannot be later than the ending date.'); return false;}
	}
	frm.submit();
}
</script>

<cfif isdefined("startdate") and trim(startdate) IS NOT ""><cfset odbc_startdate = createodbcdate(startdate)></cfif>
<cfif isdefined("enddate") and trim(enddate) IS NOT ""><cfset odbc_enddate = dateadd("d",1,createodbcdate(enddate))></cfif>

<cfif isdefined("searchphrase")>
	<cfset searchphrase = trim(searchphrase)>
	<cfif trim(searchphrase) IS NOT "">
		<cfset titlekeywords = "">
		<cfloop index="item" list="#searchphrase#">
			<cfif item IS NOT listgetat(searchphrase, listlen(searchphrase))>
				<cfset titlekeywords = titlekeywords & "dealerdiary_entries.subject LIKE '%#trim(item)#%' #searchtype# ">
			<cfelse>
				<cfset titlekeywords = titlekeywords & "dealerdiary_entries.subject LIKE '%#trim(item)#%'">
			</cfif>
		</cfloop>
		<cfset titlekeywords = "(" & titlekeywords & ")">
	
		<cfset bodykeywords = "">
		<cfloop index="item" list="#searchphrase#">
			<cfif item IS NOT listgetat(searchphrase, listlen(searchphrase))>
				<cfset bodykeywords = bodykeywords & "dealerdiary_entries.content LIKE '%#trim(item)#%' #searchtype# ">
			<cfelse>
				<cfset bodykeywords = bodykeywords & "dealerdiary_entries.content LIKE '%#trim(item)#%'">
			</cfif>
		</cfloop>
		<cfset bodykeywords = "(" & bodykeywords & ")">
		<cfset keywords = titlekeywords & " OR " & bodykeywords>
	</cfif>
</cfif>

<cfquery name="getnoteinfo" datasource="#ds#">
	select dealerdiary_entries.parentid, dealerdiary_entries.entryid, dealerdiary_entries.created, dealerdiary_entries.edited, dealerdiary_entries.content, dealerdiary_entries.subject, admin_users.firstname, admin_users.lastname, admin_users.adminuserid, Admin_Users_Departments.department
	from dealerdiary_entries
	left join admin_users on dealerdiary_entries.creatorid = admin_users.adminuserid
	left join Admin_Users_Departments on dealerdiary_entries.departmentid = Admin_Users_Departments.departmentid
	where dealerdiary_entries.dealernumber = '#dn#' 
	<cfif isdefined("keywords") and trim(keywords) IS NOT "">
	and (#preservesinglequotes(keywords)#)
	</cfif>
	<cfif deptid IS NOT 0>and dealerdiary_entries.departmentid = #deptid#</cfif> 
	<cfif isdefined("odbc_startdate") and isdefined("odbc_enddate")>and created >= #odbc_startdate# and created < #odbc_enddate#</cfif> 
	order by #orderby#
</cfquery>

<cfquery name="getdepts" datasource="#ds#">
	select * from Admin_Users_Departments
	order by department asc
</cfquery>

<div align="center" class="normal">
<cfif getdealer.recordcount GT 0>

	<table width="725" border="1" cellspacing="0" cellpadding="5">
		<tr>
			<td colspan="2" align="center" bgcolor="FFFFCC" class="header"><b>Dealer Diary - View Dealer Information</b></td>
		</tr>
		<cfoutput query="getdealer">
		<tr valign="top">
			<td width="50%"><b>Dealer Number : </b>#getdealer.dealernumber#</td>
		    <td width="50%"><b>Dealer Name : </b>#getdealer.dealername#</td>
		</tr>
		<tr valign="top">
			<td><b>Primary Phone : </b>#getdealer.primaryphone#</td>
		    <td><b>Secondary Phone :</b> #getdealer.secondaryphone#</td>
		</tr>
		<tr valign="top">
			<td><b>Account Manager : </b>#getdealer.acctmanager#</td>
		    <td><b>Contact : </b>#getdealer.contact#</td>
		</tr>
		<tr>
			<td colspan="2"><b>Special Notation:</b>
			<cfif getsec.seclevelid lte 2>&nbsp;<a href="javascript:editnotation(#getdealer.dealerid#);">[edit]</a></cfif>
			&nbsp;#getdealer.notes#</td>
		</tr>
		<tr>
			<td colspan="2"><b>Last MPower Login:</b> #lastLogin#</td>
		</tr>
		<tr>
			<cfset dateInact = dateFormat(#getdealer.dateInactive#,'MM/DD/YY')>
			<cfif dateInact is '01/01/00'>
				<cfset dateInact = 'Currently Active'>
			</cfif>
			<form method="post" action="getdealer.cfm">
				<input type="hidden" name="dn" value="#dn#">
				<td colspan="2">
					<b>Date Inactive:</b> #dateInact#
					<span style="float:right">
						<input type="text" name="newDateInactive" id="newDateInactive" />
						<input type="submit"  value="Update" id="updateDateInactive" name="updateDateInactive" />
					</span>
				</td>
			</form>
		</tr>
		<tr>
			<td colspan="2" bgcolor="DDDDDD"><b>Notes</b></td>
		</tr>
		<form method="post" action="getdealer.cfm">
		<input type="hidden" name="dn" value="#dn#">
		<input type="hidden" name="sort" value="#sort#">
		<tr>
			<td colspan="2">
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td><b>Department:</b>&nbsp;&nbsp;</td>
					<td>
					<select name="deptid" onChange="this.form.submit();">
						<option value="0" <cfif deptid IS 0>selected</cfif> >All Departments</option>
					<cfloop query="getdepts">
						<option value="#departmentid#" <cfif getdepts.departmentid IS deptid>selected</cfif> >#department#</option>
					</cfloop>
					</select>&nbsp;&nbsp;					</td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td colspan="2"><b>Filter Notes By Date:</b>
			<table border="0" cellspacing="0" cellpadding="3">
            	<tr>
            		<td>
					<b>Start: </b>
            		<input onClick="showCal('StartDate');" name="startdate" type="text" size="15" maxlength="50" class="normal" <cfif isdefined("startdate")>value="#startdate#"</cfif> >
                    <a style="text-decoration:none;" href="javascript:showCal('StartDate');"><img src="../images/calicon.gif" width="20" height="20" border="0" align="absmiddle"></a>&nbsp;&nbsp;					</td>
            		<td>
					<b>End: </b>
           			<input onClick="showCal('EndDate');" name="enddate" type="text" size="15" maxlength="50" class="normal" <cfif isdefined("enddate")>value="#enddate#"</cfif> >
                    <a style="text-decoration:none;" href="javascript:showCal('EndDate');"><img src="../images/calicon.gif" width="20" height="20" border="0" align="absmiddle"></a>					</td>
            	</tr>
            </table>
			<br>
			<b>Keyword Search:</b> (separate keywords with a comma) <br> 
			<input name="searchphrase" type="text" size="30" maxlength="100" class="normal" <cfif isdefined("searchphrase")>value="#searchphrase#"</cfif> >
			<br>
			<input name="searchtype" type="radio" value="AND" <cfif isdefined("searchtype")><cfif searchtype IS "AND">checked</cfif></cfif> >
			All keywords
			<br>
			<input name="searchtype" type="radio" value="OR" <cfif isdefined("searchtype")><cfif searchtype IS "OR">checked</cfif></cfif> >
			Some keywords<br>
			<br>
			<input type="button" value="Apply Filters" class="normal" onClick="applyFilters(this.form);">
			<input type="button" value="Reset View" class="normal" onClick="document.location='getdealer.cfm?dn=#dn#';">
			</td>
		</tr>
		<tr>
			<td colspan="2">
			<cfif isdefined("cookie.adminlogin")>
				<input onClick="addnote('#urlencodedformat(urlstring)#');" type="button" value="Add Note">
			</cfif>
			</td>
		</tr>
		</form>
		</cfoutput>
		<tr>
			<td colspan="2">
			<table width="100%" border="1" cellspacing="0" cellpadding="3">
				<cfoutput>
				<tr>
					<td width="20%" bgcolor="DDDDDD">
					<cfif sort IS "name_asc"><cfset sortlink = "name_desc"><cfelse><cfset sortlink = "name_asc"></cfif>
					<b><a href="getdealer.cfm?sort=#sortlink#&dn=#dn##urlstring#">Name</a></b>					</td>
					<td bgcolor="DDDDDD">
					<cfif sort IS "subject_asc"><cfset sortlink = "subject_desc"><cfelse><cfset sortlink = "subject_asc"></cfif>
					<b><a href="getdealer.cfm?sort=#sortlink#&dn=#dn##urlstring#">Subject</a></b>					</td>
					<td width="18%" align="center" bgcolor="DDDDDD">
					<cfif sort IS "date_asc"><cfset sortlink = "date_desc"><cfelse><cfset sortlink = "date_asc"></cfif>
					<b><a href="getdealer.cfm?sort=#sortlink#&dn=#dn##urlstring#">Date &amp; Time</a></b>					</td>
					<td bgcolor="DDDDDD" width="15%" align="center">
					<cfif sort IS "dept_asc"><cfset sortlink = "dept_desc"><cfelse><cfset sortlink = "dept_asc"></cfif>
					<b><a href="getdealer.cfm?sort=#sortlink#&dn=#dn##urlstring#">Department</a></b>					</td>
				</tr>
				</cfoutput>
				<cfset idlist = "">
				<cfloop query="getnoteinfo">
					<cfquery name="getreplies" dbtype="query">
						select * from getnoteinfo where parentid = #getnoteinfo.entryid# order by created asc
					</cfquery>
					<cfif isdefined("keywords") and trim(keywords) IS NOT "">
					<cfset idlist = listappend(idlist,getnoteinfo.entryid)>
						<cfoutput>
						<tr>
							<td valign="top" nowrap>#getnoteinfo.firstname# #getnoteinfo.lastname#</td>
							<td valign="top"><a href="##" onClick="viewnote(#getnoteinfo.entryid#, '#urlencodedformat(urlstring)#');">#getnoteinfo.subject#</a>&nbsp;</td>
							<td valign="top" align="center">#dateformat(created, 'mm/dd/yyyy')# #timeformat(created,'hh:mm tt')#</td>
							<td valign="top" align="center" nowrap>#getnoteinfo.department#</td>
						</tr>
						</cfoutput>
					<cfelse>
						<cfif getnoteinfo.parentid IS 0>
						<cfset idlist = listappend(idlist,getnoteinfo.entryid)>
							<cfoutput>
							<tr>
								<td valign="top" nowrap>#getnoteinfo.firstname# #getnoteinfo.lastname#</td>
								<td valign="top"><a href="##" onClick="viewnote(#getnoteinfo.entryid#, '#urlencodedformat(urlstring)#');">#getnoteinfo.subject#</a>&nbsp;</td>
								<td valign="top" align="center">#dateformat(created, 'mm/dd/yyyy')# #timeformat(created,'hh:mm tt')#</td>
								<td valign="top" align="center" nowrap>#getnoteinfo.department#</td>
							</tr>
							</cfoutput>
							<cfloop query="getreplies">
							<cfset idlist = listappend(idlist,getreplies.entryid)>
							<cfoutput>
							<tr>
								<td valign="top" nowrap>#getreplies.firstname# #getreplies.lastname#</td>
								<td valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="##" onClick="viewnote(#getreplies.entryid#, '#urlencodedformat(urlstring)#');">#getreplies.subject#</a>&nbsp;</td>
								<td valign="top" align="center">#dateformat(getreplies.created, 'mm/dd/yyyy')# #timeformat(getreplies.created,'hh:mm tt')#</td>
								<td valign="top" align="center" nowrap>#getreplies.department#</td>
							</tr>
							</cfoutput>
							</cfloop>
						</cfif>
					</cfif>
				</cfloop>
			</table>
			</td>
		</tr>
		<!--- 
		<tr>
			<td colspan="2" bgcolor="FFFFCC">
			<b>#getnoteinfo.firstname# #getnoteinfo.lastname# : Created #dateformat(created, 'mm/dd/yyyy')# #timeformat(created, 'hh:mm tt')#<cfif trim(getnoteinfo.edited) IS NOT ""> : Edited #dateformat(edited, 'mm/dd/yyyy')# #timeformat(edited, 'hh:mm tt')# : <a href="javascript:notehist(#getnoteinfo.entryid#);">Click to View Edit History</a></cfif></b>&nbsp;
			<cfif isdefined("cookie.adminlogin")><cfif getnoteinfo.adminuserid IS cookie.adminlogin><a href="javascript:editnote(#entryid#);">[Edit Note]</a></cfif></cfif>
			</td>
		</tr>
		<tr>
			<td colspan="2">#replace(getnoteinfo.content,"#chr(13)#","<br>","all")#</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		 --->
		<cfoutput>
		<tr>
			<td colspan="2">
			<cfif isdefined("cookie.adminlogin")>
				<input onClick="addnote('#urlencodedformat(urlstring)#');" type="button" value="Add Note">
			</cfif>
			<input onClick="printnotes('#idlist#');" type="button" value="Print These Notes">
			<!--- 10/20/2005 : pg : removed until i get account manager data.
			<cfif isdefined("cookie.adminlogin")>
				<input onClick="notifyacctmanager(#dn#);" type="button" value="Notify Account Manager">
			</cfif>
			--->
			</td>
		</tr>
		</cfoutput>
	</table>

<cfelse>

	<br>
	<table width="725" border="1" cellspacing="0" cellpadding="5">
		<tr>
			<td bgcolor="FFFFCC" class="header"><b>Notice</b></td>
		</tr>
		<cfoutput>
		<tr>
			<td><b>That dealer is not on file, <a href="dealers/add.cfm?return=yes&dn=#dn#">click here</a> to enter into the system.</b></td>
		</tr>
		</cfoutput>
	</table>

</cfif>
<cfif isdefined("cookie.adminlogin")>
<br>
<a href="index.cfm">Return to Dealer Diary</a>
</cfif>
</div>
