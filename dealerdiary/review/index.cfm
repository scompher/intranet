
<link rel="stylesheet" type="text/css" href="/styles.css">

<script language="JavaScript" type="text/JavaScript">
function viewnote(id,urlstring) {
	notewin = window.open("../noteview.cfm?id=" + id + "&urlstring=" + urlstring,"notewin","width=625,height=425,scrollbars=yes,resizable=yes");
	notewin.focus();
}
function editnotation(id) {
	notewin = window.open("../editnotation.cfm?id=" + id, "notewin","width=550,height=400");
	notewin.focus();
}
</script>

<cfif isDefined("url.approve")>
	<cfquery name="approveNote" datasource="#ds#">
		update dealerdiary_entries 
		set approved = 1, approvedBy = #cookie.adminlogin#, dateTimeApproved = #createodbcdatetime(now())# 
		where entryid = #noteid# 
	</cfquery>
	<cflocation url="index.cfm">
</cfif>

<cfif isDefined("url.remove")>
	<cfquery name="removeNote" datasource="#ds#">
		delete from dealerdiary_entries 
		where entryid = #noteid# 
	</cfquery>
	<cflocation url="index.cfm">
</cfif>

<cfquery name="getNotes" datasource="#ds#">
	select * 
	from dealerdiary_entries
	where approved = 0 and publicEntry = 1
</cfquery>

<div align="center">
<table width="700" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Review Unapproved Public Notes</b></td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td width="10%" align="center" nowrap class="linedrowrightcolumn"><b>Action</b></td>
					<td width="50%" class="linedrowrightcolumn"><b>Subject</b></td>
					<td width="17%" align="center" nowrap class="linedrowrightcolumn"><b>Submitted By </b></td>
					<td width="23%" align="center" nowrap class="linedrow"><b>Date/Time Submitted </b></td>
				</tr>
				<cfloop query="getnotes">
					<cfquery name="getCreator" datasource="#ds#">
						select * from admin_users 
						where adminuserid = #getnotes.creatorid#
					</cfquery>
					<cfoutput>
					<tr>
						<td valign="top" nowrap class="linedrowrightcolumn">
						<a href="##" onClick="viewnote(#getNotes.entryid#, '');">[View]</a><br>
						<a onClick="return confirm('Are you sure you wish to approve this note?');" href="index.cfm?approve=1&noteid=#getnotes.entryid#">[Approve]</a><br>
						<a onClick="return confirm('Are you sure you wish to remove this note?');" href="index.cfm?remove=1&noteid=#getnotes.entryid#">[Remove]</a>
						</td>
						<td valign="top" class="linedrowrightcolumn">#subject#</td>
						<td align="center" valign="top" nowrap class="linedrowrightcolumn">#getcreator.firstname# #getcreator.lastname#</td>
						<td align="center" valign="top" nowrap class="linedrow">#dateformat(created, 'mm/dd/yyyy')# #timeformat(created, 'hh:mm tt')#</td>
					</tr>
					</cfoutput>
				</cfloop>
			</table>
		</td>
	</tr>
</table>
<br>
<a style="text-decoration:underline;" class="normal" href="/index.cfm">Return to Intranet</a>
</div>

