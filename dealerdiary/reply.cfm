
<cfif isDefined("form.btnAddNote")>

	<cfset created = createodbcdatetime(now())>
	
	<!--- save note --->
	<cfquery name="addnote" datasource="#ds#">
		insert into dealerdiary_entries (creatorID, dealernumber, created, content, subject, departmentid, parentid)
		values (#cookie.adminlogin#, '#dn#', #created#, '#form.content#', '#form.subject#', #form.departmentid#, #form.parentid#)
	</cfquery>
	
<!--- refresh opener window --->
<cfoutput>
<script language="JavaScript">
<!--
self.opener.location="getdealer.cfm?dn=#dn##urlstring#";
self.close();
//-->
</script> 
</cfoutput>

<cfelse>

	<script language="JavaScript" type="text/JavaScript">
	function checkform(frm) {
		if (frm.departmentid.selectedIndex == 0) {alert('Please choose a department.'); frm.departmentid.focus(); return false;}
		if (frm.subject.value == "") {alert('Please enter the subject of the note.'); frm.subject.focus(); return false;}
		if (frm.content.value == "") {alert('Please enter the body of the note.'); frm.content.focus(); return false;}
		
		frm.submit();
	}
	</script>

	<cfquery name="getuser" datasource="#ds#">
		select * from admin_users where adminuserid = #cookie.adminlogin#
	</cfquery>
	<cfquery name="getnoteinfo" datasource="#ds#">
		select * from dealerdiary_entries
		where entryid = #pid#
	</cfquery>
<!--- 
	<cfquery name="getdepts" datasource="#ds#">
		select Admin_Users_Departments.*
		from Admin_Users_Departments
		inner join Admin_Users_Departments_Lookup on Admin_Users_Departments.departmentid = Admin_Users_Departments_Lookup.departmentid
		where Admin_Users_Departments_Lookup.adminuserid = #cookie.adminlogin#
		order by department asc
	</cfquery>
 --->
	<body onLoad="document.forms[0].content.focus();">
	<div align="center">
		<form method="post" action="reply.cfm">
		<cfoutput>
		<input type="hidden" name="urlstring" value="#urlstring#">
		<input type="hidden" name="dn" value="#dn#">
		<input type="hidden" name="departmentid" value="#did#">
		<input type="hidden" name="parentid" value="#pid#">
		<input type="hidden" name="subject" value="RE: #getnoteinfo.subject#">
		</cfoutput>
		<table border="0" cellpadding="3">
			<cfoutput>
			<tr>
				<td><b>#getuser.firstname# #getuser.lastname#</b></td>
			</tr>
			</cfoutput>
			<!--- 
			<tr>
				<td>
				<b>Department:</b><br>
				<select name="departmentid" class="normal">
					<option value="0"></option>
				<cfloop query="getdepts">
				<cfoutput>
					<option value="#departmentid#">#department#</option>
				</cfoutput>
				</cfloop>
				</select>
				</td>
			</tr>
			--->
			<tr>
				<td>
				<b>Body of reply:</b><br>
				<textarea name="content" style="width:500px" rows="7" class="normal"></textarea>
				</td>
			</tr>
			<tr>
				<td>
				<input type="hidden" name="btnAddNote" value="1">
				<input onClick="checkform(this.form);" type="button" value="Add Reply">
				<input onClick="self.close();" type="button" value="Close">
				</td>
			</tr>
		</table>
		</form>
	</div>
	</body>

</cfif>
