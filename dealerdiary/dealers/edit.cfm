
<style type="text/css">
	TABLE {font-family:Arial, Helvetica, sans-serif;font-size:12px; background-color:#FFFFFF;}
	.normal {font-family:Arial, Helvetica, sans-serif;font-size:12px}
	.heading {font-family:Arial, Helvetica, sans-serif;font-size:16px}
	.titlebar {font-family: Arial, Helvetica, sans-serif;	font-size: 12px;font-weight: bold;color: #FFFFFF;}
	.small {font-family:Verdana, Arial, Helvetica, sans-serif;font-size:10px}
	a:link {color:#000000; text-decoration: underline;}
	a:visited {text-decoration: underline; color: #000000;}
	a:hover {text-decoration: underline; color: #0000FF;}
	a:active {text-decoration: underline; color: #000000;}
	.box {border: 1px solid #000000;}
	.header {
		font-family: Arial, Helvetica, sans-serif;
		font-size: 12px;
		color: #FFFFFF;
		background-color: #000066;
	}
-->
</style>

<cfif isdefined("form.btnEditDealer")>

	<!--- save info --->
	<cfquery name="savedealer" datasource="#ds#">
		update dealerdiary_dealers 
		set dealernumber = '#form.dealernumber#', dealername = '#form.dealername#', primaryphone = '#form.primaryphone#', secondaryphone = '#form.secondaryphone#', acctmanager = '#form.acctmanager#', contact = '#form.contact#'
		where dealerid = #form.dealerid#
	</cfquery>

	<cflocation url="index.cfm">

<cfelse>

	<cfquery name="getdealer" datasource="#ds#">
		select * from dealerdiary_dealers
		where dealerid = #id#
	</cfquery>

	<script language="JavaScript" type="text/JavaScript">
	function checkform(frm) {
		if (frm.dealernumber.value == "") {alert('The dealer number is required.'); frm.dealernumber.focus(); return false;}
		if (frm.dealername.value == "") {alert('The dealer name is required.'); frm.dealername.focus(); return false;}
		
		frm.submit();
	}
	</script>
	
	<div align="center">
	<form method="post" action="edit.cfm">
	<cfoutput>
	<input type="hidden" name="dealerid" value="#id#">
	</cfoutput>
	<table  border="1" cellspacing="0" cellpadding="5">
		<tr align="center" bgcolor="ffffcc">
			<td colspan="2" class="header"><b>Edit a Dealer</b></td>
		</tr>
		<cfoutput query="getdealer">
		<tr>
			<td width="154">Dealer Number </td>
			<td><input name="dealernumber" type="text" class="normal" maxlength="4" size="5" value="#getdealer.dealernumber#"></td>
		</tr>
		<tr>
			<td>Dealer Name </td>
			<td><input name="dealername" type="text" class="normal" maxlength="255" style="width:400px" value="#getdealer.dealername#"></td>
		</tr>
		<tr>
			<td>Primary Phone </td>
		    <td><input name="primaryphone" type="text" class="normal" maxlength="50" style="width:400px" value="#getdealer.primaryphone#"></td>
		</tr>
		<tr>
			<td>Secondary Phone </td>
			<td><input name="secondaryphone" type="text" class="normal" maxlength="50" style="width:400px" value="#getdealer.secondaryphone#"></td>
		</tr>
		<tr>
			<td>Account Manager </td>
			<td><input name="acctmanager" type="text" class="normal" maxlength="255" style="width:400px" value="#getdealer.acctmanager#"></td>
		</tr>
		<tr>
			<td>Contact</td>
		    <td><input name="contact" type="text" class="normal" maxlength="255" style="width:400px" value="#getdealer.contact#"></td>
		</tr>
		</cfoutput>
		<tr>
			<td colspan="2">
			<input type="hidden" name="btnEditDealer" value="1">
			<input type="button" value="Update Dealer" onClick="checkform(this.form);">
			<input type="reset" name="Reset" value="Reset Fields">
			</td>
		</tr>
	</table>
	</form>
	<a href="index.cfm" class="normal">Return to Dealer Menu</a>
	</div>

</cfif>
