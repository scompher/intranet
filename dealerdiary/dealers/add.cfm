
<style type="text/css">
	TABLE {font-family:Arial, Helvetica, sans-serif;font-size:12px; background-color:#FFFFFF;}
	.normal {font-family:Arial, Helvetica, sans-serif;font-size:12px}
	.heading {font-family:Arial, Helvetica, sans-serif;font-size:16px}
	.titlebar {font-family: Arial, Helvetica, sans-serif;	font-size: 12px;font-weight: bold;color: #FFFFFF;}
	.small {font-family:Verdana, Arial, Helvetica, sans-serif;font-size:10px}
	a:link {color:#000000; text-decoration: underline;}
	a:visited {text-decoration: underline; color: #000000;}
	a:hover {text-decoration: underline; color: #0000FF;}
	a:active {text-decoration: underline; color: #000000;}
	.box {border: 1px solid #000000;}
	.header {
		font-family: Arial, Helvetica, sans-serif;
		font-size: 12px;
		color: #FFFFFF;
		background-color: #000066;
	}
-->
</style>

<cfif isdefined("form.btnAddDealer")>

	<!--- save info --->
	<cfquery name="savedealer" datasource="#ds#">
		insert into dealerdiary_dealers (dealernumber, dealername, primaryphone, secondaryphone, acctmanager, contact)
		values ('#form.dealernumber#', '#form.dealername#', '#form.primaryphone#', '#form.secondaryphone#', '#form.acctmanager#', '#form.contact#')
	</cfquery>

	<cfif isdefined("form.return")>
		<cflocation url="../getdealer.cfm?dn=#form.dealernumber#">
	<cfelse>
		<cflocation url="index.cfm">
	</cfif>

<cfelse>

	<script language="JavaScript" type="text/JavaScript">
	function checkform(frm) {
		if (frm.dealernumber.value == "") {alert('The dealer number is required.'); frm.dealernumber.focus(); return false;}
		if (frm.dealername.value == "") {alert('The dealer name is required.'); frm.dealername.focus(); return false;}

		frm.submit();
	}
	</script>
	
	<div align="center">
	<form method="post" action="add.cfm">
	<cfif isdefined("url.return")><input type="hidden" name="return" value="yes"></cfif>
	<table  border="1" cellspacing="0" cellpadding="5">
		<tr align="center" bgcolor="ffffcc">
			<td colspan="2" class="header"><b>Add a New Dealer</b></td>
		</tr>
		<tr>
			<td width="154">Dealer Number </td>
			<td><input name="dealernumber" type="text" class="normal" maxlength="4" size="5" <cfif isdefined("url.dn")>value="<cfoutput>#dn#</cfoutput>"</cfif>></td>
		</tr>
		<tr>
			<td>Dealer Name </td>
			<td><input name="dealername" type="text" class="normal" maxlength="255" style="width:400px"></td>
		</tr>
		<tr>
			<td>Primary Phone </td>
		    <td><input name="primaryphone" type="text" class="normal" maxlength="50" style="width:400px"></td>
		</tr>
		<tr>
			<td>Secondary Phone </td>
			<td><input name="secondaryphone" type="text" class="normal" maxlength="50" style="width:400px"></td>
		</tr>
		<tr>
			<td>Account Manager </td>
			<td><input name="acctmanager" type="text" class="normal" maxlength="255" style="width:400px"></td>
		</tr>
		<tr>
			<td>Contact</td>
		    <td><input name="contact" type="text" class="normal" maxlength="255" style="width:400px"></td>
		</tr>
		<tr>
			<td colspan="2">
			<input type="hidden" name="btnAddDealer" value="1">
			<input type="button" value="Add Dealer" onClick="checkform(this.form);">
			<input type="reset" name="Reset" value="Clear Fields">
			</td>
		</tr>
	</table>
	</form>
	<a href="index.cfm" class="normal">Return to Dealer Menu</a>
	</div>

</cfif>
