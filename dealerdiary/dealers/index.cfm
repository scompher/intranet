<style type="text/css">
	TABLE {font-family:Arial, Helvetica, sans-serif;font-size:12px; background-color:#FFFFFF;}
	.normal {font-family:Arial, Helvetica, sans-serif;font-size:12px}
	.heading {font-family:Arial, Helvetica, sans-serif;font-size:16px}
	.titlebar {font-family: Arial, Helvetica, sans-serif;	font-size: 12px;font-weight: bold;color: #FFFFFF;}
	.small {font-family:Verdana, Arial, Helvetica, sans-serif;font-size:10px}
	a:link {color:#000000; text-decoration: underline;}
	a:visited {text-decoration: underline; color: #000000;}
	a:hover {text-decoration: underline; color: #0000FF;}
	a:active {text-decoration: underline; color: #000000;}
	.box {border: 1px solid #000000;}
</style>

<div align="center" class="normal">
<a href="../index.cfm">Return to Dealer Diary Menu</a>
<br>
</div>
<cfset urlstring = "">
<cfif isdefined("searchstring")><cfset urlstring = urlstring & "&searchstring=#searchstring#"></cfif>

<cfquery name="getdealers" datasource="#ds#">
	select * from dealerdiary_dealers
	where active = 1 
	<cfif isdefined("searchstring")>and dealername LIKE '%#searchstring#%'</cfif>
	order by dealername asc
</cfquery>

<div align="center" class="normal">

<!--- search bar --->
<form method="post" action="index.cfm">
<table border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><b>Search By Dealer Name : </b> (or part of name)</td>
    <td><input name="searchstring" type="text" class="normal" size="50" maxlength="100" <cfif isdefined("searchstring")>value="<cfoutput>#searchstring#</cfoutput>"</cfif> ></td>
    <td><input name="Submit" type="submit" class="normal" value="Search"></td>
</tr>
</table>
</form>
<!--- search bar --->
<!--- pagination code --->
<cfset maxrows = "100">
<cfparam name="start" default="1">
<cfset pages = ceiling(getdealers.recordcount / maxrows)>
<table width="725" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center">
	<table border="0" cellspacing="0" cellpadding="5">
	  <tr>
		<td valign="top">Page</td>
		<td valign="top">
		<cfloop from="1" to="#pages#" index="page">
		<cfset s = (maxrows * page) - (maxrows - 1)>
		<cfset currpage = evaluate((start/maxrows) + 1)>
		<cfoutput>
			<a href="index.cfm?start=#s##urlstring#">#page#</a>&nbsp;<!--- <cfif page is not pages> | </cfif> --->
		</cfoutput>
		</cfloop>|&nbsp;<a href="index.cfm">View All</a>
		</td>
	  </tr>
	</table>
	</td>
</tr>
</table>
<!--- pagination code --->
<table width="725"  border="1" cellspacing="0" cellpadding="5">
	<tr bgcolor="dddddd">
		<td width="70" align="center" nowrap><b>Action</b></td>
		<td width="65" align="center" nowrap><b>Dealer # </b></td>
		<td width="590" align="center" nowrap><b>Dealer Name </b></td>
	</tr>
	<cfoutput query="getdealers" startrow="#start#" maxrows="#maxrows#">
	<tr>
		<td align="center">
		<table  border="0" cellspacing="0" cellpadding="5">
        	<tr align="center">
        		<td><a href="edit.cfm?id=#getdealers.dealerid#"><img src="/images/edit.gif" alt="Edit Dealer" width="16" height="16" border="0"></a></td>
        		<td><a onClick="return confirm('Are you sure you wish to delete this dealer?');" href="delete.cfm?id=#getdealers.dealerid#"><img src="/images/delete.gif" alt="Delete Dealer" width="16" height="16" border="0"></a></td>
       		</tr>
        </table>
		</td>
		<td align="center">#getdealers.dealernumber#</td>
		<td><a href="../getdealer.cfm?dn=#getdealers.dealernumber#">#getdealers.dealername#</a></td>
	</tr>
	</cfoutput>
	<tr>
		<td><input name="button" type="submit" class="normal" value="Add New" onClick="document.location='add.cfm'"></td>
		<td colspan="2">&nbsp;</td>
	</tr>
</table>
<br>
<!--- pagination code --->
<cfset maxrows = "100">
<cfparam name="start" default="1">
<cfset pages = ceiling(getdealers.recordcount / maxrows)>
<table width="725" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center">
	<table border="0" cellspacing="0" cellpadding="5">
	  <tr>
		<td valign="top">Page</td>
		<td valign="top">
		<cfloop from="1" to="#pages#" index="page">
		<cfset s = (maxrows * page) - (maxrows - 1)>
		<cfset currpage = evaluate((start/maxrows) + 1)>
		<cfoutput>
			<a href="index.cfm?start=#s##urlstring#">#page#</a>&nbsp;<!--- <cfif page is not pages> | </cfif> --->
		</cfoutput>
		</cfloop>|&nbsp;<a href="index.cfm">View All</a>
		</td>
	  </tr>
	</table>
	</td>
</tr>
</table>
<!--- pagination code --->
<br>
<a href="../index.cfm">Return to Dealer Diary Menu</a>
</div>
