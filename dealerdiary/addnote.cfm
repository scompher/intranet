<cfparam name="form.publicEntry" default="0">

<cfif isDefined("form.btnAddNote")>

	<cfset created = createodbcdatetime(now())>
	
	<!--- save note --->
	<cfquery name="addnote" datasource="#ds#">
		insert into dealerdiary_entries (creatorID, dealernumber, created, content, subject, departmentid, parentid, accountNumber, publicEntry)
		values (#cookie.adminlogin#, '#dn#', #created#, '#form.content#', '#form.subject#', #form.departmentid#, 0, '#form.accountNumber#', #form.publicEntry#)
	</cfquery>
	
<!--- refresh opener window --->
<cfoutput>
<script language="JavaScript">
<!--
self.opener.location="getdealer.cfm?dn=#dn##urlstring#";
self.close();
//-->
</script> 
</cfoutput>

<cfelse>

	<script language="JavaScript" type="text/JavaScript">
	function checkform(frm) {
		if (frm.departmentid.selectedIndex == 0) {alert('Please choose a department.'); frm.departmentid.focus(); return false;}
		if (frm.subject.value == "") {alert('Please enter the subject of the note.'); frm.subject.focus(); return false;}
		if (frm.content.value == "") {alert('Please enter the body of the note.'); frm.content.focus(); return false;}
		
		frm.submit();
	}
	</script>

	<cfquery name="getuser" datasource="#ds#">
		select * from admin_users where adminuserid = #cookie.adminlogin#
	</cfquery>

	<cfquery name="getdepts" datasource="#ds#">
		select Admin_Users_Departments.*
		from Admin_Users_Departments
		order by Admin_Users_Departments.department asc
	</cfquery>

	<body onLoad="document.forms[0].content.focus();">
	<div align="center">
		<form method="post" action="addnote.cfm">
		<cfoutput>
		<input type="hidden" name="urlstring" value="#urlstring#">
		<input type="hidden" name="dn" value="#dn#">
		</cfoutput>
		<table border="0" cellpadding="3">
			<cfoutput>
			<tr>
				<td><b>#getuser.firstname# #getuser.lastname#</b></td>
			</tr>
			</cfoutput>
			<tr>
				<td>
				<b>Department:</b><br>
				<select name="departmentid" class="normal">
					<option value="0"></option>
				<cfloop query="getdepts">
				<cfoutput>
					<option value="#departmentid#">#department#</option>
				</cfoutput>
				</cfloop>
				</select>
				</td>
			</tr>
			<tr>
				<td>
				<b>Account Number:</b> (If applicable)<br>
				<input type="text" name="accountNumber" style="width:90px" maxlength="12">
				</td>
			</tr>
			<tr>
				<td>
				<b>Subject:</b><br>
				<input type="text" name="subject" style="width:500px" maxlength="255"></td>
			</tr>
			<tr>
				<td>
				<b>Body:</b><br>
				<textarea name="content" style="width:500px" rows="7" class="normal"></textarea>
				</td>
			</tr>
			<tr>
				<td valign="middle">
				<input type="checkbox" name="publicEntry" value="1" style="vertical-align:middle">
				<b>Make this note publicly viewable on COP-A-LINK</b><br>
				(note: public notes must be approved before showing up on COP-A-LINK)
				</td>
			</tr>
			<tr>
				<td>
				<input type="hidden" name="btnAddNote" value="1">
				<input onClick="checkform(this.form);" type="button" value="Add Note">
				<input onClick="self.close();" type="button" value="Close">
				</td>
			</tr>
		</table>
		</form>
	</div>
	</body>

</cfif>
