<cfsetting showdebugoutput="no">

<cfquery name="getnotes" datasource="#ds#">
	select dealerdiary_entries.*, admin_users.firstname, admin_users.lastname
	from dealerdiary_entries
	inner join admin_users on dealerdiary_entries.creatorid = admin_users.adminuserid
	where dealerdiary_entries.entryid IN (#ids#)
	order by entryid 
</cfquery>

<cfquery name="getdealername" datasource="#ds#">
	select * from dealerdiary_dealers
	where dealernumber = '#getnotes.dealernumber#'
</cfquery>

<body onLoad="window.print();">
<div align="center" class="normal">
<table width="590" border="0" cellspacing="0" cellpadding="2">
	<cfoutput>
	<tr>
		<td><b style="font-size:14px">Note Printout for Dealer #getnotes.dealernumber#, #getdealername.dealername#</b><hr width="100%" size="1" class="normal"></td>
	</tr>
	</cfoutput>
	<cfloop list="#ids#" index="id">
		<cfquery name="getnote" dbtype="query">
			select * from getnotes
			where entryid = #id#
		</cfquery>
		<cfoutput query="getnote">
		<tr>
			<td><b>#getnote.firstname# #getnote.lastname# : #dateformat(getnote.created,'mm/dd/yyyy')# #timeformat(getnote.created,'hh:mm tt')#</b></td>
		</tr>
		<tr>
			<td><b>#getnote.subject#</b></td>
		</tr>
		<tr>
			<td>#replace(getnote.content, "#chr(13)#", "<br>", "all")#</td>
		</tr>
		<tr>
			<td><hr width="100%" size="1" class="normal"></td>
		</tr>
		</cfoutput>
	</cfloop>
</table>
</div>
</body>
