<style type="text/css">
<!--
.header {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #FFFFFF;
	background-color: #000066;
}
-->
</style>

<cfif isdefined("form.btnsendnotifications")>

	<cfquery name="getdealerinfo" datasource="#ds#">
		select * from dealerdiary_dealers
		where dealernumber = '#form.dn#'
	</cfquery>
	<cfquery name="getuserinfo" datasource="#ds#">
		select * from admin_users
		where adminuserid = #cookie.adminlogin#
	</cfquery>

<cfset emails = right(form.emaillist,len(form.emaillist) - 1)>

<cfloop index="email" list="#emails#">
<cfmail from="pgregory@copsmonitoring.com" to="#email#" subject="Dealer Diary Notification" username="copalink@copsmonitoring.com" password="copsmoncal">
You are receiving this email because #getuserinfo.firstname# #getuserinfo.lastname# chose to notify you to look at the dealer diary for the following dealer:

Dealer number : #form.dn#
Delaer Name : #getdealerinfo.dealername#

#form.message#

You can view the details by following this link:

#request.appurl#dealerdiary/getdealer.cfm?dn=#form.dn#

</cfmail>
</cfloop>

	<br>
	<div align="center">
	<form method="post" action="notifyacctmanager.cfm">
	<table border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td><b>Notifications have been sent.</b></td>
		</tr>
		<tr>
			<td align="center">&nbsp;</td>
		</tr>
		<tr>
			<td align="center"><input type="button" onClick="self.close();" value="Close Window"></td>
		</tr>
	</table>
	</form>
	</div>
	
<cfelse>
	
	<cfquery name="getusers" datasource="#ds#">
		select * from admin_users
		order by lastname asc
	</cfquery>
	
	<script language="JavaScript" type="text/JavaScript">
	function assignusers(theItem, theDestination) {
		var txt = theItem.options[theItem.selectedIndex].text;
		var item = theItem.options[theItem.selectedIndex].value;
		theDestination[theDestination.length] = new Option(txt, item)
	}
	
	function popItem(theItem, inArray, selIndx) {
		var currList = "";
		for (var i = 0;i < inArray.length; i++)
		{
			theItem[i] = new Option(inArray[i]);
			currList = currList + '-' + inArray[i].slice(0,inArray[i].indexOf('.'));
		}
		if (selIndx == theItem.length) theItem.selectedIndex = selIndx - 1; else theItem.selectedIndex = selIndx;
	}
	
	function removeItem(theItem) {
		var tempArray = new Array();
		var currIndex = theItem.selectedIndex;
		var count = 0;
		for (var i=0;i<theItem.length;i++) {
			if (i != theItem.selectedIndex) {
				tempArray[count] = theItem.options[i].text;
				count += 1;
			}
		}
		theItem.length = 0;
		popItem(theItem,tempArray,currIndex);
	}
	
	function getitems(frm, theItem) {
		if (frm.message.value == "") {alert('You must fill out the message you want to send to the people being notified.'); return false;}
		for (var i=0;i < theItem.length; i++) {
			frm.emaillist.value = frm.emaillist.value + "," + theItem.options[i].value;
		}
		frm.submit();
	}
	</script>
	
	<div align="center">
	<form method="post" action="notifyacctmanager.cfm">
	<input type="hidden" name="emaillist" value="">
	<input type="hidden" name="btnsendnotifications" value="1">
	<cfoutput>
	<input type="hidden" name="dn" value="#dn#">
	</cfoutput>
	<table border="1" cellspacing="0" cellpadding="5">
		<tr bgcolor="FFFFCC">
			<td colspan="2" class="header"><b>Account Manager to Notify : Bart Weiner</b></td>
		</tr>
		<tr>
			<td><b>Select others to notify and click &quot;add&quot;: </b></td>
			<td><b>Additionally Notified:</b></td>
		</tr>
		<tr valign="top">
			<td>
			<select name="email" size="10">
			<cfoutput query="getusers">
				<option value="#getusers.email#">#getusers.lastname#, #getusers.firstname#</option>
			</cfoutput>
			</select>
			</td>
			<td>
			<select name="cc" size="10" style="width:250px">
			</select>
			</td>
		</tr>
		<tr>
			<td><input type="button" onClick="assignusers(this.form.email, this.form.cc);" value="Add"></td>
			<td><input type="button" onClick="removeItem(this.form.cc);" value="Remove"></td>
		</tr>
		<tr>
			<td colspan="2"><b>Message to recipients:<br>
				<textarea rows="5" name="message" style="width:490px " class="normal"></textarea>
			</b></td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2"><input type="button" onClick="getitems(this.form, this.form.cc);" value="Send Notifications"></td>
		</tr>
	</table>
	</form>
	</div>
	
</cfif>

