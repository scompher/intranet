
<cfparam name="url.qaid" default="0">
<cfparam name="url.viewflag" default="0">
<cfparam name="form.saveMeetingScheduled" default="0">
<cfparam name="form.qa_programsChecked" default="">
<cfparam name="showPrintIcon" default="false">
<cfparam name="url.action" default="">
<cfparam name="form.qaid" default="0">
<cfparam name="form.attachmentID" default="">
<cfparam name="form.qaComplete" default="0">
<cfparam name="form.qaRejected" default="0">

<cfinclude template="functions.cfm">

<cfif url.qaid is 0>
	<cfset loadData = false>
<cfelse>
	<cfset loadData = true>
	<cfset form.qaid = url.qaid>
</cfif>

<!--- validate errors here --->
<cfinclude template="validate.cfm">

<cfif errorFlag is 0>
	<cfinclude template="UpdateData.cfm">
</cfif>

<!--- Add an attachment --->
<cfif isDefined("form.btnAddFile")>
	<cfif #form.qaid# is 0>
		<cfset dateTimeStarted = createodbcdatetime(now())>
		<cfdump var="dateTimeStarted">
		<cfquery name="createAttID" datasource="#ds#">
			INSERT into QualityAssuranceChecklist (prog_dateTimeStarted) 
			VALUES (#dateTimeStarted#) 
		</cfquery>
		<!--- get new QAID and save as form.qaid --->
		<cfquery name="getQAID" datasource="#ds#">
			select max(qaid) as newid from QualityAssuranceChecklist 
			where prog_dateTimeStarted = #dateTimeStarted# 
		</cfquery>
		<cfset form.qaid = getQAID.newid>
	</cfif>

	<cfif trim(form.attachment) is not "">
		<cffile action="upload" filefield="attachment" destination="#request.Directpath#\qualityAssurance\attachments\" nameconflict="makeunique">
		<cfset savedFile = cffile.serverFile>
		<!--- Add file to DB --->
		<cfquery name = "InsertFileDb" datasource = "#ds#">
			INSERT INTO QualityAssuranceChecklist_attachments (fileName, qaid)
			VALUES ('#savedFile#',
				'#form.qaid#') 
		</cfquery>
		<cflocation url="qaForm.cfm?qaid=#form.qaid#">
	</cfif>
</cfif>

<!--- Delete File from DB --->
<cfif url.action is "delete">
	<!--- Query to get file info from DB for deletion--->	
	<cfquery name="GetAttachmentDel" datasource="#ds#" >
		SELECT QualityAssuranceChecklist_attachments.fileName
		FROM   QualityAssuranceChecklist_attachments 
		WHERE QualityAssuranceChecklist_attachments.attachmentID = #url.attachmentID#
	</cfquery>
	
	<cfif FileExists("#request.Directpath#\qualityAssurance\attachments\#GetAttachmentDel.filename#")>
		<cffile action="delete" file = "#request.Directpath#\qualityAssurance\attachments\#GetAttachmentDel.filename#">
	</cfif>
	<cfquery name = "DeleteAttachment" datasource = "#ds#">
		DELETE FROM QualityAssuranceChecklist_attachments 
		WHERE qaid = #url.qaid# and
		attachmentID = #url.attachmentID#
	</cfquery>
	<cfset form.qaid = url.qaid>
</cfif>

<!--- Query to get file info from DB --->	
<cfquery name="GetQAAttachment" datasource="#ds#" >
	SELECT QualityAssuranceChecklist_attachments.*
	FROM   QualityAssuranceChecklist_attachments 
	WHERE QualityAssuranceChecklist_attachments.qaid = #form.qaid#
</cfquery>

<cfif loadData is false>
	<cfparam name="form.qaid" default="0">
	<!--- default values to empty for new items --->
	<cfparam name="form.prog_dateStarted" default="">
	<cfparam name="form.prog_purpose" default="">
	<cfparam name="form.prog_inquiryNumber" default="">
	<cfparam name="form.prog_projectNumber" default="">
	<cfparam name="form.prog_description" default="">
	<cfparam name="form.prog_requestor" default="">
	<cfparam name="form.prog_programmer" default="">
	<cfparam name="form.prog_program" default="">
	<cfparam name="form.prog_programsModified" default="">
	<cfparam name="form.prog_commentsAdded" default="">
	<cfparam name="form.prog_compiled" default="">
	<cfparam name="form.prog_debugRemoved" default="">
	<cfparam name="form.prog_cataloged" default="">
	<cfparam name="form.prog_programmerSignOffID" default="">
	<cfparam name="form.programmingComplete" default="0">
	
	<!---  QA Section form variables--->
	<cfparam name="form.qa_programsChecked" default="">
	<cfparam name="form.qa_signOffID" default="">
	<cfparam name="form.qaComplete" default="">
	<cfparam name="form.qaRejected" default="0">
	<cfparam name="form.qaRejectedDescription" default="">

	<!---  Rollout Section form variables--->
	<cfparam name="form.ro_meetingScheduled" default="">
	<cfparam name="form.ro_meetingDateTime" default="">
	<cfparam name="form.rolloutComplete" default="0">
	
	<!---  Production Section form variables--->
	<cfparam name="form.prod_cataloged" default="">
	<cfparam name="form.prod_pvUpdated" default="">
	<cfparam name="form.prod_deptNotified" default="">
	<cfparam name="form.prod_makeLiveDate" default="">
	<cfparam name="form.prod_completedDate" default="">
	<cfparam name="form.productionComplete" default="0">
	<cfparam name="form.qaComplete" default="0">
	<cfparam name="form.rolloutComplete" default="0">
	<cfparam name="form.programmingComplete" default="0">
<cfelse>
	<!--- do query to get current values --->
	<cfquery name="getQAFormInfo" datasource="#ds#">
		SELECT * from QualityAssuranceChecklist
		WHERE qaid = #form.qaid#
	</cfquery>
	<cfif getQAFormInfo.recordcount is 0>
		<div align="center" class="alert"><b>This item is unavailable</b></div>
		<cfabort>
	</cfif>
	<!--- default values to current value for existing items --->
	<cfset form.qaid = getQAFormInfo.qaid>
	<cfset form.prog_dateStarted = dateformat(getQAFormInfo.prog_dateTimeStarted,'mm/dd/yyyy')>
	<cfset form.prog_purpose = getQAFormInfo.prog_purpose>
	<cfset form.prog_inquiryNumber = getQAFormInfo.prog_inquiryNumber>
	<cfset form.prog_projectNumber = getQAFormInfo.prog_projectNumber>
	<cfset form.prog_description = getQAFormInfo.prog_description>
	<cfset form.prog_requestor = getQAFormInfo.prog_requestor>
	<cfset form.prog_programmer = getQAFormInfo.prog_programmer>
	<cfset form.prog_program = getQAFormInfo.prog_program>
	<cfset form.prog_programsModified = getQAFormInfo.prog_programsModified>
	<cfset form.prog_commentsAdded = getQAFormInfo.prog_commentsAdded>
	<cfset form.prog_compiled = getQAFormInfo.prog_compiled>
	<cfset form.prog_debugRemoved = getQAFormInfo.prog_debugRemoved>
	<cfset form.prog_cataloged = getQAFormInfo.prog_cataloged>
	<cfset form.prog_programmerSignOffID = getQAFormInfo.prog_programmerSignOffID>
	<cfset form.programmingComplete = getQAFormInfo.programmingComplete>
	
	<!---  QA Section form variables--->
	<cfset form.qa_programsChecked = getQAFormInfo.qa_programsChecked>
	<cfset form.qa_date = getQAFormInfo.qa_date>
	<cfset form.qa_signOffID = getQAFormInfo.qa_signOffID>
	<cfset form.qaRejected = getQAFormInfo.qaRejected>
	<cfset form.qaRejectedDescription = getQAFormInfo.qaRejectedDescription>
	
	<!---  Rollout Section form variables--->
	<cfset form.ro_meetingScheduled = getQAFormInfo.ro_meetingScheduled>
	<cfset form.ro_meetingDateTime = getQAFormInfo.ro_meetingDateTime>
	<cfset form.ro_meetingDate = #dateformat(getQAFormInfo.ro_meetingDateTime,'mm/dd/yyyy')#>
	<cfset form.ro_meetingTimeHH = #timeformat(getQAFormInfo.ro_meetingDateTime,'hh')#>
	<cfset form.ro_meetingTimeMM = #timeformat(getQAFormInfo.ro_meetingDateTime,'mm')#>
	<cfset form.ro_meetingTimeTT = #timeformat(getQAFormInfo.ro_meetingDateTime,'tt')#>
	<cfset form.rolloutComplete = getQAFormInfo.rolloutComplete>
	
	<!---  Production Section form variables--->
	<cfset form.prod_cataloged = getQAFormInfo.prod_cataloged>
	<cfset form.prod_pvUpdated = getQAFormInfo.prod_pvUpdated>
	<cfset form.prod_deptNotified = getQAFormInfo.prod_deptNotified>
	<cfset form.prod_makeLiveDate = getQAFormInfo.prod_makeLiveDate>
	<cfset form.prod_completedDate = getQAFormInfo.prod_completedDate>
	<cfset form.qaComplete = getQAFormInfo.qaComplete>
	<cfset form.rolloutComplete = getQAFormInfo.rolloutComplete>
	<cfset form.productionComplete = getQAFormInfo.productionComplete>
	
</cfif>

<link rel="stylesheet" type="text/css" href="../styles.css">

<br />

<!---  IF VIEW ONLY, SET COMPLETED FLAGS TO DONE --->	
<cfif url.viewflag is 'y'>
	<cfset form.qaComplete = 1>
	<cfset form.rolloutComplete = 1>
	<cfset form.productionComplete = 1>
	<cfset showPrintIcon = true>
</cfif>

<div align="center" class="normal">
<cfif showPrintIcon is true>
<table width="675" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td>
		<a href="javascript:window.print();"><img src="/images/printerfriendlyicon.gif" alt="Print This Page" border="0" align="absmiddle" />&nbsp;<b>Print this page</b></a>
		</td>
    </tr>
</table>
</cfif>

<!--- Upload File --->
<cfform action="qaForm.cfm" method="post" enctype="multipart/form-data">
	<cfinput type="hidden" name="qaid" value="#form.qaid#" />
	<table width="675" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>COPS Quality Assurance Checklist - New Incident</b></td>
	</tr>
	<tr>	
		<td class="greyrowbottomnopadding">
			<table border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td>
						<b>Add Attachments:</b>&nbsp;
						<cfinput type="file" name="attachment" style="width:300px; background-color:##FFFFFF; vertical-align:middle;">
						<cfinput type="submit" name="btnAddFile" id="Addfile" value="Add File" style="vertical-align:middle;">
					</td>
				</tr>
			</table>
			<cfif GetQAAttachment.recordcount gt 0>
				<table border="0" cellspacing="0" cellpadding="3">
				<tr>	
					<td><b>Current Attachments:</b></td>
				</tr>
				<cfoutput query="GetQAAttachment">
				<tr>
					<td>
					<a target="_blank" href="/qualityAssurance/attachments/#urlencodedformat(GetQAAttachment.filename)#"><img align="absmiddle" src="/images/view.gif" alt="View" width="16" height="16" border=""/>&nbsp;</a>
					<a onclick="return confirm('Are you sure you want to delete this?');" href="qaForm.cfm?qaid=#GetQAAttachment.qaid#&attachmentID=#GetQAAttachment.attachmentID#&action=delete"><img align="absmiddle" src="/images/delete.gif" alt="Delete" width="16" height="16" border="" /></a>&nbsp;#GetQAAttachment.fileName#<br>
					</td>
				</tr>
				</cfoutput>
				</table>
			</cfif>
		</td>
	</tr>
	</table>
</cfform>

<cfform method="post" action="qaForm.cfm" enctype="multipart/form-data">
	<cfinput type="hidden" name="qaid" value="#form.qaid#">
	<cfinput type="hidden" name="viewflag" value="#url.viewflag#">
	<cfinput type="hidden" name="saveMeetingScheduled" value="#form.saveMeetingScheduled#">

	<!---  ONCE SECTION COMPLETED, SHOW AS VIEW ONLY --->
	<cfif form.programmingComplete is 1 and form.qaid is not 0>
		<cfinclude template="section1Display.cfm">
	<cfelse>
		<cfinclude template="section1.cfm">
	</cfif>

	<cfif form.programmingComplete is 1>
		<br />
		<cfif form.qaComplete is 0>
			<cfinclude template="section2.cfm">
		<cfelse>
			<cfinclude template="section2Display.cfm">
		</cfif>
	</cfif>
	
	<cfif form.programmingComplete is 1 and form.qaComplete is 1>
		<br />
		<cfif form.rolloutComplete is 0>
			<cfinclude template="section3.cfm">
		<cfelse>
			<cfinclude template="section3Display.cfm">
		</cfif>
	</cfif>

	<cfif form.programmingComplete is 1 and form.qaComplete is 1 and form.rolloutComplete is 1>
		<br />
		<cfif form.productionComplete is 0>
			<cfinclude template="section4.cfm">
		<cfelse>
			<cfinclude template="section4Display.cfm">
		</cfif>
	</cfif>

</cfform>
<br>
<a style="text-decoration:underline;" href="index.cfm">Return to Checklist Menu</a>
</div>



