<!--- QA Checklist Section4 Production View--->
<link rel="stylesheet" type="text/css" href="../styles.css">

<cfquery name="GetQAForm" datasource="#ds#" >
SELECT * from QualityAssuranceChecklist
WHERE qaid = #form.qaid#
</cfquery>

<table width="675" border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td class="highlightbar"><b>Production View</b></td>
	</tr>
	<tr>	
		<td class="greyrowbottomnopadding">
		<table width="100%" border="0" cellspacing="0" cellpadding="5">
		<cfoutput query="GetQAForm">
			<tr>
				<td colspan="2">
				    <cfif #GetQAForm.prod_cataloged# is 1>
						Cataloged in COPS (If Subroutine)						
					</cfif> 
				</td>
			</tr>
			<tr>
				<td colspan="2"><cfif #GetQAForm.prod_pvUpdated# is 1>
						Program Version (PV) Updated					
					</cfif> 
				</td>
			</tr>
			<tr>
				<td colspan="2"><cfif #GetQAForm.prod_deptNotified# is 1>
						Department Notified
					</cfif> 
				</td>	
			</tr>
			<tr>
			  	<td width="125"><b>Make Live Date: </b></td>
				<td width="528">#DateFormat(GetQAForm.prod_makeLiveDate,'mm/dd/yyyy')# </td>
			</tr>
			<tr>
			    <td><b>Completed Date: </b></td>
				<td width="528">#DateFormat(GetQAForm.prod_completedDate,'mm/dd/yyyy')# </td>
			</tr>
			<tr>
				<td colspan="2">
					<cfif GetQAForm.productionComplete is 1>
					Completed Production
					</cfif>
				</td>
			</tr>
		</cfoutput>
		</table>
		</td>
	</tr>
</table>



