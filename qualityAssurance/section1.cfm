<link rel="stylesheet" type="text/css" href="../styles.css">

<cfquery name="getEmployees" datasource="#ds#">
	select adminuserid, firstname + ' ' + lastname as employeeName 
	from admin_users 
	where adminuserid = #cookie.adminlogin#
	order by firstname, lastname 
</cfquery>

<cfquery name="getTechEmployees" datasource="#ds#">
	SELECT Admin_Users.firstname + ' ' + Admin_Users.lastname AS employeeName
	FROM Admin_Users_Departments_Lookup 
	LEFT JOIN Admin_Users ON Admin_Users_Departments_Lookup.adminuserid = Admin_Users.adminuserid
	WHERE Admin_Users_Departments_Lookup.departmentid = 1 and Admin_Users.active = 1
	ORDER BY admin_users.firstname asc, admin_users.lastname asc 
</cfquery>

<table width="675" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>COPS Quality Assurance Checklist - New Incident</b></td>
	</tr>
	<tr>	
		<td class="greyrowbottomnopadding">
			<table border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td width="170" nowrap="nowrap"><b>Purpose:</b></td>
					<td width="483">
						<cfselect onChange="this.form.submit();" style="vertical-align:middle;" name="prog_purpose" required="yes" message="Purpose is required.">
						<option <cfif form.prog_purpose is ""> selected </cfif> value=""></option>
						<option <cfif form.prog_purpose is "inquiry"> selected </cfif> value="inquiry">Inquiry</option>
						<option <cfif form.prog_purpose is "project"> selected </cfif> value="project">Project</option>
						<option <cfif form.prog_purpose is "Program Fix (Bug)"> selected </cfif> value="Program Fix (Bug)">Program Fix (Bug)</option>
						<option <cfif form.prog_purpose is "Special Request"> selected </cfif> value="Special Request">Special Request</option>
						</cfselect>
					</td>
				</tr>
				<cfif trim(form.prog_purpose) is "inquiry">
					<td nowrap="nowrap"><b>Inquiry Number:</b></td>
					<td>
						<cfinput type="text" name="prog_inquiryNumber" id="prog_inquiryNumber" value="#form.prog_inquiryNumber#" required="yes" message="Inquiry Number is required and numeric." validate="integer">
					</td>
				</cfif>
				<cfif trim(form.prog_purpose) is "project">
					<td nowrap="nowrap"><b>Project Number:</b></td>
					<td>
						<cfinput type="text" name="prog_projectNumber" id="prog_projectNumber" value="#form.prog_projectNumber#" required="yes" message="Project Number is required.">
					</td>
				</cfif>
				<tr>
					<td colspan="2"><b>Description:</b></td>
				</tr>
				<tr>
					<td colspan="2">
						<cftextarea style="width:650px;" rows="5" name="prog_description" required="yes" message="Description is required."><cfoutput>#form.prog_description#</cfoutput></cftextarea>
					</td>
				</tr>
				<tr>
					<td nowrap="nowrap"><b>Requested By:</b></td>
					<td>
						<cfinput type="text" name="prog_requestor" id="prog_requestor" value="#form.prog_requestor#" required="yes" message="Requested by is required.">
					</td>
				</tr>
				<tr>
					<td nowrap="nowrap"><b>Programmer:</b></td>
					<td>
						<select name="prog_programmer">
							<option value=""></option>
							<cfoutput query="getTechEmployees">
								<option <cfif getTechEmployees.employeeName is form.prog_programmer>selected</cfif> value="#employeeName#">#employeeName#</option>
							</cfoutput>
						</select>
					</td>
				</tr>
				<tr>
					<td nowrap="nowrap"><b>Program:</b></td>
					<td>
						<cfif form.prog_program is "change">
							<cfinput type="radio" checked name="prog_program" value="change" required="yes" message="Program is required."/>
						<cfelse>
							<cfinput type="radio" name="prog_program" value="change" required="yes" message="Program is required."/>
						</cfif>
						<b>Change</b>
						<cfif form.prog_program is "new">
							<cfinput type="radio" checked name="prog_program" value="new" required="yes" message="Program is required."/>
						<cfelse>
							<cfinput type="radio" name="prog_program" value="new" required="yes" message="Program is required."/>
						</cfif>	
						<b>New</b>
					</td>
				</tr>
				<tr>
				<td colspan="2" class="alert"><b>LIST OF PROGRAMS MODIFIED BELOW MUST BE SEPARATED BY A COMMA.</b></td>
				</tr>
				<tr>
					<td colspan="2"><b>Programs Modified:</b></td>
				</tr>
				<tr>
					<td colspan="2">
						<cftextarea style="width:650px;" rows="5" name="prog_programsModified" required="yes" message="Programs Modified is required."><cfoutput>#form.prog_programsModified#</cfoutput></cftextarea>
					</td>
				</tr>
				<tr>
					<td colspan="2" class="nopadding">
						<table border="0" cellspacing="0" cellpadding="5">
							<tr>
								<td>
									<input type="checkbox" <cfif form.prog_commentsAdded is 1>checked</cfif> name="prog_commentsAdded" value="1" />
									Add Version Comments </td>
								<td>
									<input type="checkbox" <cfif form.prog_compiled is 1>checked</cfif> name="prog_compiled" value="1" />
									Compiled </td>
								<td>
									<input type="checkbox" <cfif form.prog_debugRemoved is 1>checked</cfif> name="prog_debugRemoved" value="1" />
									Debug Statements Removed </td>
								<td>
									<input type="checkbox" <cfif form.prog_cataloged is 1>checked</cfif> name="prog_cataloged" value="1" />
									Cataloged (If Subroutine) </td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<b>Programmer Sign Off:</b>
						<cfoutput query="getEmployees">
							#getEmployees.employeeName#
							<cfinput type="HIDDEN" name="prog_programmerSignOffID" value="#cookie.adminlogin#">
						</cfoutput>
					</td>
				</tr>
				<tr>
					<td>
						<input type="checkbox" <cfif form.programmingComplete is 1>checked</cfif> name="programmingComplete" value="1" />
						Complete Programming 
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<cfinput type="submit" name="btnSaveProgramming" id="Saveinfo" value="Save Info">
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

