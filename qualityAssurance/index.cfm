<!--- Quality Assurance Checklist Index Page --->

<link rel="stylesheet" type="text/css" href="../styles.css">
<cfparam name="url.viewflag" default="">

<cfquery name="getTechEmployees" datasource="#ds#">
	SELECT Admin_Users.firstname + ' ' + Admin_Users.lastname AS employeeName
	FROM Admin_Users_Departments_Lookup 
	LEFT JOIN Admin_Users ON Admin_Users_Departments_Lookup.adminuserid = Admin_Users.adminuserid
	WHERE Admin_Users_Departments_Lookup.departmentid = 1 and Admin_Users.active = 1
	ORDER BY admin_users.firstname asc, admin_users.lastname asc 
</cfquery>

</br><div align="center" class="normal" style="position:relative;z-index:0">

<cfparam name="err" default="">

<!--- pagination variables --->
<cfset maxrows = 50>
<cfparam name="p" default="1">
<cfparam name="pages" default="1">
<!--- pagination variables --->

<!--- cal cstart page --->
<cfset start = (maxrows * p) - (maxrows - 1)>
<!--- cal cstart page --->

<!--- convert URL variables to form variables --->
<cfif isDefined("url.searchProgrammer")>
	<cfset form.searchProgrammer = url.searchProgrammer>
</cfif>
<cfif isDefined("url.searchPurpose")>
	<cfset form.searchPurpose = url.searchPurpose>
</cfif>
<cfif isDefined("url.searchDesc")>
	<cfset form.searchDesc = url.searchDesc>
</cfif>
<cfif isDefined("url.searchInquiryNumber")>
	<cfset form.searchInquiryNumber = url.searchInquiryNumber>
</cfif>
<cfif isDefined("url.searchProjectNumber")>
	<cfset form.searchProjectNumber = url.searchProjectNumber>
</cfif>
<cfif isDefined("url.searchOrder")>
	<cfset form.searchOrder = url.searchOrder>
</cfif>
<cfif isDefined("url.searchStartDate")>
	<cfset form.searchStartDate = url.searchStartDate>
</cfif>
<cfif isDefined("url.searchEndDate")>
	<cfset form.searchEndDate = url.searchEndDate>
</cfif>

<!--- if Submit button is entered, display data --->
<!---<cfif isDefined("form.btnSubmit")>
	<cfinclude template = "validateSearch.cfm">
</cfif>--->

<!---if Reset button is entered, erase data --->
<cfif isDefined("form.btnReset")>
	<cflocation url="index.cfm">
</cfif>

<cfinclude template = "searchForm.cfm">

<div align="center">

</br>
<table width="600" border="0" cellpadding="5" cellspacing="0">
		<form method="post" action="qaForm.cfm">
		<tr>
			<td style="padding-left:0px;"><input type="button" onclick="document.location='qaForm.cfm';" value="Create New QA Checklist" /></td>
		</tr>
		</form>
</table>

</div>

<cfif trim(err) is "" and not isDefined("form.btnReset")>
	
	</br>
		
	<cfquery name="getItems" datasource="#ds#">
		select QualityAssuranceChecklist.* 
		from QualityAssuranceChecklist 
		where 
		<cfif form.showPreferences is "incompleteOnly">
			(programmingComplete = 0 or qaComplete = 0 or rolloutComplete = 0 or productionComplete = 0) and 
		</cfif>
		<cfif trim(form.searchStartDate) is not "">
			prog_dateTimeStarted >= #createodbcdate(form.searchStartDate)# and 
		</cfif>
		<cfif trim(form.searchEndDate) is not "">
			<cfset form.searchEndDate = dateadd("d",form.searchEndDate,1)>
			prog_dateTimeStarted < #createodbcdate(form.searchEndDate)# and 
		</cfif>
		<cfif trim(form.searchProgrammer) is not "">
			(prog_programmer LIKE '%#trim(form.searchProgrammer)#%') and 
		</cfif>						
		<cfif trim(form.searchPurpose) is not "">
			(prog_purpose LIKE '%#trim(form.searchPurpose)#%') and 
		</cfif>
		<cfif trim(form.searchDesc) is not "">
			(prog_description LIKE '%#trim(form.searchDesc)#%') and 
		</cfif>
		<cfif trim(form.searchInquiryNumber) is not "">
			prog_inquiryNumber = '#form.searchInquiryNumber#' and 
		</cfif>
		<cfif trim(form.searchProjectNumber) is not "">
			prog_projectNumber = '#form.searchProjectNumber#' and 
		</cfif>
		<cfif trim(form.searchPrograms) is not "">
			<cfset counter = 0>
			(
			<cfloop list="#form.searchPrograms#" index="program">
			<cfset counter = counter + 1>
			prog_programsModified like '%#trim(program)#%' <cfif counter lt listlen(form.searchPrograms)>or</cfif> 
			</cfloop>
			) and 
		</cfif>
		active=1 and 
		1=1 
		Order by prog_dateTimeStarted #form.searchOrder# 
	</cfquery>
	
	<!--- calculate number of pages --->
	<cfif isnumeric(getItems.recordcount)>
		<cfset totalItems = getItems.recordcount>
	<cfelse>
		<cfset totalItems = 0>
	</cfif>
	<cfset pages = ceiling(totalItems / maxrows)>
	
	<p></p>
	<cfif (#getItems.recordcount#) gt 1>
	<h4>
	<cfoutput>
	#numberformat(getItems.recordcount)# Records Found.	</cfoutput>	</h4>
	<cfelse>
	<h4>
	<cfoutput>
	#getItems.recordcount# Record Found.	</cfoutput>	</h4>
	</cfif>

	<p></p>
	<!--- 
	<cfif getItems.recordcount gt 0>
		<cfset end = start + maxrows - 1>
		<cfif end gt getItems.recordcount>
			<cfset end = getItems.recordcount>
		</cfif>
		<cfoutput>
		Displaying items #start# - #end#
		</cfoutput>
		<p></p>
	</cfif>
	 --->
	
	<!--- convert search parameters into url variables --->
	<cfset searchString = "searchProgrammer=#form.searchProgrammer#&searchPurpose=#form.searchPurpose#&searchKeyword=#form.searchKeyword#&searchDesc=#form.searchDesc#&searchInquiryNumber=#form.searchInquiryNumber#&searchProjectNumber=#form.searchProjectNumber#">
	<cfif trim(form.searchStartDate) is not "">
		<cfset searchString = searchString & "&searchStartDate=#trim(dateformat(form.searchStartDate,'mm/dd/yyyy'))#">
		<!--- <cfset searchString = "searchProgrammer=#form.searchProgrammer#&searchPurpose=#form.searchPurpose#&searchKeyword=#form.searchKeyword#&searchDesc=#form.searchDesc#&searchInquiryNumber=#form.searchInquiryNumber#&searchProjectNumber=#form.searchProjectNumber#&searchStartDate=#trim(dateformat(form.searchStartDate,'mm/dd/yyyy'))#&searchEndDate=#trim(dateformat(dateadd("d",form.searchEndDate,-1),'mm/dd/yyyy'))#"> --->
	</cfif>
	<cfif trim(form.searchEndDate) is not "">
		<cfset searchString = searchString & "&searchEndDate=#trim(dateformat(dateadd("d",form.searchEndDate,-1),'mm/dd/yyyy'))#">
	</cfif>
	
	<!--- Display Checklist --->
	</br>
	<table width="80%" border="0" cellpadding="5" cellspacing="0">
		<tr>
			<td class="highlightbar"><b>COPS Quality Assurance Checklist Menu</b></td>
		</tr>
		<tr>
		<td class="greyrowbottomnopadding">
		<table width="100%" border="0" cellpadding="5" cellspacing="0">
			<tr>
				<td nowrap="nowrap" class="linedrowrightcolumn" align="center" width="10%">&nbsp;</td>
				<td nowrap="nowrap" class="linedrowrightcolumn" align="center" width="10%">
				<b>Completed</b><br />
				<table border="0" cellspacing="0" cellpadding="3">
					<tr>
						<td width="16" align="center"><b>P</b></td>
						<td width="16" align="center"><b>Q</b></td>
						<td width="16" align="center"><b>R</b></td>
						<td width="16" align="center"><b>PR</b></td>
					</tr>
				</table>
				</td>
				<td width="8%" align="center" valign="bottom" nowrap="nowrap" class="linedrowrightcolumn"><b>Action				</b></td>							
				<td align="center" valign="bottom" nowrap="nowrap" class="linedrowrightcolumn"><b>Date/Time Created </b></td>
				<td valign="bottom" nowrap="nowrap" class="linedrowrightcolumn"><b>Purpose </b></td>
				<td valign="bottom" nowrap="nowrap" class="linedrowrightcolumn"><b>Programmer </b></td>
				<td valign="bottom" class="linedrow"><b>Description </b></td>
			</tr>
			<!--- <cfoutput query="getItems" startrow="#start#" maxrows="#maxrows#"> --->
			<cfoutput query="getItems">
			<tr>
				<td width="10" align="center" nowrap="nowrap" class="linedrowrightcolumn">#getitems.currentrow#.</td>
				<td align="center" nowrap="nowrap" class="linedrowrightcolumn">
				<table border="0" cellspacing="0" cellpadding="3">
					<tr>
						<td align="center" width="16">
						<cfif getItems.programmingcomplete is 1>
							<img src="/images/completed.gif" alt="Completed" width="16" height="16" border="" />
						<cfelse>
							&nbsp;
						</cfif>
						</td>
						<td align="center" width="16">
						<cfif getItems.qaComplete is 1>
							<img src="/images/completed.gif" alt="Completed" width="16" height="16" border="" />
						<cfelse>
							&nbsp;
						</cfif>
						</td>
						<td align="center" width="16">
						<cfif getItems.rolloutComplete is 1>
							<img src="/images/completed.gif" alt="Completed" width="16" height="16" border="" />
						<cfelse>
							&nbsp;
						</cfif>
						</td>
						<td align="center" width="16">
						<cfif getItems.productionComplete is 1>
							<img src="/images/completed.gif" alt="Completed" width="16" height="16" border="" />
						<cfelse>
							&nbsp;
						</cfif>
						</td>
					</tr>
				</table>
				</td>
				<td align="center" nowrap="nowrap" class="linedrowrightcolumn">
				<a href="qaForm.cfm?qaid=#getItems.qaID#&viewflag=n"><img src="/images/edit.gif" alt="Edit" width="16" height="16" border="" /></a>
				&nbsp;
				<a href="qaForm.cfm?qaid=#getItems.qaid#&viewflag=y"><img src="/images/view.gif" alt="View" width="16" height="16" border=""/></a>
				</td>
				<td align="center" nowrap="nowrap" class="linedrowrightcolumn">#dateformat(getItems.prog_dateTimeStarted,'mm/dd/yyyy')# #timeformat(getItems.prog_dateTimeStarted,'hh:mm tt')#</td>
				<td nowrap="nowrap" class="linedrowrightcolumn">#getItems.prog_purpose#
					<cfif getItems.prog_purpose is "inquiry">
						#getItems.prog_inquiryNumber#
					</cfif>
					<cfif getItems.prog_purpose is "project">
						#getItems.prog_projectNumber#
					</cfif>
				</td>
				<td nowrap="nowrap" class="linedrowrightcolumn">#getItems.prog_programmer#</td>
				<td class="linedrow">#left(getItems.prog_description,100)#</td>
			</tr>
			</cfoutput>
		</table>
		</td>
		</tr>
	</table>
</cfif>

<br />
	<a style="text-decoration:underline;" href="/index.cfm">Return to Intranet</a>
<br />
<br />

</div>
