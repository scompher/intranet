<!--- QA Checklist Section4 Production --->
<link rel="stylesheet" type="text/css" href="../styles.css">
<table width="675" border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td class="highlightbar"><b>Production</b></td>
	</tr>
	<tr>	
		<td class="greyrowbottomnopadding">
		<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr>
			<td width="42%">
				<input type="checkbox" <cfif form.prod_cataloged is 1>checked</cfif> name="prod_cataloged" value="1" />
				<b>Cataloged in COPS (If Subroutine): </b></td>
			<td width="58%">
				<input type="checkbox" <cfif form.prod_pvUpdated is 1>checked</cfif> name="prod_pvUpdated" value="1" />
				 <b>Program Version (PV) Updated: </b></td>
			</tr>
			<tr>
			 <td>
				<input type="checkbox" <cfif form.prod_deptNotified is 1>checked</cfif> name="prod_deptNotified" value="1" />
			   <b>Department Notified: </b></td> 
			 <td>&nbsp;</td>
			</tr>
			<tr>
			 <td><b>Make Live Date: </b></td>
			  <td><cfinput type="datefield" name="prod_makeLiveDate" style="width:75px" value="#dateformat(form.prod_makeLiveDate,'mm/dd/yyyy')#" required="yes" message="Make Live Date is required."></td>
			</tr>
			<tr>
			    <td><b>Completed Date: </b></td>
				  <td><cfinput type="datefield" name="prod_completedDate" style="width:75px" value="#dateformat(form.prod_completedDate,'mm/dd/yyyy')#" required="yes" message="Completed Date is required."></td>
			    </tr>
			<tr>
			<td>
				<input type="checkbox" <cfif form.productionComplete is 1>checked</cfif> name="productionComplete" value="1" />
				<b>Production Completed: </b></td>
			</tr>
			<tr>
				<td colspan="2">
					<cfinput type="submit" name="btnSaveProduction" id="Saveinfo" value="Save Info">
				</td>
			</tr>
		</table>
		</td>
	</tr>
</table>


