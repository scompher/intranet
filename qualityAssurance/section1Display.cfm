<link rel="stylesheet" type="text/css" href="../styles.css"><table width="600" border="0" cellpadding="5" cellspacing="0">

<cfquery name="GetQAForm" datasource="#ds#" >
SELECT * from QualityAssuranceChecklist
WHERE qaid = #form.qaid#
</cfquery>

<cfquery name="getEmployees" datasource="#ds#">
	select adminuserid, firstname + ' ' + lastname as employeeName 
	from admin_users 
	where adminuserid = #GetQAForm.prog_programmerSignOffID#
	order by firstname, lastname 
</cfquery>

<table width="675" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>COPS Quality Assurance Checklist - View</b></td>
	</tr>
	<tr>	
		<td class="greyrowbottomnopadding">
			<table border="0" cellspacing="0" cellpadding="5">
				<cfoutput query="GetQAForm">
				<tr>
					<td width="107" nowrap="nowrap"><b>Date Started:</b></td>
					<td width="545">#DateFormat(GetQAForm.prog_dateTimeStarted,'mm/dd/yyyy')# #TimeFormat(GetQAForm.prog_dateTimeStarted,'hh:mm tt')#</td>
					</td>
				</tr>
				<tr>
					<td nowrap="nowrap"><b>Purpose:</b></td>
					<td>#GetQAForm.prog_purpose#</td>
				</tr>
				<cfif trim(GetQAForm.prog_purpose) is "inquiry">
					<td nowrap="nowrap"><b>Inquiry Number:</b></td>
					<td>#GetQAForm.prog_inquiryNumber#&nbsp;<a href="http://192.168.107.10/inquiry/findinquiry.cfm?i=#GetQAForm.prog_inquiryNumber#" target="_blank" style="text-decoration:underline">[View Inquiry]</a></td>	
				</cfif>
				<cfif trim(GetQAForm.prog_purpose) is "project">
					<td nowrap="nowrap"><b>Project Number:</b></td>
					<td>#GetQAForm.prog_projectNumber#</td>
				</cfif>
				<tr>
					<td colspan="2"><b>Description:</b></td>
				</tr>
				<tr>
					<td colspan="2">#GetQAForm.prog_description#</td>
				</tr>
				<tr>
					<td nowrap="nowrap"><b>Requested By:</b></td>
					<td>#GetQAForm.prog_requestor#</td>
				</tr>
				<tr>
					<td nowrap="nowrap"><b>Programmer:</b></td>
					<td>#GetQAForm.prog_programmer#</td>
				</tr>
				<tr>
					<td nowrap="nowrap"><b>Program:</b></td>
					<td>#GetQAForm.prog_program#</td>
				</tr>
				<tr>
					<td colspan="2"><b>Programs Modified:</b></td>
				</tr>
				<tr>
					<td colspan="2">#replace(GetQAForm.prog_programsModified,",","<br />","all")#</td>
				</tr>
				<tr>
					<td colspan="2" class="nopadding">
						<table border="0" cellspacing="0" cellpadding="5">
							<tr>
								<td><cfif GetQAForm.prog_commentsAdded is 1>
									Add Version Comments
									</cfif> </td>
								<td><cfif GetQAForm.prog_compiled is 1>
									Compiled 
									</cfif></td>
								<td><cfif GetQAForm.prog_debugRemoved is 1>
									Debug Statements Removed 
									</cfif></td>
								<td><cfif GetQAForm.prog_cataloged is 1>
									Cataloged (If Subroutine) 
									</cfif></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<cfif GetQAForm.programmingComplete is 1>
						Completed Programming 
						</cfif>
					</td>
				</tr>
				</cfoutput>
				<tr>
					<td colspan="2">
						<b>Programmer Sign Off:</b>
						<cfoutput query="getEmployees">
							<option <cfif getEmployees.employeeName is GetQAForm.prog_programmerSignOffID>selected</cfif> value="#employeeName#">#employeeName#</option>
						</cfoutput>
					</td>
				</tr>
				
				<tr>
				</tr>
			</table>
		</td>
	</tr>
</table>

