<!--- QA Checklist Search Form page --->
<link rel="stylesheet" type="text/css" href="/styles.css">

<cfparam name = "form.searchProgrammer" default="">
<cfparam name = "form.searchKeyword" default="">
<cfparam name = "form.searchPurpose" default="">
<cfparam name = "form.searchDesc" default="">
<cfparam name = "form.searchInquiryNumber" default="">
<cfparam name = "form.searchProjectNumber" default="">
<cfparam name = "form.searchOrder" default="DESC">
<cfparam name = "form.searchStartDate" default="">
<cfparam name = "form.searchEndDate" default="">
<cfparam name = "form.showPreferences" default="incompleteOnly">
<cfparam name = "form.searchPrograms" default="">

<div align="center" class="normal" style="position:relative;z-index:0">
<cfform method="post" action="index.cfm">
	<table width="600" border="0" cellpadding="5" cellspacing="0">
        <tr>
            <td class="highlightbar"><b>Search By: </b></td>
        </tr>
        <tr>
            <td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td colspan="2">
					<b>Enter Date Range, Programmer, Purpose, Description, Project or Inquiry Number or any combination.<br />
					The default is all incompleted checklists. </b>					</td>
				</tr>
				<tr>
                    <td colspan="2"><b>Date Range: </b>&nbsp;Please enter in the format mm/dd/yyyy or use the calendar</td>
                </tr>
                <tr>
                    <td width="121"><b>Begin Date: </b></td>
					<td width="529" style="z-index:3; position:relative;">
					<cfinput type="datefield" name="searchStartDate" value="#form.searchStartDate#" validate="date" message="Please enter a valid beginning date" style="width:75px;">
					</td>
                </tr>
                <tr>
                    <td><b>End Date: </b></td>
					<td style="z-index:2; position:relative;">
					<cfinput type="datefield" name="searchEndDate" value="#form.searchEndDate#" validate="date" message="Please enter a valid ending date" style="width:75px;">
					</td>
                </tr>
                <tr>
                    <td nowrap="nowrap"><b>Purpose: </b></td>
					<td>
					<cfinput type="text" name="searchPurpose" value="#form.searchPurpose#" style="width:200px">
					</td>
                </tr>
                <tr>
                    <td nowrap="nowrap"><b>Programmer:</b></td>
                    <td><cfinput type="text" name="searchProgrammer"  value="#form.searchProgrammer#" style="width:200px"></td>
                </tr>
				<tr>
                	<td>&nbsp;</td>
                	<td class="smallred">Enter entire name or part of the name to search</td>
                </tr>
                <tr>
                	<td><b>Program(s):</b></td>
                	<td>
                		<cfinput type="text" name="searchPrograms" value="#form.searchPrograms#" style="width:250px">
                	</td>
                </tr>
				<tr>
                	<td>&nbsp;</td>
                	<td class="smallred">Separate multiple programs with a comma</td>
                </tr>
                <tr>
                	<td><b>Description:</b></td>
                	<td>
                		<cfinput type="text" name="searchDesc" value="#form.searchDesc#" style="width:250px">
                	</td>
                </tr>
				<tr>
				<td><b>Project Number:</b></td>
                	<td>
                		<cfinput type="text" name="searchProjectNumber" value="#form.searchProjectNumber#" style="width:50px">
                	</td>
				</tr>
				<tr>
				<td><b>Inquiry Number:</b></td>
                	<td>
                		<cfinput type="text" name="searchInquiryNumber" value="#form.searchInquiryNumber#" style="width:50px">
                	</td>
				</tr>
                <tr>
                	<td><b>List Results:</b> </td>
                	<td>
                		<select name="searchOrder">
							<option <cfif form.searchOrder is "DESC">selected</cfif> value="DESC">Newest First</option>
                			<option <cfif form.searchOrder is "ASC">selected</cfif> value="ASC">Oldest First</option>
               			</select>
               		</td>
                </tr>
				<tr>
					<td>&nbsp;</td>
					<td>
					<select name="showPreferences">
						<option <cfif showPreferences is "incompleteOnly">selected</cfif> value="incompleteOnly">Show incomplete items only</option>
						<option <cfif showPreferences is "showAll">selected</cfif> value="showAll">Show all items</option>
					</select>
					</td>
				</tr>
                <tr>
					<td colspan="2">
						<cfinput type="submit" name="btnSubmit" id="Search Now" value="Search Now"> &nbsp;
						<cfinput type="submit" name="btnReset" id="Reset Search" value="Reset Search">&nbsp;
					</td>
                </tr>
            </table>
			</td>
        </tr>
    </table>
	</cfform>
</div>

