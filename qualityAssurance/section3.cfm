<!--- QA Checklist Section3 Rollout --->
<cfparam name="form.ro_meetingDate" default="">
<cfparam name="form.ro_meetingTimeHH" default="">
<cfparam name="form.ro_meetingTimeMM" default="">
<cfparam name="form.ro_meetingTimeTT" default="">

<link rel="stylesheet" type="text/css" href="../styles.css">
<table width="675" border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td class="highlightbar"><b>Rollout</b></td>
	</tr>
	<tr>	
		<td class="greyrowbottomnopadding">
		<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr>	
				<td nowrap="nowrap"><b>Meeting:</b></td>
				<td>
					<cfif form.ro_meetingScheduled is "meetingScheduled">
						<cfinput onClick="this.form.saveMeetingScheduled.value = 1; this.form.submit();" type="radio" checked="yes" name="ro_meetingScheduled" value="meetingScheduled" required="yes" message="Meeting is required."/>
					<cfelse>
						<cfinput onClick="this.form.saveMeetingScheduled.value = 1; this.form.submit();" type="radio" name="ro_meetingScheduled" value="meetingScheduled" required="yes" message="Meeting is required."/>
					</cfif>
					<b>Meeting Scheduled</b>
					<cfif form.ro_meetingScheduled is "meetingNotRequired">
						<cfinput onClick="this.form.saveMeetingScheduled.value = 1; this.form.submit();" type="radio" checked="yes" name="ro_meetingScheduled" value="meetingNotRequired" required="yes" message="Meeting is required."/>
					<cfelse>
						<cfinput onClick="this.form.saveMeetingScheduled.value = 1; this.form.submit();" type="radio" name="ro_meetingScheduled" value="meetingNotRequired" required="yes" message="Meeting is required."/>
					</cfif>	
					<b>Meeting Not Required</b>
				</td>
			</tr>
			<cfif trim(form.ro_meetingScheduled) is "meetingScheduled">
				<tr>
					<td><b>Meeting Date: </b></td>
					<td>
						<cfinput type="datefield" name="ro_meetingDate" style="width:75px" value="#form.ro_meetingDate#" required="no" message="Meeting Date is required.">
					</td>
				</tr>
				<tr>
					<td><b>Meeting Time: </b></td>
					 <td>
						<cfinput type="text" name="ro_meetingTimeHH" style="width:25px; vertical-align:middle;" value="#form.ro_meetingTimeHH#" required="no" message="Hour is required."> : 
						<cfinput type="text" name="ro_meetingTimeMM" style="width:25px; vertical-align:middle;" value="#form.ro_meetingTimeMM#" required="no" message="Minutes are required.">
						<cfselect name="ro_meetingTimeTT" style="vertical-align:middle;" required="no" message="AM or PM is required.">
							<option <cfif form.ro_meetingTimeTT is "AM">selected</cfif> value="AM">AM</option>
							<option <cfif form.ro_meetingTimeTT is "PM">selected</cfif> value="PM">PM</option>
						</cfselect>
					</td>
				</tr>
			</cfif>
			<tr>
				<td>
				<input type="checkbox" <cfif form.rolloutComplete is 1>checked</cfif> name="rolloutComplete" value="1" />
				<b>Rollout Completed: </b></td>
			</tr>
			<tr>
				<td colspan="2">
					<cfinput type="submit" name="btnSaveRollout" id="Saveinfo" value="Save Info">
				</td>
			</tr>
		</table>
		</td>
	</tr>
</table>


