<!--- QA Checklist Section2 QA --->
<link rel="stylesheet" type="text/css" href="../styles.css">

<cfparam name = "form.qaRejected" default="0">
<cfparam name = "form.qaRejectedDescription" default="">
<cfparam name = "form.qaComplete" default="0">

<cfquery name="getEmployees" datasource="#ds#">
	select adminuserid, firstname + ' ' + lastname as employeeName 
	from admin_users 
	<!---where adminuserid = #qaid#--->
	where adminuserid = #cookie.adminlogin#
	order by firstname, lastname 
</cfquery>

<table width="675" border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td class="highlightbar"><b>Quality Assurance</b></td>
	</tr>
	<tr>	
		<td class="greyrowbottomnopadding">
		<table border="0" cellspacing="0" cellpadding="5">
		<td>
		<cfif prog_programmerSignOffID is getEmployees.adminuserid>
			You cannot QA your own programs
		<cfelse>
		</td>
			<tr>
			    <td valign="top" nowrap="nowrap"><b>QA Program List:</b></td>
			    <td valign="top" class="nopadding" colspan="2" >
			    	<table border="0" cellspacing="0" cellpadding="2">
						<cfloop list="#form.prog_programsModified#" index="programName">
							<cfoutput>
							<tr>
								<td>
									<cfif listfindnocase(form.qa_programsChecked,trim(programName)) is not 0>
										<!---<cfinput checked="yes" type="checkbox" name="qa_programsChecked" required="yes" message="At least one program must be selected." value="#trim(programName)#"/>--->
										<cfinput checked="yes" type="checkbox" name="qa_programsChecked" value="#trim(programName)#"/>
									<cfelse>
										<cfinput type="checkbox" name="qa_programsChecked" value="#trim(programName)#">
									</cfif>
								</td>
								<td>#trim(programName)#</td>
							</tr>
							</cfoutput>
						</cfloop>
					</table>
			    </td>
			  </tr>
				<tr>
					<td><b>QA Date: </b></td>
					<td>
						<cfinput type="datefield" name="qa_date" style="width:75px" value="#dateformat(form.qa_date,'mm/dd/yyyy')#" required="yes" message="QA Date is required.">
					</td>
				</tr>
				<cfinput type="HIDDEN" name="qa_signOffID" value="#cookie.adminlogin#">
				<tr>
					<td>
					<input type="checkbox" <cfif form.qaRejected is 1>checked</cfif> name="qaRejected" value="1" />
					<b>QA Rejected: </b></td>
				</tr>
				<tr>
					<td><b>Please explain why QA was rejected below: </b></span></td>
				</tr>
				<tr>
					<td colspan="2">
						<cftextarea style="width:650px;" rows="5" name="qaRejectedDescription"><cfoutput>#qaRejectedDescription#</cfoutput></cftextarea>
						<cfif form.qaRejected is 1>
							<cfif trim(form.qaRejectedDescription) is "">
							<span class="alert">QA Explanation is required.</span>
							</cfif>
						</cfif>
					</td>
				</tr>
				<tr>
				<td>
				<input type="checkbox" <cfif form.qaComplete is 1>checked</cfif> name="qaComplete" value="1" />
				<b>QA Completed: </b></td>
				</tr>
			<tr>
				<td colspan="2">
					<cfinput type="submit" name="btnSaveQA" id="Saveinfo" value="Save Info">
				</td>
			</tr>
		</cfif>
		</table>
		</td>
	</tr>
</table>
