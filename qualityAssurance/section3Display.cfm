<!--- QA Checklist Section3 Rollout View--->
<link rel="stylesheet" type="text/css" href="../styles.css">

<cfquery name="GetQAForm" datasource="#ds#" >
SELECT * from QualityAssuranceChecklist
WHERE qaid = #form.qaid#
</cfquery>

<table width="675" border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td class="highlightbar"><b>Rollout View</b></td>
	</tr>
	<tr>	
		<td class="greyrowbottomnopadding">
		<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<cfoutput query="GetQAForm">
			<tr>
				<td width="130" nowrap="nowrap"><b>Meeting Scheduled: </b></td>
				<td>
					<cfif #GetQAForm.ro_meetingScheduled# is "meetingScheduled">
						Yes
					</cfif>
					<cfif #GetQAForm.ro_meetingScheduled# is "meetingNotRequired">
						No, not required
					</cfif>
				</td>
			</tr>
			<cfif #GetQAForm.ro_meetingScheduled# is "meetingScheduled">
				<tr>
					<td nowrap="nowrap"><b>Meeting Date: </b></td>
					<td width="523">#DateFormat(GetQAForm.ro_meetingDateTime,'mm/dd/yyyy')# </td>
				</tr>
				<tr>
					<td nowrap="nowrap"><b>Meeting Time: </b></td>
					 <td width="523">#TimeFormat(GetQAForm.ro_meetingDateTime,'hh:mm tt')#</td>
				</tr>
			</cfif>
			<tr>
				<td>
					<cfif GetQAForm.rolloutComplete is 1>
					Completed Rollout
					</cfif>
				</td>
			</tr>
			</cfoutput>
		</table>
		</td>
	</tr>
</table>



