<!--- Qualtiy Assurance Checklist Update Page --->

<!--- Set variable to null in case no value passed from form --->
<cfparam name="form.prog_purpose" default="">
<cfparam name="form.prog_inquiryNumber" default="">
<cfparam name="form.prog_projectNumber" default="">
<cfparam name="form.prog_description" default="">
<cfparam name="form.prog_requestor" default="">
<cfparam name="form.prog_programmer" default="">
<cfparam name="form.prog_program" default="">
<cfparam name="form.prog_programsModified" default="">
<cfparam name="form.prog_commentsAdded" default="">
<cfparam name="form.prog_compiled" default="">
<cfparam name="form.prog_debugRemoved" default="">
<cfparam name="form.prog_cataloged" default="">
<cfparam name="form.prog_programmerSignOffID" default="">
<cfparam name="form.programmingComplete" default="0">
<cfparam name="form.qaComplete" default="0">
<cfparam name="form.qa_programsChecked" default="">
<cfparam name="form.qaRejected" default="0">
<cfparam name="form.qaRejectedDescription" default="">
<cfparam name="form.ro_meetingDate" default="">
<cfparam name="form.ro_meetingTimeHH" default="">
<cfparam name="form.ro_meetingTimeMM" default="">
<cfparam name="form.ro_meetingTimeTT" default="">
<cfparam name="form.ro_meetingScheduled" default="">
<cfparam name="form.rolloutComplete" default="">
<cfparam name="form.prod_cataloged" default="">
<cfparam name="form.prod_pvUpdated" default="">
<cfparam name="form.prod_deptNotified" default="">
<cfparam name="form.productionComplete" default="">

<!--- programming section logic --->
<cfif isDefined("form.btnSaveProgramming")>

	<!--- replace ; with , in program list --->
	<cfset form.prog_programsModified = replace(form.prog_programsModified, ";", ",", "all")>

	<!--- do save query --->
	<cfif form.qaid is not 0>
		<!--- update query --->
		<cfquery name="updateProgramming" datasource="#ds#">
			UPDATE QualityAssuranceChecklist 
			SET prog_purpose     	= '#form.prog_purpose#',
			prog_inquiryNumber		= '#form.prog_inquiryNumber#',
			prog_projectNumber		= '#form.prog_projectNumber#',
			prog_description 		= '#form.prog_description#',
			prog_requestor   		= '#form.prog_requestor#',
			prog_programmer 	 	= '#form.prog_programmer#',
			prog_program	 		= '#form.prog_program#',
			prog_programsModified 	= '#form.prog_programsModified#',
			prog_commentsAdded		= '#form.prog_commentsAdded#',
			prog_compiled			= '#form.prog_compiled#',
			prog_debugRemoved		= '#form.prog_debugRemoved#',
			prog_cataloged			= '#form.prog_cataloged#',	
			prog_programmerSignOffID = '#form.prog_programmerSignOffID#',
			active				     = 1
			WHERE qaid = #form.qaid# 
		</cfquery>
	<cfelse>
		<!--- insert query --->
		<cfset dateTimeStarted = createodbcdatetime(now())>
		<cfquery name="createRecord" datasource="#ds#">
			INSERT into QualityAssuranceChecklist (prog_dateTimeStarted,prog_purpose,prog_inquiryNumber,prog_projectNumber,prog_description, prog_requestor,prog_programmer,prog_program,prog_programsModified,prog_commentsAdded,prog_compiled,prog_debugRemoved,prog_cataloged,active,prog_programmerSignOffID) 
			VALUES (#dateTimeStarted#, 
				'#form.prog_purpose#',
				'#form.prog_inquiryNumber#',
				'#form.prog_projectNumber#',
				'#form.prog_description#',
				'#form.prog_requestor#',
				'#form.prog_programmer#',
				'#form.prog_program#',
				'#form.prog_programsModified#',
				'#form.prog_commentsAdded#',
				'#form.prog_compiled#',
				'#form.prog_debugRemoved#',
				'#form.prog_cataloged#',
				1,
				'#cookie.adminlogin#')
		</cfquery>
		<!--- get new QAID and save as form.qaid --->
		<cfquery name="getQAID" datasource="#ds#">
			select max(qaid) as newid from QualityAssuranceChecklist 
			where prog_dateTimeStarted = #dateTimeStarted# 
		</cfquery>
		<cfset form.qaid = getQAID.newid>
		
		<!--- send email to stella --->
		<cfmail from="system.info@copsmonitoring.com" to="pgregory@copsmonitoring.com" subject="A new QA form has been submitted">
		A new QA form has been submitted, you can view here: 
		
		You can view the details here: http://192.168.107.10/qualityAssurance/qaForm.cfm?qaid=#form.qaid#		
		</cfmail>
		
	</cfif>
	<cfif form.programmingComplete is 1>
		<!--- update programmingComplete flag in DB --->
		<cfquery name="setProgrammingComplete" datasource="#ds#">
			UPDATE QualityAssuranceChecklist 
			SET programmingComplete = 1 
			WHERE qaid = #form.qaid# 
		</cfquery>
	</cfif>
	<cfset loadData = true>
</cfif>
	
<!--- qa section logic --->
<cfif isDefined("form.btnSaveQA")>
	<cfquery name="updateQA" datasource="#ds#">
		UPDATE QualityAssuranceChecklist 
		SET qa_date		    = '#form.qa_date#',
		qa_signOffID	  	= '#form.qa_signOffID#',
		qa_programsChecked	= '#form.qa_programsChecked#',
		qaRejected			= '#form.qaRejected#',
		qaRejectedDescription = '#form.qaRejectedDescription#'
		WHERE 
		qaid = #form.qaid# 
	</cfquery>

	<cfif form.qaRejected is 1>
		<cfquery name="getProgrammingComplete" datasource="#ds#">
			SELECT programmingComplete from QualityAssuranceChecklist
			WHERE qaid = #form.qaid#
		</cfquery>
		<cfif getProgrammingComplete.programmingComplete is 1>
			<cfquery name="reOpenProgrammingComplete" datasource="#ds#">
				update QualityAssuranceChecklist 
				set programmingComplete = 0,
				    qaComplete = 0
				WHERE qaid = #form.qaid#
			</cfquery>
		</cfif>
		<cfif form.qaRejected is 1> 
		<!--- Send reject email to programmer to notify of rejection --->
		<cfquery name="getqainfo" datasource="#ds#">
			select * from QualityAssuranceChecklist 
			where qaid = #form.qaid# 
		</cfquery>
		<cfif getqainfo.recordcount gt 0>
			<cfquery name="getprogrammersemail" datasource="#ds#">
				select * from admin_users 
				where adminuserid = #getqainfo.prog_programmerSignOffID# 
			</cfquery>
			
			<cfif getprogrammersemail.recordcount GT 0>
<cfmail from="system.info@copsmonitoring.com" to="#getprogrammersemail.email#" subject="QA Checklist QA Section Rejected" username="system" password="V01c3">
This is to notify you that your QA Checklist QA Section is rejected for the following:

 #form.qaRejectedDescription#
</cfmail>
			</cfif>
		</cfif>
		
			<cflocation url="index.cfm?qaid=#form.qaid#">
		</cfif>
	</cfif>
	
	<cfif form.qaComplete is 1>
		<!--- update QAComplete flag in DB --->
		<cfquery name="setQAComplete" datasource="#ds#">
			update QualityAssuranceChecklist 
			set qaComplete = 1
			where qaid = #form.qaid# 
		</cfquery>
		
		<!--- get email of original programmer to send alert to --->
		<cfquery name="getqainfo" datasource="#ds#">
			select * from QualityAssuranceChecklist 
			where qaid = #form.qaid# 
		</cfquery>
		<cfif  getqainfo.recordcount gt 0>
			<cfquery name="getprogrammersemail" datasource="#ds#">
				select * from admin_users 
				where adminuserid = #getqainfo.prog_programmerSignOffID# 
			</cfquery>
			
			<cfif getprogrammersemail.recordcount GT 0>
<cfmail from="system.info@copsmonitoring.com" to="#getprogrammersemail.email#" subject="QA Checklist QA Section Completed" username="system" password="V01c3">
This is to notify you that your QA Checklist QA Section is completed.
 
You can view the details here: http://192.168.107.10/qualityAssurance/qaForm.cfm?qaid=#form.qaid#
</cfmail>
			</cfif>
		</cfif>
	</cfif>
	<cfset loadData = true>
</cfif>

<!--- rollout section logic --->
<cfif isDefined("form.btnSaveRollout") or form.saveMeetingScheduled is 1>
	<cfif trim(form.ro_meetingDate) is not "">
		<cfset form.ro_meetingDateTime = "#form.ro_meetingDate# #form.ro_meetingTimeHH#:#form.ro_meetingTimeMM# #form.ro_meetingTimeTT#">
		<cfset form.ro_meetingDateTime = createodbcdatetime(form.ro_meetingDateTime)>
	<cfelse>
		<cfset form.ro_meetingDateTime = "">
	</cfif>
	
	<cfquery name="updateRollout" datasource="#ds#">
		UPDATE QualityAssuranceChecklist 
		SET 
		<cfif trim(form.ro_meetingDateTime) is not "">
			ro_meetingDateTime = #form.ro_meetingDateTime#,
		</cfif>
		ro_meetingScheduled = '#form.ro_meetingScheduled#'
		WHERE qaid = #form.qaid# 
	</cfquery>

	<cfif form.rolloutComplete is 1>
		<!--- update ROLLOUTComplete flag in DB --->
		<cfquery name="setRolloutComplete" datasource="#ds#">
			update QualityAssuranceChecklist 
			set rolloutComplete = 1 
			where qaid = #form.qaid# 
		</cfquery>
		<!---<cfset form.saveMeetingScheduled = 0>--->
	</cfif>
	<cfset loadData = true>
</cfif>

<!--- production section logic --->
<cfif isDefined("form.btnSaveProduction")>
	<cfquery name="updateProduction" datasource="#ds#">
		UPDATE QualityAssuranceChecklist 
		SET prod_makeLiveDate = '#form.prod_makeLiveDate#',
		<cfif trim(form.prod_cataloged) is not "">
			prod_cataloged  = '#form.prod_cataloged#', 
		</cfif>
		<cfif trim(form.prod_pvUpdated) is not "">
			prod_pvUpdated = '#form.prod_pvUpdated#',
		</cfif>
		<cfif trim(form.prod_deptNotified) is not "">
			prod_deptNotified = '#form.prod_deptNotified#',
		</cfif>
		prod_completedDate =  '#form.prod_completedDate#'
		WHERE qaid = #form.qaid# 
	</cfquery>
	
	<cfif form.productionComplete is 1>
	<!--- update productionComplete flag in DB --->
		<cfquery name="setProductionComplete" datasource="#ds#">
			update QualityAssuranceChecklist 
			set productionComplete = 1 
			where qaid = #form.qaid# 
		</cfquery>
	</cfif>
	<cfset loadData = true>
</cfif>




