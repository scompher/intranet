
<cfquery name="GetAttachmentInfo" datasource="#ds#" >
	SELECT QualityAssuranceChecklist_attachments.fileName
	FROM   QualityAssuranceChecklist_attachments 
	WHERE QualityAssuranceChecklist_attachments.attachmentID = #url.attachmentID#
</cfquery>

<cfheader name="content-disposition" value="attachment;filename=#getAttachmentInfo.fileName#">
<cfcontent file="#request.directpath#\qualityAssurance\#getAttachmentInfo.fileName#" type="application/unknown">

