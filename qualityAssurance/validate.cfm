<cfset errorFlag = 0>
<cfset errorMsg = "">

<cfif isDefined("form.btnSaveProgramming")>
    <cfif trim(form.prog_purpose) is "">
        <cfset errorMsg = listappend(errorMsg,"The purpose is required.","|")>
        <cfset errorFlag = 1>
    </cfif>
    <!--- Validate Inquiry Number --->
    <cfif trim(form.prog_purpose) is "inquiry">
        <cfif form.prog_inquiryNumber is not 0>
            <cfif isNumeric(form.prog_inquiryNumber)>
                <cfif isInquiry(form.prog_inquiryNumber) is false>
                    <cfset errorMsg = listappend(errorMsg,"The inquiry specified does not exist.","|")>
                    <cfset errorFlag = 1>
                </cfif>
                <cfelse>
                <cfset errorMsg = listappend(errorMsg,"The inquiry number must be numeric.","|")>
                <cfset errorFlag = 1>
            </cfif>
        </cfif>
    </cfif>
	<cfif errorFlag is 1>
		<cfset form.programmingComplete = 0>
	</cfif>
</cfif>

<cfif isDefined("form.btnSaveQA")>
	<cfif form.qaComplete is 1 and form.qaRejected is 1>
		<cfset errorMsg = listappend(errorMsg,"You cannot reject and complete at the same time.")>
		<cfset errorFlag = 1>
		<cfset loadData = true>
	</cfif>
</cfif>

<!--- Display red error box --->
<cfif trim(errorMsg) is not "">
    <cfloop list = "#errorMsg#" index = "message" delimiters = "|">
        <br />
        <div align="center">
            <table width="300" border="0" cellpadding="5" cellspacing="0" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold; color:#FFFFFF; border:solid thin #CC0000; background-color:#FF0000; padding:5px">
                <tr>
                    <td><cfoutput>#message#</cfoutput></td>
                </tr>
            </table>
        </div>
    </cfloop>
</cfif>
