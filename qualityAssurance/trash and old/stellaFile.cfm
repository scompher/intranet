<!--- Uploading a file to the server --->
<link rel="stylesheet" type="text/css" href="/styles.css">

<cfparam name="url.action" default="">
<cfparam name="form.qaid" default="">
<cfparam name="form.attachmentID" default="">

<cfset form.qaid = 40>

<cfif isDefined("form.btnAddFile")>
	<cffile action="upload" filefield="attachment" destination="#request.Directpath#\qualityAssurance\attachments\" nameconflict="makeunique">
	<cfset savedFile = cffile.serverFile>
	<!--- Add file to DB --->
	<cfquery name = "InsertFileDb" datasource = "#ds#">
		INSERT INTO QualityAssuranceChecklist_attachments (fileName, qaid)
		VALUES ('#savedFile#',
			'#form.qaid#') 
	</cfquery>
</cfif>

<!--- Delete File from DB --->
<cfif url.action is "delete">
	<!--- Query to get file info from DB for deletion--->	
	<cfquery name="GetAttachmentDel" datasource="#ds#" >
		SELECT QualityAssuranceChecklist_attachments.fileName
		FROM   QualityAssuranceChecklist_attachments 
		WHERE QualityAssuranceChecklist_attachments.attachmentID = #url.attachmentID#
	</cfquery>
	
	<cfif FileExists("#request.Directpath#\qualityAssurance\attachments\#GetAttachmentDel.filename#")>
		<cffile action="delete" file = "#request.Directpath#\qualityAssurance\attachments\#GetAttachmentDel.filename#">
	</cfif>
	<cfquery name = "DeleteAttachment" datasource = "#ds#">
		DELETE FROM QualityAssuranceChecklist_attachments 
		WHERE qaid = #url.qaid# and
		attachmentID = #url.attachmentID#
	</cfquery>
</cfif>

<!--- Query to get file info from DB --->	
<cfquery name="GetQAAttachment" datasource="#ds#" >
	SELECT QualityAssuranceChecklist_attachments.*
	FROM   QualityAssuranceChecklist_attachments 
	WHERE QualityAssuranceChecklist_attachments.qaid = #form.qaid#
</cfquery>
<div class="normal" style="width:600px; position:relative;z-index:1;">

<!--- Upload File --->
<cfform action="stellaFile.cfm" method="post" enctype="multipart/form-data">
	<table width="600" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td><b>Add Attachments:</b></td>
		</tr>
		<tr>
			<td>
				<cfinput type="file" name="attachment">
				<cfinput type="submit" name="btnAddFile" id="Addfile" value="Add File">
			</td>
		</tr>
	</table>
</cfform>

<table width="600" border="0" cellspacing="0" cellpadding="5">
	<tr>	
		<td><b>Current Attachments:</b></td>
	</tr>
	<tr>
		<td>
			<cfoutput query="GetQAAttachment">
				<a target="_blank" href="/qualityAssurance/attachments/#fileName#"><img src="/images/view.gif" alt="View" width="16" height="16" border=""/>&nbsp;</a>					
				<a onclick="return confirm('Are you sure you want to delete this?');" href="stellaFile.cfm?qaid=#GetQAAttachment.qaid#&amp;attachmentID=#GetQAAttachment.attachmentID#&amp;action=delete"><img src="/images/delete.gif" alt="Delete" width="16" height="16" border="" /></a>
				#GetQAAttachment.fileName# <br>
			</cfoutput>
	</td>
	</tr>
</table>

</div>
