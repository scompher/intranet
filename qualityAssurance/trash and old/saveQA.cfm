<cfparam name = "form.purpose" default = "">
<cfparam name = "form.description" default = "">
<cfparam name = "form.requestor" default = "">
<cfparam name = "form.listDateTimeCreated" default = "">
<cfparam name = "form.listDateTimeCompleted" default = "">

<cfset dateTime = createodbcdatetime(now())>
<cfset form.listDateTimeCreated = dateTime>

<cfquery name = "UpdateDb" datasource = "#ds#">
	INSERT INTO QualityAssuranceChecklist (purpose,description,requestor,employeeID,employeeName,listDateTimeCreated,listDateTimeCompleted)
	VALUES ('#form.purpose#',
		'#form.description#', 
		'#form.requestor',
		'#loginID#',
		'#form.employeeName#',
		#listDateTimeCreated#,
		#listDateTimeCompleted#)
</cfquery>