<!--- Uploading a file to the server --->

<cfif isDefined("form.btnSaveFile")>
	<cffile action="upload" filefield="attachment" destination="#request.Directpath#\qualityAssurance\attachments\" nameconflict="makeunique">
	<cfset savedFile = cffile.serverFile>
	<cfoutput>#savedFile#</cfoutput>
<cfelse>
	<cfform action="stellaFile.cfm" method="post" enctype="multipart/form-data">
		<cfinput type="file" name="attachment">
		<cfinput type="submit" name="btnSaveFile" id="Savefile" value="Save File">
	</cfform>
</cfif>

<!---<cfoutput>Letter One</cfoutput> <br />
<cfoutput>Letter Two</cfoutput> <br />
<cfoutput>Letter Three</cfoutput> <br />--->