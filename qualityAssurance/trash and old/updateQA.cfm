<cfset dateTime = createodbcdatetime(now())>
<cfset form.listDateTimeCreated = dateTime>
<cfset ro_meetingTime = createodbcdatetime(ro_meetingTimeHH & " " & ro_meetingTimeMM & " " & ro_meetingTimeHH)>
<cfset ro_meetingDateTime = createodbcdatetime(ro_meetingDate & " " & ro_meetingTime)>

<cfquery name = "UpdateDb" datasource = "#ds#">
	UPDATE QualityAssuranceChecklist 
		SET prog_purpose     		= '#form.prog_purpose#',
			prog_description 		= '#form.prog_description#',
			prog_requestor   		= '#form.prog_requestor#',
			prog_programmer 	 	= '#form.prog_programmer#',
			prog_program	 		= '#form.prog_program#',
			prog_programsModified 	= '#form.prog_programsModified#',
			prog_commentsAdded		= '#form.prog_commentsAdded#',
			prog_compiled			= '#form.prog_compiled#',
			
		WHERE qaID = #form.qaID# 
</cfquery>


