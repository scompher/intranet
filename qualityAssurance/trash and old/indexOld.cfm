
<link rel="stylesheet" type="text/css" href="../../styles.css">
<cfparam name="url.viewflag" default="">

<div align="center" class="normal" style="position:relative;z-index:0">
<table width="725" border="0" cellpadding="5" cellspacing="0">
	<cfform method="post" action="../qaForm.cfm">
	<tr>
		<td style="padding-left:0px;"><input type="submit" value="Create New QA Checklist" /></td>
	</tr>
	</cfform>
</table>

<cfquery name="getItems" datasource="#ds#">
	select * 
	from QualityAssuranceChecklist 
	order by qaid desc
</cfquery>

<table width="725" border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td class="highlightbar"><b>COPS Quality Assurance Checklist Menu</b></td>
	</tr>
	<tr>
	<td class="greyrowbottomnopadding">
	<table width="100%" border="0" cellpadding="5" cellspacing="0">
		<tr>
			<td nowrap="nowrap" class="linedrowrightcolumn"><b>Completed</b></td>	
			<td width="8%" align="center" nowrap="nowrap" class="linedrowrightcolumn">&nbsp;
			</td>							
			<td nowrap="nowrap" class="linedrowrightcolumn"><b>Date/Time Created </b></td>
			<td nowrap="nowrap" class="linedrowrightcolumn"><b>Purpose </b></td>
			<td nowrap="nowrap" class="linedrowrightcolumn"><b>Programmer </b></td>
			<td class="linedrow"><b>Description </b></td>
		</tr>
		<cfoutput query="getItems">
		<tr>
			<td align="center" nowrap="nowrap" class="linedrowrightcolumn">
			<cfif getItems.productionComplete is 1><img src="/images/completed.gif" alt="Completed" width="16" height="16" border="" /><cfelse>&nbsp;</cfif>
			<!---<cfif #getItems.productionComplete# is 1><img src="/images/completed.gif" alt="Completed" width="16" height="16" border="" />
			<cfelseif #getItems.programmingComplete# is 1>PROG_Complete
			<cfelseif #getItems.qaComplete# is 1>QA_Complete
			<cfelseif #getItems.rolloutComplete# is 1>RO_Complete
			</cfif>--->
			</td>
			<td align="center" nowrap="nowrap" class="linedrowrightcolumn">
			<a href="../qaForm.cfm?qaid=#getItems.qaID#&amp;viewflag=n"><img src="/images/edit.gif" alt="Edit" width="16" height="16" border="" /></a>
			&nbsp;
			<a href="../qaForm.cfm?qaid=#getItems.qaid#&amp;viewflag=y"><img src="/images/view.gif" alt="View" width="16" height="16" border=""/></a>
			</td>
			<td nowrap="nowrap" class="linedrowrightcolumn">#dateformat(getItems.prog_dateTimeStarted,'mm/dd/yyyy')# #timeformat(getItems.prog_dateTimeStarted,'hh:mm tt')#</td>
			<td nowrap="nowrap" class="linedrowrightcolumn">#getItems.prog_purpose#
				<cfif getItems.prog_purpose is "inquiry">
					#getItems.prog_inquiryNumber#
				</cfif>
				<cfif getItems.prog_purpose is "project">
					#getItems.prog_projectNumber#
				</cfif>
			</td>
			<td nowrap="nowrap" class="linedrowrightcolumn">#getItems.prog_programmer#</td>
			<td nowrap="nowrap" class="linedrow">#left(getItems.prog_description,100)#</td>
		</tr>
		</cfoutput>
	</table>
	</td>
	</tr>
</table>

<br />
	<a style="text-decoration:underline;" href="/index.cfm">Return to Intranet</a>
</div>