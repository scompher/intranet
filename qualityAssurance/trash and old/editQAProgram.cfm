
<!---<cfquery name="GetQAChecklists" datasource="#ds#" >
	SELECT * from QualityAssuranceChecklist
	WHERE qaID = #qaID# 
</cfquery>--->

<link rel="stylesheet" type="text/css" href="/styles.css">

<cfparam name="form.startDate" default="">
<cfparam name="form.purpose" default="">
<cfparam name="form.inquiryNumber" default="">
<cfparam name="form.projectNumber" default="">
<cfparam name="form.qaDescription" default="">
<cfparam name="form.qaEmployeeName" default="">
<cfparam name="form.qaProgrammer" default="">
<cfparam name="form.qaChangeNew" default="">
<cfparam name="form.programsModified" default="">
<cfparam name="form.comments" default="">
<cfparam name="form.compiled" default="">
<cfparam name="form.debugRemoved" default="">
<cfparam name="form.cataloged" default="">
<cfparam name="form.programmerSignOff" default="">
<cfparam name="form.meetingScheduled" default="">


<div align="center" class="normal" >
<cfform method="post" action="editQAProgram.cfm">
<cfinput type = "hidden" name="qaID" value="#url.qaID#"/>

<table border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>COPS Quality Assurance Checklist - View</b></td>
	</tr>
	<tr>	
		<td class="greyrowbottomnopadding">
			<table border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td width="107" nowrap="nowrap"><b>Date Started:</b></td>
					<cfoutput query="GetQAChecklists">
					<td>
						<cfoutput>#GetQAChecklists.dateStarted#</cfoutput>
					</td>
				</tr>
				<tr>
					<td nowrap="nowrap"><b>Purpose:</b></td>
					<td>
					<cfoutput>#GetQAChecklists.purpost#</cfoutput>
					</td>
				</tr>
				<cfif trim(form.purpose) is "inquiry">
					<td nowrap="nowrap"><b>Inquiry Number:</b></td>
					<td>
					<cfoutput>#GetQAChecklists.qaInquiryNumber#</cfoutput>
					</td>
				</cfif>
				<cfif trim(form.purpose) is "project">
					<td nowrap="nowrap"><b>Project Number:</b></td>
					<td>
						<cfoutput>#GetQAChecklists.qaProjectNumber#</cfoutput>
					</td>
				</cfif>
				<tr>
					<td colspan="2"><b>Description:</b></td>
				</tr>
				<tr>
					<td colspan="2">
					<cfoutput>#GetQAChecklists.qaDescription#</cfoutput>
					</td>
				</tr>
				<tr>
					<td nowrap="nowrap"><b>Requested By:</b></td>
					<td>
					<cfoutput>#GetQAChecklists.qaEmployeeName#</cfoutput>
					</td>
				</tr>
				<tr>
					<td nowrap="nowrap"><b>Programmer:</b></td>
					<td>
					<cfoutput>#GetQAChecklists.qaProgrammer#</cfoutput>
					</td>
				</tr>
				<tr>
					<td nowrap="nowrap"><b>Program:</b></td>
					<td>
					<cfoutput>#GetQAChecklists.qaProgram#</cfoutput>
					</td>
				</tr>
				<tr>
					<td colspan="2"><b>Programs Modified:</b></td>
				</tr>
				<tr>
					<td colspan="2">
					<cfoutput>#GetQAChecklists.qaProgramsList#</cfoutput>
					</td>
				</tr>
				<tr>
					<td colspan="2" class="nopadding">
						<table border="0" cellspacing="0" cellpadding="5">
							<tr>
								<td>
								<cfoutput>#GetQAChecklists.comments#</cfoutput>
									Add Version Comments: </td>
								<td>
								<cfoutput>#GetQAChecklists.compiled#</cfoutput>
									Compiled: </td>
								<td>
								<cfoutput>#GetQAChecklists.debugRemoved#</cfoutput>
									Debug Statements Removed: </td>
								<td>
								<cfoutput>#GetQAChecklists.cataloged#</cfoutput>
									Cataloged (If Subroutine): </td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<b>Programmer Sign Off:</b>
						<cfoutput>#form.programmerSignOff#</cfoutput>
					</td>
					</tr>
				<tr>
					<td colspan="2">
						<cfinput type="button" name="btnExit" id="Saveinfo" onClick="document.location='index.cfm';" value="Exit View">
					</td>
				</tr>
			</cfoutput>
			</table>
		</td>
	</tr>
</table>

</cfform>

<br>
<a style="text-decoration:underline;" href="index.cfm">Return to Quality Assurance Menu List</a>
<br />
</div>

