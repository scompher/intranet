<!--- 10/31/2011 : SW : QA Checklist Attachment Menu --->
<link rel="stylesheet" type="text/css" href="/styles.css">

<cfquery name="GetQAAttachment" datasource="#ds#" >
	SELECT QualityAssuranceChecklist_attachments.*, QualityAssuranceChecklist.qaid 
	FROM   QualityAssuranceChecklist_attachments 
	LEFT JOIN QualityAssuranceChecklist ON QualityAssuranceChecklist_attachments.qaid = QualityAssuranceChecklist.qaid 
	<!---order by QualityAssuranceChecklist_attachments.qaid asc --->
</cfquery>

<div align="center" class="normal" style="position:relative;z-index:0">

<table border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>COPS Quality Assurance Checklist - Attachment List</b></td>
	</tr>
	<tr>	
		<td class="greyrowbottomnopadding">
			<table border="0" cellspacing="0" cellpadding="5">
			 	<tr>
                	<td width="4%" class="linedrowrightcolumn">&nbsp;</td>
                    <td width="11%" nowrap="nowrap" class="linedrowrightcolumn" align="center"><b>Action</b></td>
                        <td width="25%" align="center" nowrap="nowrap" class="linedrowrightcolumn"><b>Attachment</b></td>
						<td width="25%" align="center" nowrap="nowrap" class="linedrow"><b>Purpose</b></td>
             	</tr>
				<cfoutput query="GetQAAttachment">
					<tr>
						<td align="center" nowrap="nowrap" class="linedrowrightcolumn"><a href="viewAttachemnt.cfm?qaid=#GetQAAttachment.qaid#"><img src="/images/view.gif" alt="View" width="16" height="16" border="" />&nbsp;</a>
							<a onclick="return confirm('Are you sure you want to delete this?');" href="deleteAttachment.cfm?noteid=#GetQAAttachment.qaid#"><img src="/images/delete.gif" alt="Delete" width="16" height="16" border="" />&nbsp;</a>
							<!---<a href="viewApprovalMessage.cfm?noteid=#GetDealerMessage.noteid#"><img src="/images/view.gif" alt="View" width="16" height="16" border=""/></a></td>							--->
						<td nowrap="nowrap" class="linedrowrightcolumn">
						</td>
					</tr>
				</cfoutput>
			</table>
		</td>
	</tr>
</table>

	<br />
	<a style="text-decoration:underline;" href="/index.cfm">Return to Intranet</a>
</div>