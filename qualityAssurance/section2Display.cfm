<!--- QA Checklist Section2 QA View --->
<link rel="stylesheet" type="text/css" href="../styles.css">

<cfquery name="GetQAForm" datasource="#ds#" >
SELECT * from QualityAssuranceChecklist
WHERE qaid = #form.qaid#
</cfquery>

<cfif GetQAForm.qa_signOffID is not 0 and GetQAForm.qa_signOffID is not "">
	<cfquery name="getEmployees" datasource="#ds#">
		select adminuserid, firstname + ' ' + lastname as employeeName 
		from admin_users 
		where adminuserid = #GetQAForm.qa_signOffID#
		order by firstname, lastname 
	</cfquery>
</cfif>

<table width="675" border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td class="highlightbar"><b>Quality Assurance View</b></td>
	</tr>
	<tr>	
		<td class="greyrowbottomnopadding">
		<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr>
			    <td width="22%" valign="top"><b>QA Program List: </b></td>
			    <td width="78%" valign="top">
				<cfoutput>
				#replace(form.qa_programsChecked,",","<br />","all")#
				</cfoutput>
				</td>
			   </tr>
				
				<tr>
					<td colspan="2" class="nopadding">
						<table border="0" cellspacing="0" cellpadding="5">
						<cfoutput query="GetQAForm">
						<tr>
							<td><b>QA Date: </b></td>
								<td width="545">#DateFormat(GetQAForm.qa_date,'mm/dd/yyyy')# </td>
						</tr>
						<tr>
							<td><b>QA Sign Off: </b></td>
							<cfif #GetQAForm.qa_signOffID# is not "">
								<td>#getEmployees.employeeName#</td>
							<cfelse>
								<td>&nbsp;</td>
							</cfif>
						</tr>
						<tr>
							<cfif #GetQAForm.qaRejected# is not 0>
								<td><span class="alert">QA Rejected</span></td>
								<td><b>QA Description:</b> #GetQAForm.qaRejectedDescription#</td>
							<cfelse>
								<cfif #GetQAForm.qaRejectedDescription# is not "">
								<td><span class="alert">QA Notes</span></td>
								<td><b>QA Description:</b> #GetQAForm.qaRejectedDescription#</td>
								</cfif>
							</cfif>
							<cfif #GetQAForm.qaRejectedDescription# is not "">
							
							</cfif>
						</tr>
						<tr>
							<td>
								<cfif GetQAForm.qaComplete is 1>
								Completed QA
								</cfif>
							</td>
						</tr>
						</cfoutput>
						</table>
					</td>
				</tr>
		</table>
		</td>
	</tr>
</table>
