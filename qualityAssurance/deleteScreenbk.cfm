<link rel="stylesheet" type="text/css" href="../styles.css">

<cfparam name = "form.prog_id" default="">

<div align="center" class="normal" style="position:relative;z-index:0">

<cfform method="post" action="deleteQArecord.cfm">	
<table border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td class="highlightbar"><b>COPS Quality Assurance Checklist Delete Function</b></td>
	</tr>
	<tr>
	<td class="greyrowbottomnopadding">
	<table width="100%" border="0" cellpadding="5" cellspacing="0">
		<tr>
			<td nowrap="nowrap"><b>Enter id of record to delete</b></td>
			<td>
				<cfinput type="text" name="prog_id" id="prog_id" value="#form.prog_id#" required="yes" message="Must enter a record id.">
			</td>
			
			</td>							
		</tr>
	</table>
	</td>
	</tr>
</table>
</cfform>

<br />
	<a style="text-decoration:underline;" href="/index.cfm">Return to Intranet</a>
</div>

