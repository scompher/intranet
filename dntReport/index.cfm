<!--- LOAD HEADER --->
<cfinclude template="/dntReport/Template/header.cfm">

<!--- LOAD JAVA --->
<script src="/dntReport/script/dntIndexJava.js"></script>

<!--- LOAD MAIN TABBED WINDOW --->
<table width=100%><tr>
	<td align='center'>
		<div id='dntTabs'>
			<ul style='margin-left: 20px;'>
     			<li>View</li>
		    	<li>Add</li>
     			<li>Edit</li>
 		    </ul>
 		    <div><cfinclude template="view.cfm"></div>
 		    <div><cfinclude template="add.cfm"></div>
 		    <div><cfinclude template="edit.cfm"></div>
		</div>
	</td>
</tr></table>
    	
<!--- TEST FIELD --->
<div id='indexTest'></div>

<!--- LOAD FOOTER --->    
<cfinclude template="/dntReport/Template/footer.cfm">