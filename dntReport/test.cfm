<cfxml variable="xmlrequest">
<request>
  <dealerNumber>6867</dealerNumber>
  <dealerPasscode>s3cur1</dealerPasscode>
  <command>setBasicInfo</command>
  <data>
    <action>NEW</action>
    <accountNumber />
    <billingAccountNumber>511014396590168956</billingAccountNumber>
    <basicInformation xmlns="http://ucontrol.com/integration/cs/cops/v1">
      <name>COPS LOGS RETEST</name>
      <templateNumber>6867-1</templateNumber>
      <address>1215 Resi Test Account BRL W, APT 1234</address>
      <city>Manchester</city>
      <state>CT</state>
      <zipCode>06040-3816</zipCode>
      <ctvPhone1>1234567890</ctvPhone1>
      <ctvPhone2>0987654321</ctvPhone2>
      <licenseExpirationDate />
      <panelType>UCONTROL</panelType>
      <timeZone>E</timeZone>
      <accountType>R</accountType>
    </basicInformation>
  </data>
</request>
</cfxml>

<cf_copalink command="#xmlrequest#">

<cfdump var="#xmlparse(result)#">

