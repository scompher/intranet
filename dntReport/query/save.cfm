<cfoutput>
<!--- STOP EMPTY OUTPUT --->
<cfset dealerList='#form.dealerList#'>
<cfif dealerList is not ''>
	
	<!--- SAVE MAIN REPORT --->
	<cfquery name="saveMain" datasource="#ds#">
		insert into dntReport_Main ( reportName, frequency, day, active, actDate, ReportFormat, ReportType )
		values ( '#form.rptName#', '#form.frequency#', '#form.day#', 1, '#form.actDate#', '#form.rptFormat#', '#form.rptType#' )
	</cfquery> 
	
	<!--- GET ALL REPORT IDS --->					
	<cfquery name="getReportID" datasource="#ds#">
		select max(id)
		as id
		from dntReport_Main
	</cfquery> 

	<!--- GET CURRENT ID --->
	<cfset currentID='#getReportID.id#'>
	
	<!--- SAVE DEALER REPORT --->	
	<cfloop list='#form.dealerList#' index='currentDealer'>					
		<cfquery name="saveDealer" datasource="#ds#">
			insert into dntReport_Dealer ( reportID, dealer )
			values ( #currentID#, '#currentDealer#' )
		</cfquery> 
	</cfloop>
	
	<!--- SAVE E-MAIL REPORT --->	
	<cfloop list='#form.emailList#' index='currentEmail'>					
		<cfquery name="saveEmail" datasource="#ds#">
			insert into dntReport_Email ( reportID, email )
			values ( #currentID#, '#currentEmail#' )
		</cfquery> 
	</cfloop>
</cfif>
</cfoutput>

<!--- RETURN TO INDEX --->
<cflocation url="/dntReport/index.cfm" >