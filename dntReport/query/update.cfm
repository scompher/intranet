<cfoutput>
<!--- STOP EMPTY OUTPUT --->
<cfset dealerListEdit='#dealerListEdit#'>
<cfif dealerListEdit is not ''>
	
	<!--- SAVE MAIN REPORT --->
	<cfquery name="saveMain" datasource="#ds#">
		update dntReport_Main 
		set reportName = '#form.rptNameEdit#', frequency = '#form.frequencyEdit#', day = '#form.dayEdit#', active = 1, actDate = '#form.actDateEdit#',
		ReportFormat = '#form.rptFormatEdit#', ReportType = '#form.rptTypeEdit#'
		where id = #editID#
	</cfquery> 
	
	<!--- SAVE DEALER REPORT --->	
	<cfquery name='deleteDealer' datasource='#ds#'>
		delete from dntReport_Dealer
		where reportID = #editID#
	</cfquery>
	<cfloop list='#dealerListEdit#' index='currentDealer'>					
		<cfquery name="saveDealer" datasource="#ds#">
			insert into dntReport_Dealer ( reportID, dealer )
			values ( #editID#, '#currentDealer#' )
		</cfquery> 
	</cfloop>
	
	<!--- SAVE E-MAIL REPORT --->	
	<cfquery name='deleteEmail' datasource='#ds#'>
		delete from dntReport_Email
		where reportID = #editID#
	</cfquery>
	<cfloop list='#emailListEdit#' index='currentEmail'>					
		<cfquery name="saveEmail" datasource="#ds#">
			insert into dntReport_Email ( reportID, email )
			values ( #editID#, '#currentEmail#' )
		</cfquery> 
	</cfloop>
</cfif>
</cfoutput>

<!--- RETURN TO INDEX --->
<cflocation url="/dntReport/index.cfm" >