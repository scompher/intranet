<!--- LOAD JAVA --->
<script src="/dntReport/script/dntAddJava.js?2017-10-13_01''"></script>

<!--- SET PARAMETERS --->
<cfparam name='dealerList' default=''>
<cfparam name='emailList' default=''>
<cfparam name='frequency' default=''>
<cfparam name='day' default=''>
<cfparam name='reportName' default=''>
<cfparam name='activeDate' default=''>
<cfparam name='rptName' default=''>
<cfparam name='actDate' default=''>
<cfparam name='rptFormat' default=''>
<cfparam name='rptType' default=''>
<cfset activeDate=dateFormat(now(),"mm/dd/yyyy")>
<cfoutput>
<br><br>

<!--- DISPLAY INSTRUCTIONS --->
<table width=90% align='center'>
	<tr><td>
		<b style='font-size: 14px;'>Adding New Report...</b>
		<p style='font-size: 12px;'>-Enter new dealer number or e-mail in the appropriate field.</p>
		<p style='font-size: 12px;'>-Click on one or more Dealer/E-mail and use [Backspace] or [Delete] to remove them from the list. (You can also remove a Dealer/E-mail by re-entering it into the appropriate field)</p>
		<p style='font-size: 12px;'>-Reports are set to be 'active' the day they are created, but this can be changed by clicking on the Date field and entering or choosing a date from the calender.
	</td></tr>
</table>
<br>

<!--- DISPLAY INPUT FIELDS --->
<table width=90% align='center'>
	
	<!--- DEALER/E-MAIL INPUT --->	
	<tr>
		<td><b style='font-size: 12px;'>Dealer ##: </b></td>
		<td width='4.2%'></td>
		<td align='left'><b style='font-size: 12px;'>E-Mail: </b></td>
	</tr><tr>
		<td><input id='dealer' type='text'></td>
		<td colspan=2 align='right'><input id='email' type='text'></td>
    </tr><tr>
    	<td><div id='dlrBox' name='dlrBox'></div></td>
    	<td colspan=2 align='right'><div id='emlBox' name='emlBox'></div></td>
	</tr>
</table>
	
	
<table width=90% align='center'>
	<!--- REPORT NAME/ACTIVATION DATE INPUTS --->	
	<tr>
		<td><b style='font-size: 12px;'>Report Name:</b></td>
		<td colspan=5><input id='reportName' type='text'></td>
		<td><b style='font-size: 12px;'>Activation Date:</b></td>
		<td align='right'><input id='activeDate' type='text' value='#activeDate#'></td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td><b style='font-size: 12px;'>Frequency:</b></td>
		<td><div id='freqDrop'></div></td>
		<td><b style='font-size: 12px;'>Day:</b></td>
		<td><div id='dayDrop'></div></td>
		<!--<td><div id='dayDrop'></div></td>-->
		<td><b style='font-size: 12px;'>Format:</b></td>
		<td><div id="formatDrop"></div></td>
		<td colspan="2"><b style='font-size: 12px;'>Type: </b>
			<span style="margin-left:4px;">
				<label for="chkDNT">DNT</label>
    			<input type="checkbox" name="chkDNT" id="chkDNT" checked/>
    			<label for="chkNCF">NCF</label>    		
    			<input type="checkbox" name="chkNCF" id="chkNCF" />
    		</span>
    	</td>
    		
		<td></td>
		
		<!--- HIDDEN FORM FOR SUBMISSION --->
		<td align='right'>
		<form id="inputForm" method="post" action="query/save.cfm">
    		<input type='hidden' id='dealerList' name='dealerList' value='#dealerList#'>
    		<input type='hidden' id='emailList'  name='emailList'  value='#emailList#'>
    		<input type='hidden' id='actDate'    name='actDate'    value='#actDate#'>
    		<input type='hidden' id='rptName'    name='rptName'    value='#rptName#'>
    		<input type='hidden' id='frequency'  name='frequency'  value='#frequency#'>
    		<input type='hidden' id='day'        name='day'        value='#day#'>
    		<input type='hidden' id='rptFormat'  name='rptFormat'  value='#rptFormat#'>
    		<input type='hidden' id='rptType'  name='rptType'  value='#rptType#'>
			<input id='submit' name='submit' type='submit' value='Submit'>
		</form>
		</td>
	</tr>
	
	<!--- SPACER --->	
	<tr><td colspan='6'><br></td></tr>
	
	<!--- SPACER --->	
	<tr><td colspan='6'><br></td></tr>
</table>	

<!--- TEST FIELD --->
<div id='addTest'></div>
</cfoutput>