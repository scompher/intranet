<!--- LOAD JAVA --->
<script src="/dntReport/script/dntViewJava.js"></script>

<!--- SET PARAMETERS --->
<cfparam name='selID' default=''>

<!--- LOAD REPORT INFO --->
<cfinclude template="/dntReport/query/getReports.cfm">
<cfoutput>
	
<!--- HIDDEN FORM TO SEND INFO TO EDIT.CFM --->
<form name='hiddenForm' id='hiddenForm' method='post'>
    <input type='hidden' name='idsList' id='idsList' value='#idsList#'>
    <input type='hidden' name='rptList' id='rptList' value='#rptList#'>
    <input type='hidden' name='frqList' id='frqList' value='#frqList#'>
    <input type='hidden' name='dayList' id='dayList' value='#dayList#'>
    <input type='hidden' name='dlrList' id='dlrList' value='#dlrList#'>
    <input type='hidden' name='emlList' id='emlList' value='#emlList#'>    	
	<input type='hidden' name='rptTypeList' id='rptTypeList' value='#rptTypeList#'>
	<input type='hidden' name='rptFormatList' id='rptFormatList' value='#rptFormatList#'>	
</form>
<br><br>
	
<!--- DISPLAY INSTRUCTIONS --->
<table width=90% align='center'>
	<tr><td>
		<b style='font-size: 14px;'>Viewing DNT Reports...</b>
		<p style='font-size: 12px;'>-Click on a heading to set a filter (i.e: click on 'Dealers' and enter a Dealer number in the 'contains' field to limit display to reports that have that dealer attached.)</p>
		<p style='font-size: 12px;'>-Click a row and press [Enter] or Double-Click a row to edit that reprort.</p>
		<p style='font-size: 12px;'>-Click a row and use [Delete] or [Backspace], or the Deactivate button to deactivate the selected report<br>&nbsp;(note: Deactivated reports remain in the system but will not show or run.)</p>
	</td></tr>
</table>
<br>
	
<!--- DISPLAY TABLE AND DEACTIVATE BUTTON --->
<table width=90% align='center'>
	<tr><td><div id="reportTable"></div></td></tr>
	<tr><td align='right'>
	<form id='deactivateForm' method='post' action='query/deactivate.cfm'>
    	<input type='hidden' name='selID' id='selID' value='#selID#'>
		<input id='deactivate' type='button' value='Deactivate'>
	</form>
	</td></tr>
</table>
<br>

<!--- LOAD JAVA KEY LOGIC --->
<script src="/dntReport/script/dntViewKeyJava.js?2017-10-13_01"></script>
	
<!--- TEST FIELD --->
<div id='viewTest'></div>
</cfoutput>