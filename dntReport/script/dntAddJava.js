$(document).ready(function () {
					
	/* CREATE DEALER INPUT */
	$("#dealer").jqxInput({
		placeHolder: 'Dealer #',
		height: 25, 
		width: 74, 
		minLength: 4, 
		maxLength: 4, 
		searchMode: 'none',
		theme: 'energyblue' 
	});
		
		
	/* CREATE E-MAIL INPUT */
	$("#email").jqxInput({
		placeHolder: 'E-mail',
		height: 25, 
		width: 673, 
		minLength: 1, 
		maxLength: 50, 
		theme: 'energyblue' 
	});
		
	
	/* CREATE REPORT NAME INPUT */
	$("#reportName").jqxInput({
		placeHolder: 'Report Name',
		height: 25, 
		width: 400, 
		minLength: 1, 
		maxLength: 16, 
		theme: 'energyblue' 
	});
		
		
	/* CREATE START DATE INPUT */
	$("#activeDate").jqxInput({
		placeHolder: 'Activation Date',
		height: 25, 
		width: 110, 
		minLength: 1, 
		maxLength: 10, 
		theme: 'energyblue' 
	});
	
		
	/* START-DATE CALENDER */
	$( function() { $( "#activeDate" ).datepicker() });
	
	
	/* CREATE FREQUENCY DROP-DOWN */
    var freqSource = [ 'Daily', 'Weekly', 'Monthly' ];
    $("#freqDrop").jqxDropDownList({ 
       	source: freqSource, 
       	selectedIndex: 0,
       	dropDownHeight: 78,
       	width: 80, 
       	height: 25,
       	theme: 'energyblue'  
     });
         
         
         
	/* CREATE DAY DROP-DOWN */
    var daySource = [ 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday' ];
    $("#dayDrop").jqxDropDownList({ 
      	source: daySource, 
      	selectedIndex: 0,
       	dropDownHeight: 182,
    	disabled: true,
       	width: 110, 
       	height: 25,
       	theme: 'energyblue'  
    });
	
	/* CREATE FORMAT DROP-DOWN */
    var formatSource = [ 'Excel', 'PDF'];
    $("#formatDrop").jqxDropDownList({ 
       	source: formatSource , 
       	selectedIndex: 0,
       	dropDownHeight: 54,
       	width: 70, 
       	height: 25,
       	theme: 'energyblue'  
     });		
		
	
	/*
	//Requires JQuery 1.12
	$( function() { $( "#chkDNT" ).checkboxradio() });
	$( function() { $( "#chkNCF" ).checkboxradio() });
	*/	
		
	/* DISABLE DAY DROP-DOWN WHEN NOT SELECTING DAILY */
	$("#freqDrop").on('change', function (event){     
   		var freqOption = event.args.index;
   		$("#dayDrop").jqxDropDownList({ disabled: true }); 
    	if (freqOption == 1) { $("#dayDrop").jqxDropDownList({ disabled: false }) };
	});
		
		
	/* CREATE SUBMIT BUTTON */
    $("#submit").jqxButton({ 
		height: 25, 
		width: 70, 
		theme: 'energyblue' 
	});
		 
		 
	/* CREATE DEALER LIST BOX*/
	var dlrSource = [];
    $("#dlrBox").jqxListBox({ 
        source: dlrSource,
        height: 250,  
        width: 80, 
        multiple: true,
        theme: 'energyblue' 
    });
		 
		 
	/* CREATE E-MAIL LIST BOX*/
	var emlSource = [];
    $("#emlBox").jqxListBox({ 
    	source: emlSource,
        height: 250,  
        width: 679, 
        multiple: true,
        theme: 'energyblue' 
    });

		
	/* GIVE DEALER INPUT INITIAL FOCUS */
	$('#dealer').jqxInput('focus');
		
		
	/* GIVE DEALER INPUT FOCUS WHEN SWITCHING TABS */
 	$('#dntTabs').on('tabclick', function (event) {
     	var tabCLicked = event.args.item;
     	if ( tabCLicked == 1 ) {
			$('#dealer').jqxInput('focus');
     	};
	});
                         
                 
	/* UPDATE LIST OF DEALERS TO ADD */
	$("#dealer").keydown(function(event) { 
			
		// Get key pressed.
    	var thisKey = event.which;
        	
        // Enter Key Logic
    	if ( thisKey == 13 ) {
    			
    		// Get input and make uppercase.
    		var value = $('#dealer').val();
			value = value.toUpperCase();
				
			// Make sure a value has been entered...
			if (value != '') { 
				
				// Make sure that value is 4 characters long...
				if (value.length == 4) {
				
				// Trim bad characters and reconfirm length...
					value = value.replace(/[\W_]+/g,'');
					if (value.length == 4) {
						
						// Get current dealer list.		
       					var dlrList = [];
						var dlrItems = $("#dlrBox").jqxListBox('getItems'); 
      					for (var i = 0; i < dlrItems.length; i++) { dlrList[i] = dlrItems[i].label };		
						var dlrIndex = dlrList.indexOf(value)
					
						// If the value is not on the list, add it...
						if ( dlrIndex < 0) {
							$("#dlrBox").jqxListBox('insertAt', value, -1 ); 
													
						// ...otherwise remove it. 
						} else {
							$("#dlrBox").jqxListBox('removeAt', dlrIndex ); 
						};
					
						// Clear input field.
						$('#dealer').val('');
						
					// ...or return error.
					} else {
						$('#dealer').val(value);
						$('#dealer').jqxInput('focus');
						alert('*Error: Invalid dealer number!')
					};

				// ...or return error.
				} else {
					$('#dealer').val(value);
					$('#dealer').jqxInput('focus');
					alert('*Error: Dealer must be 4 characters long!')
				};
					
			// ...or...
			} else {
					
				// ...Make sure a dealer list exits.
       			var dlrList = [];
				var dlrItems = $("#dlrBox").jqxListBox('getItems'); 
      			for (var i = 0; i < dlrItems.length; i++) { dlrList[i] = dlrItems[i].label };
				if ( dlrList != '' ) {
						
					// Make sure an e-mail list exists.
       				var emlList = [];
					var emlItems = $("#emlBox").jqxListBox('getItems'); 
      				for (var i = 0; i < emlItems.length; i++) { emlList[i] = emlItems[i].label };
					if ( emlList != '' ) {
							
						// Make sure report has a name.
						var reportName = $("#reportName").val()
											
						// If so, submit...
						if ( reportName != '' ) {
							$('#submit').jqxButton('focus');
							
						// ...otherwise return Error.
						} else {
							$('#reportName').jqxInput('focus');
							alert('*Error: Submission requires a Report Name!');
						};
							
					// ...otherwise return Error.
					} else {
						$('#email').jqxInput('focus');
						alert('*Error: Submission requires at least 1 E-mail!');
					};
					
				// ...otherwise return Error.	
				}  else  {
					alert('*Error: Submission requires at least 1 Dealer!');
        		};
			};
				
		// Down arrow key logic.
    	} else if ( thisKey == 40 ) {
			$('#dlrBox').jqxListBox('focus');
    	};
	});
		
		
	/* UPDATE LIST OF E-MAILS TO ADD */
	$("#email").keydown(function(event){ 
			
		// Get key pressed.
    	var thisKey = event.which;
        	
        // Enter key logic.
    	if ( thisKey == 13 ) {
    			
    		// Get input and make uppercase.
    		var value = $('#email').val();
			value = value.toUpperCase();
    		value = value.replace(/ /g,'');
				
			// Make sure a value has been entered...
			if (value != '') {	
				
				// Validate e-mail...
    			var emailChecks = new RegExp(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/);
    			var emailResult = emailChecks.test(value);
				if (emailResult == true) {
					
					// Get current E-mail list.
       				var emlList = [];
					var emlItems = $("#emlBox").jqxListBox('getItems'); 
      				for (var i = 0; i < emlItems.length; i++) { emlList[i] = emlItems[i].label };		
					var emlIndex = emlList.indexOf(value)
					
					// If the value is not on the list, add it...
					if ( emlIndex < 0) {
						$("#emlBox").jqxListBox('insertAt', value, -1 ); 
						
					// ...otherwise remove it. 
					} else {
						$("#emlBox").jqxListBox('removeAt', emlIndex ); 
					};
											
					// Clear input field.
					$('#email').val('');
				
				// ...or return error.
				} else {
					$('#email').jqxInput('focus');
					alert('*Error: Invalid e-mail address!');
				};
				
					
			// ...or...
			} else {
					
				// ...make sure e-mail list exists....
       			var emlList = [];
				var emlItems = $("#emlBox").jqxListBox('getItems'); 
      			for (var i = 0; i < emlItems.length; i++) { emlList[i] = emlItems[i].label };
				if ( emlList != '' ) {
						
					// Make sure a dealer list exists.
       				var dlrList = [];
					var dlrItems = $("#dlrBox").jqxListBox('getItems'); 
      				for (var i = 0; i < dlrItems.length; i++) { dlrList[i] = dlrItems[i].label };
					if ( dlrList != '' ) {
							
						// Make sure report has a name.
						var reportName = $("#reportName").val()
											
						// If so, submit...
						if ( reportName != '' ) {
							$('#submit').jqxButton('focus');
							
						// ...otherwise return Error.
						} else {
							$('#reportName').jqxInput('focus');
							alert('*Error: Submission requires a Report Name!');
						};
							
					// ...otherwise return Error.
					} else {
						$('#dealer').jqxInput('focus');
						alert('*Error: Submission requires at least 1 Dealer!');
					};
					
				// ...otherwise return Error.	
				}  else  {
					alert('*Error: Submission requires at least 1 E-mail!');
        		};
			};
    	};
	});
		
		
	/* REPORT NAME INPUT FUNCTIONS */
	$("#reportName").keydown(function(event){ 
			
		// Get key pressed.
    	var thisKey = event.which;
        	
        // Enter key logic.
    	if ( thisKey == 13 ) {
    			
    		// Get input and make uppercase.
    		var reportName = $('#reportName').val();
				
			// Make sure a value has been entered...
			if (reportName != '') {
					
				// ...make sure e-mail list exists....
       			var emlList = [];
				var emlItems = $("#emlBox").jqxListBox('getItems'); 
      			for (var i = 0; i < emlItems.length; i++) { emlList[i] = emlItems[i].label };
				if ( emlList != '' ) {
						
					// Make sure a dealer list exists.
       				var dlrList = [];
					var dlrItems = $("#dlrBox").jqxListBox('getItems'); 
      				for (var i = 0; i < dlrItems.length; i++) { dlrList[i] = dlrItems[i].label };
							
					// If so, submit...
					if ( dlrList != '' ) {
						$('#submit').jqxButton('focus');
							
					// ...otherwise return Error.
					} else {
						$('#dealer').jqxInput('focus');
						alert('*Error: Submission requires at least 1 Dealer!');
					};
					
				// ...otherwise return Error.	
				}  else  {
					$('#email').jqxInput('focus');
					alert('*Error: Submission requires at least 1 E-mail!');
        		};
			} else {
				alert('*Error: Submission requires a Report Name!');
			};
		};
	});
		
				
	/* DEALER BOX CONTROLS */	
	$('#dlrBox').on('select', function (row) {
    	var rowArgs = row.args;
    	if (rowArgs) {
			$('#dlrBox').keydown(function (event) {
        		var dlrIndex = rowArgs.index;
        				
        		// Get key pressed.
    			var thisKey = event.which;
        	
        		// Delete or backspace key logic.
    			if ( thisKey == 8 || thisKey == 46 ) {
    				var dlrItems = $("#dlrBox").jqxListBox('getSelectedItems'); 
    				var dlrLen = dlrItems.length;
      				if (dlrLen > 0) {
          				for (var i = dlrLen-1; i >= 0 ; i--) {
							$("#dlrBox").jqxListBox('removeAt', dlrItems[i].index ); 
							$("#dlrBox").jqxListBox('unselectIndex', dlrItems[i].index ); 
          				};
      				};
    			};
			});
    	};
	});
		
				
	/* E-MAIL BOX CONTROLS */	
	$('#emlBox').on('select', function (row) {
    	var rowArgs = row.args;
    	if (rowArgs) {
			$('#emlBox').keydown(function (event) {
        		var emlIndex = rowArgs.index;
        				
        		// Get key pressed.
    			var thisKey = event.which;
        	
        		// Delete or backspace key logic.
    			if ( thisKey == 8 || thisKey == 46 ) {
    				var emlItems = $("#emlBox").jqxListBox('getSelectedItems'); 
    				var emlLen = emlItems.length;
      				if (emlLen > 0) {
          				for (var i = emlLen-1; i >= 0 ; i--) {
							$("#emlBox").jqxListBox('removeAt', emlItems[i].index ); 
							$("#emlBox").jqxListBox('unselectIndex', emlItems[i].index ); 
          				};
      				};
    			};
			});
    	};
	});
		
		
	/* SUBMIT BUTTON LOGIC */
	$("#submit").on('click', function(event){ 
			
    	// Get dealer and email lists
       	var emlList = [];
       	var dlrList = [];
       	var frequency = $("#freqDrop").val()
       	var day = $("#dayDrop").val()
       	if ( frequency != 'Weekly' ) { day = '' }
       	var reportName = $("#reportName").val()
       	reportName = reportName.toUpperCase()
       	var activeDate = $("#activeDate").val()
		var emlItems = $("#emlBox").jqxListBox('getItems'); 
      	for (var i = 0; i < emlItems.length; i++) { emlList[i] = emlItems[i].label };
		var dlrItems = $("#dlrBox").jqxListBox('getItems'); 
      	for (var i = 0; i < dlrItems.length; i++) { dlrList[i] = dlrItems[i].label };
				
		// If report name is missing, return error and set focus to report name...
		submitCheck: {
		if ( reportName == '' ) {
			$('#reportName').jqxInput('focus');
			alert('*Error: Submission requires a Report Name!');	
			event.preventDefault()
			break submitCheck;
				
		// ...or if dealer list is missing, return error and set focus to dealer...
		} else if ( dlrList == '' ) {
			$('#dealer').jqxInput('focus');
			alert('*Error: Submission requires at least 1 Dealer!');
			event.preventDefault()
			break submitCheck;
					
		// ...or if e-mail list is missing, return error and set focus to e-mail...	
		} else if ( emlList == '' ) {
			$('#email').jqxInput('focus');
			alert('*Error: Submission requires at least 1 E-mail!');
			event.preventDefault()
			break submitCheck;
		}else if ($('#chkDNT').prop("checked") == false && $('#chkNCF').prop("checked") == false ){
			alert('Select DNT, NCF or both');
			event.preventDefault();
			break submitCheck;
		// ...otherwise continue.
		} else {
			if ( activeDate != '' ) {
				if ( activeDate.length != 8 && activeDate.length != 10 ) {
					$('#activeDate').jqxInput('focus');
					alert('*Error: Active date incorrect format!');
					event.preventDefault()
				} else if ( activeDate[2] != '/' || activeDate[5] != '/') {
					$('#activeDate').jqxInput('focus');
					alert('*Error: Active date incorrect format!');
					event.preventDefault()
				} else {
					alert('*Form SUBMITTED!');
					$('#dealer').jqxInput('focus');
					$('#dealerList').val(dlrList);
					$('#emailList').val(emlList);
					$('#frequency').val(frequency);
					$('#day').val(day);
					$('#rptName').val(reportName);
					$('#actDate').val(activeDate);
					$("#rptFormat").val($("#formatDrop").val());
										
					var fmt= [];
					if ($('#chkDNT').prop("checked") == true)
						fmt.push("DNT");
					if ($('#chkNCF').prop("checked") == true)
						fmt.push("NCF");
					$("#rptType").val(fmt.join());
					
   					document.getElementById('inputForm').submit();   					
   				};
			} else {
				$('#activeDate').jqxInput('focus');
				alert('*Error: Active date required!');
				event.preventDefault()
			};
		}};
	});	
});