$(document).ready(function () {
				
	/* CREATE DEALER INPUT */
	$("#dealerEdit").jqxInput({
		placeHolder: 'Dealer #',
		height: 25, 
		width: 74, 
		minLength: 1, 
		maxLength: 4, 
		searchMode: 'none',
		theme: 'energyblue' 
	});
		
		
	/* CREATE E-MAIL INPUT */
	$("#emailEdit").jqxInput({
		placeHolder: 'E-mail',
		height: 25, 
		width: 673, 
		minLength: 1, 
		maxLength: 50, 
		theme: 'energyblue' 
	});
		
	
	/* CREATE REPORT NAME INPUT */
	$("#reportNameEdit").jqxInput({
		placeHolder: 'Report Name',
		height: 25, 
		width: 400, 
		minLength: 1, 
		maxLength: 16, 
		theme: 'energyblue' 
	});
		
		
	/* CREATE START DATE INPUT */
	$("#activeDateEdit").jqxInput({
		placeHolder: 'Activation Date',
		height: 25, 
		width: 110, 
		minLength: 1, 
		maxLength: 10, 
		theme: 'energyblue' 
	});
	
		
	/* START-DATE CALENDER */
	$( function() { $( "#activeDateEdit" ).datepicker() });
	
	
	/* CREATE FREQUENCY DROP-DOWN */
    var freqSource = [ 'Daily', 'Weekly', 'Monthly' ];
    $("#freqDropEdit").jqxDropDownList({ 
       	source: freqSource, 
       	selectedIndex: 0,
       	dropDownHeight: 78,
       	width: 80, 
       	height: 25,
       	theme: 'energyblue'  
     });
         
         
	/* CREATE DAY DROP-DOWN */
    var daySource = [ 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday' ];
    $("#dayDropEdit").jqxDropDownList({ 
      	source: daySource, 
      	selectedIndex: 0,
       	dropDownHeight: 182,
    	disabled: true,
       	width: 110, 
       	height: 25,
       	theme: 'energyblue'  
    });
    
		
	/* CREATE FORMAT DROP-DOWN */
    var formatSourceEdit = [ 'Excel', 'PDF'];
    $("#formatDropEdit").jqxDropDownList({ 
       	source: formatSourceEdit, 
       	selectedIndex: 0,
       	dropDownHeight: 54,
       	width: 70, 
       	height: 25,
       	theme: 'energyblue'  
     });		
				
		
	/* DISABLE DAY DROP-DOWN WHEN NOT SELECTING DAILY */
	$("#freqDropEdit").on('change', function (event){     
   		var freqOption = event.args.index;
   		$("#dayDropEdit").jqxDropDownList({ disabled: true }); 
    	if (freqOption == 1) { $("#dayDropEdit").jqxDropDownList({ disabled: false }) };
	});
		
		
	/* CREATE SUBMIT BUTTON */
    $("#submitEdit").jqxButton({ 
		height: 25, 
		width: 70, 
		theme: 'energyblue' 
	});
		 
		 
	/* CREATE DEALER LIST BOX*/
	var dlrSource = [];
    $("#dlrBoxEdit").jqxListBox({ 
        source: dlrSource,
        height: 250,  
        width: 80, 
        multiple: true,
        theme: 'energyblue' 
    });
		 
		 
	/* CREATE E-MAIL LIST BOX*/
	var emlSource = [];
    $("#emlBoxEdit").jqxListBox({ 
    	source: emlSource,
        height: 250,  
        width: 679, 
        multiple: true,
        theme: 'energyblue' 
    });

		
	/* GIVE DEALER INPUT INITIAL FOCUS */
	$('#dealerEdit').jqxInput('focus');
		
		
	/* GIVE DEALER INPUT FOCUS WHEN SWITCHING TABS */
 	$('#dntTabs').on('tabclick', function (event) {
     	var tabCLicked = event.args.item;
     	if ( tabCLicked == 1 ) {
			$('#dealerEdit').jqxInput('focus');
     	};
	});
                         
                 
	/* UPDATE LIST OF DEALERS TO ADD */
	$("#dealerEdit").keydown(function(event) { 
			
		// Get key pressed.
    	var thisKey = event.which;
        	
        // Enter Key Logic
    	if ( thisKey == 13 ) {
    			
    		// Get input and make uppercase.
    		var value = $('#dealerEdit').val();
			value = value.toUpperCase();
				
			// Make sure a value has been entered...
			if (value != '') {
				
				// Make sure that value is 4 characters long...
				if (value.length == 4) {
				
				// Trim bad characters and reconfirm length...
					value = value.replace(/[\W_]+/g,'');
					if (value.length == 4) {
					
						// Get current dealer list.		
       					var dlrList = [];
						var dlrItems = $("#dlrBoxEdit").jqxListBox('getItems'); 
      					for (var i = 0; i < dlrItems.length; i++) { dlrList[i] = dlrItems[i].label };		
						var dlrIndex = dlrList.indexOf(value)
					
						// If the value is not on the list, add it...
						if ( dlrIndex < 0) {
						$("#dlrBoxEdit").jqxListBox('insertAt', value, -1 ); 
												
						// ...otherwise remove it. 
						} else {
							$("#dlrBoxEdit").jqxListBox('removeAt', dlrIndex ); 
						};
					
						// Clear input field.
						$('#dealerEdit').val('');
						
					// ...or return error.
					} else {
						$('#dealerEdit').val(value);
						$('#dealerEdit').jqxInput('focus');
						alert('*Error: Invalid dealer number!')
					};

				// ...or return error.
				} else {
					$('#dealer').val(value);
					$('#dealerEdit').jqxInput('focus');
					alert('*Error: Dealer must be 4 characters long!');
				};
					
			// ...or...
			} else {
					
				// ...Make sure a dealer list exits.
       			var dlrList = [];
				var dlrItems = $("#dlrBoxEdit").jqxListBox('getItems'); 
      			for (var i = 0; i < dlrItems.length; i++) { dlrList[i] = dlrItems[i].label };
				if ( dlrList != '' ) {
						
					// Make sure an e-mail list exists.
       				var emlList = [];
					var emlItems = $("#emlBoxEdit").jqxListBox('getItems'); 
      				for (var i = 0; i < emlItems.length; i++) { emlList[i] = emlItems[i].label };
					if ( emlList != '' ) {
							
						// Make sure report has a name.
						var reportName = $("#reportNameEdit").val()
											
						// If so, submit...
						if ( reportName != '' ) {
							$('#submitEdit').jqxButton('focus');
							
						// ...otherwise return Error.
						} else {
							$('#reportNameEdit').jqxInput('focus');
							alert('*Error: Submission requires a Report Name!');
						};
							
					// ...otherwise return Error.
					} else {
						$('#emailEdit').jqxInput('focus');
						alert('*Error: Submission requires at least 1 E-mail!');
					};
					
				// ...otherwise return Error.	
				}  else  {
					alert('*Error: Submission requires at least 1 Dealer!');
        		};
			};
				
		// Down arrow key logic.
    	} else if ( thisKey == 40 ) {
			$('#dlrBoxEdit').jqxListBox('focus');
    	};
	});
		
		
	/* UPDATE LIST OF E-MAILS TO ADD */
	$("#emailEdit").keydown(function(event){ 
			
		// Get key pressed.
    	var thisKey = event.which;
        	
        // Enter key logic.
    	if ( thisKey == 13 ) {
    			
    		// Get input and make uppercase.
    		var value = $('#emailEdit').val();
			value = value.toUpperCase();
    		value = value.replace(/ /g,'');
			
			// Make sure a value has been entered...
			if (value != '') {
				
				// Validate e-mail...
    			var emailChecks = new RegExp(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/);
    			var emailResult = emailChecks.test(value);
				if (emailResult == true) {
					
					// Get current E-mail list.
	       			var emlList = [];
					var emlItems = $("#emlBoxEdit").jqxListBox('getItems'); 
	      			for (var i = 0; i < emlItems.length; i++) { emlList[i] = emlItems[i].label };		
					var emlIndex = emlList.indexOf(value)
						
					// If the value is not on the list, add it...
					if ( emlIndex < 0) {
						$("#emlBoxEdit").jqxListBox('insertAt', value, -1 ); 
							
					// ...otherwise remove it. 
					} else {
						$("#emlBoxEdit").jqxListBox('removeAt', emlIndex ); 
					};
						
					// Clear input field.
					$('#emailEdit').val('');
				
				// ...or return error.
				} else {
					$('#emailEdit').jqxInput('focus');
					alert('*Error: Invalid e-mail address!');
				};
					
			// ...or...
			} else {
					
				// ...make sure e-mail list exists....
       			var emlList = [];
				var emlItems = $("#emlBoxEdit").jqxListBox('getItems'); 
      			for (var i = 0; i < emlItems.length; i++) { emlList[i] = emlItems[i].label };
				if ( emlList != '' ) {
						
					// Make sure a dealer list exists.
       				var dlrList = [];
					var dlrItems = $("#dlrBoxEdit").jqxListBox('getItems'); 
      				for (var i = 0; i < dlrItems.length; i++) { dlrList[i] = dlrItems[i].label };
					if ( dlrList != '' ) {
							
						// Make sure report has a name.
						var reportName = $("#reportNameEdit").val()
											
						// If so, submit...
						if ( reportName != '' ) {
							$('#submitEdit').jqxButton('focus');
							
						// ...otherwise return Error.
						} else {
							$('#reportNameEdit').jqxInput('focus');
							alert('*Error: Submission requires a Report Name!');
						};
							
					// ...otherwise return Error.
					} else {
						$('#dealerEdit').jqxInput('focus');
						alert('*Error: Submission requires at least 1 Dealer!');
					};
					
				// ...otherwise return Error.	
				}  else  {
					alert('*Error: Submission requires at least 1 E-mail!');
        		};
			};
    		
		// Down arrow key logic.
    	} else if ( thisKey == 40 ) {
			$('#emlBoxEdit').jqxListBox('focus');
    	};
	});
		
		
	/* REPORT NAME INPUT FUNCTIONS */
	$("#reportNameEdit").keydown(function(event){ 
			
		// Get key pressed.
    	var thisKey = event.which;
        	
        // Enter key logic.
    	if ( thisKey == 13 ) {
    			
    		// Get input and make uppercase.
    		var reportName = $('#reportNameEdit').val();
				
			// Make sure a value has been entered...
			if (reportName != '') {
					
				// ...make sure e-mail list exists....
       			var emlList = [];
				var emlItems = $("#emlBoxEdit").jqxListBox('getItems'); 
      			for (var i = 0; i < emlItems.length; i++) { emlList[i] = emlItems[i].label };
				if ( emlList != '' ) {
						
					// Make sure a dealer list exists.
       				var dlrList = [];
					var dlrItems = $("#dlrBoxEdit").jqxListBox('getItems'); 
      				for (var i = 0; i < dlrItems.length; i++) { dlrList[i] = dlrItems[i].label };
							
					// If so, submit...
					if ( dlrList != '' ) {
						$('#submitEdit').jqxButton('focus');
							
					// ...otherwise return Error.
					} else {
						$('#dealerEdit').jqxInput('focus');
						alert('*Error: Submission requires at least 1 Dealer!');
					};
					
				// ...otherwise return Error.	
				}  else  {
					$('#emailEdit').jqxInput('focus');
					alert('*Error: Submission requires at least 1 E-mail!');
        		};
			} else {
				alert('*Error: Submission requires a Report Name!');
			};
		};
	});
		
				
	/* DEALER BOX CONTROLS */	
	$('#dlrBoxEdit').on('select', function (row) {
    	var rowArgs = row.args;
    	if (rowArgs) {
			$('#dlrBoxEdit').keydown(function (event) {
        		var dlrIndex = rowArgs.index;
        				
        		// Get key pressed.
    			var thisKey = event.which;
        	
        		// Delete or backspace key logic.
    			if ( thisKey == 8 || thisKey == 46 ) {
    				var dlrItems = $("#dlrBoxEdit").jqxListBox('getSelectedItems'); 
    				var dlrLen = dlrItems.length;
      				if (dlrLen > 0) {
          				for (var i = dlrLen-1; i >= 0 ; i--) {
							$("#dlrBoxEdit").jqxListBox('removeAt', dlrItems[i].index ); 
							$("#dlrBoxEdit").jqxListBox('unselectIndex', dlrItems[i].index ); 
          				};
      				};
    			};
			});
    	};
	});
		
				
	/* E-MAIL BOX CONTROLS */	
	$('#emlBoxEdit').on('select', function (row) {
    	var rowArgs = row.args;
    	if (rowArgs) {
			$('#emlBoxEdit').keydown(function (event) {
        		var emlIndex = rowArgs.index;
        				
        		// Get key pressed.
    			var thisKey = event.which;
        	
        		// Delete or backspace key logic.
    			if ( thisKey == 8 || thisKey == 46 ) {
    				var emlItems = $("#emlBoxEdit").jqxListBox('getSelectedItems'); 
    				var emlLen = emlItems.length;
      				if (emlLen > 0) {
          				for (var i = emlLen-1; i >= 0 ; i--) {
							$("#emlBoxEdit").jqxListBox('removeAt', emlItems[i].index ); 
							$("#emlBoxEdit").jqxListBox('unselectIndex', emlItems[i].index ); 
          				};
      				};
    			};
			});
    	};
	});
		
		
	/* SUBMIT BUTTON LOGIC */
	$("#submitEdit").on('click', function(event){ 
			
    	// Get dealer and email lists
       	var emlList = [];
       	var dlrList = [];
       	var frequency = $("#freqDropEdit").val()
       	var day = $("#dayDropEdit").val()
       	if ( frequency != 'Weekly' ) { day = '' }
       	var reportName = $("#reportNameEdit").val()
       	reportName = reportName.toUpperCase()
       	var activeDate = $("#activeDateEdit").val()
		var emlItems = $("#emlBoxEdit").jqxListBox('getItems'); 
      	for (var i = 0; i < emlItems.length; i++) { emlList[i] = emlItems[i].label };
		var dlrItems = $("#dlrBoxEdit").jqxListBox('getItems'); 
      	for (var i = 0; i < dlrItems.length; i++) { dlrList[i] = dlrItems[i].label };
				
		// If report name is missing, return error and set focus to report name...
		if ( reportName == '' ) { reportName = 'DNT REPORT' };
		
		submitCheck: {
		// ...or if dealer list is missing, return error and set focus to dealer...
		if ( dlrList == '' ) {
			$('#dealerEdit').jqxInput('focus');
			alert('*Error: Submission requires at least 1 Dealer!');
			event.preventDefault()
			break submitCheck;
					
		// ...or if e-mail list is missing, return error and set focus to e-mail...	
		} else if ( emlList == '' ) {
			$('#emailEdit').jqxInput('focus');
			alert('*Error: Submission requires at least 1 E-mail!');
			event.preventDefault()
			break submitCheck;
		}else if ($('#chkDNTEdit').prop("checked") == false && $('#chkNCFEdit').prop("checked") == false ){
			alert('Select DNT, NCF or both');
			event.preventDefault();
			break submitCheck;					
		// ...otherwise continue.
		} else {
			if ( activeDate != '' ) {
				if ( activeDate.length != 8 && activeDate.length != 10 ) {
					$('#activeDateEdit').jqxInput('focus');
					alert('*Error: Active date incorrect format!');
					event.preventDefault()
				} else if ( activeDate[2] != '/' || activeDate[5] != '/') {
					$('#activeDateEdit').jqxInput('focus');
					alert('*Error: Active date incorrect format!');
					event.preventDefault()
				} else {
					alert('*Form SUBMITTED!');
					$('#dealerEdit').jqxInput('focus');
					$('#dealerListEdit').val(dlrList);
					$('#emailListEdit').val(emlList);
					$('#frequencyEdit').val(frequency);
					$('#dayEdit').val(day);
					$('#rptNameEdit').val(reportName);
					$('#actDateEdit').val(activeDate);					
					
					$("#rptFormatEdit").val($("#formatDropEdit").val());										
					var fmtEdit= [];
					if ($('#chkDNTEdit').prop("checked") == true)
						fmtEdit.push("DNT");
					if ($('#chkNCFEdit').prop("checked") == true)
						fmtEdit.push("NCF");
					$("#rptTypeEdit").val(fmtEdit.join());
					
   					document.getElementById('editForm').submit();
   				};
			} else {
				$('#activeDateEdit').jqxInput('focus');
				alert('*Error: Active date required!');
				event.preventDefault()
			};
		}};
	});	
});