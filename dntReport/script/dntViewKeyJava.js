/* KEY LOGIC FOR VIEW.CFM */
document.addEventListener("keydown", keyLogic);
function keyLogic() {	
			
	// Only allow function on View Tab.
	var selectedItem = $('#dntTabs').jqxTabs('selectedItem'); 
	if ( selectedItem == 0 ) {
	
	// Make sure row is selected...
	var thisIndex = $('#reportTable').jqxGrid('getselectedrowindex');
	if ( thisIndex != -1 ) {
		
		// Get key pressed.
   		var thisKey = event.which;
       	
    	// Enter Key Logic
   		if ( thisKey == 13 ) {		
   			
   			// Grab info from selected row and head to edit tab.
   			$("#dlrBoxEdit").jqxListBox('clear');
    		$("#emlBoxEdit").jqxListBox('clear');
    		var rowIndex = thisIndex;
    		var edID = $('#reportTable').jqxGrid('getcellvalue', thisIndex, 'hiddenID');
			var edName = $('#reportTable').jqxGrid('getcellvalue', thisIndex, 'reportName');
			var edFreq = $('#reportTable').jqxGrid('getcellvalue', rowIndex, 'frequencies');
			var edDay = $('#reportTable').jqxGrid('getcellvalue', rowIndex, 'days');
			var edDlr = $('#reportTable').jqxGrid('getcellvalue', thisIndex, 'dealers');
			var edEml = $('#reportTable').jqxGrid('getcellvalue', thisIndex, 'emails');
    		edDlr = edDlr.split("<br>");
    		edEml = edEml.split("<br>");
    		$('#dntTabs').jqxTabs('enableAt', 2);
    		$('#dntTabs').jqxTabs('select', 2);
    		$("#editID").val(edID) 
    		$("#reportNameEdit").val(edName) 
			var dLen = edDlr.length;
    		for (i = 0; i < dLen; i++) { 
    			var dValue = edDlr[i];
				$("#dlrBoxEdit").jqxListBox('insertAt', dValue, -1 ); 
			};
			var eLen = edEml.length;
    		for (i = 0; i < eLen; i++) { 
    			var eValue = edEml[i];
				$("#emlBoxEdit").jqxListBox('insertAt', eValue, -1 ); 
			};

			var freqSource = [ 'Daily', 'Weekly', 'Monthly' ];
	    	for (f = 0; f < freqSource.length; f++) { if (freqSource[f] == edFreq) {freqIn = f}};
	   		$("#freqDropEdit").jqxDropDownList( 'selectIndex', freqIn );
	    	var daySource = [ 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday' ];
	    	
	    	var dayIn = 0;
	    	for (d = 0; d < daySource.length; d++) { if (daySource[d] == edDay) {dayIn = d}};
	   		$("#dayDropEdit").jqxDropDownList( 'selectIndex', dayIn );
			$('#dealerEdit').jqxInput('focus');
			
			
			$('#chkDNTEdit').attr('checked', false);
			$('#chkNCFEdit').attr('checked', false);
			
			var type = $('#rptTypeList').val().split(':')[rowIndex];
			if (type.indexOf("DNT") > -1) $('#chkDNTEdit').prop('checked', 'checked');
			if (type.indexOf("NCF") > -1) $('#chkNCFEdit').prop('checked', 'checked');
			
			/* This doesn't work in IE */		
			//if (type.includes("DNT") == true) $('#chkDNTEdit').prop('checked', 'checked');
			//if (type.includes("NCF") == true) $('#chkNCFEdit').prop('checked', 'checked');		
					
			var format = $('#rptFormatList').val().split(':')[rowIndex];		
			var formatSource= [ 'Excel', 'PDF'];
			for (fm=0; fm<formatSource.length; fm++){
				if (formatSource[fm] == format){
					$("#formatDropEdit").jqxDropDownList( 'selectIndex', fm );
					break;
				}
			}						
			
			$('#dealerEdit').jqxInput('focus');
				
		// Delete/Backspace key logic.
   		} else if ( thisKey == 8 || thisKey == 46 ) {
   			
   			// Grab selected report ID and deactivate that report.
     		var selID = $('#reportTable').jqxGrid('getcellvalue', thisIndex, 'hiddenID');
			var selName = $('#reportTable').jqxGrid('getcellvalue', thisIndex, 'reportName');
    		var r = confirm('Are you sure you want to DEACTIVATE "'+selName+'"?');
    		if (r == true) {
        		$("#selID").val(selID) 
        		document.getElementById("deactivateForm").submit();
    		};
    	};
    	
    // ... or refocus.
    } else {
		$('#reportTable').jqxInput('focus');
    }; 
    };
};