$(document).ready(function () {
		
	/* CREATE DEACTIVATE BUTTON */
    $("#deactivate").jqxButton({ 
		height: 25, 
		width: 90, 
		theme: 'energyblue' 
	});
    	
    	
   	/* GET REPORT INFO FROM LISTS */	
   	var reportID = $('#idsList').val().split(':');
   	var reportName = $('#rptList').val().split(':');
   	var frequencies = $('#frqList').val().split(':');
   	var days = $('#dayList').val().split(':');
    var dealers = $('#dlrList').val().split(':');
    var emails = $('#emlList').val().split(':');    
    var types = $('#rptTypeList').val().split(':');    
    var formats = $('#rptFormatList').val().split(':');
    
       
        
    /* SET ROW INFO */    
    var z = 0
    var data = [];
	var maxID = reportID.length;
    for (var x = 0; x < maxID; x++) {
       	var row = {};
        row["hiddenID"] = reportID[x];
        row["reportName"] = reportName[x];
        row["frequencies"] = frequencies[x];
        row["days"] = days[x];
        row["dealers"] =  dealers[x].replace(/,/g, '<br>');
        row["emails"] = emails[x].replace(/,/g, '<br>');        
        row["types"] = types[x].replace(/,/g, '<br>');
        row["formats"] = formats[x].replace(/,/g, '<br>');
        
        data[x] = row;
    };
        
        
    /* SET SOURCE */    
  	var source = {
      	localdata: data,
       	datatype: "array"
    };
            
            
    /* SET DATA ADAPTER */
    var dataAdapter = new $.jqx.dataAdapter(source, {
       	loadComplete: function (data) { },
       	loadError: function (xhr, status, error) { }      
    });
           
           
    /* CREATE REPORT TABLE */   
    $("#reportTable").jqxGrid( {
       	source: dataAdapter,
       	width: 850,
       	pageable: false,
       	autoheight: true,
   		autorowheight: true,
   		filterable: true,
	   	theme: 'energyblue',
        columns: [
           	{ text: 'hiddenID', datafield: 'hiddenID', hidden: 'true'},
           	{ text: '<b>Report Name</b>', datafield: 'reportName', width: 150, cellsalign: 'left' },
           	{ text: '<b>Freq.</b>', datafield: 'frequencies', width: 65, cellsalign: 'left' },
           	{ text: '<b>Day</b>', datafield: 'days', width: 85, cellsalign: 'left' },
        	{ text: '<b>Dealers</b>', datafield: 'dealers', width: 70, cellsalign: 'right' },
            { text: '<b>E-mails</b>', datafield: 'emails', width: 370, cellsalign: 'left' },            
            { text: '<b>Format</b>', datafield: 'formats', width: 60, cellsalign: 'left' },
            { text: '<b>Types</b>', datafield: 'types', width: 50, cellsalign: 'left' }
            
            
        ]
    });
    		
    	
    /* DOUBLE CLICK TO EDIT */
    $("#reportTable").on('rowdoubleclick', function(event) {
    	$("#dlrBoxEdit").jqxListBox('clear');
    	$("#emlBoxEdit").jqxListBox('clear');
    	var rowIndex = event.args.rowindex;
    	var edID = $('#reportTable').jqxGrid('getcellvalue', rowIndex, 'hiddenID');
		var edName = $('#reportTable').jqxGrid('getcellvalue', rowIndex, 'reportName');
		var edFreq = $('#reportTable').jqxGrid('getcellvalue', rowIndex, 'frequencies');
		var edDay = $('#reportTable').jqxGrid('getcellvalue', rowIndex, 'days');
		var edDlr = $('#reportTable').jqxGrid('getcellvalue', rowIndex, 'dealers');
		var edEml = $('#reportTable').jqxGrid('getcellvalue', rowIndex, 'emails');
    	edDlr = edDlr.split("<br>");
    	edEml = edEml.split("<br>");
    	$('#dntTabs').jqxTabs('enableAt', 2);
    	$('#dntTabs').jqxTabs('select', 2);
    	$("#editID").val(edID) 
    	$("#reportNameEdit").val(edName) 
		var dLen = edDlr.length;
    	for (i = 0; i < dLen; i++) { 
    		var dValue = edDlr[i];
			$("#dlrBoxEdit").jqxListBox('insertAt', dValue, -1 ); 
		};
		var eLen = edEml.length;
    	for (i = 0; i < eLen; i++) { 
    		var eValue = edEml[i];
			$("#emlBoxEdit").jqxListBox('insertAt', eValue, -1 ); 
		};
    	var freqSource = [ 'Daily', 'Weekly', 'Monthly' ];
    	for (f = 0; f < freqSource.length; f++) { if (freqSource[f] == edFreq) {freqIn = f}};
   		$("#freqDropEdit").jqxDropDownList( 'selectIndex', freqIn );
    	var daySource = [ 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday' ];
    	
    	var dayIn = 0;
    	for (d = 0; d < daySource.length; d++) { if (daySource[d] == edDay) {dayIn = d}};
   		$("#dayDropEdit").jqxDropDownList( 'selectIndex', dayIn );
		$('#dealerEdit').jqxInput('focus');
		
		
		$('#chkDNTEdit').attr('checked', false);
		$('#chkNCFEdit').attr('checked', false);
		
		var type = $('#rptTypeList').val().split(':')[rowIndex];
		if (type.indexOf("DNT") > -1) $('#chkDNTEdit').prop('checked', 'checked');
		if (type.indexOf("NCF") > -1) $('#chkNCFEdit').prop('checked', 'checked');
		
		/* This doesn't work in IE */		
		//if (type.includes("DNT") == true) $('#chkDNTEdit').prop('checked', 'checked');
		//if (type.includes("NCF") == true) $('#chkNCFEdit').prop('checked', 'checked');		
				
		var format = $('#rptFormatList').val().split(':')[rowIndex];		
		var formatSource= [ 'Excel', 'PDF'];
		for (fm=0; fm<formatSource.length; fm++){
			if (formatSource[fm] == format){
				$("#formatDropEdit").jqxDropDownList( 'selectIndex', fm );
				break;
			}
		}	
		
    });
        
    	
    /* DEACTIVATE BUTTON LOGIC */	
	$("#deactivate").on('click', function(event){ 
    	var selIndex = $('#reportTable').jqxGrid('getselectedrowindex');
    	if ( selIndex != -1 ) {
     		var selID = $('#reportTable').jqxGrid('getcellvalue', selIndex, 'hiddenID');
			var selName = $('#reportTable').jqxGrid('getcellvalue', selIndex, 'reportName');
    		var r = confirm('Are you sure you want to DEACTIVATE "'+selName+'"?');
    		if (r == true) {
        		$("#selID").val(selID) 
        		document.getElementById("deactivateForm").submit();
    		};
    	} else {
			$('#reportTable').jqxInput('focus');
    	};
	});	
});