<!--- 6/13 SW View Suggestion Menu--->

<!--- Query to check if suggestion was viewed before or not--->
<cfquery name="CheckView" datasource="#ds#">
if exists (select suggestionID from suggestionBox where (dateTimeViewed is null or dateTimeViewed = '') and 
suggestionID = #suggestionID#)
begin
   update suggestionBox
   set dateTimeViewed = #createodbcdatetime(now())# 
   where suggestionID = #suggestionID# 
end
</cfquery>

<!--- Query to check if suggestion was viewed or not--->
<cfquery name="CheckViewFlag" datasource="#ds#">
if exists (select suggestionID from suggestionBox where (flagViewed = 0) and 
suggestionID = #suggestionID#)
begin
   update suggestionBox
   set flagViewed = 1
   where suggestionID = #suggestionID# 
end
</cfquery>

<cfquery name="GetSuggestions" datasource="#ds#" >
	SELECT * 
	FROM  suggestionbox 
	WHERE suggestionID = #suggestionID#
</cfquery>

<!---Check to see if Send Email button was selected--->
<cfif isDefined("form.btnSendEmail")>
	<cfset url.suggestionID = form.suggestionID>

<!---send email alert--->
<cfset toList = "jmcmullen@copsmonitoring.com">
<cfmail from="ccroge@copsmonitoring.com" to="#toList#" subject="Employee Suggestion Box" username="system" password="V01c3">
This is to notify you of an Employee Suggestion that needs your attention:

Suggestion ID.................#GetSuggestions.suggestionID#
Suggestion Type..............#GetSuggestions.suggestionType#
Suggestion:

#GetSuggestions.suggestion#
</cfmail>

	<cfquery name="UpdateEmailFlag" datasource="#ds#" >
		update suggestionBox
		set flagEmailed = 1, dateTimeEmailed = #createodbcdatetime(now())#
		where suggestionID = #suggestionID# 
	</cfquery>
</cfif>

<!---Check to see if Go To Edit button was selected--->
<cfif isDefined("form.btnGoToEdit")>
	<cflocation url="editSuggestion.cfm?suggestionID=#suggestionID#">
</cfif>


<link rel="stylesheet" type="text/css" href="/styles.css">

<div align="center" class="normal" >
<cfform method="post" action="viewSuggestion.cfm">
<cfinput type = "hidden" name="suggestionID" value="#suggestionID#"/>

<table width="650" border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td class="highlightbar"><b>Employee Suggestion - View Entry</b></td>
	</tr>
	<tr>
	<td class="greyrowbottomnopadding">
	<table border="0" cellpadding="5" cellspacing="0">
		<td nowrap="nowrap"><b>Date/Time Created:</b></td>
		<cfoutput query="GetSuggestions">
		<td>
			#dateformat(GetSuggestions.dateTimeCreated,'mm/dd/yyyy')# #timeformat(GetSuggestions.dateTimeCreated,'hh:mm tt')#		</td>
		<tr>
			<td nowrap="nowrap"><b>Spoke with Manager:</b></td>
			<td>
			<cfif GetSuggestions.talkedToManager is 1>YES<cfelse>NO</cfif>
			</td>
		</tr>
		<tr>
		<td nowrap="nowrap"><b>Suggestion Number:</b></td>
			<td>
				#GetSuggestions.suggestionID#			</td>
		</tr>
		<tr>	
			<td width="131" nowrap="nowrap"><b>Employee Name:</b></td>
				<td width="397">
				#GetSuggestions.empFirstName# #GetSuggestions.empLastName#				</td>
		</tr>
			<td nowrap="nowrap"><b>Employee Number:</b></td>
			<td>
				#GetSuggestions.empNumber#			</td>
		<tr>
			<td nowrap="nowrap"><b>Employee Location:</b></td>
			<td>
				#GetSuggestions.location#			</td>
		</tr>
		<tr>
		<td nowrap="nowrap"><b>Suggestion Type:</b></td>
			<td>
				#GetSuggestions.suggestionType#			</td>
		</tr>
		<tr>
		<td valign="top" nowrap="nowrap"><b>Suggestion:</b></td>
			<td colspan="2">
			   <p>#GetSuggestions.suggestion#</p>
			</td>
		</tr>
		<tr>
		<td nowrap="nowrap"><b>Published:</b></td>
			<td>
				<cfif GetSuggestions.flagApproved is 0>
					No
				<cfelseif GetSuggestions.flagApproved is 1>
					Yes
				</cfif>
			</td>
		</tr>
		<tr>	
			<td nowrap="nowrap"><b>Emailed:</b></td>
			<td>
				<cfif GetSuggestions.flagEmailed is 0>
					No
				<cfelseif GetSuggestions.flagEmailed is 1>
					Yes
				</cfif>
			</td>
		</tr>
		<tr>
			<td nowrap="nowrap"><b>Date/Time Viewed:</b></td>
				<td>
					#dateformat(GetSuggestions.dateTimeViewed,'mm/dd/yyyy')# #timeformat(GetSuggestions.dateTimeViewed,'hh:mm tt')#				</td>
		</tr>
		<tr>
			<td nowrap="nowrap"><b>Response By:</b></td>
				<td>
					<p>#GetSuggestions.suggResponseName#</p>
				</td>
		</tr>
		<tr>
			<td nowrap="nowrap"><b>Response:</b></td>
				<td>
					<p>#GetSuggestions.suggResponse#</p>
				</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2">
				<cfinput type="submit" name="btnSendEmail" id="Send Email" value="Send Email"> 
				<cfinput type="submit" name="btnGoToEdit" id="Go To Edit" value="Go To Edit"> 
			</td>
		</tr>
		</cfoutput>
	</table>	
	</td>
	</tr>
</table>
</cfform>

<br>
<a style="text-decoration:underline;" href="index.cfm">Return to Employee Suggestion Maintenance Menu</a>
<br />

</div>
