<!--- 06/2013 : SW : Employee Suggestion Box --->

<!---THIS IS THE REAL WEBSITE TO POINT TO --->
<!---http://www.copsmonitoring.com/suggest/--->

<link rel="stylesheet" type="text/css" href="/styles.css">
<cfparam name="form.searchStatus" default="">
<cfparam name="form.searchKeyword" default="">
<cfparam name="form.searchBeginDate" default="">
<cfparam name="form.searchEndDate" default="">
<cfparam name="form.searchApproved" default="0">
<cfparam name="form.searchEmailed" default="">
<cfparam name="form.suggestion" default="">
<cfparam name="form.suggdatetime" default="">
<cfparam name="form.empName" default=""> 
<cfparam name="form.Approved" default="">
<cfparam name="form.emailed" default="">
<cfparam name="form.suggestionid" default="">
<cfparam name="form.empNumber" default="">
<cfparam name="form.flagViewed" default="">
<cfparam name="newEndDate" default="">
<cfparam name="errorList" default="">

<!--- convert URL variables to form variables --->
<cfif isDefined("url.searchStatus")>
	<cfset form.searchStatus = url.searchStatus>
</cfif>
<cfif isDefined("url.searchKeyword")>
	<cfset form.searchKeyword = url.searchKeyword>
</cfif>
<cfif isDefined("url.searchStartDate")>
	<cfset form.searchStartDate = url.searchStartDate>
</cfif>
<cfif isDefined("url.searchEndDate")>
	<cfset form.searchEndDate = url.searchEndDate>
</cfif>
<cfif isDefined("url.searchApproved")>
	<cfset form.searchApproved = url.searchApproved>
</cfif>
<cfif isDefined("url.searchEmailed")>
	<cfset form.searchEmailed = url.searchEmailed>
</cfif>

<!--- validate form stuff --->
<cfif trim(form.searchBeginDate) is not "" AND trim(form.searchEndDate) is not "">
	<cfif datecompare(form.searchBeginDate,form.searchEndDate) is 1>
		<cfset errorList = listappend(errorList,"The beginning date cannot be later than the ending date.","~")>
	</cfif>
</cfif>

<cfform method="post" action="index.cfm">

<br />

<div align="center">

<!--- output error messages --->
<cfif trim(errorList) is not "">
	<p>
	<div align="left" style="width:400px; background-color:#FF0000; color:#FFFFFF; font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; padding:5px; border:1px #000000 solid;">
	There are errors with your request:<br />
	<br />
	<cfloop list="#errorList#" index="listItem" delimiters="~">
		<cfoutput>
		<li>#listItem#</li>
		</cfoutput>
	</cfloop>
	</div>
	</p>
</cfif>

<!--- start of search box --->
<div class="normal" style="width:600px; position:relative;z-index:1;">
	<table width="600" border="0" cellspacing="0" cellpadding="5">
	    <tr>
            <td class="highlightbar"><b>Employee Suggestion Search Function</b></td>
        </tr>
        <tr>
            <td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td width="16%" nowrap="nowrap"><b>Status:</b></td>
				<td width="84%">
					<cfselect name="searchStatus">
						<option value="">All</option>
						<option <cfif form.searchStatus is 1>selected</cfif> value="1">Read</option>
						<option <cfif form.searchStatus is 0>selected</cfif> value="0">Unread</option>
					</cfselect>
				</td>
			</tr>
			<tr>
				<td nowrap="nowrap"><b>Key Word Search: </b></td>
				<td><cfinput type="text" name="searchKeyword" value="#form.searchKeyword#" style="width:300px"></td>
			</tr>
			<tr>
				<td nowrap="nowrap"><b>Date Created: </b></td>
				<td class="nopadding">
					<table border="0" cellspacing="0" cellpadding="5">
						<tr>
							<td>Beginning Date:</td>
							<td><cfinput type="datefield" name="searchBeginDate" style="vertical-align:middle; width:75px;" value="#form.searchBeginDate#"> </td>
							<td>&nbsp;</td>
							<td>Ending Date:</td>
							<td><cfinput style="vertical-align:middle; width:75px;" type="datefield" name="searchEndDate" value="#form.searchEndDate#"></td>
						</tr>
					</table>
				</td>
				</tr>
				<tr>
					<td nowrap="nowrap"><b>Published:</b></td>
					<td>
						<cfselect name="searchApproved">
							<option value="">All</option>
							<option <cfif form.searchApproved is 1>selected</cfif> value="1">Yes</option>
							<option <cfif form.searchApproved is 0>selected</cfif> value="0">No</option>
						</cfselect>
					</td>
				</tr>	
				<tr>
					<td nowrap="nowrap"><b>Emailed:</b></td>
					<td>
						<cfselect name="searchEmailed">
							<option value="">All</option>
							<option <cfif form.searchEmailed is 1>selected</cfif> value="1">Yes</option>
							<option <cfif form.searchEmailed is 0>selected</cfif> value="0">No</option>
						</cfselect>
					</td>
				</tr>	
				<tr>
					<td colspan="2">
					<cfinput type="submit" name="btnSearch" id="Start Search" value="Start Search"> 
					<cfinput type="button" name="btnClearSearch" onClick="document.location = 'index.cfm';" id="Clear Search" value="Clear Search">
					</td>
				</tr>
			</table>
			</td>
		</tr>		
	</table>
</div>

</cfform>

<!--- keyword search first then other search from keyword results --->
<cfset suggestionIDList = "">
<cfif trim(form.searchKeyword) is not "">
	<cfsearch name = "getKeywordResults" collection = "intranet_suggestionbox" criteria = "#form.searchKeyword#">
	<cfset suggestionIDLIst = valuelist(getKeywordResults.key)>
</cfif>

<cfif #form.searchBeginDate# is not "">
	<cfset newEndDate = DateAdd ("d",1,form.searchEndDate)>
</cfif>

<!--- Get Suggestions --->
<cfquery name="GetSuggestions" datasource="#ds#" >
	SELECT * 
	FROM  suggestionbox 
	WHERE flagDeleted = 0
	<cfif form.searchStatus is not "">
		and flagViewed = #form.searchStatus# 
	</cfif>
	<cfif form.searchApproved is not "">
		and flagApproved = #form.searchApproved# 
	</cfif>
	<cfif form.searchEmailed is not "">
		and flagEmailed = #form.searchEmailed#
	</cfif>
	<cfif form.searchBeginDate is not "">
	 and dateTimeCreated >= #CreateODBCDateTime(form.searchBeginDate)#
	</cfif>
	<cfif form.searchEndDate is not "">
	 and dateTimeCreated <= #CreateODBCDateTime(form.searchEndDate)#
	</cfif>
	<cfif trim(suggestionIDList) is not "">
		and suggestionID IN (#suggestionIDList#) 
	</cfif>
	order by suggestionbox.dateTimeCreated asc 
</cfquery>

<!--- Suggestions Menu --->
<br />
<br />
<div align="center" class="normal" style="position:relative;z-index:0">
    <table width="803" border="0" cellpadding="5" cellspacing="0">
		<tr>
            <td width="793" class="highlightbar"><b>Employee Suggestion - Maintenance Menu</b></td>
        </tr>
	  <tr>
            <td class="greyrowbottomnopadding">
                <table width="99%" border="0" cellpadding="5" cellspacing="0">
                    <tr>
                        <td width="3%" class="linedrowrightcolumn">&nbsp;</td>
                        <td nowrap="nowrap" class="linedrowrightcolumn" align="center"><b>Action</b></td>
						<td width="16%" align="center" nowrap="nowrap" class="linedrowrightcolumn"><b>Suggestion #</b></td>
                        <td width="16%" align="center" nowrap="nowrap" class="linedrowrightcolumn"><b>Created</b></td>
						<td width="9%" align="center" nowrap="nowrap" class="linedrowrightcolumn"><b>Employee</b></td>
						<td width="9%" align="center" nowrap="nowrap" class="linedrowrightcolumn"><b>Employee # </b></td>
						<td width="9%" align="center" nowrap="nowrap" class="linedrowrightcolumn"><b>Location</b></td>
                        <td width="37%" align="center" nowrap="nowrap" class="linedrowrightcolumn"><b>Suggestion</b></td>
						<td width="9%" align="center" nowrap="nowrap" class="linedrowrightcolumn"><b>Published</b></td>
                        <td width="8%" align="center" nowrap="nowrap" class="linedrow"><b>Emailed</b></td>
                    </tr>
					<cfoutput query="GetSuggestions">
					<tr>
						<td align="center" class="linedrowrightcolumn">
							<cfif GetSuggestions.flagViewed is 0>
								<img border="0" src="/images/unread.gif" />
							<cfelseif GetSuggestions.flagViewed is 1>
								<img border="0" src="/images/read.gif" /></a>
							</cfif>
						</td>
						<td align="center" nowrap="nowrap" class="linedrowrightcolumn"><a href="editSuggestion.cfm?suggestionID=#GetSuggestions.suggestionID#"><img src="/images/edit.gif" alt="Edit" width="16" height="16" border="" />&nbsp;</a>
						<a onclick="return confirm('Are you sure you want to delete this?');" href="deleteSuggestion.cfm?suggestionID=#GetSuggestions.suggestionID#"><img src="/images/delete.gif" alt="Delete" width="16" height="16" border="" />&nbsp;</a>
						<a href="viewSuggestion.cfm?suggestionID=#GetSuggestions.suggestionID#"><img src="/images/view.gif" alt="View" width="16" height="16" border=""/></a></td>
							<td nowrap="nowrap" class="linedrowrightcolumn">#suggestionID#</td>							
							<td nowrap="nowrap" class="linedrowrightcolumn">#dateformat(GetSuggestions.dateTimeCreated,'mm/dd/yyyy')# #timeformat(GetSuggestions.dateTimeCreated,'hh:mm tt')#&nbsp;</td>
							<td nowrap="nowrap" class="linedrowrightcolumn">#GetSuggestions.empFirstName# #GetSuggestions.empLastName#&nbsp;</td>
							<td nowrap="nowrap" class="linedrowrightcolumn">#empNumber#&nbsp;</td>
							<td nowrap="nowrap" class="linedrowrightcolumn">#location#&nbsp;</td>
							<td nowrap="nowrap" class="linedrowrightcolumn">#left(suggestion,100)#&nbsp;</td>
							<td nowrap="nowrap" class="linedrowrightcolumn">
							<cfif GetSuggestions.flagApproved is 0>
								No
							<cfelseif GetSuggestions.flagApproved is 1>
								Yes
							</cfif>
							</td>
							<td nowrap="nowrap" class="linedrowrightcolumn"> 
							<cfif GetSuggestions.flagEmailed is 0>
								No
							<cfelseif GetSuggestions.flagEmailed is 1>
								Yes
							</cfif>
							</td>
					</tr>
					</cfoutput>
				</table>
			</td>
		</tr>
	</table>
	<br />
	<a class="normal" style="text-decoration:underline;" href="/index.cfm">Return to Intranet</a>
</div>	


		
	
