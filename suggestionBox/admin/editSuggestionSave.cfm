<!---  7/13 SW Suggestion Box Edit Save Data --->
<cfset dateTimeEdited = createodbcdatetime(now())>

<cfparam name="form.suggestionType" default="">
<cfparam name="form.suggestion" default="">
<cfparam name="form.suggestionNumber" default="0">
<cfparam name="form.suggestionid" default="0">
<cfparam name="form.dateTimeEdited" default="">
<cfparam name="form.flagApproved" default="0">
<cfparam name="form.employeeFirstName" default="">
<cfparam name="form.employeeLastName" default="">
<cfparam name="form.empNumber" default="">
<cfparam name="form.suggestionid" default="">
<cfparam name="form.location" default="">
<cfparam name="form.flagEmailed" default="0">
<cfparam name="form.dateTimeApproved" default="">

<cfif flagApproved is "1">
	<cfset dateTimeApproved = #createodbcdatetime(now())#>
</cfif> 

<cfquery name = "UpdateDb" datasource="#ds#">
	UPDATE suggestionbox 
	SET empFirstname   = '#trim(form.employeeFirstName)#',
		empLastname    = '#trim(form.employeeLastName)#',
		empNumber      = '#trim(form.empNumber)#',
		location	   = '#trim(form.location)#',
		suggestionType = '#trim(form.suggestionType)#',
		suggestion     = '#trim(form.suggestion)#',
		flagApproved   = '#trim(form.flagApproved)#',
		dateTimeEdited = #trim(dateTimeEdited)#,
		suggResponse   = '#trim(form.suggResponse)#',
		<cfif trim(dateTimeApproved) is not "">
			dateTimeApproved = #trim(dateTimeApproved)#, 
		</cfif>
		suggResponseName = '#trim(suggResponseName)#'		
	WHERE suggestionID = #form.suggestionID#
</cfquery>


<!--- Return back to Main page--->
<cflocation url="index.cfm">
