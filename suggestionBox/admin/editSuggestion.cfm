<!--- 6/13 SW Edit Suggestion Menu--->
<link rel="stylesheet" type="text/css" href="/styles.css">

<cfif isDefined("form.btnExit")>
	<cflocation url="index.cfm?suggestionID=#suggestionID#">
</cfif>

<cfif isDefined("form.btnSave")>
	<cfset url.suggestionID = form.suggestionID>
	<cfset flagViewed = "1">
	<cfinclude template="editSuggestionSave.cfm">
</cfif>

<!---Check to see if Save/Send Email button was selected--->
<cfif isDefined("form.btnSaveSend")>
	<cfset url.suggestionID = form.suggestionID>
	<cfset flagViewed = "1">
	<cfinclude template="editSuggestionSave.cfm">
</cfif>

<!---Get Suggestion selected    --->
<cfquery name="GetSuggestions" datasource="#ds#" >
	SELECT * 
	FROM  suggestionbox 
	WHERE suggestionID = #suggestionID#
</cfquery>

<!--- mark viewed --->
<cfquery name="markviewed" datasource="#ds#">
	update suggestionbox 
	set flagViewed = 1, dateTimeViewed = GETDATE() 
	where suggestionID = #suggestionID# 
</cfquery>

<div align="center" class="normal">
<cfform method="post" action="editSuggestion.cfm">
<cfinput type = "hidden" name="suggestionID" value="#suggestionID#"/>

<cfparam name="form.suggResponse" default="#GetSuggestions.suggResponse#">
<cfparam name="form.suggResponseName" default="#GetSuggestions.suggResponseName#">
<cfparam name="form.suggestion" default="#GetSuggestions.suggestion#">
<cfparam name="form.suggestionNumber" default="#GetSuggestions.suggestionID#">
<cfparam name="form.suggestionType" default="#GetSuggestions.suggestionType#">
<cfparam name="form.dateTimeEdited" default="">
<cfparam name="form.flagApproved" default="#GetSuggestions.flagApproved#">
<cfparam name="form.Emailed" default="#GetSuggestions.dateTimeEmailed#">
<cfparam name="form.employeeFirstName" default="#GetSuggestions.empFirstName#">
<cfparam name="form.employeeLastName" default="#GetSuggestions.empLastName#">
<cfparam name="form.empNumber" default="#GetSuggestions.empNumber#">
<cfparam name="form.suggestionid" default="#GetSuggestions.suggestionID#">
<cfparam name="form.location" default="#GetSuggestions.location#">
<cfparam name="form.flagEmailed" default="#GetSuggestions.flagEmailed#">
<cfparam name="form.suggResponse" default="#GetSuggestions.suggResponse#">
<cfparam name="form.suggResponseName" default="#GetSuggestions.suggResponseName#">

<!---<cfdump var="#GetSuggestions#">--->
	
<table border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td class="highlightbar"><b>Employee Suggestion - Edit Entry</b></td>
	</tr>
	<tr>
	<td class="greyrowbottomnopadding">
	<table border="0" cellpadding="5" cellspacing="0">
	<cfoutput>
	<tr>
		<td nowrap="nowrap"><b>Suggestion Number:</b></td>
		<td> 
			<cfoutput>#form.suggestionNumber#</cfoutput>
		</td>
	</tr>
	<tr>	
		<td valign="top" nowrap="NOWRAP"><b>Suggestion Type:</b>&nbsp;</td>
		<td class="nopadding">
			<input type="checkbox" <cfif listfindnocase(trim(form.suggestionType), "Improve Workplace") is not 0> checked </cfif> name="suggestionType" id="workplace" value="Improve Workplace" /> Improve Workplace<br />
			<input type="checkbox" <cfif listfindnocase(trim(form.suggestionType), "Improve Dealers") is not 0> checked </cfif> name="suggestionType" id="dealers" value="Improve Dealers" /> Improve Dealers<br />
			<input type="checkbox" <cfif listfindnocase(trim(form.suggestionType), "Improve Customers") is not 0> checked </cfif> name="suggestionType" id="customers" value="Improve Customers" /> Improve Customers<br />
		</td>
	</tr>
			<tr>
					<td nowrap="nowrap"><b>Employee First Name:</b></td>
					<td>
					<cfinput type="text" name="employeeFirstName" id="employeeFirstName" value="#form.employeeFirstName#" style="width:300px">
					</td>
				</tr>
				<tr>
				<td nowrap="nowrap"><b>Employee Last Name:</b></td>
				<td>
					<cfinput type="text" name="employeeLastName" id="employeeLastName" value="#form.employeeLastName#" style="width:300px">
					</td>
				</tr>
				<tr>
					<td nowrap="nowrap"><b>Employee Number:</b></td>
					<td>
						<cfinput type="text" name="empNumber" id="empNumber" value="#form.empNumber#" style="width:50px">
					</td>
				</tr>
				<tr>
					<td nowrap="nowrap"><b>Location:</b></td>
					<td>
					<cfselect name="location" multiple="no" required="yes" message="Location is required">
						<option <cfif trim(form.location) is "AZ"> selected </cfif> value="AZ">AZ</option>
						<option <cfif trim(form.location) is "FL"> selected </cfif> value="FL">FL</option>
						<option <cfif trim(form.location) is "MD"> selected </cfif> value="MD">MD</option>
						<option <cfif trim(form.location) is "NJ"> selected </cfif> value="NJ">NJ</option>
						<option <cfif trim(form.location) is "TN"> selected </cfif> value="TN">TN</option>
						<option <cfif trim(form.location) is "TX"> selected </cfif> value="TX">TX</option>
					</cfselect>
					</td>
				</tr>
			<tr>
				<td colspan="2" nowrap="nowrap"><b>Suggestion:</b>&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2" nowrap="nowrap">Enter starts a new paragraph, shift + enter goes to next line</td>
			</tr>
			<tr>
				<td colspan="2">
					<cftextarea name="suggestion" value="#form.suggestion#" rows="5" required="yes" id="suggestion" style="width:500px" message="Suggestion is required."></cftextarea>
				</td>
			</tr>
			<tr>
				<td nowrap="nowrap"><b>Publish (Y/N):</b></td>
				<td>
					<cfselect onChange="this.form.submit();" name="flagApproved" multiple="no" required="yes" message="Approved is required">
					<option <cfif form.flagApproved is "1"> selected </cfif> value="1">Yes</option>
					<option <cfif form.flagApproved is "0"> selected </cfif> value="0">No</option>
					</cfselect>
					<cfif flagApproved is 1>
						<cfoutput>#dateformat(GetSuggestions.dateTimeApproved,'mm/dd/yyyy')# #timeformat(GetSuggestions.dateTimeApproved,'hh:mm tt')#</cfoutput>
					</cfif>
				</td>
			</tr>
			<tr>
				<td nowrap="nowrap"><b>Emailed (Y/N):</b></td>
				<td>
				<cfif flagEmailed is 1>
					Yes
					<cfoutput>#dateformat(GetSuggestions.dateTimeEmailed,'mm/dd/yyyy')# #timeformat(GetSuggestions.dateTimeEmailed,'hh:mm tt')#</cfoutput>
				<cfelse>
					No
				</cfif>
				</td>
			</tr>
			<tr>
				<td nowrap="nowrap"><b>Response By:</b></td>
					<td>
					<cfinput type="text" name="suggResponseName" id="suggResponseName" value="#form.suggResponseName#" style="width:300px">
					</td>
			</tr>
			<tr>
				<td colspan="2" nowrap="nowrap"><b>Suggestion Response:</b>&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2">
					<cftextarea name="suggResponse" value="#form.suggResponse#" rows="5" required="no" id="suggResponse" style="width:500px"></cftextarea>
				</td>
			</tr>
		<tr>
			<td colspan="2">
			<cfinput type="submit" name="btnSave" id="Save Info" value="Save Updates" style="width:100px"> &nbsp;
			<cfinput type="submit" name="btnExit" id="Exit" value="Exit" style="width:100px"> &nbsp;
			</td>
		</tr>
	  </cfoutput>
	</table>	
	</td>
	</tr>
</table>
</cfform>

<br>
<a style="text-decoration:underline;" href="index.cfm">Return to Employee Suggestion Maintenance Menu</a>
<br />
</div>








