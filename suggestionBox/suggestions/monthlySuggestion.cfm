<style type="text/css">
	TABLE {font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px;}
</style>

<!---<cfdump var="#searchMonth#">
<cfdump var="#searchYr#">--->

<cfset currMth = "#searchMonth#/1/#searchYr#">
<cfset nextMonth = DateAdd("m",1,currMth)>
<cfset #nextMonth2# = #dateformat(#nextMonth#,'mm/dd/yyyy')#>

<!---Get Suggestions   --->
<cfquery name="GetSuggestions" datasource="#ds#" >
	SELECT * 
	FROM  suggestionbox 
	WHERE dateTimeApproved >= '#currMth#'
	AND dateTimeApproved < '#nextMonth2#'
	AND flagDeleted = 0
	AND flagApproved = 1
</cfquery>

<div align="center">
<table width="955" border="0" cellspacing="0" cellpadding="0" background="images/bg.jpg">
    <tr>
        <td><img src="images/top.jpg" width="955" height="140"></td>
    </tr>
	 <tr>
        <td align="center">
			<h1><i><cfoutput>#MonthAsString(searchMonth)# #searchYr# Suggestions</cfoutput></i></h1>
        </td>
    </tr>
    <tr>
        <td align="center" valign="top">
		<table width="850" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td style="padding:0px;">
				<cfif getSuggestions.recordcount gt 0>
					<cfoutput query="GetSuggestions">
					<table width="100%" border="0" cellspacing="0" cellpadding="5">
						<tr>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>
								<p>#GetSuggestions.currentRow#. #replace(GetSuggestions.suggestion, chr(13), "<br>", "all")#</p>
							</td>
						</tr>
						<cfif #GetSuggestions.suggResponse# is not "">
							<tr>
								<td><b><p>#GetSuggestions.suggResponseName#'s Response: #GetSuggestions.suggResponse#</p></b> </td>
							</tr>
						</cfif>
					</table>
					</cfoutput>
				<cfelse>
					<table width="100%" border="0" cellspacing="0" cellpadding="5">
						<tr>
							<td align="center"><br />Sorry, there were no suggestions for this month.</td>
						</tr>
					</table>
				</cfif>
				</td>
			</tr>
		</table>
		</td>
    </tr>
    <tr>
        <td><img src="images/bottom.jpg" width="955" height="140" /></td>
    </tr>
</table>

<br>
<a class="normal" style="text-decoration:underline;" href="searchSuggestion.cfm">Return to Employee Suggestion Search Page</a>
<br />
</div>



