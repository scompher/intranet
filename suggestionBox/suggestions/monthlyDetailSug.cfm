<style type="text/css">
	TABLE {font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px;}
</style>

<!---<td><cfoutput><a href="monthlySuggestion.cfm?suggestionID=#GetSuggestions.suggestionID#">#MonthAsString(m)# #currYr# </a></cfoutput></td>--->
<!---Get Suggestion selected    --->
<cfquery name="GetSuggestions" datasource="#ds#" >
	SELECT * 
	FROM  suggestionbox 
	WHERE suggestionID = #suggestionID#
</cfquery>

<div align="center">
<table width="955" border="0" cellspacing="0" cellpadding="0" background="images/bg.jpg">
    <tr>
        <td><img src="images/top.jpg" width="955" height="140"></td>
    </tr>
    <tr>
        <td align="center" valign="top">
		<table width="850" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td style="padding:0px;">
				<!--- content here --->
				<cfoutput query="GetSuggestions">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<cfif isDate(datetimeapproved)>
					 <tr>
						<td align="center">
							<h1><i>A <cfoutput>#MonthAsString(dateformat(datetimeapproved,'mm'))# #dateformat(datetimeapproved,'yyyy')# Suggestion</cfoutput></i></h1>
						</td>
					</tr>
					</cfif>
					<tr>
						<td>
							<p>#GetSuggestions.suggestion#</p>   
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<cfif #GetSuggestions.suggResponse# is not "">
						<tr>
							<td><p>#GetSuggestions.suggResponseName#'s Response: #GetSuggestions.suggResponse#</p> </td>
						</tr>
					</cfif>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
					  <!---  <td><i><a href="javascript:history.go(-1);">Return to previous page</a></i> </td>--->
					  <td align="center"><i><a style="text-decoration:underline;" href="searchSuggestion.cfm?searchKeyword=#searchKeyword#">Return to Employee Suggestion Search Page</a></i></td>
					</tr>
				</table>
				</cfoutput>
				<!--- content here --->
				</td>
			</tr>
		</table>
		</td>
    </tr>
    <tr>
        <td><img src="images/bottom.jpg" width="955" height="140" /></td>
    </tr>
</table>
</div>

<!---<br>
<a style="text-decoration:underline;" href="searchSuggestion.cfm">Return to Employee Suggestion Search Page</a>
<br />--->
