<style type="text/css">
	TABLE {font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px;}
	a:link {color:#0000FF} 
	a:visited {color:#0000FF} 
	a:hover {color:#0000FF} 
	a:active {color:#0000FF} 
</style>

<cfparam name="searchMonth" default="">
<cfparam name="searchYr" default="">
<cfparam name="currYr" default="#dateformat(now(),'yyyy')#" >
<cfparam name="searchKeyword" default="">

<cfset endDate = "#dateformat(now(),'mm')#/#daysinmonth(now())#/#dateformat(now(),'yyyy')#">
<cfset sd = dateadd("m",-12,endDate)>
<cfset startDate = "#dateformat(sd,'mm')#/1/#dateformat(sd,'yyyy')#">

<cfset suggestionIDList = "">

<cfif trim(searchKeyword) is not "">
	<cfsearch name = "getKeywordResults" collection = "intranet_suggestionbox" criteria = "#searchKeyword#">
	<cfset suggestionIDLIst = valuelist(getKeywordResults.key)>

	<cfquery name="GetSuggestions" datasource="#ds#">
		SELECT * 
		FROM  suggestionbox 
		where 1=1 
		<cfif trim(suggestionIDList) is not "">
			and suggestionID IN (#suggestionIDList#) 
		</cfif>
	</cfquery>
</cfif>


<div align="center">
<table width="955" border="0" cellspacing="0" cellpadding="0" background="images/bg.jpg">
    <tr>
        <td><img src="images/top.jpg" width="955" height="140"></td>
    </tr>
    <tr>
        <td align="center" valign="top">
		<table width="850" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td>
				<!--- content here --->
					<table width="100%" border="0" cellspacing="0" cellpadding="5">
						<tr>
							<td align="center"><h1><i>Suggestion Box</i></h1></td>
						</tr>
						<tr>
							<td align="center">
								<table border="0" cellspacing="0" cellpadding="5">
									<tr>
										<td><b>View by Month</b></td>
									</tr>
									<!---
									<cfloop from="1" to="12" index="m">
										<tr>
											<td><cfoutput><a href="monthlySuggestion.cfm?searchMonth=#m#&searchYr=#currYr#"> #MonthAsString(m)# #currYr# </a></cfoutput></td>
										</tr>
									</cfloop>
									--->
									<cfloop from="0" to="12" index="i">
										<cfset currDate = dateadd('m',i,startDate)>
										<cfoutput>
										<tr>
											<td><a href="monthlySuggestion.cfm?searchMonth=#dateformat(currDate,'m')#&searchYr=#dateformat(currDate,'yyyy')#">#monthAsString(dateformat(currDate,'mm'))# #dateformat(currDate,'yyyy')#</a></td>
										</tr>
										</cfoutput>
									</cfloop>
								</table>
							</td>
						</tr>
						<tr>
							<td align="center">
								<table width="80%" border="0" cellspacing="0" cellpadding="5">
									<tr>
										<td>&nbsp;</td>
									</tr>
									<form method="post" action="searchSuggestion.cfm">
									<tr>
										<td nowrap="nowrap" valign="middle">
											<b>Search by Keyword(s):</b>
											<input type="text" name="searchKeyword" maxlength="255" value="<cfoutput>#searchKeyword#</cfoutput>" />
											<input type="submit" name="btnSearchKeywords" value="Search" />
										</td>
									</tr>
									</form>
									<cfif trim(searchKeyword) is not "">
									<tr>
										<td align="center">
											<h3><i>Search Results</i></h3>
										</td>
									</tr>
									<tr>
										<cfoutput query="GetSuggestions">
										<td>#GetSuggestions.currentRow#. #left(GetSuggestions.suggestion,100)#...<a href="monthlyDetailSug.cfm?suggestionID=#GetSuggestions.suggestionID#&searchKeyword=#searchKeyword#"><i>Click here to read more</i> </a></td>
									</tr>
									</cfoutput>
									</cfif>
								</table>
							</td>
						</tr>
					</table>
				<!--- content here --->
				</td>
			</tr>
		</table>
		</td>
    </tr>
    <tr>
        <td><img src="images/bottom.jpg" width="955" height="140"></td>
    </tr>
</table>

<br>
	<a class="normal" style="text-decoration:underline;" href="/index.cfm">Return to Intranet</a>
<br />

</div>

