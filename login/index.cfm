
<cfif isdefined("cookie.adminlogin")><cflocation url="/index.cfm"></cfif>

<link rel="stylesheet" type="text/css" href="/styles.css">

<body onLoad="document.loginform.username.focus();">

<script language="JavaScript" type="text/JavaScript">
function checkform(frm) {
	if (frm.username.value == '') {
		alert('Please enter a username');
		frm.username.focus();
		return false;
	}
	if (frm.password.value == '') {
		alert('Please enter a password');
		frm.password.focus();
		return false;
	}
	return true;
}
</script>

<div align="center">
<form method="post" action="/login/login.cfm" name="loginform">
<table border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td colspan="2" align="center"><img src="/Images/cops-logo.jpg"><br><br><b>Intranet Login</b><br></td>
	</tr>
<cfif isDefined("url.m")>
	<cfoutput>
    <tr>
        <td colspan="2" align="center"><b style="color:ff0000">#m#</b></td>
    </tr>
	</cfoutput>
</cfif>
    <tr>
        <td width="80"><b>Username:</b></td>
        <td width="242"><input name="username" style="width:200px" type="text" class="normal" tabindex="1"></td>
    </tr>
    <tr>
        <td><b>Password:</b></td>
        <td valign="middle">
		<input style="vertical-align:middle; width:200px" name="password" type="password" class="normal" tabindex="2">
		</td>
    </tr>
	<tr>
		<td colspan="2">
			<a style="text-decoration:underline; color:#FF0000; font-size:12px;" href="forgotpw.cfm"><b>Forgot your username or password? CLICK HERE!</b></a>
		</td>
	</tr>
    <tr>
    	<td colspan="2" align="center"><input type="checkbox" name="rememberInfo" value="1" tabindex="3"> Remember my login information</td>
   	</tr>
    <tr align="center">
        <td colspan="2"><input tabindex="4" onClick="return checkform(this.form);" name="Submit" type="submit" class="normal" value="Log In"></td>
    </tr>
</table>
</form>
</div>
</body>
