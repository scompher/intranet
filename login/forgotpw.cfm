
<cfif isDefined("form.btnSendPW")>

	<!--- get pw --->
	<cfquery name="getpw" datasource="#ds#">
		select * from admin_users
		where email = '#form.email#'
	</cfquery>

	<cfif getpw.recordcount IS 0>
		<cfset msg = "The email address you've provided is incorrect.">
	<cfelse>
	
<cfmail from="pgregory@copsmonitoring.com" to="#getpw.email#" subject="COPS Monitoring Intranet Lost Password Reminder" username="copalink@copsmonitoring.com" password="copsmoncal">
Here is your login information for the COPS Monitoring Intranet.

COPS Intranet Login URL : #request.appurl#login/index.cfm

Username : #getpw.username#
Password : #getpw.password#

</cfmail>
	
		<cfset msg = "Your password has been sent.">	
	</cfif>

	<div align="center">
		<table border="1" cellpadding="5" cellspacing="0">
			<tr>
				<td colspan="2" align="center" bgcolor="FFFFCC"><b>Forgotten Password</b></td>
			</tr>
			<cfoutput>
			<tr>
				<td colspan="2" align="center">#msg#</td>
			</tr>
			</cfoutput>
			<tr>
				<td align="center" colspan="2" bgcolor="DDDDDD"><a href="index.cfm">Return to Login</a></td>
			</tr>
		</table>
	</div>

<cfelse>

<div align="center">
	<form method="post" action="forgotpw.cfm">
	<table border="1" cellpadding="5" cellspacing="0">
		<tr>
			<td colspan="2" align="center" bgcolor="FFFFCC"><b>Forgotten Password</b></td>
		</tr>
		<tr>
			<td><b>Email Address:</b></td>
			<td><input type="text" name="email" size="50" maxlength="100" class="normal"></td>
		</tr>
		<tr>
			<td colspan="2" align="center"><input type="submit" name="btnSendPW" value="Send Me My Password"></td>
		</tr>
		<tr>
			<td align="center" colspan="2" bgcolor="DDDDDD"><a href="index.cfm">Return to Login</a></td>
		</tr>
	</table>
	</form>
</div>

</cfif>
