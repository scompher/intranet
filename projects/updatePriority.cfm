<cfset id = attributes.projectid>
<cfset newp = attributes.newPriority>
<cfset ds = attributes.datasource>

<cfquery name="getOldP" datasource="#ds#">
select priority from projects_main
where projectid = #id# 
</cfquery>
<cfset oldP = getOldP.priority>

<cfquery name="updateP" datasource="#ds#">
begin
	update projects_main
	set priority = priority - 1
	where userid = #cookie.adminlogin# and priority > #oldp#
end
<cfif newp is not 0>
begin
	update projects_main
	set priority = priority + 1
	where userid = #cookie.adminlogin# and projectid <> #id# and priority >= #newp#
end
</cfif>
begin
	update projects_main
	set priority = #newp#
	where projectid = #id# and userid = #cookie.adminlogin#
end
</cfquery>

<cfset projectid = id>
<cfinclude template="updateLastUpdated.cfm">

