
<cfparam name="form.updateS" default="0">
<cfparam name="form.updateP" default="0">
<cfparam name="form.updateDate" default="0">

<cfif isdefined("form.btnSaveInfo")>
	<cfquery name="saveNew" datasource="#ds#">
		insert into projects_main (name, description, notes, priority, statusid, lastUpdated, userid) 
		values ('#form.newName#', '#form.newDescription#', '#form.newNotes#', #form.newPriority#, #form.newStatusID#, #createodbcdatetime(now())#, #cookie.adminlogin#)
	</cfquery>
	<cflocation url="index.cfm">
</cfif>

<cfif form.updateS is 1>
	<cf_updateStatus projectid="#form.update_projectid#" statusid="#form.update_statusid#" datasource="#ds#">
	<cflocation url="index.cfm">
</cfif>

<cfif form.updateP is 1>
	<cf_updatePriority projectid="#form.update_projectid#" newPriority="#form.update_priority#" datasource="#ds#">
	<cflocation url="index.cfm">
</cfif>

<cfquery name="getProjects" datasource="#ds#">
	select projects_main.*, projects_status.statusid
	from projects_main 
	left join projects_status on projects_main.statusid = projects_status.statusid 
	where userid = #cookie.adminlogin#
	order by priority asc
</cfquery>

<cfquery name="getStatus" datasource="#ds#">
	select * 
	from projects_status 
	order by statusid asc
</cfquery>

<link rel="stylesheet" type="text/css" href="../styles.css">

<script language="javascript1.2" type="text/javascript">
function modifyNameWin(projectid) {
	var notewin = window.open("modifyName.cfm?projectid=" + projectid,"notewin","status=0,toolbar=0,location=0,menubar=0,directories=0,resizeable=0,scrollbars=0,height=250,width=410");
	notewin.opener = self;
}

function modifyNoteWin(projectid) {
	var notewin = window.open("modifyNote.cfm?projectid=" + projectid,"notewin","status=0,toolbar=0,location=0,menubar=0,directories=0,resizeable=0,scrollbars=0,height=250,width=410");
	notewin.opener = self;
}

function modifyDescriptionWin(projectid) {
	var descwin = window.open("modifyDesc.cfm?projectid=" + projectid,"descwin","status=0,toolbar=0,location=0,menubar=0,directories=0,resizeable=0,scrollbars=0,height=250,width=410");
	descwin.opener = self;
}

function updateStatus(frm, projectid, statusid) {
	frm.update_projectid.value = projectid;
	frm.update_statusid.value = statusid;
	frm.updateS.value = 1;
	frm.submit();
}

function updatePriority(frm, projectid, priority) {
	frm.update_projectid.value = projectid;
	frm.update_priority.value = priority;
	frm.updateP.value = 1;
	frm.submit();
}
</script>

<cfquery name="getPriorities" datasource="#ds#">
	select priority from projects_main where statusid <> 3 and userid = #cookie.adminlogin#
</cfquery>

<div align="center" class="normal">
<table width="900" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>My Current Projects</b></td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
			<form method="post" action="index.cfm" name="mainform">
			<input type="hidden" name="update_projectid" value="0" />
			<input type="hidden" name="update_priority" value="0" />
			<input type="hidden" name="update_statusid" value="0" />
			<input type="hidden" name="updateS" value="0" />
			<input type="hidden" name="updateP" value="0" />
			<table width="100%" border="0" cellspacing="0" cellpadding="3">
				<tr>
					<td width="6%" align="center" nowrap class="headerbar">&nbsp;</td>
					<td width="6%" align="center" nowrap class="headerbar"><b>Priority</b></td>
					<td width="21%" align="center" nowrap class="headerbar"><b>Project Name</b> <span class="smalltable">(click to modify)</span></td>
					<td width="22%" align="center" nowrap class="headerbar"><b>Description </b><span class="smalltable">(click to modify)</span></td>
					<td width="28%" align="center" nowrap class="headerbar"><b>Notes </b><span class="smalltable">(click to modify)</span></td>
					<td width="12%" align="center" nowrap class="headerbar"><b>Status</b></td>
					<td width="11%" align="center" nowrap class="headerbar"><b>Last Updated</b></td>
				</tr>
				<cfoutput query="getProjects">
					<cfset projectstatus = getprojects.statusid>
					<cfif projectStatus is not 3>
					<tr>
						<td align="center" valign="top" class="linedrowrightcolumn"><a href="javascript: if (confirm('Are you sure you wish to delete this project?')) document.location='delete.cfm?projectid=#projectid#';"><img border="0" src="/images/delete.gif" alt="Delete Project" width="16" height="16" /></a></td>
						<td align="center" valign="top" class="linedrowrightcolumn">
							<select name="priority" onchange="updatePriority(this.form, #projectid#, this.options[this.selectedIndex].value);" >
								<cfloop from="1" to="#getpriorities.recordcount#" index="p">
									<option <cfif priority is p>selected</cfif> value="#p#">#p#</option>
								</cfloop>
							</select>
						</td>
						<td valign="top" class="linedrowrightcolumn"><a href="javascript:modifyNameWin(#projectid#);">#name#</a></td>
						<td valign="top" class="linedrowrightcolumn">
						<cfif trim(description) is "">
							<a href="javascript:modifyDescriptionWin(#projectid#);"><i>Click here to add/modify description</i></a>
						<cfelse>
							<a href="javascript:modifyDescriptionWin(#projectid#);">#replace(description, chr(13), "<br />", "all")#</a>
						</cfif>
						</td>
						<td valign="top" class="linedrowrightcolumn">
							<cfif trim(notes) is "">
								<a href="javascript:modifyNoteWin(#projectid#);"><i>Click here to add/modify notes</i></a>
							<cfelse>
								<a href="javascript:modifyNoteWin(#projectid#);">#replace(notes, chr(13), "<br />", "all")#</a>
							</cfif>
						</td>
						<td align="center" valign="top" class="linedrowrightcolumn">
							<select name="statusid" onchange="updateStatus(this.form, #projectid#,this.options[this.selectedIndex].value);">
								<cfloop query="getStatus">
									<option <cfif projectstatus is getstatus.statusid>selected</cfif> value="#statusid#">#status#</option>
								</cfloop>
							</select>
						</td>
						<td align="center" valign="top" class="linedrowrightcolumn">#dateformat(lastupdated,'mm/dd/yyyy')#</td>
					</tr>
					</cfif>
				</cfoutput>
				<cfif isdefined("form.btnAddProject")>
				<tr>
					<td align="center" valign="top" class="linedrowrightcolumn"><a href="index.cfm"><img border="0" src="/images/delete.gif" alt="Cancel" width="16" height="16" /></a></td>
					<td align="center" valign="top" class="linedrowrightcolumn">
						<cfquery name="countOpenProjects" dbtype="query">
							select count(projectid) as projcount
							from getprojects 
							where userid = #cookie.adminlogin# and statusid <> 3
						</cfquery>
						<cfif countOpenProjects.recordcount is 0>
							<cfset newPri = 1>
						<cfelse>
							<cfset newPri = countOpenProjects.projCount + 1>
						</cfif>
						<select name="newPriority" >
							<cfloop from="1" to="#newPri#" index="p">
								<cfoutput>
								<option <cfif p is newPri>selected</cfif> value="#p#">#p#</option>
								</cfoutput>
							</cfloop>
						</select>
					</td>
					<td valign="top" class="linedrowrightcolumn">
						<input type="text" name="newName" style="width:200px" value="Enter project name" onfocus="if (this.value == 'Enter project name') this.value = '';" />
					</td>
					<td valign="top" class="linedrowrightcolumn">
						<textarea name="newDescription" style="width:250px" onfocus="if (this.value == 'Enter project information') this.value = '';" rows="3">Enter project information</textarea>
					</td>
					<td valign="top" class="linedrowrightcolumn">
						<textarea name="newNotes" style="width:250px" onfocus="if (this.value == 'Enter project information') this.value = '';" rows="3">Enter project information</textarea>
					</td>
					<td align="center" valign="top" class="linedrowrightcolumn">
						<select name="newStatusid">
							<cfoutput query="getStatus">
								<option value="#statusid#">#status#</option>
							</cfoutput>
						</select>
					</td>
					<td align="center" valign="top" class="linedrowrightcolumn">
						<!--- <input name="btnSaveInfo" type="submit" class="sidebar" value="Save Info" /> --->
						&nbsp;
					</td>
				</tr>
				</cfif>
				<tr>
					<td colspan="7" class="headerbar"><b>Completed Projects</b></td>
				</tr>
				<cfoutput query="getProjects">
					<cfset projectstatus = getprojects.statusid>
					<cfif projectStatus is 3>
					<tr>
						<td align="center" valign="top" class="linedrowrightcolumn"><a href="javascript: if (confirm('Are you sure you wish to delete this project?')) document.location='delete.cfm?projectid=#projectid#';"><img border="0" src="/images/delete.gif" alt="Delete Project" width="16" height="16" /></a></td>
						<td align="center" valign="top" class="linedrowrightcolumn">
						<!--- 
							<select name="priority" onchange="updatePriority(this.form, #projectid#, this.options[this.selectedIndex].value);" >
								<cfloop from="1" to="#getprojects.recordcount#" index="p">
									<option <cfif priority is p>selected</cfif> value="#p#">#p#</option>
								</cfloop>
							</select>
						 --->&nbsp;
						</td>
						<td valign="top" class="linedrowrightcolumn"><a href="javascript:modifyNameWin(#projectid#);">#name#</a></td>
						<td valign="top" class="linedrowrightcolumn">
						<cfif trim(description) is "">
							<a href="javascript:modifyDescriptionWin(#projectid#);"><i>Click here to add/modify description</i></a>
						<cfelse>
							<a href="javascript:modifyDescriptionWin(#projectid#);">#replace(description, chr(13), "<br />", "all")#</a>
						</cfif>
						</td>
						<td valign="top" class="linedrowrightcolumn">
							<cfif trim(notes) is "">
								<a href="javascript:modifyNoteWin(#projectid#);"><i>Click here to add/modify notes</i></a>
							<cfelse>
								<a href="javascript:modifyNoteWin(#projectid#);">#replace(notes, chr(13), "<br />", "all")#</a>
							</cfif>
						</td>
						<td align="center" valign="top" class="linedrowrightcolumn">
							<select name="statusid" onchange="updateStatus(this.form, #projectid#,this.options[this.selectedIndex].value);">
								<cfloop query="getStatus">
									<option <cfif projectstatus is getstatus.statusid>selected</cfif> value="#statusid#">#status#</option>
								</cfloop>
							</select>
						</td>
						<td align="center" valign="top" class="linedrowrightcolumn">#dateformat(lastupdated,'mm/dd/yyyy')#</td>
					</tr>
					</cfif>
				</cfoutput>
				<tr>
					<td colspan="7" align="left" class="linedrowrightcolumn">
						<cfif isdefined("form.btnAddProject")>
							<input name="btnSaveInfo" type="submit" class="sidebar" value="Save New Project" />
						<cfelse>
							<input name="btnAddProject" type="submit" class="sidebar" value="Add new Project">
						</cfif>
					</td>
				</tr>
			</table>
			</form>
		</td>
	</tr>
</table>
<br />
<br />
<a href="/index.cfm" style="text-decoration:underline;">Return to Intranet</a>
</div>
