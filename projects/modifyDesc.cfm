<link rel="stylesheet" type="text/css" href="../styles.css">

<cfif isdefined("form.btnSaveChanges")>
	<cfquery name="savedescription" datasource="#ds#">
		update projects_main
		set description = '#form.description#' 
		where projectid = #form.projectid# 
	</cfquery>
	<cfinclude template="updateLastUpdated.cfm">
	<script type="text/javascript">
	opener.location=opener.location;
	self.close();
	</script>
	<cfabort>
</cfif>

<cfquery name="Getdescription" datasource="#ds#">
	select description from projects_main
	where projectid = #url.projectid#
</cfquery>

<div align="center">
<table width="390" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Modify Description</b> </td>
	</tr>
	<form method="post" action="modifyDesc.cfm">
	<cfoutput>
	<input type="hidden" name="projectid" value="#url.projectid#" />
	</cfoutput>
	<tr>
		<td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td>
						<textarea name="description" style="width:380px" rows="10"><cfoutput>#getdescription.description#</cfoutput></textarea>
					</td>
				</tr>
				<tr>
					<td>
						<input name="btnSaveChanges" type="submit" class="sidebarsmall" value="Save Changes">
					</td>
				</tr>
			</table>
		</td>
	</tr>
	</form>
</table>
</div>