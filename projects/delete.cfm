
<!--- get curr priority --->
<cfquery name="getPri" datasource="#ds#">
	select priority from projects_main where projectid = #projectid#
</cfquery>
<cfset p = getPri.priority>

<!--- del project --->
<cfquery name="delProject" datasource="#ds#">
	delete from projects_main 
	where projectid = #projectid# 
</cfquery>

<!--- update other priorities --->
<cfquery name="updateOthers" datasource="#ds#">
	update projects_main
	set priority = priority - 1
	where userid = #cookie.adminlogin# and priority > #p#
</cfquery>

<cflocation url="index.cfm"> 
