<cfset projectid = attributes.projectid> 
<cfset newstatus = attributes.statusid>
<cfset ds = attributes.datasource>

<!--- get project info --->
<cfquery name="getProjectInfo" datasource="#ds#">
	select * from projects_main
	where projectid = #projectid#
</cfquery>

<cfset oldStatus = getProjectInfo.statusid>
<cfset oldPriority = getProjectInfo.priority>

<!--- update status --->
<cfquery name="updateStatus" datasource="#ds#">
	update projects_main 
	set statusid = #newstatus# 
	where projectid = #projectid#
</cfquery>

<!--- if old status is completed, make new priority lowest --->
<cfif oldStatus is 3>
	<!--- get max priority --->
	<cfquery name="getHighP" datasource="#ds#">
		select max(priority) as highP from projects_main 
		where userid = #cookie.adminlogin# 
	</cfquery>
	<cfset newP = getHighP.Highp + 1>
	<cfquery name="updateP" datasource="#ds#">
		update projects_main 
		set priority = #newP# 
		where projectid = #projectid#
	</cfquery>
</cfif>

<!--- if new status is completed set new priority to 0 --->
<cfif newstatus is 3>
	<cf_updatePriority projectid="#projectid#" newPriority="0" datasource="#ds#">
</cfif>

<cfinclude template="updateLastUpdated.cfm">