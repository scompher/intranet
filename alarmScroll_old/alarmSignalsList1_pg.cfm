 <cfparam name="alarmList" default="">
 <cfparam name="temp" default="">
  <cfparam name="url.did" default=0>
  
  <cfparam name="url.type" default="">
 
<cfif url.did IS 0>
	<cfinclude template="#request.TemplateMapping#Auth.cfm"> 
	<cfquery name="insertid" datasource="#request.ODBC_Datasource#">
		INSERT INTO Scrolling_AlarmList_Temp (dealerNumber, PassCode)
		VALUES ('#undealerNumber#', '#unpasscode#')	
	</cfquery>
	<cfquery name="getmaxid" datasource="#request.ODBC_Datasource#">
		SELECT MAX(did) AS dealerid
		FROM Scrolling_AlarmList_Temp
	</cfquery>
	<cfquery name="getInfo" datasource="#request.ODBC_Datasource#">
		SELECT *
		FROM Scrolling_AlarmList_Temp
		Where did = #getmaxid.dealerid#
	</cfquery>
	<cfset url.did = #getmaxid.dealerid#>
<cfelse>
	<cfquery name="getInfo" datasource="#request.ODBC_Datasource#">
		SELECT *
		FROM Scrolling_AlarmList_Temp
		Where did = #url.did#
	</cfquery>
</cfif>
  
<cfoutput>
	<meta http-equiv="refresh" content="120;url=#request.AppURL#History/alarmScroll/alarmSignalsList1.cfm?did=#url.did#&type=#url.type#">
</cfoutput> 

<cfinclude template="/templates/ie_guide_1.css">
<body bgcolor="#000066" text="#FFFFFF" leftmargin=0 topmargin=0 marginheight="0" marginwidth="0">
<cfif url.did IS NOT 0>
	<CFSETTING enablecfoutputonly="Yes">

	<CFSET SendString = #getInfo.dealerNumber# & #getInfo.PassCode# & "SL~" &#url.type# & #chr(13)#>

	<cf_copalink command="#SendString#">
	
	<cfoutput>
	#result#<br>
	</cfoutput>
	<cfabort>
	
	<CFSETTING enablecfoutputonly="No">
	
	<CFIF #Left(result,2)# is 'RJ'>
	
	<CFSET MinusStringLength = #Len(result)# - 2>
	
		<cfinclude template="#request.TemplateMapping#header.cfm">
		<CFOUTPUT>
		<table width="635" cellpadding="0" cellspacing="3" Border="0"><tr><td align="center">
		<Table width="575" cellpadding="0" cellspacing="2" border="0">
			<TR>
				<TD align="Center">
					<span class="hdrtext">C.O.P.S. Monitoring - Scrolling List of Alarm Signals</span>
				</TD>			
			</TR>
			<TR>
				<TD>
					<BR><span class="subhdrtext">Your request has been rejected by the server</span>
					<BR><BR><span class="formtext">Reason:</span>
					<BR><span class="formtext">#Right(result,MinusStringLength)#</span>
				</TD>
			</TR>
			<TR>
				<TD>
					<BR><span class="formtext">Click
					<a href="javascript:history.go(-1)" class="link">here</a>
					 to go back and resubmit the form</span>
				</TD>
			</TR>
		</Table>	
		</td></tr></table>
		</CFOUTPUT>
		<cfinclude template="#request.TemplateMapping#footer.cfm">
	
	<CFELSE>
		<CFSET alarmList_current = result>
		<CFSET alarmList_current = #Replace(result,"<EOF>#CHR(13)##CHR(10)#","","All")#>
		
		<CFSET Endloop = 0>
		<CFLOOP condition="EndLoop EQ 0">
			<CFIF #Find("~~",alarmList_current)# is 0>
				<CFSET Endloop = 1>
			<CFELSE>
				<CFSET alarmList_current = #Replace(alarmList_current,"~~","~ ~","All")#>
			</CFIF>
		</CFLOOP>
		
		<CFOUTPUT>
		<table width="635" cellpadding="0" cellspacing="4" Border="0"><tr><td align="center">
		<Table width="100%" cellpadding="0" cellspacing="0" border="0">
			<TR>
				<TD align="Center">
					<span class="hdrtext">C.O.P.S. Monitoring - Scrolling List of Alarm Signals</span>
				</TD>			
			</TR>
			<TR>
				<TD align="Center">
					<BR><span class="subhdrtext">List of Alarm Signals</span><br><br>
				</TD>
			</TR>
			<TR>
				<TD>
					<Table width="100%" cellpadding="2" cellspacing="0" border="1">
						<TR>
							<TD align="center">
								<span class="subhdrtext">Incident Number</span>
							</TD>
							<TD align="center">
								<span class="subhdrtext">Account Number</span>
							</TD>
							<TD align="center">
								<span class="subhdrtext">Name</span>
							</TD>
							<TD align="center">
								<span class="subhdrtext">Date</span>
							</TD>
							<TD align="center">
								<span class="subhdrtext">Time</span>
							</TD>
							<TD align="center">
								<span class="subhdrtext">Code</span>
							</TD>
							<TD align="center">
								<span class="subhdrtext">Zone</span>
							</TD>
							<TD align="center">
								<span class="subhdrtext">Condition</span>
							</TD>
							<TD align="center">
								<span class="subhdrtext">Dispatcher Handled</span>
							</TD>
							<TD align="center">
								<span class="subhdrtext">Status</span>
							</TD>
							<TD align="center">
								<span class="subhdrtext">Proc. Date</span>
							</TD>
							<TD align="center">
								<span class="subhdrtext">Proc. Time</span>
							</TD>
						</TR>

						<cfquery name="getAlarmList" datasource="#request.ODBC_Datasource#">
							SELECT *
							FROM Scrolling_AlarmList_Temp 
							WHERE did = #url.did#	
						</cfquery>
							
						<cfif getAlarmList.RecordCount IS 0>
							<cfset temp = #alarmList_current#>
							
							<cfquery name="insertAlarmList" datasource="#request.ODBC_Datasource#">
								UPDATE Scrolling_AlarmList_Temp 
								SET  alarmList = '#temp#'
								WHERE did = #url.did#
							</cfquery>
							
						<cfelse>
							
							<!--- add | characters to denote new data string for output variation --->
							<cfset alarmList_current = "|#alarmList_current#|">
							<cfset temp = ListPrepend(#getAlarmList.alarmList#,#alarmList_current#)>
							<cfset updateList = replace(temp, "|","","all")>
							
						 	<cfquery name="updateAlarmList" datasource="#request.ODBC_Datasource#">
								UPDATE Scrolling_AlarmList_Temp 
								SET  alarmList = '#variables.updateList#'
								WHERE did = #url.did#	
							</cfquery>
						 </cfif>
						
						<!--- set new info format flag --->
						<cfset newFlag = 0>
												
						<CFLOOP from="1" to="#ListLen(temp,"#Chr(13)#")#" index="X">
						
							 <CFSET TempRecord = ListGetAt(temp,X,"#Chr(13)#")>
							 <CFSET TempRecord= #Replace(TempRecord,",","","All")#>
							 <cfif ListLen(TempRecord,"~") GT 9>
								<TR>
								<CFLOOP from="1" to="#ListLen(TempRecord,"~")#" index="y">
									<TD align="center">
										<cfif Y is 1>
											<cfset test = ListGetAt(TempRecord,y,"~")>
											<cfif (findnocase("|",test,1) IS NOT 0) AND (newFlag IS 0)>
												<cfset newFlag = 1>
											<cfelseif (findnocase("|",test,1) IS NOT 0) AND (newFlag IS 1)>
												<cfset newFlag = 0>
											</cfif>											
										</cfif>
										<cfset item = ListGetAt(TempRecord,y,"~")>
										<cfset item = replace(variables.item, "|","","all")>
										<cfif newFlag IS 1>
											<cfif (Y is 1) and (trim(ListGetAt(TempRecord,9,"~")) IS "Y")>
												&nbsp;<span class="formtext" style="color : FF0000;"><b>#Trim(variables.item)#</b></span>
											<cfelse>
												&nbsp;<span class="formtextyellow">#Trim(variables.item)#</span>
											</cfif>
										<cfelse>
											<cfif (Y is 1) and (trim(ListGetAt(TempRecord,9,"~")) IS "Y")>
												&nbsp;<span class="formtext" style="color : FF0000;"><b>#Trim(variables.item)#</b></span>
											<cfelse>
												&nbsp;<span class="formtext">#Trim(variables.item)#</span>
											</cfif>
										</cfif>
									</TD>
								</CFLOOP> 
								 <CFIF #ListLen(TempRecord,"~")# is '11'>
									<TD>&nbsp;</TD>
								</CFIF>  	
							</TR>
							</cfif>
						</CFLOOP>
					</TAble>
				</TD>
			</TR>
		</Table>	
		</td></tr></table>
		</CFOUTPUT>
		
	
	</CFIF>
</cfif>
</body>
