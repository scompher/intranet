
<cfparam name="dealernumber" default="">
<cfparam name="type" default="1|2">

<link rel="stylesheet" type="text/css" href="../styles.css">

<SCRIPT LANGUAGE="JavaScript" TYPE="text/javascript">
<!--
	function transvar3()
	{
		if (checkForm(document.mainform))
		{
			var type = "";
			if (document.mainform.type[0].checked==true)
				type = "1|";
			if (document.mainform.type[1].checked==true)
				type = type + "2|";
			if (document.mainform.type[2].checked==true)
				type = type + "3|";
				
			if (type == "")
			{
				alert ("Please check at least one check box")	
				return;
			}	
			type = type.substr(0, type.length-1);
			
			// urlstring = "alarmsignalsList1.cfm?type=" + type + "&dn=" + document.mainform.dealerNumber.value + "&pc=" + document.mainform.passcode.value;
			urlstring = "test.cfm?type=" + type + "&dn=" + document.mainform.dealerNumber.value + "&pc=" + document.mainform.passcode.value;
			// nwWindow=window.open(urlstring, 'scrollWindow');
			// nwWindow.focus();
			document.location = urlstring;
		}
	}    
	
	function checkForm(frm) {
		if (frm.dealerNumber.value == "") {alert('The dealer number is required.'); frm.dealerNumber.focus(); return false;}
		if (frm.passcode.value == "") {alert('The passcode is required.'); frm.passcode.focus(); return false;}
		if (frm.passcode.value.length < 5) {alert('The passcode must be 5 characters.'); frm.passcode.focus(); return false;}
		
		return true;
	}
//-->
</SCRIPT>

<body onLoad="if (document.mainform.dealerNumber.value == '') {document.mainform.dealerNumber.focus();} else {document.mainform.passcode.focus();}">
<div align="center" class="normal">
<!--- <Form method="post" action="alarmSignalsList1.cfm" name="mainform"> --->
<Form method="post" action="test.cfm" name="mainform">
	<table border="0" cellspacing="0" cellpadding="5">
		<cfif isdefined("url.invalidLogin")>
		<tr align="center">
			<td colspan="2" style="font-weight:bold; color:#FF0000;">Invalid Dealer Number or Passcode<br><br></td>
		</tr>
		</cfif>
		<tr>
			<td colspan="2" align="center" valign="top" nowrap bgcolor="#FFFFFF" class="header">Scrolling List of Alarms</td>
			</tr>
		<tr>
			<td align="right" valign="top" nowrap class="greyrowleft">&nbsp;</td>
			<td nowrap class="greyrowright">&nbsp;</td>
		</tr>
		<tr>
			<td align="right" nowrap class="greyrowleft">Dealer Number: </td>
			<td nowrap class="greyrowright">
				<input onKeyUp="if (this.value.length == 4) {this.form.passcode.focus();}" type="text" name="dealerNumber" value="<cfoutput>#dealernumber#</cfoutput>">
			</td>
		</tr>
		<tr>
			<td align="right" nowrap class="greyrowleft">Passcode:</td>
			<td nowrap class="greyrowright">
				<input type="text" name="passcode" value="">
			</td>
		</tr>
		<tr>
			<td align="right" valign="top" nowrap class="greyrowleft">Options:</td>
			<td nowrap class="greyrowright" style="padding:0px">
				<table border="0" cellpadding="3" cellspacing="0" class="grey">
					
					<tr>
						<td>
							<input name="type" type="checkbox" value="checkbox" <cfif listfind(type,1,"|") is not 0>checked</cfif> >
						</td>
						<td>Dispatcher Handled Alarms</td>
					</tr>
					<tr>
						<td>
							<input name="type" type="checkbox" value="checkbox" <cfif listfind(type,2,"|") is not 0>checked</cfif> >
						</td>
						<td>Logged In Alarms</td>
					</tr>
					<tr>
						<td>
							<input type="checkbox" name="type" value="checkbox" <cfif listfind(type,3,"|") is not 0>checked</cfif> >
						</td>
						<td>Test Results</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="2" align="center" class="greyrowbottom">
				<input name="Submit" type="button" class="sidebar" value="View Scrolling Alarms" style="padding:3px" onClick="transvar3();" />
			</td>
		</tr>
	</table>
</Form>
<br>
<a href="/index.cfm" style="text-decoration:underline;">Return to Intranet Menu</a>
</div>
</body>
