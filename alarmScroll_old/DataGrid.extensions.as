// separate cell or row properties container
// needed for modification #4,8

RowCellStyle = function()
{
}

// separate cell or row properties container
// needed for modification #4,8


RowCellStyle.prototype.getCellHeight = function()
{
   if( this.textStyle != undefined && this.textStyle.size != undefined )
      return this.textStyle.size + 12;

   return undefined;
}


// header background drawing
// needed for modification #3

FDataGridClass.prototype.drawHeaderBG = function()
{
   this.header_mc.headerBG.removeMovieClip();

   if( this.columnHeaders != false )
   {
      var tmp = this.header_mc.createEmptyMovieClip("headerBG", 1);

      var x_from = 0.75;

      for( var i = this.leftDisplayed; i < this.colArray.length; i++ )
      {
         var w = this.colArray[i].getWidth();

         if( x_from + w > this.realWidth )
            w = this.realWidth - x_from;

         if( i == this.colArray.length - 1 && x_from + w < this.realWidth )
            w = this.realWidth - x_from;

         tmp.moveTo( x_from, 1 );

         var bgCol = (this.styleTable.header.value==undefined) ? 0xaaaaaa : this.styleTable.header.value;

         if( this.colArray[i].styleTable.headerBG.value != undefined )
            bgCol = this.colArray[i].styleTable.headerBG.value;

         tmp.beginFill(bgCol);

         var bCol = bgCol;

         if( this.colArray[i].styleTable.showHeaderSeparator.value != false )
            bCol = (this.styleTable.gridLines.value==undefined) ? 0xdddddd : this.styleTable.gridLines.value;

         tmp.lineStyle(0,bCol);
         tmp.lineTo( x_from + w, 1 );
         tmp.lineStyle(0,bCol,100);
         tmp.lineTo( x_from + w, this.headerHeight );
         tmp.lineTo( x_from, this.headerHeight );
         tmp.lineStyle(0,0x000000,0);
         tmp.lineTo( x_from, 1 );

         tmp.endFill();

         x_from += w;
      }
   }
}

// columns drawing
// needed for modifications 1,3

FDataGridClass.prototype.drawColumns = function()
{
	this.drawHeaderBG();

	var x = 0.75;
	var oldX = 1;
	var tmpHeight = this.height - 1;

	for( var i = this.leftDisplayed; i < this.colArray.length; i++ )
	{
		x += this.colArray[i].width;

		if (this.columnHeaders)
		{
         if( oldX + 2 >= this.realWidth )
         {
            this.header_mc["label"+i].removeMovieClip();
            this.header_mc["sep"+i].removeMovieClip();
         }
         else
         {
   			var lbl = this.header_mc["label"+i];

            lbl._x = oldX + 2;

            if (i==this.colArray.length-1)
               this.resizeColumn(i,this.colArray[i].width);

            lbl.setSize(this.colArray[i].width-5);

            if( x + 1 > this.realWidth )
               this.header_mc["sep"+i].removeMovieClip();
            else
               this.header_mc["sep"+i]._x = x-1;
         }
		}

		oldX = x;
	}

	for (var i=0; i<this.numDisplayed; i++)
   	this.rows[i].sizeColumns();
}

// row drawing
// needed for modifications 1,4,7,8,9

FGridRowClass.prototype.chooseRowCol = function(sel)
{
   this.highlight2.clear();

   var rowHeight = this.highlight_mc._height;

   if( rowHeight == undefined )
      rowHeight = this.controller.itmHgt;

   if( rowHeight > this.controller.realHeight )
      rowHeight = this.controller.realHeight;

   var tmp = this.highlight2;

   if( tmp == undefined)
      tmp = this.createEmptyMovieClip("highlight2", 1);
   else
      tmp.clear();

   var x_from = 1;

   for( var i = this.controller.leftDisplayed; i < this.controller.colArray.length; i++ )
   {
      var w = this.controller.colArray[i].getWidth();

      if( x_from + w > this.controller.realWidth )
         w = this.controller.realWidth - x_from;

      if( i == this.controller.colArray.length - 1 && x_from + w < this.controller.realWidth )
         w = this.controller.realWidth - x_from;

      tmp.moveTo( x_from, 0 );

      var bgCol = ( this.controller.styleTable.backGround.value == undefined) ? 0xffffff : this.controller.styleTable.backGround.value;

      var cellBG = this.controller.getCellStyle( i, this.getItemIndex() ).background;

      if( cellBG != undefined )
         bgCol = cellBG;
      else
      {
         var rowBG = this.controller.getRowStyle( this.getItemIndex() ).background;

         if( rowBG == undefined && this.controller.aRC )
            rowBG = ( this.getItemIndex()%2 == 0 ) ? this.controller.row1 : this.controller.row2;

         if( rowBG == undefined || this.controller.colArray[i].styleTable.isRowAlternationVisible.value == false)
         {
            if( this.controller.colArray[i].styleTable.background.value != undefined )
               bgCol = this.controller.colArray[i].styleTable.background.value;
         }
         else
            bgCol = rowBG;
      }

		var lineCol = (this.controller.styleTable.gridLines.value == undefined) ? 0x333333 : this.controller.styleTable.gridLines.value;

      var lineColVert = this.controller.colArray[i].styleTable.lineColor.value;
      var lineColHoriz = this.controller.getRowStyle( this.getItemIndex() ).lineColor;

      var lineThickVert = this.controller.colArray[i].styleTable.lineThick.value;
      var lineThickHoriz = this.controller.getRowStyle( this.getItemIndex() ).lineThick;

      if( lineThickVert == 0 )
         lineColVert = bgCol;

      if( lineThickHoriz == 0 )
         lineColHoriz = bgCol;

      if( lineThickVert == undefined )
         lineThickVert = 0;

      if( lineThickHoriz == undefined )
         lineThickHoriz = 0;

      if( lineColVert == undefined )
         lineColVert = lineCol;

      if( lineColHoriz == undefined )
         lineColHoriz = lineCol;

      tmp.beginFill( bgCol );
      tmp.lineStyle( 0, bgCol, 100 );

      var b_off = lineThickVert / 2;
      if( b_off == 0 )  b_off = 1;

      var b_off2 = lineThickHoriz / 2;
      if( b_off2 == 0 )  b_off2 = 1;

      tmp.lineTo( x_from + w - b_off, 0 );

      if( i < this.controller.colArray.length - 1 && this.controller.gridLines )
      {
         tmp.lineStyle( lineThickVert, lineColVert, 100 );
         tmp.lineTo( x_from + w - b_off, rowHeight - b_off2 );
         tmp.lineTo( x_from + w - b_off - 0.15, rowHeight - b_off2 );
         tmp.lineStyle( 0, bgCol, 100 );
      }
      else
         tmp.lineTo( x_from + w - 1, rowHeight - b_off2 );

      if( this.controller.gridLinesHoriz == true && this.getItemIndex() < this.controller.dataProvider.items.length )
      {
         tmp.lineStyle( lineThickHoriz, lineColHoriz, 100 );
         tmp.lineTo( x_from, rowHeight - b_off2 );
         tmp.lineStyle( 0, bgCol, 100 );
      }
      else
         tmp.lineTo( x_from, rowHeight - b_off2 );

      tmp.lineTo( x_from, 0 );

      tmp.endFill();

      x_from += w;

      if( x_from >= this.controller.realWidth )
         break;
   }

   var col_obj = new Color( tmp );

   if(sel)
   {
      var myTObj = { ra: '45', rb: '0', ga: '45', gb: '0', ba: '45', bb: '0', aa: '30', ab: '0'};
      col_obj.setTransform( myTObj );
   }
   else
   {
      var myTObj = { ra: '100', rb: '0', ga: '100', gb: '0', ba: '100', bb: '0', aa: '100', ab: '0'};
      col_obj.setTransform( myTObj );
   }
}

// actions on column resizing
// needed for modification 1,3,4,7,8,9

FGridRowClass.prototype.sizeColumns = function()
{
	var totX = 0;
	for (var i = this.controller.leftDisplayed; i < this.controller.colArray.length; i++)
	{
		this.cells[i]._x = totX; // Math.floor

      this.cells[i].setSize(this.controller.colArray[i].width-5);

		this.cells[i].labelField.selectable = false;
		totX+=this.controller.colArray[i].width;
	}

   this.chooseRowCol(selected);

   if( this.controller.aRC && itmObj==undefined )
		this.highlight_mc._alpha = 0;
}

// row content accomodation
// needed for modification #4,10,11

FGridRowClass.prototype.layoutContent = function(width)
{
	var totX = 0;
	var offset = Math.floor((this.controller.itmHgt - this.controller.oldItmHgt) / 2);
	var maxHeight = 0;
	if( this.controller.colArray.length != 0 )
	{
      if( this.controller.showHScroll == true )
   		for( var i = 0; i < this.controller.leftDisplayed; i++ )
            if( this.cells[i] != undefined )
            {
               this.cells[i].removeMovieClip();
               this.cells[i] = undefined;
            }

		for( var i = this.controller.leftDisplayed; i < this.controller.leftDisplayed + this.controller.colDisplayed; i++ )
		{
         if( this.cells[i] == undefined || this.cells[i].labelField != undefined )
         {
            var cell_style = this.controller.getCellStyle( i, this.getItemIndex() );

            var tmpStyle;

            if( this.itemNum != undefined )
               tmpStyle = cell_style.textStyle;

            if( tmpStyle == undefined )
               tmpStyle = this.controller.colArray[i].textStyle;

            if( tmpStyle == undefined )
               tmpStyle = this.controller.textStyle;

            if( this.cells[i] == undefined || this.maxHeight >= this.controller.realHeight )
               this.cells[i] = this.attachMovie(this.controller.colArray[i].cellSymbol, "fCell"+i, 2+i, { hostComponent:this.controller, customTextStyle:tmpStyle, textStyle:tmpStyle });
            else
               this.cells[i].labelField.setNewTextFormat( tmpStyle );

            var autoSizeFlag = this.controller.styleTable.autoSizeRows.value;
            if( this.controller.colArray[i].width <= 25 || this.controller.colArray[i].styleTable.noWrap.value == true )
               autoSizeFlag = false;

            this.cells[i].labelField.autoSize = autoSizeFlag;

            if( autoSizeFlag == true )
               this.cells[i].labelField.wordWrap = true;

            if( this.controller.styleTable.html.value != undefined )
               this.cells[i].labelField.html = this.controller.styleTable.html.value;
         }


         this.cells[i]._x = totX;
         this.cells[i]._y = offset;

         var itemObj = this.getItemModel();
         this.displayValue( itemObj[ this.controller.colArray[i].colName ], i );

         if( itemObj != undefined )
        	   this.cells[i].setSize( this.controller.colArray[i].width - 5 );

         if( this.controller.showHScroll == true )
            this.cells[i]._xscale = 100;

         if( this.cells[i].labelField != undefined )
         {
            if( this.controller.styleTable.cellWordWrap.value == true && this.cells[i].labelField.textHeight > this.cells[i].labelField._height )
               this.cells[i].labelField._height = this.cells[i].labelField.textHeight + 5;

            if(this.controller.colArray[i].width <= 25 && this.cells[i].labelField.textHeight > 0 )
               this.cells[i].labelField._height = this.cells[i].labelField.textHeight + 5;
         }

         if( cell_style.clickHandlerFunc != undefined )
         {
            this.cells[i].onPress = cell_style.clickHandlerObj[ cell_style.clickHandlerFunc ];
            this.cells[i].colIndex = i;
            this.cells[i].rowIndex = this.getItemIndex();
         }
         else
         {
            delete this.cells[i].onPress;
            delete this.cells[i].colIndex;
            delete this.cells[i].rowIndex;
         }

         if( this.cells[i].labelField != undefined )
         {
            if( this.cells[i].labelField.autoSize == true )
   		     	maxHeight = Math.max( maxHeight, this.cells[i].labelField.textHeight );
            else
      			maxHeight = Math.max( maxHeight, this.cells[i].getHeight() );
         }
         else
            if( itemObj != undefined )
               maxHeight = Math.max( maxHeight, this.cells[i]._height );

         maxHeight = Math.min( maxHeight, this.controller.realHeight );

			this.cells[i].labelField.selectable = false;
			this.cells[i].colIndex = i;
			this.cells[i].getGrid = this.controller.getGrid;
			this.cells[i].getCellIndex = this.controller.getCellIndex;
			totX+=this.controller.colArray[i].width;
			this.editCellSetup();
		}

      if( this.controller.showHScroll == true )
         for( var i = this.controller.leftDisplayed + this.controller.colDisplayed; i < this.controller.colArray.length; i++ )
            if( this.cells[i] != undefined )
            {
               this.cells[i].removeMovieClip();
               this.cells[i] = undefined;
            }

		maxHeight = Math.max(this.controller.itmHgt, maxHeight);

		for (var i = this.controller.leftDisplayed; i < this.controller.colDisplayed; i++)
		{
         var vAlign = this.controller.colArray[i].styleTable.vertAlign.value;

         if( vAlign == "top" )
   		   this.cells[i]._y = 0;
         else
         {
            var cellH = this.cells[i]._height;

            if( this.cells[i].labelField.textHeight != undefined )
               cellH = this.cells[i].labelField.textHeight + 5;

            if( cellH - 5 == 0 )
             	this.cells[i]._y = 1;
            else
            {
               if( vAlign == "bottom" )
                  this.cells[i]._y = Math.Max( 0, maxHeight - cellH );
               else
                  this.cells[i]._y = Math.Max( 0, ( maxHeight - cellH ) / 2 );
            }
         }
		}
	}
	else
	{
		this.cells[0] = this.attachMovie("FLabelSymbol", "fCell0", 2, {hostComponent:this.controller});
	}

   this.maxHeight = maxHeight;
}


// cell's text style retrieving
// needed for modification #4

FDataGridClass.prototype.isCellHeightRedefined = function()
{
  if( this.styleTable.autoSizeRows.value == true )  return true;
  if( this.styleTable.cellWordWrap.value == true )  return true;

	for( var i in this.cellStyles )
		if( this.cellStyles[i].getCellHeight() != undefined )
			return true;

	return false;
}


// cell's editable check
// needed for modification #5

FDataGridClass.prototype.isCellEditable = function( col, row )
{
   if( this.editable != true ) return false;

	for( var i = 0; i < this.cellStyles.length; i++ )
		if( this.cellStyles[i].colIndex == col
          && this.cellStyles[i].rowIndex == row
          && this.cellStyles[i].editable != undefined )
			return this.cellStyles[i].editable;

   if( this.colArray[ col ].editable == false )
     return false;

	return true;
}

// cell's text style setting
// needed for modification #4

FDataGridClass.prototype.setCellStyle = function( col, row, propName, value )
{
	if( this.cellStyles == undefined )
	   this.cellStyles = new Array();

	var found;

	for( var i = 0; i < this.cellStyles.length; i++ )
		if( this.cellStyles[i].colIndex == col && this.cellStyles[i].rowIndex == row )
			found = i;

	if( found == undefined )
	{
		var cell_style = new RowCellStyle();
		cell_style.colIndex = col;
		cell_style.rowIndex = row;
		cell_style.rowID = this.dataProvider.getItemID( row )
		found = this.cellStyles.push( cell_style ) - 1;
	}

	if( propName.subString( 0, 4 ) == "text" )
	{
		if( this.cellStyles[ found ].textStyle == undefined )
			this.cellStyles[ found ].textStyle = new TextFormat();

		var textProp = propName.subString( 4, propName.length );
		this.cellStyles[ found ].textStyle[ textProp ] = value;
		this.invalidate("setSize");
		return true;
	}
   else
   {
		var cell_style = this.cellStyles[ found ];
      cell_style[ propName ] = value;
      return true;
   }



	return undefined;
}

// cell's style retrieving
// needed for modification #4,11

FDataGridClass.prototype.getCellStyle = function( col, row )
{
	for( var i in this.cellStyles )
		if( this.cellStyles[i].colIndex == col && this.cellStyles[i].rowIndex == row )
			return this.cellStyles[i];

	return undefined;
}


// row's text style setting
// needed for modification #8

FDataGridClass.prototype.setRowStyle = function( row, propName, value )
{
	if( this.rowStyles == undefined )
	   this.rowStyles = new Array();

	var found;

	for( var i = 0; i < this.rowStyles.length; i++ )
		if( this.rowStyles[i].rowIndex == row )
			found = i;

	if( found == undefined )
	{
		var row_style = new RowCellStyle();
		row_style.rowIndex = row;
		row_style.rowID = this.dataProvider.getItemID( row )
		found = this.rowStyles.push( row_style ) - 1;
	}

	if( propName.subString( 0, 4 ) == "text" )
	{
		if( this.rowStyles[ found ].textStyle == undefined )
			this.rowStyles[ found ].textStyle = new TextFormat();

		var textProp = propName.subString( 4, propName.length );
		this.rowStyles[ found ].textStyle[ textProp ] = value;
		this.invalidate("setSize");
		return true;
	}
   else
   {
		var row_style = this.rowStyles[ found ];
      row_style[ propName ] = value;
		this.invalidate("updateControl");
      return true;
   }

	return undefined;
}

// row's style retrieving
// needed for modification #8

FDataGridClass.prototype.getRowStyle = function( row )
{
	for( var i in this.rowStyles )
		if( this.rowStyles[i].rowIndex == row )
			return this.rowStyles[i];

	return undefined;
}

// actions on grid's data changing ( sort etc )
// needed for modification 4

FDataGridClass.prototype.modelChanged = function(eventObj)
{
	super.modelChanged(eventObj);

	if (eventObj.event == "updateAll")
    	this.invalidate("setSize");
   else
   {
      if(eventObj.event == "sort")
      {
         for( var i in this.cellStyles )
         {
            for( var row = 0; row < this.getLength(); row++ )
               if( this.cellStyles[i].rowID == this.dataProvider.getItemID( row ) )
               {
                  this.cellStyles[i].rowIndex = row;
                  break;
               }
         }

         if( this.isCellHeightRedefined() == true )
            this.invalidate("relayoutRows");

         return;
      }

      if(eventObj.event == "addRows")
      {
         this.invalidate("relayoutRows");
         return;
      }

      this.initScrollBar();
      this.invalidate("drawColumns");
      this.invalidate("initHeaders");
   }
}


// row size calculation
// needed for modification 4

FSelectableItemClass.prototype.setSize = function(width, height)
{
	this.width = width;
	this.layoutContent(width); // EXTEND this for alternate content.

	if (this.highlight_mc==undefined)
   {
		this.attachMovie( "FHighlightSymbol", "highlight_mc", LOWEST_DEPTH);
		this.highlight_mc._x=1;
		this.highlight_mc.controller = this;
		this.highlight_mc._alpha = 0;
		this.highlight_mc.trackAsMenu = true;
		this.highlight_mc.useHandCursor = false;
		this.highlight_mc.onPress = this.highlightPress;
		this.highlight_mc.onDragOver = this.highlightDragOver;

	}

	this.editHighlightSetup();

	this.highlight_mc._width = width-0.5;
	this.highlight_mc._height = Math.max( height, this.maxHeight );

}

// grid's rows sizes calculation
// needed for modification 4

FDataGridClass.prototype.setSize = function(w,h)
{
   if( this.idSequence == undefined )
      this.idSequence = 0;

	if (!this.enable)
		return;

	this._yscale = this._xscale = 100;
	delete this.methodTable["setSize"];

	if (this.borderOn)
		this.boundingBox_mc._visible = true;

	this.width = Math.max(w, 20);
	this.height = Math.max(h, 40);
   this.initialHeight = this.height;

	this.container_mc.removeMovieClip();
   delete this.bottomMask;
   this.rows.length = 0;
	this.scrollBar_mc = undefined;
	this.scrollBarH_mc = undefined;
	this.container_mc = this.createEmptyMovieClip("container", 3);

   if( this.leftDisplayed == undefined )
      this.leftDisplayed = 0;

   if( this.colDisplayed == undefined )
      this.colDisplayed = this.colArray.length;

	this.realWidth = this.width - 17;

   this.initHeaders();

	if(this.columnHeaders)
		this.container_mc._y = this.header_mc._height;
   else
		this.container_mc._y = 1;

   if( this.ShowHScroll == true )
	   this.realHeight = this.height - 16;
   else
	   this.realHeight = this.height;

	this.colArray[ this.colArray.length - 1 ].setWidth( this.colArray[ this.colArray.length - 1 ].width );

   this.itmHgt = undefined;

   this.relayoutRows();

	this.drawColumns();

	delete this.methodTable["drawColumns"];
}

// grid's rows contents accomodation
// needed for modification 4

FDataGridClass.prototype.relayoutRows = function()
{
	var headerH = this.header_mc._height;
   var w = this.width;
   var h = this.height;

   if( this.itmHgt == undefined )
   	this.measureItmHgt();

	this.oldItmHgt = this.itmHgt;

	if( this.rowHeight != undefined && this.itmHgt < this.rowHeight )
		this.itmHgt = this.rowHeight;

   var total_height = 0;

	if( this.tmpRowCount != undefined )
   {
		this.numDisplayed = this.tmpRowCount;
		delete this.tmpRowCount;
    	this.height = this.numDisplayed * (this.itmHgt-2) + 2 + headerH;
	}
   else
      if( this.rowHeight != undefined && this.styleTable.autoSizeRows.value != true )
      {
         this.numDisplayed = Math.floor( ( this.realHeight - headerH ) / this.itmHgt + 0.5 );
      	this.height = this.numDisplayed * this.itmHgt + headerH;
         total_height = this.height - headerH;
      }
      else
      {
         var rows_to_display = 0;

         var r = 0;

         while( total_height < h - headerH )
         {
            var row_height = 0;

            if( r >= this.rows.length || this.rows[r] == undefined )
            {
               this.idSequence++;
              	this.rows[r] = this.container_mc.attachMovie( this.itemSymbol, "row" + this.idSequence + "_mc", 10 + r, {controller:this,itemNum:r});
            }

            if( this.topDisplayed + r >= this.getLength() )
            {
               row_height = this.itmHgt;
               this.rows[r].maxHeight = undefined;
	            this.rows[r].highlight_mc._height = this.itmHgt;
            }
            else
            {
               var offset = ( this.scrollOffset == undefined ) ? 0 : this.scrollOffset;
               this.rows[r].setSize( this.width - offset, this.itmHgt );
               row_height = this.rows[r].maxHeight;
            }

            if( row_height == 0 )
               row_height = this.itmHgt;

            var distance = row_height;

            if( this.styleTable.gridHeightAutosizing.value != false )
               distance = row_height / 2;

            if( headerH + total_height + row_height - this.realHeight < distance )
            {
               total_height += row_height;
               rows_to_display++;
            }
            else
               break;

            r++;
         }

         this.numDisplayed = rows_to_display;

         if( this.styleTable.gridHeightAutosizing.value != false )
            this.height = total_height + headerH;

         if( this.showHScroll == true )
            this.height += 16;
      }

	var pos = this.getScrollPosition();

   for( var i = this.numDisplayed; i < this.rows.length; i++ )
   {
      this.rows[i].removeMovieClip();
      delete this.rows[i];
   }

	this.focusRect.removeMovieClip();

   if( this.styleTable.gridHeightAutosizing.value != false )
   {
   	this.boundingBox_mc._xscale = this.boundingBox_mc._yscale = 100;
   	this.boundingBox_mc._xscale = this.width * 100 / this.boundingBox_mc._width;
   	this.boundingBox_mc._yscale = this.height * 100 / this.boundingBox_mc._height;
   }
   else
      if( this.boundingBox_mc._height != this.initialHeight )
      {
         this.height = this.initialHeight;

         this.boundingBox_mc._xscale = this.boundingBox_mc._yscale = 100;
         this.boundingBox_mc._xscale = this.width * 100 / this.boundingBox_mc._width;
         this.boundingBox_mc._yscale = this.initialHeight * 100 / ( this.boundingBox_mc._height - 1 );
      }


   var y = 0;

	for (var i = 0; i < this.numDisplayed; i++)
   {
      if( this.rows[i] == undefined )
      {
         this.idSequence++;
       	this.rows[i] = this.container_mc.attachMovie( this.itemSymbol, "row" + this.idSequence + "_mc", 10 + i, {controller:this,itemNum:i});
         this.rows[i].setSize( this.width - offset, this.itmHgt );
      }

		this.container_mc[ "fListItem" + i + "_mc" ] = this.rows[i];

		this.rows[i]._y = y;
      this.rows[i].drawItem( this.getItemAt( this.topDisplayed + i ), this.isSelected( this.topDisplayed + i ) );

      if( this.rows[i].maxHeight != undefined )
         y += this.rows[i].maxHeight;
      else
         y += this.itmHgt;
	}

   if( this.showHScroll == true )
   {
      var mm_func_h = this.scrollBarH_mc.scrollThumb_mc.onMouseMove;

   	this.setHScrollPosition( this.leftDisplayed );
      this.initHScrollBar();

      this.scrollBarH_mc.scrollThumb_mc.onMouseMove = mm_func_h;
   }

   var mm_func_v = this.scrollBar_mc.scrollThumb_mc.onMouseMove;

	this.setScrollPosition( pos );
	this.scrollBar_mc._y = 0;
	this.scrollBar_mc.setSize( this.height - this.scrollBarH_mc._height );
	this.scrollBar_mc.setLargeScroll( this.numDisplayed - 1 );
	this.initScrollBar();

   if( this.columnHeaders != false )
   	this.scrollBar_mc._y -= headerH;

   if( this.styleTable.gridHeightAutosizing.value != false )
     	this.scrollBar_mc.setSize( this.height );
   else
     	this.scrollBar_mc.setSize( this.realHeight );

   this.scrollBar_mc.scrollThumb_mc.onMouseMove = mm_func_v;
   this.scrollBar_mc.scrollThumb_mc.onMouseUp = this.scrollBar_mc.scrollThumb_mc.onRelease;

   if( this.styleTable.gridHeightAutosizing.value == false )
   {
      if( this.bottomMask == undefined )
   	{
         this.bottomMask = this.container_mc.createEmptyMovieClip( "maskrow_mc", 4000001 );
         this.bottomMask.lineStyle(0);
         this.bottomMask.beginFill(0x000000);
         this.bottomMask.lineTo(1,0);
         this.bottomMask.lineTo(1,1);
         this.bottomMask.lineTo(0,1);
         this.bottomMask.lineTo(0,0);
         this.bottomMask.endFill();

         this.bottomMask._width = this.width-offset;
      }

		this.bottomMask._y = this.rows[ this.numDisplayed - 1 ]._y;

      if( this.topDisplayed + this.numDisplayed > this.dataProvider.getLength() )
      {
         this.bottomMask._height = this.itmHgt - ( total_height + headerH - this.initialHeight );
         this.rows[ this.numDisplayed - 1 ].setMask( this.bottomMask );
      }
      else
      {
         var bh = total_height + headerH - this.realHeight;

         if( bh < this.rows[ this.numDisplayed - 1 ]._height )
            this.bottomMask._height = this.rows[ this.numDisplayed - 1 ].maxHeight - bh;
         else
            this.bottomMask._height = 1;

         var last_row = this.rows[ this.numDisplayed - 1 ];

         last_row.setMask( this.bottomMask );
         for( var i = 0; i < last_row.cells.length; i++ )
         {
            last_row.cells[i].labelField.autoSize = "none";

            if( last_row.cells[i]._y >= this.bottomMask._height )
               last_row.cells[i].labelField._height = 0;
            else
               if( last_row.cells[i]._y + last_row.cells[i].labelField._height > this.bottomMask._height )
                  last_row.cells[i].labelField._height = this.bottomMask._height - last_row.cells[i]._y;
         }
      }
   }

   if( this.showHScroll == true )
   {
      var last_col = this.leftDisplayed + this.colDisplayed - 1;

      for( var i = 0; i < this.numDisplayed; i++ )
         if( this.rows[i].cells[last_col]._x + this.rows[i].cells[last_col]._width > this.realWidth )
         {
            this.rows[i].cells[last_col].labelField.autoSize = "none";

            if( this.rows[i].cells[last_col]._x >= this.realWidth )
               this.rows[i].cells[last_col]._width = 0;
            else
               this.rows[i].cells[last_col]._width = this.realWidth - this.rows[i].cells[last_col]._x;
         }
   }
}
                                                             
// grid's rows content displaying
// needed for modification 1,4,5,6,7,8,9

FGridRowClass.prototype.displayContent = function(itmObj, selected)
{
	if( itmObj.label == "Sizer: PjtTopg" )
   {
		this.fCell0.setValue(itmObj.label, selected);
		return;
	}

	if( this.highlighted != selected || this.redraw )
   {
		var prop = (selected) ? "textSelected" : "textColor";
		if(!this.enable)
			prop = "textDisabled";

		var clr = this.controller.styleTable[prop].value;

		if (clr==undefined)
      {
			clr = (selected) ? 0xffffff : 0x000000;
			if (!this.enable)
				clr = 0x666666;
		}
	}

	var i = this.cells.length;

	while(i--)
   {
      var native_value = itmObj[ this.controller.colArray[i].colName ];

      if( this.cells[i].labelField.type == "input" )
         this.cells[i].setValue( native_value, selected );
      else
         this.displayValue( native_value, i );

      if( this.controller.isCellEditable( i, this.getItemIndex() ) == true )
      {
         this.cells[i].labelField.border = this.controller.styleTable["editableBorder"].value;
         this.cells[i].labelField.borderColor = this.controller.styleTable["editableBorderColor"].value;
         this.cells[i].labelField.background = this.controller.styleTable["editableBackground"].value;
         this.cells[i].labelField.backgroundColor = this.controller.styleTable["editableBackgroundColor"].value;
      }

		if( this.highlighted != selected || this.redraw )
      {
			var colClr = this.controller.colArray[i].styleTable[prop].value;

			if( colClr == undefined )
				colClr = clr;

			this.cells[i].setColor( colClr );
		}
	}

   this.chooseRowCol(selected);

   if( this.controller.aRC && itmObj==undefined )
		this.highlight_mc._alpha = 0;

	delete this.redraw;
}

FGridRowClass.prototype.displayValue = function(native_value, col)
{
   if( native_value == undefined || this.cells[ col ].labelField == undefined )
      this.cells[ col ].setValue( native_value, selected );
   else
   {
      var converter = this.controller.colArray[ col ].styleTable["displayConverter"].value;

      if( converter != undefined )
      {
         var converter_obj = this.controller.colArray[ col ].styleTable["displayConverterObj"].value;

         if( converter_obj == undefined )
            converter_obj = _root;

         native_value = converter_obj[ converter ]( native_value );
         this.cells[ col ].setValue( native_value, selected );
      }
      else
         if( native_value != undefined )
         {
            if( this.controller.colArray[ col ].styleTable["displayFactor"].value != undefined )
               native_value = Number( native_value ) * this.controller.colArray[ col ].styleTable["displayFactor"].value;

            var tmp_text = "";

            if( this.controller.colArray[ col ].styleTable["displayPrefix"].value != undefined )
               tmp_text = tmp_text + this.controller.colArray[ col ].styleTable["displayPrefix"].value;

            tmp_text = tmp_text + String( native_value );

            if( this.controller.colArray[ col ].styleTable["displayPostfix"].value != undefined )
               tmp_text = tmp_text + this.controller.colArray[ col ].styleTable["displayPostfix"].value;

            this.cells[ col ].setValue( tmp_text, selected );
         }
         else
            this.cells[ col ].setValue( undefined, selected );
   }
}

// actions on cell edit begin
// needed for modification 4

// this is the highlight_mc - controller is the row
FGridRowClass.prototype.startEditCell = function()
{
	var grid = this.controller.controller;
	if(this.controller.enable)
   {
  	   if( grid.editable && this.controller.getItemIndex() < grid.getLength() )
      {
			for( var i = grid.leftDisplayed; i < this.controller.cells.length; i++ )
         {
            var is_editable = grid.getCellStyle( i, this.controller.getItemIndex() ).editable;

				if(is_editable == true || ( is_editable == undefined && grid.colArray[i].editable == true ) )
            {
					if (this.controller.cells[i].hitTest(_root._xmouse, _root._ymouse))
               {
						grid.selectedCell = { index: this.controller.getItemIndex(), colName: grid.colArray[i].colName };

						grid.setSelectedCell(this.controller.getItemIndex(), grid.colArray[i].colName);

						clearInterval(grid.dragScrolling);
						delete grid.onMouseUp;

                  grid.editCellListener.currEditedLabelField = this.controller.cells[i].labelField;
                  grid.editCellListener.rowIndex = this.controller.getItemIndex();
                  grid.editCellListener.colName = grid.colArray[i].colName;
                  grid.editCellListener.colIndex = i;

						return;
					}
				}
			}
		}
	}
}

// actions on selection changing
// needed for modification 4,6

FDataGridClass.prototype.setSelectedCell = function(index, colName)
{
	if (!this.enable || !this.editable) return;

  	Selection.setFocus(this);

   var rowIdx = index-this.topDisplayed;
	var colIdx = this.getColumnIndex(colName);

   var itemObj = this.rows[rowIdx].getItemModel();
   this.rows[rowIdx].cells[colIdx].setValue( itemObj[ this.colArray[colIdx].colName ], this.rows[rowIdx].highlighted );

	if (this.topDisplayed > index || index >= this.topDisplayed + this.numDisplayed)
		this.setScrollPosition(index);

	this.rows[rowIdx].lastEdited = this.rows[rowIdx].cells[colIdx].getLabel();
	this.rows[rowIdx].cells[colIdx].labelField.controller = this.rows[rowIdx];
	this.selectedCell = { index: index, colName: colName };
	Selection.removeListener(_global._focusControl);

	this.rows[rowIdx].cells[colIdx].setFocus();
}

// actions on selection changing
// needed for modification 6

// in this case, this is a textField, controller is the row.
FGridRowClass.prototype.cellBlurred = function()
{
	this.selectable = false;
	this.type = "dynamic";
	this.hscroll=0;
	this.background = false;
	this.border = false;
	var grid = this.controller.controller;

   if( grid.styleTable.autoSizeRows.value == true && this.editCellListener.currEditedLabelField != undefined )
      grid.editCellListener.currEditedLabelField.text = grid.dataProvider.getItemAt( this.controller.getItemIndex() )[ grid.colArray[this.cellNum].colName ];

	grid.editCell(this.cellNum, this.controller.itemNum, this.controller.cells[this.cellNum].getLabel());

	var caught = grid.catchTab(this.controller.getItemIndex(), grid.colArray[this.cellNum].colName);
	if (!caught)
   {
		Key.removeListener(_global.FGridKeyListener);
		delete _global.FGridKeyListener;
		Selection.addListener(_global._focusControl);
		Selection.addListener(grid.editCellListener);
	}

   var itemObj = this.controller.getItemModel();
   this.controller.displayContent( itemObj, this.controller.highlighted );
}

FGridRowClass.prototype.cellFocused = function()
{
	if (_global.FGridKeyListener==undefined) {
		_global.FGridKeyListener = new Object()
	}
	_global.lastLabel = this._parent;
	_global.lastController = this.controller;
	_global.FGridKeyListener.onKeyDown = function()
	{
		if (Key.isDown(Key.ESCAPE)) {
			lastLabel.setValue(lastController.lastEdited);
			Selection.setFocus(lastController);
		} else if (Key.isDown(Key.TAB)) {
			// extra support for tabbing - see Nav mod
			lastController.controller.manageTab();
		}
	}

	var grid = this.controller.controller;

   if( grid.editCellListener == undefined )
   {
      grid.editCellListener = new Object();
      grid.editCellListener.grid = grid;
      grid.editCellListener.onKeyDown = function()
      {
         if( grid.styleTable.autoSizeRows.value == true && this.currEditedLabelField != undefined )
         {
            var _str = String.fromCharCode(Key.getAscii());

            if( Key.getCode() == key.ENTER || _str.length > 0 )
            {
               var str = new String( grid.dataProvider.getItemAt(this.rowIndex)[this.colName] );

               if( this.currEditedLabelField.length + 1 < str.length )
                  str = str.charAt( str.length - 1 );

               var arr = str.split("");

               var caret = Selection.getCaretIndex();
               var sBegin = Selection.getBeginIndex();
               var sEnd = Selection.getEndIndex();

               if(Key.getCode() == key.ENTER)
                  arr.splice( caret, 0, "\n" );
               else
                  if( Key.getCode() == Key.DELETEKEY )
                  {
                     if( sBegin < sEnd )
                        arr.splice( sBegin, sEnd - sBegin );
                     else
                        arr.splice( caret, 1 );
                  }
                  else
                     if( Key.getCode() == Key.BACKSPACE )
                     {
                        if( sBegin < sEnd )
                           arr.splice( sBegin, sEnd - sBegin );
                        else
                           arr.splice( caret - 1, 1 );
                     }
                     else
                     {
                        if( sBegin < sEnd )
                           arr.splice( sBegin, sEnd - sBegin, _str );
                        else
                           arr.splice( caret, 0, _str );
                     }

               if(Key.getCode() == key.ENTER)
               {
                  this.currEditedLabelField.text = arr.join("");
                  Selection.setSelection( caret + 1, caret + 1 );
               }

               grid.dataProvider.getItemAt(this.rowIndex)[this.colName] = arr.join("");

               this.currEditedLabelField._height = this.currEditedLabelField.textHeight + 5;

               if( Key.getCode() == key.ENTER
                   || this.currEditedLabelField.textHeight > this.currEditedLabelField.controller.maxHeight )
               {
                  var row = grid.rows[ this.rowIndex - grid.topDisplayed ];

                  for( var i = 0; i < row.cells.length; i++ )
                  {
                     if( this.cells[i].labelField.autoSize == true )
                        row.maxHeight = Math.max( row.maxHeight, row.cells[i].labelField.textHeight );
                     else
                        row.maxHeight = Math.max( row.maxHeight, row.cells[i].getHeight() );
                  }

                  var hDelta = row.maxHeight - row.highlight_mc._height;
                  if( hDelta > 0 )
                  {
                     row.highlight_mc._height = row.maxHeight;
                     row.drawItem( grid.getItemAt( this.rowIndex ), grid.isSelected( this.rowIndex ));

                     var delrows = 0;

                     for( var i = row.itemNum + 1; i < grid.numDisplayed; i++ )
                     {
                        if( grid.rows[i]._y + hDelta < grid.initialHeight - grid.header_mc._height )
                           grid.rows[i]._y += hDelta;
                        else
                        {
                           grid.rows[i].removeMovieClip();
                           delete grid.rows[i];
                           delrows++;
                        }
                     }

                     grid.numDisplayed -= delrows;

                     if( grid.topDisplayed + grid.numDisplayed <= grid.dataProvider.getLength() )
                     {
                        grid.scrollBar_mc.setScrollProperties( grid.numDisplayed - 1, 0, grid.getLength() - grid.numDisplayed + 1 );
                        grid.updateControl();
                     }

                     if( grid.styleTable.gridHeightAutosizing.value == false )
                     {
                        var last_row = grid.rows[ grid.numDisplayed - 1 ];
                        grid.bottomMask._y = grid.rows[ grid.numDisplayed - 1 ]._y;

                        if( grid.topDisplayed + grid.numDisplayed > grid.dataProvider.getLength() )
                        {
                           grid.bottomMask._height = grid.itmHgt - ( grid.getRowsHeightFromTo( 0, grid.numDisplayed - 1 ) + grid.header_mc._height - grid.initialHeight );
                           last_row.setMask( grid.bottomMask );
                        }
                        else
                        {
                           var bh = grid.getRowsHeightFromTo( 0, grid.numDisplayed - 1 ) + grid.header_mc._height - grid.initialHeight;

                           if( bh < grid.rows[ grid.numDisplayed - 1 ]._height )
                              grid.bottomMask._height = grid.rows[ grid.numDisplayed - 1 ]._height - bh;
                           else
                              grid.bottomMask._height = 1;

                           last_row.setMask( grid.bottomMask );
                           for( var i = 0; i < last_row.cells.length; i++ )
                           {
                              last_row.cells[i].labelField.autoSize = "none";

                              if( last_row.cells[i]._y >= grid.bottomMask._height )
                                 last_row.cells[i].labelField._height = 0;
                              else
                                 if( last_row.cells[i]._y + last_row.cells[i].labelField._height > grid.bottomMask._height )
                                    last_row.cells[i].labelField._height = grid.bottomMask._height - last_row.cells[i]._y;
                           }
                        }
                     }
                     else
                     {
                        grid.height = new_height;

                        var mm_func = grid.scrollBar_mc.scrollThumb_mc.onMouseMove;
                        grid.scrollBar_mc.setSize( grid.height );
                        grid.scrollBar_mc.scrollThumb_mc.onMouseMove = mm_func;

                        grid.boundingBox_mc._yscale = 100;
                        grid.boundingBox_mc._yscale = grid.height * 100 / grid.boundingBox_mc._height;
                     }

                  }

                  for( var i = 0; i < row.cells.length; i++ )
                  {
                     var vAlign = grid.colArray[i].styleTable.vertAlign.value;

                     if( vAlign == "top" )
                        row.cells[i]._y = 0;
                     else
                     {
                        var cellH = row.cells[i]._height;

                        if( row.cells[i].labelField.textHeight != undefined )
                           cellH = row.cells[i].labelField.textHeight + 5;

                        if( cellH - 5 == 0 )
                           this.cells[i]._y = 1;
                        else
                        {
                           if( vAlign == "bottom" )
                              row.cells[i]._y = Math.Max( 0, row.maxHeight - cellH );
                           else
                              row.cells[i]._y = Math.Max( 0, ( row.maxHeight - cellH ) / 2 );
                        }
                     }
                  }
               }
            }
         }
      }
   }

	Key.addListener(_global.FGridKeyListener);
   Key.addListener( grid.editCellListener );
}


// setting of horizontal grid lines
// needed for modification 7

FDataGridClass.prototype.showHorizGridLines = function(bool)
{
	this.gridLinesHoriz = bool;
	this.invalidate("drawColumns");
}

// puts down the header labels and background
// needed for modification 10

FDataGridClass.prototype.initHeaders = function()
{
	if(this.columnHeaders)
   {
      var tmp = this.createEmptyMovieClip("header_mc", 5);
      var maxHeight = 0;

      this.colDisplayed = 0;
      var x = 0;

      for( var i = this.leftDisplayed; i < this.colArray.length; i++ )
      {
         this.colDisplayed++;

         var old_x = x;

         x += this.colArray[i].width;

         var tmpStyle = this.colArray[i].styleTable.headerText.value;
         if (tmpStyle==undefined)
            tmpStyle = this.styleTable.headerText.value;

         var lbl = tmp.attachMovie(this.colArray[i].headerSymbol, "label"+i, i+3, {hostComponent:this, customTextStyle:tmpStyle, textStyle:tmpStyle});

         if( this.styleTable.headerWordWrap.value == true )
         {
            lbl.labelField.wordWrap = true;
            lbl.labelField.autoSize = true;
         }

         lbl.labelField.html = false;

         lbl.setSize( this.colArray[i].width - 5 );

         lbl.setValue( this.colArray[i].header );

         maxHeight = Math.max(maxHeight, lbl.labelField.textHeight + 5 );
         lbl.column = this.colArray[i];
         lbl.asc = false;
         this.colArray[i].lbl = lbl;
         lbl.controller = this;
         lbl.labelField.selectable = false;

         if (lbl.column.sortable && lbl.onPress==undefined)
         {
            lbl.useHandCursor = false;
            lbl.onPress = this.labelPress;
            lbl.onRelease = this.labelRelease;
            lbl.onDragOut = this.dragOutHeader;
            lbl.onReleaseOutside = this.labelUp;
            lbl.labelUp = this.labelUp;
         }

         lbl._x = old_x + 2;

         lbl.setSize( this.colArray[i].width - 5 );

         if( x > this.realWidth )  break;
      }

      if (this.colArray.length==0)
      {
         var tmpStyle = this.styleTable.columnHeader.value;
         var lbl = tmp.attachMovie("fLabelSymbol", "label", 3, {hostComponent:this, customTextStyle:tmpStyle, textStyle:tmpStyle});
         maxHeight = lbl._height;
         lbl.removeMovieClip();
      }

      if( this.styleTable.headerHeight.value == undefined )
         this.headerHeight = maxHeight;
      else
         this.headerHeight = this.styleTable.headerHeight.value;

      for( var i = this.leftDisplayed; i < this.colArray.length; i++ )
      {
         var label = this.header_mc["label"+i];

         var vAlign = this.styleTable.headerVertAlign.value;

         if( vAlign == "top" )
            label._y = 0;
         else
            if( vAlign == "bottom" )
               label._y = this.headerHeight - label.labelField.textHeight - 5;
            else
               label._y = ( this.headerHeight - label.labelField.textHeight - 5 ) / 2;

         if( label._x + label._width > this.realWidth )
         {
            label.labelField.autoSize = "none";

            if( label._x >= this.realWidth )
               label._width = 0;
            else
               label._width = this.realWidth - label._x;
         }

         if (i < this.colArray.length - 1 && this.colArray[i].styleTable.showHeaderSeparator.value != false)
         {
            var sep = tmp.attachMovie("HeaderSeperator", "sep"+i, 40+i+3);
            sep._height = this.headerHeight;
            this.registerSkinElement(sep, "gridLines");

            if (this.colArray[i].resizable && this.resizable)
            {
               sep.useHandCursor = false;
               sep.col = i;
               sep.controller = this;
               sep.onRollOver = this.showStretcher;
               sep.onPress = this.startSizing;
               sep.onRelease = sep.onReleaseOutside = this.stopSizing;
               sep.onRollOut = this.hideStretcher;
            }
         }
      }

		this.drawHeaderBG();
	}
   else
	{
   	this.header_mc.removeMovieClip();

      for( var i = this.leftDisplayed; i < this.colArray.length; i++ )
      {
         this.colDisplayed++;

         x += this.colArray[i].width;

         if( x > this.realWidth )  break;
      }
   }
}


// actions on vertical scrolling
// needed for modification 4,7,8,9

FDataGridClass.prototype.scrollHandler = function(scrollBar)
{
   if(this.topDisplayed == undefined)  return;

	var pos = scrollBar.getScrollPosition();
	var deltaPos = pos - this.topDisplayed;

	if ( deltaPos == 0 ) return;

	this.topDisplayed=pos;
	var scrollUp = deltaPos>0;
	deltaPos = Math.abs(deltaPos);

	if (deltaPos >= this.numDisplayed)
   {
		this.relayoutRows();
	}
   else
   {
		var tmpArray = new Array();
      var sum_height = 0;
      var delrows = 0;

      if(scrollUp)
      {
	    	var moveBlockLength = this.numDisplayed - deltaPos;
         var moveBlockDistance = this.getRowsHeightFromTo( 0, deltaPos - 1 );
         var shuffleBlockPos = this.getRowsHeightFromTo( deltaPos, this.numDisplayed - 1 );

         sum_height = this.header_mc._height + moveBlockDistance;

         var delrows2 = 0;

         for( var i = 0; i < this.numDisplayed; i++)
         {
            if( i < deltaPos )
            {
               if( shuffleBlockPos > this.realHeight - this.header_mc._height )
               {
                  if( this.rows[i] != undefined )
                  {
                     this.rows[i].removeMovieClip();
                     delete this.rows[i];
                     delrows2++;
                  }
               }
               else
               {
                  var newRow = i + moveBlockLength;
                  var newItem = this.topDisplayed + newRow;

                  tmpArray[newRow] = this.rows[i];
                  this.container_mc["fListItem" + newRow + "_mc"] = this.rows[i];
                  tmpArray[newRow].itemNum = newRow;

                  if( this.rowHeight == undefined || this.styleTable.autoSizeRows.value == true )
                     tmpArray[newRow].setSize( this.rows[i].width, this.itmHgt );

                  tmpArray[newRow]._y = shuffleBlockPos;

                  if( this.rows[i].maxHeight == undefined )
                     shuffleBlockPos += this.itmHgt;
                  else
                     shuffleBlockPos += this.rows[i].maxHeight;

                  tmpArray[newRow].drawItem(this.getItemAt(newItem), this.isSelected(newItem));

                  tmpArray[newRow].swapDepths( 10 + newRow );
               }
            }
            else
            {
               var distance = 0;

               if( this.styleTable.gridHeightAutosizing.value != false )
                  distance = this.rows[ i ].maxHeight / 2;

               if( !delrows )
               {
                  var newRow = i - deltaPos;
                  tmpArray[newRow] = this.rows[i];
                  this.container_mc["fListItem" + newRow + "_mc"] = this.rows[i];
                  tmpArray[newRow].itemNum = newRow;

                  if( i == this.numDisplayed - 1 )
                     for( var j = 0; j < this.rows[i].cells.length; j++ )
                     {
                        if( this.colArray[j].width > 25 )
                           this.rows[i].cells[j].labelField.autoSize = this.styleTable.autoSizeRows.value;

                        if( this.styleTable.cellWordWrap.value == true && this.rows[i].cells[j].labelField.textHeight > this.rows[i].cells[j].labelField._height )
                           this.rows[i].cells[j].labelField._height = this.rows[i].cells[j].labelField.textHeight + 5;

                        if(this.colArray[j].width <= 25 && this.rows[i].cells[j].labelField.textHeight > 0 )
                           this.rows[i].cells[j].labelField._height = this.rows[i].cells[j].labelField.textHeight + 5;
                     }

                  this.rows[i]._y -= moveBlockDistance;

                  if( this.rows[i].maxHeight == undefined )
                     sum_height += this.itmHgt;
                  else
                     sum_height += this.rows[i].maxHeight;

                  tmpArray[newRow].swapDepths( 10 + newRow );
               }
               else
               {
                  if( this.rows[i] != undefined )
                  {
                     this.rows[i].removeMovieClip();
                     delete this.rows[i];
                     delrows++;
                  }
               }
            }
         }

         delrows += delrows2;
      }
      else
      {
         var moveBlockDistance = 0;
         var moveBlockLength = 0;
         var x_length = this.numDisplayed - deltaPos;

         var delrows2 = 0;
         var i = 0;

         while( i < deltaPos )
         {
            if( moveBlockDistance < this.realHeight - this.header_mc._height )
            {
               moveBlockLength++;

               this.rows[ x_length + i ]._y = moveBlockDistance;
               this.rows[ x_length + i ].itemNum = i;

               if( this.rowHeight == undefined || this.styleTable.autoSizeRows.value == true )
                  this.rows[ x_length + i ].setSize( this.rows[ 0 ].width, this.itmHgt );

               if( this.rows[ x_length + i ].maxHeight == undefined )
                  moveBlockDistance += this.itmHgt;
               else
                  moveBlockDistance += this.rows[ x_length + i ].maxHeight;
            }
            else
            {
               this.rows[ x_length + i ].removeMovieClip();
               delete this.rows[ x_length + i ];
               delrows2++;
            }

            i++;
         }

         sum_height = this.header_mc._height + moveBlockDistance;

         for( var i = 0; i < this.numDisplayed; i++)
         {
            if( i >= x_length )
            {
               var newRow = i - x_length;
               var newItem = this.topDisplayed + newRow;

               tmpArray[newRow] = this.rows[i];
               this.container_mc["fListItem" + newRow + "_mc"] = this.rows[i];
               tmpArray[newRow].itemNum = newRow;

               tmpArray[newRow].drawItem(this.getItemAt(newItem), this.isSelected(newItem));

               tmpArray[newRow].swapDepths( 10 + newRow );
            }
            else
            {
               var distance = 0;

               if( this.styleTable.gridHeightAutosizing.value != false )
                  distance = this.rows[ i ].maxHeight / 2;

               if( !delrows && sum_height < this.realHeight - distance )
               {
                  var newRow = i + deltaPos;
                  tmpArray[newRow] = this.rows[i];
                  this.container_mc["fListItem" + newRow + "_mc"] = this.rows[i];
                  tmpArray[newRow].itemNum = newRow;

                  this.rows[i]._y += moveBlockDistance;

                  if( this.rows[i].maxHeight == undefined )
                     sum_height += this.itmHgt;
                  else
                     sum_height += this.rows[i].maxHeight;

                  tmpArray[newRow].swapDepths( 10 + newRow );
               }
               else
                  if( this.rows[i] != undefined )
                  {
                     this.rows[i].removeMovieClip();
                     delete this.rows[i];
                     delrows++;
                  }
            }
         }

         delrows += delrows2;
      }

      this.numDisplayed -= delrows;

		this.rows = tmpArray;

      var new_height = this.getRowsHeightFromTo( 0, this.numDisplayed - 1 ) + this.header_mc._height;

      if( this.rowHeight == undefined || this.styleTable.autoSizeRows.value == true )
         while( new_height < this.realHeight )
         {
            var ins = this.numDisplayed;

            this.idSequence++;
            this.rows[ ins ] = this.container_mc.attachMovie( this.itemSymbol, "row" + this.idSequence + "_mc", 10 + ins, {controller:this,itemNum:ins});

            this.rows[ ins ]._y = new_height - this.header_mc._height;
            this.rows[ ins ].setSize( this.rows[ 0 ].width, this.itmHgt );

            var distance = this.itmHgt;

            if( this.rows[ ins ].maxHeight != undefined )
               distance = this.rows[ ins ].maxHeight;

            if( this.styleTable.gridHeightAutosizing.value != false )
               distance = this.rows[ ins ].maxHeight / 2;

            if( new_height + this.rows[ ins ].maxHeight - this.realHeight < distance )
            {
               this.container_mc[ "fListItem" + ins + "_mc" ] = this.rows[ ins ];
               this.rows[ ins ].drawItem( this.getItemAt( ins + this.topDisplayed ), this.isSelected( ins + this.topDisplayed ) );

               if( this.rows[ ins ].maxHeight == undefined )
                  new_height += this.itmHgt;
               else
                  new_height += this.rows[ this.numDisplayed ].maxHeight;

               this.numDisplayed++;
            }
            else
            {
               this.rows[ ins ].removeMovieClip();
               delete this.rows[ ins ];
               break;
            }
         }

      if( this.styleTable.gridHeightAutosizing.value == false )
      {
         var last_row = this.rows[ this.numDisplayed - 1 ];
    		this.bottomMask._y = last_row._y;

         if( this.topDisplayed + this.numDisplayed > this.dataProvider.getLength() )
         {
            this.bottomMask._height = this.itmHgt - ( new_height - this.realHeight );
            last_row.setMask( this.bottomMask );
         }
         else
         {
            this.bottomMask._height = last_row.maxHeight - ( new_height - this.realHeight );
            last_row.setMask( this.bottomMask );

            for( var i = 0; i < last_row.cells.length; i++ )
            {
               last_row.cells[i].labelField.autoSize = "none";

               if( last_row.cells[i]._y >= this.bottomMask._height )
                  last_row.cells[i].labelField._height = 0;
               else
                  if( last_row.cells[i]._y + last_row.cells[i].labelField._height > this.bottomMask._height )
                     last_row.cells[i].labelField._height = this.bottomMask._height - last_row.cells[i]._y;
            }
         }
      }
      else
      {
         this.height = new_height;

         this.scrollBar_mc.width = this.height;
         this.scrollBar_mc.scrollTrack_mc._yscale = 100;
         this.scrollBar_mc.scrollTrack_mc._yscale = 100 * this.scrollBar_mc.width / this.scrollBar_mc.scrollTrack_mc._height;
         this.scrollBar_mc.downArrow_mc._y = this.scrollBar_mc.width - this.scrollBar_mc.downArrow_mc._height;
         this.scrollBar_mc.trackSize = this.scrollBar_mc.width - (2 * this.scrollBar_mc.downArrow_mc._height);

      	this.boundingBox_mc._yscale = 100;
       	this.boundingBox_mc._yscale = this.height * 100 / this.boundingBox_mc._height;
      }

      if( this.showHScroll == true )
      {
         var last_col = this.leftDisplayed + this.colDisplayed - 1;

         for( var i = 0; i < this.numDisplayed; i++ )
            if( this.rows[i].cells[last_col]._x + this.rows[i].cells[last_col]._width > this.realWidth )
            {
               this.rows[i].cells[last_col].labelField.autoSize = "none";

               if( this.rows[i].cells[last_col]._x >= this.realWidth )
                  this.rows[i].cells[last_col]._width = 0;
               else
                  this.rows[i].cells[last_col]._width = this.realWidth - this.rows[i].cells[last_col]._x;
            }
      }
	}

	this.lastPosition = pos;

}


// cell's link handler setting
// needed for modification #11

FDataGridClass.prototype.setCellClickHandler = function( col, row, handlerObj, handlerFunc )
{
	if( this.cellStyles == undefined )
		this.cellStyles = new Array();

	var found;

	for( var i = 0; i < this.cellStyles.length; i++ )
		if( this.cellStyles[i].colIndex == col && this.cellStyles[i].rowIndex == row )
			found = i;

	if( found == undefined )
	{
		var cell_style = new RowCellStyle();
		cell_style.colIndex = col;
		cell_style.rowIndex = row;
		cell_style.rowID = this.dataProvider.getItemID( row )
		found = this.cellStyles.push( cell_style ) - 1;
	}

	var cell_style = this.cellStyles[ found ];
	cell_style.clickHandlerObj = handlerObj;
	cell_style.clickHandlerFunc = handlerFunc;

	return undefined;
}

// actions on column's style property setting ( invalidation )
// needed for modification #1,3,6,8,10

FGridColumn.prototype.setCustomStyleProperty = function(propName, value)
{
	if( propName == "headerText" )
   {
		this.parentGrid.invalidate("setSize");
		return true;
	}
	else
		if(  propName == "vertAlign" )
		{
			this.parentGrid.invalidate("relayoutRows");
			return true;
		}
	else
		if( propName == "background"
			|| propName == "headerBG"
			|| propName == "isRowAlternationVisible"
			|| propName == "lineColor"
			|| propName == "lineThick"
			|| propName == "vertAlign"
		)
         {
            this.parentGrid.invalidate("drawColumns");
            return true;
         }
         else
            if( propName.subString(0,4) == "text" )
            {
               if (this.textStyle==undefined)
                  this.textStyle = new TextFormat();

               var textProp = propName.subString(4, propName.length);
               this.textStyle[textProp] = value;
               this.parentGrid.invalidate("setSize");
               return true;
            }
            else
               if( propName == "displayPrefix"
                   || propName == "displayPostfix"
                   || propName == "displayFactor"
                   || propName == "displayConverter"
                 )
               {
                  this.parentGrid.invalidate("updateControl");
                  return true;
               }
}

// actions on grid's style property setting ( invalidation )
// needed for modification #5

FDataGridClass.prototype.setStyleProperty = function(propName, value, isGlobal)
{
   super.setStyleProperty(propName, value, isGlobal);

   if( propName == "autoSizeRows" )
   {
      if (this.styleTable["cellWordWrap"] == undefined)
      {
         this.styleTable["cellWordWrap"] = new Object();
         this.styleTable["cellWordWrap"].useGlobal = true;
      }

      this.styleTable["cellWordWrap"].value = value;
   }

   if( propName == "cellWordWrap" )
   {
      if (this.styleTable["autoSizeRows"] == undefined)
      {
         this.styleTable["autoSizeRows"] = new Object();
         this.styleTable["autoSizeRows"].useGlobal = true;
      }

      this.styleTable["autoSizeRows"].value = value;
   }
}


FDataGridClass.prototype.setCustomStyleProperty = function(propName, value)
{
   if( propName == "editableBorder"
       || propName == "editableBorderColor"
       || propName == "editableBackground"
       || propName == "editableBackgroundColor"
     )
   {
      this.invalidate("updateControl");
      return true;
   }
   else
      if( propName == "gridlines"
          || propName == "header"
        )
      {
         this.invalidate("drawColumns");
         return true;
      }
      else
         if( propName == "autoSizeRows"
             || propName == "cellWordWrap"
           )
         {
            this.setStyleProperty("gridHeightAutosizing", false );
            return true;
         }
         else
            if( propName == "headerHeight" )
            {
               this.invalidate("setSize");
               return true;
            }
}

// actions on grid's style property setting ( invalidation )
// needed for modification #5

FGridColumn.prototype.setStyleProperty = function(propName, value, isGlobal)
{
	if( value == "" || value == undefined )
   {
      if( propName == "displayPrefix"
          || propName == "displayPostfix"
          || propName == "displayFactor"
          || propName == "displayConverter"
        )
      {
         this.parentGrid.invalidate("updateControl");
         delete this.styleTable[propName];
      }

      return;
   }

   if( propName == "displayFactor" )
   {
   	var tmpValue = parseFloat(value);

     	if (!isNaN(tmpValue))
		   value = tmpValue;
   }
   else
   {
   	var tmpValue = parseInt(value);

     	if (!isNaN(tmpValue))
		   value = tmpValue;
   }

	var global = (arguments.length>2) ? isGlobal : false;

	if (this.styleTable[propName]==undefined)
   {
		this.styleTable[propName] = new Object();
		this.styleTable[propName].useGlobal=true;
	}

	if (this.styleTable[propName].useGlobal || !global)
   {
		this.styleTable[propName].value = value;

		if( this.setCustomStyleProperty(propName, value) )
      {
			// a hook for extending further styleProperty reactions.
		}
      else
         if( propName == "embedFonts"
             || propName == "columnBold"
             || propName == "columnItalic"
             || propName == "noWrap"
           )
			   this.invalidate("setSize");
		   else
            if( propName == "showHeaderSeparator" )
               this.invalidate("initHeaders");
            else
               if (propName.subString(0,4)=="text")
               {
                  if (this.textStyle==undefined)
                  {
                     this.textStyle = new TextFormat();
                  }

                  var textProp = propName.subString(4, propName.length);
                  this.textStyle[textProp] = value;
                  this.invalidate("setSize");
               }
               else
               {
                  for (var j in this.styleTable[propName].coloredMCs)
                  {
                     var myColor = new Color(this.styleTable[propName].coloredMCs[j]);
                     if (this.styleTable[propName].value==undefined)
                     {
                        var myTObj = { ra: '100', rb: '0', ga: '100', gb: '0', ba: '100', bb: '0', aa: '100', ab: '0'};
                        myColor.setTransform(myTObj);
                     }
                     else
                     {
                        myColor.setRGB(value);
                     }
                  }
               }

		this.styleTable[propName].useGlobal = global;
	}
}

FDataGridClass.prototype.getRowsHeightFromTo = function(from, to)
{
   var res = 0;

   for( var i = from; i <= to; i++ )
      if( this.rows[i].maxHeight == undefined )
         res += this.itmHgt;
      else
         res += this.rows[i].maxHeight;

   return res;
}

FDataGridClass.prototype.stopSizing = function()
{
	if (!this.controller.isStretchable(this.col) || !this.controller.enable)
		return;

	this.controller.stretchBar._visible = false;
	var widthChange = this.controller.stretchBar._x - this.oldX;
	this.onRollOut();
	this.controller.resizeColumn(this.col, this.controller.colArray[this.col].width+widthChange);

   if( this.controller.styleTable.autoSizeRows.value == true )
   	this.controller.invalidate("setSize");
}

FDataGridClass.prototype.initScrollBar = function()
{
	if (this.scrollBar_mc==undefined) {
		this.container_mc.attachMovie("FScrollBarSymbol","scrollBar_mc",3000, {hostStyle:this.styleTable}); // 3000 is arbitrarily high
		this.scrollBar_mc = this.container_mc.scrollBar_mc;
		this.scrollBar_mc.setChangeHandler("scrollHandler", this);
		this.scrollBar_mc.setSize(this.height);
		this.scrollBar_mc._x = this.width - this.scrollBar_mc._width;
		this.scrollBar_mc._y = 0;
		this.scrollBar_mc.setLargeScroll(this.numDisplayed-1);
		this.scrollOffset = this.scrollBar_mc._width;
	}

	if ( (( (!this.permaScrollBar || this.showVScroll!=true) && this.getLength()<=this.numDisplayed) ) || this.showVScroll==false) {
		this.realWidth = this.width;
		this.scrollBar_mc._visible = false;
	} else {
		this.scrollBar_mc._visible = true;
		this.realWidth = this.width-this.scrollBar_mc._width;
		this.scrollBar_mc.scrollPosition = this.getScrollPosition();
		this.scrollBar_mc.setScrollProperties( this.numDisplayed, 0, this.getLength() - 1 );
	}

//	for ( var i = 0; i < this.numDisplayed; i++)
//		this.rows[i].setSize( this.realWidth, this.itmHgt );

//	this.invalidate("updateControl");
}


FDataGridClass.prototype.clickHandler = function(itmNum)
{
	this.focusRect.removeMovieClip();
	if (!this.focused) {
		this.pressFocus();
	}
	this.selectionHandler(itmNum);
	this.onMouseUp = this.releaseHandler;
}

FDataGridClass.prototype.invalidate = function(methodName)
{
	this.methodTable[methodName] = true;

   if( methodName == "relayoutRows" )
   {
      delete this.methodTable["initScrollBar"];
      delete this.methodTable["updateControl"];
   }

	this.onEnterFrame = this.cleanUI;
}

FLabelClass.prototype.setValue = function(label)
{
	this.labelField.text = label;

   if( this.hostComponent.styleTable.html.value == true )
   	this.labelField.htmlText = label;
}

FDataGridClass.prototype.setScrollPosition = function(pos)
{
	if (this.enable) {
		pos = Math.min(pos, this.getLength());
		pos = Math.max(pos,0);
		this.scrollBar_mc.setScrollPosition(pos);
	}
}

FDataGridClass.prototype.setHScrollPosition = function(pos)
{
	if (this.enable) {
		pos = Math.min(pos, this.colArray.length);
		pos = Math.max(pos,0);
		this.scrollBarH_mc.setScrollPosition(pos);
	}
}

FDataGridClass.prototype.setDataProvider = function(dP)
{
	super.setDataProvider(dP);
	this.generateCols();
   this.invalidate("relayoutRows");
}

FDataGridClass.prototype.initHScrollBar = function()
{
	if (this.scrollBarH_mc == undefined)
   {
		this.container_mc.attachMovie("FScrollBarSymbol","scrollBarH_mc",3001, {hostStyle:this.styleTable}); // 3000 is arbitrarily high
		this.scrollBarH_mc = this.container_mc.scrollBarH_mc;
		this.scrollBarH_mc._rotation = 270;
		this.scrollBarH_mc.setChangeHandler("hScrollHandler", this);
		this.scrollBarH_mc.setSize( this.realWidth + 1 );
		this.scrollBarH_mc._x = 0;

		this.scrollBarH_mc.setLargeScroll( this.colDisplayed - 1 );
		this.scrollOffsetH = this.scrollBarH_mc._height;
	}

   this.scrollBarH_mc._y = this.height - this.header_mc._height + 2;

   this.scrollBarH_mc.scrollPosition = this.leftDisplayed;
	this.scrollBarH_mc.setScrollProperties( 1, 0, this.colArray.length - 1 );

   this.scrollBarH_mc.scrollThumb_mc.onMouseUp = this.scrollBarH_mc.scrollThumb_mc.onRelease;
}

FDataGridClass.prototype.resizeColumn = function(col, w)
{
   if( this.leftDisplayed == undefined )
      this.leftDisplayed = 0;

	var startX = 0;
	for (var i = this.leftDisplayed; i < col; i++) {
		startX += this.colArray[i].width;
	}
	var origSpace = this.realWidth - startX - this.colArray[col].width;
	var newSpace = this.realWidth - startX - w;
	this.colArray[col].width = w;

   if( this.showHScroll != true )
   {
      for (var i=col+1; i < this.colArray.length; i++) {
         if (!this.colArray[i].resizable) {
            newSpace-=this.colArray[i].width;
            origSpace-=this.colArray[i].width;
         }
      }

      var totX = 0;
      for (var i=col+1; i<this.colArray.length; i++) {
         if (this.colArray[i].resizable) {

            this.colArray[i].width = this.colArray[i].width * newSpace / origSpace;

            totX += this.colArray[i].width;
         }
      }

      var deadSpace = 0;
      var buffer = 0;
      var filled = false;
      for( var i = this.colArray.length - 1; i >= 0; i-- )
      {
         if (this.colArray[i].resizable)
         {
            if (!filled)
            {
               this.colArray[i].width += newSpace - totX;
               filled = true;
            }

            if (buffer > 0)
            {
               this.colArray[i].width -= buffer;
               buffer=0;
            }

            if (this.colArray[i].width<this.minColWidth)
            {
               buffer += this.minColWidth-this.colArray[i].width;
               this.colArray[i].width = this.minColWidth;
            }
         }
         else
         {
            deadSpace+=this.colArray[i].width;
         }
      }
   }

	this.invalidate("drawColumns");
}

FDataGridClass.prototype.hScrollHandler = function(scrollBar)
{
   if(this.leftDisplayed == undefined)  return;

	var pos = scrollBar.getScrollPosition();
	var deltaPos = pos - this.leftDisplayed;

	if ( deltaPos == 0 ) return;

	this.leftDisplayed = pos;
	this.initHeaders();

	if(this.columnHeaders)
		this.container_mc._y = this.header_mc._height;
   else
		this.container_mc._y = 1;

	this.relayoutRows();
	this.invalidate("drawColumns");
}


