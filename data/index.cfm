<cfsetting showdebugoutput="no">

<title>Ops Intranet Menu</title>

<cfquery name="GetItems" datasource="#ds#">
	select * 
	from OpsMenuItems
	order by itemid asc 
</cfquery>

<cfset dataAccessList = "Phone Extension List,All Lines Search,Create an Equipment Trouble Ticket,Equipment Trouble Ticket Help and Training Documentation">

<link rel="stylesheet" type="text/css" href="../styles.css">

<div align="center">
<table width="400" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center" class="highlightbar"><b>Data Entry Menu</b></td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<cfoutput query="getItems">
				<cfif trim(itemURL) is "">
					<cfset location = "/Ops/files/#fileName#">
				<cfelse>
					<cfset location = itemURL>
				</cfif>
				<cfif listfindnocase(dataAccessList,trim(itemName)) is not 0>
					<tr>
						<td width="4%" class="linedrow">#getItems.currentrow#.</td>
						<td width="96%" class="linedrow"><a target="_blank" style="text-decoration:underline" href="#location#">#itemName#</a></td>
					</tr>
				</cfif>
				</cfoutput>
			</table>
		</td>
	</tr>
</table>
</div>
