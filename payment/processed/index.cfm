
<link rel="stylesheet" type="text/css" href="../../styles.css">

<cfparam name="start" default="1">
<cfparam name="rows" default="25">
<cfif start LT 1><cfset start = 1></cfif>

<cfparam name="o" default="dtd">

<cfswitch expression="#o#">
	<cfcase value="tida"><cfset orderby = "order by paymentid ASC"></cfcase>
	<cfcase value="tidd"><cfset orderby = "order by paymentid DESC"></cfcase>
	<cfcase value="dta"><cfset orderby = "order by datetime ASC"></cfcase>
	<cfcase value="dtd"><cfset orderby = "order by datetime DESC"></cfcase>
	<cfcase value="pda"><cfset orderby = "order by datetimeprocessed ASC"></cfcase>
	<cfcase value="pdd"><cfset orderby = "order by datetimeprocessed DESC"></cfcase>
	<cfcase value="ta"><cfset orderby = "order by type ASC"></cfcase>
	<cfcase value="td"><cfset orderby = "order by type DESC"></cfcase>
	<cfcase value="aa"><cfset orderby = "order by convert(money,amount) ASC"></cfcase>
	<cfcase value="ad"><cfset orderby = "order by convert(money,amount) DESC"></cfcase>
</cfswitch>

<cfset yearBack = dateadd("yyyy",-1,now())>

<!--- get processed records --->
<cfquery name="getinfo" datasource="copalink">
	select paymentid, type, amount, datetime, dateTimeProcessed
	from Payments
	where processed = 1 and dateTimeProcessed >= '#dateformat(yearBack,'mm/dd/yyyy')#'
	#orderby#
</cfquery>

<cfset pages = ceiling(getinfo.recordcount / rows)>

<div align="center"> 

<table width="725" border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td align="center" class="highlightbar"><b>Processed Transactions - click on column headings to sort </b></td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
		<table width="100%" border="0" cellpadding="5" cellspacing="0" class="grey">
			<cfoutput>
			<tr bgcolor="FFFFCC">
				<td width="20%" align="center" nowrap="nowrap" class="horizontalnavbuttons">
					
					<b>
					<cfif o IS "tida">
						<a href="index.cfm?start=#start#&o=tidd">Transaction ID</a>
						<cfelse>
						<a href="index.cfm?start=#start#&o=tida">Transaction ID</a>
					</cfif>
					</b></td>
				<td width="20%" align="center" nowrap="nowrap" class="horizontalnavbuttons">
					
					<b>
					<cfif o IS "dta">
						<a href="index.cfm?start=#start#&o=dtd">Payment Date</a>
						<cfelse>
						<a href="index.cfm?start=#start#&o=dta">Payment Date</a>
					</cfif>
					</b></td>
				<td width="20%" align="center" nowrap="nowrap" class="horizontalnavbuttons">
					
					<b>
					<cfif o IS "pda">
						<a href="index.cfm?start=#start#&o=pdd">Processed Date</a>		
						<cfelse>
						<a href="index.cfm?start=#start#&o=pda">Processed Date</a>
					</cfif>
					</b></td>
				<td width="20%" align="center" nowrap="nowrap" class="horizontalnavbuttons">
					
					<b>
					<cfif o IS "ta">
						<a href="index.cfm?start=#start#&o=td">Type</a>		
						<cfelse>
						<a href="index.cfm?start=#start#&o=ta">Type</a>
					</cfif>
					</b></td>
				<td width="20%" align="center" nowrap="nowrap" class="horizontalnavbuttons">
					
					<b>
					<cfif o IS "aa">
						<a href="index.cfm?start=#start#&o=ad">Amount</a>
						<cfelse>
						<a href="index.cfm?start=#start#&o=aa">Amount</a>
					</cfif>
					</b></td>
			</tr>
			</cfoutput>
			<cfif getinfo.recordcount GT 0>
			<cfoutput query="getinfo" startrow="#start#" maxrows="#rows#">
			<tr>
				<td align="center" class="linedrow">
				<cfif getinfo.type IS "check">
					<a href="checkdetail.cfm?tid=#getinfo.paymentid#&start=#start#">#getinfo.paymentid#</a>		
				<cfelseif getinfo.type IS "credit card">
					<a href="ccdetail.cfm?tid=#getinfo.paymentid#&start=#start#">#getinfo.paymentid#</a>
				</cfif>
				</td>
				<td align="center" class="linedrow">#dateformat(getinfo.datetime, 'mm/dd/yyyy')#</td>
				<td align="center" class="linedrow"><cfif trim(getinfo.dateTimeProcessed) IS NOT "">#dateformat(getinfo.dateTimeProcessed, 'mm/dd/yyyy')#<cfelse>&nbsp;</cfif></td>
				<td align="center" class="linedrow">
				<cfif getinfo.type IS "check">
					Check
				<cfelseif getinfo.type IS "credit card">
					Credit Card
				</cfif>
				</td>
				<td align="right" class="linedrow">$#getinfo.amount#</td>
			</tr>
			</cfoutput>
			<cfelse>
			<tr>
				<td colspan="5"><b>There are no processed transactions on file</b></td>
			</tr>
			</cfif>
		</table>
		</td>
	</tr>
</table>

<br>
<cfif pages GT 1>
<table width="500" border="0" cellpadding="5" cellspacing="0">
<tr>
	<td align="center">
	<cfloop index="p" from="1" to="#pages#">
	<cfoutput>
	<a href="index.cfm?start=#evaluate((p * rows) - (rows-1))#&o=#o#">#p#</a>
	<cfif p IS NOT pages> | </cfif>
	</cfoutput>
	</cfloop>
	</td>
</tr>
</table>
</cfif>
<br>
<table width="500" border="0" cellpadding="5" cellspacing="0">
<tr>
	<td align="center"><a href="/index.cfm">Return to Administrative Menu</a></td>
</tr>
</table>
</div>
