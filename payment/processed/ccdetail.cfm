<link rel="stylesheet" type="text/css" href="../../styles.css">

<cfquery name="getinfo" datasource="copalink">
	select Payments.*, Payments_CC_Detail.* 
	from Payments, Payments_CC_Detail
	where Payments.paymentid = #url.tid# AND Payments.paymentid = Payments_CC_Detail.paymentid
</cfquery>

<div align="center">
	
	<table width="600" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td class="highlightbar">&nbsp;</td>
		</tr>
		<tr>
			<td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<cfoutput query="getinfo">
				<tr>
					<td width="23%" nowrap="nowrap">Transaction ID </td>
					<td width="77%">#getinfo.paymentid#</td>
				</tr>
				<tr>
					<td nowrap="nowrap">Payment Date/Time</td>
					<td>#dateformat(getinfo.datetime,'mm/dd/yyyy')# at #timeformat(getinfo.datetime, 'h:mm tt')#</td>
				</tr>
				<tr>
					<td nowrap="nowrap">Processed Date/Time</td>
					<td>#dateformat(getinfo.dateTimeProcessed,'mm/dd/yyyy')# at #timeformat(getinfo.dateTimeProcessed, 'h:mm tt')#</td>
				</tr>
				<tr>
					<td nowrap="nowrap">Name or Company </td>
					<td>#getinfo.companyName#</td>
				</tr>
				<tr>
					<td nowrap="nowrap">Dealer Number</td>
					<td>#getinfo.dealernumber#</td>
				</tr>
				<tr>
					<td nowrap="nowrap">Credit Card Number </td>
					<td>#getinfo.cardNumber#</td>
				</tr>
				<tr>
					<td nowrap="nowrap">Credit Card Expiration</td>
					<td>#getinfo.expMM#/#getinfo.expYY#</td>
				</tr>
				<tr>
					<td nowrap="nowrap">CVV2</td>
					<td><cfif trim(getinfo.cvv2) IS NOT "">#getinfo.cvv2#<cfelse>&nbsp;</cfif></td>
				</tr>
				<tr>
					<td nowrap="nowrap">Card Holder</td>
					<td>#getinfo.cardHolder#</td>
				</tr>
				 <tr>
					<td nowrap="nowrap">Address</td>
					<td>#getinfo.address#</td>
				</tr>
				<tr>
					<td nowrap="nowrap">Invoice Number </td>
					<td>#getinfo.invoiceNumber#</td>
				</tr>
				<tr>
					<td nowrap="nowrap">Amount</td>
					<td>$#getinfo.amount#</td>
				</tr>
				<tr>
					<td nowrap="nowrap">Authorized By </td>
					<td>#getinfo.authorizedBy#</td>
				</tr>
				<tr>
					<td nowrap="nowrap">Email Address</td>
					<td>#getinfo.email#</td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap">Comments</td>
					<td valign="top"><cfif trim(getinfo.comments) IS NOT "">#replace(getinfo.comments, "#chr(13)#", "<br>", "all")#<cfelse>&nbsp;</cfif></td>
				</tr>
				</cfoutput>
			</table>
			</td>
		</tr>
	</table>

	<br>

	<cfoutput>
    <table border="0" cellpadding="5" cellspacing="0">
        <tr>
            <!--- <td width="50%" align="center"><!--- <a href="process.cfm?tid=#url.tid#&start=#url.start#">Mark Processed</a> --->&nbsp;</td> --->
            <td align="center"><a href="index.cfm?start=#url.start#">Return to Transactions </a></td>
        </tr>
    </table>
	</cfoutput>
</div>
