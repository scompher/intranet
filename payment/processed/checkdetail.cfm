<link rel="stylesheet" type="text/css" href="../../styles.css">

<cfquery name="getinfo" datasource="copalink">
	select Payments.*, Payments_Check_Detail.* 
	from Payments, Payments_Check_Detail
	where Payments.paymentid = #url.tid# AND Payments.paymentid = Payments_Check_Detail.paymentid
</cfquery>

<div align="center">

	<table width="600" border="0" cellpadding="5" cellspacing="0">
		<tr>
			<td class="highlightbar"><b>Transaction Detail</b></td>
		</tr>
		<tr>
			<td class="greyrowbottomnopadding">
				<table width="100%" border="0" cellpadding="5" cellspacing="0">
					<cfoutput query="getinfo">
					<tr>
						<td width="23%" nowrap="nowrap">Transaction ID </td>
						<td width="77%">#getinfo.paymentid#</td>
					</tr>
					<tr>
						<td nowrap="nowrap">Payment Date/Time</td>
						<td>#dateformat(getinfo.datetime,'mm/dd/yyyy')# at #timeformat(getinfo.datetime, 'h:mm tt')#</td>
					</tr>
					<tr>
						<td nowrap="nowrap">Processed Date/Time</td>
						<td>#dateformat(getinfo.dateTimeProcessed,'mm/dd/yyyy')# at #timeformat(getinfo.dateTimeProcessed, 'h:mm tt')#</td>
					</tr>
					<tr>
						<td nowrap="nowrap">Amount</td>
						<td>$#getinfo.amount#</td>
					</tr>
					<tr>
						<td nowrap="nowrap">Email Address</td>
						<td>#getinfo.email#</td>
					</tr>
					<tr>
						<td nowrap="nowrap">Name or Company </td>
						<td>#getinfo.name#</td>
					</tr>
					<tr>
						<td nowrap="nowrap">Address</td>
						<td>#getinfo.address#</td>
					</tr>
					<tr>
						<td nowrap="nowrap">City, State, Zip </td>
						<td>#getinfo.city#, #getinfo.state#, #getinfo.zip#</td>
					</tr>
					<tr>
						<td nowrap="nowrap">Phone/Fax</td>
						<td>#getinfo.phoneFax#</td>
					</tr>
					<tr>
						<td nowrap="nowrap">Bank Name</td>
						<td>#getinfo.bankName#</td>
					</tr>
					<tr>
						<td nowrap="nowrap">Bank City/State </td>
						<td>#getinfo.bankCityState#</td>
					</tr>
					<tr>
						<td nowrap="nowrap">Check Number </td>
						<td>#getinfo.checkNum#</td>
					</tr>
					<tr>
						<td nowrap="nowrap">ABA/Transit/Routing ##</td>
						<td>#getinfo.ABATransitRoutingNum#</td>
					</tr>
					<tr>
						<td nowrap="nowrap">Account Number</td>
						<td>#getinfo.accountNumber#</td>
					</tr>
					<tr>
						<td nowrap="nowrap">Dealer Number </td>
						<td>#getinfo.dealernumber#</td>
					</tr>
					<tr>
						<td nowrap="nowrap">Invoice Number </td>
						<td><cfif trim(getinfo.invoiceNumber) IS "">&nbsp;<cfelse>#getinfo.invoiceNumber#</cfif></td>
					</tr>
					<tr>
						<td valign="top" nowrap="nowrap">Comments</td>
						<td valign="top"><cfif trim(getinfo.comments) IS NOT "">#replace(getinfo.comments, "#chr(13)#", "<br>", "all")#<cfelse>&nbsp;</cfif></td>
					</tr>
					</cfoutput>
				</table>
			</td>
		</tr>
	</table>

	<br>

	<cfoutput>
    <table border="0" cellpadding="5" cellspacing="0">
        <tr>
            <td align="center"><a href="index.cfm?start=#url.start#">Return to Transactions </a></td>
        </tr>
    </table>
	</cfoutput>
</div>

