
<cfquery name="getinfo" datasource="copalink">
	select Payments.*, Payments_Check_Detail.* 
	from Payments, Payments_Check_Detail
	where Payments.paymentid = #url.tid# AND Payments.paymentid = Payments_Check_Detail.paymentid
</cfquery>

<div align="center">
    <table border="0" cellpadding="5" cellspacing="0">
        <tr>
            <td align="center"><b>Transaction Detail</b></td>
        </tr>
    </table>
	<cfoutput query="getinfo">
    <table border="1" cellpadding="5" cellspacing="0">
        <tr>
            <td width="35%" bgcolor="FFFFCC"><b>Transaction ID </b></td>
            <td>#getinfo.paymentid#</td>
        </tr>
        <tr>
            <td bgcolor="FFFFCC"><b>Payment Date/Time</b></td>
            <td>#dateformat(getinfo.datetime,'mm/dd/yyyy')# at #timeformat(getinfo.datetime, 'h:mm tt')#</td>
        </tr>
        <tr>
            <td bgcolor="FFFFCC"><b>Amount</b></td>
            <td>$#getinfo.amount#</td>
        </tr>
        <tr>
            <td bgcolor="FFFFCC"><b>Email Address</b></td>
            <td>#getinfo.email#</td>
        </tr>
        <tr>
            <td bgcolor="FFFFCC"><b>Name or Company </b></td>
            <td>#getinfo.name#</td>
        </tr>
        <tr>
            <td bgcolor="FFFFCC"><b>Address</b></td>
            <td>#getinfo.address#</td>
        </tr>
        <tr>
            <td bgcolor="FFFFCC"><b>City, State, Zip </b></td>
            <td>#getinfo.city#, #getinfo.state#, #getinfo.zip#</td>
        </tr>
        <tr>
            <td bgcolor="FFFFCC"><b>Phone/Fax</b></td>
            <td>#getinfo.phoneFax#</td>
        </tr>
        <tr>
            <td bgcolor="FFFFCC"><b>Bank Name</b></td>
            <td>#getinfo.bankName#</td>
        </tr>
        <tr>
            <td bgcolor="FFFFCC"><b>Bank City/State </b></td>
            <td>#getinfo.bankCityState#</td>
        </tr>
        <tr>
            <td bgcolor="FFFFCC"><b>Check Number </b></td>
            <td>#getinfo.checkNum#</td>
        </tr>
        <tr>
            <td bgcolor="FFFFCC"><b>ABA/Transit/Routing ##</b></td>
            <td>#getinfo.ABATransitRoutingNum#</td>
        </tr>
        <tr>
            <td bgcolor="FFFFCC"><b>Account Number</b></td>
            <td>#getinfo.accountNumber#</td>
        </tr>
        <tr>
            <td bgcolor="FFFFCC"><b>Dealer Number </b></td>
            <td>#getinfo.dealernumber#</td>
        </tr>
        <tr>
            <td bgcolor="FFFFCC"><b>Invoice Number </b></td>
            <td><cfif trim(getinfo.invoiceNumber) IS "">&nbsp;<cfelse>#getinfo.invoiceNumber#</cfif></td>
        </tr>
        <tr>
            <td valign="top" bgcolor="FFFFCC"><b>Comments</b></td>
            <td valign="top"><cfif trim(getinfo.comments) IS NOT "">#replace(getinfo.comments, "#chr(13)#", "<br>", "all")#<cfelse>&nbsp;</cfif></td>
        </tr>
    </table>
	</cfoutput>
	<br>
	<cfoutput>
    <table width="500" border="0" cellpadding="5" cellspacing="0">
        <tr>
            <td width="50%" align="center"><a onClick="return confirm('Are you sure you wish to mark this transaction as processed?');" href="process.cfm?tid=#url.tid#&start=#url.start#">Mark Processed</a></td>
            <td width="50%" align="center"><a href="index.cfm?start=#url.start#">Return to Transactions </a></td>
        </tr>
    </table>
	</cfoutput>
</div>

