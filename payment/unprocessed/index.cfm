
<cfparam name="start" default="1">
<cfparam name="rows" default="25">
<cfif start LT 1><cfset start = 1></cfif>

<cfparam name="o" default="dtd">

<cfswitch expression="#o#">
	<cfcase value="tida"><cfset orderby = "order by paymentid ASC"></cfcase>
	<cfcase value="tidd"><cfset orderby = "order by paymentid DESC"></cfcase>
	<cfcase value="dta"><cfset orderby = "order by datetime ASC"></cfcase>
	<cfcase value="dtd"><cfset orderby = "order by datetime DESC"></cfcase>
	<cfcase value="ta"><cfset orderby = "order by type ASC"></cfcase>
	<cfcase value="td"><cfset orderby = "order by type DESC"></cfcase>
	<cfcase value="aa"><cfset orderby = "order by convert(money,amount) ASC"></cfcase>
	<cfcase value="ad"><cfset orderby = "order by convert(money,amount) DESC"></cfcase>
</cfswitch>

<!--- get unprocessed records --->
<cfquery name="getinfo" datasource="copalink">
	select paymentid, type, amount, datetime
	from Payments
	where processed = 0
	#orderby#
</cfquery>

<cfset pages = ceiling(getinfo.recordcount / rows)>

<div align="center"> 
<table border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td align="center"><b>Unprocessed Transactions</b></td>
	</tr>
</table>
<table border="1" cellpadding="5" cellspacing="0">
	<cfoutput>
    <tr bgcolor="FFFFCC">
        <td width="25%" align="center">
		<cfif o IS "tida">
		<b><a href="index.cfm?start=#start#&o=tidd">Transaction ID</a></b>
		<cfelse>
		<b><a href="index.cfm?start=#start#&o=tida">Transaction ID</a></b>
		</cfif>
		</td>
        <td width="25%" align="center">
		<cfif o IS "dta">
		<a href="index.cfm?start=#start#&o=dtd"><b>Payment Date</b></a>
		<cfelse>
		<a href="index.cfm?start=#start#&o=dta"><b>Payment Date</b></a>
		</cfif>
		</td>
        <td width="25%" align="center">
		<cfif o IS "ta">
		<a href="index.cfm?start=#start#&o=td"><b>Type</b></a>		
		<cfelse>
		<a href="index.cfm?start=#start#&o=ta"><b>Type</b></a>
		</cfif>
		</td>
        <td align="center">
		<cfif o IS "aa">
		<a href="index.cfm?start=#start#&o=ad"><b>Amount</b></a>
		<cfelse>
		<a href="index.cfm?start=#start#&o=aa"><b>Amount</b></a>
		</cfif>
		</td>
	</tr>
	</cfoutput>
	<cfif getinfo.recordcount GT 0>
	<cfoutput query="getinfo" startrow="#start#" maxrows="#rows#">
    <tr <cfif getinfo.currentrow MOD 2 IS 0>bgcolor="EEEEEE"</cfif> >
        <td align="center">
		<cfif getinfo.type IS "check">
			<a href="checkdetail.cfm?tid=#getinfo.paymentid#&start=#start#">#getinfo.paymentid#</a>		
		<cfelseif getinfo.type IS "credit card">
			<a href="ccdetail.cfm?tid=#getinfo.paymentid#&start=#start#">#getinfo.paymentid#</a>
		</cfif>
		</td>
        <td align="center">#dateformat(getinfo.datetime, 'mm/dd/yyyy')#</td>
        <td align="center">
		<cfif getinfo.type IS "check">
			Check
		<cfelseif getinfo.type IS "credit card">
			Credit Card
		</cfif>
		</td>
        <td align="right">$#getinfo.amount#</td>
	</tr>
	</cfoutput>
	<cfelse>
	<tr>
		<td colspan="4"><b>There are no unprocessed transactions on file</b></td>
	</tr>
	</cfif>
</table>
<br>
<cfif pages GT 1>
<table width="500" border="0" cellpadding="5" cellspacing="0">
<tr>
	<td align="center">
	<cfloop index="p" from="1" to="#pages#">
	<cfoutput>
	<a href="index.cfm?start=#evaluate((p * rows) - (rows-1))#&o=#o#">#p#</a>
	<cfif p IS NOT pages> | </cfif>
	</cfoutput>
	</cfloop>
	</td>
</tr>
</table>
</cfif>
<br>
<table width="500" border="0" cellpadding="5" cellspacing="0">
<tr>
	<td align="center"><a href="/index.cfm">Return to Administrative Menu</a></td>
</tr>
</table>
</div>
