<cfquery name="getinfo" datasource="copalink">
	select Payments.*, Payments_CC_Detail.* 
	from Payments, Payments_CC_Detail
	where Payments.paymentid = #url.tid# AND Payments.paymentid = Payments_CC_Detail.paymentid
</cfquery>

<div align="center">
    <table border="0" cellpadding="5" cellspacing="0">
        <tr>
            <td align="center"><b>Transaction Detail</b></td>
        </tr>
    </table>
	<cfoutput query="getinfo">
    <table border="1" cellpadding="5" cellspacing="0">
        <tr>
            <td width="35%" bgcolor="FFFFCC"><b>Transaction ID </b></td>
			<td>#getinfo.paymentid#</td>
        </tr>
        <tr>
            <td bgcolor="FFFFCC"><b>Payment Date/Time</b></td>
            <td>#dateformat(getinfo.datetime,'mm/dd/yyyy')# at #timeformat(getinfo.datetime, 'h:mm tt')#</td>
        </tr>
       <tr>
            <td bgcolor="FFFFCC"><b>Name or Company </b></td>
            <td>#getinfo.companyName#</td>
        </tr>
        <tr>
            <td bgcolor="FFFFCC"><b>Dealer Number</b></td>
            <td>#getinfo.dealernumber#</td>
        </tr>
        <tr>
            <td bgcolor="FFFFCC"><b>Credit Card Number </b></td>
            <td>#getinfo.cardNumber#</td>
        </tr>
        <tr>
            <td bgcolor="FFFFCC"><b>Credit Card Expiration</b></td>
            <td>#getinfo.expMM#/#getinfo.expYY#</td>
        </tr>
        <tr>
            <td bgcolor="FFFFCC"><b>CVV2</b></td>
            <td><cfif trim(getinfo.cvv2) IS NOT "">#getinfo.cvv2#<cfelse>&nbsp;</cfif></td>
        </tr>
        <tr>
            <td bgcolor="FFFFCC"><b>Card Holder</b></td>
            <td>#getinfo.cardHolder#</td>
        </tr>
         <tr>
            <td bgcolor="FFFFCC"><b>Address</b></td>
            <td>#getinfo.address#</td>
        </tr>
        <tr>
            <td bgcolor="FFFFCC"><b>Invoice Number </b></td>
            <td><cfif trim(getinfo.invoiceNumber) IS NOT "">#getinfo.invoiceNumber#<cfelse>&nbsp;</cfif></td>
        </tr>
        <tr>
            <td bgcolor="FFFFCC"><b>Amount</b></td>
            <td>$#getinfo.amount#</td>
        </tr>
        <tr>
            <td bgcolor="FFFFCC"><b>Authorized By </b></td>
            <td>#getinfo.authorizedBy#</td>
        </tr>
        <tr>
            <td bgcolor="FFFFCC"><b>Email Address</b></td>
            <td>#getinfo.email#</td>
        </tr>
        <tr>
            <td valign="top" bgcolor="FFFFCC"><b>Comments</b></td>
            <td valign="top"><cfif trim(getinfo.comments) IS NOT "">#replace(getinfo.comments, "#chr(13)#", "<br>", "all")#<cfelse>&nbsp;</cfif></td>
        </tr>
    </table>
	</cfoutput>
	<br>
	<cfoutput>
    <table width="500" border="0" cellpadding="5" cellspacing="0">
        <tr>
            <td width="50%" align="center"><a onClick="return confirm('Are you sure you wish to mark this transaction as processed?');" href="process.cfm?tid=#url.tid#&start=#url.start#">Mark Processed</a></td>
            <td width="50%" align="center"><a href="index.cfm?start=#url.start#">Return to Transactions </a></td>
        </tr>
    </table>
	</cfoutput>
</div>
