<link rel="stylesheet" type="text/css" href="../../styles.css">

<cfif isdefined("form.btnEditCheck")>

	<cfparam name="form.reverseARcheck1" default="0">
	<cfparam name="form.reverseARcheck2" default="0">
	
	<cfif (trim(form.dateredeposited) IS NOT "") AND (trim(form.dateredeposited) IS NOT "mm/dd/yyyy")>
		<cfset odbc_dateredeposited = createodbcdate(form.dateredeposited)>
	<cfelse>
		<cfset odbc_dateredeposited = "NULL">
	</cfif>
	<cfif (trim(form.dateredeposited2) IS NOT "") AND (trim(form.dateredeposited2) IS NOT "mm/dd/yyyy")>
		<cfset odbc_dateredeposited2 = createodbcdate(form.dateredeposited2)>
	<cfelse>
		<cfset odbc_dateredeposited2 = "NULL">
	</cfif>
	
	<cfquery name="updatecheck" datasource="copalink">
		update Payments_Bounces 
		set checknum = '#form.checknum#', dealernum = '#form.dealernum#', dealername = '#form.dealername#', checktype = '#form.checktype#', bouncereason = '#form.bouncereason#', dateredeposited = #odbc_dateredeposited#, dateredeposited2 = #odbc_dateredeposited2#, nsfinvoice1 = '#form.nsfinvoice1#', nsfinvoice2 = '#form.nsfinvoice2#', reverseARcheck1 = #form.reverseARcheck1#, reverseARcheck2 = #form.reverseARcheck2#, notes = '#form.notes#', amount = '#form.amount#'
		where bounceid = #id#
	</cfquery>

	<cflocation url="index.cfm">

<cfelse>

	<cfquery name="getinfo" datasource="copalink">
		select * from payments_bounces
		where bounceid = #id#
	</cfquery>

	<script language="javascript" src="cal2.js">
	/*
	Xin's Popup calendar script-  Xin Yang (http://www.yxscripts.com/)
	Script featured on/available at http://www.dynamicdrive.com/
	This notice must stay intact for use
	*/
	</script>
	<script language="javascript" src="cal_conf2.js"></script>
	
	<script language="JavaScript" type="text/JavaScript">
	function checkform(frm) {
		if (frm.checknum.value == "") {alert('The check number is required.'); frm.checknum.focus(); return false;}
		if (frm.amount.value == "") {alert('The check amount is required.'); frm.amount.focus(); return false;}
		if (frm.dealernum.value == "") {alert('The dealer number is required.'); frm.dealernum.focus(); return false;}
		if (frm.dealername.value == "") {alert('The dealer name is required.'); frm.dealername.focus(); return false;}
		if (!frm.checktype[0].checked && !frm.checktype[1].checked) {alert('The check type is required.'); frm.checktype[0].focus(); return false;}
		if (frm.bouncereason.value == "") {alert('The reason for bounced check is required.'); frm.bouncereason.focus(); return false;}

		frm.submit();
	}
	</script>
	
	<div align="center" class="normal">

	<table border="0" cellspacing="0" cellpadding="5">
		<form method="post" action="edit.cfm" name="addform">
		<cfoutput>
		<input type="hidden" name="id" value="#id#">
		</cfoutput>
		<input type="hidden" name="btnEditCheck" value="1">
		<tr>
			<td align="center" class="highlightbar"><b>Edit a bounced check</b></td>
		</tr>
		<tr>
			<td class="greyrowbottomnopadding">
				<cfoutput query="getinfo">
				<table border="0" cellspacing="0" cellpadding="5" class="grey">
					<tr>
						<td valign="middle" nowrap="nowrap">Check Number:</td>
						<td valign="middle">
							<input name="checknum" type="text" class="normal" maxlength="50" value="#getinfo.checknum#" />
						</td>
					</tr>
					<tr>
						<td valign="middle" nowrap="nowrap">Amount:</td>
						<td valign="middle">
							<input name="amount" type="text" class="normal" maxlength="50" value="#getinfo.amount#" />
						</td>
					</tr>
					<tr>
						<td valign="middle" nowrap="nowrap">Dealer Number:</td>
						<td valign="middle">
							<input name="dealernum" type="text" class="normal" maxlength="4" value="#getinfo.dealernum#" />
						</td>
					</tr>
					<tr>
						<td valign="middle" nowrap="nowrap">Dealer Name:</td>
						<td valign="middle">
							<input name="dealername" type="text" class="normal" style="width:300px" maxlength="255" value="#getinfo.dealername#" />
						</td>
					</tr>
					<tr>
						<td valign="middle" nowrap="nowrap">Check Type:</td>
						<td valign="middle">
							<input name="checktype" type="radio" value="Paper" <cfif getinfo.checktype IS "Paper">checked</cfif>>
							Paper Check&nbsp;&nbsp;&nbsp;&nbsp;
							<input name="checktype" type="radio" value="Phone" <cfif getinfo.checktype IS "Phone">checked</cfif>>
							Pay by Phone Check </td>
					</tr>
					<tr>
						<td valign="top" nowrap="nowrap">Reason for bouced check:</td>
						<td valign="top">
							<textarea name="bouncereason" class="normal" style="width:300px" rows="3">#getinfo.bouncereason#</textarea>
						</td>
					</tr>
					<tr>
						<td valign="middle" nowrap="nowrap">1st NSF Fee Invoice ##:</td>
						<td valign="middle">
							<input name="nsfinvoice1" type="text" class="normal" maxlength="50" value="#getinfo.nsfinvoice1#" />
						</td>
					</tr>
					<tr>
						<td valign="middle" nowrap="nowrap">1st reverse check in A/R:</td>
						<td valign="middle">
							<input type="checkbox" name="reverseARcheck1" value="1" <cfif getinfo.reverseARcheck1 IS NOT 0>checked</cfif>>
						</td>
					</tr>
					<tr>
						<td valign="middle" nowrap="nowrap">Date of 1st Redeposit:</td>
						<td valign="middle">
							<input name="dateredeposited" type="text" class="normal" maxlength="10" size="10" <cfif trim(getinfo.dateredeposited) IS NOT "">value="#dateformat(getinfo.dateredeposited, 'mm/dd/yyyy')#"<cfelse>value="mm/dd/yyyy"</cfif>>
						<a style="text-decoration:none;" href="javascript:showCal('addCalendar');"><img align="absmiddle" border="0" src="/images/calicon.gif" /></a> </td>
					</tr>
					<tr>
						<td valign="middle" nowrap="nowrap">2nd NSF Fee Invoice ##:</td>
						<td valign="middle">
							<input name="nsfinvoice2" type="text" class="normal" maxlength="50" value="#getinfo.nsfinvoice2#" />
						</td>
					</tr>
					<tr>
						<td valign="middle" nowrap="nowrap">2nd reverse check in A/R:</td>
						<td valign="middle">
							<input type="checkbox" name="reverseARcheck2" value="1" <cfif getinfo.reverseARcheck2 IS NOT 0>checked</cfif>>
						</td>
					</tr>
					<tr>
						<td valign="middle" nowrap="nowrap">Date of 2nd Redeposit:</td>
						<td valign="middle">
							<input name="dateredeposited2" type="text" class="normal" maxlength="10" size="10" <cfif trim(getinfo.dateredeposited2) IS NOT "">value="#dateformat(getinfo.dateredeposited2, 'mm/dd/yyyy')#"<cfelse>value="mm/dd/yyyy"</cfif>>
						<a style="text-decoration:none;" href="javascript:showCal('addCalendar2');"><img align="absmiddle" border="0" src="/images/calicon.gif" /></a> </td>
					</tr>
					<tr>
						<td valign="top" nowrap="nowrap">Notes:</td>
						<td valign="top">
							<textarea name="notes" class="normal" style="width:300px" rows="3">#getinfo.notes#</textarea>
						</td>
					</tr>
					<tr>
						<td colspan="2" align="center" valign="middle">
							<input type="button" class="sidebar" onclick="checkform(this.form);" value="Update Check" />
							&nbsp;
							<input name="Reset" type="reset" class="sidebar" value="Reset Fields" />
						</td>
					</tr>
				</table>
				</cfoutput>
			</td>
		</tr>
		</form>
	</table>

	<br />
	<a href="index.cfm">Return to Listings</a>
	</div>

</cfif>

