<link rel="stylesheet" type="text/css" href="../../styles.css">
<cfquery name="getinfo" datasource="copalink">
select * from payments_bounces
	where bounceid = #id#
</cfquery>
<div align="center" class="normal">
	<table width="500" border="0" cellpadding="5" cellspacing="0">
		<tr>
			<td align="center" class="highlightbar"><b>Check Details</b></td>
		</tr>
		<tr>
			<td class="greyrowbottomnopadding">
				<cfoutput query="getinfo">
					<table width="100%" border="0" cellpadding="5" cellspacing="0" class="grey">
						<tr>
							<td width="32%" valign="middle" nowrap="nowrap">Date Entered:</td>
							<td width="68%" valign="middle">#dateformat(getinfo.datetime,'mm/dd/yyyy')#</td>
						</tr>
						<tr>
							<td valign="middle" nowrap="nowrap">Amount:</td>
							<td valign="middle">#getinfo.amount#&nbsp;</td>
						</tr>
						<tr>
							<td valign="middle" nowrap="nowrap">Check Number:</td>
							<td valign="middle">#getinfo.checknum#</td>
						</tr>
						<tr>
							<td valign="middle" nowrap="nowrap">Dealer Number:</td>
							<td valign="middle">#getinfo.dealernum#</td>
						</tr>
						<tr>
							<td valign="middle" nowrap="nowrap">Dealer Name:</td>
							<td valign="middle">#getinfo.dealername#</td>
						</tr>
						<tr>
							<td valign="middle" nowrap="nowrap">Check Type:</td>
							<td valign="middle"> #getinfo.checktype# </td>
						</tr>
						<tr>
							<td valign="top" nowrap="nowrap">Reason for bouced check:</td>
							<td valign="top">#replace(getinfo.bouncereason,"#chr(13)#","<br>","all")#</td>
						</tr>
						<tr>
							<td valign="middle" nowrap="nowrap">1st NSF Fee Invoice ##:</td>
							<td valign="middle">#getinfo.nsfinvoice1#&nbsp;</td>
						</tr>
						<tr>
							<td valign="middle" nowrap="nowrap">1st reverse check in A/R:</td>
							<td valign="middle">
								<cfif getinfo.reverseARcheck1 IS NOT 0>
									Yes
									<cfelse>
									No
								</cfif>
							</td>
						</tr>
						<tr>
							<td valign="middle" nowrap="nowrap">Date of 1st Redeposit:</td>
							<td valign="middle">
								<cfif trim(getinfo.dateredeposited) IS NOT "">
									#dateformat(getinfo.dateredeposited, 'mm/dd/yyyy')#
									<cfelse>
									&nbsp;
								</cfif>
							</td>
						</tr>
						<tr>
							<td valign="middle" nowrap="nowrap">2nd NSF Fee Invoice ##:</td>
							<td valign="middle">#getinfo.nsfinvoice2#&nbsp;</td>
						</tr>
						<tr>
							<td valign="middle" nowrap="nowrap">2nd reverse check in A/R:</td>
							<td valign="middle">
								<cfif getinfo.reverseARcheck2 IS NOT 0>
									Yes
									<cfelse>
									No
								</cfif>
							</td>
						</tr>
						<tr>
							<td valign="middle" nowrap="nowrap">Date of 2nd Redeposit:</td>
							<td valign="middle">
								<cfif trim(getinfo.dateredeposited2) IS NOT "">
									#dateformat(getinfo.dateredeposited2, 'mm/dd/yyyy')#
									<cfelse>
									&nbsp;
								</cfif>
							</td>
						</tr>
						<tr>
							<td valign="top" nowrap="nowrap">Notes:</td>
							<td valign="top">#replace(getinfo.notes, "#chr(13)#", "<br>", "all")#&nbsp;</td>
						</tr>
					</table>
				</cfoutput>
			</td>
		</tr>
	</table>
	<br>
	<a href="index.cfm">Return to Listings</a> </div>
