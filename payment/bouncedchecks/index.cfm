
<link rel="stylesheet" type="text/css" href="../../styles.css">

<cfparam name="start" default="1">
<cfset maxrows = "25">
<cfset search = "">
<cfparam name="sort" default="date_desc">
<!--- 
<cfparam name="begin" default="#dateformat(now(),'mm/dd/yyyy')#">
<cfparam name="end" default="#dateformat(now(),'mm/dd/yyyy')#">
<cfparam name="begintime" default="00:00AM">
<cfparam name="endtime" default="11:59PM">
--->
<cfparam name="begin" default="">
<cfparam name="end" default="">
<cfparam name="begintime" default="00:00AM">
<cfparam name="endtime" default="11:59PM">
<cfparam name="s" default="1">

<script language="JavaScript" type="text/JavaScript">
function doSearch(frm) {
	var searchstring = "";
	
	searchstring += "&checknum=" + frm.checknum.value;
	searchstring += "&dealernum=" + frm.dealernum.value;
	searchstring += "&dealername=" + frm.dealername.value;
	searchstring += "&dateredep=" + frm.dateredep.value;
	if (frm.checktype[0].checked)
		searchstring += "&checktype=" + frm.checktype[0].value;
	else if (frm.checktype[1].checked)
		searchstring += "&checktype=" + frm.checktype[1].value;
	searchstring += "&begin=" + frm.begindate.value;
	if (frm.begintime.value != "")
		searchstring += "&begintime=" + frm.begintime.value + frm.begintt.options[frm.begintt.selectedIndex].value;

	searchstring += "&end=" + frm.enddate.value;
	if (frm.endtime.value != "") 
		searchstring += "&endtime=" + frm.endtime.value + frm.endtt.options[frm.endtt.selectedIndex].value;

	document.location = "index.cfm?sort=" + frm.sort.value + "&start=" + frm.start.value + searchstring;
}
</script>
<script language="javascript" src="cal2.js">
/*
Xin's Popup calendar script-  Xin Yang (http://www.yxscripts.com/)
Script featured on/available at http://www.dynamicdrive.com/
This notice must stay intact for use
*/
</script>
<script language="javascript" src="cal_conf2.js"></script>

<cfswitch expression="#sort#">
	<cfcase value="date_asc"><cfset orderby = "datetime asc"></cfcase>
	<cfcase value="date_desc"><cfset orderby = "datetime desc"></cfcase>
	<cfcase value="check_asc"><cfset orderby = "checknum asc"></cfcase>
	<cfcase value="check_desc"><cfset orderby = "checknum desc"></cfcase>
	<cfcase value="amount_asc"><cfset orderby = "amount asc"></cfcase>
	<cfcase value="amount_desc"><cfset orderby = "amount desc"></cfcase>
	<cfcase value="dealernum_asc"><cfset orderby = "dealernum asc"></cfcase>
	<cfcase value="dealernum_desc"><cfset orderby = "dealernum desc"></cfcase>
	<cfcase value="dealername_asc"><cfset orderby = "dealername asc"></cfcase>
	<cfcase value="dealername_desc"><cfset orderby = "dealername desc"></cfcase>
	<cfcase value="checktype_asc"><cfset orderby = "checktype asc"></cfcase>
	<cfcase value="checktype_desc"><cfset orderby = "checktype desc"></cfcase>
	<cfcase value="dateredep_asc"><cfset orderby = "dateredeposited asc"></cfcase>
	<cfcase value="dateredep_desc"><cfset orderby = "dateredeposited desc"></cfcase>
	<cfcase value="dateredep2_asc"><cfset orderby = "dateredeposited2 asc"></cfcase>
	<cfcase value="dateredep2_desc"><cfset orderby = "dateredeposited2 desc"></cfcase>
</cfswitch>

<cfset searchstring = "">
<cfset urlstring = "">
<cfif isdefined("url.checknum") and trim(url.checknum) IS NOT ""><cfset searchstring = searchstring & "and checknum LIKE '%#url.checknum#%' "><cfset urlstring = urlstring & "&checknum=#checknum#"></cfif>
<cfif isdefined("url.dealernum") and trim(url.dealernum) IS NOT ""><cfset searchstring = searchstring & "and dealernum LIKE '%#url.dealernum#%' "><cfset urlstring = urlstring & "&dealernum=#dealernum#"></cfif>
<cfif isdefined("url.dealername") and trim(url.dealername) IS NOT ""><cfset searchstring = searchstring & "and dealername LIKE '%#url.dealername#%' "><cfset urlstring = urlstring & "&dealername=#dealername#"></cfif>
<cfif isdefined("url.checktype") and trim(url.checktype) IS NOT ""><cfset searchstring = searchstring & "and checktype = '#url.checktype#' "><cfset urlstring = urlstring & "&checktype=#checktype#"></cfif>
<cfif isdefined("url.dateredep") and trim(url.dateredep) IS NOT ""><cfset searchstring = searchstring & "and (dateredeposited = #createodbcdate(url.dateredep)# OR dateredeposited2 = #createodbcdate(url.dateredep)#) "><cfset urlstring = urlstring & "&dateredep=#dateredep#"></cfif>
<cfif isdefined("url.begin") and trim(url.begin) IS NOT "">
	<cfif isdefined("url.begintime") and trim(url.begintime) IS NOT "">
		<cfset searchstring = searchstring & "and datetime >= #createodbcdatetime("#begin# #begintime#")# ">
		<cfset urlstring = urlstring & "&begin=#begin#&begintime=#begintime#">
	<cfelse>
		<cfset searchstring = searchstring & "and datetime >= '#url.begin#' ">
		<cfset urlstring = urlstring & "&begin=#begin#">
	</cfif>
</cfif>
<cfif isdefined("url.end") and trim(url.end) IS NOT "">
	<cfif isdefined("url.endtime") and trim(url.endtime) IS NOT "">
		<cfset searchstring = searchstring & "and datetime <= #createodbcdatetime("#end# #endtime#")# ">
		<cfset urlstring = urlstring & "&end=#end#&endtime=#endtime#">
	<cfelse>
		<cfset searchstring = searchstring & "and datetime <= '#url.end#' ">
		<cfset urlstring = urlstring & "&end=#end#">
	</cfif>
</cfif>

<cfquery name="getchecks" datasource="copalink">
	select * from payments_bounces
	where 1 = 1 #preservesinglequotes(searchstring)#
	order by #orderby#
</cfquery>

<div align="center" class="normal">
<form method="post" action="index.cfm" name="searchform">
<cfoutput>
<input type="hidden" name="start" value="#start#">
<input type="hidden" name="sort" value="#sort#">
</cfoutput>
<table border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Search Criteria</b></td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">&nbsp;
		<table border="0" cellspacing="0" cellpadding="5" class="grey">
			<cfoutput>
			<tr>
				<td nowrap="nowrap">Check ##:</td>
				<td>
				<input name="checknum" type="text" class="normal" maxlength="50" <cfif isdefined("url.checknum")>value="#url.checknum#"</cfif> >
				</td>
			</tr>
			<tr>
				<td nowrap="nowrap">Dealer ##: </td>
				<td><input name="dealernum" type="text" class="normal" maxlength="50" <cfif isdefined("url.dealernum")>value="#url.dealernum#"</cfif> ></td>
			</tr>
			<tr>
				<td nowrap="nowrap">Dealer Name: </td>
				<td><input name="dealername" type="text" class="normal" style="width:300px" maxlength="255" <cfif isdefined("url.dealername")>value="#url.dealername#"</cfif> ></td>
			</tr>
			<tr>
				<td nowrap="nowrap">Check Type: </td>
				<td>
				<input name="checktype" type="radio" value="Paper" <cfif isdefined("url.checktype")><cfif url.checktype IS "Paper">checked</cfif></cfif> >Paper Check&nbsp;&nbsp;&nbsp;&nbsp;
				<input name="checktype" type="radio" value="Phone" <cfif isdefined("url.checktype")><cfif url.checktype IS "Phone">checked</cfif></cfif> >Pay by Phone Check		</td>
			</tr>
			<tr>
				<td nowrap="nowrap">Any Date Redeposited:</td>
				<td>
				<input name="dateredep" type="text" class="normal" size="10" maxlength="10" <cfif isdefined("url.dateredep")>value="#url.dateredep#"</cfif> > 
				<a style="text-decoration:none;" href="javascript:showCal('redepositCalendar');"><img align="absmiddle" border="0" src="/images/calicon.gif"></a>		</td>
			</tr>
			</cfoutput>
			<tr>
				<td valign="top" nowrap="nowrap">Date/Time Range:</td>
				<td class="nopadding">
					<table  border="0" cellpadding="2" cellspacing="0" class="grey">
					<tr>
						<td>
						Start Date:				</td>
						<td>
						<cfoutput>
						 <input onFocus="if (this.value.length > 0) this.select();" type="text" name="begindate" size="12" maxlength="12" class="small" <cfif trim(begin) IS NOT "00:00AM">value="#dateformat(begin,'mm/dd/yyyy')#"<cfelse><!--- value="mm/dd/yyyy" ---></cfif> > <a style="text-decoration:none;" href="javascript:showCal('beginCalendar');"><img align="absmiddle" border="0" src="/images/calicon.gif"></a><br>
						</cfoutput>				</td>
					</tr>
					<tr>
						<td>
						Time (hh:mm) :				</td>
						<td>
						<cfoutput>
						<input onFocus="if (this.value.length > 0) this.select();" type="text" name="begintime" size="5" maxlength="5" <cfif trim(begintime) IS NOT "">value="#timeformat(begintime,'hh:mm')#"<cfelse><!--- value="00:00" ---></cfif> class="small">
						<select name="begintt" class="small">
							<option value="AM" <cfif trim(begintime) IS NOT ""><cfif ucase(timeformat(begintime,'tt')) IS "AM">selected</cfif></cfif> >AM</option>
							<option value="PM" <cfif trim(begintime) IS NOT ""><cfif ucase(timeformat(begintime,'tt')) IS "PM">selected</cfif></cfif> >PM</option>
						</select>
						</cfoutput>				</td>
					</tr>
					<tr>
						<td>
						End Date:				</td>
						<td>
						<cfoutput>
						<input onFocus="if (this.value.length > 0) this.select();" type="text" name="enddate" size="12" maxlength="12" class="small" <cfif trim(end) IS NOT "11:59PM">value="#dateformat(end,'mm/dd/yyyy')#"<cfelse><!--- value="mm/dd/yyyy" ---></cfif> > <a style="text-decoration:none;" href="javascript:showCal('endCalendar');"><img align="absmiddle" border="0" src="/images/calicon.gif"></a><br>
						</cfoutput>				</td>
					</tr>
					<tr>
						<td>
						Time (hh:mm) :				</td>
						<td>
						<cfoutput>
						<input onFocus="if (this.value.length > 0) this.select();" type="text" name="endtime" size="5" maxlength="5" <cfif trim(endtime) IS NOT "">value="#timeformat(endtime,'hh:mm')#"<cfelse><!--- value="00:00" ---></cfif> class="small">
						<select name="endtt" class="small">
							<option value="AM" <cfif trim(endtime) IS NOT ""><cfif ucase(timeformat(endtime,'tt')) IS "AM">selected</cfif></cfif> >AM</option>
							<option value="PM" <cfif trim(endtime) IS NOT ""><cfif ucase(timeformat(endtime,'tt')) IS "PM">selected</cfif></cfif> >PM</option>
						</select>
						</cfoutput>				</td>
					</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td align="center" colspan="2">
				<input type="button" class="sidebar" onClick="doSearch(this.form);" value="Search Now">
				&nbsp;
				<input type="reset" class="sidebar" value="Reset Fields">
				</td>
			</tr>
		</table>
		</td>
	</tr>
</table>
</form>

<!--- pagination code --->
<cfset pages = ceiling(getchecks.recordcount / maxrows)>
<table width="725" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center">
	<table border="0" cellspacing="0" cellpadding="5">
	  <tr>
		<td valign="top"><cfif pages GT 1>Page</cfif></td>
		<td valign="top">
		<cfif pages GT 1>
			<cfloop from="1" to="#pages#" index="page">
			<cfset s = (maxrows * page) - (maxrows - 1)>
			<cfset currpage = evaluate((start/maxrows) + 1)>
			<cfoutput>
				<a href="index.cfm?start=#s#&sort=#sort##urlstring#">#page#</a><cfif page is not pages> | </cfif>
			</cfoutput>
			</cfloop>
		</cfif>
		&nbsp;<a href="index.cfm">View All</a>
		</td>
	  </tr>
	</table>
	</td>
</tr>
</table>
<!--- pagination code --->
<br>
<table border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center" nowrap><a href="add.cfm">Add New</a></td>
		<td colspan="8">&nbsp;</td>
	</tr>
	<cfoutput>
	<tr>
		<td align="center" nowrap class="horizontalnavbuttons">Action</td>
		<td width="64" align="center" nowrap class="horizontalnavbuttons">
			
			<cfif sort IS "check_asc">
				<cfset sortlink = "check_desc">
				<cfelse>
				<cfset sortlink = "check_asc">
			</cfif>
			<a href="index.cfm?start=#start#&sort=#sortlink##urlstring#">Check ##</a> </td>
	    <td width="75" align="center" nowrap class="horizontalnavbuttons">
			
			<cfif sort IS "amount_asc">
				<cfset sortlink = "amount_desc">
				<cfelse>
				<cfset sortlink = "amount_asc">
			</cfif>
			<a href="index.cfm?start=#start#&sort=#sortlink##urlstring#">Amount</a> </td>
	    <td width="75" align="center" nowrap class="horizontalnavbuttons">
			
			<cfif sort IS "date_asc">
				<cfset sortlink = "date_desc">
				<cfelse>
				<cfset sortlink = "date_asc">
			</cfif>
			<a href="index.cfm?start=#start#&sort=#sortlink##urlstring#">Date/Time</a> </td>
	    <td width="61" align="center" nowrap class="horizontalnavbuttons">
			
			<cfif sort IS "dealernum_asc">
				<cfset sortlink = "dealernum_desc">
				<cfelse>
				<cfset sortlink = "dealernum_asc">
			</cfif>
			<a href="index.cfm?start=#start#&sort=#sortlink##urlstring#">Dealer ##</a> </td>
	    <td width="323" align="center" nowrap class="horizontalnavbuttons">
			
			<cfif sort IS "dealername_asc">
				<cfset sortlink = "dealername_desc">
				<cfelse>
				<cfset sortlink = "dealername_asc">
			</cfif>
			<a href="index.cfm?start=#start#&sort=#sortlink##urlstring#">Dealer Name</a> </td>
	    <td width="49" align="center" nowrap class="horizontalnavbuttons">
			
			<cfif sort IS "checktype_asc">
				<cfset sortlink = "checktype_desc">
				<cfelse>
				<cfset sortlink = "checktype_asc">
			</cfif>
			<a href="index.cfm?start=#start#&sort=#sortlink##urlstring#">Type</a> </td>
	    <td width="80" align="center" nowrap class="horizontalnavbuttons">
			
			<cfif sort IS "dateredep_asc">
				<cfset sortlink = "dateredep_desc">
				<cfelse>
				<cfset sortlink = "dateredep_asc">
			</cfif>
			<a href="index.cfm?start=#start#&sort=#sortlink##urlstring#">Redeposit ##1</a> </td>
	    <td width="80" align="center" nowrap class="horizontalnavbuttons">
			
			<cfif sort IS "dateredep2_asc">
				<cfset sortlink = "dateredep2_desc">
				<cfelse>
				<cfset sortlink = "dateredep2_asc">
			</cfif>
			<a href="index.cfm?start=#start#&sort=#sortlink##urlstring#">Redeposit ##2</a> </td>
	</tr>
	</cfoutput>
	<cfoutput query="getchecks" startrow="#start#" maxrows="#maxrows#">
	<tr>
		<td align="center" class="linedrow">
		<a href="edit.cfm?id=#getchecks.bounceid#"><img src="/images/edit.gif" border="0" alt="Edit"></a>
		<!--- <a href="delete.cfm"><img src="/images/delete.gif" border="0" alt="Delete"></a> --->
		</td>
		<td align="center" class="linedrow"><a href="details.cfm?id=#getchecks.bounceid#">#getchecks.checknum#</a></td>
		<td class="linedrow">#getchecks.amount#&nbsp;</td>
		<td align="center" class="linedrow">#dateformat(getchecks.datetime,'mm/dd/yyyy')#</td>
		<td align="center" class="linedrow">#getchecks.dealernum#</td>
		<td class="linedrow">#getchecks.dealername#</td>
		<td align="center" class="linedrow">#getchecks.checktype#</td>
		<td align="center" class="linedrow"><cfif trim(getchecks.dateredeposited) IS NOT "">#dateformat(getchecks.dateredeposited,'mm/dd/yyyy')#<cfelse>&nbsp;</cfif></td>
	    <td align="center" class="linedrow"><cfif trim(getchecks.dateredeposited2) IS NOT "">#dateformat(getchecks.dateredeposited2,'mm/dd/yyyy')#<cfelse>&nbsp;</cfif></td>
	</tr>
	</cfoutput>
	<tr>
		<td align="center" nowrap><a href="add.cfm">Add New</a></td>
		<td colspan="8">&nbsp;</td>
	</tr>
</table>
<br>
<!--- pagination code --->
<cfset pages = ceiling(getchecks.recordcount / maxrows)>
<cfset pages = ceiling(getchecks.recordcount / maxrows)>
<table width="725" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center">
	<table border="0" cellspacing="0" cellpadding="5">
	  <tr>
		<td valign="top"><cfif pages GT 1>Page</cfif></td>
		<td valign="top">
		<cfif pages GT 1>
			<cfloop from="1" to="#pages#" index="page">
			<cfset s = (maxrows * page) - (maxrows - 1)>
			<cfset currpage = evaluate((start/maxrows) + 1)>
			<cfoutput>
				<a href="index.cfm?start=#s#&sort=#sort##urlstring#">#page#</a><cfif page is not pages> | </cfif>
			</cfoutput>
			</cfloop>
		</cfif>
		&nbsp;<a href="index.cfm">View All</a>
		</td>
	  </tr>
	</table>
	</td>
</tr>
</table>
<!--- pagination code --->
<br>
<br>
<a href="/index.cfm">Return to Intranet Menu</a>
</div>


<!--- =============================================================================== --->




