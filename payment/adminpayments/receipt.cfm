<style type="text/css">
	TABLE {font-family:Verdana, Arial, Helvetica, sans-serif;font-size:10px}
	.normal {font-family:Verdana, Arial, Helvetica, sans-serif;font-size:10px}
	.heading {font-family:Arial, Helvetica, sans-serif;font-size:12px}
	.formtext {font-family:Verdana, Arial, Helvetica, sans-serif;font-size:10px}
	.hdrtext {font-family:Arial, Helvetica, sans-serif;font-size:12px;font-weight:bold}
	.subhdrtext {font-family:Verdana, Arial, Helvetica, sans-serif;font-size:10px;font-weight:bold}
</style>

<cfquery name="getpayment" datasource="copalink">
	select * from Payments
	where paymentid = #cookie.paymentid#
</cfquery>
<cfif getpayment.type IS "check">
	<cfquery name="getdetail" datasource="copalink">
		select * from Payments_Check_Detail
		where paymentid = #cookie.paymentid#
	</cfquery>
<cfelseif getpayment.type IS "credit card">
	<cfquery name="getdetail" datasource="copalink">
		select * from Payments_CC_Detail
		where paymentid = #cookie.paymentid#
	</cfquery>
</cfif>
<div align="center">
	<table cellpadding="5" cellspacing="0" border="0" class="formtext">
	<tr>
		<td align="Center" class="hdrtext">C.O.P.S. Monitoring - Online Payment</td>
	</tr>
	<tr>
		<td align="center"><br><span class="subhdrtext">Please print this page for your records</span><br><br></td>
	</tr>
	<tr>
		<td align="center">Payments received after 3:00 PM EST will be posted on the next business day.</td>
	</tr>
	<tr>
		<td align="center">
		<cfoutput>
		<table border="0" cellpadding="5" cellspacing="0" class="formtext">
		<tr>
			<td><b>Transaction ID</b></td>
			<td>#cookie.paymentid#</td>
		</tr>
		<tr>
			<td><b>Amount</b></td>
			<td>$#getpayment.amount#</td>
		</tr>
		<tr>
			<td><b>Date</b></td>
			<td>#dateformat(getpayment.datetime,'mm/dd/yyyy')# #timeformat(getpayment.datetime,'hh:mm tt')# EST</td>
		</tr>
		<cfif getpayment.type IS "check">
		<tr>
			<td><b>Check Number</b></td>
			<td>#getdetail.checkNum#</td>
		</tr>
		</cfif>
		<cfif getpayment.type IS "credit card">
		<tr>
			<td><b>Credit Card Number</b></td>
			<td>XXXXXXXXX#right(getdetail.cardNumber,4)#</td>
		</tr>
		<tr>
			<td><b>Expiration Date:</b></td>
			<td>#getdetail.expMM#/#getdetail.expYY#</td>
		</tr>
		</cfif>
		<tr>
			<td colspan="2">
			<b>Comments:</b><br>
			#replace(getpayment.comments, "#chr(13)#", "<br>", "all")#
			</td>
		</tr>
		<tr><td><input type="button" value="Print Page" onClick="print();"></td></tr>
		</table>
		</cfoutput>
		</td>
	</tr>
	</table>
<br>
<table width="500" border="0" cellpadding="5" cellspacing="0">
<tr>
	<td align="center"><a href="/index.cfm">Return to Administrative Menu</a></td>
</tr>
</table>
</div>
