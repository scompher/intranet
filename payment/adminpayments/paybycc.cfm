<style type="text/css">
	TABLE {font-family:Verdana, Arial, Helvetica, sans-serif;font-size:10px}
	.normal {font-family:Verdana, Arial, Helvetica, sans-serif;font-size:10px}
	.heading {font-family:Arial, Helvetica, sans-serif;font-size:12px}
	.formtext {font-family:Verdana, Arial, Helvetica, sans-serif;font-size:10px}
	.hdrtext {font-family:Arial, Helvetica, sans-serif;font-size:12px;font-weight:bold}
	.subhdrtext {font-family:Verdana, Arial, Helvetica, sans-serif;font-size:10px;font-weight:bold}
</style>

<script language="JavaScript" type="text/JavaScript">
function checkform(frm) {
	if (frm.dealernumber.value == '') {
		alert('Please provide the dealer number.');
		frm.dealernumber.focus();
		return false;
	}
	if (frm.email.value == '') {
		alert('Please provide your email address.');
		frm.email.focus();
		return false;
	}
	if (frm.companyName.value == '') {
		alert('Please provide your company name.');
		frm.companyName.focus();
		return false;
	}
	if (frm.cardNumber.value == '') {
		alert('Please provide the credit card number.');
		frm.cardNumber.focus();
		return false;
	}
	if (frm.cardNumber.value.length < 12 || isNaN(frm.cardNumber.value)) {
		alert('The credit card number must be at least 12 digits.');
		frm.cardNumber.focus();
		return false;
	}
	if (frm.expMM.selectedIndex == 0) {
		alert('Please provide the expiration date month.');
		frm.expMM.focus();
		return false;
	}
	if (frm.expYY.selectedIndex == 0) {
		alert('Please provide the expiration date year.');
		frm.expYY.focus();
		return false;
	}
	if (frm.cardHolder.value == '') {
		alert('Please provide the card holders name.');
		frm.cardHolder.focus();
		return false;
	}
	if (frm.address.value == '') {
		alert('Please provide the address.');
		frm.address.focus();
		return false;
	}
	/*
	if (frm.invoiceNumber.value == '') {
		alert('Please provide the invoice number.');
		frm.invoiceNumber.focus();
		return false;
	}
	*/
	if (frm.amountDollars.value == '') {
		alert('Please provide the amount dollars.');
		frm.amountDollars.focus();
		return false;
	}
	if (frm.amountDollars.value.indexOf(".") != -1) {
		alert('Please ensure the amount dollars does not contain a decimal.');
		frm.amountDollars.focus();
		return false;
	}
	if (frm.amountCents.value == '') {
		alert('Please provide the amount cents.');
		frm.amountCents.focus();
		return false;
	}
	if (frm.amountCents.value.indexOf(".") != -1) {
		alert('Please ensure the amount cents does not contain a decimal.');
		frm.amountCents.focus();
		return false;
	}
	if (frm.authorizedBy.value == '') {
		alert('Please provide the name of the person authorizing this charge.');
		frm.authorizedBy.focus();
		return false;
	}
	return true;
}
</script>

<div align="center">
	<table cellpadding="5" cellspacing="0" border="0" class="formtext">
	<tr>
		<td colspan="2" align="Center" class="hdrtext">C.O.P.S. Monitoring - Online Payment</td>
	</tr>
	<tr>
		<td colspan="2" align="center"><br><span class="subhdrtext">Pay by Credit Card</span><br><br></td>
	</tr>
	<tr>
		<td colspan="2">
		<br>
		An administrative fee of 3% may apply when using a credit card as a method of payment.
		<br><br>
		Payments received after 3:00 PM EST will be posted on the next business day.
		</td>
	</tr>
	</table>
	<form method="post" action="confirm.cfm">
	<input type="hidden" name="type" value="credit card">
	<table cellpadding="5" cellspacing="0" border="0" class="formtext">
	<tr>
		<td colspan="2">
		<b>Dealer Number:</b><br>
		<input type="text" name="dealernumber" size="50" maxlength="255" class="formtext" style="color:000000">
		</td>
	</tr>
	<tr>
		<td colspan="2">
		<b>Dealers Email Address:</b><br>
		<input type="text" name="email" size="50" maxlength="255" class="formtext" style="color:000000">
		</td>
	</tr>	
	<tr>
		<td colspan="2">
		<b>Company Name:</b><br>
		<input type="text" name="companyName" size="50" maxlength="255" class="formtext" style="color:000000">
		</td>
	</tr>
	<tr>
		<td colspan="2">
		<b>Card Number:</b> (no spaces or dashes)<br>
		<input type="text" name="cardNumber" size="50" maxlength="50" class="formtext" style="color:000000">
		</td>
	</tr>
	<tr>
		<td colspan="2">
		<b>Expiration:</b><br>
		<select name="expMM">
			<option value=""></option>
		<cfloop from="1" to="12" index="x">
		<cfoutput>
			<option value="#x#">#numberformat(x,'00')#</option>
		</cfoutput>
		</cfloop>
		</select>
		<select name="expYY">
			<option value=""></option>
		<cfloop from="#dateformat(now(),'yyyy')#" to="#evaluate(dateformat(now(),'yyyy') + 6)#" index="x">
		<cfoutput>
			<option value="#x#">#x#</option>
		</cfoutput>
		</cfloop>
		</select>
		</td>
	</tr>
	<tr>
		<td colspan="2">
		<b>CVV2:</b> (3 or 4 digits, located on the signature line)<br>
		<input type="text" name="cvv2" size="4" maxlength="4" class="formtext" style="color:000000">
		</td>
	</tr>
	<tr>
		<td colspan="2">
		<b>Card Holder:</b><br>
		<input type="text" name="cardHolder" size="50" maxlength="255" class="formtext" style="color:000000">
		</td>
	</tr>
	<tr>
		<td colspan="2">
		<b>Address:</b><br>
		<input type="text" name="address" size="50" maxlength="255" class="formtext" style="color:000000">
		</td>
	</tr>
	<tr>
		<td colspan="2">
		<b>Invoice Number:</b><br>
		<input type="text" name="invoiceNumber" size="50" maxlength="50" class="formtext" style="color:000000">
		</td>
	</tr>
	<tr>
		<td colspan="2">
		<b>Amount:</b><br>
		$<input type="text" name="amountDollars" size="5" maxlength="5" class="formtext" style="color:000000">.
		<input type="text" name="amountCents" size="2" maxlength="2" class="formtext" style="color:000000">
		</td>
	</tr>
	<tr>
		<td colspan="2">
		<b>Authorized By:</b><br>
		<input type="text" name="authorizedBy" size="50" maxlength="255" class="formtext" style="color:000000">
		</td>
	</tr>
	<tr>
		<td colspan="2">
		<b>Comments:</b><br>
		<textarea name="comments" rows="7" cols="50" class="formtext" style="color:000000"></textarea>
		</td>
	</tr>
	<tr><td align="right"><input onClick="return checkform(this.form);" type="submit" name="btnSubmitPayment" value="Submit Payment"></td><td width="25%">&nbsp;</td></tr>
	</table>
	</form>
<br>
<table width="500" border="0" cellpadding="5" cellspacing="0">
<tr>
	<td align="center"><a href="/index.cfm">Return to Administrative Menu</a></td>
</tr>
</table>
</div>
