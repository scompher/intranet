<style type="text/css">
	TABLE {font-family:Verdana, Arial, Helvetica, sans-serif;font-size:10px}
	.normal {font-family:Verdana, Arial, Helvetica, sans-serif;font-size:10px}
	.heading {font-family:Arial, Helvetica, sans-serif;font-size:12px}
	.formtext {font-family:Verdana, Arial, Helvetica, sans-serif;font-size:10px}
	.hdrtext {font-family:Arial, Helvetica, sans-serif;font-size:12px;font-weight:bold}
	.subhdrtext {font-family:Verdana, Arial, Helvetica, sans-serif;font-size:10px;font-weight:bold}
</style>

<script language="JavaScript" type="text/JavaScript">
function checkform(frm) {
	if (frm.dealernumber.value == '') {
		alert('Please provide the dealer number.');
		frm.dealernumber.focus();
		return false;
	}
	if (frm.email.value == '') {
		alert('Please provide your email address.');
		frm.email.focus();
		return false;
	}
	if (frm.name.value == '') {
		alert('Please provide the name or company on the check.');
		frm.name.focus();
		return false;
	}
	if (frm.address.value == '') {
		alert('Please provide the address on the check.');
		frm.address.focus();
		return false;
	}
	if (frm.city.value == '') {
		alert('Please provide the city on the check.');
		frm.city.focus();
		return false;
	}
	if (frm.state.value == '') {
		alert('Please provide the state on the check.');
		frm.state.focus();
		return false;
	}
	if (frm.zip.value == '') {
		alert('Please provide the zipcode on the check.');
		frm.zip.focus();
		return false;
	}
	if (frm.city.value == '') {
		alert('Please provide the city on the check.');
		frm.city.focus();
		return false;
	}
	if (frm.phoneFax.value == '') {
		alert('Please provide your phone or fax number.');
		frm.phoneFax.focus();
		return false;
	}
	if (frm.bankName.value == '') {
		alert('Please provide the bank name on the check.');
		frm.bankName.focus();
		return false;
	}
	if (frm.bankCityState.value == '') {
		alert('Please provide the bank city and/or state on the check.');
		frm.bankCityState.focus();
		return false;
	}
	if (frm.checkNum.value == '') {
		alert('Please provide the check number on the check.');
		frm.checkNum.focus();
		return false;
	}
	if (isNaN(frm.checkNum.value)) {
		alert('Please ensure the check number is correct.');
		frm.checkNum.focus();
		return false;
	}
	if (frm.checkNum.value.indexOf(".") != -1) {
		alert('Please ensure the check number is correct.');
		frm.checkNum.focus();
		return false;
	}
	if (frm.checkNum.value < 0) {
		alert('Please ensure the check number is correct.');
		frm.checkNum.focus();
		return false;
	}
	if (frm.checkAmtDollars.value == '') {
		alert('Please provide the check amount (in dollars).');
		frm.checkAmtDollars.focus();
		return false;
	}
	if (frm.checkAmtDollars.value.indexOf(".") != -1) {
		alert('Please verify the check amount dollars does not contain a decimal.');
		frm.checkAmtDollars.focus();
		return false;
	}
	if (frm.checkAmtCents.value == '') {
		alert('Please provide the check amount cents.');
		frm.checkAmtDollars.focus();
		return false;
	}
	if (frm.checkAmtCents.value.indexOf(".") != -1) {
		alert('Please verify the check amount cents does not contain a decimal.');
		frm.checkAmtCents.focus();
		return false;
	}
	if (!frm.OnFile.checked) {
		if (frm.ABATransitRoutingNum.value == '') {
			alert('Please provide the check ABA/Transit/Routing Numbers.');
			frm.ABATransitRoutingNum.focus();
			return false;
		}
		if (isNaN(frm.ABATransitRoutingNum.value)) {
			alert('Please ensure check ABA/Transit/Routing Numbers are correct.');
			frm.ABATransitRoutingNum.focus();
			return false;
		}
	}
	if (frm.accountNumber.value == '') {
		alert('Please provide the checking account number.');
		frm.accountNumber.focus();
		return false;
	}
	/*
	if (frm.invoiceNumber.value == '') {
		alert('Please provide the invoice number you are paying.');
		frm.invoiceNumber.focus();
		return false;
	}
	*/
	return true;
}
</script>

<div align="center">
	<form method="post" action="confirm.cfm">
	<input type="hidden" name="type" value="check">
	<table cellpadding="5" cellspacing="0" border="0">
	<tr>
		<td colspan="2" align="Center" class="hdrtext">C.O.P.S. Monitoring - Online Payment</td>
	</tr>
	<tr>
		<td colspan="2" align="center"><br><span class="subhdrtext">Pay by Check</span><br><br>
		</td>
	</tr>
	<tr><td colspan="2"><img src="/images/personal_check.gif" border="0"></td></tr>
	<tr>
		<td colspan="2">
		<br>
		Payments received after 3:00 PM EST will be posted on the next business day.
		<br>
		<br>
		</td>
	</tr>
	<tr>
		<td colspan="2">
		<b>Dealer Number:</b><br>
		<input type="text" name="dealernumber" size="50" maxlength="255" class="formtext" style="color:000000">
		</td>
	</tr>
	<tr>
		<td colspan="2">
		<b>Dealers Email Address:</b><br>
		<input type="text" name="email" size="50" maxlength="255" class="formtext" style="color:000000">
		</td>
	</tr>
	<tr>
		<td colspan="2">
		<b>Name or Company on Check:</b><br>
		<input type="text" name="name" size="50" maxlength="255" class="formtext" style="color:000000">
		</td>
	</tr>
	<tr>
		<td colspan="2">
		<b>Address:</b><br>
		<input type="text" name="address" size="50" maxlength="255" class="formtext" style="color:000000">
		</td>
	</tr>
	<tr>
		<td colspan="2" style="padding:0,0,0,0">
		<table border="0" cellpadding="5" cellspacing="0" class="formtext">
		<tr>
			<td><b>City:</b><br><input type="text" name="city" size="20" maxlength="100" class="formtext" style="color:000000"></td>
			<td><b>State:</b><br><input type="text" name="state" size="2" maxlength="2" class="formtext" style="color:000000"></td>
			<td><b>Zipcode:</b><br><input type="text" name="zip" size="10" maxlength="10" class="formtext" style="color:000000"></td><tr>
		</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td colspan="2">
		<b>Phone/Fax:</b><br>
		<input type="text" name="phoneFax" size="50" maxlength="255" class="formtext" style="color:000000">
		</td>
	</tr>
	<tr>
		<td colspan="2">
		<b>Bank Name:</b><br>
		<input type="text" name="bankName" size="50" maxlength="255" class="formtext" style="color:000000">
		</td>
	</tr>
	<tr>
		<td colspan="2">
		<b>Bank City/State:</b><br>
		<input type="text" name="bankCityState" size="50" maxlength="255" class="formtext" style="color:000000">
		</td>
	</tr>
	<tr>
		<td colspan="2" style="padding:0,0,0,0">
		<table border="0" cellpadding="5" cellspacing="0" class="formtext">
		<tr>
			<td><b>Check Number:</b><br><input type="text" name="checkNum" size="20" maxlength="50" class="formtext" style="color:000000"></td>
			<td>&nbsp;</td>
			<td><b>Amount of Check:</b><br>
			$<input type="text" name="checkAmtDollars" size="5" maxlength="5" class="formtext" style="color:000000">.<input type="text" name="checkAmtCents" size="2" maxlength="2" class="formtext" style="color:000000">
			</td>
		</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td colspan="2">
		<b>ABA/Transit/Rounting Number:</b><br>
		<input type="text" name="ABATransitRoutingNum" size="10" maxlength="9" class="formtext" style="color:000000"><br>
		<input type="checkbox" name="OnFile" value="Used Info On File" onClick="if (this.form.ABATransitRoutingNum.disabled) {this.form.ABATransitRoutingNum.disabled = false;  this.form.ABATransitRoutingNum.value = '';} else {this.form.ABATransitRoutingNum.disabled = true; this.form.ABATransitRoutingNum.value = 'On File';}"><b>Use Information On File</b>
		</td>
	</tr>
	<tr>
		<td colspan="2">
		<b>Account Number:</b><br>
		<input type="text" name="accountNumber" size="50" maxlength="50" class="formtext" style="color:000000">
		</td>
	</tr>
	<tr>
		<td colspan="2">
		<b>Invoice Number:</b> <br>
		(if left empty, payment will be applied to the oldest open invoice)<br>
		<input type="text" name="invoiceNumber" size="20" maxlength="50" class="formtext" style="color:000000">
		</td>
	</tr>
	<tr>
		<td colspan="2">
		<b>Comments:</b><br>
		<textarea name="comments" rows="7" cols="50" class="formtext" style="color:000000"></textarea>
		</td>
	</tr>
	<tr><td align="right"><input onClick="return checkform(this.form);" type="submit" name="btnSubmitPayment" value="Submit Payment"></td><td width="25%">&nbsp;</td></tr>
	</table>
	</form>
<br>
<table width="500" border="0" cellpadding="5" cellspacing="0">
<tr>
	<td align="center"><a href="/index.cfm">Return to Administrative Menu</a></td>
</tr>
</table>
</div>