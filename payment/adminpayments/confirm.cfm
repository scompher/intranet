<style type="text/css">
	TABLE {font-family:Verdana, Arial, Helvetica, sans-serif;font-size:10px}
	.normal {font-family:Verdana, Arial, Helvetica, sans-serif;font-size:10px}
	.heading {font-family:Arial, Helvetica, sans-serif;font-size:12px}
	.formtext {font-family:Verdana, Arial, Helvetica, sans-serif;font-size:10px}
	.hdrtext {font-family:Arial, Helvetica, sans-serif;font-size:12px;font-weight:bold}
	.subhdrtext {font-family:Verdana, Arial, Helvetica, sans-serif;font-size:10px;font-weight:bold}
</style>

<cfif isDefined("btnVerify")>

	<cfif form.type IS "check">
		<cftransaction>
		<cfquery name="savecheckinfo" datasource="copalink">
			insert into Payments (type, datetime, amount, processed, email, dealernumber, adminuserid, comments)
			values ('#form.type#', #now()#, '#form.checkAmt#', 0, '#form.email#', '#form.dealernumber#', #cookie.adminLogin#, '#form.comments#')
		</cfquery>
		<cfquery name="getID" datasource="copalink">
			select max(paymentid) as newid from payments
		</cfquery>
		<cfquery name="savecheckdetail" datasource="copalink">
			insert into Payments_Check_Detail (paymentid, name, address, city, state, zip, phoneFax, bankName, bankCityState, checkNum, ABATransitRoutingNum, accountNumber, invoiceNumber)
			values (#getid.newid#, '#form.name#', '#form.address#', '#form.city#', '#form.state#', '#form.zip#', '#form.phoneFax#', '#form.bankName#', '#bankCityState#', '#form.checkNum#', '#form.ABATransitRoutingNum#', '#form.accountNumber#', '#form.invoiceNumber#')
		</cfquery>
		</cftransaction>
	<cfelseif form.type IS "credit card">
		<cftransaction>
		<cfquery name="saveccinfo" datasource="copalink">
			insert into Payments (type, datetime, amount, processed, email, dealernumber, adminuserid, comments)
			values ('#form.type#', #now()#, '#form.amount#', 0, '#form.email#', '#form.dealernumber#', #cookie.adminLogin#, '#form.comments#')
		</cfquery>
		<cfquery name="getID" datasource="copalink">
			select max(paymentid) as newid from payments
		</cfquery>
		<cfquery name="saveccdetail" datasource="copalink">
			insert into payments_cc_detail (paymentid, companyName, cardNumber, expMM, expYY, cvv2, cardHolder, invoiceNumber, authorizedBy, address)
			values (#getid.newid#, '#form.companyName#', '#form.cardNumber#', #form.expMM#, #form.expYY#, '#form.cvv2#', '#form.cardHolder#', '#form.invoiceNumber#', '#form.authorizedBy#', '#form.address#')
		</cfquery>
		</cftransaction>
	</cfif>

<!--- send email --->
<cfmail from="payments@copalink.net" to="#form.email#" subject="C.O.P.S. Monitoring Online Payment Receipt" username="copalink@copsmonitoring.com" password="copsmoncal">
Thank you for using COPS Monitoring.  Your payment receipt details are listed below:

Please allow 1 business day for payment to be applied to your account.  Payments received after 3:00 PM EST will be posted on the next business day.
<cfif form.type is "credit card">
An administrative fee of 3% may apply when using a credit card as a method of payment.
</cfif>
Transaction Date...................#dateformat(now(),'mm/dd/yyyy')#
TransactionID......................#getid.newid#
<cfif form.type IS "check">
Amount.............................#form.checkAmt#
Check Number.......................#form.checkNum#
</cfif>
<cfif form.type IS "credit card">
Amount.............................#form.amount#
Credit Card Number.................XXXXXXXXX#right(form.cardNumber,4)#
Expiration Date....................#form.expMM#/#form.expYY#
</cfif>
Comments:
#form.comments#

If you have any questions regarding this payment, please call our Accounting Department at 800-367-2677 ext. 1804

Thank You
</cfmail>

<cfmail from="payments@copalink.net" to="kkortonick@copsmonitoring.com" subject="C.O.P.S. Monitoring Online Payment Notification" username="copalink@copsmonitoring.com" password="copsmoncal">
An online payment has been posted to the site.

You can view it here:

#request.appurl#/login/index.cfm

</cfmail>

<cfmail from="payments@copalink.net" to="pgregory@copsmonitoring.com" subject="C.O.P.S. Monitoring Online Payment Notification" username="copalink@copsmonitoring.com" password="copsmoncal">
An online payment has been posted to the site.

You can view it here:

#request.appurl#/login/index.cfm

</cfmail>

	<cfset cookie.paymentid = getid.newid>
	<meta http-equiv="refresh" content="0;url=receipt.cfm">
	
<cfelse>

<div align="center">
	<form method="post" action="confirm.cfm">
	<cfoutput>
	<input type="hidden" name="type" value="#form.type#">
	</cfoutput>
	<table cellpadding="5" cellspacing="0" border="0" class="formtext">
	<tr>
		<td align="Center" class="hdrtext">C.O.P.S. Monitoring - Online Payment</td>
	</tr>
	<tr>
		<td align="center">
		<br><span class="subhdrtext">Payment Confirmation</span><br><br>
		<div align="left">Please verify that the information below is correct so we may process your payment.</div>
		</td>
	</tr>
	<cfif form.type IS "check">
		<cfoutput>
		<input type="hidden" name="dealernumber" value="#form.dealernumber#">
		<input type="hidden" name="email" value="#form.email#">
		<input type="hidden" name="name" value="#form.name#">
		<input type="hidden" name="address" value="#form.address#">
		<input type="hidden" name="city" value="#form.city#">
		<input type="hidden" name="state" value="#form.state#">
		<input type="hidden" name="zip" value="#form.zip#">
		<input type="hidden" name="phoneFax" value="#form.phoneFax#">
		<input type="hidden" name="bankName" value="#form.bankName#">
		<input type="hidden" name="bankCityState" value="#form.bankCityState#">
		<input type="hidden" name="checkNum" value="#form.checkNum#">
		<input type="hidden" name="checkAmt" value="#form.checkAmtDollars#.#form.checkAmtCents#">
		<input type="hidden" name="ABATransitRoutingNum" value="#form.ABATransitRoutingNum#">
		<input type="hidden" name="accountNumber" value="#form.accountNumber#">
		<input type="hidden" name="invoiceNumber" value="#form.invoiceNumber#">
		<input type="hidden" name="comments" value="#form.comments#">
		<tr>
			<td>
			<b>Dealer Number:</b><br>
			#dealernumber#
			</td>
		</tr>
		<tr>
			<td>
			<b>Dealer email address:</b><br>
			#email#
			</td>
		</tr>
		<tr>
			<td>
			<b>Name or Company on Check:</b><br>
			#name#
			</td>
		</tr>
		<tr>
			<td>
			<b>Address:</b><br>
			#address#
			</td>
		</tr>
		<tr>
			<td style="padding:0,0,0,0">
			<table border="0" cellpadding="5" cellspacing="0" class="formtext">
			<tr>
				<td><b>City:</b><br>#city#</td>
				<td><b>State:</b><br>#state#</td>
				<td><b>Zipcode:</b><br>#zip#</td><tr>
			</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td>
			<b>Phone/Fax:</b><br>
			#phoneFax#
			</td>
		</tr>
		<tr>
			<td>
			<b>Bank Name:</b><br>
			#bankName#
			</td>
		</tr>
		<tr>
			<td>
			<b>Bank City/State:</b><br>
			#bankCityState#
			</td>
		</tr>
		<tr>
			<td style="padding:0,0,0,0">
			<table border="0" cellpadding="5" cellspacing="0" class="formtext">
			<tr>
				<td><b>Check Number:</b><br>#checkNum#</td>
				<td>&nbsp;</td>
				<td><b>Amount of Check:</b><br>$#form.checkAmtDollars#.#form.checkAmtCents#</td>
			</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td>
			<b>ABA/Transit/Rounting Number:</b><br>
			#ABATransitRoutingNum#
			</td>
		</tr>
		<tr>
			<td>
			<b>Account Number:</b><br>
			#accountNumber#
			</td>
		</tr>
		<tr>
			<td>
			<b>Invoice Number:</b><br>
			#invoiceNumber#
			</td>
		</tr>
		<tr>
			<td>
			<b>Comments:</b><br>
			#replace(comments, "#chr(13)#", "<br>", "all")#
			</td>
		</tr>
		</cfoutput>
	<cfelseif form.type IS "credit card">
		<cfoutput>
		<input type="hidden" name="dealernumber" value="#form.dealernumber#">
		<input type="hidden" name="email" value="#form.email#">
		<input type="hidden" name="companyName" value="#form.companyName#">
		<input type="hidden" name="cardNumber" value="#form.cardNumber#">
		<input type="hidden" name="expMM" value="#form.expMM#">
		<input type="hidden" name="expYY" value="#form.expYY#">
		<input type="hidden" name="cvv2" value="#form.cvv2#">
		<input type="hidden" name="cardHolder" value="#form.cardHolder#">
		<input type="hidden" name="address" value="#form.address#">
		<input type="hidden" name="invoiceNumber" value="#form.invoiceNumber#">
		<input type="hidden" name="amount" value="#form.amountDollars#.#form.amountCents#">
		<input type="hidden" name="authorizedBy" value="#form.authorizedBy#">
		<input type="hidden" name="comments" value="#form.comments#">
		<tr>
			<td>
			<b>Dealer Number:</b><br>
			#dealernumber#
			</td>
		</tr>
		<tr>
			<td>
			<b>Dealer email address:</b><br>
			#email#
			</td>
		</tr>
		<tr>
			<td>
			<b>Company Name:</b><br>
			#companyName#
			</td>
		</tr>
		<tr>
			<td>
			<b>Card Number:</b><br>
			#cardNumber#
			</td>
		</tr>
		<tr>
			<td>
			<b>Expiration:</b><br>
			#expMM#/#expYY#
			</td>
		</tr>
		<tr>
			<td>
			<b>CVV2:</b> (3 digits, located on the signature line)<br>
			#cvv2#
			</td>
		</tr>
		<tr>
			<td>
			<b>Card Holder:</b><br>
			#cardHolder#
			</td>
		</tr>
		<tr>
			<td>
			<b>Address:</b><br>
			#address#
			</td>
		</tr>
		<tr>
			<td>
			<b>Invoice Number:</b><br>
			#invoiceNumber#
			</td>
		</tr>
		<tr>
			<td>
			<b>Amount:</b><br>
			$#amountDollars#.#amountCents#
			</td>
		</tr>
		<tr>
			<td>
			<b>Authorized By:</b><br>
			#authorizedBy#
			</td>
		</tr>
		<tr>
			<td>
			<b>Comments:</b><br>
			#replace(comments, "#chr(13)#", "<br>", "all")#
			</td>
		</tr>
		</cfoutput>
	</cfif>
	<tr>
		<td align="center"><br>
		<input type="submit" name="btnVerify" value="This information is correct, please process my payment."><br>
		<input onClick="history.go(-1);" type="button" name="btnEditInfo" value="This is incorrect, return and edit information.">
		</td>
	</tr>
	</table>
	</form>
</div>
</cfif>

