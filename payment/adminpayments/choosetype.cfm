
<cfif isDefined("form.btnContinue")>
	
<cfswitch expression="#form.paymenttype#">
	<cfcase value="check"><cflocation url="paybycheck.cfm"></cfcase>
	<cfcase value="credit card"><cflocation url="paybycc.cfm"></cfcase>
	<cfdefaultcase><cflocation url="choosetype.cfm"></cfdefaultcase>
</cfswitch>
	
<cfelse>
<div align="center">
	<form method="post" action="choosetype.cfm">
	<table cellpadding="5" cellspacing="0" border="0">
	<tr>
		<td colspan="2" align="Center" class="heading"><b>C.O.P.S. Monitoring - Online Payment</b></td>
	</tr>
	<tr>
		<td colspan="2" align="center">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr><td>
		<b>Please choose your payment type:</b><br><br>
		<input type="radio" name="paymenttype" value="check">Check<br>
		<input type="radio" name="paymenttype" value="credit card">Credit Card
		</td></tr>
		</table>
		</td>
	</tr>
	<tr><td align="right"><input type="submit" name="btnContinue" value="Continue"></td><td width="25%">&nbsp;</td></tr>
	</table>
	</form>
<br>
<table width="500" border="0" cellpadding="5" cellspacing="0">
<tr>
	<td align="center"><a href="/index.cfm">Return to Administrative Menu</a></td>
</tr>
</table>

</div>
</cfif>
