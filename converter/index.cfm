<link rel="stylesheet" type="text/css" href="../styles.css">

<cfset err = "">

<cfif isDefined("form.btnConvertDocument")>

	<cffile action="upload" filefield="fileToConvert" destination="#expandpath('.')#\uploadedDocs" nameconflict="makeunique">
	<cfset sourceFile = file.ServerFile>
	<cfset sourceFileName = file.ServerFileName>
	<cfset clientFile = file.ClientFile>
	<cfset clientFileName = file.ClientFileName>
	<cfset destinationFile = "#sourceFileName#.pdf">

	<cfif right(clientFile,1) is "x">
		<cfset err = listappend(err,"docx and xlsx formats are not supported")>
	</cfif>

	<cfif trim(err) is "">

		<CFX_DocumentConverter sourcefile="#expandpath('.')#\uploadedDocs\#sourceFile#" targetfile="#expandpath('.')#\convertedDocs\#destinationFile#">

		<!---<cfdocument format="pdf" srcfile="#expandpath('.')#\uploadedDocs\#sourceFile#"  filename="#expandpath('.')#\convertedDocs\#destinationFile#"></cfdocument>--->
	
		<div align="center" class="normal">
		<b>Document converted successfully.</b>
		<br>
		<br>
		<cfoutput>
		<a class="normal" style="text-decoration:underline" href="convertedDocs/#destinationFile#">Right click here and choose &quot;Save Target As&quot; to download</a>
		</cfoutput>
		<br />
		<br />
		<br />
		<a href="/index.cfm" style="text-decoration:underline;">Return to Intranet</a>
		</div>	
		<cfabort>
	
	</cfif>

</cfif>

<div align="center">
<cfif trim(err) is not "">
<br />
<cfloop list="#err#" index="e">
<cfoutput>
<strong class="alert">#e#</strong>
</cfoutput>
</cfloop>
<br /><br />
</cfif>
<table border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Document Converter</b> </td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
		<form method="post" action="index.cfm" enctype="multipart/form-data">
		<table width="400" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td><b>Upload File to Convert to PDF: </b></td>
			</tr>
			<tr>
				<td>
				<input type="file" name="fileToConvert">
				</td>
			</tr>
			<tr>
				<td class="alert">NOTE: docx and xlsx file formats are not supported</td>
			</tr>
			<tr>
				<td>
					<input name="btnConvertDocument" type="submit" class="sidebar" value="Convert File">
				</td>
			</tr>
		</table>
		</form>
		</td>
	</tr>
</table>
<br />
<a class="normal" href="/index.cfm" style="text-decoration:underline">Return to Intranet Menu</a>
</div>


	