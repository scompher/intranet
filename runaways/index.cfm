<cfsetting showdebugoutput="no">
<cfset timeOccurredTimeZone = "Eastern">

<link rel="stylesheet" type="text/css" href="../styles.css">

<script language="javascript" src="cal2.js">
/*
Xin's Popup calendar script-  Xin Yang (http://www.yxscripts.com/)
Script featured on/available at http://www.dynamicdrive.com/
This notice must stay intact for use
*/
</script>
<script language="javascript" src="cal_conf2.js"></script>

<script language="javascript" src="functions.js"></script>


<div align="center">
<table width="725" border="0" cellpadding="5" cellspacing="0">
	<form method="post" action="index.cfm" name="mainform">
	<tr>
		<td class="highlightbar"><b>Runaway Input Form</b></td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
			<table border="0" cellpadding="5" cellspacing="0" class="grey">
				<tr>
					<td colspan="2" class="nopadding">
					<table border="0" cellpadding="5" cellspacing="0" class="grey">
						<tr>
							<td nowrap="nowrap">Initiated By: </td>
							<td nowrap="nowrap">
								<input name="textfield" type="text" size="50" maxlength="100" value="Philip Gregory" />
							</td>
						</tr>
						<tr>
							<td nowrap="nowrap">Ocurred On: </td>
							<td nowrap="nowrap" class="nopadding"><cfoutput>
								<table border="0" cellpadding="5" cellspacing="0" class="grey">
									<tr>
										<td>Date:</td>
										<td>
											<input name="dateOccurred" type="text" style="width:75px" value="" maxlength="10" />
										</td>
										<td> <a style="text-decoration:none;" href="javascript:showCal('DateOccurred');"> <img src="../images/calicon.gif" alt="Select a Date" align="absmiddle" width="20" height="20" border="0" /> </a> </td>
										<td>Time:</td>
										<td class="nopadding">
											<table border="0" cellpadding="5" cellspacing="0" class="grey">
												<tr>
													<td class="nopadding">
														<input name="timeOccurredHH" type="text" style="width:25px" maxlength="2" />
													</td>
													<td valign="middle">:</td>
													<td class="nopadding">
														<input name="timeOccurredMM" type="text" style="width:25px" maxlength="2" />
													</td>
													<td>
														<select name="timeOccurredTT">
															<option <cfif timeformat(now(),'tt') is "AM">selected</cfif> value="AM">AM</option>
															<option <cfif timeformat(now(),'tt') is "PM">selected</cfif> value="PM">PM</option>
														</select>
													</td>
													</tr>
											</table>
										</td>
									</tr>
								</table>
							</cfoutput></td>
						</tr>
						<tr>
							<td nowrap="nowrap">Rec-Acct #: </td>
							<td>
								<table border="0" cellpadding="1" cellspacing="0" class="grey">
									<tr>
										<td>
											<input name="rec" type="text" style="width:45px" size="0" maxlength="4" />
										</td>
										<td>&ndash;</td>
										<td>
											<input name="acct" type="text" style="width:45px" maxlength="4" />
										</td>
									</tr>
								</table>
							</td>
						</tr>
						
						<tr>
							<td nowrap="nowrap">Initial Count: </td>
							<td nowrap="nowrap">
								<input name="initialCount" type="text" id="initialCount" style="width:35px" size="10" maxlength="50" />
							</td>
						</tr>
						<tr>
							<td nowrap="nowrap">Account Name: </td>
							<td nowrap="nowrap">
								<input name="textfield2" type="text" size="50" maxlength="100" />
							</td>
						</tr>
						<tr>
							<td nowrap="nowrap">Dealer Number: </td>
							<td nowrap="nowrap" class="nopadding">
								<table border="0" cellpadding="5" cellspacing="0" class="grey">
									<tr>
										<td>
											<input name="dealerNumber" type="text" style="width:45px" maxlength="4" />
										</td>
										<td>
											<input onclick="this.form.dealername.value = 'COPS Monitoring, Inc.';" name="Submit3" type="button" class="sidebarsmall" value="Lookup" />
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td nowrap="nowrap">Dealer Name: </td>
							<td nowrap="nowrap">
								<input name="dealername" type="text" size="50" maxlength="100" />
							</td>
						</tr>
						<tr>
							<td nowrap="nowrap">Initial Time: </td>
							<td nowrap="nowrap"><cfoutput>
								<table border="0" cellpadding="5" cellspacing="0" class="grey">
									<tr>
										<td class="nopadding">
											<input name="timeOccurredHH2" type="text" style="width:25px" maxlength="2" />
										</td>
										<td valign="middle">:</td>
										<td class="nopadding">
											<input name="timeOccurredMM2" type="text" style="width:25px" maxlength="2" />
										</td>
										<td>
											<select name="select">
												<option <cfif timeformat(now(),'tt') is "AM">selected</cfif> value="AM">AM</option>
												<option <cfif timeformat(now(),'tt') is "PM">selected</cfif> value="PM">PM</option>
											</select>
										</td>
										</tr>
								</table>
							</cfoutput></td>
						</tr>
						<tr>
							<td nowrap="nowrap">Codes Received: </td>
							<td nowrap="nowrap">
								<input name="textfield222" type="text" size="50" maxlength="100" />
							</td>
						</tr>
						<tr>
							<td nowrap="nowrap">Emergency:</td>
							<td nowrap="nowrap" class="nopadding">
								<table border="0" cellpadding="5" cellspacing="0" class="grey">
									<tr>
										<td>
											<input name="emergency" type="radio" value="Yes" />
										</td>
										<td>Yes</td>
										<td>
											<input name="emergency" type="radio" value="No" />
										</td>
										<td>No</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td nowrap="nowrap">If yes, type: </td>
							<td nowrap="nowrap">
								<input name="textfield2222" type="text" size="50" maxlength="100" />
							</td>
						</tr>
						<tr>
							<td nowrap="nowrap">Verified By: </td>
							<td nowrap="nowrap" class="nopadding">
								<table border="0" cellpadding="5" cellspacing="0" class="grey">
									<tr>
										<td>
											<input name="verifiedby" type="radio" value="PD" />
										</td>
										<td>Police Department </td>
										<td>
											<input name="verifiedby" type="radio" value="FD" />
										</td>
										<td>Fire Department </td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td nowrap="nowrap">Responder Name: </td>
							<td nowrap="nowrap">
								<input name="textfield2223" type="text" size="50" maxlength="100" />
							</td>
						</tr>
						<tr>
							<td nowrap="nowrap">Responding Date/ETA:</td>
							<td nowrap="nowrap" class="nopadding"><cfoutput>
								<table border="0" cellpadding="5" cellspacing="0" class="grey">
									<tr>
										<td>Date:</td>
										<td>
											<input name="dateOccurred2" type="text" style="width:75px" value="" maxlength="10" />
										</td>
										<td> <a style="text-decoration:none;" href="javascript:showCal('DateOccurred');"> <img src="../images/calicon.gif" alt="Select a Date" align="absmiddle" width="20" height="20" border="0" /> </a> </td>
										<td>Time:</td>
										<td class="nopadding">
											<table border="0" cellpadding="5" cellspacing="0" class="grey">
												<tr>
													<td class="nopadding">
														<input name="timeOccurredHH3" type="text" style="width:25px" maxlength="2" />
													</td>
													<td valign="middle">:</td>
													<td class="nopadding">
														<input name="timeOccurredMM3" type="text" style="width:25px" maxlength="2" />
													</td>
													<td>
														<select name="select2">
															<option <cfif timeformat(now(),'tt') is "AM">selected</cfif> value="AM">AM</option>
															<option <cfif timeformat(now(),'tt') is "PM">selected</cfif> value="PM">PM</option>
														</select>
													</td>
													</tr>
											</table>
										</td>
									</tr>
								</table>
							</cfoutput></td>
						</tr>
					</table>
					</td>
				</tr>
				<tr>
					<td colspan="2" nowrap="nowrap">Research &amp; Comments: (include date &amp; time of each call and results of each) </td>
				</tr>
				<tr>
					<td colspan="2" align="center" bgcolor="#FF0000"><b><font color="#FFFFFF">Do not call Commerce Alarm #5714 on any runaways unless it hits 500 signals in history</font></b> </td>
				</tr>
				<tr>
					<td colspan="2" class="nopadding">
					<table border="0" cellpadding="5" cellspacing="0" class="grey">
						<tr>
							<td>First Notification:</td>
							<td class="nopadding"><cfoutput>
								<table border="0" cellpadding="5" cellspacing="0" class="grey">
									<tr>
										<td>Date:</td>
										<td>
											<input name="dateOccurred3" type="text" style="width:75px" value="" maxlength="10" />
										</td>
										<td> <a style="text-decoration:none;" href="javascript:showCal('DateOccurred');"> <img src="../images/calicon.gif" alt="Select a Date" align="absmiddle" width="20" height="20" border="0" /> </a> </td>
										<td>Time:</td>
										<td class="nopadding">
											<table border="0" cellpadding="5" cellspacing="0" class="grey">
												<tr>
													<td class="nopadding">
														<input name="timeOccurredHH4" type="text" style="width:25px" maxlength="2" />
													</td>
													<td valign="middle">:</td>
													<td class="nopadding">
														<input name="timeOccurredMM4" type="text" style="width:25px" maxlength="2" />
													</td>
													<td>
														<select name="select3">
															<option <cfif timeformat(now(),'tt') is "AM">selected</cfif> value="AM">AM</option>
															<option <cfif timeformat(now(),'tt') is "PM">selected</cfif> value="PM">PM</option>
														</select>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</cfoutput></td>
						</tr>
					</table>
					</td>
				</tr>

				<tr>
					<td colspan="2" align="center"><b>Contact dealer, if unable to reach dealer contact premis &amp; RP list</b> </td>
				</tr>
				<tr>
					<td colspan="2">Comments and additional calls: </td>
				</tr>
				<tr>
					<td colspan="2">
						<textarea name="textarea" cols="125" rows="5"></textarea>
					</td>
				</tr>
				<tr>
					<td colspan="2"><b>Check that you have notified the dealer of the following:</b></td>
				</tr>
				<tr>
					<td colspan="2" class="nopadding">
						<table border="0" cellpadding="5" cellspacing="0" class="grey">
							<tr>
								<td>
									<input type="checkbox" name="checkbox" value="checkbox" />
								</td>
								<td>Notified dealer that runaway is causing other alarm signals to be delayed from being received by reducing available capacity on our receivers which prevents other accounts from reporting potential emergencies. </td>
							</tr>
							<tr>
								<td>
									<input type="checkbox" name="checkbox2" value="checkbox" />
								</td>
								<td>Explained to dealer, the runaway is delaying our dispatchers from handling actual alarms, cuasing more labor to be added to maintain response time and quality levels. </td>
							</tr>
							<tr>
								<td>
									<input type="checkbox" name="checkbox3" value="checkbox" />
								</td>
								<td>Explain to dealer that runaways are billed at the rate of .10 per signal for ALL activity (including Test, Dispatcher Handled, &amp; Log In signals) </td>
							</tr>
							<tr>
								<td>
									<input type="checkbox" name="checkbox4" value="checkbox" />
								</td>
								<td>Recommended that the dealer pass the charge through to the subscriber </td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td width="162" nowrap="nowrap">Resolved by: (Employee #) </td>
					<td width="554">
						<input name="dealerNumber2" type="text" style="width:45px" maxlength="4" />
					</td>
				</tr>
				<tr>
					<td>Date/Time Signals Stopped: </td>
					<td class="nopadding"><cfoutput>
						<table border="0" cellpadding="5" cellspacing="0" class="grey">
							<tr>
								<td>Date:</td>
								<td>
									<input name="dateOccurred32" type="text" style="width:75px" value="" maxlength="10" />
								</td>
								<td> <a style="text-decoration:none;" href="javascript:showCal('DateOccurred');"> <img src="../images/calicon.gif" alt="Select a Date" align="absmiddle" width="20" height="20" border="0" /> </a> </td>
								<td>Time:</td>
								<td class="nopadding">
									<table border="0" cellpadding="5" cellspacing="0" class="grey">
										<tr>
											<td class="nopadding">
												<input name="timeOccurredHH42" type="text" style="width:25px" maxlength="2" />
											</td>
											<td valign="middle">:</td>
											<td class="nopadding">
												<input name="timeOccurredMM42" type="text" style="width:25px" maxlength="2" />
											</td>
											<td>
												<select name="select4">
													<option <cfif timeformat(now(),'tt') is "AM">selected</cfif> value="AM">AM</option>
													<option <cfif timeformat(now(),'tt') is "PM">selected</cfif> value="PM">PM</option>
												</select>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</cfoutput></td>
				</tr>
				<tr>
					<td colspan="2" class="nopadding">
						<table width="100%" border="0" cellpadding="5" cellspacing="0" class="grey">
							<tr>
								<td width="14%" nowrap="nowrap">Total Signals: </td>
								<td width="11%">
									<input name="dealerNumber22" type="text" style="width:45px" maxlength="4" />
								</td>
								<td width="11%" nowrap="nowrap">In Testing: </td>
								<td width="11%">
									<input name="dealerNumber222" type="text" style="width:45px" maxlength="4" />
								</td>
								<td width="10%" nowrap="nowrap">In History: </td>
								<td width="11%">
									<input name="dealerNumber223" type="text" style="width:45px" maxlength="4" />
								</td>
								<td width="9%" nowrap="nowrap">Resolved?</td>
								<td width="23%" class="nopadding">
									<table border="0" cellpadding="5" cellspacing="0" class="grey">
										<tr>
											<td width="20">
												<input name="resolved" type="radio" value="Yes" />
											</td>
											<td width="24">Yes</td>
											<td width="20">
												<input name="resolved" type="radio" value="No" />
											</td>
											<td width="19">No</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2" nowrap="nowrap"><b>** This information affects charges to dealer &amp; customer. Please proofread to ensure information is correct **</b></td>
				</tr>
				<tr>
					<td colspan="2" align="center" bgcolor="#FFFF66"><b>All information must be filled out properly before forwarding to dealer support</b></td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="2">
						<input name="Submit" type="submit" class="sidebar" value="Submit Form" />
					 &nbsp;
					 <input name="Submit2" type="submit" class="sidebar" value="Cancel" />
					</td>
				</tr>
			</table>
		</td>
	</tr>
	</form>
</table>
</div>
