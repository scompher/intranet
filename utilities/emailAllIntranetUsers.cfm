
<link rel="stylesheet" type="text/css" href="/styles.css">

<cfif isDefined("form.btnSendEmail")>

<cfquery name="GetEmails" datasource="#ds#">
	select email from admin_users 
	where active = 1 
	order by email asc 
</cfquery>

<cfmail from="pgregory@copsmonitoring.com" to="#email#" subject="#form.subject#" query="getEmails">
#form.emailContent#
</cfmail>

Done.

<cfelse>

	<br>
	
	<div align="center">
	<table border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td class="highlightbar"><b>Email all Intranet Users</b> </td>
		</tr>
		<tr>
			<td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<form method="post" action="emailAllIntranetUsers.cfm">
				<tr>
					<td>Subject:</td>
				</tr>
				<tr>
					<td><input name="subject" type="text" style="width:600px" maxlength="255"></td>
				</tr>
				<tr>
					<td>Content:</td>
				</tr>
				<tr>
					<td><textarea name="emailContent" style="width:600px" rows="10"></textarea></td>
				</tr>
				<tr>
					<td><input type="submit" name="btnSendEmail" value="Send Email"></td>
				</tr>
				</form>
			</table>
			</td>
		</tr>
	</table>
	</div>

</cfif>
