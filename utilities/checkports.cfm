<style type="text/css">
.normal {font-family:Arial, Helvetica, sans-serif; font-size:12px}
</style>

<cfset okCount = 0>

<cfset invalid = false>

<cf_copalink command="testconns">

<div class="normal">
<cfoutput>
#dateformat(now(), 'long')# #timeformat(now(),'long')#<br /><br />
</cfoutput>

	<cfloop list="#result#" delimiters=":" index="item">
		<cfoutput>
			#item# - 
			<cfif findnocase("999944455SC",item) gt 0>
				<cfset okCount = okCount + 1>
				OK
			<cfelseif findnocase("disabled",item) gt 0>
				DISABLED
				<cfset invalid = true>
			<cfelseif findnocase("busy",item) gt 0>
				<cfset okCount = okCount + 1>
				BUSY
			<cfelse>
				INVALID RESPONSE
				<cfset invalid = true>
			</cfif>
			<br />
		</cfoutput>
	</cfloop>
	
	<cfif invalid>
		**************************************<br />
		ONE OR MORE PORTS ARE RETURNING AN INVALID RESPONSE OR ARE DISABLED.
		<br /><br />
		<a href="restart.cfm?action=restart">CLICK HERE TO RESET CONNECTIONS</a> <br />
		**************************************<br />
	</cfif>

	<cfif okCount is 3>
	<br />
	<br />
	ALL PORTS OK [<cfoutput>#okCount#</cfoutput>] <br />
	<cfelse>
	<br />
	<br />
	PORT ERROR [<cfoutput>#okCount#</cfoutput>] <br />
	</cfif>
	<br />
	<br />
	
	<a href="index.cfm">Return to menu</a>

</div>