<CFAPPLICATION NAME="copalink" CLIENTMANAGEMENT="NO" sessionmanagement="yes">

<cfinclude template="/mappings.cfm">

<!--- stuff to prevent caching of pages --->

<!--- Client side cache prevention --->
<meta http-equiv="Expires" content="0">

<!--- Setup our expire times for Netscape and Internet Explorer --->
<cfoutput>
        <!--- Internet Explorer Date Formate: (Fri, 30 Oct 1998 14:19:41 GMT) --->
        <cfset MSIEtimestamp='#dateformat(now(),"DDD")#,#dateformat(now(),"DD")#
		#dateformat(now(),"Mmm")# #timeformat(now(),"HH:MM:SS")#'>

        <!--- Netscape Date Formate: Netscape (Wednesday, Apr 26 2000 17:45:25 PM) --->
        <cfset NETSCAPEtimestamp='#dateformat(now(),"DDDD")#,#dateformat(now(),"MMM")#
		#dateformat(now(),"dd")# #dateformat(now(),"YYY")#
		#timeformat(now(),"HH:MM:SS tt")#'>
</cfoutput>

<!--- Tell HTTP Header to force expire of page - nocache --->
<cfif HTTP_USER_AGENT contains "MSIE">
        <cfheader name="Expires" value="<cfoutput>#MSIEtimestamp#</cfoutput>">
        <cfheader name="Pragma" value="no-cache">
        <cfheader name="cache-control" value="no-cache, no-store, must-revalidate">
<cfelse>
        <cfheader name="Expires" value="<cfoutput>#NETSCAPEtimestamp#</cfoutput>">
        <cfheader name="Pragma" value="no-cache">
        <cfheader name="cache-control" value="no-cache, no-store, must-revalidate">
</cfif>
<!--- stuff to prevent caching of pages --->

