
<cfparam name="form.switchTo" default="">

<cfif isDefined("form.btnDoSwitchover")>
	<cfif trim(form.switchTo) is not "">
		<cfquery name="updateConfig" datasource="#ds#">
			update config_main 
			set D3IpAddress = '#form.switchTo#' 
		</cfquery>
	</cfif>
</cfif>

<cfquery name="getConfig" datasource="#ds#">
	select * from config_main
</cfquery>

<table width="100%" border="0" cellspacing="0" cellpadding="3" class="formtextblack">
	<form method="post" action="socketSwitchover.cfm">
	<tr>
		<td><b>Current Socket Connection Configuration:</b></td>
	</tr>
	<tr>
		<td>
		<cfoutput>
		Current Server IP: #getConfig.D3IpAddress# 
		</cfoutput>
		</td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	<tr>
		<td><b>Switch to:</b> </td>
	</tr>
	<tr>
		<td>
			<select name="switchTo" style="vertical-align:middle">
				<option value=""></option>
				<option value="192.168.3.11">Sarah 192.168.3.11</option>
				<option value="192.168.3.10">Pam 192.168.3.10</option>
			</select> 
			<input type="submit" name="btnDoSwitchover" value="Do Switch" style="vertical-align:middle" />
		</td>
	</tr>
	<tr>
		<td><input type="submit" name="btnTestConnection" value="Test Connection" /></td>
	</tr>
	</form>
	<cfif isDefined("form.btnTestConnection")>
	<tr>
		<td>
		<cftry>
			<cfobject name="system" component="components.system">
			<cfset subcount = system.getSubscriberCount("9999","44455")>
			<cfif not isnumeric(subcount)>
				<cfoutput>#subcount#</cfoutput><br />
			</cfif>
			<b style="color:#009933">Connection OK!</b>
			<cfcatch type="any">
			<b style="color:#FF0000">ERROR: Unable to establish connection</b>
			</cfcatch>
		</cftry>
		</td>
	</tr>
	</cfif>
	<tr>
		<td><br /><a href="index.cfm">Return to Menu</a></td>
	</tr>
</table>