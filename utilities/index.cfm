<cfinclude template="functions.cfm">

<title>INTRANET LIVE Utility Menu</title>

<style type="text/css">
.normal {font-family:Arial, Helvetica, sans-serif; font-size:12px}
.red {font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#FF0000}
.green {font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#006633}
</style>

<cfset runtime = CreateObject("java","java.lang.Runtime").getRuntime()>
<cfset freeMemory = runtime.freeMemory() / 1024 / 1024>
<cfset totalMemory = runtime.totalMemory() / 1024 / 1024>
<cfset maxMemory = runtime.maxMemory() / 1024 / 1024>
<cfset memoryInUse = Round(totalMemory-freeMemory)>

<cfset smdrResult = checkSMDR()>
<cfset slaResult = checkSLA()>
<cfset ahResult = checkAH()>

<div class="normal">
<cfoutput>
<b>INTRANET LIVE Utility Menu</b>
<br />
<br />
#dateformat(now(),'mm/dd/yyyy')# #timeformat(now(),'hh:mm:ss tt')#
<br />
<br />
Maximum Memory Allowed: #numberformat(Round(maxMemory))# MB<br />
Total Allocated Memory: #numberformat(Round(totalMemory))# MB<br />
Free Allocated Memory: #numberformat(Round(freeMemory))# MB<br>
Memory In Use: #numberformat(Round(memoryInUse))# MB<br>
</cfoutput>
<br />
<br />
<li><a href="restartlivecfmx.cfm">Restart CFMX</a></li>
<br />
<br />
<li>
<a href="/utilities/checkSMDR.cfm">Check SMDR</a> 
<cfif smdrResult.status is "RED"><span class="red"><b>Status Red</b></span><cfelse><span class="green"><b>Status Green</b></span></cfif>
</li>
<br />
<br />
<li>
<a href="/utilities/checkSLA.cfm">Check Scrolling List</a>
<cfif slaResult.status is "RED"><span class="red"><b>Status Red</b></span><cfelse><span class="green"><b>Status Green</b></span></cfif>
</li>
<br />
<br />
<li>
<a href="/utilities/checkAccumHist.cfm">Check Accumulated History</a>
<cfif ahResult.status is "RED"><span class="red"><b>Status Red</b></span><cfelse><span class="green"><b>Status Green</b></span></cfif>
</li>
<br />
<br />
<li>
<a href="/utilities/viewSLALog.cfm">View Scrolling List Logfile</a>
</li>
<br />
<br />
<li>
<a href="/utilities/viewAHLog.cfm">View Accumulated History Logfile</a>
</li>
<br />
<br />
<li><a href="restartAccumServer.cfm">Restart Accumulated History Server</a></li>
<br />
<br />
<li><a href="ScrollingAlarmsSwitchover.cfm">Scrolling Alarms Switchover Utility</a></li>
</ul>

</div>
