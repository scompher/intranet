
<cfsetting enablecfoutputonly="no" requesttimeout="240">

<cfparam name="action" default="">

<style type="text/css">
.normal {font-family:Arial, Helvetica, sans-serif; font-size:12px}
</style>

<div class="normal">
<cfoutput>#dateformat(now(), 'long')# #timeformat(now(),'long')#<br /><br /></cfoutput>
<cfswitch expression="#action#">
	<cfcase value="restart">
		<cfif fileExists("D:\java\restartlog.txt")>
			<cffile action="delete" file="D:\java\restartlog.txt">
		</cfif>
		<cftry>
		<cfexecute name="d:\java\restartPICK.bat" timeout="1" outputfile="d:\java\restartlog.txt"></cfexecute>
		<cfcatch type="any"></cfcatch>
		</cftry>
		restarting...
		<meta http-equiv="refresh" content="10;url='restart.cfm?action=read'" />
	</cfcase>
	<cfcase value="read">
		<cfif fileExists("d:\java\restartlog.txt")>
			<cffile action="read" file="d:\java\restartlog.txt" variable="result">
			Result:<br>
			<cfif trim(result) is "">
				Still restarting...<br />
			<cfelse>
				<cfoutput>
				#replace(result, chr(13), "<br />", "all")#
				</cfoutput>			
			</cfif>
			<hr>
		<cfelse>
			Restartlog not generated yet<hr />
		</cfif>
		<cfif trim(result) is "">
			<meta http-equiv="refresh" content="10;url='restart.cfm?action=read'" />
		</cfif>
	</cfcase>
	<cfdefaultcase>No action chosen</cfdefaultcase>
</cfswitch>
<br />
<br />
<a href="index.cfm">Return to menu</a>
</div>
