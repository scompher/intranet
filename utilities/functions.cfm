<cffunction name="checkSMDR" returntype="struct" output="no">
	<cfset result = structNew()>
	
	<cfset lastID = 0>
	
	<cfif fileExists("#request.directpath#\utilities\smdrCheck.txt")>
		<cffile action="read" file="#request.directpath#\utilities\smdrCheck.txt" variable="lastID">
	</cfif>
	
	<cfset lastID = trim(lastID)>
	
	<cfquery name="checkSMDR" datasource="smdr">
		select top 1 itemid from smdrData with (nolock) 
		order by itemid desc 
	</cfquery>
	<cfset currentID = trim(checkSMDR.itemid)>
	
	<cfif trim(currentID) is ""><cfset currentID = 0></cfif>
	
	<cfset result.lastid = lastid>
	<cfset result.currentid = currentid>
	
	<cfif currentID LTE lastID>
		<cfset result.status = "RED">
	<cfelse>
		<cfset result.status = "GREEN">
	</cfif>
	
	<cfreturn result>
</cffunction>

<cffunction name="checkSLA" returntype="struct" output="no">
	<cfset result = structNew()>
	
	<cfset lastID = 0>
	
	<cfif fileExists("#request.directpath#\utilities\slaCheck.txt")>
		<cffile action="read" file="#request.directpath#\utilities\slaCheck.txt" variable="lastID">
	</cfif>
	
	<cfset lastID = trim(lastID)>
	
	<cfquery name="checkSLA" datasource="copalink">
		select top 1 alarmid from recent_alarms with (nolock) 
		order by alarmid desc 
	</cfquery>
	<cfset currentID = trim(checkSLA.alarmid)>
	
	<cfif trim(currentID) is ""><cfset currentID = 0></cfif>
	
	<cfset result.lastid = lastid>
	<cfset result.currentid = currentid>
	
	<cfif currentID LTE lastID>
		<cfset result.status = "RED">
	<cfelse>
		<cfset result.status = "GREEN">
	</cfif>
	
	<cfreturn result>
</cffunction>

<cffunction name="checkAH" returntype="struct" output="no">
	<cfset result = structNew()>
	
	<cfset lastID = 0>
	
	<cfif fileExists("#request.directpath#\utilities\accumHistCheck.txt")>
		<cffile action="read" file="#request.directpath#\utilities\accumHistCheck.txt" variable="lastID">
	</cfif>
	
	<cfset lastID = trim(lastID)>
	
	<cfquery name="checkAH" datasource="copalink">
		select top 1 itemid from accumulatedHistory with (nolock) 
		order by itemid desc 
	</cfquery>
	<cfset currentID = trim(checkAH.itemid)>
	
	<cfif trim(currentID) is ""><cfset currentID = 0></cfif>
	
	<cfset result.lastid = lastid>
	<cfset result.currentid = currentid>
	
	<cfif currentID LTE lastID>
		<cfset result.status = "RED">
	<cfelse>
		<cfset result.status = "GREEN">
	</cfif>
	
	<cfreturn result>
</cffunction>
