
<cfsetting enablecfoutputonly="no" requesttimeout="240">

<cfparam name="action" default="">

<style type="text/css">
.normal {font-family:Arial, Helvetica, sans-serif; font-size:12px}
</style>

<div class="normal">
<cfoutput>#dateformat(now(), 'long')# #timeformat(now(),'long')#<br /><br /></cfoutput>
<cfswitch expression="#action#">
	<cfcase value="restart">
		<cfif fileExists("D:\bat\restart.txt")>
			<cffile action="delete" file="D:\bat\restart.txt">
		</cfif>
		<cftry>
		<cfexecute name="d:\bat\restart.bat" timeout="1" outputfile="d:\bat\restart.txt"></cfexecute>
		<cfcatch type="any"></cfcatch>
		</cftry>
		restarting...
		<meta http-equiv="refresh" content="3;url='restartlivecfmx.cfm?action=read'" />
	</cfcase>
	<cfcase value="read">
		<cfset result = "">
		<cfif fileExists("d:\bat\restart.txt")>
			<cffile action="read" file="d:\bat\restart.txt" variable="result">
			Result:<br>
			<cfif trim(result) is "">
				Still restarting...<br />
			<cfelse>
				<cfoutput>
				#replace(result, chr(13), "<br />", "all")#
				</cfoutput>			
			</cfif>
			<hr>
		<cfelse>
			Restartlog not generated yet<hr />
		</cfif>
		<cfif trim(result) is "">
			<meta http-equiv="refresh" content="5;url='restartlivecfmx.cfm?action=read'" />
		</cfif>
	</cfcase>
	<cfdefaultcase>
	Are you sure you wish to restart LIVE CFMX service?
	<br />
	<br />
	<a href="restartlivecfmx.cfm?action=restart">Yes</a>&nbsp;&nbsp;<a href="index.cfm">No</a>
	</cfdefaultcase>
</cfswitch>
<br />
<br />
<a href="index.cfm">Return to menu</a>
</div>
