<!--- 
<table width="100%" border="0" cellspacing="0" cellpadding="5" class="formtextblack">
	<tr>
		<td>
		<b>Steps to take:</b><br />
		1. Stop service (if its not already stopped) <br />
		2. Update config file and reset ports if necessary<br />
		3. Restart Service<br />
		4. Check Log to ensure connection properly made (also tandem to view data going through)<br />
		5. Repeat if necessary<br />
		</td>
	</tr>
</table>
 --->
<br />

<cfset br = "#chr(13)#">

<cfif isDefined("form.btnDoSwitchover")>

	<cfif trim(form.switchTo) is not "">
		<cffile action="read" file="d:\java\scrollingconfig.dat" variable="fileContent">
		
		<cfloop list="#fileContent#" delimiters="#br#" index="line">
			<cfoutput>
			<cfif findnocase("datasource.hostIP=", line) is not 0>
				<cfset lineToChange = line>
				<cfset newLine = "datasource.hostIP=#switchTo#">
			</cfif>
			</cfoutput>
		</cfloop>
		
		<cfset newFileContent = replace(fileContent, lineToChange, newLine, "all")>
		
		<cffile action="write" file="d:\java\scrollingconfig.dat" output="#trim(newFileContent)#">
	</cfif>
</cfif>

<cfif isDefined("form.StartScroller")>

	<cfexecute name="d:\java\startScroller.bat" timeout="3" variable="outcome"></cfexecute>

</cfif>

<cfif isDefined("form.StopScroller")>

	<cfexecute name="d:\java\stopScroller.bat" timeout="3" variable="outcome"></cfexecute>

</cfif>

<cfif isDefined("form.RestartScroller")>

	<cfexecute name="d:\java\stopScroller.bat" timeout="5" variable="outcome"></cfexecute>
	<cfexecute name="d:\java\startScroller.bat" timeout="5" variable="outcome"></cfexecute>
	
	<span class="formtextblack">
	Scrolling List Server Restarted
	</span>
	<br />
	<br />

</cfif>

<cfif isDefined("form.ViewScrollerLog")>
	
	<style type="text/css">
	.normal {font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000000}
	.red {font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#FF0000}
	.green {font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#006633}
	</style>
	
	<cftry>
	<cffile action="read" file="D:\java\logs\ScrollingAlarmServer.log" variable="fileContent">
	<cfcatch type="any">
		<cfset fileContent = "<b>ERROR OCCURRED: </b><br>" & cfcatch.Detail & "<br>" & cfcatch.Message & "<br><br>">
	</cfcatch>
	</cftry>
	<span class="normal">
	<br />
	<cfoutput>
	#trim(replace(fileContent,br,"<br />","all"))#
	</cfoutput>
	<cfif findnocase("Client /192.168.3.10 connected to port ", fileContent) is not 0>
	<span style="color:#006633">
	<b>Scrolling listener was logged in</b>
	</span>
	<br /><br />
	<cfelse>
	<span style="color:#FF0000">
	<b>According to log file, the scrolling listener could not be logged in</b>
	</span>
	<br /><br />
	</cfif>
	</span>

</cfif>

<cffile action="read" file="d:\java\scrollingconfig.dat" variable="fileContent">

<cfexecute name="d:\java\viewservices.bat" timeout="3" variable="serviceList"></cfexecute>

<table width="100%" border="0" cellspacing="0" cellpadding="3" class="formtextblack">
	<form method="post" action="ScrollingAlarmsSwitchover.cfm">
	<!--- 
	<tr>
		<td>
		<b>
		<cfif findnocase("Copalink Scrolling Alarm Server",serviceList) is not 0>
		<span style="color:#006633">
		Copalink Scrolling Alarm Listener Service IS running
		</span>
		<cfelse>
		<span style="color:#FF0000">
		Copalink Scrolling Alarm Listener Service IS NOT running
		</span>
		</cfif>
		</b>
		</td>
	</tr>
	<tr>
		<td><b>Current Scrolling List Configuration:</b></td>
	</tr>
	<tr>
		<td>
		<cfoutput>
		#replace(fileContent,br,"<br />","all")#
		</cfoutput>
		</td>
	</tr>
	<tr>
		<td><b>Switch to:</b> </td>
	</tr>
	<tr>
		<td>
			<select name="switchTo" style="vertical-align:middle">
				<option value=""></option>
				<option value="192.168.3.11">Sarah 192.168.3.11</option>
				<option value="192.168.3.10">Pam 192.168.3.10</option>
			</select> 
			<input type="submit" name="btnDoSwitchover" value="Update Config File" style="vertical-align:middle" />
		</td>
	</tr>
	--->
	<tr>
		<td>
			<input type="submit" name="ViewServices" value="View Running Services" />
		</td>
	</tr>
	<tr>
		<td>
			<input type="submit" name="StopScroller" value="Stop Scrolling Listener Service" />
		</td>
	</tr>
	<tr>
		<td>
			<input type="submit" name="StartScroller" value="Start Scrolling Listener Service" />
		</td>
	</tr>
	<tr>
		<td>
			<input type="submit" name="RestartScroller" value="Re-Start Scrolling Listener Service" />
		</td>
	</tr>
	<tr>
		<td>
			<input type="submit" name="ViewScrollerLog" value="View Scrolling Listener Service Log" />
		</td>
	</tr>
	</form>
	<tr>
		<td><br /><a href="index.cfm">Return to Menu</a></td>
	</tr>
</table>

<cfif isDefined("form.ViewServices")>

	<cfexecute name="d:\java\viewservices.bat" timeout="3" variable="serviceList"></cfexecute>
	
	<span class="formtextblack">
	<br />
	<b>Currently Running Services:</b><br />
	<cfloop list="#serviceList#" index="svc" delimiters="#br#">
	<cfoutput>
	<cfif trim(svc) is not "D:\ColdFusion9\runtime\bin>net start" and trim(svc) is not "These Windows services are started:" and trim(svc) is not "">
	<a href="/utilities/restartService.cfm?serviceName=#svc#">[restart]</a>
	</cfif> 
	#svc#<br />
	</cfoutput>
	</cfloop>
	<br />
	</span>

</cfif>
