
<link rel="stylesheet" type="text/css" href="../styles.css">

<div align="center">
	<br />
	<table width="500" border="0" cellpadding="5" cellspacing="0" class="grey">
		<tr>
			<td class="highlightbar"><b>Timesheet Entry</b></td>
		</tr>
		<tr>
			<td class="greyrowbottom" style="padding:0px">
				<table border="0" cellpadding="5" cellspacing="0" class="grey">
					
					
					<tr>
						<td width="67" nowrap="nowrap">Action:</td>
						<td width="133">
							<select name="select9">
								<option></option>
								<option>Bug Fix</option>
								<option>Research</option>
								<option>Troubleshooting</option>
								<option>Admin</option>
								<option>Meeting</option>
								<option>Enhancement</option>
							</select>
						</td>
						<td width="2">&nbsp;</td>
						<td width="73">System:</td>
						<td width="185">
							<select name="select10">
								<option></option>
								<option>Digi</option>
								<option>Monitoring</option>
								<option>Email</option>
								<option>Receiver</option>
								<option>Telephone</option>
								<option>G3</option>
								<option>Lan</option>
								<option>PC</option>
								<option>Netpick</option>
								<option>Copalink</option>
								<option>Intranet</option>
							</select>
						</td>
					</tr>
					<tr>
						<td nowrap="nowrap">Project:</td>
						<td>
							<select name="select11">
								<option></option>
								<option>Digi</option>
								<option>RCVR</option>
								<option>All Line</option>
								<option>IT REQ</option>
								<option>Inquiry</option>
								<option>Netpick</option>
								<option>Other</option>
							</select>
						</td>
						<td>&nbsp;</td>
						<td>Status:</td>
						<td>
							<select name="select12">
								<option></option>
								<option>Closed</option>
								<option>Open</option>
								<option>Pending</option>
								<option>Hold</option>
								<option>Forwarded</option>
							</select>
						</td>
					</tr>
					<tr>
						<td nowrap="nowrap">Planned:</td>
						<td>
							<select name="select13">
								<option></option>
								<option>Self-Generated</option>
								<option>Yes</option>
								<option>Driveby</option>
								<option>Email</option>
								<option>Call</option>
								<option>Fire</option>
							</select>
						</td>
						<td>&nbsp;</td>
						<td>Department:</td>
						<td>
							<select name="select14">
								<option></option>
								<option>Ops</option>
								<option>Data Entry</option>
								<option>Dealer Sup</option>
								<option>Accounting</option>
								<option>IT</option>
								<option>Exec</option>
								<option>Dealer</option>
							</select>
						</td>
					</tr>
					<tr>
						<td nowrap="nowrap">Requestor:</td>
						<td colspan="4">
							<input type="text" style="width:300px" name="textfield33" />
						</td>
					</tr>
					
					<tr>
						<td nowrap="nowrap">Start Time: </td>
						<td colspan="4" nowrap="nowrap" class="nopadding">
							<table border="0" cellpadding="5" cellspacing="0" class="grey">
								<tr>
									<td class="nopadding">
										<table border="0" cellpadding="5" cellspacing="0" class="grey">
											<tr>
												<td>
													<input type="text" style="width:25px" name="textfield324" />
												</td>
												<td class="nopadding">:</td>
												<td>
													<input type="text" style="width:25px" name="textfield3223" />
												</td>
												<td>
													<select name="select15">
														<option></option>
														<option>AM</option>
														<option>PM</option>
													</select>
												</td>
											</tr>
										</table>
									</td>
									<td nowrap="nowrap">&nbsp;</td>
									<td nowrap="nowrap">Stop Time:</td>
									<td class="nopadding">
										<table border="0" cellpadding="5" cellspacing="0" class="grey">
											<tr>
												<td>
													<input type="text" style="width:25px" name="textfield3242" />
												</td>
												<td class="nopadding">:</td>
												<td>
													<input type="text" style="width:25px" name="textfield32232" />
												</td>
												<td>
													<select name="select16">
														<option></option>
														<option>AM</option>
														<option>PM</option>
													</select>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="5" nowrap="nowrap">Description:</td>
					</tr>
					<tr>
						<td colspan="5" nowrap="nowrap">
							<textarea name="textarea2" rows="3" style="width:500px"></textarea>
						</td>
					</tr>
					<tr>
						<td colspan="5" nowrap="nowrap">
							<input name="Submit2" type="submit" class="sidebar" value="Enter Time" />
						</td>
					</tr>
				</table>
			</td></tr>
	</table>
	<br />
	<table border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Timesheet Entry</b></td>
	</tr>
	<tr>
		<td class="greyrowbottom" style="padding:0px;">
			<table border="0" cellpadding="5" cellspacing="0" class="grey">
				<form method="post" action="index.cfm">
				<tr>
					<td width="73" nowrap="nowrap">Action:</td>
					<td width="417">
						<select name="select">
							<option></option>
							<option>Bug Fix</option>
							<option>Research</option>
							<option>Troubleshooting</option>
							<option>Admin</option>
							<optioN>Meeting</optioN>
							<option>Enhancement</option>
						</select>
					</td>
				</tr>
				<tr>
					<td nowrap="nowrap">System:</td>
					<td>
						<select name="select2">
							<option></option>
							<option>Digi</option>
							<option>Monitoring</option>
							<option>Email</option>
							<option>Receiver</option>
							<option>Telephone</option>
							<option>G3</option>
							<option>Lan</option>
							<option>PC</option>
							<option>Netpick</option>
							<optioN>Copalink</optioN>
							<option>Intranet</option>
						</select>
					</td>
				</tr>
				<tr>
					<td nowrap="nowrap">Project:</td>
					<td>
						<select name="select3">
							<option></option>
							<option>Digi</option>
							<option>RCVR</option>
							<option>All Line</option>
							<option>IT REQ</option>
							<option>Inquiry</option>
							<option>Netpick</option>
							<option>Other</option>
						</select>
					</td>
				</tr>
				<tr>
					<td nowrap="nowrap">Status:</td>
					<td>
						<select name="select5">
							<option></option>
							<option>Closed</option>
							<option>Open</option>
							<option>Pending</option>
							<option>Hold</option>
							<option>Forwarded</option>
						</select>
					</td>
				</tr>
				<tr>
					<td nowrap="nowrap">Planned:</td>
					<td>
						<select name="select6">
							<option></option>
							<option>Self-Generated</option>
							<option>Yes</option>
							<option>Driveby</option>
							<option>Email</option>
							<option>Call</option>
							<option>Fire</option>
						</select>
					</td>
				</tr>
				<tr>
					<td nowrap="nowrap">Requestor:</td>
					<td>
						<input type="text" style="width:300px" name="textfield3" />
					</td>
				</tr>
				<tr>
					<td nowrap="nowrap">Department:</td>
					<td>
						<select name="select4">
							<option></option>
							<option>Ops</option>
							<option>Data Entry</option>
							<option>Dealer Sup</option>
							<option>Accounting</option>
							<option>IT</option>
							<option>Exec</option>
							<option>Dealer</option>
						</select>
					</td>
				</tr>
				<tr>
					<td nowrap="nowrap">Start Time:</td>
					<td class="nopadding">
						<table border="0" cellpadding="5" cellspacing="0" class="grey">
							<tr>
								<td>
									<input type="text" style="width:25px" name="textfield32" />
								</td>
								<td class="nopadding">:</td>
								<td>
									<input type="text" style="width:25px" name="textfield322" />
								</td>
								<td>
									<select name="select7">
										<option></option>
										<option>AM</option>
										<option>PM</option>
									</select>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td nowrap="nowrap">Stop Time:</td>
					<td class="nopadding">
						<table border="0" cellpadding="5" cellspacing="0" class="grey">
							<tr>
								<td>
									<input type="text" style="width:25px" name="textfield323" />
								</td>
								<td class="nopadding">:</td>
								<td>
									<input type="text" style="width:25px" name="textfield3222" />
								</td>
								<td>
									<select name="select8">
										<option></option>
										<option>AM</option>
										<option>PM</option>
									</select>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td nowrap="nowrap">Description:</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td colspan="2" nowrap="nowrap">
						<textarea name="textarea" rows="3" style="width:500px"></textarea>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<input name="Submit" type="submit" class="sidebar" value="Enter Time" />
					</td>
				</tr>
				</form>
			</table>
		</td></tr>
</table>
</div>

