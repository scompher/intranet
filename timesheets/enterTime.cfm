
<link rel="stylesheet" type="text/css" href="../styles.css">

<cfparam name="d" default="#dateformat(now(),'mm/dd/yyyy')#">
<cfparam name="skipV" default="0">
<cfparam name="form.btnLoadTemplate" default="0">

<cfif form.btnLoadTemplate is 1>
	<cfquery name="gettemplate" datasource="#ds#">
		select * from timesheet_templates
		where templateid = #form.templateid#
	</cfquery>
	
	<cfset form.templateid = gettemplate.templateid>
	<cfset form.templateName = gettemplate.templateName>
	<cfset form.statusid = gettemplate.statusid>
	<cfset form.categoryid = gettemplate.categoryid>
	<cfset form.actionid = gettemplate.actionid>
	<cfset form.systemid = gettemplate.systemid>
	<cfset form.projectid = gettemplate.projectid>
	<cfset form.plannedid = gettemplate.plannedid>
	<cfset form.departmentid = gettemplate.departmentid>
	<cfset form.requestor = gettemplate.requestor>
	<cfset form.description = gettemplate.description>
	<cfset form.startTimeHH = timeformat(gettemplate.startTime, 'hh')>
	<cfset form.startTimeMM = timeformat(gettemplate.startTime, 'mm')>
	<cfset form.startTimeTT = timeformat(gettemplate.startTime, 'tt')>
	<cfset form.stopTimeHH = timeformat(gettemplate.stopTime, 'hh')>
	<cfset form.stopTimeMM = timeformat(gettemplate.stopTime, 'mm')>
	<cfset form.stopTimeTT = timeformat(gettemplate.stopTime, 'tt')>
	<cfset skipV = 0>
</cfif>

<cfparam name="form.templateid" default="0">
<cfparam name="form.templateName" default="">
<cfparam name="form.statusid" default="0">
<cfparam name="form.categoryid" default="0">
<cfparam name="form.actionid" default="0">
<cfparam name="form.systemid" default="0">
<cfparam name="form.projectid" default="0">
<cfparam name="form.plannedid" default="0">
<cfparam name="form.departmentid" default="0">
<cfparam name="form.requestor" default="">
<cfparam name="form.description" default="">
<cfparam name="form.startTimeHH" default="#timeformat(now(),'hh')#">
<cfparam name="form.startTimeMM" default="#timeformat(now(),'mm')#">
<cfparam name="form.startTimeTT" default="#timeformat(now(),'tt')#">
<cfparam name="form.stopTimeHH" default="#timeformat(now(),'hh')#">
<cfparam name="form.stopTimeMM" default="#timeformat(now(),'mm')#">
<cfparam name="form.stopTimeTT" default="#timeformat(now(),'tt')#">
<cfparam name="form.form.btnEnterTime" default="0">

<cfif trim(form.startTimeHH) is ""><cfset form.startTimeHH = "00"></cfif>
<cfif trim(form.startTimeMM) is ""><cfset form.startTimeMM = "00"></cfif>
<cfif trim(form.startTimeTT) is ""><cfset form.startTimeTT = "AM"></cfif>
<cfif trim(form.stopTimeHH) is ""><cfset form.stopTimeHH = "00"></cfif>
<cfif trim(form.stopTimeMM) is ""><cfset form.stopTimeMM = "00"></cfif>
<cfif trim(form.stopTimeTT) is ""><cfset form.stopTimeTT = "AM"></cfif>

<cfif form.btnEnterTime is 1>
	<!--- save entry --->
	<cfset entryDate = createodbcdate(d)>
	<cfset startTime = "#form.startTimeHH#:#form.startTimeMM# #form.startTimeTT#">
	<cfset stopTime = "#form.stopTimeHH#:#form.stopTimeMM# #form.stopTimeTT#">

	<cfset start = timeformat(startTime, 'HH:MM')>
	<cfset stop = timeformat(stopTime, 'HH:MM')>
	<cfset hours = datediff("n",start,stop) / 60>
	<cfset hours = decimalformat(hours)>

	<cfquery name="saveTimeEntry" datasource="#ds#">
		insert into timesheet_main (adminuserid, entryDate, categoryid, actionid, systemid, projectid, statusid, plannedid, departmentid, requestor, startTime, stopTime, description, hours)
		values (#cookie.adminlogin#, #entryDate#, #form.categoryid#, #form.actionid#, #form.systemid#, #form.projectid#, #form.statusid#, #form.plannedid#, #form.departmentid#, '#form.requestor#', '#startTime#', '#stopTime#', '#form.description#', #hours#)
	</cfquery>

	<cfoutput>
	<script language="javascript" type="text/javascript">
	opener.location='viewtime.cfm?d=#form.d#';
	window.close();
	</script>
	</cfoutput>
	
	<cfabort>
</cfif>

<cfquery name="getcats" datasource="#ds#">
	select * from timesheet_categories
	order by category asc
</cfquery>

<cfquery name="getactions" datasource="#ds#">
	select * from timesheet_actions
	order by actionName asc
</cfquery>

<cfquery name="getsystems" datasource="#ds#">
	select * 
	from timesheet_systems
	inner join timesheet_systems_users on timesheet_systems.systemid = timesheet_systems_users.systemid
	where timesheet_systems_users.adminuserid = #cookie.adminlogin# or timesheet_systems_users.adminuserid = -1 and timesheet_systems.active = 1
	order by systemName asc
</cfquery>

<cfquery name="getprojects" datasource="#ds#">
	select * 
	from timesheet_projects
	inner join timesheet_projects_users on timesheet_projects.projectid = timesheet_projects_users.projectid
	where timesheet_projects_users.adminuserid = #cookie.adminlogin# or timesheet_projects_users.adminuserid = -1 and timesheet_projects.active = 1
	order by projectName asc
</cfquery>

<cfquery name="getstatus" datasource="#ds#">
	select * from timesheet_status
	order by statusName asc
</cfquery>

<cfquery name="getplanned" datasource="#ds#">
	select * from timesheet_planned
	order by plannedid asc
</cfquery>

<cfquery name="getdepartments" datasource="#ds#">
	select * from admin_users_departments
	order by department asc
</cfquery>

<cfquery name="getTemplates" datasource="#ds#">
	select * from timesheet_templates
	where adminuserid = #cookie.adminlogin#
	order by templateName asc
</cfquery>

<script language="JavaScript" type="text/javascript">
function validateForm(frm) {

	if (frm.skipV.value == 0) {
		if (frm.categoryid.selectedIndex == 0) {alert('The category is required.'); frm.categoryid.focus(); return false;}
		if (frm.actionid.selectedIndex == 0) {alert('The action is required.'); frm.actionid.focus(); return false;}
		if (frm.statusid.selectedIndex == 0) {alert('The status is required.'); frm.statusid.focus(); return false;}
		if (frm.plannedid.selectedIndex == 0) {alert('Scheduled is required.'); frm.plannedid.focus(); return false;}
		if (frm.departmentid.selectedIndex == 0) {alert('The department is required.'); frm.departmentid.focus(); return false;}
		
		if (frm.startTimeTT.selectedIndex == 0) {alert('You must choose the start times AM or PM.'); return false;}
		if (frm.stopTimeTT.selectedIndex == 0) {alert('You must choose the stop times AM or PM.'); return false;}
	
		var starttime = new Date();
		var stoptime = new Date();
	
		var startHrs = frm.startTimeHH.value;
		var startMins = frm.startTimeMM.value;
		var stopHrs = frm.stopTimeHH.value;
		var stopMins = frm.stopTimeMM.value;
	
		if (frm.startTimeTT.selectedIndex == 1 && startHrs == 12) {startHrs = startHrs - 12;}
		if (frm.startTimeTT.selectedIndex == 1 && startHrs == 12) {stopHrs = stopHrs - 12;}
		if (frm.startTimeTT.selectedIndex == 2 && startHrs != 12) {startHrs = startHrs + 12;}
		if (frm.stopTimeTT.selectedIndex == 2 && stopHrs != 12) {stopHrs = stopHrs + 12;}
	
		starttime.setHours(startHrs);
		starttime.setMinutes(startMins);
		stoptime.setHours(stopHrs);
		stoptime.setMinutes(stopMins);
		
		if (stoptime < starttime) {alert('Stop time must be later than start time.'); return false;}
	
		if (frm.description.value == "") {alert('The description is required.'); frm.description.focus(); return false;}
	
		frm.btnEnterTime.value = 1;
		frm.submit();
	}

}
</script>

<body onLoad="document.forms[0].templateid.focus();">
<div align="center">
<table width="500" border="0" cellpadding="5" cellspacing="0" class="grey">
	<tr>
		<td class="highlightbar"><b>Timesheet Entry</b></td>
	</tr>
	<tr>
		<td class="greyrowbottom" style="padding:0px">
			<table border="0" cellpadding="5" cellspacing="0" class="grey">
				<form method="post" action="enterTime.cfm">
				<input type="hidden" name="btnEnterTime" value="0" />
				<input type="hidden" name="btnLoadTemplate" value="0" />
				<cfoutput>
				<input type="hidden" name="skipV" value="#skipV#">
				<input type="hidden" name="d" value="#d#" />
				</cfoutput>
				<tr>
					<td colspan="5" align="center">Items marked in <b>bold</b> are <b>required.</b></td>
				</tr>
				<tr>
					<td>Template:</td>
					<td colspan="4" style="padding:0px">
						<table border="0" cellpadding="5" cellspacing="0" class="grey">
							<tr>
								<td>
									<select name="templateid" onChange="this.form.skipV.value = 1; this.form.btnLoadTemplate.value = 1; this.form.submit();">
										<option value="0">Select Template</option>
										<cfoutput query="gettemplates">
											<option <cfif form.templateid is gettemplates.templateid>selected</cfif> value="#templateid#">#templateName#</option>
										</cfoutput>
									</select>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><b>Category:</b></td>
					<td>
						<select name="categoryid">
							<option value="0">Select Category</option>
							<cfoutput query="getcats">
								<option <cfif form.categoryid is getcats.categoryid>selected</cfif> value="#categoryid#">#category#</option>
							</cfoutput>
						</select>
					</td>
					<td colspan="3">&nbsp;</td>
				</tr>
				<tr>
					<td width="67" nowrap="nowrap"><b>Task:</b></td>
					<td width="133">
						<select name="actionid">
							<option value="0">Select Task</option>
							<cfoutput query="getactions">
								<option <cfif form.actionid is getactions.actionid>selected</cfif> value="#actionid#">#actionName#</option>
							</cfoutput>
						</select>
					</td>
					<td width="2">&nbsp;</td>
					<td width="73">System:</td>
					<td width="185">
						<select name="systemid">
							<option value="0">Select System</option>
							<cfoutput query="getsystems">
								<option <cfif form.systemid is getsystems.systemid>selected</cfif> value="#systemid#">#systemName#</option>
							</cfoutput>
						</select>
					</td>
				</tr>
				<tr>
					<td nowrap="nowrap">Project:</td>
					<td>
						<select name="projectid">
							<option value="0">Select Project</option>
							<cfoutput query="getprojects">
								<option <cfif form.projectid is getprojects.projectid>selected</cfif> value="#projectid#">#projectName#</option>
							</cfoutput>
						</select>
					</td>
					<td>&nbsp;</td>
					<td><b>Status:</b></td>
					<td>
						<select name="statusid">
							<option value="0">Select Status</option>
							<cfoutput query="getstatus">
								<option <cfif form.statusid is getstatus.statusid>selected</cfif> value="#statusid#">#statusName#</option>
							</cfoutput>
						</select>
					</td>
				</tr>
				<tr>
					<td nowrap="nowrap"><b>Scheduled:</b></td>
					<td>
						<select name="plannedid">
							<option value="0">Select Scheduled</option>
							<cfoutput query="getplanned">
								<option <cfif form.plannedid is getplanned.plannedid>selected</cfif> value="#plannedid#">#planned#</option>
							</cfoutput>
						</select>
					</td>
					<td>&nbsp;</td>
					<td><b>Department:</b></td>
					<td>
						<select name="departmentid">
							<option value="0">Select Department</option>
							<cfoutput query="getdepartments">
								<option <cfif form.departmentid is getdepartments.departmentid>selected</cfif> value="#departmentid#">#department#</option>
							</cfoutput>
						</select>
					</td>
				</tr>
				<tr>
					<td nowrap="nowrap">Requestor:</td>
					<td colspan="4">
						<input type="text" style="width:300px" name="requestor" value="<cfoutput>#form.requestor#</cfoutput>" />
					</td>
				</tr>
				<tr>
					<td nowrap="nowrap"><b>Start Time: </b></td>
					<td colspan="4" nowrap="nowrap" class="nopadding">
						<table border="0" cellpadding="5" cellspacing="0" class="grey">
							<tr>
								<td class="nopadding">
									<table border="0" cellpadding="5" cellspacing="0" class="grey">
										<cfoutput>
										<tr>
											<td>
												<input type="text" style="width:25px" name="startTimeHH" value="#startTimeHH#" onFocus="this.select();" onKeyUp="if (this.value >= 12) {this.form.startTimeTT.selectedIndex = 2;} if (this.value.charAt(this.value.length-1) == ':') {this.value = this.value.slice(0, this.value.length - 1); this.form.startTimeMM.focus();}" />
											</td>
											<td class="nopadding">:</td>
											<td>
												<input type="text" style="width:25px" name="startTimeMM" value="#startTimeMM#" onFocus="this.select();" />
											</td>
											<td>
												<select name="startTimeTT">
													<option value=""></option>
													<option <cfif startTimeTT is "AM">selected</cfif> value="AM">AM</option>
													<option <cfif startTimeTT is "PM">selected</cfif> value="PM">PM</option>
												</select>
											</td>
										</tr>
										</cfoutput>
									</table>
								</td>
								<td nowrap="nowrap">&nbsp;</td>
								<td nowrap="nowrap"><b>Stop Time:</b></td>
								<td class="nopadding">
									<table border="0" cellpadding="5" cellspacing="0" class="grey">
										<cfoutput>
										<tr>
											<td>
												<input type="text" style="width:25px" name="stopTimeHH" value="#stopTimeHH#" onFocus="this.select();" onKeyUp="if (this.value >= 12) {this.form.stopTimeTT.selectedIndex = 2;} if (this.value.charAt(this.value.length-1) == ':') {this.value = this.value.slice(0, this.value.length - 1); this.form.stopTimeMM.focus();}" />
											</td>
											<td class="nopadding">:</td>
											<td>
												<input type="text" style="width:25px" name="stopTimeMM" value="#stopTimeMM#" onFocus="this.select();" />
											</td>
											<td>
												<select name="stopTimeTT">
													<option value=""></option>
													<option <cfif stopTimeTT is "AM">selected</cfif> value="AM">AM</option>
													<option <cfif stopTimeTT is "PM">selected</cfif> value="PM">PM</option>
												</select>
											</td>
										</tr>
										</cfoutput>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="5" nowrap="nowrap"><b>Description:</b></td>
				</tr>
				<tr>
					<td colspan="5" nowrap="nowrap">
						<textarea name="description" rows="3" style="width:500px"><cfoutput>#form.description#</cfoutput></textarea>
					</td>
				</tr>
				<tr>
					<td colspan="5" nowrap="nowrap">
						<input type="button" onClick="validateForm(this.form);" class="sidebar" value="Enter Time" />
						<input type="button" class="sidebar" value="Cancel" onClick="self.close();" />
					</td>
				</tr>
				</form>
			</table>
		</td>
	</tr>
</table>
</div>
</body>
