
<link rel="stylesheet" type="text/css" href="../styles.css">

<cfparam name="d" default="#dateformat(now(),'mm/dd/yyyy')#">

<script language="JavaScript" type="text/javascript">
	function openWin(url) {
		var t = screen.height / 4;
		var l = (screen.width/2) - 300;
		var timeWin = window.open(url, "enterTimeWindow", "width=600,height=415,top=" + t + ",left=" + l);

	}
	function showMonth(urlString) {
		var calendarWin = window.open("showcalendar.cfm?p=viewtime.cfm&" + urlString, "calendarWin", "width=375,height=350");
	}
</script>

<cfquery name="getTime" datasource="#ds#">
	select 
	timesheet_main.*, 
	timesheet_categories.category, 
	timesheet_actions.actionName,
	timesheet_systems.systemName,
	timesheet_projects.projectName,
	timesheet_status.statusName,
	Admin_Users_Departments.department
	from timesheet_main
	left join timesheet_categories on timesheet_main.categoryid = timesheet_categories.categoryid 
	left join timesheet_actions on timesheet_main.actionid = timesheet_actions.actionid
	left join timesheet_systems on timesheet_main.systemid = timesheet_systems.systemid
	left join timesheet_projects on timesheet_main.projectid = timesheet_projects.projectid
	left join timesheet_status on timesheet_main.statusid = timesheet_status.statusid
	left join Admin_Users_Departments on timesheet_main.departmentid = Admin_Users_Departments.departmentid
	where timesheet_main.entryDate = #createodbcdate(d)# and adminuserid = #cookie.adminlogin#
	order by timesheet_main.startTime asc
</cfquery>

<div align="center">
<table width="900" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td valign="middle">
		<img src="../images/edit.gif" alt="Edit" width="16" height="16" align="absmiddle"/> = Edit Item<br />
		<img src="../images/delete.gif" alt="Delete" width="16" height="16" align="absmiddle" /> = Delete Item
		</td>
	</tr>
	<tr>
		<td class="nopadding">
			<table width="100%" border="0" cellpadding="5" cellspacing="0" class="highlightbar">
				<tr>
					<td width="67%"><b>Time Tracking for <cfoutput>#dayofweekasstring(dayofweek(d))# #dateformat(d, "long")#</cfoutput></b></td>
					<td width="33%" nowrap="nowrap">
					<cfoutput>
					<b>
					<a href="viewtime.cfm?d=#dateformat(now(),'mm/dd/yyyy')#" style="text-decoration:underline;">Today</a> | 
					<a href="viewtime.cfm?d=#dateformat(dateadd("d",-1,d),'mm/dd/yyyy')#" style="text-decoration:underline;">Previous Day</a> | 
					<a href="viewtime.cfm?d=#dateformat(dateadd("d",1,d),'mm/dd/yyyy')#" style="text-decoration:underline;">Next Day</a> | 
					<a href="javascript:showMonth('m=#dateformat(now(),'m')#&y=#dateformat(now(),'yyyy')#');" style="text-decoration:underline;">Jump To Day</a>
					</b>
					</cfoutput>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td class="greyrowbottom" style="padding:0px">
			<form method="post" action="viewtime.cfm">
			<table width="100%" border="0" cellpadding="5" cellspacing="0" class="grey">
				<tr>
					<td width="1%"><b>Action</b></td>
					<td><b>Time Span </b></td>
					<td align="center"><b>Hours</b></td>
					<td><b>Category</b></td>
					<td><b>Task</b></td>
					<td><b>System</b></td>
					<td><b>Project</b></td>
					<td><b>Status</b></td>
					<td><b>Dept.</b></td>
					<td><b>Requestor</b></td>
				</tr>
				<cfset totalTime = 0.00>
				<cfoutput query="getTime">
				<cfset totalTime = totalTime + hours>
				<tr>
					<td nowrap="nowrap">
					<a href="javascript: openWin('editTime.cfm?d=#d#&id=#getTime.entryid#');"><img src="../images/edit.gif" alt="Edit" width="16" height="16" border="0" /></a>&nbsp;
					<a href="javascript: if (confirm('Are you sure you wish to delete this item?')) {document.location='delete.cfm?id=#getTime.entryid#&d=#d#';}"><img src="../images/delete.gif" alt="Delete" width="16" height="16" border="0" /></a>					</td>
					<td nowrap="nowrap">#timeformat(startTime, 'hh:mm tt')# - #timeformat(stopTime, 'hh:mm tt')#</td>
					<td align="center">#decimalformat(hours)#</td>
					<td><cfif trim(category) IS NOT "">#category#<cfelse>None</cfif></td>
					<td><cfif trim(actionName) IS NOT "">#actionName#<cfelse>None</cfif></td>
					<td><cfif trim(systemName) IS NOT "">#systemName#<cfelse>None</cfif></td>
					<td><cfif trim(projectName) IS NOT "">#projectName#<cfelse>None</cfif></td>
					<td><cfif trim(statusName) IS NOT "">#statusName#<cfelse>None</cfif></td>
					<td><cfif trim(department) IS NOT "">#department#<cfelse>None</cfif></td>
					<td>#requestor#&nbsp;</td>
				</tr>
				<tr>
					<td colspan="1">&nbsp;</td>
					<td colspan="9">#replace(description, "#chr(13)#", "<br />", "all")#&nbsp;</td></tr>
				</cfoutput>
				<tr><td colspan="10">&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td><b>Total Time</b></td>
					<td align="center"><b><cfoutput>#decimalformat(totalTime)#</cfoutput></b></td>
					<td colspan="7">&nbsp;</td>
					</tr>
				<tr>
					<td colspan="10">
					<cfoutput>
						<input type="button" onclick="openWin('enterTime.cfm?d=#d#');" class="sidebar" value="Add Time Entry">
						&nbsp;
						<!--- 
						<input type="button" onclick="openWin('addSystem.cfm?d=#d#');" class="sidebar" value="Add System" />
						&nbsp;
						--->
						<input type="button" onclick="openWin('createEditTemplate.cfm');" class="sidebar" value="Manage Templates">
					</cfoutput>
					</td>
				</tr>
			</table>
			</form>
		</td>
	</tr>
</table>
<br />
<a class="normal" href="/index.cfm" style="text-decoration:underline">Return to Intranet Menu</a>
</div>

