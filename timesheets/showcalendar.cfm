<link rel="stylesheet" type="text/css" href="../styles.css">

<style type="text/css">
<!--
.calbox {
	border-top-width: 1px;
	border-right-width: 1px;
	border-bottom-width: 1px;
	border-left-width: 1px;
	border-top-style: none;
	border-right-style: solid;
	border-bottom-style: solid;
	border-left-style: solid;
	border-top-color: #000000;
	border-right-color: #000000;
	border-bottom-color: #000000;
	border-left-color: #000000;
}
-->
</style>

<cfparam name="currentmonth" default="#dateformat(now(),'mm')#">
<cfparam name="currentyear" default="#dateformat(now(),'yyyy')#">

<cfif isDefined("url.m")>
	<cfset currentmonth = url.m>
</cfif>
<cfif isDefined("url.y")>
	<cfset currentyear = url.y>
</cfif>

<cfset days = daysinmonth("#currentmonth#/1/#currentyear#")>

<cfset prevmonth = currentmonth - 1>
<cfset prevyear = currentyear>
	<cfif prevmonth IS 0>
		<cfset prevmonth = 12>
		<cfset prevyear = currentyear - 1>
	</cfif>
<cfset nextmonth = currentmonth + 1>
<cfset nextyear = currentyear>
	<cfif nextmonth IS 13>
		<cfset nextmonth = 1>
		<cfset nextyear = currentyear + 1>
	</cfif>

<cfset count = 1>

<table width="350" border="0" cellpadding="2" cellspacing="0">
<tr>
	<td width="100%" style="padding:5px">
	<table width="100%" border="0" cellpadding="5" cellspacing="0" class="highlightbar">
	<tr>
		<td width="33%" valign="bottom" align="center"><cfoutput><a href="showcalendar.cfm?m=#variables.prevmonth#&amp;y=#variables.prevyear#&p=#p#"><b>#monthasstring(variables.prevmonth)#</b></a></cfoutput></td>
		<td width="33%" valign="bottom" align="center"><cfoutput><b>#monthasstring(currentmonth)# #currentyear#</b></cfoutput></td>
		<td width="33%" valign="bottom" align="center"><cfoutput><a href="showcalendar.cfm?m=#variables.nextmonth#&amp;y=#variables.nextyear#&p=#p#"><b>#monthasstring(variables.nextmonth)#</b></a></cfoutput></td>
	</tr>
	</table>
	<table width="100%" border="0" cellpadding="2" cellspacing="0" class="greyrow">
	<tr>
		<td width="40" align="center">Sun</td>
		<td width="40" align="center">Mon</td>
		<td width="40" align="center">Tue</td>
		<td width="40" align="center">Wed</td>
		<td width="40" align="center">Thur</td>
		<td width="40" align="center">Fri</td>
		<td width="40" align="center">Sat</td>
	</tr>
	</table>	
	<table width="100%" cellpadding="2" cellspacing="0" class="greyrowbottom" border="0">
	<cfloop index="x" from="1" to="6">
	<cfif (X IS 6)>
		<cfif NOT isdate("#currentmonth#/#count#/#currentyear#")>
		<cfbreak>
		</cfif>
	</cfif>
	<tr>
		<cfloop index="y" from="1" to="7">
		<cfset currentDate = "#currentmonth#/#count#/#currentyear#">
		<cfif isdate(#variables.currentDate#)>
			<cfif dayofweek(#variables.currentDate#) IS y>
				<td align="center" valign="middle" width="40" height="40">
				<cfoutput>
				<a style="text-decoration: none;" href="javascript:opener.location='#p#?d=#variables.currentDate#'; self.close();">
				<cfif isDefined("url.showdate")>
					<cfif variables.currentdate IS url.showdate>
						<b style="color:ff0000;">#count#</b>
					<cfelse>
						#count#
					</cfif>
				<cfelse>
					<cfif variables.currentdate IS dateformat(now(),'mm/dd/yyyy')>
						<b style="color:ff0000;">#count#</b>
					<cfelse>
						#count#
					</cfif>
				</cfif>
				</a>
				</cfoutput>
				</td>
			<cfset count = count + 1>
			<cfelse>
			<td align="center" valign="middle" width="40" height="40">&nbsp;</td>
			</cfif>
		<cfelse>
			<td width="40" height="40" align="right">&nbsp;</td>
		</cfif>
		</cfloop>
	</tr>
	</cfloop>
	</table>
	</td></tr>
<cfoutput>
<tr>
	<td align="center">
	<br>
	<a href="showcalendar.cfm?m=#dateformat(now(),'m')#&amp;y=#dateformat(now(),'yyyy')#&p=#p#">Jump to current month</a>
	</td>
</tr>
</cfoutput>
</table>
