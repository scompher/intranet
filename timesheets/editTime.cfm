<link rel="stylesheet" type="text/css" href="../styles.css">

<cfparam name="d" default="#dateformat(now(),'mm/dd/yyyy')#">

<cfparam name="form.categoryid" default="0">
<cfparam name="form.actionid" default="0">
<cfparam name="form.systemid" default="0">
<cfparam name="form.projectid" default="0">
<cfparam name="form.plannedid" default="0">
<cfparam name="form.departmentid" default="0">
<cfparam name="form.requestor" default="">
<cfparam name="form.description" default="">
<cfparam name="form.startTimeHH" default="">
<cfparam name="form.startTimeMM" default="">
<cfparam name="form.startTimeTT" default="">
<cfparam name="form.stopTimeHH" default="">
<cfparam name="form.stopTimeMM" default="">
<cfparam name="form.stopTimeTT" default="">

<cfif trim(form.startTimeHH) is ""><cfset form.startTimeHH = "00"></cfif>
<cfif trim(form.startTimeMM) is ""><cfset form.startTimeMM = "00"></cfif>
<cfif trim(form.startTimeTT) is ""><cfset form.startTimeTT = "AM"></cfif>
<cfif trim(form.stopTimeHH) is ""><cfset form.stopTimeHH = "00"></cfif>
<cfif trim(form.stopTimeMM) is ""><cfset form.stopTimeMM = "00"></cfif>
<cfif trim(form.stopTimeTT) is ""><cfset form.stopTimeTT = "AM"></cfif>

<cfif isdefined("form.btnUpdateTime")>
	<!--- save entry --->
	<cfset startTime = "#form.startTimeHH#:#form.startTimeMM# #form.startTimeTT#">
	<cfset stopTime = "#form.stopTimeHH#:#form.stopTimeMM# #form.stopTimeTT#">

	<cfset start = timeformat(startTime, 'HH:MM')>
	<cfset stop = timeformat(stopTime, 'HH:MM')>
	<cfset hours = datediff("n",start,stop) / 60>
	<cfset hours = decimalformat(hours)>

	<cfquery name="saveTimeEntry" datasource="#ds#">
		update timesheet_main 
		set statusid = #form.statusid#, categoryid = #form.categoryid#, actionid = #form.actionid#, systemid = #form.systemid#, projectid = #form.projectid#, plannedid = #form.plannedid#, departmentid = #form.departmentid#, requestor = '#form.requestor#', startTime = '#startTime#', stopTime = '#stopTime#', description = '#form.description#', hours = #hours#
		where entryid = #id#
	</cfquery>

	<cfoutput>
	<script language="javascript" type="text/javascript">
	opener.location='viewtime.cfm?d=#form.d#';
	window.close();
	</script>
	</cfoutput>
	
	<cfabort>
</cfif>

<cfquery name="getcats" datasource="#ds#">
	select * from timesheet_categories
	order by category asc
</cfquery>

<cfquery name="getactions" datasource="#ds#">
	select * from timesheet_actions
	order by actionName asc
</cfquery>

<cfquery name="getsystems" datasource="#ds#">
	select * 
	from timesheet_systems
	inner join timesheet_systems_users on timesheet_systems.systemid = timesheet_systems_users.systemid
	where timesheet_systems_users.adminuserid = #cookie.adminlogin# or timesheet_systems_users.adminuserid = -1 and timesheet_systems.active = 1
	order by systemName asc
</cfquery>

<cfquery name="getprojects" datasource="#ds#">
	select * 
	from timesheet_projects
	inner join timesheet_projects_users on timesheet_projects.projectid = timesheet_projects_users.projectid
	where timesheet_projects_users.adminuserid = #cookie.adminlogin# or timesheet_projects_users.adminuserid = -1 and timesheet_projects.active = 1
	order by projectName asc
</cfquery>

<cfquery name="getstatus" datasource="#ds#">
	select * from timesheet_status
	order by statusName asc
</cfquery>

<cfquery name="getplanned" datasource="#ds#">
	select * from timesheet_planned
	order by plannedid asc
</cfquery>

<cfquery name="getdepartments" datasource="#ds#">
	select * from admin_users_departments
	order by department asc
</cfquery>

<cfquery name="getTime" datasource="#ds#">
	select * from timesheet_main
	where entryid = #id#
</cfquery>

<script language="JavaScript" type="text/javascript">
function validateForm(frm) {

	if (frm.categoryid.selectedIndex == 0) {alert('The category is required.'); frm.categoryid.focus(); return false;}
	if (frm.actionid.selectedIndex == 0) {alert('The action is required.'); frm.actionid.focus(); return false;}
	if (frm.statusid.selectedIndex == 0) {alert('The status is required.'); frm.statusid.focus(); return false;}
	if (frm.plannedid.selectedIndex == 0) {alert('Scheduled is required.'); frm.plannedid.focus(); return false;}
	if (frm.departmentid.selectedIndex == 0) {alert('The department is required.'); frm.departmentid.focus(); return false;}
	
	if (frm.startTimeTT.selectedIndex == 0) {alert('You must choose the start times AM or PM.'); return false;}
	if (frm.stopTimeTT.selectedIndex == 0) {alert('You must choose the stop times AM or PM.'); return false;}

	var starttime = new Date();
	var stoptime = new Date();

	var startHrs = frm.startTimeHH.value;
	var startMins = frm.startTimeMM.value;
	var stopHrs = frm.stopTimeHH.value;
	var stopMins = frm.stopTimeMM.value;

	if (frm.startTimeTT.selectedIndex == 1 && startHrs == 12) {startHrs = startHrs - 12;}
	if (frm.startTimeTT.selectedIndex == 1 && startHrs == 12) {stopHrs = stopHrs - 12;}
	if (frm.startTimeTT.selectedIndex == 2 && startHrs != 12) {startHrs = startHrs + 12;}
	if (frm.stopTimeTT.selectedIndex == 2 && stopHrs != 12) {stopHrs = stopHrs + 12;}

	starttime.setHours(startHrs);
	starttime.setMinutes(startMins);
	stoptime.setHours(stopHrs);
	stoptime.setMinutes(stopMins);
	
	if (stoptime < starttime) {alert('Stop time must be later than start time.'); return false;}

	if (frm.description.value == "") {alert('The description is required.'); frm.description.focus(); return false;}

	frm.btnUpdateTime.value = 1;
	frm.submit();

}
</script>

<div align="center">
<table width="500" border="0" cellpadding="5" cellspacing="0" class="grey">
	<tr>
		<td class="highlightbar"><b>Timesheet Entry</b></td>
	</tr>
	<tr>
		<td class="greyrowbottom" style="padding:0px">
			<table border="0" cellpadding="5" cellspacing="0" class="grey">
				<form method="post" action="editTime.cfm">
				<input type="hidden" name="btnUpdateTime" value="0" />
				<cfoutput>
				<input type="hidden" name="d" value="#d#" />
				<input type="hidden" name="id" value="#id#" />
				</cfoutput>
				<tr>
					<td colspan="5" align="center" nowrap="nowrap">Items marked in <b>bold</b> are <b>required.</b> </td>
					</tr>
				<tr>
					<td nowrap="nowrap"><b>Category:</b></td>
					<td>
						<select name="categoryid">
							<option <cfif gettime.categoryid is 0>selected</cfif> value="0">Select Category</option>
							<cfoutput query="getcats">
								<option <cfif gettime.categoryid is getcats.categoryid>selected</cfif> value="#categoryid#">#category#</option>
							</cfoutput>
						</select>
					</td>
					<td colspan="3">&nbsp;</td>
					</tr>
				<tr>
					<td width="67" nowrap="nowrap"><b>Task:</b></td>
					<td width="133">
						<select name="actionid">
							<option <cfif gettime.actionid is 0>selected</cfif> value="0">Select Task</option>
							<cfoutput query="getactions">
								<option <cfif gettime.actionid is getactions.actionid>selected</cfif> value="#actionid#">#actionName#</option>
							</cfoutput>
						</select>
					</td>
					<td width="2">&nbsp;</td>
					<td width="73">System:</td>
					<td width="185">
						<select name="systemid">
							<option <cfif gettime.systemid is 0>selected</cfif> value="0">Select System</option>
							<cfoutput query="getsystems">
								<option <cfif gettime.systemid is getsystems.systemid>selected</cfif> value="#systemid#">#systemName#</option>
							</cfoutput>
						</select>
					</td>
				</tr>
				<tr>
					<td nowrap="nowrap">Project:</td>
					<td>
						<select name="projectid">
							<option <cfif gettime.projectid is 0>selected</cfif> value="0">Select Project</option>
							<cfoutput query="getprojects">
								<option <cfif gettime.projectid is getprojects.projectid>selected</cfif> value="#projectid#">#projectName#</option>
							</cfoutput>
						</select>
					</td>
					<td>&nbsp;</td>
					<td><b>Status:</b></td>
					<td>
						<select name="statusid">
							<option <cfif gettime.statusid is 0>selected</cfif> value="0">Select Status</option>
							<cfoutput query="getstatus">
								<option <cfif gettime.statusid is getstatus.statusid>selected</cfif> value="#statusid#">#statusName#</option>
							</cfoutput>
						</select>
					</td>
				</tr>
				<tr>
					<td nowrap="nowrap"><b>Scheduled:</b></td>
					<td>
						<select name="plannedid">
							<option <cfif gettime.plannedid is 0>selcted</cfif> value="0">Select Scheduled</option>
							<cfoutput query="getplanned">
								<option <cfif gettime.plannedid is getplanned.plannedid>selected</cfif> value="#plannedid#">#planned#</option>
							</cfoutput>
						</select>
					</td>
					<td>&nbsp;</td>
					<td><b>Department:</b></td>
					<td>
						<select name="departmentid">
							<option <cfif gettime.departmentid is 0>selected</cfif> value="0">Select Department</option>
							<cfoutput query="getdepartments">
								<option <cfif gettime.departmentid is getdepartments.departmentid>selected</cfif> value="#departmentid#">#department#</option>
							</cfoutput>
						</select>
					</td>
				</tr>
				<tr>
					<td nowrap="nowrap">Requestor:</td>
					<td colspan="4">
						<cfoutput>
						<input type="text" style="width:300px" name="requestor" value="#gettime.requestor#" />
						</cfoutput>
					</td>
				</tr>
				<tr>
					<td nowrap="nowrap"><b>Start Time: </b></td>
					<td colspan="4" nowrap="nowrap" class="nopadding">
						<table border="0" cellpadding="5" cellspacing="0" class="grey">
							<tr>
								<td class="nopadding">
									<table border="0" cellpadding="5" cellspacing="0" class="grey">
										<tr>
											<td>
												<cfoutput>
												<input type="text" style="width:25px" name="startTimeHH" value="#timeformat(gettime.starttime,'hh')#" onFocus="if (this.value >= 12) {this.form.startTimeTT.selectedIndex = 2;} this.select();" onKeyUp="if (this.value.charAt(this.value.length-1) == ':') {this.value = this.value.slice(0, this.value.length - 1); this.form.startTimeMM.focus();}" />
												</cfoutput>											</td>
											<td class="nopadding">:</td>
											<td>
												<cfoutput>
												<input type="text" style="width:25px" name="startTimeMM" value="#timeformat(gettime.starttime,'MM')#" onFocus="this.select();" />
												</cfoutput>											</td>
											<td>
												<select name="startTimeTT">
													<option value=""></option>
													<option <cfif timeformat(gettime.starttime, 'tt') is "AM">selected</cfif> value="AM">AM</option>
													<option <cfif timeformat(gettime.starttime, 'tt') is "PM">selected</cfif> value="PM">PM</option>
												</select>
											</td>
										</tr>
									</table>
								</td>
								<td nowrap="nowrap">&nbsp;</td>
								<td nowrap="nowrap"><b>Stop Time:</b></td>
								<td class="nopadding">
									<table border="0" cellpadding="5" cellspacing="0" class="grey">
										<tr>
											<td>
												<cfoutput>
												<input type="text" style="width:25px" name="stopTimeHH" value="#timeformat(gettime.stoptime,'hh')#" onFocus="this.select();" onKeyUp="if (this.value >= 12) {this.form.stopTimeTT.selectedIndex = 2;} if (this.value.charAt(this.value.length-1) == ':') {this.value = this.value.slice(0, this.value.length - 1); this.form.stopTimeMM.focus();}" />
												</cfoutput>											</td>
											<td class="nopadding">:</td>
											<td>
												<cfoutput>
												<input type="text" style="width:25px" name="stopTimeMM" value="#timeformat(gettime.stoptime,'MM')#" onFocus="this.select();" />
												</cfoutput>											</td>
											<td>
												<select name="stopTimeTT">
													<option></option>
													<option <cfif timeformat(gettime.stoptime, 'tt') is "AM">selected</cfif> value="AM">AM</option>
													<option <cfif timeformat(gettime.stoptime, 'tt') is "PM">selected</cfif> value="PM">PM</option>
												</select>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="5" nowrap="nowrap"><b>Description:</b></td>
				</tr>
				<tr>
					<td colspan="5" nowrap="nowrap">
						<textarea name="description" rows="3" style="width:500px"><cfoutput>#gettime.description#</cfoutput></textarea>
					</td>
				</tr>
				<tr>
					<td colspan="5" nowrap="nowrap">
						<!--- <input name="btnUpdateTime" type="submit" class="sidebar" value="Update Time" /> --->
						<input type="button" onClick="validateForm(this.form);" class="sidebar" value="Update Time" />
						<input type="button" class="sidebar" value="Cancel" onClick="self.close();" />
					</td>
				</tr>
				</form>
			</table>
		</td>
	</tr>
</table>
</div>
