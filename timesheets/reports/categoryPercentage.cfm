

<cfset sd = createodbcdate(dateadd("d", -7, ed))>
<cfset ed2 = dateadd("d",1,ed)>
<cfset ed2 = createodbcdate(ed2)>

<cfquery name="getreport" datasource="#ds#">
	select 
	sum(timesheet_main.hours) as totalhours, 
	timesheet_categories.category
	from timesheet_main
	left join timesheet_categories on timesheet_main.categoryid = timesheet_categories.categoryid
	where timesheet_main.entrydate >= #sd# and timesheet_main.entrydate < #ed2#
	group by timesheet_categories.category
	order by timesheet_categories.category
</cfquery>

<cfset total = 37.5 * 8>

<table border="0" cellspacing="3" cellpadding="5">
	<cfoutput>
	<tr>
		<td colspan="3" align="center"><b>Reporting range from #dateformat(sd,'mm/dd/yyyy')# to #dateformat(ed,'mm/dd/yyyy')#</b></td>
	</tr>
	</cfoutput>
	<tr><td colspan="3">&nbsp;</td></tr>
	<tr>
		<td width="60%"><b>Category</b></td>
		<td align="center"><b>Hours</b></td>
		<td align="center"><b>% of total available (<cfoutput>#total#</cfoutput>)</b></td>
	</tr>
	<cfset grandtotalhours = 0>
	<cfset grandtotalpercent = 0>
	<cfoutput query="getreport">
	<cfset percent = (totalhours / total) * 100>
	<tr>
		<td>#category#</td>
		<td align="center">#numberformat(totalhours, "00.00")#</td>
		<td align="center">#numberformat(percent, "00.00")#%</td>
	</tr>
	<cfset grandtotalhours = grandtotalhours + totalhours>
	<cfset grandtotalpercent = grandtotalpercent + percent>
	</cfoutput>
	<cfoutput>
	<tr>
		<td>&nbsp;</td>
		<td><hr></td>
		<td align="center"><hr width="30%"></td>
	</tr>
	<tr>
		<td>Totals</td>
		<td align="center">#numberformat(grandtotalhours, "00.00")#</td>
		<td align="center">#numberformat(grandtotalpercent, "00.00")#%</td>
	</tr>
	</cfoutput>
</table>
