
<cfparam name="form.projectid" default="0">
<cfparam name="form.adminuserid" default="0">

<cfquery name="getProjects" datasource="#ds#">
	select * from timesheet_projects
	order by projectName asc
</cfquery>

<cfquery name="getEmps" datasource="#ds#">
	select distinct admin_users.adminuserid, admin_users.firstname, admin_users.lastname
	from admin_users
	inner join timesheet_main on admin_users.adminuserid = timesheet_main.adminuserid
	order by admin_users.lastname asc, admin_users.firstname asc
</cfquery>

<cfquery name="getProjectTime" datasource="#ds#">
	select 
	distinct timesheet_projects.projectName, 
	timesheet_main.hours 
	from timesheet_main 
	inner join timesheet_projects on timesheet_main.projectid = timesheet_projects.projectid
	where 
	<cfif form.projectid is not 0>timesheet_main.projectid = #form.projectid# and </cfif>
	<cfif form.adminuserid is not 0>timesheet_main.adminuserid = #form.adminuserid# and </cfif>
	1 = 1
	order by projectName asc
</cfquery>

<cfquery name="getCurrentProj" dbtype="query">
	select projectName from getProjects
	where projectid = #form.projectid#
</cfquery>

<cfquery name="getCurrentEmp" dbtype="query">
	select firstname, lastname from getEmps
	where adminuserid = #form.adminuserid#
</cfquery>

<link rel="stylesheet" type="text/css" href="../../styles.css">

<div align="center">
<table width="600" border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td class="highlightbar"><b>Hours by Project Report</b></td>
	</tr>
	<tr>
		<td class="greyrowbottom" style="padding:0px">
			<table border="0" cellpadding="5" cellspacing="0" class="grey">
				<tr>
					<td colspan="2" style="padding:0px">
						<table border="0" cellpadding="5" cellspacing="0" class="grey">
							<form method="post" action="project.cfm">
							<tr>
								<td colspan="6"><b>Filter Controls:</b></td>
								</tr>
							<tr>
								<td>Project:</td>
								<td>
									<select name="projectid">
										<option <cfif form.projectid is 0>selected</cfif> value="0">All</option>
										<cfoutput query="getprojects">
											<option <cfif form.projectid is getprojects.projectid>selected</cfif> value="#getprojects.projectid#">#getprojects.projectname#</option>
										</cfoutput>
									</select>
								</td>
								<td>&nbsp;&nbsp;</td>
								<td>Employee:</td>
								<td>
									<select name="adminuserid">
										<option <cfif form.adminuserid is 0>selected</cfif> value="0">All</option>
										<cfoutput query="getEmps">
											<option <cfif form.adminuserid is getEmps.adminuserid>selected</cfif> value="#getemps.adminuserid#">#getemps.lastname#, #getemps.firstname#</option>
										</cfoutput>
									</select>
								</td>
								<td>
									<input name="Submit" type="submit" class="sidebar" value="Apply Filter" />
								</td>
							</tr>
							</form>
						</table>
					</td>
				</tr>
				<!--- 
				<tr><td colspan="2" class="nopadding">
					<cfoutput>
					<table border="0" cellpadding="5" cellspacing="0" class="grey">
						<tr>
							<td>Project:</td>
							<td><cfif form.projectid is not 0>#getCurrentProj.projectName#<cfelse>All</cfif></td>
							<td width="15">&nbsp;</td>
							<td>Employee:</td>
							<td><cfif form.adminuserid is not 0>#getCurrentEmp.firstname# #getCurrentEmp.lastname#<cfelse>All</cfif></td>
						</tr>
					</table>
					</cfoutput>
				</td></tr>
				--->
				<tr>
					<td><b>Project Name</b></td>
					<td><b>Total Hours</b></td>
				</tr>
				<cfoutput query="getProjectTime" group="projectName">
				<cfset totalhrs = 0.0>
				<tr>
					<td>#projectName#</td>
					<td>
					<cfoutput>
						<cfset totalhrs = totalhrs + getProjectTime.hours>
					</cfoutput>
					#decimalformat(totalHrs)#
					</td>
				</tr>
				</cfoutput>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<br />
<a class="normal" style="text-decoration:underline;" href="/index.cfm">Return to Intranet Menu</a>
</div>
