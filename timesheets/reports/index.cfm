
<link rel="stylesheet" type="text/css" href="../../styles.css">

<div align="center">
<table border="0" cellspacing="0" cellpadding="7">
	<tr>
		<td class="highlightbar" align="center"><b>Timesheet Reporting Menu</b></td>
	</tr>
	<tr>
		<td class="greyrow">1. <a href="projectHours.cfm" style="text-decoration:underline;">Project Hours Detail</a></td>
	</tr>
	<tr>
		<td class="greyrow">2. <a href="projectHoursByDate.cfm" style="text-decoration:underline;">Hours Summary by Date Range</a></td>
	</tr>
	<tr>
		<td class="greyrow">3. <a href="hoursByEmployee.cfm" style="text-decoration:underline;">Project Hours by Employee</a></td>
	</tr>
	<tr>
		<td class="greyrow">4. <a href="taskHoursDetailReport.cfm" style="text-decoration:underline;">Task Hours by Employee Detail</a></td>
	</tr>
	<tr>
		<td class="greyrow">5. <a href="scheduledHours.cfm" style="text-decoration:underline;">Scheduled Hours by Employee Detail</a></td>
	</tr>
	<tr>
		<td class="greyrow">6. <a href="showAllXLS.cfm" style="text-decoration:underline;">Timesheets Full Detail</a></td>
	</tr>
	<tr>
		<td class="greyrow">7. <a href="categoryPercentageXLS.cfm" style="text-decoration:underline;">Category Percentage</a></td>
	</tr>
	<tr>
		<td class="greyrow">8. <a href="employeePercentageXLS.cfm" style="text-decoration:underline;">Employee Percentage</a></td>
	</tr>
	<tr>
		<td class="greyrowbottom">9. <a href="employeePercentageDaily.cfm" style="text-decoration:underline;">Daily Employee Percentage</a></td>
	</tr>
</table>
<br />
<a href="/index.cfm" class="normal" style="text-decoration:underline;">Return to Intranet Menu</a>
</div>

