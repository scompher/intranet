
<!--- <cfset sd = createodbcdate(dateadd("d", -7, ed))> --->
<cfset sd = ed>
<cfset sd = createodbcdate(sd)>
<cfset ed2 = dateadd("d",1,ed)>
<cfset ed2 = createodbcdate(ed2)>

<cfquery name="getreport" datasource="#ds#">
	select 
	admin_users.firstname + ' ' + admin_users.lastname as fullname, 
	timesheet_actions.actionName, 
	admin_users_departments.department, 
	sum(timesheet_main.hours) as totalhours 
	from timesheet_main
	left join timesheet_actions on timesheet_main.actionid = timesheet_actions.actionid
	left join admin_users_departments on timesheet_main.departmentid = admin_users_departments.departmentid
	inner join admin_users on timesheet_main.adminuserid = admin_users.adminuserid
	where timesheet_main.entrydate >= #sd# and timesheet_main.entrydate < #ed2#
	group by admin_users.lastname, admin_users.firstname, timesheet_actions.actionName, admin_users_departments.department 
	order by fullname, timesheet_actions.actionName
</cfquery>

<cfset total = 7.5>

<table border="0" cellspacing="3" cellpadding="5">
	<cfoutput>
	<tr>
		<td colspan="4" align="center"><b>Reporting for #dateformat(sd,'mm/dd/yyyy')#</b></td>
	</tr>
	</cfoutput>
	<tr><td colspan="5">&nbsp;</td></tr>
	<tr>
		<td><b>Employee</b></td>
		<td><b>Task</b></td>
		<td><b>Department</b></td>
		<td align="center"><b>Total Hours</b></td>
		<td align="center"><b>% of Total</b></td>
	</tr>
	<tr><td colspan="4">&nbsp;</td></tr>
	<cfoutput query="getreport" group="fullname">
	<cfset grandtotalhours = 0>
	<cfset grandtotalpercent = 0>
	<tr>
		<td colspan="4"><b>#fullname#</b></td>
	</tr>
	<cfoutput>
	<cfset percent = numberformat((totalhours / total) * 100, "00.00")>
	<tr>
		<td>&nbsp;</td>
		<td>#actionName#</td>
		<td>#department#</td>
		<td align="center">#numberformat(totalhours, "00.00")#</td>
		<td align="center">#numberformat(percent, "00.00")#%</td>
	</tr>
	<cfset grandtotalhours = grandtotalhours + totalhours>
	<cfset grandtotalpercent = grandtotalpercent + percent>
	</cfoutput>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td align="center"><hr width="80%"></td>
		<td align="center"><hr width="80%"></td>
	</tr>
	<tr>
		<td><b>Totals</b></td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td align="center"><b>#numberformat(grandtotalhours, "00.00")#</b></td>
		<td align="center"><b>#numberformat(grandtotalpercent, "00.00")#%</b></td>
	</tr>
	<tr><td colspan="4">&nbsp;</td></tr>
	</cfoutput>
</table>
