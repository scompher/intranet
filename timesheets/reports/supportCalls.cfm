
<cfset ed = createodbcdate(now())>
<cfset sd = dateadd("d", -7, ed)>
<cfset ed = dateadd("d", 1, ed)>

<cfquery name="gettime" datasource="#ds#">
	select timesheet_main.entryDate, timesheet_main.starttime, timesheet_main.hours, admin_users.adminuserid, admin_users.firstname, admin_users.lastname, timesheet_actions.actionName
	from timesheet_main
	inner join admin_users on timesheet_main.adminuserid = admin_users.adminuserid
	inner join timesheet_actions on timesheet_main.actionid = timesheet_actions.actionid
	where 
	timesheet_main.adminuserid IN (16,17) and 
	(timesheet_main.entryDate >= #sd# and timesheet_main.entryDate < #ed#) and 
	timesheet_main.actionid IN (106,107)
	order by lastname, firstname, entrydate, starttime
</cfquery>

<cfset callHrs = 0.0>
<cfset emailHrs = 0.0>

<cfset totalCalls = 0>
<cfset totalEmails = 0>

<cfoutput><b>Support Information between #dateformat(sd,'mm/dd/yyyy')# and #dateformat(dateadd("d",-1,ed),'mmd/dd/yyyy')# for:</b><br /></cfoutput>
<br />
<cfoutput query="gettime" group="adminuserid">
	<b>#firstname# #lastname#</b><br />
	<cfoutput>
		<cfif actionName is "Support Call">
			<cfset callHrs = callHrs + hours>
			<cfset totalCalls = totalCalls + 1>
		<cfelseif actionName is "Support Email">
			<cfset emailHrs = emailHrs + hours>
			<cfset totalEmails = totalEmails + 1>
		</cfif>
	</cfoutput>
	Total Support Calls: #totalCalls#<br />
	Total Support Call Time: #numberFormat(callHrs,"00.00")# Hours<br />
	<br />
	Total Support Emails: #totalEmails#<br />
	Total Support Email Time: #numberFormat(emailHrs,"00.00")# Hours<br />
	<br />
</cfoutput>
