<cfdocument format="pdf" orientation="portrait" scale="75">

<cfset toDate = ed>
<cfset ed = dateadd("d",1,ed)>

<cfset sd = createodbcdate(sd)>
<cfset ed = createodbcdate(ed)>

<cfquery name="getemp" datasource="#ds#">
	select firstname, lastname from admin_users
	where adminuserid = #adminuserid# and active <> 0
</cfquery>

<cfquery name="getProjectInfo" datasource="#ds#">
	select distinct
	timesheet_projects.projectName, 
	sum(timesheet_main.hours) as totalHours 
	from timesheet_main
	left join timesheet_projects on timesheet_main.projectid = timesheet_projects.projectid
	where timesheet_main.entryDate >= #sd# and timesheet_main.entryDate < #ed# and adminuserid = #adminuserid#
	group by projectName 
	order by projectName asc
</cfquery>

<cfquery name="getSystemInfo" datasource="#ds#">
	select distinct
	timesheet_systems.systemName, 
	sum(timesheet_main.hours) as totalHours 
	from timesheet_main
	left join timesheet_systems on timesheet_main.systemid = timesheet_systems.systemid
	where timesheet_main.entryDate >= #sd# and timesheet_main.entryDate < #ed# and adminuserid = #adminuserid#
	group by systemName 
	order by systemName asc
</cfquery>

<cfquery name="getActionInfo" datasource="#ds#">
	select distinct
	timesheet_actions.actionName, 
	sum(timesheet_main.hours) as totalHours 
	from timesheet_main
	left join timesheet_actions on timesheet_main.actionid = timesheet_actions.actionid
	where timesheet_main.entryDate >= #sd# and timesheet_main.entryDate < #ed# and adminuserid = #adminuserid#
	group by actionName 
	order by actionName asc
</cfquery>

<cfquery name="getPlannedInfo" datasource="#ds#">
	select distinct
	timesheet_planned.planned, 
	sum(timesheet_main.hours) as totalHours 
	from timesheet_main
	left join timesheet_planned on timesheet_main.plannedid = timesheet_planned.plannedid
	where timesheet_main.entryDate >= #sd# and timesheet_main.entryDate < #ed# and adminuserid = #adminuserid#
	group by planned 
	order by planned asc
</cfquery>

<div align="center">
<table width="600" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td colspan="2" align="center"><b>C.O.P.S. Monitoring Time Reporting</b></td>
	</tr>
	<tr><td colspan="2">&nbsp;</td></tr>
	<cfoutput>
	<tr>
		<td colspan="2" align="center"><b>Reporting period #dateformat(sd, 'mm/dd/yyyy')# to #dateformat(toDate, 'mm/dd/yyyy')# for #getemp.firstname# #getemp.lastname#</b></td>
	</tr>
	<tr><td colspan="2">&nbsp;</td></tr>
	</cfoutput>
	<tr>
		<td><b>Project</b></td>
		<td><b>Hours</b></td>
	</tr>
	<cfoutput query="getProjectInfo">
	<cfif projectName is not "" and lcase(projectName) is not "other">
	<tr>
		<td>
			#projectName#
		</td>
		<td>#numberformat(totalHours, "00.00")#</td>
	</tr>
	</cfif>
	</cfoutput>
	<tr><td colspan="2">&nbsp;</td></tr>
	<tr>
		<td><b>System</b></td>
		<td><b>Hours</b></td>
	</tr>
	<cfoutput query="getSystemInfo">
	<cfif systemName is not "">
	<tr>
		<td>
		#systemName#
		</td>
		<td>#numberformat(totalHours, "00.00")#</td>
	</tr>
	</cfif>
	</cfoutput>
	<tr><td colspan="2">&nbsp;</td></tr>
	<tr>
		<td><b>Task</b></td>
		<td><b>Hours</b></td>
	</tr>
	<cfoutput query="getActionInfo">
	<cfif actionName is not "">
	<tr>
		<td>
		#actionName#
		</td>
		<td>#numberformat(totalHours, "00.00")#</td>
	</tr>
	</cfif>
	</cfoutput>
	<tr><td colspan="2">&nbsp;</td></tr>
	<tr>
		<td><b>Scheduled</b></td>
		<td><b>Hours</b></td>
	</tr>
	<cfoutput query="getPlannedInfo">
	<cfif planned is not "">
	<tr>
		<td>
		#planned#
		</td>
		<td>#numberformat(totalHours, "00.00")#</td>
	</tr>
	</cfif>
	</cfoutput>
</table>
</div>

</cfdocument>
