<link rel="stylesheet" type="text/css" href="../../styles.css">

<cfif isDefined("form.btnGetReport")>
	
	<cfset urladdress = "#request.AppURL#timesheets/reports/categoryPercentage.cfm">

	<cfhttp method="post" url="#urladdress#" throwonerror="yes">
		<cfhttpparam name="ed" type="formfield" value="#ed#">
	</cfhttp>
	
	<cffile action="write" file="#request.DirectPath#\timesheets\reports\report.xls" output="#cfhttp.FileContent#">
	
	<div align="center">
	<table border="0" cellspacing="0" cellpadding="5" width="400">
		<tr>
			<td class="highlightbar"><b>Category Percentage Reporting</b></td>
		</tr>
		<tr>
			<td align="center" class="greyrow"><br /><a style="text-decoration:underline" class="normal" href="report.xls">Right-click and select "save as" to view your report</a></td>
		</tr>
		<tr><td class="greyrowbottom">&nbsp;</td></tr>
	</table>
	<br />
	<p class="normal"><a style="text-decoration:underline" href="index.cfm">Return to Reporting Menu</a> </p>
	</div>
	
	<cfabort>

</cfif>

<script language="javascript" src="../cal2.js">
/*
Xin's Popup calendar script-  Xin Yang (http://www.yxscripts.com/)
Script featured on/available at http://www.dynamicdrive.com/
This notice must stay intact for use
*/
</script>
<script language="javascript" src="../cal_conf2.js"></script>
<!--- 
<cfquery name="getEmps" datasource="#ds#">
	select * from admin_users
	where defaultdeptid = 1
	order by lastname, firstname
</cfquery>
--->
<div align="center">
<table border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Category Percentage Reporting </b></td>
	</tr>
	<form method="post" action="categoryPercentageXLS.cfm" name="mainform">
	<!--- 
	<tr>
		<td class="greyrow" style="padding:0px">
			<table border="0" cellpadding="5" cellspacing="0" class="grey">
				<tr>
					<td nowrap><b>Select Employee:</b></td>
					<td>
						<select name="adminuserid">
							<option value="0">All</option>
						<cfoutput query="getEmps">
							<option value="#adminuserid#">#firstname# #lastname#</option>
						</cfoutput>
						</select>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	--->
	<tr>
		<td class="greyrow"><b>Select 7 day Reporting Ending Date:</b></td>
	</tr>
	<tr>
		<td class="greyrow" style="padding:0px">
			<cfoutput>
			<table border="0" cellspacing="0" cellpadding="5" class="grey">
				<tr>
					<td>Ending:</td>
					<td><span class="grey">
						<input name="ed" type="text" id="ed" style="width:75px" value="#dateformat(now(),'mm/dd/yyyy')#" maxlength="10" />
					</span></td>
					<td><a style="text-decoration:none;" href="javascript:showCal('endDate');"><img src="/images/calicon.gif" alt="Select a Date" align="absmiddle" width="20" height="20" border="0" /></a></td>
				</tr>
			</table>
			</cfoutput>		</td>
	</tr>
	<tr>
		<td class="greyrowbottom">
			<input name="btnGetReport" type="submit" class="sidebar" value="Get Report">
		</td>
	</tr>
	</form>
</table>
<br />
<p class="normal"><a style="text-decoration:underline" href="index.cfm">Return to Reporting Menu</a> </p>
</div>
