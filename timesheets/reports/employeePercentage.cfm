

<cfset sd = createodbcdate(dateadd("d", -7, ed))>
<cfset ed2 = dateadd("d",1,ed)>
<cfset ed2 = createodbcdate(ed2)>

<cfquery name="getreport" datasource="#ds#">
	select 
	admin_users.firstname + ' ' + admin_users.lastname as fullname, 
	timesheet_categories.category, 
	sum(timesheet_main.hours) as totalhours 
	from timesheet_main
	left join timesheet_categories on timesheet_main.categoryid = timesheet_categories.categoryid
	inner join admin_users on timesheet_main.adminuserid = admin_users.adminuserid
	where timesheet_main.entrydate >= #sd# and timesheet_main.entrydate < #ed2#
	group by admin_users.lastname, admin_users.firstname, timesheet_categories.category
	order by fullname, timesheet_categories.category
</cfquery>

<cfset total = 37.5>

<table border="0" cellspacing="3" cellpadding="5">
	<cfoutput>
	<tr>
		<td colspan="4" align="center"><b>Reporting range from #dateformat(sd,'mm/dd/yyyy')# to #dateformat(ed,'mm/dd/yyyy')#</b></td>
	</tr>
	</cfoutput>
	<tr><td colspan="4">&nbsp;</td></tr>
	<tr>
		<td><b>Employee</b></td>
		<td><b>Category</b></td>
		<td align="center"><b>Total Hours</b></td>
		<td align="center"><b>% of Total</b></td>
	</tr>
	<cfoutput query="getreport" group="fullname">
	<cfset grandtotalhours = 0>
	<cfset grandtotalpercent = 0>
	<tr>
		<td colspan="4">#fullname#</td>
	</tr>
	<cfoutput>
	<cfset percent = (totalhours / total) * 100>
	<tr>
		<td>&nbsp;</td>
		<td>#category#</td>
		<td align="center">#numberformat(totalhours, "00.00")#</td>
		<td align="center">#numberformat(percent, "00.00")#%</td>
	</tr>
	<cfset grandtotalhours = grandtotalhours + totalhours>
	<cfset grandtotalpercent = grandtotalpercent + percent>
	</cfoutput>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td align="center"><hr width="30%"></td>
		<td align="center"><hr width="30%"></td>
	</tr>
	<tr>
		<td>Totals</td>
		<td>&nbsp;</td>
		<td align="center">#numberformat(grandtotalhours, "00.00")#</td>
		<td align="center">#numberformat(grandtotalpercent, "00.00")#%</td>
	</tr>
	<tr><td colspan="4">&nbsp;</td></tr>
	</cfoutput>
</table>

