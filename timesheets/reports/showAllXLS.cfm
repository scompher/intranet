<link rel="stylesheet" type="text/css" href="../../styles.css">

<cfif isDefined("form.btnGetReport")>
	
	<cfset urladdress = "#request.AppURL#timesheets/reports/showAll.cfm">
	<!--- 
	<cfhttp method="post" url="#urladdress#" throwonerror="yes">
		<cfhttpparam name="sd" type="formfield" value="#sd#">
		<cfhttpparam name="ed" type="formfield" value="#ed#">
		<cfhttpparam name="adminuserid" type="formfield" value="#adminuserid#">	
	</cfhttp>
	 --->
	<cfinclude template="showAll.cfm">

	<cffile action="write" file="#request.DirectPath#\timesheets\reports\report.xls" output="#fileContent#">

	<div align="center">
	<table border="0" cellspacing="0" cellpadding="5" width="400">
		<tr>
			<td class="highlightbar"><b>Timesheet Detail Reporting</b></td>
		</tr>
		<tr>
			<td align="center" class="greyrow"><br /><a style="text-decoration:underline" class="normal" href="report.xls">Right-click and select "save as" to view your report</a></td>
		</tr>
		<tr><td class="greyrowbottom">&nbsp;</td></tr>
	</table>
	<br />
	<p class="normal"><a style="text-decoration:underline" href="index.cfm">Return to Reporting Menu</a> </p>
	</div>
	
	<cfabort>

</cfif>

<script language="javascript" src="../cal2.js">
/*
Xin's Popup calendar script-  Xin Yang (http://www.yxscripts.com/)
Script featured on/available at http://www.dynamicdrive.com/
This notice must stay intact for use
*/
</script>
<script language="javascript" src="../cal_conf2.js"></script>

<cfquery name="getEmps" datasource="#ds#">
	select * from admin_users
	where defaultdeptid = 1 and active <> 0 
	order by lastname, firstname
</cfquery>

<div align="center">
<form method="post" action="showAllXLS.cfm" name="mainform">
<table border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Timesheet Detail Reporting </b></td>
	</tr>
	<tr>
		<td class="greyrow" style="padding:0px">
			<table border="0" cellpadding="5" cellspacing="0" class="grey">
				<tr>
					<td nowrap><b>Select Employee:</b></td>
					<td>
						<select name="adminuserid">
							<option value="0">All</option>
						<cfoutput query="getEmps">
							<option value="#adminuserid#">#firstname# #lastname#</option>
						</cfoutput>
						</select>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td class="greyrow"><b>Select Reporting Date Range:</b></td>
	</tr>
	<tr>
		<td class="greyrow" style="padding:0px">
			<cfoutput>
			<table border="0" cellspacing="0" cellpadding="5" class="grey">
				<tr>
					<td>Starting:</td>
					<td class="grey">
						<input name="sd" type="text" id="sd" style="width:75px" value="#dateformat(now(),'mm/dd/yyyy')#" maxlength="10" />
					</td>
					<td class="grey"> <a style="text-decoration:none;" href="javascript:showCal('startDate');"> <img src="/images/calicon.gif" alt="Select a Date" align="absmiddle" width="20" height="20" border="0" /> </a> </td>
					<td width="20" class="grey">&nbsp;</td>
					<td>Ending:</td>
					<td><span class="grey">
						<input name="ed" type="text" id="ed" style="width:75px" value="#dateformat(now(),'mm/dd/yyyy')#" maxlength="10" />
					</span></td>
					<td><a style="text-decoration:none;" href="javascript:showCal('endDate');"><img src="/images/calicon.gif" alt="Select a Date" align="absmiddle" width="20" height="20" border="0" /></a></td>
				</tr>
			</table>
			</cfoutput>		</td>
	</tr>
	<tr>
		<td class="greyrowbottom">
			<input name="btnGetReport" type="submit" class="sidebar" value="Get Report">
		</td>
	</tr>
</table>
<br />
<p class="normal"><a style="text-decoration:underline" href="index.cfm">Return to Reporting Menu</a> </p>
</form>
</div>
