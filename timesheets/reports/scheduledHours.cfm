
<script language="javascript" src="../cal2.js">
/*
Xin's Popup calendar script-  Xin Yang (http://www.yxscripts.com/)
Script featured on/available at http://www.dynamicdrive.com/
This notice must stay intact for use
*/
</script>
<script language="javascript" src="../cal_conf2.js"></script>

<cfparam name="sd" default="#now()#">
<cfparam name="ed" default="#now()#">
<cfparam name="form.plannedid" default="0">
<cfparam name="form.adminuserid" default="0">

<cfquery name="getPlanned" datasource="#ds#">
	select * from timesheet_planned
	order by planned asc
</cfquery>

<cfquery name="getEmps" datasource="#ds#">
	select distinct admin_users.adminuserid, admin_users.firstname, admin_users.lastname
	from admin_users
	inner join timesheet_main on admin_users.adminuserid = timesheet_main.adminuserid
	where admin_users.active <> 0 
	order by admin_users.lastname asc, admin_users.firstname asc
</cfquery>

<cfquery name="getTime" datasource="#ds#">
	select 
	timesheet_main.hours, timesheet_main.entryDate, timesheet_main.description, 
	admin_users.firstname, admin_users.lastname, admin_users.adminuserid, 
	timesheet_planned.planned, timesheet_planned.plannedid
	from timesheet_main 
	left join timesheet_planned on timesheet_main.plannedid = timesheet_planned.plannedid
	inner join admin_users on timesheet_main.adminuserid = admin_users.adminuserid 
	where 
	<cfif sd is not "">
		timesheet_main.entryDate >= #createodbcdate(sd)# and 
	</cfif>
	<cfif ed is not "">
		timesheet_main.entryDate < #createodbcdate(dateadd("d", 1, ed))# and 
	</cfif>
	<cfif form.plannedid is not 0>
		timesheet_planned.plannedid = #form.plannedid# and 
	</cfif>
	<cfif form.adminuserid is not 0>
		timesheet_main.adminuserid = #form.adminuserid# and 
	</cfif>
	1=1
	order by timesheet_planned.planned asc, admin_users.lastname asc, admin_users.firstname asc, timesheet_main.entryDate asc
</cfquery>

<link rel="stylesheet" type="text/css" href="../../styles.css">

<div align="center">
<table width="600" border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td class="highlightbar"><b>Task Hours Detail Report</b></td>
	</tr>
	<tr>
		<td class="greyrow" style="padding:0px;">
		<form method="post" action="scheduledHours.cfm" name="mainform">
			<table border="0" cellpadding="5" cellspacing="0" class="grey">
				<cfoutput>
				<tr>
					<td colspan="8" style="padding:0px">
					<table border="0" cellspacing="0" cellpadding="5" class="grey">
						<tr>
							<td><b>Starting:</b></td>
							<td class="grey">
								<input name="sd" type="text" id="sd" style="width:75px" value="#dateformat(sd,'mm/dd/yyyy')#" maxlength="10" />
							</td>
							<td class="grey"> <a style="text-decoration:none;" href="javascript:showCal('startDate');"> <img src="/images/calicon.gif" alt="Select a Date" align="absmiddle" width="20" height="20" border="0" /> </a> </td>
							<td width="20" class="grey">&nbsp;</td>
							<td><b>Ending:</b></td>
							<td><span class="grey">
								<input name="ed" type="text" id="ed" style="width:75px" value="#dateformat(ed,'mm/dd/yyyy')#" maxlength="10" />
							</span></td>
							<td><a style="text-decoration:none;" href="javascript:showCal('endDate');"><img src="/images/calicon.gif" alt="Select a Date" align="absmiddle" width="20" height="20" border="0" /></a></td>
						</tr>
					</table>
					</td>
				</tr>
				</cfoutput>
				<tr>
					<td><b>Scheduled:</b></td>
					<td>
						<select name="plannedid">
							<option value="0">All</option>
							<cfoutput query="getPlanned">
								<option <cfif getPlanned.plannedid is form.plannedid>selected</cfif> value="#plannedid#">#planned#</option>
							</cfoutput>
						</select>
					</td>
					<td>&nbsp;</td>
					<td><b>Employee:</b></td>
					<td>
						<select name="adminuserid">
							<option <cfif form.adminuserid is 0>selected</cfif> value="0">All</option>
							<cfoutput query="getEmps">
								<option <cfif form.adminuserid is getEmps.adminuserid>selected</cfif> value="#getemps.adminuserid#">#getemps.lastname#, #getemps.firstname#</option>
							</cfoutput>
						</select>
					</td>
					<td>
						<input name="btnGetReport" type="submit" class="sidebar" value="Get Report">
					</td>
				</tr>
			</table>
		</form>
		</td>
	</tr>
	<cfif isdefined("form.btnGetReport")>
	<tr>
		<td class="greyrow">&nbsp;</td>
	</tr>
	<tr>
		<td class="greyrowbottom" style="padding:0px">
			<table border="0" cellpadding="5" cellspacing="0" class="grey">
				<tr>
					<td><b>Scheduled/Employee</b></td>
					<td align="center"><b>Hours</b></td>
					<td align="center"><b>Date</b></td>
					<td><b>Description</b></td>
				</tr>
				<cfoutput query="getTime" group="adminuserid">
					<cfset totalHours = 0>
					<tr>
						<td colspan="4">
						<cfif trim(planned) IS "">
							<b>None?</b>
						<cfelse>
							<b>#firstname# #lastname#</b>
						</cfif>
						</td>
					</tr>
					<cfoutput>
						<cfset totalHours = totalHours + hours>
						<tr>
							<td valign="top">&nbsp;&nbsp;&nbsp;&nbsp;#firstname# #lastname#</td>
							<td valign="top" align="center">#numberformat(hours,'00.00')#</td>
							<td valign="top" align="center">#dateformat(entryDate, 'mm/dd/yyyy')#</td>
							<td>#left(description,100)#<cfif len(description) gt 100>...</cfif></td>
						</tr>
					</cfoutput>
					<tr>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;<b>Total Hours</b></td>
						<td nowrap="nowrap" align="center"><b>#numberformat(totalHours,'00.00')#</b></td>
						<td>&nbsp;</td>
					</tr>
				</cfoutput>
			</table>
		</td>
	</tr>
	<cfelse>
	<tr>
		<td class="greyrowbottom">&nbsp;</td>
	</tr>
	</cfif>
</table>
<br>
<a class="normal" style="text-decoration:underline;" href="index.cfm">Return to Reporting Menu</a>
</div>
