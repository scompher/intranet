
<cfquery name="getusers" datasource="#ds#">
	select distinct 
	admin_users.adminuserid, 
	admin_users.firstname, 
	admin_users.lastname  
	from admin_users
	inner join Admin_Users_Departments_Lookup on admin_users.adminuserid = Admin_Users_Departments_Lookup.adminuserid
	where Admin_Users_Departments_Lookup.departmentid IN (1,4)
</cfquery>

<div align="center">
<table border="0" cellspacing="0" cellpadding="5">
	<form action="process.cfm" method="post" enctype="multipart/form-data">
	<tr>
		<td>Enter Date: 
			<input type="text" name="d">
		</td>
	</tr>
	<tr>
		<td>Select User: 
			<select name="adminuserid">
				<option value="0"></option>
				<cfoutput query="getusers">
					<option value="#getusers.adminuserid#">#adminuserid# = #firstname# #lastname#</option>
				</cfoutput>
			</select>
		</td>
	</tr>
	<tr>
		<td>Upload File: 
			<input name="inputFile" type="file" id="inputFile">
		</td>
	</tr>
	<tr>
		<td>
			<input type="submit" name="Submit" value="Process">
		</td>
	</tr>
	</form>
</table>
</div>
