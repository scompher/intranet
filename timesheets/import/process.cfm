<cfsetting showdebugoutput="no">

<cfoutput><h3>Timesheet import for #dateformat(d, 'mm/dd/yyyy')#</h3></cfoutput>

<cffile action="upload" filefield="inputFile" destination="#request.DirectPath#\timesheets\import\" nameconflict="makeunique">
<cfset savedfile = file.ServerFile>
<cfset uploadefile = file.ClientFile>

<cffile action="read" file="#request.DirectPath#\timesheets\import\#savedFile#" variable="input">
<cfset t = "|">
<cfset count = 1>

<cfloop list="#input#" delimiters="#chr(13)#" index="line">
	<cfif trim(line) is not "" and count gt 1>
		<cfset line = replace(line, "#chr(9)#", "|", "all")>

		<cfset EndLoop = 0>
		<cfloop condition="EndLoop EQ 0">
			<cfif find("||",line) is 0>
				<cfset Endloop = 1>
			<cfelse>
				<cfset line = Replace(line,"||","| |","All")>
			</cfif>
		</cfloop>

		<cfoutput>#line#<br /></cfoutput>
		<cfset starttime = listgetat(line,1,t)>
		<cfset stoptime = listgetat(line,2,t)>
		<cfif listlen(line,t) gte 3><cfset description = listgetat(line,3,t)><cfelse><cfset description = ""></cfif>
		<cfif listlen(line,t) gte 4><cfset action = listgetat(line,4,t)><cfelse><cfset action = ""></cfif>
		<cfif listlen(line,t) gte 5><cfset system = listgetat(line,5,t)><cfelse><cfset system = ""></cfif>
		<cfif listlen(line,t) gte 6><cfset project = listgetat(line,6,t)><cfelse><cfset project = ""></cfif>
		<cfif listlen(line,t) gte 7><cfset requestor = listgetat(line,7,t)><cfelse><cfset requestor = ""></cfif>
		<cfif listlen(line,t) gte 8><cfset department = listgetat(line,8,t)><cfelse><cfset department = ""></cfif>
		<cfif department is "IT"><cfset department = "I.T."></cfif>
		<cfif listlen(line,t) gte 9><cfset status = listgetat(line,9,t)><cfelse><cfset status = ""></cfif>
		<cfif listlen(line,t) gte 10><cfset planned = listgetat(line,10,t)><cfelse><cfset planned = ""></cfif>
		
		<cfoutput>
		start = #starttime#<br />
		stop = #stoptime#<br />
		description = #description#<br />
		action = #action#<br />
		system = #system#<br />
		project = #project#<br />
		requestor = #requestor#<br />
		department = #department#<br />
		status = #status#<br />
		planned = #planned#<br />
		</cfoutput>
		
		<!--- drop into database, get IDs for selections --->
		<cfquery name="getactionid" datasource="#ds#">
			select actionid from timesheet_actions where actionName = '#trim(action)#'
		</cfquery>
		<cfif getactionid.recordcount is not 0><cfset actionid = getactionid.actionid><cfelse><cfset actionid = 0></cfif>
		
		<cfquery name="getsystemid" datasource="#ds#">
			select systemid from timesheet_systems where systemName = '#trim(system)#'
		</cfquery>
		<cfif getsystemid.recordcount is not 0><cfset systemid = getsystemid.systemid><cfelse><cfset systemid = 0></cfif>
		
		<cfquery name="getprojectid" datasource="#ds#">
			select projectid from timesheet_projects where projectName = '#trim(project)#'
		</cfquery>
		<cfif getprojectid.recordcount is not 0><cfset projectid = getprojectid.projectid><cfelse><cfset projectid = 0></cfif>
		
		<cfquery name="getdepartmentid" datasource="#ds#">
			select departmentid from admin_users_departments where department = '#trim(department)#'
		</cfquery>
		<cfif getdepartmentid.recordcount is not 0><cfset departmentid = getdepartmentid.departmentid><cfelse><cfset departmentid = 0></cfif>
		
		<cfquery name="getstatusid" datasource="#ds#">
			select statusid from timesheet_status where statusName = '#trim(status)#'
		</cfquery>
		<cfif getstatusid.recordcount is not 0><cfset statusid = getstatusid.statusid><cfelse><cfset statusid = 0></cfif>
		
		<cfquery name="getplannedid" datasource="#ds#">
			select plannedid from timesheet_planned where planned = '#trim(planned)#'
		</cfquery>
		<cfif getplannedid.recordcount is not 0><cfset plannedid = getplannedid.plannedid><cfelse><cfset plannedid = 0></cfif>
	
	<cfoutput>
	actionid = #actionid#<br />
	systemid = #systemid#<br />
	projectid = #projectid#<br />
	departmentid = #departmentid#<br />
	statusid = #statusid#<br />
	plannedid = #plannedid#<br />
	</cfoutput>

	<!--- insert into database --->
	<cfset entryDate = createodbcdate(d)>
	<cfset starttime = createodbctime(starttime)>
	<cfset stoptime = createodbctime(stoptime)>
	<cfset start = timeformat(starttime, 'HH:MM')>
	<cfset stop = timeformat(stoptime, 'HH:MM')>
	<cfset hours = datediff("n",start,stop) / 60>
	<cfset hours = decimalformat(hours)>

	<cfquery name="saveTimeEntry" datasource="#ds#">
		insert into timesheet_main (adminuserid, entryDate, actionid, systemid, projectid, plannedid, departmentid, requestor, startTime, stopTime, description, hours)
		values (#adminuserid#, #entryDate#, #actionid#, #systemid#, #projectid#, #plannedid#, #departmentid#, '#requestor#', #startTime#, #stopTime#, '#description#', #hours#)
	</cfquery>

	</cfif>

	<hr />
	<cfset count = count + 1>
</cfloop>

