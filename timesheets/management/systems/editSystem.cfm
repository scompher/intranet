<link rel="stylesheet" type="text/css" href="../../../styles.css">

<cfif not isdefined("assignedList")>
	<cfquery name="getsys" datasource="#ds#">
		select timesheet_systems.*, timesheet_systems_users.adminuserid
		from timesheet_systems
		left join timesheet_systems_users on timesheet_systems.systemid = timesheet_systems_users.systemid
		where timesheet_systems.systemid = #sid#
		order by timesheet_systems.systemName
	</cfquery>
	<cfset systemname = getsys.systemname>
	<cfset assignedList = valuelist(getsys.adminuserid)>
	<cfif assignedList is ""><cfset assignedList = 0></cfif>
</cfif>

<cfif isDefined("form.btnEditsystem")>

	<cftransaction>
		<cfquery name="editsystem" datasource="#ds#">
			begin
				update timesheet_systems
				set systemname = '#form.systemname#'
				where systemid = #sid#
			end
			begin
				delete from timesheet_systems_users
				where systemid = #sid#
			end
			<cfloop list="#form.assignedList#" index="uid">
				<cfif uid is not 0>
					begin
						if not exists (select * from timesheet_systems_users where systemid = #sid# and adminuserid = #uid#)
						begin
							insert into timesheet_systems_users (systemid, adminuserid)
							values (#sid#, #uid#)
						end
					end
				</cfif>
			</cfloop>
		</cfquery>
	</cftransaction>

	<cflocation url="index.cfm">

	<cfabort>
</cfif>

<cfif isDefined("form.btnAddUser")>

	<cfif form.adminuserid gt 0>
		<cfif assignedList is -1><cfset assignedList = 0></cfif>
		<cfif listfind(assignedList, form.adminuserid) is 0>
			<cfset assignedList = listappend(assignedList, form.adminuserid)>
		</cfif>
	<cfelse>
		<cfif form.adminuserid is -1>
			<cfset assignedList = -1>
		</cfif>
	</cfif>

</cfif>

<cfif isDefined("form.btnRemoveUser")>

	<cfif isdefined("form.removeadminuserid")>
		<cfloop list="#form.removeadminuserid#" index="uid">
		
			<cfset index = listfind(assignedLIst, uid)>
			<cfset assignedList = listdeleteat(assignedList, index)>
			
		</cfloop>
	
		<cfif assignedList is ""><cfset assignedList = 0></cfif>
	</cfif>

</cfif>

<cfquery name="getUsers" datasource="#ds#">
	select adminuserid, firstname, lastname
	from admin_users
	where defaultdeptid = 1 and active = 1
	order by firstname, lastname
</cfquery>

<cfquery name="getAssignedUsers" datasource="#ds#">
	select adminuserid, firstname, lastname
	from admin_users
	where adminuserid IN (#assignedList#)
	order by firstname, lastname
</cfquery>

<div align="center">
<table border="0" cellpadding="5" cellspacing="0" class="grey">
<tr>
	<td class="highlightbar"><b>Edit system</b></td>
</tr>
<tr>
	<td class="greyrowbottom" style="padding:0px">
		<table width="100%" border="0" cellspacing="0" cellpadding="5" class="grey">
			<form method="post" action="editSystem.cfm">
			<cfoutput>
			<input type="hidden" name="sid" value="#sid#" />
			<input type="hidden" name="assignedList" value="#assignedList#" />
			<tr>
				<td width="18%" nowrap>system Name:</td>
				<td width="82%">
					<input name="systemName" value="#systemName#" type="text" style="width:170px;" maxlength="30">
				</td>
			</tr>
			</cfoutput>
			<tr>
				<td nowrap>Assigned To: </td>
				<td class="nopadding">
					<table border="0" cellpadding="5" cellspacing="0" class="grey">
						<tr>
							<td>
								<select name="adminuserid" class="normal">
									<option value="0"></option>
									<option value="-1">Everyone</option>
									<cfoutput query="getUsers">
										<option value="#adminuserid#">#firstname# #lastname#</option>
									</cfoutput>
								</select>
								</td>
							<td>
								<input name="btnAddUser" type="submit" class="sidebarsmall" value="Add" />
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td nowrap>&nbsp;</td>
				<td class="nopadding">
					<table width="100%" border="0" cellpadding="5" cellspacing="0" class="grey">
						<cfif assignedList is -1>
							<tr>
								<td width="8%">
									<input type="checkbox" name="removeAdminUserID" value="-1" />
								</td>
								<td width="92%">Everyone</td>
							</tr>
						<cfelse>
							<cfif getassignedusers.recordcount is 0>
								<tr>
									<td>This system is currently unassigned</td>
								</tr>
							<cfelse>
								<cfoutput query="getAssignedUsers">
								<tr>
									<td width="8%">
										<input type="checkbox" name="removeAdminUserID" value="#adminuserid#" />
									</td>
									<td width="92%">#firstname# #lastname#</td>
								</tr>
								</cfoutput>
							</cfif>
						</cfif>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<input name="btnEditsystem" type="submit" class="sidebar" value="Update system">&nbsp;
					<input type="button" class="sidebar" value="Cancel" onclick="document.location='index.cfm';">&nbsp;
					<cfif assignedList is not 0>
					<input name="btnRemoveUser" type="submit" class="sidebar" value="Remove Selected Users" />
					</cfif>
				</td>
			</tr>
			</form>
		</table>
	</td></tr>
</table>
</div>

