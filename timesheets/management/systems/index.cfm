
<link rel="stylesheet" type="text/css" href="../../../styles.css">

<cfparam name="filter" default="active">

<cfswitch expression="#filter#">
	<cfcase value="active"><cfset active = 1></cfcase>
	<cfcase value="deleted"><cfset active = 0></cfcase>
	<cfcase value="all"><cfset active = -1></cfcase>
</cfswitch>

<cfquery name="getsys" datasource="#ds#">
	select timesheet_systems.*, timesheet_systems_users.adminuserid, admin_users.firstname, admin_users.lastname
	from timesheet_systems
	left join timesheet_systems_users on timesheet_systems.systemid = timesheet_systems_users.systemid
	left join admin_users on timesheet_systems_users.adminuserid = admin_users.adminuserid
	<cfif active is not -1>
		where timesheet_systems.active = #active#
	</cfif>
	order by timesheet_systems.systemName, admin_users.lastname, admin_users.firstname
</cfquery>

<div align="center">
<table width="900" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>System Managment</b></td>
	</tr>
	<tr>
		<td class="greyrowbottom">
			<table width="100%" border="0" cellpadding="5" cellspacing="0" class="grey">
				<tr>
					<td colspan="3" class="nopadding">
						<table border="0" cellpadding="5" cellspacing="0" class="grey">
							<tr>
								<td nowrap="nowrap"><b>Show only:</b></td>
								<td>
									<form method="post" action="index.cfm">
										<select name="filter" class="normal" onchange="this.form.submit();">
											<option <cfif filter is "all">selected</cfif> value="all">All systems</option>
											<option <cfif filter is "active">selected</cfif> value="active">Active systems</option>
											<option <cfif filter is "deleted">selected</cfif> value="deleted">Deleted systems</option>
										</select>
									</form>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td width="8%" align="center"><b>Action</b></td>
					<td width="41%"><b>System</b></td>
					<td width="51%"><b>Assigned To</b></td>
				</tr>
				<cfoutput query="getsys" group="systemid">
				<tr>
					<td align="center" nowrap="nowrap" class="linedrow">
						<cfif getsys.active is 1>
							<a href="editsystem.cfm?sid=#systemid#"><img src="../../../images/edit.gif" alt="Edit" width="16" height="16" border="0" /></a>&nbsp;
							<a href="javascript: if (confirm('Are you sure you wish to delete this item?')) {document.location='delsystem.cfm?sid=#systemid#';}"><img src="../../../images/delete.gif" alt="Delete" width="16" height="16" border="0" /></a>
						<cfelse>
							<input type="button" value="Reactivate" class="sidebarsmall" onclick="if (confirm('Are you sure you wish to reactivate this system?')) document.location = 'reactivate.cfm?sid=#systemid#';" />
						</cfif>
					</td>
					<td class="linedrow">#systemName#</td>
					<td class="linedrow">
					<cfset assignedList = "">
					<cfset idlist = "">
					<cfoutput>
						<cfset assignedList = listappend(assignedList, "#firstname# #lastname#")>
						<cfset idlist = listappend(idlist, adminuserid)>
					</cfoutput>
					<cfif idlist is "-1">
						Everyone
					<cfelse>
						#replace(assignedList, ",", ", ", "all")#&nbsp;
					</cfif>
					</td>
				</tr>
				</cfoutput>
				<form method="post" action="">
				<tr>
					<td align="center" nowrap="nowrap">
						<input name="Submit" type="button" onClick="document.location='addsystem.cfm';" class="sidebar" value="Add New">
					</td>
					<td colspan="2">&nbsp;</td>
				</tr>
				</form>
			</table>
		</td>
	</tr>
</table>

<br />
<a class="normal" href="/index.cfm" style="text-decoration:underline">Return to Intranet Menu</a>

</div>
