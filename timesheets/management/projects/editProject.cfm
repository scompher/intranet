<link rel="stylesheet" type="text/css" href="../../../styles.css">

<cfif not isdefined("assignedList")>
	<cfquery name="getproj" datasource="#ds#">
		select timesheet_projects.*, timesheet_projects_users.adminuserid
		from timesheet_projects
		left join timesheet_projects_users on timesheet_projects.projectid = timesheet_projects_users.projectid
		where timesheet_projects.projectid = #pid#
		order by timesheet_projects.projectName
	</cfquery>
	<cfset projectname = getproj.projectname>
	<cfset assignedList = valuelist(getproj.adminuserid)>
	<cfif assignedList is ""><cfset assignedList = 0></cfif>
</cfif>

<cfif isDefined("form.btnEditProject")>

	<cftransaction>
		<cfquery name="editProject" datasource="#ds#">
			begin
				update timesheet_projects
				set projectname = '#form.projectname#'
				where projectid = #pid#
			end
			begin
				delete from timesheet_projects_users
				where projectid = #pid#
			end
			<cfloop list="#form.assignedList#" index="uid">
				<cfif uid is not 0>
					begin
						if not exists (select * from timesheet_projects_users where projectid = #pid# and adminuserid = #uid#)
						begin
							insert into timesheet_projects_users (projectid, adminuserid)
							values (#pid#, #uid#)
						end
					end
				</cfif>
			</cfloop>
		</cfquery>
	</cftransaction>

	<cflocation url="projects.cfm">

	<cfabort>
</cfif>

<cfif isDefined("form.btnAddUser")>

	<cfif form.adminuserid gt 0>
		<cfif assignedList is -1><cfset assignedList = 0></cfif>
		<cfif listfind(assignedList, form.adminuserid) is 0>
			<cfset assignedList = listappend(assignedList, form.adminuserid)>
		</cfif>
	<cfelse>
		<cfif form.adminuserid is -1>
			<cfset assignedList = -1>
		</cfif>
	</cfif>

</cfif>

<cfif isDefined("form.btnRemoveUser")>

	<cfif isdefined("form.removeadminuserid")>
		<cfloop list="#form.removeadminuserid#" index="uid">
		
			<cfset index = listfind(assignedLIst, uid)>
			<cfset assignedList = listdeleteat(assignedList, index)>
			
		</cfloop>
	
		<cfif assignedList is ""><cfset assignedList = 0></cfif>
	</cfif>

</cfif>

<cfquery name="getUsers" datasource="#ds#">
	select adminuserid, firstname, lastname
	from admin_users
	where defaultdeptid = 1 and active = 1
	order by firstname, lastname
</cfquery>

<cfquery name="getAssignedUsers" datasource="#ds#">
	select adminuserid, firstname, lastname
	from admin_users
	where adminuserid IN (#assignedList#)
	order by firstname, lastname
</cfquery>

<div align="center">
<table border="0" cellpadding="5" cellspacing="0" class="grey">
<tr>
	<td class="highlightbar"><b>Edit Project</b></td>
</tr>
<tr>
	<td class="greyrowbottom" style="padding:0px">
		<table width="100%" border="0" cellspacing="0" cellpadding="5" class="grey">
			<form method="post" action="editProject.cfm">
			<cfoutput>
			<input type="hidden" name="pid" value="#pid#" />
			<input type="hidden" name="assignedList" value="#assignedList#" />
			<tr>
				<td width="18%" nowrap>Project Name:</td>
				<td width="82%">
					<input name="projectName" value="#projectName#" type="text" style="width:170px;" maxlength="30">
				</td>
			</tr>
			</cfoutput>
			<tr>
				<td nowrap>Assigned To: </td>
				<td class="nopadding">
					<table border="0" cellpadding="5" cellspacing="0" class="grey">
						<tr>
							<td>
								<select name="adminuserid" class="normal">
									<option value="0"></option>
									<option value="-1">Everyone</option>
									<cfoutput query="getUsers">
										<option value="#adminuserid#">#firstname# #lastname#</option>
									</cfoutput>
								</select>
								</td>
							<td>
								<input name="btnAddUser" type="submit" class="sidebarsmall" value="Add" />
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td nowrap>&nbsp;</td>
				<td class="nopadding">
					<table width="100%" border="0" cellpadding="5" cellspacing="0" class="grey">
						<cfif assignedList is -1>
							<tr>
								<td width="8%">
									<input type="checkbox" name="removeAdminUserID" value="-1" />
								</td>
								<td width="92%">Everyone</td>
							</tr>
						<cfelse>
							<cfif getassignedusers.recordcount is 0>
								<tr>
									<td>This project is currently unassigned</td>
								</tr>
							<cfelse>
								<cfoutput query="getAssignedUsers">
								<tr>
									<td width="8%">
										<input type="checkbox" name="removeAdminUserID" value="#adminuserid#" />
									</td>
									<td width="92%">#firstname# #lastname#</td>
								</tr>
								</cfoutput>
							</cfif>
						</cfif>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<input name="btnEditProject" type="submit" class="sidebar" value="Update Project">&nbsp;
					<input type="button" class="sidebar" value="Cancel" onclick="document.location='projects.cfm';">&nbsp;
					<cfif assignedList is not 0>
					<input name="btnRemoveUser" type="submit" class="sidebar" value="Remove Selected Users" />
					</cfif>
				</td>
			</tr>
			</form>
		</table>
	</td></tr>
</table>
</div>

