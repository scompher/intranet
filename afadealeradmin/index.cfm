
<cfparam name="form.newDealerNumber" default="">
<cfparam name="form.newemail" default="">
<cfparam name="form.reportid" default="0">
<cfparam name="form.action" default="">
<cfparam name="form.actionID" default="0">

<cfif isDefined("url.action")><cfset form.action = url.action></cfif>
<cfif isDefined("url.reportid")><cfset form.reportid = url.reportid></cfif>
<cfif isDefined("url.actionID")><cfset form.actionID = url.actionID></cfif>

<link rel="stylesheet" type="text/css" href="../styles.css">
<div align="center">

<cfinclude template="actions.cfm">

<form method="post" action="index.cfm">

<table width="600" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>AFA Dealer Maintenance</b></td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td align="center" nowrap="nowrap" class="linedrowrightcolumn">Dealer #:</td>
					<td class="linedrow" colspan="2">
						<input type="text" name="newDealerNumber" maxlength="5" style="width:50px; vertical-align:middle" value="" />
					</td>
				</tr>
				<tr>
					<td align="center" nowrap="nowrap" class="linedrowrightcolumn">Report Email:</td>
					<td class="linedrow" colspan="2">
						<input type="text" name="newEmail" maxlength="255" style="width:300px; vertical-align:middle" value="" />
					</td>
				</tr>
				<tr>
					<td class="linedrowrightcolumn">&nbsp;</td>
					<td class="linedrow" colspan="2">
						<input type="submit" name="btnAddNewDealer" style="vertical-align:top;" value="Add New Dealer" class="sidebar" />
					</td>
				</tr>
				<tr>
					<td class="linedrow" colspan="3">&nbsp;</td>
				</tr>
				<tr>
					<td width="9%" align="center" class="linedrowrightcolumn"><b>Action</b></td>
					<td width="20%" align="center" class="linedrowrightcolumn"><b>Dealer #</b></td>
					<td class="linedrow"><b>E-Mail Address </b></td>
				</tr>
				<cfoutput query="getDealers">
				<cfif form.action is "EDIT" and form.actionID is getDealers.afaid>
					<input type="hidden" name="actionID" value="#form.actionID#" />
					<tr>
						<td align="center" class="linedrowrightcolumn">Editing</td>
						<td class="linedrow"><input type="text" name="dealerNumber" value="#getDealers.dealerNumber#" style="width:50px;" maxlength="5" /></td>
						<td class="linedrow"><input type="text" name="sendToEmail" value="#getDealers.sendToEmail#" style="width:300px;" maxlength="255" /></td>
					</tr>
					<tr>
						<td align="center" class="linedrowrightcolumn">&nbsp;</td>
						<td colspan="2" class="linedrow">
						<input type="submit" name="BtnUpdateDealer" value="Update" class="sidebar" />
						<input type="button" value="Cancel" onclick="document.location='index.cfm';" class="sidebar" />
						</td>
					</tr>
				<cfelse>
					<tr>
						<td align="center" class="linedrowrightcolumn">
						<a href="index.cfm?action=EDIT&actionID=#getDealers.afaID#">[E]</a>
						<a onclick="return confirm('Are you sure you wish to remove this Dealer?');" href="index.cfm?action=DELETE&actionID=#getDealers.afaID#">[D]</a>
						</td>
						<td class="linedrow">#dealerNumber#</td>
						<td class="linedrow">#sendToEmail#</td>
					</tr>
				</cfif>
				</cfoutput>
			</table>
		</td>
	</tr>
</table>

</form>

<br />
<a class="normal" style="text-decoration:underline;" href="/index.cfm">Return to Intranet</a>

</div>

