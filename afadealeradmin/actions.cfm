
<cfset ds = "copalink">

<cfif isDefined("form.btnAddNewDealer")>
	<cfquery name="addDealer" datasource="#ds#">
		if not exists (select * from afaDealers where dealerNumber = '#form.newDealerNumber#')
		begin
			insert into afaDealers (dealerNumber, sendToEmail) 
			values ('#trim(form.newDealerNumber)#', '#trim(newEmail)#')
		end
	</cfquery>
	<cflocation url="index.cfm">
</cfif>

<cfif isDefined("form.btnUpdateDealer")>
	<cfquery name="updateDealer" datasource="#ds#">
		update afaDealers 
		set dealerNumber = '#trim(form.dealerNumber)#', sendToEmail = '#trim(form.sendToEmail)#'
		where afaid = #form.actionid# 
	</cfquery>
	<cflocation url="index.cfm">
</cfif>

<cfif form.action is "DELETE">
	<cfquery name="delDealer" datasource="#ds#">
		delete from afaDealers 
		where afaid = #form.actionid# 
	</cfquery>
	<cflocation url="index.cfm">
</cfif>

<cfquery name="getDealers" datasource="#ds#">
	select * 
	from afaDealers 
	order by dealerNumber asc 
</cfquery>

