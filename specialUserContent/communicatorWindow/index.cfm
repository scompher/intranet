<cfparam name="url.rr" default="0">

<title>The Communicator</title>

<cfif url.rr gt 0>
	<meta http-equiv="refresh" content="<cfoutput>#url.rr#</cfoutput>;url=index.cfm?rr=<cfoutput>#url.rr#</cfoutput>" />
</cfif>
<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td>
		Refresh every 
		<cfif url.rr is 5>
			5 | 
		<cfelse>
			<a style="text-decoration:underline;" href="index.cfm?rr=5">5</a> | 
		</cfif>
		<cfif url.rr is 10>
			10 | 
		<cfelse>
			<a style="text-decoration:underline;" href="index.cfm?rr=10">10</a> | 
		</cfif>
		<cfif url.rr is 15>
			15 | 
		<cfelse>
			<a style="text-decoration:underline;" href="index.cfm?rr=15">15</a> | 
		</cfif>
		<cfif url.rr is 30>
			30 | 
		<cfelse>
			<a style="text-decoration:underline;" href="index.cfm?rr=30">30</a> | 
		</cfif>
		Seconds &nbsp;
		<cfif url.rr is 0>
			[Refresh OFF]
		<cfelse>
			<a style="text-decoration:underline;" href="index.cfm?rr=0">[Refresh Off]</a>
		</cfif>
		<a style="text-decoration:underline;" href="index.cfm?rr=<cfoutput>#url.rr#</cfoutput>">[Refresh Now]</a>
		</td>
	</tr>
</table>
<title>Communicator Window</title>
<cfinclude template="/dashboards/included/dashboardCommunicator.cfm">