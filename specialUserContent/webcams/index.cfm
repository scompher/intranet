
<script>
function showCamWin(winName,pageTitle,camURL) {
	window.open("/specialUserContent/webcams/showCam.cfm?t=" + pageTitle + "&u=" + camURL,winName,"width=320,height=240,scrollbars=0,status=0,location=0,resizable=0");
}
</script>

<cfif isDefined("url.hideCameras")>
	<cfcookie name="hideCameras" value="true" expires="never">
</cfif>

<cfif isDefined("url.showCameras")>
	<cfcookie name="hideCameras" expires="now">
	<meta http-equiv="refresh" content="0;url=/index.cfm" />
</cfif>

<cfif not isDefined("cookie.hideCameras")>
	<cfset showTolist = "39,62,70,135">
	<cfif (getsec.defaultdeptid is 1) or listfind(showToList,cookie.adminlogin) is not 0>
	<table border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td class="nopadding">
			<a href="/index.cfm?hideCameras=true">[Hide Cameras]</a><br /><br />
			</td>
		</tr>
		<tr>
			<td class="nopadding">
			<cfset camURL = "http://192.168.3.157:8098">
			<table border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td class="highlightbar"><b>Kitchen Cam</b>&nbsp;&nbsp;&nbsp;<a href="javascript:showCamWin('kitchenCam','Kitchen Cam','<cfoutput>#camURL#</cfoutput>');">[Click Pop Out Cam]</a></td>
				</tr>
				<tr>
					<td class="greyrowbottomnopadding">
					<table width="100%" border="0" cellspacing="0" cellpadding="5">
						<tr>
							<td>
							<object border="1" type="application/x-shockwave-flash" width="320" height="240" data="<cfoutput>#camURL#</cfoutput>/flashMJPEG.swf">
							<param name="movie" value="<cfoutput>#camURL#</cfoutput>/flashMJPEG.swf?webcam=<cfoutput>#camURL#</cfoutput>/cam_1.jpg&amp;refresh=50&amp;connect=&amp;offline=&amp;transtype=Fade&amp;bgcolor=#FFFFFF&amp;txtcolor=#808080" />
							<param name="FlashVars" value="webcam=<cfoutput>#camURL#</cfoutput>/cam_1.jpg&amp;refresh=50&amp;connect=&amp;offline=&amp;transtype=Fade&amp;bgcolor=#FFFFFF&amp;txtcolor=#808080" />
							<param name="loop" value="false" />
							<param name="menu" value="false" />
							<param name="quality" value="best" />
							<param name="scale" value="noscale" />
							<param name="salign" value="lt" />
							<param name="wmode" value="opaque" />
							</object>
							</td>
						</tr>
					</table>
					</td>
				</tr>
			</table>
			</td>
			<!--- 
			<td>&nbsp;</td>
			<td class="nopadding">
			<cfset camURL = "http://192.168.3.38:8080">
			<table border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td class="highlightbar"><b>Surfer Cam</b>&nbsp;&nbsp;&nbsp;<a href="javascript:showCamWin('billCam','Surfer Cam','<cfoutput>#camURL#</cfoutput>');">[Click Pop Out Cam]</a></td>
				</tr>
				<tr>
					<td class="greyrowbottomnopadding">
					<table width="100%" border="0" cellspacing="0" cellpadding="5">
						<tr>
							<td>
							<object border="1" type="application/x-shockwave-flash" width="320" height="240" data="<cfoutput>#camURL#</cfoutput>/flashMJPEG.swf">
							<param name="movie" value="<cfoutput>#camURL#</cfoutput>/flashMJPEG.swf?webcam=<cfoutput>#camURL#</cfoutput>/cam_1.jpg&amp;refresh=50&amp;connect=&amp;offline=&amp;transtype=Fade&amp;bgcolor=#FFFFFF&amp;txtcolor=#808080" />
							<param name="FlashVars" value="webcam=<cfoutput>#camURL#</cfoutput>/cam_1.jpg&amp;refresh=50&amp;connect=&amp;offline=&amp;transtype=Fade&amp;bgcolor=#FFFFFF&amp;txtcolor=#808080" />
							<param name="loop" value="false" />
							<param name="menu" value="false" />
							<param name="quality" value="best" />
							<param name="scale" value="noscale" />
							<param name="salign" value="lt" />
							<param name="wmode" value="opaque" />
							</object>
							</td>
						</tr>
					</table>
					</td>
				</tr>
			</table>
			</td>
			--->
		</tr>
		<tr><td>&nbsp;</td></tr>
		<tr>
			<!--- 
			<td class="nopadding">
			<cfset camURL = "http://192.168.3.22:8080">
			<table border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td class="highlightbar"><b>John Cam</b>&nbsp;&nbsp;&nbsp;<a href="javascript:showCamWin('johnCam','John Cam','<cfoutput>#camURL#</cfoutput>');">[Click Pop Out Cam]</a></td>
				</tr>
				<tr>
					<td class="greyrowbottomnopadding">
					<table width="100%" border="0" cellspacing="0" cellpadding="5">
						<tr>
							<td>
							<object border="1" type="application/x-shockwave-flash" width="320" height="240" data="<cfoutput>#camURL#</cfoutput>/flashMJPEG.swf">
							<param name="movie" value="<cfoutput>#camURL#</cfoutput>/flashMJPEG.swf?webcam=<cfoutput>#camURL#</cfoutput>/cam_1.jpg&amp;refresh=50&amp;connect=&amp;offline=&amp;transtype=Fade&amp;bgcolor=#FFFFFF&amp;txtcolor=#808080" />
							<param name="FlashVars" value="webcam=<cfoutput>#camURL#</cfoutput>/cam_1.jpg&amp;refresh=50&amp;connect=&amp;offline=&amp;transtype=Fade&amp;bgcolor=#FFFFFF&amp;txtcolor=#808080" />
							<param name="loop" value="false" />
							<param name="menu" value="false" />
							<param name="quality" value="best" />
							<param name="scale" value="noscale" />
							<param name="salign" value="lt" />
							<param name="wmode" value="opaque" />
							</object>
							</td>
						</tr>
					</table>
					</td>
				</tr>
			</table>
			</td>
			<td>&nbsp;</td>
			--->
			<!--- 
			<td class="nopadding">
			<cfset camURL = "http://192.168.3.21:8080">
			<table border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td class="highlightbar"><b>Darren Cam</b>&nbsp;&nbsp;&nbsp;<a href="javascript:showCamWin('darrenCam','Darren Cam','<cfoutput>#camURL#</cfoutput>');">[Click Pop Out Cam]</a></td>
				</tr>
				<tr>
					<td class="greyrowbottomnopadding">
					<table width="100%" border="0" cellspacing="0" cellpadding="5">
						<tr>
							<td>
							<object border="1" type="application/x-shockwave-flash" width="320" height="240" data="<cfoutput>#camURL#</cfoutput>/flashMJPEG.swf">
							<param name="movie" value="<cfoutput>#camURL#</cfoutput>/flashMJPEG.swf?webcam=<cfoutput>#camURL#</cfoutput>/cam_1.jpg&amp;refresh=50&amp;connect=&amp;offline=&amp;transtype=Fade&amp;bgcolor=#FFFFFF&amp;txtcolor=#808080" />
							<param name="FlashVars" value="webcam=<cfoutput>#camURL#</cfoutput>/cam_1.jpg&amp;refresh=50&amp;connect=&amp;offline=&amp;transtype=Fade&amp;bgcolor=#FFFFFF&amp;txtcolor=#808080" />
							<param name="loop" value="false" />
							<param name="menu" value="false" />
							<param name="quality" value="best" />
							<param name="scale" value="noscale" />
							<param name="salign" value="lt" />
							<param name="wmode" value="opaque" />
							</object>
							</td>
						</tr>
					</table>
					</td>
				</tr>
			</table>
			</td>
			--->
		</tr>
	</table>
	<br />
	</cfif>
<cfelse>
	<table border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td class="nopadding">
			<a href="index.cfm?showCameras=true">[Show Cameras]</a>
			</td>
		</tr>
	</table>
</cfif>
