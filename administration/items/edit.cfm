<link rel="stylesheet" type="text/css" href="../../styles.css">

<cfif isDefined("form.btnEditItem")>

	<cfquery name="UpdateItem" datasource="#ds#">
		update Admin_Security_Items 
		set itemName = '#form.itemName#', sectionID = #form.sectionID#, itemURL = '#form.itemURL#'
		where itemid = #id#
	</cfquery>

	<cflocation url="index.cfm">

<cfelse>

	<cfquery name="getSections" datasource="#ds#">
		select * from Admin_Security_Sections
		order by sectionName asc
	</cfquery>

	<cfquery name="getItem" datasource="#ds#">
		select * from Admin_Security_Items
		where itemid = #id#
	</cfquery>

	<div align="center">
	<table border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td class="highlightbar"><b>Edit an Item</b></td>
		</tr>
		<tr>
			<td class="greyrowbottom" style="padding:0px;">
				<table border="0" cellpadding="5" cellspacing="0" class="grey">
					<form method="post" action="edit.cfm">
					<cfoutput>
					<input type="hidden" name="id" value="#id#" />
					</cfoutput>
					<tr>
						<td nowrap>Section:</td>
						<td>
							<select name="sectionID">
								<option value="0"></option>
								<cfoutput query="getSections">
									<option <cfif getsections.sectionid is getitem.sectionid>selected</cfif> value="#sectionid#">#sectionName#</option>
								</cfoutput>
							</select>
						</td>
					</tr>
					<cfoutput query="getItem">
					<tr>
						<td nowrap>Item Name: </td>
						<td>
							<input type="text" style="width:400px" name="itemName" value="#itemName#">
						</td>
					</tr>
					<tr>
						<td nowrap>Item Location: </td>
						<td>
							<input type="text" style="width:400px" name="itemURL" value="#itemURL#">
						</td>
					</tr>
					</cfoutput>
					<tr>
						<td colspan="2">
							<input name="btnEditItem" type="submit" class="sidebar" value="Edit Item">
						</td>
					</tr>
					</form>
				</table>
			</td>
		</tr>
	</table>
	</div>

</cfif>

