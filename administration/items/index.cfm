<link rel="stylesheet" type="text/css" href="../../styles.css">

<cfparam name="form.sectionID" default="-1">

<cfquery name="getSections" datasource="#ds#">
	select * from Admin_Security_Sections
	order by sectionName asc
</cfquery>

<cfquery name="getItems" datasource="#ds#">
	select * from Admin_Security_Items
	where sectionid = #form.sectionID#
	order by itemName asc
</cfquery>

<div align="center">
<table width="700" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Intranet Section Administration</b></td>
	</tr>
	<tr>
		<td class="greyrowbottom" style="padding:0px;">
			<table width="100%" border="0" cellpadding="5" cellspacing="0" class="grey">
				<tr>
					<td colspan="3" align="center"><b>Choose a section to list the items under it</b></td>
				</tr>
				<form method="post" action="index.cfm">
				<tr>
					<td colspan="3" style="padding:0px">
						<table border="0" cellspacing="0" cellpadding="5" class="grey">
							<tr>
								<td>Choose Section: </td>
								<td>
									<select name="sectionID">
										<option value="-1"></option>
										<cfoutput query="getSections">
											<option <cfif getSections.sectionid IS form.sectioNID>selected</cfif> value="#sectionid#">#sectionName#</option>
										</cfoutput>
									</select>
								</td>
								<td>
									<input name="Submit" type="submit" class="sidebar" value="Select">
								</td>
							</tr>
						</table>
					</td>
				</tr>
				</form>
				<tr>
					<td width="9%" align="center"><b>Action</b></td>
					<td width="39%"><b>Section</b></td>
					<td width="52%"><b>Location</b></td>
				</tr>
				<cfoutput query="getItems">
				<tr>
					<td align="center">
					<a href="edit.cfm?id=#itemid#"><img border="0" src="../../images/edit.gif" alt="Edit" width="16" height="16"></a>&nbsp;
					<a href="javascript: if (confirm('Are you sure you wish to delete this item?')) {document.location='delete.cfm?id=#itemid#';}"><img border="0" src="../../images/delete.gif" alt="Delete" width="16" height="16"></a>
					</td>
					<td nowrap>#itemName#</td>
					<td nowrap>#itemURL#</td>
				</tr>
				</cfoutput>
				<form method="post" action="add.cfm">
				<tr>
					<td colspan="3">
					<input type="submit" value="Add Item" class="sidebar" />
					</td>
				</tr>
				</form>
			</table>
		</td>
	</tr>
</table>
<br />
<a href="/index.cfm" class="normal" style="text-decoration:underline;">Return to Intranet Menu</a>
</div>
