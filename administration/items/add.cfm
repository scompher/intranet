<link rel="stylesheet" type="text/css" href="../../styles.css">

<cfif isDefined("form.btnAddItem")>

	<cfquery name="AddItem" datasource="#ds#">
		insert into Admin_Security_Items (itemName, moduleID, sectionID, itemURL)
		values ('#form.itemName#', 0, #sectionID#, '#form.itemURL#')
	</cfquery>

	<cflocation url="index.cfm">

<cfelse>

	<cfquery name="getSections" datasource="#ds#">
		select * from Admin_Security_Sections
		order by sectionName asc
	</cfquery>

	<div align="center">
	<table border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td class="highlightbar"><b>Add an Item</b></td>
		</tr>
		<tr>
			<td class="greyrowbottom" style="padding:0px;">
				<table border="0" cellpadding="5" cellspacing="0" class="grey">
					<form method="post" action="add.cfm">
					<tr>
						<td nowrap>Section:</td>
						<td>
							<select name="sectionID">
								<option value="0"></option>
								<cfoutput query="getSections">
									<option value="#sectionid#">#sectionName#</option>
								</cfoutput>
							</select>
						</td>
					</tr>
					<tr>
						<td nowrap>Item Name: </td>
						<td>
							<input type="text" style="width:400px" name="itemName">
						</td>
					</tr>
					<tr>
						<td nowrap>Item Location: </td>
						<td>
							<input type="text" style="width:400px" name="itemURL">
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<input name="btnAddItem" type="submit" class="sidebar" value="Add Item">
						</td>
					</tr>
					</form>
				</table>
			</td>
		</tr>
	</table>
	</div>

</cfif>
