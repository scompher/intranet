
<link rel="stylesheet" type="text/css" href="../../styles.css">

<cfif isDefined("form.btnEditSection")>

	<cfquery name="Editcat" datasource="#ds#">
		update Admin_Security_Sections 
		set sectionName = '#form.sectionName#'
		where sectionid = #sid#
	</cfquery>
	
	<cflocation url="index.cfm">

<cfelse>

	<cfquery name="getSection" datasource="#ds#">
		select * from Admin_Security_Sections
		where sectionid = #sid#
	</cfquery>

	<div align="center">
	<table border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td class="highlightbar"><b>Edit a Section</b></td>
		</tr>
		<tr>
			<td class="greyrowbottom" style="padding:0px;">
				<table border="0" cellpadding="5" cellspacing="0" class="grey">
					<cfoutput query="getsection">
					<form method="post" action="edit.cfm">
					<input type="hidden" name="sid" value="#sid#">
					<tr>
						<td nowrap>Section Name:</td>
						<td>
							<input type="text" name="sectionName" style="width:250px;" maxlength="100" value="#getsection.sectionName#">
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<input name="btnEditSection" type="submit" class="sidebar" value="Update Section">
						</td>
					</tr>
					</form>
					</cfoutput>
				</table>
			</td>
		</tr>
	</table>
	</div>

</cfif>
