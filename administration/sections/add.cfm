
<link rel="stylesheet" type="text/css" href="../../styles.css">

<cfif isDefined("form.btnAddSection")>

	<cfquery name="addcat" datasource="#ds#">
		declare @sectionID as int
		
		set @sectionID = (select max(sectionid) from Admin_Security_Sections)
		set @sectionID = @sectionid + 1
		
		insert into Admin_Security_Sections (sectionID, sectionName)
		values (@sectionID, '#form.sectionName#')
	</cfquery>
	
	<cflocation url="index.cfm">

<cfelse>

	<div align="center">
	<table border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td class="highlightbar"><b>Add a Section</b></td>
		</tr>
		<tr>
			<td class="greyrowbottom" style="padding:0px;">
				<table border="0" cellpadding="5" cellspacing="0" class="grey">
					<form method="post" action="add.cfm">
					<tr>
						<td nowrap>Section Name:</td>
						<td>
							<input type="text" name="sectionName" style="width:250px;" maxlength="100">
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<input name="btnAddSection" type="submit" class="sidebar" value="Add Section">
						</td>
					</tr>
					</form>
				</table>
			</td>
		</tr>
	</table>
	</div>

</cfif>
