
<link rel="stylesheet" type="text/css" href="../styles.css">

<cfquery name="getDetails" datasource="nCall">
	SELECT
	CLIENTS.CLIENT_ID,
	CLIENTS.CO_NAME,
	CLIENTS.CO_ALIAS AS AKA,
	CLIENTS.ADDRESS1,
	CLIENTS.ADDRESS2,
	CLIENTS.ADDRESS3 AS CITY,
	CLIENTS.ADDRESS4 AS STATE,
	CLIENTS.POSTCODE AS ZIP_CODE,
	CLIENTS.TEL AS OFFICE_PHONE,
	CLIENTS.MOBILE,
	CLIENTS.FAX AS OFFICE_FAX,
	CLIENTS.EMAIL,
	CLIENTS.WEBSITE,
	CLIENTS.NOTES,
	CLIENTS.GREETING,
	CLIENTS.UD2 AS DEALER_NUM
	FROM CLIENTS
	WHERE CLIENTS.CLIENT_STATUS_ID=1 AND CLIENTS.CLIENT_ID = #id#
</cfquery>

<div align="center">
<cfoutput query="getDetails">
<table width="600" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td width="101" valign="top" nowrap><b>Company Name: </b></td>
		<td width="479">#co_name#</td>
	</tr>
	<tr>
		<td valign="top" nowrap><b>Company Alias: </b></td>
		<td>#aka#</td>
	</tr>
	<tr>
		<td valign="top" nowrap><b>Address:</b></td>
		<td>
		#address1#<br>
		#address2#<br>
		#city#, #state#  #zip_code#
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap><b>Phone: </b></td>
		<td>#office_phone#</td>
	</tr>
	<tr>
		<td valign="top" nowrap><b>Mobile:</b></td>
		<td>#mobile#</td>
	</tr>
	<tr>
		<td valign="top" nowrap><b>Fax:</b></td>
		<td>#office_fax#</td>
	</tr>
	<tr>
		<td valign="top" nowrap><b>Email:</b></td>
		<td>#email#</td>
	</tr>
	<tr>
		<td valign="top" nowrap><b>Website:</b></td>
		<td>#website#</td>
	</tr>
	<tr>
		<td valign="top" nowrap><b>Notes:</b></td>
		<td>#notes#</td>
	</tr>
	<tr>
		<td valign="top" nowrap><b>Greeting:</b></td>
		<td>#greeting#</td>
	</tr>
	<tr>
		<td valign="top" nowrap>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
</table>
</cfoutput>
</div>

