﻿using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Data.Entity;
using Intranet.Models;
using System.Collections.Generic;

namespace Intranet.Configuration
{
    public class IntranetContext : DbContext
    {

        public IntranetContext() : base("name=intranet")
        {
        }

        public virtual DbSet<ChangeRequest> ChangeRequest { get; set; }
        
        public virtual List<ChangeRequest> ChangeRequestSearch(string parameter)
        {
            var param = parameter != null ?
            new SqlParameter("SearchVal", parameter) :
            new SqlParameter("SearchVal", typeof(string));
            
            var result = ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<ChangeRequest>("csp_ChangeRequestFTS @SearchVal", param);            
            return result.ToList();
        }        

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new ChangeRequestConfiguration());
        }




    }
}