﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Intranet.Models;



namespace Intranet.Configuration
{
    public class ChangeRequestConfiguration : EntityTypeConfiguration<ChangeRequest>
    {


        /* Note: If additional properties are added/removed renamed etc to this mapping, the proc csp_ChangeRequestFTS 
         * should be updated so that the names of the columns returned match the properties defined here.           
         */

        public ChangeRequestConfiguration()
        {
            //Map the table
            this.ToTable("ChangeRequest");

            //PKID property
            this.HasKey(x => x.RequestID);

            //Property name > Column Name + Required + DB Auto Inc
            this.Property(x => x.RequestID).HasColumnName("RequestID").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            //Fields
            this.Property(x => x.RequestorName).HasColumnName("RequestorName").HasColumnType("varchar").HasMaxLength(255);
            this.Property(x => x.RequestorEmail).HasColumnName("RequestorEmail").HasColumnType("varchar").HasMaxLength(255);
            this.Property(x => x.RequestorDepartment).HasColumnName("RequestorDept").HasColumnType("varchar").HasMaxLength(255);            
            this.Property(x => x.RequestDate).IsOptional().HasColumnName("RequestDate").HasColumnType("DateTime");

            this.Property(x => x.Manager).HasColumnName("Manager").HasColumnType("varchar").HasMaxLength(255);
            this.Property(x => x.StatusDate).IsOptional().HasColumnName("StatusDate").HasColumnType("DateTime");
            this.Property(x => x.ChangeType).IsOptional().HasColumnName("ChangeType").HasColumnType("varchar");
            this.Property(x => x.Status).IsOptional().HasColumnName("Status").HasColumnType("varchar");

            this.Property(x => x.ChangeDate).IsOptional().HasColumnName("ChangeDate").HasColumnType("DateTime");

            this.Property(x => x.ChangeDescription).HasColumnName("ChangeDescription").HasColumnType("varchar").HasMaxLength(8000);
            this.Property(x => x.Systems).HasColumnName("Systems").HasColumnType("varchar").HasMaxLength(8000);
            this.Property(x => x.ChangeReason).HasColumnName("ChangeReason").HasColumnType("varchar").HasMaxLength(8000);
            this.Property(x => x.ChangeMadeBy).HasColumnName("ChangeMadeBy").HasColumnType("varchar").HasMaxLength(8000);
            this.Property(x => x.TestedBy).HasColumnName("TestedBy").HasColumnType("varchar").HasMaxLength(8000);
            this.Property(x => x.History).HasColumnName("History").HasColumnType("varchar(max)");
            this.Property(x => x.UpdatedBy).HasColumnName("UpdatedBy").HasColumnType("varchar").HasMaxLength(255);
            this.Property(x => x.UpdateDate).HasColumnName("UpdateDate").HasColumnType("DateTime");
        }






    }
}