<link rel="stylesheet" type="text/css" href="/styles.css">

<cfquery name="getAverages" datasource="#ds#">
	select 
	AVG(r_total) as rAvg, 
	AVG(I_Total) as iAvg, 
	AVG(A_Total) as aAvg, 
	AVG(S_Total) as sAvg, 
	AVG(E_Total) as eAvg, 
	AVG(C_Total) as cAvg, 
	AVG(SN_Total) as snAvg, 
	AVG(N_Total) as nAvg, 
	AVG(T_Total) as tAvg, 
	AVG(F_Total) as fAvg, 
	AVG(score) as scoreAvg, 
	AVG(totalScore) as totalScoreAvg, 
	AVG(points) as pointsAvg  
	from profileTest_main
</cfquery>

<div align="center">
<table width="500" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Profile Test Average Scores</b></td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
		<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td width="30%" nowrap="nowrap"><b>Average Points: </b></td>
				<td width="70%"><cfoutput>#getAverages.pointsAvg#</cfoutput></td>
			</tr>
			<tr>
				<td nowrap="nowrap"><b>Average Total Score: </b></td>
				<td><cfoutput>#getAverages.totalScoreAvg#</cfoutput></td>
			</tr>
			<tr>
				<td nowrap="nowrap"><b>Average Score: </b></td>
				<td><cfoutput>#getAverages.scoreAvg#</cfoutput></td>
			</tr>
		</table>
		</td>
	</tr>
</table>
</div>
<br />
<br />
<div align="center">
<cfchart showlegend="no" databackgroundcolor="##F7F7F7" title="Scoring Group Averages" show3d="no"  xaxistitle="Score Group"  yaxistitle="Average Score" format="flash" chartwidth="900" chartheight="500">
	<cfchartseries type="bar" seriescolor="##92A3B6" serieslabel="Scores">
		<cfloop query="getAverages">
			<cfchartdata item="R Score" value="#rAvg#">
			<cfchartdata item="I Score" value="#iAvg#">
			<cfchartdata item="A Score" value="#aAvg#">
			<cfchartdata item="S Score" value="#sAvg#">
			<cfchartdata item="E Score" value="#eAvg#">
			<cfchartdata item="C Score" value="#cAvg#">
			<cfchartdata item="SN Score" value="#snAvg#">
			<cfchartdata item="N Score" value="#nAvg#">
			<cfchartdata item="T Score" value="#tAvg#">
			<cfchartdata item="F Score" value="#fAvg#">
		</cfloop>
	</cfchartseries>
</cfchart>
<br />
<a class="normal" style="text-decoration:underline;" href="/index.cfm">Return to Intranet</a>
</div>

