
<cfparam name="format" default="html">

<cfif tid is 0>
	<cflocation url="lookup.cfm">
</cfif>


<cfinclude template="resultDetail.cfm">
<cfabort>

<cfif format is "pdf">
	<cfdocument scale="88" format="pdf">
	<cfinclude template="resultDetail.cfm">
	</cfdocument>
<cfelse>
	<div align="center" class="normal">
	<a target="_blank" href="printout.cfm?format=pdf&tid=<cfoutput>#tid#</cfoutput>"><img align="absmiddle" src="/images/pdfIconWhite.gif" alt="Print As PDF" border="0" /> &nbsp; <b>View/Print as PDF</b></a>
	<br />
	<br />
	<a style="text-decoration:underline;" href="javascript:history.go(-1);">Return to Search</a>
	<br />
	<br />
	</div>
	<cfinclude template="resultDetail.cfm">
</cfif>

