
<!--- get total --->
<cfquery name="getTotal" datasource="#ds#">
	select sum(answer) as totalScore from profileTest_answers 
	where testid = #tid# 
</cfquery>

<!--- get group totals --->
<cfquery name="getRtotal" datasource="#ds#">
	select SUM(profileTest_answers.answer) as groupTotal 
	from profileTest_answers 
	where 
	questionNumber IN (select distinct questionNumber from profileTest_ScoreGroupLookup where scoreGroup = 'R') and testid = #tid# 
</cfquery>
<cfquery name="getItotal" datasource="#ds#">
	select SUM(profileTest_answers.answer) as groupTotal 
	from profileTest_answers 
	where 
	questionNumber IN (select distinct questionNumber from profileTest_ScoreGroupLookup where scoreGroup = 'I') and testid = #tid# 
</cfquery>
<cfquery name="getAtotal" datasource="#ds#">
	select SUM(profileTest_answers.answer) as groupTotal 
	from profileTest_answers 
	where 
	questionNumber IN (select distinct questionNumber from profileTest_ScoreGroupLookup where scoreGroup = 'A') and testid = #tid# 
</cfquery>
<cfquery name="getStotal" datasource="#ds#">
	select SUM(profileTest_answers.answer) as groupTotal 
	from profileTest_answers 
	where 
	questionNumber IN (select distinct questionNumber from profileTest_ScoreGroupLookup where scoreGroup = 'S') and testid = #tid# 
</cfquery>
<cfquery name="getEtotal" datasource="#ds#">
	select SUM(profileTest_answers.answer) as groupTotal 
	from profileTest_answers 
	where 
	questionNumber IN (select distinct questionNumber from profileTest_ScoreGroupLookup where scoreGroup = 'E') and testid = #tid# 
</cfquery>
<cfquery name="getCtotal" datasource="#ds#">
	select SUM(profileTest_answers.answer) as groupTotal 
	from profileTest_answers 
	where 
	questionNumber IN (select distinct questionNumber from profileTest_ScoreGroupLookup where scoreGroup = 'C') and testid = #tid# 
</cfquery>
<cfquery name="getSNtotal" datasource="#ds#">
	select SUM(profileTest_answers.answer) as groupTotal 
	from profileTest_answers 
	where 
	questionNumber IN (select distinct questionNumber from profileTest_ScoreGroupLookup where scoreGroup = 'SN') and testid = #tid# 
</cfquery>
<cfquery name="getNtotal" datasource="#ds#">
	select SUM(profileTest_answers.answer) as groupTotal 
	from profileTest_answers 
	where 
	questionNumber IN (select distinct questionNumber from profileTest_ScoreGroupLookup where scoreGroup = 'N') and testid = #tid# 
</cfquery>
<cfquery name="getTtotal" datasource="#ds#">
	select SUM(profileTest_answers.answer) as groupTotal 
	from profileTest_answers 
	where 
	questionNumber IN (select distinct questionNumber from profileTest_ScoreGroupLookup where scoreGroup = 'T') and testid = #tid# 
</cfquery>
<cfquery name="getFtotal" datasource="#ds#">
	select SUM(profileTest_answers.answer) as groupTotal 
	from profileTest_answers 
	where 
	questionNumber IN (select distinct questionNumber from profileTest_ScoreGroupLookup where scoreGroup = 'F') and testid = #tid# 
</cfquery>

<!--- get points --->
<cfset matchlist = "Conforming,Persistent,Creative,Methodical,Idealistic,Persuasive">
<cfset points = 0>
<cfloop list="#matchList#" index="word">
	<cfquery name="compareAnswer" datasource="#ds#">
		select distinct profileTest_answers.answer 
		from profileTest_answers
		left join profileTest_questions on profileTest_answers.questionNumber = profileTest_questions.questionNumber 
		where profileTest_questions.question = '#word#' and testid = #tid# 
	</cfquery>
	<cfif compareAnswer.recordcount is 1><cfset points = points + 1></cfif>
</cfloop>

<!--- get top groups --->
<cfquery name="getGroupTotals" datasource="#ds#">
	select sum(profileTest_answers.answer) as groupTotal, profileTest_ScoreGroupLookup.scoreGroup
	from profileTest_answers
	left join profileTest_ScoreGroupLookup on profileTest_answers.questionNumber = profileTest_ScoreGroupLookup.questionNumber
	where profileTest_answers.testID = #tid# and profileTest_ScoreGroupLookup.scoreGroup IN ('R','I','A','S','E','C')
	group by profileTest_ScoreGroupLookup.scoreGroup 
	order by groupTotal desc 
</cfquery>

<cfset topGroups = "">
<cfoutput query="getGroupTotals" maxrows="3">
	<cfset topGroups = listAppend(topGroups, "#getGroupTotals.scoreGroup#")>
</cfoutput>

<cfset first = listgetat(topGroups,1)>
<cfset second = listgetat(topGroups,2)>
<cfset third = listgetat(topGroups,3)>

<!--- calculate final score --->
<cfif topGroups is "C,S,I" or topGroups is "S,C,I">
	<cfset score = 1>
<cfelseif first is "C" and second is "S" and third is not "A">
	<cfset score = 1>
<cfelseif listfindnocase(topGroups,"C") is not 0 and listfindnocase(topGroups,"S") is not 0 and listfindnocase(topGroups,"A") is 0>
	<cfset score = 2>
<cfelseif first is "C" and listfindnocase("R,E,I",second) is not 0 and third is "S">
	<cfset score = 3>
<cfelseif first is "S" and listfindnocase("R,E,I",second) is not 0 and third is "C">
	<cfset score = 4>
<cfelseif topGroups is "I,C,S" or topGroups is "I,S,C">
	<cfset score = 4>
<cfelseif listfindnocase("R,E,I",first) is not 0 and second is "C" and third is "S">
	<cfset score = 5>
<cfelseif listfindnocase(topGroups,"I") is not 0 and listfindnocase(topGroups,"C") is not 0 and listfindnocase(topGroups,"A") is 0>
	<cfset score = 5>
<cfelseif listfindnocase("R,I,E", first) is not 0 and second is "S" and third is "C">
	<cfset score = 6>
<cfelseif first is "C" and listfindnocase("R,I,E",second) is not 0 and listfindnocase("R,I,E",third) is not 0>
	<cfset score = 7>
<cfelseif first is "R" and listfindnocase(topGroups,"A") is 0 and listfindnocase(topGroups,"S") is 0 and listfindnocase(topGroups,"C") is 0>
	<cfset score = 7>
<cfelseif listfindnocase("R,E,I",first) is not 0 and second is "C" and listfindnocase("R,E,I",third) is not 0>
	<cfset score = 8>
<cfelseif listfindnocase("R,E,I",first) is not 0 and listfindnocase("R,E,I",second) is not 0 and third is "C">
	<cfset score = 9>
<cfelse>
	<cfset score = 10>
</cfif>

<!--- save result information --->
<cfquery name="saveResults" datasource="#ds#">
	update profileTest_main 
	set points = #points#, R_total = #getRtotal.groupTotal#, I_total = #getItotal.groupTotal#, A_total = #getAtotal.groupTotal#, S_total = #getStotal.groupTotal#, E_total = #getEtotal.groupTotal#, C_total = #getCtotal.groupTotal#, SN_total = #getSNtotal.groupTotal#, N_total = #getNtotal.groupTotal#, T_total = #getTtotal.groupTotal#, F_total = #getFtotal.groupTotal#, totalScore = #getTotal.totalScore#, score = #score#, testcompleted = 1 
	where testID = #tid# 
</cfquery>
