
<link rel="stylesheet" type="text/css" href="/styles.css">

<cfparam name="nameSearch" default="">
<cfparam name="searchScoreOption" default="">
<cfparam name="searchScore" default="">
<cfparam name="searchTotalScoreOption" default="">
<cfparam name="searchTotalScore" default="">
<cfparam name="searchPointsOption" default="">
<cfparam name="searchPoints" default="">
<cfparam name="sortResultsBy" default="lastname asc">

<div align="center">
<table width="500" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar">&nbsp;</td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
		<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td colspan="2"><b>Type in the first few letters of the last name to search by name: </b></td>
			</tr>
			<form method="post" action="lookup.cfm">
			<tr>
				<td width="16%" nowrap="nowrap">Last Name:</td>
				<td width="84%">
				<input type="text" name="nameSearch" value="<cfoutput>#nameSearch#</cfoutput>" style="width:200px" maxlength="50" /> 
				</td>
			</tr>
			<tr>
				<td width="16%" nowrap="nowrap">Score:</td>
				<td width="84%">
				<select name="searchScoreOption">
					<option <cfif searchScoreOption is "">selected</cfif> value=""></option>
					<option <cfif searchScoreOption is "=">selected</cfif> value="=">Equal To</option>
					<option <cfif searchScoreOption is "<">selected</cfif> value="<">Less Than
					<option <cfif searchScoreOption is ">">selected</cfif> value=">">Greater Than</option>
				</select>
				<select name="searchScore">
					<option value=""></option>
					<cfloop from="1" to="10" index="i">
						<cfoutput>
						<option <cfif searchScore is i>selected</cfif> value="#i#">#i#</option>
						</cfoutput>
					</cfloop>
				</select>
				</td>
			</tr>
			<tr>
				<td width="16%" nowrap="nowrap">Total Score:</td>
				<td width="84%">
				<select style="vertical-align:middle" name="searchTotalScoreOption">
					<option value=""></option>
					<option <cfif searchTotalScoreOption is "=">selected</cfif> value="=">Equal To</option>
					<option <cfif searchTotalScoreOption is "<">selected</cfif> value="<">Less Than
					<option <cfif searchTotalScoreOption is ">">selected</cfif> value=">">Greater Than</option>
				</select>
				<input type="text" name="searchTotalScore" value="<cfoutput>#searchTotalScore#</cfoutput>" style="width:35px; vertical-align:middle" maxlength="3" /> (no more than 630)
				</td>
			</tr>
			<tr>
				<td width="16%" nowrap="nowrap">Points:</td>
				<td width="84%">
				<select name="searchPointsOption">
					<option value=""></option>
					<option <cfif searchPointsOption is "=">selected</cfif> value="=">Equal To</option>
					<option <cfif searchPointsOption is "<">selected</cfif> value="<">Less Than
					<option <cfif searchPointsOption is ">">selected</cfif> value=">">Greater Than</option>
				</select>
				<select name="searchPoints">
					<option value=""></option>
					<cfloop from="0" to="6" index="i">
						<cfoutput>
						<option <cfif searchPoints is i>selected</cfif> value="#i#">#i#</option>
						</cfoutput>
					</cfloop>
				</select>
				</td>
			</tr>
			<tr>
				<td width="16%" nowrap="nowrap">Sort Results By:</td>
				<td width="84%">
				<select name="sortResultsBy">
					<option <cfif sortResultsBy is "lastname asc">selected</cfif> value="lastname asc">Name Ascending</option>
					<option <cfif sortResultsBy is "lastname desc">selected</cfif> value="lastname desc">Name Descending</option>
					<option <cfif sortResultsBy is "score asc">selected</cfif> value="score asc">Score low to high</option>
					<option <cfif sortResultsBy is "score desc">selected</cfif> value="score desc">Score high to low</option>
					<option <cfif sortResultsBy is "totalScore asc">selected</cfif> value="totalScore asc">Total Score low to high</option>
					<option <cfif sortResultsBy is "totalScore desc">selected</cfif> value="totalScore desc">Total Score high to low</option>
					<option <cfif sortResultsBy is "points asc">selected</cfif> value="points asc">Points low to high</option>
					<option <cfif sortResultsBy is "points desc">selected</cfif> value="points desc">Points high to low</option>
				</select>
				</td>
			</tr>
			<tr>
				<td colspan="2">
				<input name="btnSearch" type="submit" class="sidebar" value="Search Now">
				<input type="button" onclick="document.location = 'lookup.cfm';" class="sidebar" value="New Search">
				</td>
			</tr>
			</form>
		</table>
		</td>
	</tr>
</table>
<cfif not isDefined("form.btnSearch")>
<br />
<a class="normal" style="text-decoration:underline;" href="/index.cfm">Return to Intranet</a>
</cfif>
</div>

<cfif isDefined("form.btnSearch")>
	<cfquery name="SearchNames" datasource="#ds#">
		select testid, testdatetime, firstname, middleinitial, lastname, score, totalScore, points 
		from profileTest_main 
		where 
		<cfif trim(nameSearch) is not "">lastname like '#nameSearch#%' and </cfif>
		<cfif trim(searchScore) is not "">
		score <cfif trim(searchScoreOption) is not "">#searchScoreOption#<cfelse>=</cfif> #searchScore# and 
		</cfif> 
		<cfif trim(searchTotalScore) is not "">
		totalScore <cfif trim(searchTotalScoreOption) is not "">#searchTotalScoreOption#<cfelse>=</cfif> #searchTotalScore# and 
		</cfif>
		<cfif trim(searchPoints) is not "">
		points <cfif trim(searchPointsOption) is not "">#searchPointsOption#<cfelse>=</cfif> #searchPoints# and 
		</cfif>
		testCompleted = 1 and outsideAccess = 0 
		order by #sortResultsBy# 
	</cfquery>
	<br />
	<br />
	<div align="center">
	<table width="750" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td class="highlightbar"><b>Search Results</b> </td>
		</tr>
		<tr>
			<td class="greyrowbottomnopadding"><table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td width="16" align="center" class="linedrowrightcolumn">&nbsp;</td>
					<td width="128" align="center" class="linedrowrightcolumn"><b>Date Taken</b></td>
					<td width="290" class="linedrowrightcolumn"><b>Name</b></td>
					<td width="64" class="linedrowrightcolumn"><b>Score</b></td>
					<td width="121" class="linedrowrightcolumn"><b>Total Score</b></td>
					<td width="69" class="linedrow"><b>Points</b></td>
				</tr>
				<cfoutput query="SearchNames">
				<tr>
					<td align="center" class="linedrowrightcolumn"><a href="printout.cfm?tid=#testid#"><img src="/images/manifyingglass.gif" alt="View Details" width="15" height="15" border="0" /></a></td>
					<td align="center" class="linedrowrightcolumn">#dateformat(testDateTime,'mm/dd/yyyy')# #timeformat(testDateTime, 'hh:mm tt')#</td>
					<td class="linedrowrightcolumn">
					<cfif trim(firstname) is not "">#firstname#</cfif> 
					<cfif trim(middleInitial) is not "">#middleInitial#. </cfif>
					#lastname# 
					</td>
					<td class="linedrowrightcolumn">#score#</td>
					<td class="linedrowrightcolumn">#totalScore#</td>
					<td class="linedrow">#points#</td>
				</tr>
				</cfoutput>
			</table>
			</td>
		</tr>
	</table>
	<br />
	<a class="normal" style="text-decoration:underline;" href="/index.cfm">Return to Intranet</a>
	</div>
</cfif>
