
<link rel="stylesheet" type="text/css" href="/styles.css">

<cfparam name="err" default="">
<cfparam name="form.firstName" default="">
<cfparam name="form.lastName" default="">
<cfparam name="form.middleInitial" default="">

<style type="text/css">
input.rounded {
	border: 0; 
	background: url(/images/inputbg.gif) top left ; 
	height: 21px; 
	padding: 3px 0px;
	}
</style>

<title>Personality Profile Test</title>

<cfif isDefined("form.btnContinue")>
	
	<!--- validate input --->
	<cfif trim(form.firstName) is "">
		<cfset err = listappend(err,"The first name is required.")>
	</cfif>
	<cfif trim(form.lastName) is "">
		<cfset err = listappend(err,"The last name is required.")>
	</cfif>
	
	<cfif trim(err) is "">
		<cfset testDateTime = createodbcdatetime(now())>
		<cfquery name="createTest" datasource="#ds#">
			insert into profileTest_main (testDateTime, firstName, middleInitial, lastName) 
			values (#testDateTime#, '#form.firstName#', '#form.middleInitial#', '#form.lastName#') 
		</cfquery>
		<cfquery name="getTestID" datasource="#ds#">
			select max(testid) as tid from profileTest_main 
			where testDateTime = #testDateTime# and firstName = '#form.firstName#' and middleInitial = '#form.middleInitial#' and lastName = '#form.lastName#' 
		</cfquery>
		<cfset testID = getTestID.tid>
		<cflocation url="test.cfm?tid=#testid#"> 
	</cfif>
	
</cfif>
	
<div align="center">
<body onLoad="document.forms[0].firstName.focus();">
<table border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Profile Test</b> </td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
		<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<cfif trim(err) is not "">
			<cfloop list="#err#" index="m">
			<tr>
				<td colspan="2" class="alert"><b><cfoutput>#m#</cfoutput></b></td>
			</tr>
			</cfloop>
			</cfif>
			<tr>
				<td colspan="2"><b>Please enter your first name, middle initial and last name</b></td>
			</tr>
			<form method="post" action="index.cfm">
			<tr>
				<td><b>First Name: </b></td>
				<td valign="middle">
				<input style="width:250px" tabindex="1" type="text" name="firstName" maxlength="50" value="<cfoutput>#form.firstName#</cfoutput>"  />
				</td>
			</tr>
			<tr>
				<td><b>Middle Initial: </b></td>
				<td valign="middle">
				<input style="width:14px;" tabindex="2" type="text" name="middleInitial" maxlength="1" value="<cfoutput>#form.middleInitial#</cfoutput>" />
				</td>
			</tr>
			<tr>
				<td><b>Last Name: </b></td>
				<td>
				<input style="width:250px" tabindex="3" type="text" name="lastName" maxlength="50" value="<cfoutput>#form.lastName#</cfoutput>" />
				</td>
			</tr>
			<tr>
				<td colspan="2"><input tabindex="4" name="btnContinue" type="submit" class="sidebar" value="Continue" /></td>
			</tr>
			</form>
		</table>
		</td>
	</tr>
</table>
</body>
<cfif isDefined("cookie.adminlogin")>
<br>
<a class="normal" style="text-decoration:underline;" href="/index.cfm">Return to Intranet</a>
</cfif>
</div>

