
<cfsetting showdebugoutput="no">

<cfquery name="getTables" datasource="profileTestData">
	select name from sys.tables 
	where name <> 'foxuser' and name <> 'question'
	order by name asc 
</cfquery>

<cfloop query="getTables">
	<cfquery name="checkExisting" datasource="#ds#">
		select testid from profileTest_main 
		where lastname = '#getTables.name#'
	</cfquery>
	<cfif checkExisting.recordcount is 0>
		<cfset tableName = getTables.name>
		<cfquery name="readTable" datasource="profileTestData">
			select * 
			from [#tableName#] 
		</cfquery>
		
		<cfif readTable.recordcount gt 0>
			<!--- create main record --->
			<cfset testDateTime = createodbcdatetime(now())>
			<cfquery name="createTest" datasource="#ds#">
				insert into profileTest_main (testDateTime, firstName, middleInitial, lastName) 
				values (#testDateTime#, '', '', '#tableName#') 
			</cfquery>
			<cfquery name="getTestID" datasource="#ds#">
				select max(testid) as tid from profileTest_main 
				where testDateTime = #testDateTime# and firstName = '' and middleInitial = '' and lastName = '#tableName#' 
			</cfquery>
			<cfset testID = getTestID.tid>
			
			<!--- create test records --->
			<cfquery name="insertAnswers" datasource="#ds#">
				<cfloop query="readTable">
				begin
					insert into profileTest_answers (testID, questionNumber, answer) 
					values (#variables.testID#, #readTable.attrib_id#, '#readTable.response#') 
				end
				</cfloop>
			</cfquery>
			
			<!--- process answers --->
			<cfset tid = variables.testID>
			<cfinclude template="doCalculations.cfm">
		</cfif>
	</cfif>
</cfloop>

done.
