
<link rel="stylesheet" type="text/css" href="/styles.css">

<cfparam name="missList" default="">
<cfparam name="start" default="0">

<cfquery name="getMaxQuestions" datasource="#ds#">
	select max(questionNumber) as maxQ from profileTest_questions 
</cfquery>
<cfset maxQuestions = getMaxQuestions.maxQ>

<cfset qPerPage = 12>

<cfset end = start + qPerPage>
<cfif end gt maxQuestions><cfset end = maxQuestions></cfif>

<cfloop from="#evaluate(start + 1)#" to="#end#" index="i">
	<cfparam name="answer#i#" default="0">
</cfloop>

<cfif isDefined("tid")>
	<cfset testID = tid>
</cfif>

<cfif isDefined("form.BtnNext")>
	<cfloop from="#evaluate(oldStart + 1)#" to="#oldEnd#" index="i">
		<cfparam name="answer#i#" default="0">
	</cfloop>
	<!--- validate test first --->
	<cfloop from="#evaluate(oldStart + 1)#" to="#oldEnd#" index="i">
		<cfif #evaluate("answer#i#")# is 0>
			<cfset missList = listAppend(missList,i)>
		</cfif>
	</cfloop>

	<cfif trim(missList) is "">
		<cfquery name="delExisting" datasource="#ds#">
			delete from profileTest_answers 
			where questionNumber > #oldStart# and testid = #testID# 
		</cfquery>
		<!--- save answers --->
		<cfloop from="#evaluate(oldStart + 1)#" to="#oldEnd#" index="i">
			<cfquery name="insertAnswers" datasource="#ds#">
				insert into profileTest_answers (testID, questionNumber, answer) 
				values (#testID#, #i#, '#evaluate("answer#i#")#') 
			</cfquery>
		</cfloop>
		<cfif oldEnd is maxQuestions><cflocation url="process.cfm?tid=#testid#"></cfif>
	<cfelse>
		<cfset start = oldStart>
		<cfset end = oldEnd>
	</cfif>
</cfif>

<cfquery name="getQuestions" datasource="#ds#">
	select top #qPerPage# * from profileTest_questions 
	where questionNumber > #start# 
	order by questionNumber asc 
</cfquery>

<div align="center">
<cfif trim(missList) is not "">
<br />
<table width="500" border="0" cellspacing="0" cellpadding="5" bgcolor="#CC0000" class="box">
	<tr>
		<td><b style="color:#FFFFFF">You have missed some questions. They are highlighted in red below.</b></td>
	</tr>
</table>
</cfif>
<br />
<table border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Test Questions</b></td>
	</tr>
	<form method="post" action="test.cfm">
	<cfoutput>
	<input type="hidden" name="oldStart" value="#start#" />
	<input type="hidden" name="oldEnd" value="#end#" />
	<input type="hidden" name="start" value="#end#" />
	<input type="hidden" name="testID" value="#testID#" />
	</cfoutput>
	<tr>
		<td class="greyrowbottomnopadding">
		<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td colspan="2" class="linedrow"><b>For each item, choose the option which best describes yourself</b></td>
			</tr>
			<cfoutput query="getQuestions">
			<cfset newStart = getQuestions.questionNumber>
			<cfif listfind(missList,questionNumber) is not 0>
				<cfset bgc = "##CC0000">
				<cfset textcolor = "##FFFFFF">
			<cfelse>
				<cfset textcolor = "##000000">
				<cfif getQuestions.currentRow mod 2 is 0>
					<cfset bgc = "##EEEEEE">
				<cfelse>
					<cfset bgc = "##F9F9F9">
				</cfif>
			</cfif>
			<tr>
				<td style="color:#textcolor#" bgcolor="#bgc#">#questionNumber#.</td>
				<td style="color:#textcolor#" bgcolor="#bgc#"><b>#question#</b> (#definition#)</td>
			</tr>
			<tr>
				<td bgcolor="#bgc#" class="linedrow">&nbsp;</td>
				<td bgcolor="#bgc#" style="color:#textcolor#" class="linedrow">
				<input type="radio" <cfif evaluate("answer#questionNumber#") is 1>checked</cfif> name="answer#questionNumber#" value="1" /> Marginally Descriptive
				<input type="radio" <cfif evaluate("answer#questionNumber#") is 3>checked</cfif> name="answer#questionNumber#" value="3" /> Somewhat Descriptive
				<input type="radio" <cfif evaluate("answer#questionNumber#") is 5>checked</cfif> name="answer#questionNumber#" value="5" /> Usually Descriptive
				<input type="radio" <cfif evaluate("answer#questionNumber#") is 7>checked</cfif> name="answer#questionNumber#" value="7" /> Very Descriptive
				</td>
			</tr>
			</cfoutput>
		</table>
		</td>
	</tr>
	<cfif end is not maxQuestions>
	<tr>
		<td><input type="submit" name="BtnNext" value="Next Page" /></td>
	</tr>
	<cfelse>
	<tr>
		<td><input type="submit" name="BtnNext" value="Finish Test" /></td>
	</tr>
	</cfif>
	</form>
</table>
</div>
