
<cfsetting showdebugoutput="no">

<link rel="stylesheet" type="text/css" href="/styles.css">

<cfquery name="getAnswers" datasource="#ds#">
	select profileTest_questions.questionNumber, profileTest_questions.question, profileTest_answers.answer 
	from profileTest_answers 
	left join profileTest_questions on profileTest_answers.questionNumber = profileTest_questions.questionNumber
	where profileTest_answers.testID = #tid# 
	order by profileTest_questions.questionNumber asc 
</cfquery>

<cfquery name="getMainInfo" datasource="#ds#">
	select * from profileTest_main 
	where testid = #tid# 
</cfquery>

<cfset maxPerColumn = ceiling(getAnswers.recordcount / 2)>

<div align="center">
<table width="600" border="0" cellpadding="2" cellspacing="0" class="smalltable">
	<tr>
		<td colspan="2" align="center"><b style="font-size:16px">C.O.P.S. Monitoring </b></td>
	</tr>
	<tr>
		<td colspan="2" align="center">Applicant Personality Profile </td>
	</tr>
	<tr><td colspan="2">&nbsp;</td></tr>
	<tr><td colspan="2">&nbsp;</td></tr>
	<cfoutput query="getMainInfo">
	<tr>
		<td colspan="2" align="center" class="nopadding" style="padding:0px">
		<table border="0" cellpadding="5" cellspacing="0">
			<tr>
				<td valign="top" nowrap><b>Totals:</b></td>
				<td valign="top" class="nopadding">
				<table border="0" cellpadding="2" cellspacing="0" class="smalltable">
					<tr>
						<td align="center">R</td>
						<td>=</td>
						<td>#R_total#</td>
					</tr>
					<tr>
						<td align="center">I</td>
						<td>=</td>
						<td>#I_total#</td>
					</tr>
					<tr>
						<td align="center">A</td>
						<td>=</td>
						<td>#A_total#</td>
					</tr>
				</table>				</td>
				<td width="25" valign="top" class="nopadding">&nbsp;</td>
				<td valign="top" class="nopadding"><table border="0" cellpadding="2" cellspacing="0" class="smalltable">

					<tr>
						<td align="center">S</td>
						<td>=</td>
						<td>#S_total#</td>
					</tr>
					<tr>
						<td align="center">E</td>
						<td>=</td>
						<td>#E_total#</td>
					</tr>
					<tr>
						<td align="center">C</td>
						<td>=</td>
						<td>#C_total#</td>
					</tr>
				</table></td>
				<td width="25" valign="top" class="nopadding">&nbsp;</td>
				<td valign="top" class="nopadding">
				<table border="0" cellpadding="2" cellspacing="0" class="smalltable">
					<tr>
						<td align="right">SN</td>
						<td>=</td>
						<td>#SN_total#</td>
					</tr>
					<tr>
						<td align="right">N</td>
						<td>=</td>
						<td>#N_total#</td>
					</tr>
					<tr>
						<td align="right">T</td>
						<td>=</td>
						<td>#T_total#</td>
					</tr>
					<tr>
						<td align="right">F</td>
						<td>=</td>
						<td>#F_total#</td>
					</tr>
				</table>				</td>
				<td width="50" valign="top" class="nopadding">&nbsp;</td>
				<td align="center" valign="top" class="nopadding">
				<table border="0" cellpadding="2" cellspacing="0" class="smalltable">
					<tr>
						<td width="52" align="right">Points</td>
						<td width="9">=</td>
						<td width="170" nowrap="nowrap">#points# / 6 </td>
					</tr>
					<tr>
						<td align="right">Total</td>
						<td>=</td>
						<td nowrap="nowrap">#totalScore# / 630 </td>
					</tr>
					<tr>
						<td align="right">Score</td>
						<td>=</td>
						<td nowrap="nowrap">#score#</td>
					</tr>
					<tr>
						<td align="right">Applicant</td>
						<td>=</td>
						<td nowrap="nowrap">#firstName# #lastName#</td>
					</tr>
				</table>				</td>
				<!--- 
				<td width="328" valign="top" class="nopadding">
				<table border="0" cellpadding="3" cellspacing="0">
					<tr>
						<td align="center">R</td>
						<td>=</td>
						<td>Recreational, Work Outdoors </td>
					</tr>
					<tr>
						<td align="center">I</td>
						<td>=</td>
						<td>Investigative</td>
					</tr>
					<tr>
						<td align="center">A</td>
						<td>=</td>
						<td>Artistic</td>
					</tr>
					<tr>
						<td align="center">S</td>
						<td>=</td>
						<td>Social</td>
					</tr>
					<tr>
						<td align="center">E</td>
						<td>=</td>
						<td>Enterprising - Need for status recognition </td>
					</tr>
					<tr>
						<td align="center">C</td>
						<td>=</td>
						<td nowrap="nowrap">Convential, detail oriented. Can sit at a computer </td>
					</tr>
				</table>
				</td>
				--->
			</tr>
		</table>
		</td>
	</tr>
	</cfoutput>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td width="50%" valign="top">
		<table width="100%" border="0" cellpadding="2" cellspacing="0" class="smalltable">
			<tr>
				<td class="linedrow" width="11%">&nbsp;</td>
				<td class="linedrow" width="64%"><b>Attribute</b></td>
				<td class="linedrow"  width="25%" align="center"><b>Response</b></td>
			</tr>
			<cfoutput query="getAnswers" maxrows="#maxPerColumn#" startrow="1">
			<tr>
				<td nowrap valign="top">#questionNumber#.</td>
				<td valign="top" nowrap="nowrap">#question#</td>
				<td valign="top" align="center">#answer#</td>
			</tr>
			</cfoutput>
		</table>
		</td>
		<td width="50%" valign="top">
		<table width="100%" border="0" cellpadding="2" cellspacing="0" class="smalltable">
			<tr>
				<td class="linedrow"  width="12%">&nbsp;</td>
				<td class="linedrow"  width="63%"><b>Attribute</b></td>
				<td class="linedrow"  width="25%" align="center"><b>Response</b></td>
			</tr>
			<cfoutput query="getAnswers" startrow="#evaluate(maxPerColumn + 1)#">
			<tr>
				<td valign="top" nowrap>#questionNumber#.</td>
				<td valign="top" nowrap="nowrap">#question#</td>
				<td valign="top" align="center">#answer#</td>
			</tr>
			</cfoutput>
		</table>
		</td>
	</tr>
	<tr>
		<td colspan="2" class="linedrow">&nbsp;</td>
	</tr>
</table>
</div>

