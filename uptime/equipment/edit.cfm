
<cfif isDefined("form.btnSave")>
	<cfquery name="addEquipment" datasource="#ds#">
		update uptime_equipment 
		set equipmentName = '#form.equipmentName#', equipmentCategoryID = #form.equipmentCategoryID#, startDate = '#sd#'
		where equipmentid = #id# 
	</cfquery>
	<cflocation url="index.cfm">
</cfif>

<cfif isDefined("form.btnCancel")>
	<cflocation url="index.cfm">
</cfif>

<link rel="stylesheet" type="text/css" href="../../styles.css">

<script language="javascript" src="../cal2.js">
/*
Xin's Popup calendar script-  Xin Yang (http://www.yxscripts.com/)
Script featured on/available at http://www.dynamicdrive.com/
This notice must stay intact for use
*/
</script>
<script language="javascript" src="../cal_conf2.js"></script>

<cfquery name="getCats" datasource="#ds#">
	select * from uptime_equipment_categories
	order by equipmentCategory asc 
</cfquery>

<cfquery name="getItem" datasource="#ds#">
	select * from uptime_equipment 
	where equipmentID = #id# 
</cfquery>

<div align="center" class="normal">
	<table border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td class="highlightbar"><b>Equipment Maintenance - Add Equipment</b></td>
		</tr>
		<tr>
			<td class="greyrowbottomnopadding">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<form method="post" action="edit.cfm" name="mainform">
					<input type="hidden" name="id" value="<cfoutput>#id#</cfoutput>">
					<cfoutput query="getItem">
					<cfset sd = getItem.startDate>
					<tr>
						<td width="20%" nowrap>Name:</td>
						<td width="80%">
							<input name="equipmentName" type="text" size="75" maxlength="100" value="#equipmentName#">
						</td>
					</tr>
					<tr>
						<td nowrap>Category:</td>
						<td>
						<select name="equipmentCategoryID">
							<option value="0">Select a category</option>
							<cfloop query="getCats">
								<option <cfif getCats.equipmentCategoryID is getitem.equipmentcategoryid>selected</cfif> value="#equipmentCategoryID#">#equipmentCategory#</option>
							</cfloop>
						</select>
						</td>
					</tr>
					
					<tr>
						<td nowrap>Start Date: </td>
						<td class="nopadding">
							<table border="0" cellpadding="5" cellspacing="0" class="grey">
									<tr>
										<td>
											<input name="sd" type="text" style="width:75px" maxlength="10" value="#dateformat(sd, 'mm/dd/yyyy')#" />
										</td>
										<td><a style="text-decoration:none;" href="javascript:showCal('startDate');"> <img src="../../images/calicon.gif" alt="Select a Date" align="absmiddle" width="20" height="20" border="0" /></a></td>
									</tr>
							</table>
						</td>
					</tr>
					</cfoutput>
					<tr>
						<td colspan="2" nowrap>
						<input type="submit" name="btnSave" value="Save Item" class="sidebar">
						<input type="submit" name="btnCancel" value="Cancel Changes" class="sidebar">
						</td>
					</tr>
					</form>
				</table>
			</td>
		</tr>
	</table>
	<br>
	<a style="text-decoration:underline" href="/index.cfm">Return to Intranet</a>
</div>
