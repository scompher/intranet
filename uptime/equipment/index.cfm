
<link rel="stylesheet" type="text/css" href="../../styles.css">

<cfquery name="getSystems" datasource="#ds#">
	select * from uptime_equipment
	order by equipmentname asc 
</cfquery>

<div align="center" class="normal">
	<table width="100%" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td class="highlightbar"><b>Equipment Maintenance - Equipment</b></td>
		</tr>
		<tr>
			<td class="greyrowbottomnopadding">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td colspan="3"><a style="text-decoration:underline;" href="add.cfm">Add New</a></td>
					</tr>
					<tr>
						<td colspan="2" align="center" class="linedrow"><b>Action</b></td>
						<td class="linedrow"><b>Equipment</b></td>
					</tr>
					<cfoutput query="getsystems">
					<tr>
						<td width="10" align="center" class="linedrowrightcolumn"><a href="edit.cfm?id=#equipmentid#">[edit]</a></td>
						<td width="10" align="center" class="linedrowrightcolumn"><a onclick="return confirm('Are you sure you wish to delete this item?');" href="delete.cfm?id=#equipmentid#">[delete]</a></td>
						<td class="linedrow">#equipmentname#</td>
					</tr>
					</cfoutput>
				</table>
			</td>
		</tr>
	</table>
	<br>
	<a style="text-decoration:underline" href="/index.cfm">Return to Intranet</a>
</div>
