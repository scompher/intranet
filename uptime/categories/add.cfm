
<cfif isDefined("form.btnSave")>
	<cfquery name="addEquipment" datasource="#ds#">
		declare @catid as int
		begin
			set @catid = (select max(equipmentCategoryID) from uptime_equipment_categories)
		end
		begin
			set @catid = @catid + 1
		end
		begin
			insert into uptime_equipment_categories (equipmentCategoryID, equipmentCategory) 
			values (@catid, '#form.equipmentCategory#')
		end
	</cfquery>
	<cflocation url="index.cfm">
</cfif>

<cfif isDefined("form.btnCancel")>
	<cflocation url="index.cfm">
</cfif>

<link rel="stylesheet" type="text/css" href="../../styles.css">

<script language="javascript" src="../cal2.js">
/*
Xin's Popup calendar script-  Xin Yang (http://www.yxscripts.com/)
Script featured on/available at http://www.dynamicdrive.com/
This notice must stay intact for use
*/
</script>
<script language="javascript" src="../cal_conf2.js"></script>

<div align="center" class="normal">
	<table border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td class="highlightbar"><b>Equipment Maintenance - Add Category</b></td>
		</tr>
		<tr>
			<td class="greyrowbottomnopadding">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<form method="post" action="add.cfm" name="mainform">
					<tr>
						<td width="20%" nowrap>Category Name:</td>
						<td width="80%">
							<input name="equipmentCategory" type="text" size="75" maxlength="100">
						</td>
					</tr>
					<tr>
						<td colspan="2" nowrap>
						<input type="submit" name="btnSave" value="Save Item" class="sidebar">
						<input type="submit" name="btnCancel" value="Cancel Changes" class="sidebar">
						</td>
					</tr>
					</form>
				</table>
			</td>
		</tr>
	</table>
	<br>
	<a style="text-decoration:underline" href="/index.cfm">Return to Intranet</a>
</div>
