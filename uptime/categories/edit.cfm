
<cfif isDefined("form.btnSave")>
	<cfquery name="updateEquipment" datasource="#ds#">
		update uptime_equipment_categories 
		set equipmentCategory = '#form.equipmentCategory#'  
		where equipmentCategoryID = #id#
	</cfquery>
	<cflocation url="index.cfm">
</cfif>

<cfif isDefined("form.btnCancel")>
	<cflocation url="index.cfm">
</cfif>

<link rel="stylesheet" type="text/css" href="../../styles.css">

<script language="javascript" src="../cal2.js">
/*
Xin's Popup calendar script-  Xin Yang (http://www.yxscripts.com/)
Script featured on/available at http://www.dynamicdrive.com/
This notice must stay intact for use
*/
</script>
<script language="javascript" src="../cal_conf2.js"></script>

<cfquery name="getCat" datasource="#ds#">
	select * from uptime_equipment_categories
	where equipmentCategoryID = #id# 
</cfquery>

<div align="center" class="normal">
	<table border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td class="highlightbar"><b>Equipment Maintenance - Add Category</b></td>
		</tr>
		<tr>
			<td class="greyrowbottomnopadding">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<form method="post" action="edit.cfm" name="mainform">
					<input type="hidden" name="id" value="<cfoutput>#id#</cfoutput>">
					<cfoutput query="getCat">
					<tr>
						<td width="20%" nowrap>Category Name:</td>
						<td width="80%">
							<input name="equipmentCategory" type="text" size="75" maxlength="100" value="#equipmentCategory#">
						</td>
					</tr>
					</cfoutput>
					<tr>
						<td colspan="2" nowrap>
						<input type="submit" name="btnSave" value="Save Item" class="sidebar">
						<input type="submit" name="btnCancel" value="Cancel Changes" class="sidebar">
						</td>
					</tr>
					</form>
				</table>
			</td>
		</tr>
	</table>
	<br>
	<a style="text-decoration:underline" href="/index.cfm">Return to Intranet</a>
</div>
