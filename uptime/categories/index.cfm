
<link rel="stylesheet" type="text/css" href="../../styles.css">

<cfquery name="getSystems" datasource="#ds#">
	select * from uptime_equipment_categories
	order by equipmentCategory asc 
</cfquery>

<div align="center" class="normal">
	<table width="100%" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td class="highlightbar"><b>Category Maintenance </b></td>
		</tr>
		<tr>
			<td class="greyrowbottomnopadding">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td colspan="3"><a style="text-decoration:underline;" href="add.cfm">Add New</a></td>
					</tr>
					<tr>
						<td colspan="2" align="center" class="linedrow"><b>Action</b></td>
						<td class="linedrow"><b>Category Name</b></td>
					</tr>
					<cfoutput query="getsystems">
					<tr>
						<td width="10" align="center" class="linedrowrightcolumn"><a href="edit.cfm?id=#equipmentCategoryID#">[edit]</a></td>
						<td width="10" align="center" class="linedrowrightcolumn"><a onclick="return confirm('Are you sure you wish to delete this item?');" href="delete.cfm?id=#equipmentCategoryID#">[delete]</a></td>
						<td class="linedrow">#equipmentCategory#</td>
					</tr>
					</cfoutput>
				</table>
			</td>
		</tr>
	</table>
	<br>
	<a style="text-decoration:underline" href="/index.cfm">Return to Intranet</a>
</div>
