<link rel="stylesheet" type="text/css" href="../../styles.css">

<cfparam name="err" default="">

<cfif isDefined("form.btnSaveOuttage")>

	<cfif not isdate(form.dateStarted)>
		<cfset err = listappend(err,"The date started is not a valid date.")>
	</cfif>
	<cfif not isdate(form.dateEnded)>
		<cfset err = listappend(err,"The date ended is not a valid date.")>
	</cfif>

	<cfif trim(err)is "">
		<cfset startTime = createodbcdatetime("#form.dateStarted# #form.startTimeHH#:#form.startTimeMM#:#form.startTimeSS# #form.startTimeTT#")>
		<cfset endTime = createodbcdatetime("#form.dateEnded# #form.endTimeHH#:#form.endTimeMM#:#form.endTimeSS# #form.endTimeTT#")>
		<cfset mins = datediff("n", startTime, endTime)>
		<cfset hrsUnavailable = (mins / 60)>
	
		<cfquery name="updateOuttage" datasource="#ds#">
			insert into uptime_outtages (equipmentID, causeid, description, startTime, endTime, hrsUnavailable, planned)
			values (#form.equipmentID#, #form.causeid#, '#form.description#', #startTime#, #endTime#, #numberformat(hrsUnavailable, "00.00")#, #form.planned#)
		</cfquery>
		
		<cflocation url="index.cfm">
	</cfif>

</cfif>

<script language="javascript" src="../cal2.js">
/*
Xin's Popup calendar script-  Xin Yang (http://www.yxscripts.com/)
Script featured on/available at http://www.dynamicdrive.com/
This notice must stay intact for use
*/
</script>
<script language="javascript" src="../cal_conf2.js"></script>

<cfquery name="getEquipment" datasource="#ds#">
	select equipmentID, equipmentName
	from uptime_equipment
	order by equipmentName asc
</cfquery>

<cfquery name="getCauses" datasource="#ds#">
	select causeid, causeName
	from uptime_causes
	order by causeName asc
</cfquery>

<cfset today = dateformat(now(), 'mm/dd/yyyy')>

<script language="JavaScript" type="text/javascript">
function checkForm(frm) {
	
	if (frm.equipmentid.selectedIndex == 0) {alert('The equipment is required.'); frm.equipmentid.focus(); return false;}
	if (frm.causeid.selectedIndex == 0) {alert('The cause is required.'); frm.causeid.focus(); return false;}
	if (frm.description.value == "") {alert('The description is required.'); frm.description.focus(); return false;}
	if (frm.dateStarted.value == "") {alert('The date started is required.'); frm.dateStarted.focus(); return false;}
	if (frm.startTimeHH.value == "") {alert('The start time hours are required.'); frm.startTimeHH.focus(); return false;}
	if (frm.startTimeMM.value == "") {alert('The start time minutes are required.'); frm.startTimeMM.focus(); return false;}
	if (frm.startTimeSS.value == "") {alert('The start time seconds are required.'); frm.startTimeSS.focus(); return false;}
	if (frm.startTimeTT.selectedIndex == 0) {alert('The start time am or pm is required.'); frm.startTimeTT.focus(); return false;}
	if (frm.endTimeHH.value == "") {alert('The end time hours are required.'); frm.endTimeHH.focus(); return false;}
	if (frm.endTimeMM.value == "") {alert('The end time minutes are required.'); frm.endTimeMM.focus(); return false;}
	if (frm.endTimeSS.value == "") {alert('The end time seconds are required.'); frm.endTimeSS.focus(); return false;}
	if (frm.endTimeTT.selectedIndex == 0) {alert('The end time am or pm is required.'); frm.endTimeTT.focus(); return false;}
	if ((frm.planned[0].checked == false) && (frm.planned[1].checked == false)) {alert('Planned is required.'); return false;}

	frm.btnSaveOuttage.value = 1;
	frm.submit();
}
</script>

<div align="center">
<table border="0" cellspacing="0" cellpadding="5">
	<cfif trim(err) is not "">
	<tr>
		<td class="alert"><b>There are errors with your submission:</b></td>
	</tr>
	<cfloop list="#err#" index="e">
	<cfoutput>
	<tr>
		<td class="alert">
		<b>#e#</b>
		</td>
	</tr>
	</cfoutput>
	</cfloop>
	</cfif>
	<tr>
		<td class="highlightbar"><b>Enter Outage Information</b></td>
	</tr>
	<form method="post" action="" name="mainform">
	<tr>
		<td class="greyrowbottom" style="padding:0px;">
			<table border="0" cellpadding="5" cellspacing="0" class="grey">
				<tr>
					<td width="105" nowrap>Equipment:</td>
					<td width="441" nowrap>
						<select name="equipmentid">
							<option value="0"></option>
							<cfoutput query="getequipment">
								<option value="#equipmentID#">#equipmentName#</option>
							</cfoutput>
						</select>
					</td>
				</tr>
				<tr>
					<td nowrap>Cause:</td>
					<td nowrap>
						<select name="causeid">
							<option value="0"></option>
							<cfoutput query="getcauses">
								<option value="#causeid#">#causename#</option>
							</cfoutput>
						</select>
					</td>
				</tr>
				<tr>
					<td colspan="2" nowrap>Description of Problem: </td>
				</tr>
				<tr>
					<td colspan="2" nowrap>
						<textarea name="description" rows="5" style="width:550px"></textarea>
					</td>
				</tr>
				<tr>
					<td nowrap>Started: </td>
					<td nowrap class="nopadding">
						<table border="0" cellpadding="5" cellspacing="0" class="grey">
							<tr>
								<td class="nopadding">
									<table border="0" cellpadding="5" cellspacing="0" class="grey">
										<cfoutput>
											<tr>
												<td>
													<input name="dateStarted" type="text" style="width:75px" value="#today#" maxlength="10" />
												</td>
												<td><a style="text-decoration:none;" href="javascript:showCal('DateStarted');"> <img src="../../images/calicon.gif" alt="Select a Date" align="absmiddle" width="20" height="20" border="0" /></a></td>
												<td>
													<input type="text" style="width:25px" name="startTimeHH" value="#timeformat(now(),'hh')#" onFocus="this.select();" onKeyUp="if (this.value.charAt(this.value.length-1) == ':') {this.value = this.value.slice(0, this.value.length - 1); this.form.startTimeMM.focus();}" />
												</td>
												<td class="nopadding">:</td>
												<td>
													<input type="text" style="width:25px" name="startTimeMM" value="#timeformat(now(),'mm')#" onFocus="this.select();" />
												</td>
												<td class="nopadding">:</td>												
												<td>
													<input type="text" style="width:25px" name="startTimeSS" value="#timeformat(now(),'ss')#" onFocus="this.select();" />
												</td>
												<td>
													<select name="startTimeTT">
														<option value=""></option>
														<option <cfif timeformat(now(),'TT') is "AM">selected</cfif> value="AM">AM</option>
														<option <cfif timeformat(now(),'TT') is "PM">selected</cfif> value="PM">PM</option>
													</select>
												</td>
											</tr>
										</cfoutput>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td nowrap>Ended:</td>
					<td nowrap class="nopadding">
						<table border="0" cellpadding="5" cellspacing="0" class="grey">
							<tr>
								<td class="nopadding">
									<table border="0" cellpadding="5" cellspacing="0" class="grey">
										<cfoutput>
											<tr>
												<td>
													<input name="dateEnded" type="text" style="width:75px" value="#today#" maxlength="10" />
												</td>
												<td><a style="text-decoration:none;" href="javascript:showCal('DateEnded');"> <img src="../../images/calicon.gif" alt="Select a Date" align="absmiddle" width="20" height="20" border="0" /></a></td>
												<td>
													<input type="text" style="width:25px" name="endTimeHH" value="#timeformat(now(),'hh')#" onFocus="this.select();" onKeyUp="if (this.value.charAt(this.value.length-1) == ':') {this.value = this.value.slice(0, this.value.length - 1); this.form.endTimeMM.focus();}" />
												</td>
												<td class="nopadding">:</td>
												<td>
													<input type="text" style="width:25px" name="endTimeMM" value="#timeformat(now(),'mm')#" onFocus="this.select();" />
												</td>
												<td class="nopadding">:</td>
												<td>
													<input type="text" style="width:25px" name="endTimeSS" value="#timeformat(now(),'ss')#" onFocus="this.select();" />
												</td>
												<td>
													<select name="endTimeTT">
														<option value=""></option>
														<option <cfif timeformat(now(),'TT') is "AM">selected</cfif> value="AM">AM</option>
														<option <cfif timeformat(now(),'TT') is "PM">selected</cfif> value="PM">PM</option>
													</select>
												</td>
											</tr>
										</cfoutput>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td nowrap>This outage was: </td>
					<td nowrap class="nopadding">
						<table border="0" cellspacing="0" cellpadding="5" class="grey">
							<tr>
								<td>
									<input name="planned" type="radio" value="1" />
								</td>
								<td>Planned</td>
								<td>&nbsp;&nbsp;</td>
								<td>
									<input name="planned" type="radio" value="0" />
								</td>
								<td>Un-Planned</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2">
					<input type="button" onclick="return checkForm(this.form);" class="sidebar" value="Save Outage Information">
					<input type="hidden" name="btnSaveOuttage" value="0" />
					&nbsp;
					<input type="button" class="sidebar" value="Cancel &amp; Exit" onclick="document.location = 'index.cfm';">
					</td>
				</tr>
			</table>
		</td>
	</tr>
	</form>
</table>
</div>

