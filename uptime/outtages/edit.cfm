<link rel="stylesheet" type="text/css" href="../../styles.css">

<cfif isDefined("form.btnUpdateOuttage")>

	<cfset startTime = createodbcdatetime("#form.dateStarted# #form.startTimeHH#:#form.startTimeMM# #form.startTimeTT#")>
	<cfset endTime = createodbcdatetime("#form.dateEnded# #form.endTimeHH#:#form.endTimeMM# #form.endTimeTT#")>
	<cfset mins = datediff("n", startTime, endTime)>
	<cfset hrsUnavailable = (mins / 60)>

	<cfquery name="updateOuttage" datasource="#ds#">
		update uptime_outtages
		set equipmentID = #form.equipmentID#, causeid = #form.causeid#, description = '#form.description#', startTime = #startTime#, endTime = #endTime#, hrsUnavailable = #numberformat(hrsUnavailable, "00.00")#, planned = #form.planned#
		where outtageid = #oid#
	</cfquery>

	<cflocation url="index.cfm">
	
</cfif>

<script language="javascript" src="../cal2.js">
/*
Xin's Popup calendar script-  Xin Yang (http://www.yxscripts.com/)
Script featured on/available at http://www.dynamicdrive.com/
This notice must stay intact for use
*/
</script>
<script language="javascript" src="../cal_conf2.js"></script>

<cfquery name="getEquipment" datasource="#ds#">
	select equipmentID, equipmentName
	from uptime_equipment
	order by equipmentName asc
</cfquery>

<cfquery name="getCauses" datasource="#ds#">
	select causeid, causeName
	from uptime_causes
	order by causeName asc
</cfquery>

<cfquery name="getOuttage" datasource="#ds#">
	select * 
	from uptime_outtages
	where outtageid = #oid#
</cfquery>

<script language="JavaScript" type="text/javascript">
function checkForm(frm) {
	
	if (frm.equipmentid.selectedIndex == 0) {alert('The equipment is required.'); frm.equipmentid.focus(); return false;}
	if (frm.causeid.selectedIndex == 0) {alert('The cause is required.'); frm.causeid.focus(); return false;}
	if (frm.description.value == "") {alert('The description is required.'); frm.description.focus(); return false;}
	if (frm.dateStarted.value == "") {alert('The date started is required.'); frm.dateStarted.focus(); return false;}
	if (frm.startTimeHH.value == "") {alert('The start time hours are required.'); frm.startTimeHH.focus(); return false;}
	if (frm.startTimeMM.value == "") {alert('The start time minutes are required.'); frm.startTimeMM.focus(); return false;}
	if (frm.startTimeSS.value == "") {alert('The start time seconds are required.'); frm.startTimeSS.focus(); return false;}
	if (frm.startTimeTT.selectedIndex == 0) {alert('The start time am or pm is required.'); frm.startTimeTT.focus(); return false;}
	if (frm.endTimeHH.value == "") {alert('The end time hours are required.'); frm.endTimeHH.focus(); return false;}
	if (frm.endTimeMM.value == "") {alert('The end time minutes are required.'); frm.endTimeMM.focus(); return false;}
	if (frm.endTimeSS.value == "") {alert('The end time seconds are required.'); frm.endTimeSS.focus(); return false;}
	if (frm.endTimeTT.selectedIndex == 0) {alert('The end time am or pm is required.'); frm.endTimeTT.focus(); return false;}
	if ((frm.planned[0].checked == false) && (frm.planned[1].checked == false)) {alert('Planned is required.'); return false;}

	frm.btnUpdateOuttage.value = 1;
	frm.submit();
}
</script>

<div align="center">
<table border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Enter Outage Information</b></td>
	</tr>
	<form method="post" action="" name="mainform">
	<cfoutput>
	<input type="hidden" name="oid" value="#oid#">
	</cfoutput>
	<tr>
		<td class="greyrowbottom" style="padding:0px;">
			<table border="0" cellpadding="5" cellspacing="0" class="grey">
				<tr>
					<td width="105" nowrap>Equipment:</td>
					<td width="441" nowrap>
						<select name="equipmentid">
							<option value="0"></option>
							<cfoutput query="getequipment">
								<option <cfif getouttage.equipmentid is getequipment.equipmentid>selected</cfif> value="#equipmentID#">#equipmentName#</option>
							</cfoutput>
						</select>
					</td>
				</tr>
				<tr>
					<td nowrap>Cause:</td>
					<td nowrap>
						<select name="causeid">
							<option value="0"></option>
							<cfoutput query="getcauses">
								<option <cfif getouttage.causeid is getcauses.causeid>selected</cfif> value="#causeid#">#causename#</option>
							</cfoutput>
						</select>
					</td>
				</tr>
				<tr>
					<td colspan="2" nowrap>Description of Problem: </td>
				</tr>
				<tr>
					<td colspan="2" nowrap>
						<textarea name="description" rows="5" style="width:550px"><cfoutput>#getouttage.description#</cfoutput></textarea>
					</td>
				</tr>
				<tr>
					<td nowrap>Started: </td>
					<td nowrap class="nopadding">
						<table border="0" cellpadding="5" cellspacing="0" class="grey">
							<tr>
								<td class="nopadding">
									<table border="0" cellpadding="5" cellspacing="0" class="grey">
										<cfoutput>
											<tr>
												<td>
													<input name="dateStarted" type="text" style="width:75px" value="<cfoutput>#dateformat(getouttage.startTime, 'mm/dd/yyyy')#</cfoutput>" maxlength="10" />
												</td>
												<td><a style="text-decoration:none;" href="javascript:showCal('DateStarted');"> <img src="../../images/calicon.gif" alt="Select a Date" align="absmiddle" width="20" height="20" border="0" /></a></td>
												<td>
													<input type="text" style="width:25px" name="startTimeHH" value="#timeformat(getouttage.startTime,'hh')#" onFocus="this.select();" onKeyUp="if (this.value.charAt(this.value.length-1) == ':') {this.value = this.value.slice(0, this.value.length - 1); this.form.startTimeMM.focus();}" />
												</td>
												<td class="nopadding">:</td>
												<td>
													<input type="text" style="width:25px" name="startTimeMM" value="#timeformat(getouttage.startTime,'mm')#" onFocus="this.select();" />
												</td>
												<td class="nopadding">:</td>
												<td>
													<input type="text" style="width:25px" name="startTimeSS" value="#timeformat(getouttage.startTime,'ss')#" onFocus="this.select();" />
												</td>
												<td>
													<select name="startTimeTT">
														<option value=""></option>
														<option <cfif timeformat(getouttage.startTime,'TT') is "AM">selected</cfif> value="AM">AM</option>
														<option <cfif timeformat(getouttage.startTime,'TT') is "PM">selected</cfif> value="PM">PM</option>
													</select>
												</td>
											</tr>
										</cfoutput>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td nowrap>Ended:</td>
					<td nowrap class="nopadding">
						<table border="0" cellpadding="5" cellspacing="0" class="grey">
							<tr>
								<td class="nopadding">
									<table border="0" cellpadding="5" cellspacing="0" class="grey">
										<cfoutput>
											<tr>
												<td>
													<input name="dateEnded" type="text" style="width:75px" value="#dateformat(getouttage.endTime, 'mm/dd/yyyy')#" maxlength="10" />
												</td>
												<td><a style="text-decoration:none;" href="javascript:showCal('DateEnded');"> <img src="../../images/calicon.gif" alt="Select a Date" align="absmiddle" width="20" height="20" border="0" /></a></td>
												<td>
													<input type="text" style="width:25px" name="endTimeHH" value="#timeformat(getouttage.endTime,'hh')#" onFocus="this.select();" onKeyUp="if (this.value.charAt(this.value.length-1) == ':') {this.value = this.value.slice(0, this.value.length - 1); this.form.endTimeMM.focus();}" />
												</td>
												<td class="nopadding">:</td>
												<td>
													<input type="text" style="width:25px" name="endTimeMM" value="#timeformat(getouttage.endTime,'mm')#" onFocus="this.select();" />
												</td>
												<td class="nopadding">:</td>
												<td>
													<input type="text" style="width:25px" name="endTimeSS" value="#timeformat(getouttage.endTime,'ss')#" onFocus="this.select();" />
												</td>
												<td>
													<select name="endTimeTT">
														<option value=""></option>
														<option <cfif timeformat(getouttage.endTime,'TT') is "AM">selected</cfif> value="AM">AM</option>
														<option <cfif timeformat(getouttage.endTime,'TT') is "PM">selected</cfif> value="PM">PM</option>
													</select>
												</td>
											</tr>
										</cfoutput>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td nowrap>This outage was: </td>
					<td nowrap>
						<table border="0" cellspacing="0" cellpadding="5" class="grey">
							<tr>
								<td>
									<input <cfif getouttage.planned is 1>checked</cfif> name="planned" type="radio" value="1" />
								</td>
								<td>Planned</td>
								<td>&nbsp;&nbsp;</td>
								<td>
									<input <cfif getouttage.planned is 0>checked</cfif> name="planned" type="radio" value="0" />
								</td>
								<td>Un-Planned</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2">
					<input type="button" onclick="return checkForm(this.form);" class="sidebar" value="Save Outage Information">
					<input type="hidden" name="btnUpdateOuttage" value="0" />
					&nbsp;
					<input type="button" class="sidebar" value="Cancel &amp; Exit" onclick="document.location = 'index.cfm';">
					</td>
				</tr>
			</table>
		</td>
	</tr>
	</form>
</table>
</div>

