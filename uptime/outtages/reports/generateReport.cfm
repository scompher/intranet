<cffunction name="formatTime">
	<cfargument name="timeToFormat" type="numeric" required="yes">
	
		<cfset timeVal = timeToFormat>
		<cfset hours = int(timeval / 3600)>
		<cfif hours gt 0><cfset timeval = timeval - (hours * 3600)></cfif>
		<cfset mins = int(timeval / 60)>
		<cfif mins gt 0><cfset timeval = timeval - (mins * 60)></cfif>
		<cfset seconds = timeval>

		<cfset formattedTime = "#numberformat(hours,"00")#:#numberformat(mins,"00")#:#numberformat(seconds,"00")#">

	<cfreturn formattedTime>
</cffunction>

<style type="text/css">
	.linedRow {border-bottom:1px solid #000000;}
	TABLE {font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000000}
</style>

<cfset sd = createodbcdate(sd)>
<cfset ed = createodbcdate(ed)>

<cfset toDate = ed>
<!--- <cfset ed = dateadd("d",1,ed)> --->

<cfset compareDate = dateformat(now(),'mm/dd/yyyy')>
<cfif datecompare(compareDate,'05/01/2012') is 1>
	<cfset start = dateadd('yyyy',-1,compareDate)>
<cfelse>
	<cfset start = '05/01/2011'>
</cfif>
<cfset start = createodbcdatetime(start)>

<cfquery name="getInfo" datasource="#ds#">
	select 
	uptime_equipment_categories.equipmentCategory, 
	uptime_equipment.equipmentID, 
	uptime_equipment.equipmentName, 
	uptime_outtages.* 
	from uptime_equipment 
	left join uptime_outtages on uptime_equipment.equipmentid = uptime_outtages.equipmentid
	left join uptime_equipment_categories on uptime_equipment.equipmentCategoryID = uptime_equipment_categories.equipmentCategoryid
	order by equipmentCategory, equipmentName, startTime 
</cfquery>

<cfquery name="getItemsWithDowntime" datasource="#ds#">
	select outtageid from uptime_outtages
	where startTime >= #sd# and endTime < #ed# 
	order by outtageid asc 	
</cfquery>
<cfset outtagelist = valuelist(getItemsWithDowntime.outtageid)>

<cfquery name="getAfterHoursCalls" datasource="#ds#">
	select count(incidentid) as totalIncidents
	from callform_incidents
	where opened >= #sd# and opened < #ed#
</cfquery>
<!--- 
<cfquery name="gettime" datasource="#ds#">
	select timesheet_main.entryDate, timesheet_main.starttime, timesheet_main.hours, admin_users.adminuserid, admin_users.firstname, admin_users.lastname, timesheet_actions.actionName
	from timesheet_main
	inner join admin_users on timesheet_main.adminuserid = admin_users.adminuserid
	inner join timesheet_actions on timesheet_main.actionid = timesheet_actions.actionid
	where 
	timesheet_main.adminuserid IN (16,17) and 
	(timesheet_main.entryDate >= #sd# and timesheet_main.entryDate < #ed#) and 
	timesheet_main.actionid IN (106,107)
	order by lastname, firstname, entrydate, starttime
</cfquery>
 --->
<cfset callHrs = 0.0>
<cfset emailHrs = 0.0>

<cfset totalCalls = 0>
<cfset totalEmails = 0>

<cfset yearEnd = dateformat(now(),'mm/dd/yyyy')>

<cfset totalPeriodSeconds = dateDiff("s",sd,toDate)>
<cfif totalPeriodSeconds is 0>
	<cfset totalPeriodSeconds = 86400>
</cfif>
<cfset totalYearlySeconds = dateDiff("s",start,yearEnd)>

<!--- 
<cfoutput>
#formatTime(totalYearlySeconds - totalPeriodSeconds)#
</cfoutput>
--->

<div align="center">
<table width="600" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td colspan="4" align="center"><b>C.O.P.S. MONITORING EQUIPMENT UPTIME REPORT</b></td>
	</tr>
	<tr><td colspan="4">&nbsp;</td></tr>
	<tr>
		<td colspan="4" align="center">Yearly reporting starts on <cfoutput>#dateformat(start,'mm/dd/yyyy')#</cfoutput></td>
	</tr>
	<tr><td colspan="4">&nbsp;</td></tr>
	<cfoutput>
	<tr>
		<td colspan="4" align="center"><b>Uptime report from #dateformat(sd, 'mm/dd/yyyy')# to #dateformat(toDate, 'mm/dd/yyyy')#</b></td>
	</tr>
	</cfoutput>
	<tr>
		<td colspan="4">&nbsp;</td>
	</tr>
	<cfoutput>
	<tr>
		<td colspan="4"><b>Total after hours calls between #dateformat(sd, 'mm/dd/yyyy')# and #dateformat(toDate, 'mm/dd/yyyy')#: #getAfterHoursCalls.totalIncidents#</b></td>
	</tr>
	</cfoutput>
	<tr>
		<td colspan="4">&nbsp;</td>
	</tr>
	<!--- removed on 2/26/2008 per Jim McMullen
	<cfif gettime.recordcount gt 0>
		<tr>
			<td colspan="3">
			<cfoutput><b>Support Information between #dateformat(sd,'mm/dd/yyyy')# and #dateformat(toDate, 'mm/dd/yyyy')# for:</b><br /></cfoutput>
			<br />
			<cfoutput query="gettime" group="adminuserid">
				<cfset callHrs = 0.0>
				<cfset totalCalls = 0.0>
				<b>#firstname# #lastname#</b><br />
				<cfoutput>
					<cfif actionName is "Support Call">
						<cfset callHrs = callHrs + hours>
						<cfset totalCalls = totalCalls + 1>
					<cfelseif actionName is "Support Email">
						<cfset emailHrs = emailHrs + hours>
						<cfset totalEmails = totalEmails + 1>
					</cfif>
				</cfoutput>
				Total Support Calls: #totalCalls#<br />
				Total Support Call Time: #numberFormat(callHrs,"00.00")# Hours<br />
				<br />
				Total Support Emails: #totalEmails#<br />
				Total Support Email Time: #numberFormat(emailHrs,"00.00")# Hours<br />
				<br />
			</cfoutput>
			</td>
		</tr>
		<tr><td colspan="3">&nbsp;</td></tr>
	</cfif>
	--->
	<tr>
		<td valign="bottom"><b>System</b></td>
		<td align="center" valign="bottom"><b>Period Time Down<br />
			HH:MM:SS</b></td>
		<td align="center" valign="bottom"><b>Period %</b></td>
		<td align="center" valign="bottom"><b>Yearly %</b></td>
	</tr>
	<cfoutput query="getinfo" group="equipmentCategory">
		<tr>
			<td class="linedRow" colspan="4"><br /><b>#equipmentCategory#</b></td>
		</tr>
		<cfoutput group="equipmentid">
			<cfset totalPeriodSecondsDown = 0>
			<cfset totalYearlySecondsDown = 0>
			<cfoutput>
				<cfif trim(getinfo.outtageid) is not "">
					<cfset secondsDown = datediff("s",getinfo.startTime,getinfo.endTime)>
					<cfif listfindnocase(outtagelist, getinfo.outtageid) is not 0>
						<cfset totalPeriodSecondsDown = totalPeriodSecondsDown + secondsDown>
						<cfset totalYearlySecondsDown = totalYearlySecondsDown + secondsDown>
					<cfelse>
						<cfif datecompare(startTime, start) is 1>
							<cfset totalYearlySecondsDown = totalYearlySecondsDown + secondsDown>
						</cfif>
					</cfif>
				</cfif>
			</cfoutput>
			<cfset periodSecondsUp = totalPeriodSeconds - totalPeriodSecondsDown>
			<cfset periodUpTime = periodSecondsUp / totalPeriodSeconds>
			<cfset periodUpTime = periodUpTime * 100>
			<cfset yearlySecondsUp = totalYearlySeconds - totalYearlySecondsDown>
			<cfset yearlyUpTime = yearlySecondsUp / totalYearlySeconds>
			<cfset yearlyUpTime = yearlyUpTime * 100>
			<tr>
				<td>#equipmentName#</td>
				<td align="center">#formatTime(totalPeriodSecondsDown)#</td>
				<td align="center" nowrap="nowrap">#numberformat(periodUpTime,"000.000")#%</td>
				<td align="center" nowrap="nowrap">#numberformat(yearlyUpTime,"000.000")#%</td>
			</tr>
		</cfoutput>
	</cfoutput>
	<!--- <cfinclude template="/troubleTickets/reports/calcUptime.cfm"> --->
</table>
</div>

