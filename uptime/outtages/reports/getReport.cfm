
<cfparam name="form.format" default="">

<cfif form.format is "PDF">
	<cfdocument format="pdf" orientation="portrait" scale="75">
	<cfinclude template="/uptime/outtages/reports/generateReport.cfm">
	</cfdocument>
<cfelse>
	<cfinclude template="/uptime/outtages/reports/generateReport.cfm">
</cfif>
