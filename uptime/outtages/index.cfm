<link rel="stylesheet" type="text/css" href="../../styles.css">

<cfquery name="getOuttages" datasource="#ds#">
	select 
	uptime_equipment.equipmentName, 
	uptime_causes.causeName, 
	uptime_outtages.*   
	from uptime_outtages
	inner join uptime_causes on uptime_outtages.causeid = uptime_causes.causeid
	inner join uptime_equipment on uptime_outtages.equipmentid = uptime_equipment.equipmentid
	where uptime_outtages.starttime >= '1/1/#dateformat(now(),"yyyy")#'
	order by uptime_outtages.starttime desc
</cfquery>

<div align="center">
<table border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Equipment Outages</b></td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
			<table border="0" cellspacing="0" cellpadding="5" class="grey">
				<form method="post" action="index.cfm">
				<tr>
					<td align="center">
						<input type="button" class="sidebar" value="Add Outage" onclick="document.location='add.cfm';" />
					</td>
					<td colspan="5" align="center">&nbsp;</td>
				</tr>
				</form>
				<tr>
					<td align="center" valign="bottom" nowrap="nowrap"><b>Action</b></td>
					<td align="center" valign="bottom" nowrap="nowrap"><b>Start</b></td>
					<td align="center" valign="bottom" nowrap="nowrap"><b>End</b></td>
					<td align="center" valign="bottom" nowrap="nowrap"><b>Hours<br />Unavailable </b></td>
					<td valign="bottom" nowrap="nowrap"><b>Equipment</b></td>
					<td valign="bottom" nowrap="nowrap"><b>Cause</b></td>
				</tr>
				<cfoutput query="getOuttages">
				<tr>
					<td align="center">
					<a href="edit.cfm?oid=#outtageid#"><img src="../../images/edit.gif" alt="Edit" width="16" height="16" border="0" /></a>
					&nbsp;&nbsp;
					<a onclick="return confirm('Are you sure you wish to delete this outtage?');" href="delete.cfm?oid=#outtageid#"><img src="../../images/delete.gif" alt="Delete" width="16" height="16" border="0" /></a>
					</td>
					<td align="center" nowrap="nowrap">#dateformat(startTime, 'mm/dd/yyyy')# #timeformat(startTime, 'hh:mm:ss tt')#</td>
					<td align="center" nowrap="nowrap">#dateformat(endTime, 'mm/dd/yyyy')# #timeformat(endTime, 'hh:mm:ss tt')#</td>
					<td align="center">#numberformat(hrsUnavailable, '00.00')#</td>
					<td>#equipmentName#</td>
					<td>#causeName#</td>
				</tr>
				</cfoutput>
				<form method="post" action="index.cfm">
				<tr>
					<td align="center">
						<input type="button" class="sidebar" value="Add Outage" onclick="document.location='add.cfm';" />
					</td>
					<td colspan="5" align="center">&nbsp;</td>
				</tr>
				</form>
			</table>
		</td>
	</tr>
</table>
<br />
<a class="normal" style="text-decoration:underline;" href="/index.cfm">Return to Intranet Menu</a>
</div>
