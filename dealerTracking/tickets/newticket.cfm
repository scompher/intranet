<cfinclude template="/jqueryincludes.cfm">
<cfinclude template="/auth.cfm">
  <script>
  $(function() {
    $( "#ticketDate" ).datepicker();
  });
   $(function() {
    $( "#followUpDate" ).datepicker();
  });
  $(document).ready(function(){

	$("#spokeToDealer").click(function() {
		$("#showSpokeTo").show();
	});
	
	$("#emailconvo").click(function() {
		$("#showSpokeTo").show();
	});
	
	$("#leftmessage").click(function() {
		$("#showSpokeTo").hide();
	});	
  });
</script>
<cfparam name="err" default="">
<cfparam name="dealerNum" default="">
<cfparam name="dealerName" default="">
<cfparam name="issues" default="0">
<cfparam name="issueComments" default="">
<cfparam name="contactDate" default="">
<cfparam name="followUpDate" default="">
<cfparam name="openissue" default="">
<cfparam name="saveMessage" default="">

<cfif adminlogin is '182'>
<!--- get grouped dealers --->
	<cfxml variable="xmlRequest">
	<cfoutput>
	<request>
		<command>getdealerlist</command>
		<data>
			<dealerNumber>#dealerNum#</dealerNumber>
		</data>
	</request>
	</cfoutput>
	</cfxml>
	<cf_copalink command="#xmlRequest#">
	<cfset xResult = xmlparse(result)>
	<cfif xResult.response.result.xmlText is not 'RJ'>
		<cfset data = xResult.response.data>
		<cfset dealerlist = ''>
		<cfif isDefined("data.item")>
			<cfloop from="1" to="#arraylen(data.item)#" index="i"><br />
				<cfset currentdealer = data.item[i]>
				<cfset dealerNumber = currentDealer.custID.xmlText>
				<cfset dealerlist = listappend(dealerlist,"'"&dealerNumber&"'")>
			</cfloop>
		<cfelse>
			<cfset dealerlist = "'"&#dealerNum#&"'">
		</cfif>
	</cfif>
<cfelse>
<cfset dealerlist = "'"&#dealerNum#&"'">
</cfif>
<!--- get open ticket --->
<cfquery name="getOpenTicket" datasource="#ds#">
	select top 1 *
	from dealerdiary_openTicket
	where dealernum in (#PreserveSingleQuotes(dealerlist)#)
</cfquery>
	<cfquery name="getComments" datasource="#DS#">
		select *
		from dealerdiary_comments
		where dealernum in (#PreserveSingleQuotes(dealerlist)#) and active = 1
		order by commentid asc
	</cfquery>
<cfif getOpenTicket.recordcount gt 0>
	<cfset contactDate = dateformat(getOpenTicket.contactDate,'MM/DD/YYYY')>
	<cfset followUpDate = dateformat(getOpenTicket.followUp,'MM/DD/YYYY')>
	<cfset openissue = 1>
</cfif>

<cfquery name="getCategories" datasource="#DS#">
	select *
	from dealerdiary_contactCategories
	order by category desc
</cfquery>
<cfif isDefined("form.btnSaveTicket")>
<!--- check errors --->
	<cfif form.category is 0>
		<cfset err='Please select a category'>
	<cfelse>
		<cfset err = ''>
	</cfif>
	<cfif err is ''>

	<!--- open issues ticket --->
		<cfif form.issues is 1>
		<!--- save the ticket --->
			<cfquery name="saveTicket" datasource="#DS#">
				insert into dealerdiary_openTicket(
				dealernum,
				dealername,
				contactDate,
				caller,
				reason,
				calldetails,
				spoketo,
				openIssues,
				followUp)
				values (
				'#form.dealerNum#',
				'#form.dealerName#',
				'#form.ticketDate#',
				'#form.caller#',
				'#form.category#',
				'#calldetails#',
				'#form.spokeToName#',
				#form.issues#,
				'#form.followUpDate#')
			</cfquery>
			<!--- save the comment --->
			<cfif form.issueComments is not ''>
				<cfif form.callDetails is 'message'>
					<cfset personOrMessage = 'Left Message  '>
					<cfelse>
					<cfif form.spokeToName is ''>
						<cfset form.spokeToName = 'Name not entered'>
					</cfif>
					<cfif form.callDetails is 'spoke'>
						<cfset personOrMessage = 'Spoke To:  ' & form.spokeToName>
					<cfelse>
						<cfset personOrMessage = 'Emailed:  '& form.spokeToName>
					</cfif>
				</cfif>
				<cfset saveComment = personOrMessage & ' - ' & form.issueComments>
			<cfelse>
				<cfset saveComment = ''>
			</cfif>
			<cfquery name="saveComment" datasource="#ds#">
				insert into dealerdiary_comments (
				dealernum,
				comment,
				date,
				active)
				values (
				'#form.dealerNum#',
				'#saveComment#',
				'#form.ticketDate#',
				1)
			</cfquery>
			
			<!--- update dealer diary to show issues --->
			<cfquery name="dealerDiaryIssue" datasource="#ds#">
				update dealerdiary_dealers
				set openIssues = 1, lastContact = '#form.ticketDate#',
				<cfif form.callDetails is 'spoke'>
				succesfulContact = 1
				<cfelse>
				succesfulContact = 0 
				</cfif>
				, followUp = '#form.followUpDate#'
				where dealernumber in (#PreserveSingleQuotes(dealerlist)#)
			</cfquery>
			<cfset saveMessage = " - Ticket saved as Open Ticket!!">
		</cfif>
		
		<!--- save to dealer diary --->
		<cfif form.issues is 0>
			<cfif form.issueComments is not ''>
				<cfif form.callDetails is 'message'>
					<cfset personOrMessage = 'Left Message  '>
				<cfelse>
					<cfset personOrMessage = 'Spoke To:  ' & form.spokeToName>
				</cfif>
				<cfset saveComment = personOrMessage & ' - ' & form.issueComments>
			<cfelse>
				<cfset saveComment = ''>
			</cfif>
			<cfquery name="saveComment" datasource="#ds#">
				insert into dealerdiary_comments (
				dealernum,
				comment,
				date,
				active)
				values (
				'#form.dealerNum#',
				'#saveComment#',
				'#form.ticketDate#',
				1)
			</cfquery>
			<cfset currDateTime = createodbcdatetime(now())>
					<cfquery name="dealerDiaryEntry" datasource="#ds#">
						insert into dealerdiary_entries (
						dealernumber,
						creatorid,
						created,
						trackerComments,
						departmentid,
						subject,
						parentid
						)
						values (
						'#dealerNum#',
						'#adminlogin#',
						#currDateTime#,
						1,
						'#getsec.defaultdeptid#',
						'#form.category#',
						0)
					</cfquery> 
				<cfquery name = "getId" datasource="#ds#">
					select max(entryid) as dealerdiaryid
					from dealerdiary_entries
					where dealernumber = '#dealernum#' and creatorid = '#adminlogin#'
				</cfquery>
				<cfquery name="dealerDiaryNoIssue" datasource="#ds#">
					update dealerdiary_dealers
					set openIssues = 0, lastContact = '#form.ticketDate#', followUp = NULL,
					<cfif form.callDetails is 'spoke'>
						succesfulContact = 1
					<cfelse>
						succesfulContact = 0
					</cfif>
					where dealernumber in (#PreserveSingleQuotes(dealerlist)#)
				</cfquery>
				<cfquery name="updateComments" datasource="#ds#">
					update dealerdiary_comments
					set active = '0', dealerdiaryid = '#getId.dealerdiaryid#'
					where active = '1' and dealernum = '#form.dealerNum#'
				</cfquery>
			<cfset saveMessage = " - Ticket saved in Dealer Diary!!">
		</cfif>
				<div align="center">
					<table width="600px" border="0" cellspacing="0" cellpadding="5">
						<tr>
							<td class="highlightbar"><b>Dealer Ticket</b> </td>
						</tr>
						<tr>
							<td class="greyrowbottomnopadding">
								<table width="100%" border="0" cellspacing="0" cellpadding="5">
									<tr>
										<td style="color:#FF0000;"><b><cfoutput><cfif saveMessage is not ''>#saveMessage#</cfif></cfoutput></b></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					<br />
					<a href="JavaScript:window.close()" style="text-decoration:underline;" class="normal">Close Window</a>
				</div>
	</cfif>
</cfif>
<cfif saveMessage is ''>
	<div align="center">
		<table width="600px" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td class="highlightbar"><b>Dealer Ticket</b> </td>
			</tr>
			<tr>
				<td class="greyrowbottomnopadding">
					<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<cfoutput>
					<form name="saveTicket" method="post" action="newticket.cfm">
						<input type="hidden" style="width:100px" name="dealerNum" id="dealerNum" value="#dealerNum#">
						<input type="hidden" style="width:400px" name="dealerName" id="dealerName" value="#dealerName#">
						<tr>
							<td>Dealer Number:</td>
							<td><input style="width:100px" disabled="disabled" value="#dealerNum#"></td>
						</tr>
						<tr>
							<td>Dealer Name:</td>
							<td><input style="width:400px" disabled="disabled" value="#dealerName#"></td>
						</tr>
						<tr>
							<td>Contact Date:</td>
							<td><input type="text" id="ticketDate" name="ticketDate" value="#contactDate#"></td>
						</tr>
						<tr>
							<td>Caller:</td>
							<td><input type="text" id="caller" name="caller" value="#getsec.firstname# #getsec.lastname#" /></td>
						</tr>
						<tr>
							<td>Reason For Call:</td>
							<td>
								<select id="category" name="category">
									<option value="0">Please Select</option>
									<cfloop query="getCategories">
									<option value="#getCategories.category#">#getCategories.category#</option>
									</cfloop>
								</select>
							<cfif err is not ''>
								<span style="color:##FF0000;">#err#</span>
							</cfif>
							</td>
						</tr>
						<tr>
							<td>Call Details:</td>
							<td><label style="vertical-align:top;">Spoke to Dealer</label><input type="radio" checked="checked" name="calldetails" id="spokeToDealer" value="spoke"/>&nbsp;&nbsp;&nbsp;
							<label style="vertical-align:top;">Left Message </label><input type="radio" name="calldetails" id="leftmessage" value="message" />&nbsp;&nbsp;&nbsp;
							<label style="vertical-align:top;">Email </label><input type="radio" name="calldetails" id="emailconvo" value="email" /> </td>
						</tr>
						<tr id="showSpokeTo">
							<td>Spoke To/Emailed:</td>
							<td><input type="text" style="width:400px;" name="spokeToName" id="spokeToName" value="" /></td>
						</tr>
						<tr>
							<td>Any Open Issues?:</td>
							<td><label style="vertical-align:top;">No</label><input type="radio" checked="checked" name="issues" value="0" id="noissues" />&nbsp;&nbsp;&nbsp;
							<label style="vertical-align:top;">Yes</label><input type="radio" name="issues" value="1" id="yesissues" /></td>
						</tr>
						<cfif getComments.recordcount gt 0>
							<tr><td>Previous Comments:</td></tr>
							<cfloop query="getComments">
							<cfset commentDate = dateformat('#getComments.date#','MM/DD/YYYY')>
							<tr><td></td><td>#commentDate#</td></tr>
							<tr><td></td><td>#replace(getComments.comment,"#chr(10)#","<br>","all")#</td></tr>
							</cfloop>
						</cfif>
						<tr id="showIssueComments">
							<td>Comments:</td>
							<td><textarea name="issueComments" style="width:400px;" id="issueComments"></textarea></td>
						</tr>
						<tr>
							<td>Scheduled Follow Up:</td>
							<td><input type="text" id="followUpDate" name="followUpDate" value="#followUpDate#"></td>
						</tr>
						<tr>
							<td><input name="btnSaveTicket" type="submit" class="sidebar" id="btnSaveTicket" value="Save Ticket" /></td>
						</tr>
					</form>
					</cfoutput>
				</table>
			</td>
		</tr>
	</table>
	<br />
	<a href="JavaScript:window.close()" style="text-decoration:underline;" class="normal">Close Window</a>
</div>
</cfif>




