<cfinclude template="/jqueryincludes.cfm">
<cfif isDefined("form.btnSaveCategory")>
	<cfquery name="getCategory" datasource="#ds#">
		select * from dealerdiary_contactCategories
		where category = '#form.categoryName#'
	</cfquery>
	<cfif getCategory.recordcount gt 0>
		<cfquery name="updateCategory" datasource="#ds#">
			update dealerdiary_contactCategories
			set active = 1
			where category='#form.categoryName#'
		</cfquery>
		<cfelse>
			<cfquery name="saveCategory" datasource="#ds#">
				insert into dealerdiary_contactCategories(category,active)
				values('#form.categoryName#',1)
			</cfquery>
	</cfif>
</cfif>
<cfquery name="getCategories" datasource="#DS#">
	select *
	from dealerdiary_contactCategories
	where active = 1
	order by category desc
</cfquery>



<div align="center" style="width:100%">
<table border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Dealer Tracking - Add Contact Category</b></td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
		<form name="addCategory" action="manageCategory.cfm" method="post" id="addCategory">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td>Contact Category:</td>
					<td><input type="text" maxlength="150" id="categoryName" name="categoryName"/></td>
				</tr>
				<tr>
					<td><input name="btnSaveCategory" type="submit" class="sidebar" id="btnSaveCategory" value="Save Category" /></td>
				</tr>
			</table>
		</form>
		</td>
	</tr>
</table>
<br>
<br>
<br>

<table border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Dealer Tracking - Categories</b> </td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
		<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<cfoutput query="getCategories">
			<tr>
				<td><a onclick = "return confirm('Are you sure you wish to delete this item?');" href='deleteCategory.cfm?categoryID=#getCategories.categoryid#'><img src='/images/delete.gif'></img></a></td>
				<td>#getCategories.category#</td>
			</tr>
			</cfoutput>
		</table>
		</td>
	</tr>
</table>
<br>
<br>
<a href="/index.cfm" style="text-decoration:underline;" class="normal">Return to Intranet</a>
</div>
