<cfsetting showdebugoutput="no">
<cfheader name="Content-Type" value="application/json">

<cfquery name="getEmail" datasource="intranet">
	select * from Admin_Users
	where adminuserid = '#id#'
</cfquery>
<cfquery name="getDealers" datasource="intranet">
	select * 
	from dealerdiary_dealers
	<cfif getEmail.email is 'hsparks@copsmonitoring.com'>
	where openIssues = 1 or followUp is not null or manageremail = '#getEmail.email#'
	<cfelse>
	where manageremail = '#getEmail.email#'
	</cfif>
	order by dealerName asc
</cfquery>
<cfset totalitems = "">
<cfloop query="getDealers">
	<cfif getDealers.openIssues is 1>
		<cfset issue = 'Yes'>
	<cfelse>
		<cfset issue= 'No'>
	</cfif>
	<cfif getDealers.succesfulContact is 1>
		<cfset successfulLastContact = 'Yes'>
	<cfelse>
		<cfset successfulLastContact = 'No'>
	</cfif>
	<cfif getDealers.followUp is not '' and getDealers.followUp is not '1900-01-01 00:00:00.000'>
		<cfset followUpDate = DateFormat(getDealers.followUp,'MM/DD/YYYY')>
		<cfelse>
		<cfset followUpDate = ''>
	</cfif>
	<cfset lastContactDate = DateFormat(getDealers.lastContact,'MM/DD/YYYY')>
	<cfset totalitems = listappend(totalitems,"
		{
		""dealerName"": ""#getDealers.dealername#"",
		""dealerNumber"": ""#getDealers.dealernumber#"",
		""dealerID"": ""#getDealers.dealerid#"",
		""primaryPhone"" : ""#getDealers.primaryphone#"",
		""acctManager"" : ""#getDealers.acctmanager#"",
		""contactName"" : ""#getDealers.contact#"",
		""numAccounts"" : ""#getDealers.numAccounts#"",
		""issue"" : ""#issue#"",
		""masterDealer"" : ""#getDealers.masterDealer#"",
		""lastContact"" : ""#lastContactDate#"",
		""successfulContact"" : ""#successfulLastContact#"",
		""followUp"" : ""#followUpDate#""
		}")>
</cfloop>
<cfset totalitems = replace("#totalitems#","\","/","all")>
<cfoutput>[#totalitems#]</cfoutput>
