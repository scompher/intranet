<link rel="stylesheet" href="/jqwidgets/jqwidgets/styles/jqx.base.css" type="text/css" />
<link rel="stylesheet" href="/jquery-ui-1.11.4/jquery-ui.min.css" />
<link rel="stylesheet" href="/jquery-ui-1.11.4/jquery-ui.theme.min.css"/>
<link rel="stylesheet" type="text/css" href="/styles.css">
<script type="text/javascript" src="/jqwidgets/scripts/jquery-1.11.1.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js"></script>
<script type="text/javascript" src="/jqwidgets/jqwidgets/jqxcore.js"></script>
<script type="text/javascript" src="/jqwidgets/jqwidgets/jqxdata.js"></script> 
<script type="text/javascript" src="/jqwidgets/jqwidgets/jqxbuttons.js"></script>
<script type="text/javascript" src="/jqwidgets/jqwidgets/jqxscrollbar.js"></script>
<script type="text/javascript" src="/jqwidgets/jqwidgets/jqxmenu.js"></script>
<script type="text/javascript" src="/jqwidgets/jqwidgets/jqxcheckbox.js"></script>
<script type="text/javascript" src="/jqwidgets/jqwidgets/jqxlistbox.js"></script>
<script type="text/javascript" src="/jqwidgets/jqwidgets/jqxdropdownlist.js"></script>
<script type="text/javascript" src="/jqwidgets/jqwidgets/jqxgrid.js"></script>
<script type="text/javascript" src="/jqwidgets/jqwidgets/jqxgrid.sort.js"></script> 
<script type="text/javascript" src="/jqwidgets/jqwidgets/jqxgrid.pager.js"></script> 
<script type="text/javascript" src="/jqwidgets/jqwidgets/jqxgrid.selection.js"></script> 
<script type="text/javascript" src="/jqwidgets/jqwidgets/jqxgrid.edit.js"></script>
<script type="text/javascript" src="/jqwidgets/jqwidgets/jqxgrid.columnsresize.js"></script>
<script type="text/javascript" src="/jqwidgets/jqwidgets/jqxdata.export.js"></script> 
<script type="text/javascript" src="/jqwidgets/jqwidgets/jqxgrid.export.js"></script>
<script type="text/javascript" src="/jqwidgets/jqwidgets/jqxgrid.filter.js"></script>
<script type="text/javascript" src="/jqwidgets/jqwidgets/jqxgrid.columnsreorder.js"></script>
<script type="text/javascript" src="/dealerTracking/functions.js"></script>
<br>
<br>

<div align="center">
<table border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Dealer Tracking</b> </td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
		<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td><div id="jqxgrid"></div></td>
			</tr>
		</table>
		</td>
	</tr>
</table>
<br>
<input type="button" value="Export to Excel" id='excelExport' />
<br />
<br>
<a href="/index.cfm" style="text-decoration:underline;" class="normal">Return to Intranet</a>
</div>

