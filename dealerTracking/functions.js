$(document).ready(function(){
						   
	//click to open new ticket			   
	$("#jqxgrid").bind('rowdoubleclick', function (event) {
		var row = event.args.rowindex;
		var data = $("#jqxgrid").jqxGrid('getrowdata', row);
		var w = 955;
		var h = 600;
		w += 32;
		h += 96;
		wleft = (screen.width - w) / 2;
		wtop = (screen.height - h) / 2;
		var managerWin = window.open("tickets/newticket.cfm?dealernum=" + data.dealerNumber +"&dealername=" + encodeURIComponent(data.dealerName),"whatisthis","height=600,width=955,resizable=yes,toolbar=no,left=" + wleft + ",top=" + wtop);
		managerWin.opener = self;
		managerWin.focus();
	});
		
	function getCookie(cname) {
		var name = cname + "=";
		var ca = document.cookie.split(';');
		for(var i=0; i<ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0)==' ') c = c.substring(1);
			if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
		}
		return "";
	}	
		
	//grid source mapping
	var adminid = getCookie("ADMINLOGIN");
	url='grid/loaddealers.cfm?id=' + adminid
	var source =
	{
		datatype: "json",
		datafields: [
			{ name: 'dealerNumber', type:'string'},
			{ name: 'dealerName', type:'string'},
			{ name: 'primaryPhone', type:'phone'},
			{ name: 'acctManager', type:'string'},
			{ name: 'contactName', type:'string'},
			{ name: 'numAccounts', type: 'int'},
			{ name: 'issue', type:'string'},
			{ name: 'masterDealer', type:'string'},
			{ name: 'lastContact' , type:'string'},
			{ name: 'successfulContact', type:'string' },
			{ name: 'followUp', type:'string' }
		],
		url: url,
		cache: false,
		id: 'id',
		//pagesize: 100,
		root: 'Rows'
//		filter: function () {
//		// update the grid and send a request to the server.
//		$("#jqxgrid").jqxGrid('updatebounddata', 'filter');
//		},
//		beforeprocessing: function(data) {
//			source.totalrecords = data[0].TotalRows;
//		}
	};
	
	//data adaptor for source
	var dataAdapter = new $.jqx.dataAdapter(source, {
		downloadComplete: function (data, status, xhr) { },
		loadComplete: function (data) { },  
		loadError: function (xhr, status, error) { }
	});	
	
	// the actual grid	
	$("#jqxgrid").jqxGrid(
	{
		width: '1200',
		height: '600',
		source: dataAdapter,
		showfilterrow: true,
		filterable: true,
		columnsreorder: true,
		columnsresize: true,
		sortable: true,
	//	pageable: true,
	//	virtualmode: true,
		pagermode: 'simple',
//		rendergridrows: function (params) {
//			return params.data;
//		},
		columns: [
			{ text: 'Dealer Number', datafield: 'dealerNumber', width:125, minwidth:125 },
			{ text: 'Dealer Name', datafield: 'dealerName', width:200, minwidth:200 },
			{ text: 'Master Dealer', datafield: 'masterDealer', width:125, minwidth:125 },
			{ text: 'Primary Phone', datafield: 'primaryPhone', width:125, minwidth: 125},
			//{ text: 'Account Manager', datafield: 'acctManager', width:200, minwidth:200 },
			{ text: 'Contact Name', datafield: 'contactName', width:200, minwidth:200 },
			{ text: '# of Accounts', datafield: 'numAccounts', width:120, minwidth:120 },	
			{ text: 'Last Contact', datafield: 'lastContact', width:125, minwidth:125 },
			{ text: 'Successful?', datafield: 'successfulContact', width:120, minwidth: 120 },
			{ text: 'Open Issue', datafield: 'issue', width:100, minwidth: 100 },
			{ text: 'Follow Up', datafield: 'followUp', width:125, minwidth: 125 }
			]
	});
	
//	//export options
	$("#excelExport").jqxButton();
//	$("#csvExport").jqxButton();
//	$("#pdfExport").jqxButton();
	$("#excelExport").click(function () {
		$("#jqxgrid").jqxGrid('exportdata', 'xls', 'jqxGrid');           
	});        
//	$("#csvExport").click(function () {
//		$("#jqxgrid").jqxGrid('exportdata', 'csv', 'jqxGrid');
//	});
//	$("#pdfExport").click(function () {
//		$("#jqxgrid").jqxGrid('exportdata', 'pdf', 'jqxGrid');
//	});
	
	////selectable column options
//	var listSource = [{ label: 'Account', value: 'account', checked: true },
//					  { label: 'Name', value: 'name', checked: true },
//					  { label: 'Premises Phones', value: 'premphones', checked: true },
//					  { label: 'Address', value: 'address', checked: true},
//					  { label: 'City', value: 'city', checked: true },
//					  { label: 'State', value: 'state', checked: true },
//					  { label: 'Zip Code', value: 'zipcode', checked: true },
//					  { label: 'Permit', value: 'permit', checked: true },
//					  { label: 'Dealer Number', value: 'dealernum', checked: true },
//					  { label: 'Virtual Account', value: 'virtual', checked: true },
//					  { label: 'Billing ID', value: 'billingid', checked: true },
//					  { label: 'RP Phones', value: 'rpphones', checked: true },
//					  { label: 'Police Phones', value: 'policephones', checked: true },
//					  { label: 'Fire Phones', value: 'firephones', checked: true },
//					  { label: 'Medical Phones', value: 'medicalphones', checked: true },
//					  { label: 'Auxiliary Phones', value: 'auxphones', checked: true },
//					  
//					  
//					 ];
//		$("#jqxlistbox").jqxDropDownList({ source: listSource, width: 200, height: 20,  checkboxes: true });
//		$("#jqxlistbox").on('checkChange', function (event) {
//			$("#jqxgrid").jqxGrid('beginupdate');
//			if (event.args.checked) {
//				$("#jqxgrid").jqxGrid('showcolumn', event.args.value);
//				 $("#jqxgrid").jqxGrid('autoresizecolumns');
//			}
//			else {
//				$("#jqxgrid").jqxGrid('hidecolumn', event.args.value);
//				 $("#jqxgrid").jqxGrid('autoresizecolumns');
//			}
//		$("#jqxgrid").jqxGrid('endupdate');
//	});

});