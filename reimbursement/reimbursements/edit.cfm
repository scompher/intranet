
<style type="text/css">
	.redstar {
	color: #FF0000;
	font-size: 14px;
}
</style>

<cfif isdefined("form.btnedit")>

	<cfset variables.datetimeentered = createodbcdatetime(now())>
	<cfif trim(form.datereimbursed) IS NOT "">
		<cfset variables.datereimbursed = createodbcdate(form.datereimbursed)>
	<cfelse>
		<cfset variables.datereimbursed = "NULL">
	</cfif>
	<cfif trim(form.receiptdate) IS NOT "">
		<cfset variables.receiptdate = createodbcdate(form.receiptdate)>
	<cfelse>
		<cfset variables.receiptdate = "NULL">
	</cfif>
	
	<cfset form.requestedamount = replace(form.requestedamount,"$","","all")>
	<cfset form.requestedamount = replace(form.requestedamount,",","","all")>
	
	<cfset form.reimbursementamount = replace(form.reimbursementamount,"$","","all")>
	<cfset form.reimbursementamount = replace(form.reimbursementamount,",","","all")>
	
	<cfquery name="insertreceipt" datasource="#ds#">
		update reimbursements_main
		set employeenum = '#form.employeenum#', datetimeentered = #variables.datetimeentered#, categoryid = #form.categoryid#, requestedamount = #form.requestedamount#, reimbursementamount = #form.reimbursementamount#, datereimbursed = #variables.datereimbursed#, rxnumber = '#form.rxnumber#', receiptdate = #variables.receiptdate#, notes = '#form.notes#'
		where reimbursementid = #id#
	</cfquery>

	<cflocation url="index.cfm">

<cfelse>

	<cfquery name="getcats" datasource="#ds#">
		select * from reimbursements_categories
		order by categoryid asc
	</cfquery>
	
	<cfquery name="getitem" datasource="#ds#">
		select reimbursements_main.*
		from reimbursements_main
		where reimbursementid = #id#
	</cfquery>
	
	<script language="javascript" src="../cal2.js">
	/*
	Xin's Popup calendar script-  Xin Yang (http://www.yxscripts.com/)
	Script featured on/available at http://www.dynamicdrive.com/
	This notice must stay intact for use
	*/
	</script>
	<script language="javascript" src="../cal_conf2.js"></script>
	
	<script language="JavaScript" type="text/JavaScript">
	function checkform(frm) {
		var currDate = new Date();

		if (frm.employeenum.value == "") {alert('The employee number is required.'); frm.employeenum.focus(); return false;}
		if (frm.categoryid.selectedIndex == 0) {alert('The category is required'); frm.categoryid.focus(); return false;}
		if (frm.requestedamount.value == "") {alert('The requested amount is required'); frm.requestedamount.focus(); return false;}
		if (frm.datereimbursed.value == "") {alert('The date reimbursed is required.'); frm.datereimbursed.focus(); return false;}
		if (frm.receiptdate.value == "") {alert('The receipt date is required.'); frm.receiptdate.focus(); return false;}
		if (frm.reimbursementamount.value == "") {alert('The reimbursment amount is required.'); frm.reimbursementamount.focus(); return false;}
		if (frm.datereimbursed.value < frm.receiptdate.value) {alert('The reimbursement date cannot be earlier than the receipt date.'); return false;}

		var checkdate = new Date(currDate.getFullYear(), currDate.getMonth(), currDate.getDate() + 13);
		var reimdate = new Date(frm.datereimbursed.value);
		if (reimdate > checkdate) {alert('The reimbursement date cannot be more than 13 days into the future.'); return false;}
		if (reimdate.getDay() != 5) {alert('The reimbursement date must be on a friday.'); return false;}

		var currDate = new Date();
		var backDate = new Date(currDate.getFullYear(), currDate.getMonth(), currDate.getDate() - 33);
		var receiptdate = new Date(frm.receiptdate.value);
		if (receiptdate < backDate || receiptdate > currDate) {alert('The receipt date may not be greater than today or more than 30 days prior.'); return false;}

		// check requested amount cap limits
		var catid = frm.categoryid.selectedIndex;
		switch (catid) {
			case 1: // doctor
				if (frm.requestedamount.value > 15) {alert('The requested amount cannot exceed $15.00'); frm.requestedamount.focus(); return false;}
				break;
			case 3: // generic rx
				if (frm.requestedamount.value > 10) {alert('The requested amount cannot exceed $10.00'); frm.requestedamount.focus(); return false;}
				break;
			case 4: // brand rx
				if (frm.requestedamount.value > 20) {alert('The requested amount cannot exceed $20.00'); frm.requestedamount.focus(); return false;}
				break;
			case 5: // Generic RX 3 months
				if (frm.requestedamount.value > 20) {alert('The requested amount cannot exceed $20.00'); frm.requestedamount.focus(); return false;}
				break;
			case 6: // Brand rx 3 months
				if (frm.requestedamount.value > 40) {alert('The requested amount cannot exceed $40.00'); frm.requestedamount.focus(); return false;}
				break;
		}

		frm.submit();
	}
	
	function calcAmt(frm) {
		var catid = frm.categoryid.selectedIndex;
		switch (catid) {
			case 1: // doctor
				// var result = frm.requestedamount.value - 10.00;
				if (frm.requestedamount.value >= 10) {
					var result = 10.00;
				} else {
					var result = parseFloat(frm.requestedamount.value);
				}
				frm.reimbursementamount.value = result.toFixed(2);
				break;
			case 2: // specialist
				// var result = frm.requestedamount.value - 10.00;
				var result = 10.00;
				frm.reimbursementamount.value = result.toFixed(2);
				break;
			case 3: // generic rx
				var result = frm.requestedamount.value / 2;
				frm.reimbursementamount.value = result.toFixed(2);
				break;
			case 4: // brand rx
				var result = frm.requestedamount.value / 2;
				frm.reimbursementamount.value = result.toFixed(2);
				break;
			case 5: // generic RX 3 months
				var result = frm.requestedamount.value / 2;
				frm.reimbursementamount.value = result.toFixed(2);
				break;
			case 6: // brand RX 3 months
				var result = frm.requestedamount.value / 2;
				frm.reimbursementamount.value = result.toFixed(2);
				break;
		}
		
		var currDate = new Date();
		if ((currDate.getDay() < 2) || (currDate.getDay() == 2 && currDate.getHours() < 17)) {
			var forwarddate = currDate;
		} else {
			var forwarddate = new Date(currDate.getFullYear(), currDate.getMonth(), currDate.getDate() + 7);
		}

		var tmpdate;

		do
		{
			tmpdate = new Date(forwarddate.getFullYear(), forwarddate.getMonth(), forwarddate.getDate() + 1);
			forwarddate = tmpdate;
		}
		while (forwarddate.getDay() < 5);
		var month = forwarddate.getMonth() + 1;
		frm.datereimbursed.value = month + "/" + forwarddate.getDate() + "/" + forwarddate.getFullYear();

	}
	</script>
	
	<div align="center" class="normal">
	<form method="post" action="edit.cfm">
	<cfoutput>
	<input type="hidden" name="id" value="#id#">
	</cfoutput>
	<table border="1" cellspacing="0" cellpadding="5">
		<tr>
			<td colspan="2" align="center" bgcolor="FFFFCC"><b>Edit A Receipt</b></td>
		</tr>
		<tr>
			<td colspan="2"><b>A red <span class="redstar">*</span> denotes a required field </b></td>
		</tr>
		<cfoutput query="getitem">
		<tr>
			<td><b>Employee Number: <span class="redstar">*</span></b></td>
			<td><input name="employeenum" type="text" class="normal" style="width:300px " value="#getitem.employeenum#"></td>
		</tr>
		</cfoutput>
		<tr>
			<td><b>Category:<span class="redstar">*</span></b></td>
			<td>
			<select name="categoryid" class="normal">
				<option value="0"></option>
			<cfoutput query="getcats">
				<option value="#getcats.categoryid#" <cfif getitem.categoryid IS getcats.categoryid>selected</cfif> >#getcats.category#</option>
			</cfoutput>
			</select>
			</td>
		</tr>
		<cfoutput query="getitem">
		<tr>
			<td><b>Receipt Date: <span class="redstar">*</span></b></td>
			<td><input name="receiptdate" type="text" class="normal" value="#dateformat(getitem.receiptdate,'mm/dd/yyyy')#">
				<a style="text-decoration:none;" href="javascript:showCal('ReceiptDate');"><img src="../../images/calicon.gif" width="20" height="20" border="0" align="absmiddle"></a>
			</td>
		</tr>
		<tr>
			<td><b>Requested Amount: <span class="redstar">*</span></b></td>
			<td>
			$<input name="requestedamount" type="text" class="normal" style="width:75px " value="#numberformat(getitem.requestedamount,'.00')#">&nbsp;&nbsp;
			<a class="small" href="javascript:calcAmt(document.forms[0]);">Calculate Reimbursement Amount</a>
			</td>
		</tr>
		<tr>
			<td><b>Date Reimbursed:<span class="redstar">*</span></b></td>
			<td>
			<input name="datereimbursed" type="text" class="normal" value="#dateformat(getitem.datereimbursed,'mm/dd/yyyy')#">
			<a style="text-decoration:none;" href="javascript:showCal('DateReimbursed');"><img src="../../images/calicon.gif" width="20" height="20" border="0" align="absmiddle"></a>
			</td>
		</tr>
		<tr>
			<td><b>RX Number: </b></td>
			<td><input name="rxnumber" type="text" class="normal" style="width:300px " value="#getitem.rxnumber#"></td>
		</tr>
		<tr>
			<td><b>Reimbursement Amount: <span class="redstar">*</span></b></td>
			<td>$<input name="reimbursementamount" type="text" class="normal" style="width:75px " value="#numberformat(getitem.reimbursementamount,'.00')#"></td>
		</tr>
		<tr>
			<td valign="top"><b>Notes</b></td>
		    <td><textarea name="notes" rows="5" style="width:300px " class="normal">#getitem.notes#</textarea></td>
		</tr>
		</cfoutput>
		<tr>
			<td colspan="2">
			<input type="hidden" name="btnedit" value="1">
			<input onClick="checkform(this.form);" type="button" value="Update Receipt">
			<input type="reset" value="Reset Form">
			</td>
		</tr>
	</table>
	</form>
	<a href="index.cfm">Return to reimbursements menu</a>
	</div>

</cfif>

