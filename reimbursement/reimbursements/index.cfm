
<cfquery name="getitems" datasource="#ds#">
	select reimbursements_main.*, reimbursements_categories.category
	from reimbursements_main
	inner join reimbursements_categories on reimbursements_main.categoryid = reimbursements_categories.categoryid
	order by reimbursements_main.datetimeentered asc
</cfquery>

<div align="center" class="normal">
<form method="post" action="add.cfm">
<table width="725" border="1" cellspacing="0" cellpadding="5">
	<tr align="center" bgcolor="DDDDDD">
		<td><b>Action</b></td>
		<td><b>Employee # </b></td>
		<td><b>Date Submitted </b></td>
		<td><b>Date Reimbursed</b></td>
		<td><b>Category</b></td>
		<td><b>Reimbursement Amount </b></td>
	</tr>
	<cfoutput query="getitems">
	<tr>
		<td align="center">
		<a href="edit.cfm?id=#getitems.reimbursementid#"><img border="0" src="/images/edit.gif" width="16" height="16"></a>&nbsp;&nbsp;&nbsp;
		<a onClick="return confirm('Are you sure you wish to delete this receipt?');" href="delete.cfm?id=#getitems.reimbursementid#"><img border="0" src="/images/delete.gif" width="16" height="16"></a>
		</td>
		<td>#getitems.employeenum#</td>
		<td>#dateformat(getitems.datetimeentered,'mm/dd/yyyy')#</td>
		<td>#dateformat(getitems.datereimbursed,'mm/dd/yyyy')#</td>
		<td>#getitems.category#</td>
		<td>#dollarformat(getitems.reimbursementamount)#</td>
	</tr>
	</cfoutput>
	<tr>
		<td align="center"><input type="button" onClick="document.location='add.cfm';" value="Add New" class="normal"></td>
		<td colspan="5">&nbsp;</td>
	</tr>
</table>
</form>
<a href="/">Return to Intranet Menu</a>
</div>
