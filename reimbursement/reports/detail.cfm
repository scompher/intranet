
<cfparam name="sort" default="empnum_asc">

<cfswitch expression="#sort#">
	<cfcase value="empnum_asc"><cfset orderby = "reimbursements_main.employeenum asc"></cfcase>
	<cfcase value="empnum_desc"><cfset orderby = "reimbursements_main.employeenum desc"></cfcase>
	<cfcase value="dateent_asc"><cfset orderby = "reimbursements_main.datetimeentered asc"></cfcase>
	<cfcase value="dateent_desc"><cfset orderby = "reimbursements_main.datetimeentered desc"></cfcase>
	<cfcase value="cat_asc"><cfset orderby = "reimbursements_categories.category asc"></cfcase>
	<cfcase value="cat_desc"><cfset orderby = "reimbursements_categories.category desc"></cfcase>
	<cfcase value="datereim_asc"><cfset orderby = "reimbursements_main.datereimbursed asc"></cfcase>
	<cfcase value="datereim_desc"><cfset orderby = "reimbursements_main.datereimbursed desc"></cfcase>
	<cfcase value="reimamt_asc"><cfset orderby = "reimbursements_main.reimbursementamount asc"></cfcase>
	<cfcase value="reimamt_desc"><cfset orderby = "reimbursements_main.reimbursementamount desc"></cfcase>
	<cfcase value="rxnum_asc"><cfset orderby = "reimbursements_main.rxnumber asc"></cfcase>
	<cfcase value="rxnum_desc"><cfset orderby = "reimbursements_main.rxnumber desc"></cfcase>
</cfswitch>

<script language="JavaScript" type="text/JavaScript">
function showdetail(id) {
	window.open('receiptdetail.cfm?id=' + id, 'detailwin', "width=450,height=400,scrollbars=yes");
}
</script>

<script language="javascript" src="../cal2.js">
/*
Xin's Popup calendar script-  Xin Yang (http://www.yxscripts.com/)
Script featured on/available at http://www.dynamicdrive.com/
This notice must stay intact for use
*/
</script>
<script language="javascript" src="../cal_conf2.js"></script>

<cfif isdefined("receipt_startdate") and trim(receipt_startdate) IS NOT ""><cfset odbc_receipt_startdate = createodbcdate(receipt_startdate)></cfif>
<cfif isdefined("receipt_enddate") and trim(receipt_enddate) IS NOT ""><cfset odbc_receipt_enddate = dateadd("d",1,createodbcdate(receipt_enddate))></cfif>

<cfif isdefined("reimb_startdate") and trim(reimb_startdate) IS NOT ""><cfset odbc_reimb_startdate = createodbcdate(reimb_startdate)></cfif>
<cfif isdefined("reimb_enddate") and trim(reimb_enddate) IS NOT ""><cfset odbc_reimb_enddate = dateadd("d",1,createodbcdate(reimb_enddate))></cfif>

<cfquery name="getitems" datasource="#ds#">
	select reimbursements_main.*, reimbursements_categories.category
	from reimbursements_main
	inner join reimbursements_categories on reimbursements_main.categoryid = reimbursements_categories.categoryid
	where 1=1 
	<cfif isdefined("odbc_receipt_startdate") and isdefined("odbc_receipt_enddate")>and datetimeentered >= #odbc_receipt_startdate# and datetimeentered < #odbc_receipt_enddate#</cfif> 
	<cfif isdefined("odbc_reimb_startdate") and isdefined("odbc_reimb_enddate")>and datereimbursed >= #odbc_reimb_startdate# and datereimbursed < #odbc_reimb_enddate#</cfif> 
	<cfif isdefined("empnum")> and employeenum like '%#empnum#%'</cfif>
	order by #orderby#
</cfquery>

<div align="center" class="normal">
<form method="post" action="detail.cfm">
<table border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="right" nowrap><b>Filter by Recipet Date Range:</b></td>
		<td><table border="0" cellspacing="0" cellpadding="3">
				<tr>
					<td><b>Start:</b></td>
					<td>
					<cfoutput>
					<input name="receipt_startdate" type="text" id="receipt_startdate" style="width:75px " <cfif isdefined("receipt_startdate")>value="#receipt_startdate#"</cfif>>
					<a style="text-decoration:none;" href="javascript:showCal('ReceiptStartDate');"><img src="/images/calicon.gif" width="20" height="20" border="0" align="absmiddle"></a>
					</cfoutput>
					</td>
					<td width="20">&nbsp;</td>
					<td><b>End:</b></td>
					<td>
					<cfoutput>
					<input name="receipt_enddate" type="text" id="receipt_enddate" style="width:75px " <cfif isdefined("receipt_enddate")>value="#receipt_enddate#"</cfif>>
					<a style="text-decoration:none;" href="javascript:showCal('ReceiptEndDate');"><img src="/images/calicon.gif" width="20" height="20" border="0" align="absmiddle"></a>
					</cfoutput>
					</td>
				</tr>
		</table></td>
	</tr>
	<tr>
		<td align="right" nowrap><b>Filter by Reimbursement Date Range:</b></td>
		<td><table border="0" cellspacing="0" cellpadding="3">
				<tr>
					<td><b>Start:</b></td>
					<td><cfoutput>
							<input name="reimb_startdate" type="text" id="reimb_startdate" style="width:75px " <cfif isdefined("reimb_startdate")>value="#reimb_startdate#"</cfif>>
							<a style="text-decoration:none;" href="javascript:showCal('ReimbStartDate');"><img src="/images/calicon.gif" width="20" height="20" border="0" align="absmiddle"></a> </cfoutput> </td>
					<td width="20">&nbsp;</td>
					<td><b>End:</b></td>
					<td><cfoutput>
							<input name="reimb_enddate" type="text" id="reimb_enddate" style="width:75px " <cfif isdefined("reimb_enddate")>value="#reimb_enddate#"</cfif>>
							<a style="text-decoration:none;" href="javascript:showCal('ReimbEndDate');"><img src="/images/calicon.gif" width="20" height="20" border="0" align="absmiddle"></a> </cfoutput> </td>
				</tr>
		</table></td>
	</tr>
	<tr>
		<td align="right" nowrap><b>Filter by Employee #: </b></td>
		<td><cfoutput>
				<input type="text" name="empnum" style="width:75px " <cfif isdefined("empnum")>value="#empnum#"</cfif>>
		</cfoutput></td>
	</tr>
	<tr>
		<td align="right" nowrap><input name="Submit" type="submit" class="normal" value="Apply Filter"></td>
		<td nowrap>&nbsp;</td>
	</tr>
</table> 
</form>   
<table width="725" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center"><a style="font-size:16px" href="javascript:window.print();"><b>Print Report</b></a></td>
	</tr>
</table>
<br>
<table width="725" border="1" cellspacing="0" cellpadding="5">
	<cfoutput>
	<tr align="center" bgcolor="DDDDDD">
		<td>
		<cfif sort IS "empnum_asc"><cfset sortlink = "empnum_desc"><cfelse><cfset sortlink = "empnum_asc"></cfif>
		<b><a href="detail.cfm?sort=#sortlink#">Employee ##</a></b>
		</td>
	    <td>
		<cfif sort IS "dateent_asc"><cfset sortlink = "dateent_desc"><cfelse><cfset sortlink = "dateent_asc"></cfif>
		<b><a href="detail.cfm?sort=#sortlink#">Receipt Date</a></b>
		</td>
	    <td>
		<cfif sort IS "cat_asc"><cfset sortlink = "cat_desc"><cfelse><cfset sortlink = "cat_asc"></cfif>
		<b><a href="detail.cfm?sort=#sortlink#">Category</a></b>
		</td>
	    <td>
		<cfif sort IS "datereim_asc"><cfset sortlink = "datereim_desc"><cfelse><cfset sortlink = "datereim_asc"></cfif>
		<b><a href="detail.cfm?sort=#sortlink#">Date Reimbursed</a></b>
		</td>
	    <td>
		<cfif sort IS "reimamt_asc"><cfset sortlink = "reimamt_desc"><cfelse><cfset sortlink = "reimamt_asc"></cfif>
		<b><a href="detail.cfm?sort=#sortlink#">Reimbursement Amount</a></b>
		</td>
		<td>
		<cfif sort IS "rxnum_asc"><cfset sortlink = "rxnum_desc"><cfelse><cfset sortlink = "rxnum_asc"></cfif>
		<b><a href="detail.cfm?sort=#sortlink#">RX Number</a></b>
		</td>
		<td><b>Notes</b></td>
    </tr>
	</cfoutput>
	<cfoutput query="getitems">
	<tr>
		<td valign="top"><a href="javascript:showdetail(#getitems.reimbursementid#);">#getitems.employeenum#</a></td>
		<td align="center" valign="top">#dateformat(getitems.datetimeentered,'mm/dd/yyyy')#</td>
		<td valign="top">#getitems.category#</td>
		<td align="center" valign="top">#dateformat(getitems.datereimbursed,'mm/dd/yyyy')#</td>
		<td valign="top">#dollarformat(getitems.reimbursementamount)#</td>
		<td valign="top">#rxnumber#&nbsp;</td>
		<td valign="top">
		<cfif trim(getitems.notes) IS NOT "">
			#replace(getitems.notes,"#chr(13)#","<br>","all")#
		</cfif>
		<cfif trim(getitems.notesextra) IS NOT "">
			<cfif trim(getitems.notes) IS NOT ""><p></cfif>
			#replace(getitems.notesextra,"#chr(13)#","<br>","all")#
		</cfif>
		&nbsp;
		</td>
	</tr>
	</cfoutput>
</table>
<br>
<a href="index.cfm">Return to Reports Menu</a>
</div>

