
<style type="text/css">
	TABLE {font-family:Arial, Helvetica, sans-serif;font-size:12px; background-color:#FFFFFF;}
	.normal {font-family:Arial, Helvetica, sans-serif;font-size:12px}
	.heading {font-family:Arial, Helvetica, sans-serif;font-size:16px}
	.titlebar {font-family: Arial, Helvetica, sans-serif;	font-size: 12px;font-weight: bold;color: #FFFFFF;}
	.small {font-family:Verdana, Arial, Helvetica, sans-serif;font-size:10px}
	a:link {color:#000000; text-decoration: underline;}
	a:visited {text-decoration: underline; color: #000000;}
	a:hover {text-decoration: underline; color: #0000FF;}
	a:active {text-decoration: underline; color: #000000;}
	.box {border: 1px solid #000000;}
</style>

<script language="javascript" src="../cal2.js">
/*
Xin's Popup calendar script-  Xin Yang (http://www.yxscripts.com/)
Script featured on/available at http://www.dynamicdrive.com/
This notice must stay intact for use
*/
</script>
<script language="javascript" src="../cal_conf2.js"></script>

<cfif isdefined("receipt_startdate") and trim(receipt_startdate) IS NOT ""><cfset odbc_receipt_startdate = createodbcdate(receipt_startdate)></cfif>
<cfif isdefined("receipt_enddate") and trim(receipt_enddate) IS NOT ""><cfset odbc_receipt_enddate = dateadd("d",1,createodbcdate(receipt_enddate))></cfif>

<cfif isdefined("reimb_startdate") and trim(reimb_startdate) IS NOT ""><cfset odbc_reimb_startdate = createodbcdate(reimb_startdate)></cfif>
<cfif isdefined("reimb_enddate") and trim(reimb_enddate) IS NOT ""><cfset odbc_reimb_enddate = dateadd("d",1,createodbcdate(reimb_enddate))></cfif>

<cfquery name="getinfo" datasource="#ds#">
	select * 
	from reimbursements_main
	where 1=1 
	<cfif isdefined("odbc_receipt_startdate") and isdefined("odbc_receipt_enddate")>and datetimeentered >= #odbc_receipt_startdate# and datetimeentered < #odbc_receipt_enddate#</cfif> 
	<cfif isdefined("odbc_reimb_startdate") and isdefined("odbc_reimb_enddate")>and datereimbursed >= #odbc_reimb_startdate# and datereimbursed < #odbc_reimb_enddate#</cfif> 
	<cfif isdefined("empnum")> and employeenum like '%#empnum#%'</cfif>
	order by employeenum asc
</cfquery>

<div align="center" class="normal">
	<form method="post" action="summary.cfm">
	<table border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td align="right" nowrap><b>Filter by Recipet Date Range:</b></td>
		    <td>
			<table border="0" cellspacing="0" cellpadding="3">
            	<tr>
            		<td><b>Start:</b></td>
            		<td>
					<cfoutput>
					<input name="receipt_startdate" type="text" id="receipt_startdate" style="width:75px " <cfif isdefined("receipt_startdate")>value="#receipt_startdate#"</cfif>>
            		<a style="text-decoration:none;" href="javascript:showCal('ReceiptStartDate');"><img src="/images/calicon.gif" width="20" height="20" border="0" align="absmiddle"></a>
					</cfoutput>
					</td>
            	    <td width="20">&nbsp;</td>
            	    <td><b>End:</b></td>
            	    <td>
					<cfoutput>
					<input name="receipt_enddate" type="text" id="receipt_enddate" style="width:75px " <cfif isdefined("receipt_enddate")>value="#receipt_enddate#"</cfif>>
            	    <a style="text-decoration:none;" href="javascript:showCal('ReceiptEndDate');"><img src="/images/calicon.gif" width="20" height="20" border="0" align="absmiddle"></a>
					</cfoutput>
					</td>
            	</tr>
            	</table>
			</td>
		</tr>
		<tr>
        	<td align="right" nowrap><b>Filter by Reimbursement Date Range:</b></td>
        	<td><table border="0" cellspacing="0" cellpadding="3">
        			<tr>
        				<td><b>Start:</b></td>
        				<td><cfoutput>
        						<input name="reimb_startdate" type="text" id="reimb_startdate" style="width:75px " <cfif isdefined("reimb_startdate")>value="#reimb_startdate#"</cfif>>
        						<a style="text-decoration:none;" href="javascript:showCal('ReimbStartDate');"><img src="/images/calicon.gif" width="20" height="20" border="0" align="absmiddle"></a> </cfoutput> </td>
        				<td width="20">&nbsp;</td>
        				<td><b>End:</b></td>
        				<td><cfoutput>
        						<input name="reimb_enddate" type="text" id="reimb_enddate" style="width:75px " <cfif isdefined("reimb_enddate")>value="#reimb_enddate#"</cfif>>
        						<a style="text-decoration:none;" href="javascript:showCal('ReimbEndDate');"><img src="/images/calicon.gif" width="20" height="20" border="0" align="absmiddle"></a> </cfoutput> </td>
        				</tr>
        			</table></td>
		</tr>
		<tr>
			<td align="right" nowrap><b>Filter by Employee #: </b></td>
		    <td><cfoutput><input type="text" name="empnum" style="width:75px " <cfif isdefined("empnum")>value="#empnum#"</cfif>></cfoutput></td>
		</tr>
		<tr>
			<td align="right" nowrap><input name="Submit" type="submit" class="normal" value="Apply Filter"></td>
		    <td nowrap>&nbsp;</td>
		</tr>
	</table>
<br>
<table width="725" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center"><a style="font-size:16px" href="javascript:window.print();"><b>Print Report</b></a></td>
	</tr>
</table>
<br>
    <table width="725" border="1" cellpadding="5" cellspacing="0" class="normal">
	<tr bgcolor="FFFFCC">
		<td width="82">&nbsp;</td>
	    <td colspan="8" align="center" bgcolor="FFFFCC"><b>Amount to be Paid</b></td>
    </tr>
	<tr align="center" valign="bottom" bgcolor="DDDDDD">
		<td width="82" nowrap><b>Employee # </b></td>
		<td width="75" nowrap><b>Count</b></td>
		<td width="81" nowrap><b>Doctor</b></td>
		<td width="72" nowrap><b>Specialist</b></td>
		<td width="106" nowrap bgcolor="DDDDDD"><b>Generic RX </b></td>
		<td width="90" nowrap><b>Brand RX </b></td>
		<td width="90" nowrap><b>Gen RX 3 Month </b></td>
		<td width="90" nowrap><b>Brand RX 3 month</b></td>
		<td width="133" nowrap><b>Total</b></td>
	</tr>
	<cfoutput query="getinfo" group="employeenum">
		<cfset count = 0>
		<cfset drtotal = 0>
		<cfset sptotal = 0>
		<cfset genrxtotal = 0>
		<cfset brandrxtotal = 0>
		<cfset genrx3monthtotal = 0>
		<cfset brandrx3monthtotal = 0>
		<cfset emptotal = 0>
		<cfoutput>
		<cfset count = count + 1>
		<cfswitch expression="#getinfo.categoryid#">
			<cfcase value="1"><cfset drtotal = drtotal + getinfo.reimbursementamount></cfcase>
			<cfcase value="2"><cfset sptotal = sptotal + getinfo.reimbursementamount></cfcase>
			<cfcase value="3"><cfset genrxtotal = genrxtotal + getinfo.reimbursementamount></cfcase>
			<cfcase value="4"><cfset brandrxtotal = brandrxtotal + getinfo.reimbursementamount></cfcase>
			<cfcase value="5"><cfset genrx3monthtotal = genrx3monthtotal + getinfo.reimbursementamount></cfcase>
			<cfcase value="6"><cfset brandrx3monthtotal = brandrx3monthtotal + getinfo.reimbursementamount></cfcase>
		</cfswitch>
		<cfset emptotal = emptotal + getinfo.reimbursementamount>		
		</cfoutput>
		<tr>
			<td width="82">#employeenum#</td>
			<td>#count#</td>
			<td>#dollarformat(drtotal)#</td>
			<td>#dollarformat(sptotal)#</td>
			<td>#dollarformat(genrxtotal)#</td>
			<td>#dollarformat(brandrxtotal)#</td>
			<td>#dollarformat(genrx3monthtotal)#</td>
			<td>#dollarformat(brandrx3monthtotal)#</td>
			<td>#dollarformat(emptotal)#</td>
		</tr>
	</cfoutput>
</table>
</form>
<br>
<a href="index.cfm">Return to Reports Menu</a>
</div>
