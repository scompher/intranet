
<style type="text/css">
	.redstar {
	color: #FF0000;
	font-size: 14px;
}
</style>

<cfquery name="getreceipt" datasource="#ds#">
	select reimbursements_main.*, reimbursements_categories.category
	from reimbursements_main
	inner join reimbursements_categories on reimbursements_main.categoryid = reimbursements_categories.categoryid
	where reimbursements_main.reimbursementid = #id#
</cfquery>

<div align="center" class="normal">
<a href="javascript:self.close();">Close Window</a>
<br>
<br>
<table width="400" border="1" cellspacing="0" cellpadding="5">
<cfoutput query="getreceipt">
	<tr>
		<td colspan="2" align="center" bgcolor="FFFFCC"><b>View Receipt</b></td>
	</tr>
	<tr>
		<td width="145"><b>Employee Number:</b></td>
		<td>#getreceipt.employeenum#&nbsp;</td>
	</tr>
	<tr>
		<td><b>Category:</b></td>
		<td>#getreceipt.category#&nbsp;	</td>
	</tr>
	<tr>
		<td><b>Date Reimbursed:</b></td>
		<td>#dateformat(getreceipt.datereimbursed,'mm/dd/yyyy')#&nbsp;</td>
	</tr>
	<tr>
		<td><b>Requested Amount:</b></td>
		<td>#dollarformat(getreceipt.requestedamount)#&nbsp;</td>
	</tr>
	<tr>
		<td><b>RX Number: </b></td>
		<td>#rxnumber#&nbsp;</td>
	</tr>
	<tr>
		<td><b>Receipt Date:</b></td>
		<td>#dateformat(getreceipt.receiptdate,'mm/dd/yyyy')#&nbsp;</td>
	</tr>
	<tr>
		<td><b>Reimbursement Amount:</b></td>
		<td>#dollarformat(getreceipt.reimbursementamount)#&nbsp;</td>
	</tr>
	<tr>
		<td valign="top"><b>Notes</b></td>
		<td>
		<cfif trim(getreceipt.notes) IS NOT "">
			#replace(getreceipt.notes,"#chr(13)#","<br>","all")#
		</cfif>
		<cfif trim(getreceipt.notesextra) IS NOT "">
			<cfif trim(getreceipt.notes) IS NOT ""><p></cfif>
			#replace(getreceipt.notesextra,"#chr(13)#","<br>","all")#
		</cfif>
		&nbsp;
		</td>
	</tr>
</cfoutput>
</table>
</div>
