<style type="text/css">
	TABLE {font-family:Arial, Helvetica, sans-serif;font-size:12px; background-color:#FFFFFF;}
	.normal {font-family:Arial, Helvetica, sans-serif;font-size:12px}
	.heading {font-family:Arial, Helvetica, sans-serif;font-size:16px}
	.titlebar {font-family: Arial, Helvetica, sans-serif;	font-size: 12px;font-weight: bold;color: #FFFFFF;}
	.small {font-family:Verdana, Arial, Helvetica, sans-serif;font-size:10px}
	a:link {color:#000000; text-decoration: underline;}
	a:visited {text-decoration: underline; color: #000000;}
	a:hover {text-decoration: underline; color: #0000FF;}
	a:active {text-decoration: underline; color: #000000;}
	.box {border: 1px solid #000000;}
</style>

<cfquery name="getcats" datasource="#ds#">
	select * from reimbursements_categories
	order by category asc
</cfquery>

<table width="725" border="1" cellspacing="0" cellpadding="5">
	<tr>
		<td width="65" align="center" bgcolor="DDDDDD"><b>Action</b></td>
		<td width="634" bgcolor="DDDDDD"><b>Category</b></td>
	</tr>
	<cfoutput query="getcats">
	<tr>
		<td align="center">
		<a href="edit.cfm?id=#getcats.categoryid#"><img border="0" src="/images/edit.gif" width="16" height="16"></a>&nbsp;&nbsp;&nbsp;
		<a onClick="return confirm('Are you sure you wish to delete this category?');" href="delete.cfm?id=#getcats.categoryid#"><img border="0" src="/images/delete.gif" width="16" height="16"></a>
		</td>
		<td>#getcats.category#</td>
	</tr>
	</cfoutput>
	<tr>
		<td align="center"><input type="button" name="Submit" value="New" onClick="document.location='add.cfm';" ></td>
		<td>&nbsp;</td>
	</tr>
</table>
