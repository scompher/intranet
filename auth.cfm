
<cfif trim(cookie.adminlogin) is "">
	<cfcookie name="adminlogin" expires="now">
	<cfcookie name="seclist" expires="now">
	<meta http-equiv="refresh" content="0;url=/index.cfm">
	<cfabort>
</cfif>

<!--- get sec level --->
<cfquery name="getsec" datasource="#ds#">
	select admin_users.*, admin_users_departments_lookup.*
	from admin_users 
	inner join admin_users_departments_lookup on admin_users.adminuserid = admin_users_departments_lookup.adminuserid
	where admin_users.adminuserid = #cookie.adminlogin#
</cfquery>

<!--- 
<cfif getsec.seclevelid IS NOT 1>
	<cfset departmentlist = valuelist(getsec.departmentid)>
<cfelse>
	<cfquery name="getdepts" datasource="#ds#">
		select * from Admin_Users_Departments
		order by department ASC
	</cfquery>
	<cfset departmentlist = valuelist(getdepts.departmentid)>
</cfif>
--->

<cfset departmentlist = valuelist(getsec.departmentid)>

<cfif NOT isDefined("cookie.seclist")>
	<cfquery name="getseclist" datasource="#request.odbc_datasource#">
		select * from Admin_Security_Lookup
		where adminuserid = #cookie.adminlogin#
	</cfquery>
	<cfset cookie.seclist = valuelist(getseclist.itemid)>
</cfif>

<cfif not isdefined("cookie.intranetLoggedIn")>
	<cfset lastlogin = createodbcdatetime(now())>
	<cfcookie name="intranetLoggedIn" value="#lastlogin#">
	<cfquery name="updateLastLoggedIn" datasource="#ds#">
		update admin_users
		set lastlogin = #lastlogin#
		where adminuserid = #cookie.adminlogin#
	</cfquery>
</cfif>

