
<cfparam name="ticketid" default="0">

<cfquery name="delTicket" datasource="#ds#">
	delete from troubleTicket_tickets where ticketid = #ticketid# 
</cfquery>
<cfquery name="delStatusHistory" datasource="#ds#">
	delete from troubleTicket_status_history where ticketid = #ticketid# 
</cfquery>
<cfquery name="delTechHistory" datasource="#ds#">
	delete from troubleTicket_techHistory where ticketid = #ticketid# 
</cfquery>

<cflocation url="tickets.cfm">
