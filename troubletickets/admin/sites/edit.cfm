
<link rel="stylesheet" type="text/css" href="/styles.css">

<cfif isDefined("form.btnEditSite")>

	<cfquery name="saveChanges" datasource="#ds#">
		update troubleTicket_sites 
		set siteLabel = '#form.siteLabel#' 
		where siteid = #id# 
	</cfquery>

	<cflocation url="index.cfm">

<cfelse>

	<cfquery name="getSite" datasource="#ds#">
		select * from troubleTicket_sites 
		where siteid = #id# 
	</cfquery>

	<div align="center">
	<table border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td class="highlightbar"><b>Edit Site</b></td>
		</tr>
		<tr>
			<td class="greyrowbottomnopadding">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<form method="post" action="edit.cfm">
					<cfoutput>
					<input type="hidden" name="id" value="#id#" />
					</cfoutput>
					<tr>
						<td><b>Site Name:						
							<input type="text" name="siteLabel" style="width:300px; vertical-align:middle;" value="<cfoutput>#getSite.siteLabel#</cfoutput>">
						</b></td>
					</tr>
					<tr>
						<td>
							<input name="btnEditSite" type="submit" class="sidebar" value="Update Site">
							<input name="btnCancel" type="button" class="sidebar" value="Cancel" onclick="document.location = 'index.cfm';" >
						</td>
					</tr>
					</form>
				</table>
			</td>
		</tr>
	</table>
	</div>

</cfif>

