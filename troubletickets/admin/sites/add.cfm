
<link rel="stylesheet" type="text/css" href="/styles.css">

<cfif isDefined("form.btnAddSite")>

	<cfquery name="saveSiteInfo" datasource="#ds#">
		if not exists (select siteid from troubleTicket_sites where siteLabel = '#form.siteLabel#')
		begin
			insert into troubleTicket_sites (siteLabel) values ('#form.siteLabel#')
		end
	</cfquery>

	<cflocation url="index.cfm">

<cfelse>

	<div align="center">
	<table border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td class="highlightbar"><b>Add a new Site</b></td>
		</tr>
		<tr>
			<td class="greyrowbottomnopadding">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<form method="post" action="add.cfm">
					<tr>
						<td><b>Site Name:						
							<input type="text" name="siteLabel" style="width:300px; vertical-align:middle;">
						</b></td>
					</tr>
					<tr>
						<td>
							<input name="btnAddSite" type="submit" class="sidebar" value="Add Site">
							<input name="btnCancel" type="button" class="sidebar" value="Cancel" onclick="document.location = 'index.cfm';" >
						</td>
					</tr>
					</form>
				</table>
			</td>
		</tr>
	</table>
	</div>

</cfif>

