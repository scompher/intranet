
<link rel="stylesheet" type="text/css" href="/styles.css">

<cfquery name="getSites" datasource="#ds#">
	select * from troubleTicket_sites 
	order by sitelabel asc 
</cfquery>

<div align="center">
<table width="400" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Site Administration</b> </td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
		<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td width="14%" align="center" nowrap class="linedrowrightcolumn"><b>Action</b></td>
				<td width="86%" class="linedrow"><b>Site Name</b> </td>
			</tr>
			<cfoutput query="getSites">
			<tr>
				<td align="center" nowrap class="linedrowrightcolumn">
				<table width="100%" border="0" cellspacing="0" cellpadding="2">
					<tr>
						<td align="center" valign="middle" nowrap="nowrap">
						<a href="edit.cfm?id=#siteid#"><img src="/images/edit.gif" alt="Edit Item" width="16" height="16" border="0"></a>
						</td>
						<td align="center" valign="middle" nowrap="nowrap">
						<a onClick="return confirm('Are you sure you wish to delete this item?');" href="delete.cfm?id=#siteid#"><img src="/images/delete.gif" alt="Delete Item" width="16" height="16" border="0"></a>
						</td>
					</tr>
				</table>
				</td>
				<td class="linedrow">#siteLabel#</td>
			</tr>
			</cfoutput>
			<form method="post" action="index.cfm">
			<tr>
				<td colspan="2" nowrap class="linedrowrightcolumn">
					<input name="Submit" type="button" class="sidebar" value="Add New Site" onclick="document.location = 'add.cfm';" />
				</td>
			</tr>
			</form>
		</table>
		</td>
	</tr>
</table>
<br />
<a class="normal" style="text-decoration:underline;" href="../index.cfm">Return to Admin Menu</a> | <a class="normal" style="text-decoration:underline;" href="/index.cfm">Return to Intranet</a>
</div>
