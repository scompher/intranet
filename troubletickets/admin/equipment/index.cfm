
<link rel="stylesheet" type="text/css" href="/styles.css">

<cfquery name="getEquipment" datasource="#ds#">
	select troubleTicket_equipment.*, troubleTicket_sites.siteLabel, troubleTicket_departments.departmentLabel 
	from troubleTicket_equipment 
	left join troubleTicket_departments on troubleTicket_equipment.departmentid = troubleTicket_departments.departmentid 
	left join troubleTicket_sites on troubleTicket_departments.siteid = troubleTicket_sites.siteid 
	order by siteLabel, departmentLabel, equipmentLabel 
</cfquery>

<div align="center">
<table border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Equipment Administration</b> </td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
		<table border="0" cellpadding="5" cellspacing="0" width="100%">
			<form method="post" action="index.cfm">
			<tr>
				<td colspan="5" nowrap class="linedrowrightcolumn">
					<input name="Submit" type="button" class="sidebar" value="Add New Equipment" onClick="document.location = 'add.cfm';" />
				</td>
			</tr>
			</form>
			<tr>
				<td align="center" nowrap class="linedrowrightcolumn"><b>Action</b></td>
				<td nowrap class="linedrowrightcolumn"><b>Site</b></td>
				<td class="linedrowrightcolumn"><b>Department</b></td>
				<td class="linedrowrightcolumn"><b>Equipment</b></td>
				<td class="linedRow"><b>Service Tag #</b></td>
			</tr>
			<cfoutput query="getEquipment">
			<tr>
				<td align="center" nowrap class="linedrowrightcolumn">
				<table width="100%" border="0" cellspacing="0" cellpadding="2">
					<tr>
						<td align="center" valign="middle" nowrap="nowrap">
						<a href="edit.cfm?id=#equipmentid#"><img src="/images/edit.gif" alt="Edit Item" width="16" height="16" border="0"></a>
						</td>
						<td align="center" valign="middle" nowrap="nowrap">
						<a onClick="return confirm('Are you sure you wish to delete this item?');" href="delete.cfm?id=#equipmentid#"><img src="/images/delete.gif" alt="Delete Item" width="16" height="16" border="0"></a>
						</td>
					</tr>
				</table>
				</td>
				<td nowrap class="linedrowrightcolumn">#siteLabel#</td>
				<td class="linedrowrightcolumn">#departmentLabel#</td>
				<td class="linedrowrightcolumn">#equipmentLabel#</td>
				<td class="linedRow">#serviceTagNumber#&nbsp;</td>
			</tr>
			</cfoutput>
			<form method="post" action="index.cfm">
			<tr>
				<td colspan="5" nowrap class="linedrowrightcolumn">
					<input name="Submit" type="button" class="sidebar" value="Add New Equipment" onClick="document.location = 'add.cfm';" />
				</td>
			</tr>
			</form>
		</table>
		</td>
	</tr>
</table>
<br />
<a class="normal" style="text-decoration:underline;" href="../index.cfm">Return to Admin Menu</a> | <a class="normal" style="text-decoration:underline;" href="/index.cfm">Return to Intranet</a>
</div>
