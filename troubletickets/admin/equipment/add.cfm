
<link rel="stylesheet" type="text/css" href="/styles.css">

<script type="text/javascript">
function checkAll(itm) {
	if (itm.length) {
		for (var i = 0; i < itm.length; i++) {
			itm[i].checked = true;
		}
	} else {
		itm.checked = true;
	}
}

function unCheckAll(itm) {
	if (itm.length) {
		for (var i = 0; i < itm.length; i++) {
			itm[i].checked = false;
		}
	} else {
		itm.checked = false;
	}
}
</script>

<cfif isDefined("form.btnAddComponent")>
	
	<cfquery name="insertComponent" datasource="#ds#">
		if not exists (select * from troubleTicket_components where componentLabel = '#form.componentLabel#')
		begin
			insert into troubleTicket_components (componentLabel, componentCategoryID)
			values ('#form.componentLabel#', #form.componentCategoryID#) 
		end
	</cfquery>
	
</cfif>

<cfif isDefined("form.btnAddEquipment")>

	<cfquery name="saveEquipment" datasource="#ds#">
		if not exists (select equipmentid from troubleTicket_equipment where equipmentLabel = '#form.equipmentLabel#' and departmentid = #form.departmentid# and serviceTagNumber = '#form.serviceTagNumber#') 
		begin
			insert into troubleTicket_equipment (departmentID, equipmentLabel, serviceTagNumber, dateTimeCreated) 
			values (#form.departmentID#, '#form.equipmentLabel#', '#form.serviceTagNumber#', #createodbcdatetime(now())#) 
		end
	</cfquery>

	<cfquery name="getID" datasource="#ds#">
		select max(equipmentid) as newid from troubleTicket_equipment 
		where equipmentLabel = '#form.equipmentLabel#' and departmentid = #form.departmentid# and serviceTagNumber = '#form.serviceTagNumber#'
	</cfquery>
	
	<cfset eid = getid.newid>
	
	<cfquery name="insertLookup" datasource="#ds#">
		<cfloop list="#form.componentid#" index="cid">
			begin
				insert into troubleTicket_equipment_component_lookup (equipmentid, componentid, dateTimeCreated) 
				values (#eid#, #cid#, #createodbcdatetime(now())#) 
			end
		</cfloop>
	</cfquery>
	
	<cflocation url="index.cfm">

<cfelse>

	<cfquery name="getdepts" datasource="#ds#">
		select troubleTicket_departments.*, troubleTicket_sites.siteLabel
		from troubleTicket_departments
		left join troubleTicket_sites on  troubleTicket_departments.siteid = troubleTicket_sites.siteid 
		order by troubleTicket_sites.siteLabel, troubleTicket_departments.departmentLabel asc 
	</cfquery>

	<cfquery name="getComponents" datasource="#ds#">
		select * from troubleTicket_components 
		order by componentLabel asc 
	</cfquery>

	<cfquery name="getComponentCategories" datasource="#ds#">
		select * from dbo.troubleTicket_component_categories
		order by categoryLabel asc 
	</cfquery>

	<div align="center">
	<table border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td class="highlightbar"><b>Add New Equipment</b></td>
		</tr>
		<tr>
			<td class="greyrowbottomnopadding">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<form method="post" name="mainform" action="add.cfm">
					<tr>
						<td>
						<b>Department Associated: </b>						</td>
						<td>
						<select name="departmentid" style="vertical-align:middle;">
							<option value="0"></option>
						<cfoutput query="getDepts">
							<option value="#getDepts.departmentID#">#getDepts.siteLabel# - #getDepts.departmentLabel#</option>
						</cfoutput>
						</select>						
						</td>
					</tr>
					<tr>
						<td>
						<b>Equipment Label:</b>						</td>
						<td>
						<input type="text" name="equipmentLabel" style="width:300px;" value="" />
						</td>
					</tr>
					<tr>
						<td valign="top">
						<b>Select Components:</b><br />
						<a href="javascript:checkAll(document.mainform.componentid);">[Check All]</a><br />
						<a href="javascript:unCheckAll(document.mainform.componentid);">[Un-Check All]</a>						</td>
						<td class="nopadding" valign="top">
							<table border="0" cellspacing="0" cellpadding="5">
								<tr>
									<td class="nopadding" valign="top">
										<table border="0" cellspacing="0" cellpadding="1">
											<cfoutput query="getcomponents">
											<cfif getcomponents.currentrow lte (getcomponents.recordcount/2)>
											<tr>
												<td><input type="checkbox" name="componentid" value="#componentid#" /></td>
												<td>#componentLabel#</td>
											</tr>
											</cfif>
											</cfoutput>
										</table>
									</td>
									<td>&nbsp;&nbsp;&nbsp;</td>
									<td class="nopadding" valign="top">
										<table border="0" cellspacing="0" cellpadding="1">
											<cfoutput query="getcomponents">
											<cfif getcomponents.currentrow gt (getcomponents.recordcount/2)>
											<tr>
												<td><input type="checkbox" name="componentid" value="#componentid#" /></td>
												<td>#componentLabel#</td>
											</tr>
											</cfif>
											</cfoutput>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<!--- 
					<tr>
						<td><b>Add Component Not Listed:</b></td>
						<td>
							<input type="text" name="componentLabel" style="width:300px; vertical-align:middle;" value="" />
						</td>
					</tr>
					<tr>
						<td><b>Component Category: </b></td>
						<td>
						<select name="componentCategoryID" style="width:245px; vertical-align:middle;">
							<option value="0"></option>
							<cfoutput query="getComponentCategories">
								<option value="#componentCategoryID#">#categoryLabel#</option>
							</cfoutput>
						</select>
						<input type="submit" name="btnAddComponent" value="Add" style="width:50px; vertical-align:middle;" class="sidebarsmall"/>
						</td>
					</tr>
					--->
					<tr>
						<td>
						<b>Service Tag #:</b></td>
						<td>
						<input type="text" name="serviceTagNumber" style="width:300px;" value="" />
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<input name="btnAddEquipment" type="submit" class="sidebar" value="Add Equipment">
							<input name="btnCancel" type="button" class="sidebar" value="Cancel" onclick="document.location = 'index.cfm';" >
						</td>
					</tr>
					</form>
				</table>
			</td>
		</tr>
	</table>
	</div>

</cfif>

