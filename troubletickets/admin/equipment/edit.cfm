
<link rel="stylesheet" type="text/css" href="/styles.css">

<cfif isDefined("form.btnUpdateEquipment")>

	<cfquery name="updateEquipment" datasource="#ds#">
		update troubleTicket_equipment 
		set departmentid = #form.departmentid#, equipmentLabel = '#form.equipmentLabel#', serviceTagNumber = '#form.serviceTagNumber#' 
		where equipmentid = #id# 
	</cfquery>

	<cfquery name="delLookup" datasource="#ds#">
		delete from troubleTicket_equipment_component_lookup 
		where equipmentid = #id# 
	</cfquery>

	<cfquery name="updateLookup" datasource="#ds#">
		<cfloop list="#form.componentid#" index="cid">
			begin
				insert into troubleTicket_equipment_component_lookup (equipmentid, componentid) 
				values (#id#, #cid#) 
			end
		</cfloop>
	</cfquery>

	<cflocation url="index.cfm">

<cfelse>

	<cfquery name="getdepts" datasource="#ds#">
		select troubleTicket_departments.*, troubleTicket_sites.siteLabel
		from troubleTicket_departments
		left join troubleTicket_sites on  troubleTicket_departments.siteid = troubleTicket_sites.siteid 
		order by troubleTicket_sites.siteLabel, troubleTicket_departments.departmentLabel asc 
	</cfquery>

	<cfquery name="getComponents" datasource="#ds#">
		select * from troubleTicket_components 
		order by componentLabel asc 
	</cfquery>

	<cfquery name="getEquipment" datasource="#ds#">
		select * 
		from troubleTicket_equipment 
		where equipmentID = #id# 
	</cfquery>

	<cfquery name="getChosenComponents" datasource="#ds#">
		select * from troubleTicket_equipment_component_lookup 
		where equipmentID = #id# 
	</cfquery>
	<cfset componentIDList = valuelist(getChosenComponents.componentID)>

	<div align="center">
	<table border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td class="highlightbar"><b>Update Equipment</b></td>
		</tr>
		<tr>
			<td class="greyrowbottomnopadding">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<form method="post" action="edit.cfm">
					<cfoutput>
					<input type="hidden" name="id" value="#id#">
					</cfoutput>
					<tr>
						<td>
						<b>Department Associated: </b> 
						</td>
						<td>
						<select name="departmentid" style="vertical-align:middle;">
							<option value="0"></option>
						<cfoutput query="getDepts">
							<option <cfif getEquipment.departmentid is getDepts.departmentid>selected</cfif> value="#getDepts.departmentID#">#getDepts.siteLabel# - #getDepts.departmentLabel#</option>
						</cfoutput>
						</select>						
						</td>
					</tr>
					<tr>
						<td>
						<b>Equipment Label:</b> 
						</td>
						<td>
						<input type="text" name="equipmentLabel" style="width:300px;" value="<cfoutput>#getEquipment.equipmentLabel#</cfoutput>" />
						</td>
					</tr>
					<tr>
						<td valign="top"><b>Select Components:</b></td>
						<td class="nopadding" valign="top">
							<table border="0" cellspacing="0" cellpadding="5">
								<tr>
									<td class="nopadding">
										<table border="0" cellspacing="0" cellpadding="1">
											<cfoutput query="getcomponents">
											<cfif getcomponents.currentrow lte (getcomponents.recordcount/2)>
											<tr>
												<td><input <cfif listfindnocase(componentIDList,componentid) is not 0>checked</cfif> type="checkbox" name="componentid" value="#componentid#" /></td>
												<td>#componentLabel#</td>
											</tr>
											</cfif>
											</cfoutput>
										</table>
									</td>
									<td>&nbsp;&nbsp;&nbsp;</td>
									<td class="nopadding">
										<table border="0" cellspacing="0" cellpadding="1">
											<cfoutput query="getcomponents">
											<cfif getcomponents.currentrow gt (getcomponents.recordcount/2)>
											<tr>
												<td><input <cfif listfindnocase(componentIDList,componentid) is not 0>checked</cfif> type="checkbox" name="componentid" value="#componentid#" /></td>
												<td>#componentLabel#</td>
											</tr>
											</cfif>
											</cfoutput>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td>
						<b>Service Tag #:</b> 
						</td>
						<td>
						<input type="text" name="serviceTagNumber" style="width:300px;" value="<cfoutput>#getEquipment.serviceTagNumber#</cfoutput>" />
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<input name="btnUpdateEquipment" type="submit" class="sidebar" value="Update Equipment">
							<input name="btnCancel" type="button" class="sidebar" value="Cancel" onclick="document.location = 'index.cfm';" >
						</td>
					</tr>
					</form>
				</table>
			</td>
		</tr>
	</table>
	</div>

</cfif>

