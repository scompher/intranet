
<link rel="stylesheet" type="text/css" href="/styles.css">

<cfquery name="getSites" datasource="#ds#">
	select troubleTicket_components.*, troubleTicket_component_categories.categoryLabel  
	from troubleTicket_components
	left join troubleTicket_component_categories on troubleTicket_components.componentCategoryID = troubleTicket_component_categories.componentCategoryID 
	order by componentLabel asc 
</cfquery>

<div align="center">
<table width="400" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Component Administration</b> </td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
		<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td width="14%" align="center" nowrap class="linedrowrightcolumn"><b>Action</b></td>
				<td class="linedrow"><b>Component Name</b> </td>
				<td class="linedrow"><b>Category</b> </td>
			</tr>
			<cfoutput query="getSites">
			<tr>
				<td align="center" nowrap class="linedrowrightcolumn">
				<table width="100%" border="0" cellspacing="0" cellpadding="2">
					<tr>
						<td align="center" valign="middle" nowrap="nowrap">
						<a href="edit.cfm?id=#componentID#"><img src="/images/edit.gif" alt="Edit Item" width="16" height="16" border="0"></a>
						</td>
						<td align="center" valign="middle" nowrap="nowrap">
						<a onClick="return confirm('Are you sure you wish to delete this item?');" href="delete.cfm?id=#componentID#"><img src="/images/delete.gif" alt="Delete Item" width="16" height="16" border="0"></a>
						</td>
					</tr>
				</table>
				</td>
				<td class="linedrowrightcolumn">#componentLabel#</td>
				<td class="linedrow">#categoryLabel#&nbsp;</td>
			</tr>
			</cfoutput>
			<form method="post" action="index.cfm">
			<tr>
				<td colspan="2" nowrap class="linedrowrightcolumn">
					<input name="Submit" type="button" class="sidebar" value="Add New Component" onclick="document.location = 'add.cfm';" />
				</td>
			</tr>
			</form>
		</table>
		</td>
	</tr>
</table>
<br />
<a class="normal" style="text-decoration:underline;" href="../index.cfm">Return to Admin Menu</a> | <a class="normal" style="text-decoration:underline;" href="/index.cfm">Return to Intranet</a>
</div>
