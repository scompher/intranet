
<link rel="stylesheet" type="text/css" href="/styles.css">

<cfif isDefined("form.btnEditComponent")>

	<cfparam name="form.componentCategoryID" default="0">

	<cfquery name="saveChanges" datasource="#ds#">
		update troubleTicket_Components 
		set ComponentLabel = '#form.ComponentLabel#', componentCategoryID = #form.componentCategoryID#  
		where Componentid = #id# 
	</cfquery>

	<cflocation url="index.cfm">

<cfelse>

	<cfquery name="getComponent" datasource="#ds#">
		select * from troubleTicket_Components 
		where Componentid = #id# 
	</cfquery>

	<cfquery name="getCats" datasource="#ds#">
		select * from troubleTicket_component_categories 
		order by categorylabel asc 
	</cfquery>

	<div align="center">
	<table border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td class="highlightbar"><b>Edit Component</b></td>
		</tr>
		<tr>
			<td class="greyrowbottomnopadding">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<form method="post" action="edit.cfm">
					<cfoutput>
					<input type="hidden" name="id" value="#id#" />
					</cfoutput>
					<tr>
						<td>
							<b>Component Name:</b>&nbsp;<input type="text" name="ComponentLabel" style="width:300px; vertical-align:middle;" value="<cfoutput>#getComponent.ComponentLabel#</cfoutput>">
						</td>
					</tr>
					<tr>
						<td>
							<b>Component Category:</b>&nbsp;
							<select name="componentCategoryID">
								<option value="0"></option>
								<cfoutput query="getCats">
									<option <cfif getCats.componentCategoryID is getComponent.componentCategoryID>selected</cfif> value="#getCats.componentCategoryID#">#getCats.categoryLabel#</option>
								</cfoutput>
							</select>
						</td>
					</tr>
					<tr>
						<td>
							<input name="btnEditComponent" type="submit" class="sidebar" value="Update Component">
							<input name="btnCancel" type="button" class="sidebar" value="Cancel" onclick="document.location = 'index.cfm';" >
						</td>
					</tr>
					</form>
				</table>
			</td>
		</tr>
	</table>
	</div>

</cfif>

