
<link rel="stylesheet" type="text/css" href="/styles.css">

<cfif isDefined("form.btnAddComponent")>

	<cfparam name="form.componentCategoryID" default="0">
	
	<cfquery name="saveInfo" datasource="#ds#">
		if not exists (select componentid from troubleTicket_components where componentLabel = '#form.componentLabel#' and componentCategoryID = #form.componentCategoryID#)
		begin
			insert into troubleTicket_components (componentLabel,componentCategoryID) values ('#form.componentLabel#',#form.componentCategoryID#)
		end
	</cfquery>

	<cflocation url="index.cfm">

<cfelse>

	<cfquery name="getCats" datasource="#ds#">
		select * from troubleTicket_component_categories 
		order by categorylabel asc 
	</cfquery>

	<div align="center">
	<table border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td class="highlightbar"><b>Add a new Component</b></td>
		</tr>
		<tr>
			<td class="greyrowbottomnopadding">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<cfform method="post" action="add.cfm">
					<tr>
						<td>
							<b>Component Name:</b>&nbsp;<cfinput type="text" name="componentLabel" style="width:300px; vertical-align:middle;" required="yes" message="The component name is required">
						</td>
					</tr>
					<tr>
						<td>
							<b>Component Category:</b>&nbsp;
							<select name="componentCategoryID">
								<option value="0"></option>
								<cfoutput query="getCats">
									<option value="#getCats.componentCategoryID#">#getCats.categoryLabel#</option>
								</cfoutput>
							</select>
						</td>
					</tr>
					<tr>
						<td>
							<input name="btnAddComponent" type="submit" class="sidebar" value="Add Component">
							<input name="btnCancel" type="button" class="sidebar" value="Cancel" onclick="document.location = 'index.cfm';" >
						</td>
					</tr>
					</cfform>
				</table>
			</td>
		</tr>
	</table>
	</div>

</cfif>

