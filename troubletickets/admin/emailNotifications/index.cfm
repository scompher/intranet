
<link rel="stylesheet" type="text/css" href="/styles.css">

<cfif isDefined("form.btnSaveEmails")>
	
	<cfloop list="#form.fieldnames#" index="fieldname">
		<cfif (findnocase("uponcompletion",fieldname) is not 0)>
			<cfset fieldValue = evaluate(fieldname)>
			<cfset did = listgetat(fieldname,2,"_")>
			<cfquery name="updateOnCompletion" datasource="#ds#">
				if not exists (select notificationid from troubleTicket_notificationEmails where departmentid = #did#)
					begin
						insert into troubleTicket_notificationEmails (departmentid, uponCompletion) 
						values (#did#, '#fieldValue#')
					end
				else
					begin
						update troubleTicket_notificationEmails 
						set uponCompletion = '#fieldValue#' 
						where departmentid = #did# 
					end
			</cfquery>
		</cfif>
		<cfif (findnocase("uponoverdue",fieldname) is not 0)>
			<cfset fieldValue = evaluate(fieldname)>
			<cfset did = listgetat(fieldname,2,"_")>
			<cfquery name="updateOnOverdue" datasource="#ds#">
				if not exists (select notificationid from troubleTicket_notificationEmails where departmentid = #did#)
					begin
						insert into troubleTicket_notificationEmails (departmentid, uponOverdue) 
						values (#did#, '#fieldValue#')
					end
				else
					begin
						update troubleTicket_notificationEmails 
						set uponOverdue = '#fieldValue#' 
						where departmentid = #did# 
					end
			</cfquery>
		</cfif>
	</cfloop>
	
	<br />
	<div align="center" class="alert"><b>Updated Successfully</b></div>
	<br />
	
</cfif>
	
<cfquery name="getDepartments" datasource="#ds#">
	select dbo.troubleTicket_departments.departmentid, dbo.troubleTicket_sites.siteLabel + ' - ' + dbo.troubleTicket_departments.departmentLabel as deptLabel, dbo.troubleTicket_notificationEmails.uponCompletion, dbo.troubleTicket_notificationEmails.uponOverdue 
	from dbo.troubleTicket_departments 
	inner join dbo.troubleTicket_sites on dbo.troubleTicket_departments.siteid = dbo.troubleTicket_sites.siteid 
	left join dbo.troubleTicket_notificationEmails on dbo.troubleTicket_departments.departmentid = dbo.troubleTicket_notificationEmails.departmentID 
	order by dbo.troubleTicket_sites.siteLabel, dbo.troubleTicket_departments.departmentLabel
</cfquery>

<div align="center" class="normal">
<cfform method="post" action="index.cfm">
<br />
Separate multiple emails with a comma 
and click &quot;Save Information&quot; when completed <br />
<br />
<table border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Notification Email Administration</b> </td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
			<table border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td nowrap class="linedrowrightcolumn"><b>Department Name </b></td>
					<td nowrap class="linedrowrightcolumn"><b>Email on completion </b></td>
					<td nowrap class="linedrowrightcolumn"><b>Email on overdue </b></td>
				</tr>
				<cfoutput query="getDepartments">
				<tr>
					<td class="linedrowrightcolumn">#deptLabel#</td>
					<td class="linedrowrightcolumn"><cfinput maxlength="255" type="text" style="width:275px;" name="uponCompletion_#departmentid#" value="#uponCompletion#" required="no" message="The #deptLabel# on completion email is required." validateat="onserver"></td>
					<td class="linedrowrightcolumn"><cfinput maxlength="255" type="text" style="width:275px;" name="uponOverdue_#departmentid#" value="#uponOverdue#" required="no" message="The #deptLabel# on overdue email is required." validateat="onserver"></td>
				</tr>
				</cfoutput>
				<tr>
					<td colspan="3">
					<cfinput type="submit" name="btnSaveEmails" value="Save Information">
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</cfform>
<br />
<a class="normal" style="text-decoration:underline;" href="/index.cfm">Return to Intranet</a>
<br />
<br />
</div>

