
<link rel="stylesheet" type="text/css" href="../../styles.css">

<div align="center">
<table width="600" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Administrative Menu</b></td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td><b><a href="sites/">Sites</a></b></td>
				</tr>
				<tr>
					<td class="linedrow">Manage sites here. Add sites, update names or remove a site. </td>
				</tr>
				<tr>
					<td><b><a href="departments/">Departments</a></b></td>
				</tr>
				<tr>
					<td class="linedrow">Manage departments here. Add departments, update or remove. Also assign to a site. </td>
				</tr>
				<tr>
					<td><b><a href="equipment/">Equipment</a></b></td>
				</tr>
				<tr>
					<td class="linedrow">Manage equipment, assign to a site/department, and select which components belong.</td>
				</tr>
				<tr>
					<td><b><a href="components/">Components</a></b></td>
				</tr>
				<tr>
					<td>Manage components that are assigned to equipment. </td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<br />
<a class="normal" href="/index.cfm" style="text-decoration:underline;">Return to Intranet</a>
</div>