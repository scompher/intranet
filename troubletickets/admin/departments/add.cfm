
<link rel="stylesheet" type="text/css" href="/styles.css">

<cfif isDefined("form.btnAddDepartment")>

	<cfquery name="saveSiteInfo" datasource="#ds#">
		if not exists (select departmentid from troubleTicket_departments where siteid = #form.siteid# and departmentLabel = '#form.departmentLabel#')
		begin
			insert into troubleTicket_departments (siteid, departmentLabel) 
			values (#form.siteid#, '#form.departmentLabel#') 
		end
	</cfquery>

	<cflocation url="index.cfm">

<cfelse>

	<cfquery name="getSites" datasource="#ds#">
		select * from troubleTicket_sites 
		order by siteLabel asc 
	</cfquery>

	<div align="center">
	<table border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td class="highlightbar"><b>Add a new Department</b></td>
		</tr>
		<tr>
			<td class="greyrowbottomnopadding">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<form method="post" action="add.cfm">
					<tr>
						<td>
						<b>Site Associated: </b> 
						</td>
						<td>
						<select name="siteid" style="vertical-align:middle;">
							<option value="0"></option>
						<cfoutput query="getsites">
							<option value="#getsites.siteid#">#getsites.sitelabel#</option>
						</cfoutput>
						</select>						
						</td>
					</tr>
					<tr>
						<td>
						<b>Department Name:</b> 
						</td>
						<td>
						<input type="text" name="departmentLabel" style="width:300px;" value="" />
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<input name="btnAddDepartment" type="submit" class="sidebar" value="Add Department">
							<input name="btnCancel" type="button" class="sidebar" value="Cancel" onclick="document.location = 'index.cfm';" >
						</td>
					</tr>
					</form>
				</table>
			</td>
		</tr>
	</table>
	</div>

</cfif>

