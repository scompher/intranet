
<link rel="stylesheet" type="text/css" href="/styles.css">

<cfif isDefined("form.btnUpdateDept")>

	<cfquery name="saveSiteInfo" datasource="#ds#">
		update troubleTicket_departments 
		set siteid = #form.siteid#, departmentLabel = '#form.departmentLabel#' 
		where departmentid = #id# 
	</cfquery>

	<cflocation url="index.cfm">

<cfelse>

	<cfquery name="getSites" datasource="#ds#">
		select * from troubleTicket_sites 
		order by siteLabel asc 
	</cfquery>
	
	<cfquery name="getDepartment" datasource="#ds#">
		select * from troubleTicket_departments 
		where departmentid = #id# 
	</cfquery>

	<div align="center">
	<table border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td class="highlightbar"><b>Edit a Department</b></td>
		</tr>
		<tr>
			<td class="greyrowbottomnopadding">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<form method="post" action="edit.cfm">
					<cfoutput>
					<input type="hidden" name="id" value="#id#" />
					</cfoutput>
					<tr>
						<td>
						<b>Site Associated: </b> 
						</td>
						<td>
						<select name="siteid" style="vertical-align:middle;">
							<option value="0"></option>
						<cfoutput query="getsites">
							<option <cfif getDepartment.siteid is getsites.siteid>selected</cfif> value="#getsites.siteid#">#getsites.sitelabel#</option>
						</cfoutput>
						</select>						
						</td>
					</tr>
					<tr>
						<td>
						<b>Department Name:</b> 
						</td>
						<td>
						<input type="text" name="departmentLabel" style="width:300px;" value="<cfoutput>#getdepartment.departmentlabel#</cfoutput>" />
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<input name="btnUpdateDept" type="submit" class="sidebar" value="Update Department">
							<input name="btnCancel" type="button" class="sidebar" value="Cancel" onclick="document.location = 'index.cfm';" >
						</td>
					</tr>
					</form>
				</table>
			</td>
		</tr>
	</table>
	</div>

</cfif>

