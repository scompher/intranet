
<link rel="stylesheet" type="text/css" href="/styles.css">

<cfparam name="estimatedCompletionDate" default="">
<cfparam name="url.m" default="">

<cfif isDefined("form.btnExitTicket")>
	<cflocation url="tickets.cfm">
</cfif>

<cfif isDefined("form.btnMarkCompleted")>

	<cfquery name="markCompleted" datasource="#ds#">
		update troubleTicket_tickets 
		set currentStatusID = 2, dateTimeCompleted = #createodbcdatetime(now())#, dateTimeLastUpdated = #createodbcdatetime(now())# 
		where ticketid = #tid# 
	</cfquery>
	<cfquery name="updateStatusHistory" datasource="#ds#">
		insert into troubleTicket_status_history (ticketid, statusid, dateTimeUpdated, updatedByID)
		values (#tid#, 2, #createodbcdatetime(now())#, #cookie.adminlogin#) 
	</cfquery>

	<!--- email creator that ticket is closed --->
	<cfquery name="getDepartmentEmail" datasource="#ds#">
		select troubleTicket_tickets.*, troubleTicket_notificationEmails.uponCompletion as email  
		from troubleTicket_tickets 
		inner join troubleTicket_notificationEmails on troubleTicket_tickets.departmentID = troubleTicket_notificationEmails.departmentID 
		where troubleTicket_tickets.ticketid = #tid#  
	</cfquery>
		
	<cfquery name="getTicket" datasource="#ds#">
		select 
			troubleTicket_tickets.*, 
			troubleTicket_sites.siteLabel, 
			troubleTicket_departments.departmentLabel, 
			troubleTicket_equipment.equipmentLabel,
			troubleTicket_equipment.serviceTagNumber, 
			troubleTicket_components.componentid, 
			troubleTicket_components.componentLabel, 
			troubleTicket_status.statusLabel, 
			reportedUsers.firstname + ' ' + reportedUsers.lastname as reportedUser, 
			troubleTicket_components.componentCategoryID 
		from troubleTicket_tickets 
		left join admin_users as reportedUsers on troubleTicket_tickets.whoReportedID = reportedUsers.adminuserid 
		left join troubleTicket_departments on troubleTicket_tickets.departmentid = troubleTicket_departments.departmentid 
		left join troubleTicket_sites on troubleTicket_departments.siteid = troubleTicket_sites.siteid 
		left join troubleTicket_equipment on troubleTicket_tickets.equipmentid = troubleTicket_equipment.equipmentid 
		left join troubleTicket_components on troubleTicket_tickets.componentid = troubleTicket_components.componentid 
		left join troubleTicket_status on troubleTicket_tickets.currentStatusID = troubleTicket_status.statusid 
		where troubleTicket_tickets.ticketid = #tid# 
	</cfquery>
	
	<cfquery name="getTechNotes" datasource="#ds#">
		select troubleTicket_techHistory.*, Admin_Users.firstname + ' ' + Admin_Users.lastname as techName  
		from troubleTicket_techHistory 
		inner join Admin_Users on troubleTicket_techHistory.techID = Admin_Users.adminuserid 
		where ticketid = #tid# 
	</cfquery>
	
<cfif getDepartmentEmail.recordcount gt 0 or getDepartmentEmail.email is not "">
<cfmail from="system.info@copsmonitoring.com" to="#getDepartmentEmail.email#" subject="A trouble ticket created for #getticket.siteLabel# - #getticket.departmentLabel# has been closed.">
A trouble ticket created on #dateformat(getTicket.dateTimeReported,'mm/dd/yyyy')# #timeformat(getTicket.dateTimeReported,'hh:mm tt')# is now closed. 

Department: #getticket.siteLabel# - #getticket.departmentLabel#

Equipment: #getTicket.equipmentLabel#

Component: #getTicket.componentLabel# 

Problem Description: 
#getTicket.problemDescription# 

Technician Notes:
<cfloop query="getTechNotes">
#dateformat(noteDateTime,'mm/dd/yyyy')# #timeformat(noteDateTime,'hh:mm tt')# #techName#:
#techNotes#

</cfloop>

</cfmail>
</cfif>
	
	<cflocation url="tickets.cfm">
	
</cfif>

<cfif isDefined("form.btnUpdateTicket")>

	<cfif trim(form.estimatedCompletionDate) is not "">
		<cfquery name="updateTicketInfo" datasource="#ds#">
			update troubleTicket_tickets 
			set estimatedCompletionDate = '#form.estimatedCompletionDate#', dateTimeLastUpdated = #createodbcdatetime(now())# 
			where ticketid = #tid# 
		</cfquery>
	</cfif>

	<cfif trim(form.techNotes) is not "">
		<cfset noteDateTime = createodbcdatetime(now())>
		<cfquery name="saveTechNote" datasource="#ds#">
			insert into troubleTicket_techHistory (ticketid, techID, techNotes, noteDateTime) 
			values (#form.tid#, #cookie.adminlogin#, '#form.techNotes#', #noteDateTime#)
		</cfquery>
		<cfquery name="updateLastUpdated" datasource="#ds#">
			update troubleTicket_tickets 
			set dateTimeLastUpdated = #createodbcdatetime(now())# 
			where ticketid = #form.tid# 
		</cfquery>
	</cfif>

	<cflocation url="openTicket.cfm?tid=#tid#&m=Ticket Updated Successfully">

</cfif>

<cfquery name="getDepts" datasource="#ds#">
	select troubleTicket_departments.*, troubleTicket_sites.siteLabel 
	from troubleTicket_departments 
	left join troubleTicket_sites on troubleTicket_departments.siteid = troubleTicket_sites.siteid 
	order by siteLabel, departmentLabel 
</cfquery>

<cfquery name="getTicket" datasource="#ds#">
	select 
		troubleTicket_tickets.*, 
		troubleTicket_sites.siteLabel, 
		troubleTicket_departments.departmentLabel, 
		troubleTicket_equipment.equipmentLabel,
		troubleTicket_equipment.serviceTagNumber, 
		troubleTicket_components.componentid, 
		troubleTicket_components.componentLabel, 
		troubleTicket_status.statusLabel, 
		reportedUsers.firstname + ' ' + reportedUsers.lastname as reportedUser, 
		troubleTicket_components.componentCategoryID 
	from troubleTicket_tickets 
	left join admin_users as reportedUsers on troubleTicket_tickets.whoReportedID = reportedUsers.adminuserid 
	left join troubleTicket_departments on troubleTicket_tickets.departmentid = troubleTicket_departments.departmentid 
	left join troubleTicket_sites on troubleTicket_departments.siteid = troubleTicket_sites.siteid 
	left join troubleTicket_equipment on troubleTicket_tickets.equipmentid = troubleTicket_equipment.equipmentid 
	left join troubleTicket_components on troubleTicket_tickets.componentid = troubleTicket_components.componentid 
	left join troubleTicket_status on troubleTicket_tickets.currentStatusID = troubleTicket_status.statusid 
	where troubleTicket_tickets.ticketid = #tid# 
</cfquery>

<cfif getTicket.currentStatusID is 0>
	<!--- mark picked up --->
	<cfquery name="updateStatus" datasource="#ds#">
		update troubleTicket_tickets
		set currentStatusID = 1, dateTimeLastUpdated = #createodbcdatetime(now())#
		where ticketid = #tid# 
	</cfquery>
	<cfquery name="updateStatusHistory" datasource="#ds#">
		insert into troubleTicket_status_history (ticketid, statusid, dateTimeUpdated, updatedByID) 
		values (#tid#, 1, #createodbcdatetime(now())#, #cookie.adminlogin#) 
	</cfquery>
</cfif>

<cfset estimatedCompletionDate = dateformat(getTicket.estimatedCompletionDate,'mm/dd/yyyy')>

<cfset componentIDList = valuelist(getTicket.componentid)> 

<cfquery name="getTechNotes" datasource="#ds#">
	select troubleTicket_techHistory.*, admin_users.firstname + ' ' + admin_users.lastname as techName 
	from troubleTicket_techHistory 
	left join admin_users on troubleTicket_techHistory.techID = admin_users.adminuserid 
	where ticketid = #tid# 
	order by noteDateTime desc 
</cfquery>

<cfquery name="getComponents" datasource="#ds#">
	select * from troubleTicket_components 
	where componentid IN (#getTicket.componentid#)
	order by componentLabel ASC 
</cfquery>

<br />

<div align="center">
<table width="408" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td width="398" class="highlightbar"><b>Open or Update a Ticket</b> </td>
	</tr>
	<cfform method="post" name="ticketform" action="openTicket.cfm">
	<input type="hidden" name="tid" value="<cfoutput>#tid#</cfoutput>" />
	<input type="hidden" name="departmentID" value="<cfoutput>#getTicket.departmentID#</cfoutput>">
	<input type="hidden" name="componentCategoryID" value="<cfoutput>#getTicket.componentCategoryID#</cfoutput>">
	<input type="hidden" name="componentID" value="<cfoutput>#getTicket.componentID#</cfoutput>">
	<input type="hidden" name="dateTimeReported" value="<cfoutput>#getTicket.dateTimeReported#</cfoutput>" />
	<cfinput type="hidden" name="componentIDList" value="#componentIDList#">
	<tr>
		<td class="greyrowbottomnopadding">
		<table border="0" cellspacing="0" cellpadding="5">
			<cfif trim("url.m") is not "">
				<tr>
					<td class="alert" align="center">
					<cfoutput>#url.m#</cfoutput>
					</td>
				</tr>
			</cfif>
			<tr>
				<td>
				<fieldset>
				<legend><b style="color:#000000">Reported Information</b>&nbsp;</legend>
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td nowrap><b>Person Reporting:</b></td>
						<td nowrap><cfoutput>#getTicket.reportedUser#</cfoutput></td>
					</tr>
					<tr>
						<td nowrap><b>Department:</b></td>
						<td nowrap>
							<cfoutput>#getticket.siteLabel# - #getticket.departmentLabel#</cfoutput>
							<!--- 
							<cfselect name="departmentid">
								<option value="0"></option>
								<cfoutput query="getDepts">
									<option <cfif getDepts.departmentid is getTicket.departmentid>selected</cfif> value="#departmentid#">#siteLabel# - #departmentLabel#</option>
								</cfoutput>
							</cfselect>
							--->
						</td>
					</tr>
					<tr>
						<td nowrap><b>Equipment:</b></td>
						<td nowrap>
							<!--- <cfselect bindonload="yes" selected="1" name="equipmentid" bind="cfc:cfc.functions.getEquipment({departmentid},'#ds#')" value="{equipmentid}" display="{equipmentLabel}"></cfselect> --->
							<cfoutput>#getTicket.equipmentLabel#</cfoutput>
						</td>
					</tr>
					<tr>
						<td valign="top" nowrap><b>Component(s):</b></td>
						<td nowrap class="nopadding">
						<!--- <cfdiv style="display:inline; vertical-align:middle" id="componentDisplay" bind="url:cfc/showcomponents.cfm?equipmentid={equipmentid}&cidlist={componentIDList}"></cfdiv> --->
						<cfset clist = "">
						<cfloop query="getcomponents">
							<cfset clist = listappend(clist, getcomponents.componentlabel)>
						</cfloop>
						<cfoutput>#clist#</cfoutput>
						</td>
					</tr>
					<tr>
						<td colspan="2" nowrap><b>Description of Problem: </b></td>
						</tr>
					<tr>
						<td colspan="2">
						<cfoutput>#getTicket.problemDescription#</cfoutput>
						</td>
					</tr>
					<tr>
						<td colspan="2" nowrap>
							<b>Service Tag #: </b>&nbsp;
							<!--- <cfinput class="sidebar" type="submit" name="btnUpdateServiceTag" value="Update" style="vertical-align:middle;"> --->
							<cfoutput>#getTicket.serviceTagNumber#</cfoutput>
						</td>
					</tr>
				</table>
				</fieldset>
				</td>
			</tr>
			<tr>
				<td>
				<fieldset>
				<legend><b style="color:#000000">Technician Fields</b>&nbsp;</legend>
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td class="nopadding">
							<table border="0" cellspacing="0" cellpadding="5">
								<tr>
									<td><b>Estimated Completion Date:</b> (mm/dd/yyyy)</td>
									<td>
										<!--- <cfinput type="datefield" validate="date" message="You must enter a valid date" name="estimatedCompletionDate" style="width:75px;" value="#estimatedCompletionDate#"> --->
										<cfinput type="text" name="estimatedCompletionDate" value="#estimatedCompletionDate#" style="width:75px;" validate="date" message="You must enter a valid date.">
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td><b>Your Tech Notes:</b> </td>
					</tr>
					<tr>
						<td>
							<cftextarea name="techNotes" rows="7" style="width:400px"></cftextarea>
						</td>
					</tr>
					<!--- 
					<tr>
						<td>
						<cfinput class="sidebar" type="submit" name="btnSaveTechNotes" value="Add Note">
						</td>
					</tr>
					--->
					<tr>
						<td class="nopadding">
						<cfif getTechNotes.recordcount gt 0>
						<table width="100%" border="0" cellspacing="0" cellpadding="5">
							<tr>
								<td class="linedrow"><b>Previous Tech Notes:</b></td>
							</tr>
							<cfoutput query="getTechNotes">
							<tr>
								<td><b>Written by #techName# on #dateformat(noteDateTime,'mm/dd/yyyy')# #timeformat(noteDateTime,'hh:mm tt')#</b></td>
							</tr>
							<tr>
								<td class="linedrow">
								#techNotes#
								</td>
							</tr>
							</cfoutput>
						</table>
						</cfif>
						</td>
					</tr>
					<!--- 
					<tr>
						<td><a style="text-decoration:underline;" href="#">Click Here to View Ticket History for Station 5004</a></td>
					</tr>
					--->
					<tr>
						<td>
							<input name="btnUpdateTicket" type="submit" class="sidebar" value="Update Ticket" />
							<input name="btnExitTicket" type="submit" class="sidebar" value="Exit Ticket" />
							<cfif getTechNotes.recordcount gt 0>
								<cfinput onClick="return confirm('Are you sure you wish to mark this item completed?');" name="btnMarkCompleted" type="submit" class="sidebar" value="Mark Completed" />
							<cfelse>
								<cfinput onClick="alert('You cannot close this ticket without at least one technician note.');" name="btnMarkCompleted" type="button" class="sidebar" value="Mark Completed" />
							</cfif>
						</td>
					</tr>
				</table>
				</fieldset>
				</td>
			</tr>
		</table>
		</td>
	</tr>
	</cfform>
</table>
</div>
