
<cfparam name="tid" default="0">

<cfquery name="getTechNotes" datasource="#ds#">
	select troubleTicket_techHistory.*, admin_users.firstname + ' ' + admin_users.lastname as techName 
	from troubleTicket_techHistory 
	left join admin_users on troubleTicket_techHistory.techID = admin_users.adminuserid 
	where ticketid = #tid# 
	order by noteDateTime desc 
</cfquery>

<cfif getTechNotes.recordcount gt 0>
<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td><b>Previous Tech Notes:</b></td>
	</tr>
	<cfoutput query="getTechNotes">
	<tr>
		<td><b>Written by #techName# on #dateformat(noteDateTime,'mm/dd/yyyy')# #timeformat(noteDateTime,'hh:mm tt')#</b></td>
	</tr>
	<tr>
		<td class="linedrow">
		#techNotes#
		</td>
	</tr>
	</cfoutput>
</table>
</cfif>