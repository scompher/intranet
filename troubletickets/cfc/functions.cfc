<cfcomponent>
	<cffunction name="getEquipment" access="remote" returntype="array">
		<cfargument name="departmentid" type="numeric" required="yes">
		<cfargument name="ds" type="string" required="yes">

		<cfquery name="getEquipment" datasource="#ds#">
			select * from troubleTicket_equipment 
			where departmentID = #departmentid# 
			order by equipmentLabel ASC 
		</cfquery>

		<cfset equipmentArray = arraynew(2)>

		<cfset counter = 1>
				
		<cfset equipmentArray[counter][1] = 0>
		<cfset equipmentArray[counter][2] = "">
		
		<cfloop query="getEquipment">
			<cfset counter = counter + 1>
			<cfset equipmentArray[counter][1] = getEquipment.equipmentid>
			<cfset equipmentArray[counter][2] = getEquipment.equipmentlabel>
		</cfloop>

		<cfreturn equipmentArray>
	</cffunction>
	
	<cffunction name="getDepartments" access="remote" returntype="array">
		<cfargument name="siteid" type="numeric" required="yes">
		<cfargument name="ds" type="string" required="yes">

		<cfquery name="getDepartments" datasource="#ds#">
			select * from troubleTicket_departments 
			where siteid = #siteid# 
			order by departmentLabel ASC 
		</cfquery>

		<cfset deptArray = arraynew(2)>

		<cfset counter = 1>
				
		<cfset deptArray[counter][1] = 0>
		<cfset deptArray[counter][2] = "All Departments">
		
		<cfloop query="getDepartments">
			<cfset counter = counter + 1>
			<cfset deptArray[counter][1] = getDepartments.departmentid>
			<cfset deptArray[counter][2] = getDepartments.departmentLabel>
		</cfloop>

		<cfreturn deptArray>
	</cffunction>

	<cffunction name="getEquipments" access="remote" returntype="array">
		<cfargument name="departmentid" type="numeric" required="yes">
		<cfargument name="ds" type="string" required="yes">

		<cfquery name="getEquipments" datasource="#ds#">
			select * from troubleTicket_equipment
			where departmentid = #departmentid# 
			order by equipmentLabel ASC 
		</cfquery>

		<cfset equipArray = arraynew(2)>

		<cfset counter = 1>

		<cfset equipArray[counter][1] = 0>
		<cfset equipArray[counter][2] = "All Equipment">

		<cfloop query="getEquipments">
			<cfset counter = counter + 1>
			<cfset equipArray[counter][1] = getEquipments.equipmentID>
			<cfset equipArray[counter][2] = getEquipments.equipmentLabel>
		</cfloop>

		<cfreturn equipArray>
	</cffunction>
	
	<cffunction name="saveTechNote" access="remote" returntype="void">
		<cfargument name="ticketID" type="numeric" required="yes">
		<cfargument name="techID" type="numeric" required="yes">
		<cfargument name="techNotes" type="string" required="yes">
		<cfargument name="ds" type="string" required="yes">
		<!--- 		
		<cfset noteDateTime = createodbcdatetime(now())>
		
		<cfquery name="saveTechNote" datasource="#ds#">
			insert into troubleTicket_techHistory (ticketid, techID, techNotes, noteDateTime) 
			values (#ticketID#, #techID#, '#techNotes#', #noteDateTime#)
		</cfquery>
		--->
	</cffunction>
</cfcomponent>
