<cfif equipmentid is not "" and equipmentid is not 0>
	<cfif not isDefined("cidlist")>
		<cfset cidlist = "">
	</cfif>
	<cfquery name="getComponents" datasource="#ds#">
		select * from troubleTicket_components 
		where componentid IN (select componentid from troubleTicket_equipment_component_lookup where equipmentid = #equipmentid#)
		order by componentLabel ASC 
	</cfquery>
	<table border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td class="nopadding">
				<table border="0" cellspacing="0" cellpadding="2">
					<cfoutput query="getComponents">
					<cfif getComponents.currentRow LTE (getComponents.recordCount / 2)>
					<tr>
						<td><input <cfif listfindnocase(cidlist, componentid) is not 0>checked</cfif> type="checkbox" name="componentid" value="#componentid#" /></td>
						<td>#componentLabel#</td>
					</tr>
					</cfif>
					</cfoutput>
				</table>
			</td>
			<td>&nbsp;&nbsp;</td>
			<td class="nopadding">
				<table border="0" cellspacing="0" cellpadding="2">
					<cfoutput query="getComponents">
					<cfif getComponents.currentRow GT (getComponents.recordCount / 2)>
					<tr>
						<td><input <cfif listfindnocase(cidlist, componentid) is not 0>checked</cfif> type="checkbox" name="componentid" value="#componentid#" /></td>
						<td>#componentLabel#</td>
					</tr>
					</cfif>
					</cfoutput>
				</table>
			</td>
		</tr>
	</table>
</cfif>