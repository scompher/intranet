<cfif equipmentid is not "" and equipmentid is not 0>
	<cfif not isDefined("cidlist")>
		<cfset cidlist = "">
	</cfif>
	<cfquery name="getComponents" datasource="#ds#">
		select * from troubleTicket_components 
		where componentid IN (select componentid from troubleTicket_equipment_component_lookup where equipmentid = #equipmentid#)
		order by componentLabel ASC 
	</cfquery>
	<tr>
		<td nowrap><b>Component(s):</b></td>
		<td nowrap class="nopadding">
		<select name="componentid">
			<cfoutput query="getComponents">
			<option value="#componentid#">#componentLabel#</option>
			</cfoutput>
		</select>
		</td>
	</tr>
	<tr>
		<td colspan="2" nowrap><b>Description of Problem with this component: </b></td>
	</tr>
	<tr>
		<td colspan="2">
			<textarea name="componentProblemDescription" rows="3" style="width:400px;"></textarea>
		</td>
	</tr>
</cfif>