
<!--- email list --->
<cfset njEmailList = "pgregory@copsmonitoring.com,peichfeld@copsmonitoring.com">
<cfset flEmailList = "pgregory@copsmonitoring.com,technologyrequest@copsmonitoring.com,jhall@copsmonitoring.com">
<cfset azEmailList = "pgregory@copsmonitoring.com,technologyrequest@copsmonitoring.com,sbeeman@copsmonitoring.com">
<cfset txEmailList = "pgregory@copsmonitoring.com,technologyrequest@copsmonitoring.com,ktallman@copsmonitoring.com">

<cfset njEmailList = "helpdesk@copsmonitoring.com">
<cfset flEmailList = "helpdesk@copsmonitoring.com">
<cfset azEmailList = "helpdesk@copsmonitoring.com">
<cfset txEmailList = "helpdesk@copsmonitoring.com">

<cfif not isdefined("cookie.adminlogin")>
	<cfcookie name="adminlogin" value="0">
</cfif>

<link rel="stylesheet" type="text/css" href="/styles.css">

<cffunction name="sendNotificationEmail" access="private" returntype="void">
	<cfargument name="emailList" type="string" required="yes">
	<cfargument name="ticketid" type="numeric" required="yes">
	<cfmail type="html" from="system.info@copsmonitoring.com" to="#emailList#" subject="A new equipment trouble ticket has been submitted" username="system" password="V01c3">

	<link rel="stylesheet" type="text/css" href="http://www.copalink.com/intranetStyles.css">

	<div class="normal">
	#dateformat(now(),'mm/dd/yyyy')# #timeformat(now(),'hh:mm tt')# 
	<br />
	<br />
	A new equipment trouble ticket has been created.  Details below:
	<br />
	<br />
	<cfset tid = ticketid>
	<cfinclude template="viewTicket.cfm">
	</div>
	</cfmail>
</cffunction>

<cfif isDefined("form.btnSaveComponent")>
	<cfif trim(form.componentProblemDescription) is not "">
		<cfset id = listlen(componentList,"|") + 1>
		<cfset componentElement = "#form.componentid#~#form.componentProblemDescription#~#id#">
		<cfif listfindnocase(componentList,componentElement,"|") is 0>
			<cfset componentList = listappend(componentList,componentElement,"|")>
		</cfif>
	</cfif>
	<cfset form.componentid = 0>
	<cfset form.componentProblemDescription = "">
	<cfset x = structDelete(form,"btnAddComponent")>
</cfif>

<cfif isDefined("form.btnRemoveItem")>
	<cfset newList = "">
	<cfset componentList = urldecode(componentlist)>
	<cfloop list="#componentList#" index="component" delimiters="|">
		<cfset compid = listgetat(component,1,"~")>
		<cfset desc = listgetat(component,2,"~")>
		<cfset id = listgetat(component,3,"~")>
		<cfif id is not removeID>
			<cfset listElement = "#compid#~#desc#~#id#">
			<cfset newList = listappend(newList,listElement,"|")>
		</cfif>	
	</cfloop>
	<cfset componentList = newList>
</cfif>

<cfif isDefined("form.btnCreateTicket")>
	
	<cfset dateTimeReported = createodbcdatetime(now())>
	<cfset dateTimeLastUpdated = createodbcdatetime(now())>

	<cfquery name="getDept" datasource="#ds#">
		select troubleTicket_departments.departmentLabel, troubleTicket_sites.siteLabel
		from troubleTicket_departments
		inner join troubleTicket_sites on troubleTicket_departments.siteid = troubleTicket_sites.siteID
		where troubleTicket_departments.departmentid = #form.departmentID# 
	</cfquery>

	<cfset componentList = urldecode(componentlist)>
	<cfloop list="#componentList#" delimiters="|" index="component">
		<cfset compid = listgetat(component,1,"~")>
		<cfset desc = listgetat(component,2,"~")>
		<cfset id = listgetat(component,3,"~")>
		<cfquery name="saveTicketInfo" datasource="#ds#">
			insert into troubleTicket_tickets (dateTimeReported, equipmentID, problemDescription, whoReportedID, currentStatusID, dateTimeLastUpdated, departmentID, componentID)
			values (#dateTimeReported#, #form.equipmentID#, '#desc#', #cookie.adminlogin#, 0, #dateTimeLastUpdated#, #form.departmentID#, #compid#) 
		</cfquery>
		<cfquery name="getTicketID" datasource="#ds#">
			select max(ticketid) as tid 
			from troubleTicket_tickets 
			where dateTimeReported = #dateTimeReported# and equipmentID = #form.equipmentID# and whoReportedID = #cookie.adminlogin# and departmentID = #form.departmentID# and componentID = #compid# 
		</cfquery>
		<cfset ticketid = getTicketID.tid>

		<cfswitch expression="#getDept.siteLabel#">
			<cfcase value="New Jersey">
				<cfset emailList = njEmailList>
			</cfcase>
			<cfcase value="Arizona">
				<cfset emailList = azEmailList>
			</cfcase>
			<cfcase value="Florida">
				<cfset emailList = flEmailList>
			</cfcase>
			<cfcase value="Texas">
				<cfset emailList = txEmailList>
			</cfcase>
		</cfswitch>

		<cfscript>
			sendNotificationEmail(emailList,ticketid);
		</cfscript>
	</cfloop>
	
	<cfif trim(componentlist) is not "">
		<cfif cookie.adminLogin is 0>
			<div align="center" class="normal">
			<br />
			<br />
			<br />
			Thank you for submitting a trouble ticket.  Please <a style="text-decoration:underline;" href="javascript:self.close();">click here</a> to close this browser window.
			</div>
		<cfelse>
			<cflocation url="tickets.cfm">
		</cfif>
	<cfelse>
		<div align="center" class="normal">
		<br />
		<br />
		<br />
		You have not selected any affected equipment that needs fixing.  Please <a href="javascript:history.go(-1);">return and correct</a>. 
		</div>
	</cfif>
	
<cfelse>
	
	<cfparam name="form.departmentid" default="0">
	<cfparam name="form.equipmentid" default="0">
	<cfparam name="form.componentid" default="0">
	<cfparam name="form.componentlist" default="">
	
	<cfquery name="getDepts" datasource="#ds#">
		select troubleTicket_departments.*, troubleTicket_sites.siteLabel 
		from troubleTicket_departments 
		left join troubleTicket_sites on troubleTicket_departments.siteid = troubleTicket_sites.siteid 
		order by siteLabel, departmentLabel 
	</cfquery>
	
	<cfquery name="getEquipment" datasource="#ds#">
		select * from troubleTicket_equipment 
		where departmentID = #form.departmentid# 
		order by equipmentLabel ASC 
	</cfquery>
	
	<cfquery name="getComponents" datasource="#ds#">
		select * from troubleTicket_components 
		where componentid IN (select componentid from troubleTicket_equipment_component_lookup where equipmentid = #form.equipmentid#)
		order by componentLabel ASC 
	</cfquery>
	
	<cfif form.componentid is 0>
		<cfif getComponents.recordcount is 1>
			<cfset form.componentid = getComponents.componentid>
		</cfif>
	</cfif>
	
	<div align="center">
	<table width="400" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td class="highlightbar"><b>New Ticket Creation</b> </td>
		</tr>

		<tr>
			<td class="greyrowbottomnopadding">
			<cfform method="post" action="newticket.cfm">
			<input type="hidden" name="componentlist" value="<cfoutput>#urlencodedformat(componentlist)#</cfoutput>">
			<input type="hidden" name="removeID" value="0" />
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td width="1%" nowrap><b>Department:</b></td>
					<td nowrap>
						<select name="departmentid" onChange="this.form.submit();">
							<option value="0">Please select a department</option>
							<cfoutput query="getDepts">
								<option <cfif getDepts.departmentid is form.departmentid>selected</cfif> value="#departmentid#">#siteLabel# - #departmentLabel#</option>
							</cfoutput>
						</select>
					</td>
				</tr>
				<cfif getEquipment.recordcount gt 0>
				<tr>
					<td nowrap><b>Equipment:</b></td>
					<td nowrap>
						<select name="equipmentid" onchange="this.form.submit();">
							<option value="0">Please select affected equipment</option>
							<cfoutput query="getEquipment">
								<option <cfif form.equipmentid is getEquipment.equipmentid>selected</cfif> value="#equipmentid#">#equipmentLabel#</option>
							</cfoutput>
						</select>
					</td>
				</tr>
				</cfif>
				<cfif form.equipmentid is not 0>
				<tr>
					<td colspan="2">
					<fieldset>
					<cfif getcomponents.recordcount gt 1>
						<legend style="font-weight:bold; color:#000000;">Enter problem information:&nbsp;</legend>
					<cfelse>
						<legend style="font-weight:bold; color:#000000;">Enter Description of Problem:&nbsp;</legend>
					</cfif>
					<table border="0" cellspacing="0" cellpadding="5">
						<cfif getComponents.recordcount gt 0>
							<cfif getComponents.recordcount gt 1>
							<tr>
								<td>
								<b>Please Select Affected Item: </b><br />
								<select name="componentid" onchange="this.form.submit();" style="vertical-align:middle; margin-top:.5em;">
									<option value="0">Please select affected item(s)</option>
									<cfoutput query="getComponents">
										<option <cfif form.componentid is getComponents.componentid>selected</cfif> value="#componentid#">#componentLabel#</option>
									</cfoutput>
								</select>
								</td>
							</tr>
							<cfelse>
							<input type="hidden" name="componentid" value="<cfoutput>#getComponents.componentid#</cfoutput>" />
							</cfif>
							<input type="hidden" name="btnAddComponent" value="1" />
						</cfif>
						<cfif componentid is not 0>
							<tr>
								<td>
								<cfif getcomponents.recordcount gt 1>
								<b>Description of problem with selected item:</b>
								</cfif>
								</td>
							</tr>
							<tr>
								<td>
								<cftextarea required="yes" message="The problem description is required." name="componentProblemDescription" style="width:300px;" rows="2"></cftextarea>
								</td>
							</tr>
							<cfif getcomponents.recordcount gt 1>
							<tr>
								<td>
								<cfinput type="submit" name="btnSaveComponent" value="Save Problem Description" class="sidebar" />
								</td>
							</tr>
							<cfelse>
								<input type="hidden" name="btnSaveComponent" value="Save Problem Description">
							</cfif>
						<cfelse>
						
						</cfif>
						<cfif listlen(componentList,"|") gt 0>
							<tr>
								<td class="nopadding">
								<table border="0" cellspacing="0" cellpadding="5">
								<cfloop list="#componentList#" index="component" delimiters="|">
									<cfset compid = listgetat(component,1,"~")>
									<cfset desc = listgetat(component,2,"~")>
									<cfset id = listgetat(component,3,"~")>
									<cfquery name="getComponentName" datasource="#ds#">
										select componentLabel from troubleTicket_components with (nolock) 
										where componentid = #compid# 
									</cfquery>
									<cfoutput>
										<tr>
											<td nowrap="nowrap"><b>#getComponentName.componentLabel#:</b></td>
											<td>#desc#</td>
											<td><input type="submit" onclick="this.form.removeID.value = #id#" name="btnRemoveItem" value="Remove" class="sidebarsmall" style="vertical-align:middle;" /></td>
										</tr>
									</cfoutput>
								</cfloop>
								</table>
								</td>
							</tr>
						</cfif>
					</table>
					</fieldset>
					</td>
				</tr>
				</cfif>
				<tr>
					<td colspan="2">
						<cfif listlen(componentList,"|") gt 0 or getComponents.recordcount is 1>
							<input name="btnCreateTicket" type="submit" class="sidebar" value="Submit Ticket">
						</cfif>
						<cfif cookie.adminlogin is not 0>
							<input name="btnCancel" type="button" class="sidebar" onclick="document.location = 'tickets.cfm';" value="Cancel">
						<cfelse>
							<input name="btnCancel" type="button" class="sidebar" onclick="window.close();" value="Cancel">
						</cfif>
					</td>
				</tr>
			</table>
			</cfform>
			</td>
		</tr>
	</table>
	</div>

</cfif>