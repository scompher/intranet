
<cfsetting showdebugoutput="no">

<link rel="stylesheet" type="text/css" href="/styles.css">

<cfquery name="getTicket" datasource="#ds#">
	select 
		troubleTicket_tickets.ticketid, 
		troubleTicket_tickets.dateTimeReported, 
		troubleTicket_tickets.estimatedCompletionDate, 
		troubleTicket_tickets.currentStatusID, 
		troubleTicket_tickets.problemDescription, 
		troubleTicket_sites.siteLabel, 
		troubleTicket_departments.departmentLabel, 
		troubleTicket_equipment.equipmentLabel, 
		troubleTicket_equipment.serviceTagNumber, 
		troubleTicket_components.componentLabel, 
		troubleTicket_status.statusLabel, 
		troubleTicket_tickets.dateTimeLastUpdated, 
		reportedUsers.firstname + ' ' + reportedUsers.lastname as reportedUser 
	from troubleTicket_tickets 
	left join admin_users as reportedUsers on troubleTicket_tickets.whoReportedID = reportedUsers.adminuserid 
	left join troubleTicket_departments on troubleTicket_tickets.departmentid = troubleTicket_departments.departmentid 
	left join troubleTicket_sites on troubleTicket_departments.siteid = troubleTicket_sites.siteid 
	left join troubleTicket_equipment on troubleTicket_tickets.equipmentid = troubleTicket_equipment.equipmentid 
	left join troubleTicket_components on troubleTicket_tickets.componentid = troubleTicket_components.componentid 
	left join troubleTicket_status on troubleTicket_tickets.currentStatusID = troubleTicket_status.statusid 
	where troubleTicket_tickets.ticketid = #tid# 
</cfquery>

<cfquery name="getTechNotes" datasource="#ds#">
	select troubleTicket_techHistory.*, admin_users.firstname + ' ' + admin_users.lastname as techName 
	from troubleTicket_techHistory 
	left join admin_users on troubleTicket_techHistory.techID = admin_users.adminuserid 
	where ticketid = #tid# 
	order by noteDateTime desc 
</cfquery>

<cfquery name="getStatusHistory" datasource="#ds#">
	select troubleTicket_status_history.dateTimeUpdated, admin_users.firstname + ' ' + admin_users.lastname as updatedByName, troubleTicket_status.statusLabel  
	from troubleTicket_status_history 
	left join admin_users on troubleTicket_status_history.updatedByID = admin_users.adminuserid 
	left join troubleTicket_status on troubleTicket_status_history.statusid = troubleTicket_status.statusid 
	where troubleTicket_status_history.ticketid = #tid# 
	order by troubleTicket_status_history.dateTimeUpdated asc 
</cfquery>

<div align="center">
<table width="425" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>View Ticket #<cfoutput>#getTicket.ticketid#</cfoutput></b></td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td>
					<fieldset>
					<legend><b>Status History</b></legend>
					<table width="100%" border="0" cellspacing="0" cellpadding="3">
						<tr>
							<td>
							<cfoutput>
							At #dateformat(getTicket.dateTimeReported,'mm/dd/yyyy')# #timeformat(getTicket.dateTimeReported,'hh:mm tt')# ticket was created.
							</cfoutput>
							</td>
						</tr>
						<cfoutput query="getStatusHistory">
						<tr>
							<td>
							At #dateformat(dateTimeUpdated,'mm/dd/yyyy')# #timeformat(dateTimeUpdated,'hh:mm tt')# #updatedByName# #statusLabel# ticket.
							</td>
						</tr>
						</cfoutput>
					</table>
					</fieldset>					
					</td>
				</tr>
				<tr>
					<td>
					<fieldset>
					<legend><b>Ticket and Equipment Information</b></legend>
					<table width="100%" border="0" cellspacing="0" cellpadding="5">
						<cfoutput query="getTicket" group="ticketid">
						<cfset componentList = "">
						<cfoutput>
							<cfset componentList = listappend(componentList,componentLabel,", ")>
						</cfoutput>
						<tr>
							<td><b>Reported by #reportedUser# on #dateformat(dateTimeReported, 'mm/dd/yyyy')# at #timeformat(dateTimeReported, 'hh:mm tt')#</b></td>
						</tr>
						<tr>
							<td><b>Estimated Completion Date:</b> #dateformat(estimatedCompletionDate, 'mm/dd/yyyy')#</td>
						</tr>
						<tr>
							<td><b>Site:</b> #siteLabel#</td>
						</tr>
						<tr>
							<td><b>Department:</b> #departmentLabel#</td>
						</tr>
						<tr>
							<td><b>Service Tag ##:</b> #serviceTagNumber#</td>
						</tr>
						<tr>
							<td><b>Equipment:</b> #equipmentLabel#</td>
						</tr>
						<tr>
							<td><b>Component:</b> #componentList# </td>
						</tr>
						<tr>
							<td><b>Description of Problem: </b></td>
						</tr>
						<tr>
							<td>#problemDescription#</td>
						</tr>
						</cfoutput>
					</table>
					</fieldset>
					</td>
				</tr>
				<cfif getTechNotes.recordcount gt 0>
				<tr>
					<td>
					<fieldset>
					<legend><b>Technician Information</b></legend>
					<table width="100%" border="0" cellspacing="0" cellpadding="5">
						<cfoutput query="getTechNotes">
						<tr>
							<td><b>Written up by #techName# on #dateformat(noteDateTime,'mm/dd/yyyy')# at #timeformat(noteDateTime,'hh:mm tt')# </b></td>
						</tr>
						
						<tr>
							<td><b>Tech Notes:</b> </td>
						</tr>
						<tr>
							<td class="linedrow">
							#replace(techNotes, chr(13), "<br />", "all")# 
							</td>
						</tr>
						</cfoutput>
					</table>
					</fieldset>
					</td>
				</tr>
				</cfif>
				<!--- 
				<form method="post" action="openTicket.cfm">
				<input type="hidden" name="tid" value="<cfoutput>#tid#</cfoutput>" />
				<tr>
					<td>
					<input type="submit" name="btnPickUp" value="Pick Up Ticket" class="sidebar" />
					<input type="submit" name="btnPickUp" value="Modify Ticket" class="sidebar" />
					</td>
				</tr>
				</form>
				--->
			</table>
		</td>
	</tr>
</table>
</div>
