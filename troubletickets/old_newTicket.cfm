
<link rel="stylesheet" type="text/css" href="/styles.css">

<cfif isDefined("form.btnCreateTicket")>

	<cfset dateTimeReported = createodbcdatetime(now())>
	<cfset dateTimeLastUpdated = createodbcdatetime(now())>

	<cfquery name="saveTicketInfo" datasource="#ds#">
		insert into troubleTicket_tickets (dateTimeReported, equipmentID, problemDescription, whoReportedID, currentStatusID, dateTimeLastUpdated, departmentID)
		values (#dateTimeReported#, #form.equipmentID#, '#form.problemDescription#', #cookie.adminlogin#, 0, #dateTimeLastUpdated#, #form.departmentID#) 
	</cfquery>
	<cfquery name="getID" datasource="#ds#">
		select max(ticketid) as newid 
		from troubleTicket_tickets 
		where whoReportedID = #cookie.adminLogin# 
	</cfquery>
	<cfset ticketid = getid.newid>
	
	<cfloop list="#form.componentid#" index="cid">
		<cfquery name="insertIntoLookup" datasource="#ds#">
			insert into troubleTicket_ticket_component_lookup (ticketid, componentid) 
			values (#ticketid#, #cid#) 
		</cfquery>
	</cfloop>
	
	<cflocation url="tickets.cfm">
	
<cfelse>
	
	<cfquery name="getDepts" datasource="#ds#">
		select troubleTicket_departments.*, troubleTicket_sites.siteLabel 
		from troubleTicket_departments 
		left join troubleTicket_sites on troubleTicket_departments.siteid = troubleTicket_sites.siteid 
		order by siteLabel, departmentLabel 
	</cfquery>
	
	<div align="center">
	<table border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td class="highlightbar"><b>New Ticket Creation</b> </td>
		</tr>
		<tr>
			<td class="greyrowbottomnopadding">
			<cfform method="post" action="newticket.cfm">
			<table border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td nowrap><b>Department:</b></td>
					<td nowrap>
						<cfselect name="departmentid">
							<option value="0"></option>
							<cfoutput query="getDepts">
								<option value="#departmentid#">#siteLabel# - #departmentLabel#</option>
							</cfoutput>
						</cfselect>
					</td>
				</tr>
				<tr>
					<td nowrap><b>Equipment:</b></td>
					<td nowrap>
						<cfselect name="equipmentid" bind="cfc:cfc.functions.getEquipment({departmentid},'#ds#')" value="{equipmentid}" display="{equipmentLabel}"></cfselect>
					</td>
				</tr>
				<!--- 
				<tr>
					<td nowrap><b>Component(s):</b></td>
					<td nowrap class="nopadding">
					<!--- 
					<cfdiv style="display:inline; vertical-align:middle" id="componentDisplay" bind="url:cfc/showcomponents.cfm?equipmentid={equipmentid}"></cfdiv>
					--->
					<cfdiv style="display:inline; vertical-align:middle" id="componentDisplay" bind="url:cfc/selectComponent.cfm?equipmentid={equipmentid}"></cfdiv>
					</td>
				</tr>
				<tr>
					<td colspan="2" nowrap><b>Description of Problem: </b></td>
				</tr>
				<tr>
					<td colspan="2">
						<textarea name="problemDescription" rows="3" style="width:400px;"></textarea>
					</td>
				</tr>
				--->
				<tr>
					<td colspan="2">
						<input name="btnCreateTicket" type="submit" class="sidebar" value="Submit Ticket">
						<input name="btnCancel" type="button" class="sidebar" value="Cancel">
					</td>
				</tr>
			</table>
			</cfform>
			</td>
		</tr>
	</table>
	</div>

</cfif>