
<link rel="stylesheet" type="text/css" href="../../styles.css">

<cfparam name="form.filterSiteID" default="0">
<cfparam name="form.filterDepartmentID" default="0">
<cfparam name="form.startDate" default="">
<cfparam name="form.endDate" default="">
<cfparam name="form.filterStatusID" default="0,1,2">

<cfif isDefined("url.viewDetail")>
	<cfset form.filterStatusID = url.filterStatusID>
	<cfset form.filterSiteID = url.filterSiteID>
	<cfset form.filterDepartmentID = url.filterDepartmentID>
	<cfset form.startDate = url.startDate>
	<cfset form.endDate = url.endDate>
	<cfset form.filterEquipmentID = url.filterEquipmentID>
	<cfset form.btnRunReport = 1>
</cfif>

<cfquery name="getAllStatus" datasource="#ds#">
	select * from troubleTicket_status 
	order by statusid ASC 
</cfquery>

<cfquery name="getAllSites" datasource="#ds#">
	select * from troubleTicket_sites 
	order by siteLabel ASC 
</cfquery>

<cfquery name="getAllDepartments" datasource="#ds#">
	select * from troubleTicket_departments 
	order by departmentLabel ASC 
</cfquery>

<br>

<div align="center" class="normal" style="position:relative;z-index:0">
<cfform method="post" action="equipmentSummary.cfm">
<table width="400" border="0" cellspacing="0" cellpadding="5">
    <tr>
        <td class="highlightbar"><b>report header here </b></td>
    </tr>
    <tr class="greyrowbottomnopadding">
        <td class="greyrowbottomnopadding">
		<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td width="23%" nowrap>Starting Date:</td>
				<td width="77%" style="z-index:3; position:relative;">
				<cfinput type="datefield" name="startDate" validate="date" style="width:75px;" value="#form.startDate#">
				</td>
			</tr>
			<tr>
			    <td nowrap>Ending Date:</td>
			    <td style="z-index:2; position:relative;">
				<cfinput type="datefield" name="endDate" validate="date" style="width:75px;" value="#form.endDate#">
				</td>
		    </tr>
			<tr>
			    <td nowrap>Site:</td>
			    <td>
					<cfselect name="filterSiteID">
						<option value="0">All Site</option>
						<cfoutput query="getAllSites">
							<option <cfif form.filterSiteID is getAllSites.siteid>selected</cfif> value="#siteid#">#siteLabel#</option>
						</cfoutput>
					</cfselect>
				</td>
		    </tr>
			<tr>
			    <td nowrap>Department:</td>
			    <td>
					<cfselect bindonload="yes" selected="#form.filterDepartmentID#" name="filterDepartmentID" bind="cfc:troubletickets.cfc.functions.getDepartments({filterSiteID},'#ds#')" value="{departmentid}" display="{departmentLabel}"></cfselect>
				</td>
		    </tr>
			<tr>
			    <td nowrap>Status:</td>
			    <td>
					<cfoutput query="getAllStatus">
					<input <cfif listfindnocase(form.filterStatusID,getAllStatus.statusid) is not 0>checked</cfif> style="vertical-align:middle;" type="checkbox" name="filterStatusID" value="#statusid#" />#statusLabel#&nbsp;
					</cfoutput>
				</td>
		    </tr>
			<tr>
			    <td colspan="2">
			        <input name="btnRunReport" type="submit" class="sidebarsmall" value="Run Report">
			        <input name="btnNewSearch" type="button" onClick="document.location = 'equipmentSummary.cfm';" class="sidebarsmall" value="New Search">
			    </td>
			    </tr>
		</table>
		</td>
    </tr>
</table>
</cfform>
</div>

<cfif isDefined("form.btnRunReport")>
	<br>
	<cfinclude template="report_equipmentSummary.cfm">
</cfif>

<br>
<div align="center" class="normal">
<a href="/index.cfm" style="text-decoration:underline;">Return to Intranet</a>
<br />
<br />
</div>
