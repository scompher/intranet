<link rel="stylesheet" type="text/css" href="../../styles.css">

<cfif trim(form.startDate) is not "">
	<cfif not isDate(form.startDate)>
		<cfset form.startDate = "">
	</cfif>
</cfif>
<cfif (form.endDate) is not "">
	<cfif not isDate(form.endDate)>
		<cfset form.endDate = "">
	<cfelse>
		<cfif form.endDate is form.startDate>
			<cfset form.endDate = dateadd("d",1,form.endDate)>
		</cfif>
	</cfif>
</cfif>

<!--- get summary --->
<cfquery name="getSummary" datasource="#ds#">
	select distinct 
		troubleTicket_sites.siteLabel, 
		troubleTicket_departments.departmentLabel, 
		troubleTicket_equipment.equipmentLabel, 
		troubleTicket_equipment.equipmentID, 
		count(troubleTicket_tickets.ticketid) as ticketCount 
	from troubleTicket_tickets 
	left join troubleTicket_departments on troubleTicket_tickets.departmentid = troubleTicket_departments.departmentid 
	left join troubleTicket_sites on troubleTicket_departments.siteid = troubleTicket_sites.siteid 
	left join troubleTicket_equipment on troubleTicket_tickets.equipmentid = troubleTicket_equipment.equipmentid 
	where 
		<cfif trim(form.startDate) is not "">
			troubleTicket_tickets.dateTimeReported >= #createodbcdatetime(form.startDate)# and 
		</cfif>
		<cfif trim(form.endDate) is not "">
			troubleTicket_tickets.dateTimeReported < #createodbcdatetime(form.endDate)# and 
		</cfif>
		<cfif form.filterStatusID gte 0>
			troubleTicket_tickets.currentStatusID IN (#form.filterStatusID#) and 
		</cfif>
		<cfif form.filterSiteID gt 0>
			troubleTicket_sites.siteid = #form.filterSiteID# and 
		</cfif>
		<cfif form.filterDepartmentID gt 0>
			troubleTicket_tickets.departmentid = #form.filterDepartmentID# and 
		</cfif>
		1=1 
	group by 
		troubleTicket_sites.siteLabel, 
		troubleTicket_departments.departmentLabel, 
		troubleTicket_equipment.equipmentLabel, 
		troubleTicket_equipment.equipmentID 
	order by 
	ticketCount desc 
</cfquery>

<!---
old order by clauses
troubleTicket_sites.siteLabel, 
troubleTicket_departments.departmentLabel, 
troubleTicket_equipment.equipmentLabel 
--->

<div align="center">
<table width="600" border="0" cellspacing="0" cellpadding="5">
    <tr>
        <td colspan="4" align="center" class="highlightbar">
		<cfoutput>
		<b>Equipment Summary <cfif trim(form.startDate) is not "">from #dateformat(form.startDate,'mm/dd/yyyy')# </cfif> <cfif trim(form.endDate) is not "">to #dateformat(form.endDate,'mm/dd/yyyy')#<cfelse>to #dateformat(now(),'mm/dd/yyyy')#</cfif></b>
		</cfoutput>
		</td>
    </tr>
	<tr>
		<td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td width="10%" align="center" nowrap class="linedrowrightcolumn"><b>Site</b></td>
					<td width="21%" nowrap class="linedrowrightcolumn"><b>Department</b></td>
					<td width="47%" nowrap class="linedrowrightcolumn"><b>Equipment</b></td>
					<td width="22%" nowrap class="linedrow"><b>Tickets Generated </b></td>
				</tr>
				<cfoutput query="getSummary">
				<tr>
					<td align="center" nowrap class="linedrowrightcolumn">#getSummary.siteLabel#</td>
					<td nowrap class="linedrowrightcolumn">#getSummary.departmentLabel#&nbsp;</td>
					<td nowrap class="linedrowrightcolumn">#getSummary.equipmentLabel#&nbsp;</td>
					<td class="linedrowrightcolumn"><a style="text-decoration:underline;" href="../tickets.cfm?viewDetail=1&startDate=#form.startDate#&endDate=#form.endDate#&filterStatusID=#form.filterStatusID#&filterDepartmentID=#form.filterDepartmentID#&filterSiteID=#form.filterSiteID#&filterEquipmentID=#getSummary.equipmentID#">#getSummary.ticketCount#</a></td>
				</tr>				
				</cfoutput>
				<!---
				<tr>
					<td align="center" nowrap class="linedrowrightcolumn">NJ</td>
					<td nowrap class="linedrowrightcolumn">Operations</td>
					<td nowrap class="linedrowrightcolumn">Station 5001 </td>
					<td class="linedrow">23</td>
				</tr>
				<tr>
					<td align="center" nowrap class="linedrowrightcolumn">NJ</td>
					<td nowrap class="linedrowrightcolumn">Operations</td>
					<td nowrap class="linedrowrightcolumn">Station 5002 </td>
					<td class="linedrow">1</td>
				</tr>
				<tr>
					<td align="center" nowrap class="linedrowrightcolumn">NJ</td>
					<td nowrap class="linedrowrightcolumn">Data Entry </td>
					<td nowrap class="linedrowrightcolumn">Copier</td>
					<td class="linedrow">5</td>
				</tr>
				<tr>
					<td align="center" nowrap class="linedrowrightcolumn">FL</td>
					<td nowrap class="linedrowrightcolumn">Operations</td>
					<td nowrap class="linedrowrightcolumn">Station 5003 </td>
					<td class="linedrow">10</td>
				</tr>
				<tr>
					<td align="center" nowrap class="linedrowrightcolumn">FL</td>
					<td nowrap class="linedrowrightcolumn">Operations</td>
					<td nowrap class="linedrowrightcolumn">Station 5023 </td>
					<td class="linedrow">3</td>
				</tr>
				--->
			</table>
		</td>
	</tr>
</table>
</div>
