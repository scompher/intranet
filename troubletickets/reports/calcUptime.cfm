<table  border="0" cellspacing="0" cellpadding="5">

<cfquery name="getData" datasource="#ds#">
	select 
		troubleTicket_tickets.*, 
		troubleTicket_departments.departmentLabel, 
		troubleTicket_sites.siteLabel, 
		troubleTicket_component_categories.categoryLabel, 
		troubleTicket_equipment_component_lookup.dateTimeCreated 
	from troubleTicket_tickets
	left join troubleTicket_departments on troubleTicket_tickets.departmentID = troubleTicket_departments.departmentid 
	left join troubleTicket_sites on troubleTicket_departments.siteid = troubleTicket_sites.siteID 
	left join troubleTicket_components on troubleTicket_tickets.componentID = troubleTicket_components.componentID
	left join troubleTicket_component_categories on troubleTicket_components.componentCategoryID = troubleTicket_component_categories.componentCategoryID
	left join troubleTicket_equipment_component_lookup on troubleTicket_components.componentID = troubleTicket_equipment_component_lookup.componentID
	order by 
		troubleTicket_sites.siteLabel, 
		troubleTicket_departments.departmentLabel, 
		troubleTicket_component_categories.categoryLabel  
</cfquery>

<cfoutput query="getData" group="siteLabel">
	<cfoutput group="departmentLabel">
	<tr>
		<td colspan="4" valign="bottom" class="linedRow"><br /><b>#siteLabel# #departmentLabel#</b></td>
	</tr>
	<cfoutput group="categorylabel">
		<cfset categoryTotalUptime = 0>
		<cfset categoryMinutesDown = 0>
		<cfset categoryPeriodTotalUptime = 0>
		<cfset categoryPeriodMinutesDown = 0>
		<cfoutput>
			<!--- calculate items and add to category totals --->
			<cfset itemPeriodStartDate = "09/01/2010">
			<cfset itemStartDate = dateformat(dateTimeCreated,'mm/dd/yyyy')>
			<cfset itemPeriodUptime = dateDiff("n",itemPeriodStartDate,now())>
			<cfset itemTotalUptime = dateDiff("n",itemStartDate,now())>
			<cfif trim(dateTimeCompleted) is not "">
				<cfset itemMinutesDown = datediff("n",dateTimeReported,dateTimeCompleted)>
			<cfelse>
				<cfset itemMinutesDown = datediff("n",dateTimeReported,now())>
			</cfif>
			
			<cfset itemNewUpTime = itemTotalUptime - itemMinutesDown>
			<cfset itemPercentUp = (itemNewUpTime / itemTotalUptime) * 100>
			<cfset categoryTotalUptime = categoryTotalUptime + itemTotalUptime>
			<cfset categoryMinutesDown = categoryMinutesDown + itemMinutesDown>
			
			<cfset itemPeriodNewUpTime = itemPeriodUptime - itemMinutesDown>
			<cfset itemPeriodPercentUp = (itemPeriodNewUpTime / itemPeriodUptime) * 100>
			<cfset categoryPeriodTotalUptime = categoryPeriodTotalUptime + itemPeriodUptime>
			<cfset categoryPeriodMinutesDown = categoryPeriodMinutesDown + itemMinutesDown>
		</cfoutput>
		<!--- calculate category uptime --->
		<cfset categoryNewUpTime = categoryTotalUptime - categoryMinutesDown>
		<cfset categoryPercentUp = (categoryNewUpTime / categoryTotalUptime) * 100>
		<cfset categoryPeriodNewUpTime = categoryPeriodTotalUptime - categoryPeriodMinutesDown>
		<cfset categoryPeriodPercentUp = (categoryPeriodNewUpTime / categoryPeriodTotalUptime) * 100>
		<tr>
			<td>#categoryLabel#</td>
			<td align="center">#numberformat(evaluate(categoryMinutesDown / 60),"0.00")#</td>
			<td align="center">#numberformat(categoryPeriodPercentUp,"999.99")#%</td>
			<td align="center">#numberformat(categoryPercentUp,"999.99")#%</td>
		</tr>
	</cfoutput>
	</cfoutput>
</cfoutput>


</table>
