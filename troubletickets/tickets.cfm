
<link rel="stylesheet" type="text/css" href="/styles.css">

<script type="text/javascript" language="javascript">
function OpenDetailWin(id) {
	var windowWidth = 460;
	var windowHeight = 500;
	var left = (screen.width / 2) - (windowWidth / 2);
	var top = (screen.height / 2) - (windowHeight/ 2) - 150;
	DetailWin = window.open("viewTicket.cfm?tid=" + id,"DetailWin","top=" + top + ",left=" + left + ",height=" + windowHeight + ",width=" + windowWidth + ",resizable=1,status=0,titlebar=0,toolbar=0,scrollbars=1");
}
</script>

<cfparam name="form.filterStatusID" default="0,1">
<cfparam name="form.filterSiteID" default="0">
<cfparam name="form.filterDepartmentID" default="0">
<cfparam name="form.filterEquipmentID" default="0">
<cfparam name="form.filterTechID" default="0">
<cfparam name="form.filterComponentID" default="0">
<cfparam name="form.startDate" default="">
<cfparam name="form.endDate" default="">

<cfif isDefined("url.viewDetail")>
	<cfset form.filterStatusID = url.filterStatusID>
	<cfset form.filterSiteID = url.filterSiteID>
	<cfset form.filterDepartmentID = url.filterDepartmentID>
	<cfset form.startDate = url.startDate>
	<cfset form.endDate = url.endDate>
	<cfset form.filterEquipmentID = url.filterEquipmentID>
</cfif>

<cfquery name="getTickets" datasource="#ds#">
	select distinct 
		troubleTicket_tickets.ticketid, 
		troubleTicket_tickets.dateTimeReported, 
		troubleTicket_tickets.estimatedCompletionDate, 
		troubleTicket_tickets.currentStatusID, 
		troubleTicket_sites.siteLabel, 
		troubleTicket_departments.departmentLabel, 
		troubleTicket_equipment.equipmentLabel, 
		troubleTicket_components.componentLabel, 
		troubleTicket_status.statusLabel, 
		troubleTicket_tickets.dateTimeLastUpdated 
	from troubleTicket_tickets 
	left join troubleTicket_departments on troubleTicket_tickets.departmentid = troubleTicket_departments.departmentid 
	left join troubleTicket_sites on troubleTicket_departments.siteid = troubleTicket_sites.siteid 
	left join troubleTicket_equipment on troubleTicket_tickets.equipmentid = troubleTicket_equipment.equipmentid 
	left join troubleTicket_components on troubleTicket_tickets.componentid = troubleTicket_components.componentid 
	left join troubleTicket_status on troubleTicket_tickets.currentStatusID = troubleTicket_status.statusid 
	left join troubleTicket_techHistory on troubleTicket_tickets.ticketid = troubleTicket_techHistory.ticketid 
	left join troubleTicket_status_history on troubleTicket_tickets.ticketid = troubleTicket_status_history.ticketid 
	where 
	<cfif trim(form.startDate) is not "">
		troubleTicket_tickets.dateTimeReported >= #createodbcdatetime(form.startDate)# and 
	</cfif>
	<cfif trim(form.endDate) is not "">
		troubleTicket_tickets.dateTimeReported < #createodbcdatetime(form.endDate)# and 
	</cfif>
	<cfif form.filterStatusID gte 0>
		troubleTicket_tickets.currentStatusID IN (#form.filterStatusID#) and 
	</cfif>
	<cfif form.filterSiteID gt 0>
		troubleTicket_sites.siteid = #form.filterSiteID# and 
	</cfif>
	<cfif form.filterDepartmentID gt 0>
		troubleTicket_tickets.departmentid = #form.filterDepartmentID# and 
	</cfif>
	<cfif form.filterEquipmentID gt 0>
		troubleTicket_equipment.equipmentid = #form.filterEquipmentID# and 
	</cfif>
	<cfif form.filterComponentID gt 0>
		troubleTicket_tickets.componentID = #form.filterComponentID# and 
	</cfif>
	<cfif form.filterTechID gt 0>
		(troubleTicket_techHistory.techID = #form.filterTechID# or troubleTicket_status_history.updatedByID = #form.filterTechID#) and 
	</cfif>
	1=1 
	order by troubleTicket_tickets.dateTimeReported DESC 
</cfquery>

<cfquery name="getAllStatus" datasource="#ds#">
	select * from troubleTicket_status 
	order by statusid ASC 
</cfquery>

<cfquery name="getAllSites" datasource="#ds#">
	select * from troubleTicket_sites 
	order by siteLabel ASC 
</cfquery>

<cfquery name="getAllDepartments" datasource="#ds#">
	select * from troubleTicket_departments 
	order by departmentLabel ASC 
</cfquery>

<cfquery name="getAllTechs" datasource="#ds#">
	select distinct troubleTicket_techHistory.techID, Admin_Users.firstname, Admin_Users.lastname 
	from troubleTicket_techHistory 
	left join Admin_Users on troubleTicket_techHistory.techID = Admin_Users.adminuserid 
	order by Admin_Users.lastname, Admin_Users.firstname 
</cfquery>

<cfquery name="getAllComponents" datasource="#ds#">
	select * from troubleTicket_components 
	order by componentLabel asc 
</cfquery>

<div align="center" class="normal">
<table width="955" border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td class="highlightbar"><b>Trouble Ticket Pick-Up</b> </td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
			<cfform method="post" action="tickets.cfm">
			<table border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td colspan="5"><b><a style="text-decoration:underline;" target="_blank" href="/troubletickets/helpdoc/index.htm">Click Here to View the Help &amp; Training Documentation</a></b> </td>
				</tr>
				<tr>
					<td colspan="5">
					<input type="button" onclick="document.location = 'newTicket.cfm';" value="Create a New Ticket" class="sidebar" />
					</td>
				</tr>
				<tr>
					<td><b>Filter by: </b></td>
					<td>
						<cfoutput query="getAllStatus">
						<input <cfif listfindnocase(form.filterStatusID,getAllStatus.statusid) is not 0>checked</cfif> style="vertical-align:middle;" type="checkbox" name="filterStatusID" value="#statusid#" />#statusLabel#&nbsp;
						</cfoutput>
					</td>
					<td>
						<cfselect name="filterSiteID">
							<option value="0">All Site</option>
							<cfoutput query="getAllSites">
								<option <cfif form.filterSiteID is getAllSites.siteid>selected</cfif> value="#siteid#">#siteLabel#</option>
							</cfoutput>
						</cfselect>
					</td>
					<td>
						<cfselect bindonload="yes" selected="#form.filterDepartmentID#" name="filterDepartmentID" bind="cfc:cfc.functions.getDepartments({filterSiteID},'#ds#')" value="{departmentid}" display="{departmentLabel}"></cfselect>
					</td>
					<td>
						<cfselect bindonload="no" selected="#form.filterEquipmentID#" name="filterEquipmentID" bind="cfc:cfc.functions.getEquipments({filterDepartmentID},'#ds#')" value="{equipmentid}" display="{equipmentLabel}"></cfselect>
					</td>
					<td>
						<cfselect name="filterComponentID">
							<option value="0">All Components</option>
							<cfoutput query="getAllComponents">
								<option <cfif form.filterComponentID is getAllComponents.componentID>selected</cfif> value="#componentID#">#componentLabel#</option>
							</cfoutput>
						</cfselect>
					</td>
				</tr>
				<tr>
					<td colspan="5">
						<input name="btnApplyFilter" type="submit" class="sidebarsmall" value="Apply Filters">
						<input name="btnShowAll" type="button" class="sidebarsmall" value="Reset View" onclick="document.location = 'tickets.cfm';">
					</td>
				</tr>
			</table>
			</cfform>
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td width="3%" align="center" class="linedrowrightcolumnTopAndBottom">&nbsp;</td>
					<cfif cookie.adminLogin is 1>
					<td width="3%" align="center" class="linedrowrightcolumnTopAndBottom">&nbsp;</td>
					</cfif>
					<td width="3%" align="center" class="linedrowrightcolumnTopAndBottom">&nbsp;</td>
					<td width="3%" align="center" class="linedrowrightcolumnTopAndBottom">&nbsp;</td>
					<td width="5%" nowrap class="linedrowrightcolumnTopAndBottom"><b>Ticket</b></td>
					<td width="15%" nowrap class="linedrowrightcolumnTopAndBottom"><b>Date/Time Created </b></td>
					<td width="12%" nowrap class="linedrowrightcolumnTopAndBottom"><b>Est. Completion </b></td>
					<td width="4%" nowrap class="linedrowrightcolumnTopAndBottom"><b>Site</b></td>
					<td width="9%" nowrap class="linedrowrightcolumnTopAndBottom"><b>Department</b></td>
					<td width="18%" nowrap class="linedrowrightcolumnTopAndBottom"><b>Equipment - Item </b></td>
					<td width="10%" nowrap class="linedrowrightcolumnTopAndBottom"><b>Current Tech</b></td>
					<td width="9%" nowrap class="linedrowrightcolumnTopAndBottom"><b>Status</b></td>
					<td width="15%" nowrap class="linedrowrightcolumnTopAndBottom"><b>Last Update</b> </td>
				</tr>
				<cfoutput query="getTickets" group="ticketid">
					<cfquery name="getCurrentTech" datasource="#ds#">
						select top 1 troubleTicket_techHistory.techID, admin_users.firstname + ' ' + admin_users.lastname as techName 
						from troubleTicket_techHistory 
						left join admin_users on troubleTicket_techHistory.techID = admin_users.adminuserid 
						where troubleTicket_techHistory.ticketid = #ticketid# 
						order by troubleTicket_techHistory.techHistoryID desc 
					</cfquery>
					<cfif getTickets.currentStatusID is 1>
						<cfquery name="getWhoPickedUp" datasource="#ds#">
							select admin_users.firstname + ' ' + admin_users.lastname as pickedUpBy 
							from troubleTicket_status_history with (nolock) 
							left join Admin_Users on adminuserid = troubleTicket_status_history.updatedByID 
							where troubleTicket_status_history.ticketid = #ticketid# 
						</cfquery>
					</cfif>
					<cfset noOpen = false>
					<cfset today = now()>
					<cfset overdueDate = dateadd("h",-48,today)>
					<cfswitch expression="#getTickets.currentStatusID#">
						<cfcase value="0">
							<cfif datecompare(getTickets.dateTimeReported,overdueDate) lt 0>
								<cfset statusIcon = "ticket_overdue.gif"><cfset statusTxt = "Overdue Pickup">
							<cfelse>
								<cfset statusIcon = "unread.gif"><cfset statusTxt = "Unread">
							</cfif>
						</cfcase>
						<cfcase value="1"><cfset statusIcon = "read.gif"><cfset statusTxt = "Read"></cfcase>
						<cfcase value="2">
							<cfset statusIcon = "completed.gif">
							<cfset statusTxt = "Completed">
							<cfset noOpen = true>
						</cfcase>
						<cfdefaultcase><cfset statusIcon = "unread.gif"><cfset statusTxt = "Unread"></cfdefaultcase>
					</cfswitch>
					<cfset componentList = "">
					<cfoutput>
						<cfset componentList = listappend(componentList,componentLabel,", ")>
					</cfoutput>
					<cfquery name="checkdept" dbtype="query">
						select departmentid from getsec 
						where departmentid = 1
					</cfquery>
					<cfif checkdept.recordcount is 0>
						<cfset noOpen = true>
					</cfif>
					<tr>
						<td width="3%" align="center" class="linedrowrightcolumn">#getTickets.currentRow#. </td>
						<cfif cookie.adminLogin is 1>
						<td align="center" class="linedrowrightcolumn">
							<a onclick="return confirm('Are you sure you wish to delete this ticket?');" href="delete.cfm?ticketid=#ticketid#"><img src="/images/delete.gif" border="0" /></a>
						</td>
						</cfif>
						<td align="center" class="linedrowrightcolumn">
						<cfif noOpen is false>
							<a <cfif getTickets.currentStatusID is 0>onclick="return confirm('Are you sure you wish to open this ticket?');"</cfif> href="openTicket.cfm?tid=#ticketid#"><img border="0" src="/images/#statusIcon#" alt="#statusTxt#" width="15" height="15"></a>
						<cfelse>
							<img border="0" src="/images/#statusIcon#" alt="#statusTxt#" width="15" height="15">
						</cfif>
						</td>
						<td align="center" class="linedrowrightcolumn"><a href="javascript:OpenDetailWin(#ticketid#);"><img src="/images/manifyingglass.gif" border="0" alt="View Ticket" width="15" height="15" /></a></td>
						<td nowrap class="linedrowrightcolumn">#ticketid#</td>
						<td nowrap class="linedrowrightcolumn">#dateformat(dateTimeReported,'mm/dd/yyyy')# #timeformat(dateTimeReported,'hh:mm tt')#</td>
						<td nowrap class="linedrowrightcolumn">
						<cfif trim(estimatedCompletionDate) is not "">
							<cfif (datediff("d",estimatedCompletionDate,now()) gt 0) and getTickets.currentStatusID is not 2>
								<span style="color:##FF0000"><b>#dateformat(estimatedCompletionDate,'mm/dd/yyyy')#</b></span>
							<cfelse>
								#dateformat(estimatedCompletionDate,'mm/dd/yyyy')#
							</cfif>
						</cfif>&nbsp;
						</td>
						<td nowrap class="linedrowrightcolumn">#siteLabel#</td>
						<td nowrap class="linedrowrightcolumn">#departmentLabel#</td>
						<td nowrap class="linedrowrightcolumn">#equipmentLabel# - #componentList#</td>
						<td nowrap class="linedrowrightcolumn">
							<cfif getTickets.currentStatusID is 1>
								#getWhoPickedUp.pickedUpBy#&nbsp;
							<cfelse>
								#getCurrentTech.techName#&nbsp;
							</cfif>
						</td>
						<td nowrap class="linedrowrightcolumn">#statusLabel#</td>
						<td nowrap class="linedrow">#dateformat(dateTimeLastUpdated,'mm/dd/yyyy')# #timeformat(dateTimeLastUpdated,'hh:mm tt')#</td>
					</tr>
				</cfoutput>
			</table>
		</td>
	</tr>
</table>
<cfif isDefined("url.viewDetail")>
<br />
<cfoutput>
<a style="text-decoration:underline;" href="reports/equipmentSummary.cfm?viewDetail=1&startDate=#form.startDate#&endDate=#form.endDate#&filterStatusID=#form.filterStatusID#&filterDepartmentID=#form.filterDepartmentID#&filterSiteID=#form.filterSiteID#&filterEquipmentID=#form.filterEquipmentID#">Return to Summary Report</a>
</cfoutput>
<br />
</cfif>
<br />
<a class="normal" style="text-decoration:underline;" href="/index.cfm">Return to Intranet Menu</a>
</div>
