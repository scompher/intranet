<link rel="stylesheet" type="text/css" href="/styles.css">

<cfparam name="fepsite" default="NJ">

<cfloop from="1" to="2" index="fep">

<cfquery name="getStatus" datasource="#fepsite#-fep#fep#">
	select * from config_signalsources order by name asc 
</cfquery>
<cfset rows = getStatus.recordcount / 10>

<style type="text/css">
.green {background-color:#00FF00; color:#000000;}
.red {background-color:#FF0000; color:#000000;}
</style>

<div align="center">
<table width="950" border="0" cellspacing="0" cellpadding="5" style="border:1px solid #D7D7D7;">
    <tr>
        <td class="highlightbar"><b><cfoutput>#fepsite# FEP #fep#</cfoutput> Signal Source Status View</b></td>
    </tr>
    <tr>
        <td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<cfloop from="1" to="#rows#" index="i">
				<tr>
					<cfoutput query="getStatus" maxrows="10" startrow="#evaluate((i * 10) - 9)#">
						<td class="nopadding">
							<table width="100%" border="0" cellspacing="2" cellpadding="5">
								<tr>
									<td <cfif getStatus.isSafetyNetconnected is 1>style="background-color:##00FF00; color:##000000;"<cfelse>style="background-color:##FF0000; color:##FFFFFF;"</cfif> align="center" width="50%" class="linedrowrightcolumn">SNET</td>
									<td <cfif getStatus.isConnected is 1>style="background-color:##00FF00; color:##000000;"<cfelse>style="background-color:##FF0000; color:##FFFFFF;"</cfif> align="center" width="50%" class="linedrowrightcolumn">CONN</td>
								</tr>
								<tr>
									<td colspan="2" class="linedrowrightcolumn" align="center">
									#name#
									</td>
								</tr>
							</table>
						</td>
					</cfoutput>
				</tr>
				</cfloop>
			</table>
		</td>
    </tr>
</table>
<br />
<a class="normal" style="text-decoration:underline;" href="/index.cfm">Return to Intranet</a>
</div>
<br />
<br />

</cfloop>