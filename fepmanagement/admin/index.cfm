
<cfparam name="fepsite" default="">
<cfparam name="showss" default="1">

<cfinclude template="functions.cfm">

<script type="text/javascript">
function editSS(fep,ssid) {
	w = 660;
	h = 250;
	wleft = (screen.width - w) / 2;
	wtop = ((screen.height - h) / 2) - 150;
	window.open("editSS.cfm?fep=" + fep + "&ssid=" + ssid,"EditWin","height=" + h + ",width=" + w + ",resizeable=no,scrollbars=no,left=" + wleft + ",top=" + wtop);
}
function addSS(fep) {
	w = 660;
	h = 250;
	wleft = (screen.width - w) / 2;
	wtop = ((screen.height - h) / 2) - 150;
	window.open("addSS.cfm?fep=" + fep,"AddWin","height=" + h + ",width=" + w + ",resizeable=no,scrollbars=no,left=" + wleft + ",top=" + wtop);
}
function performAction(fep,ssid,action) {
	var i = action.selectedIndex;
	var doAction = action.options[i].value;
	if (doAction == "edit") {
		editSS(fep,ssid);
	} else if (doAction == "delete") {
		if (confirm('Are you sure you wish to remove this signal source?')) {
			document.location = 'delSS.cfm?fep=' + fep + "&ssid=" + ssid;
		}
	} else if (doAction == "copy") {
		if (confirm('Are you sure you wish to copy this signal source?')) {
			document.location = 'copyAsNew.cfm?fep=' + fep + '&ssid=' + ssid;		
		}
	} else if (doAction == "reset") {
		if (confirm('Are you sure you wish to reset this connection?')) {
			document.location = 'restartSS.cfm?fep=' + fep + '&ssid=' + ssid;
		}
	}
	action.selectedIndex = -1;
}
</script>

<link rel="stylesheet" type="text/css" href="/styles.css">

<style type="text/css">
Example
 
table
 {
 border-collapse:collapse;
 }
 table,th, td
 {
 border: 1px solid #EEEEEE;
 }
 td 
 {
 vertical-align:top;
 padding:5px;
 }
</style>

<div align="center">
<table width="950" border="0" cellspacing="0" cellpadding="5">
    <tr>
        <td class="highlightbar"><b>FEP Administration </b></td>
    </tr>
    <tr>
        <td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td><b>Choose FEP site:</b> <a href="index.cfm?fepsite=NJ">NJ</a> | <a href="index.cfm?fepsite=NJV">NJV</a> | <a href="index.cfm?fepsite=AZ">AZ</a> | <a href="index.cfm?fepsite=FL">FL</a> | <a href="index.cfm?fepsite=TX">TX</a> | <a href="index.cfm?fepsite=MD">MD</a></td></td>
				</tr>
				<cfif fepsite is not "">
					<cfloop from="1" to="2" index="i">
						<cfquery name="getConfig" datasource="#fepsite#-fep#i#">
							select * from config_main 
						</cfquery>
						<cfquery name="getSS" datasource="#fepsite#-fep#i#">	
							select * from config_signalsources order by signalsourceid asc 
						</cfquery>
						<cfoutput>
						<tr>
							<td class="highlightbar">
							<b>#fepsite# FEP #i# Details</b>
							<cfif getConfig.maintenanceMode is 1>
							<i><b> - THIS FEP IS CURRENTLY IN MAINTENANCE MODE</b></i>
							</cfif>
							</td>
						</tr>
						<tr>
							<td>
							<a href="manageFep.cfm?fepsite=#fepsite#&fep=#i#">[edit configuration values]</a>&nbsp;
							<a onclick="return confirm('Are you sure you wish to mark this fep for restart?');" href="manageFep.cfm?fepsite=#fepsite#&fep=#i#&restartfep=true">[Flag for Restart]</a>&nbsp;
							<cfif getconfig.maintenancemode is 1>
								<a href="setMode.cfm?fepsite=#fepsite#&fep=#i#&maintenance=0">[turn off maintenance mode]</a>
							<cfelse>
								<a href="setMode.cfm?fepsite=#fepsite#&fep=#i#&maintenance=1">[turn on maintenance mode]</a>
							</cfif>
							</td>
						</tr>
						</cfoutput>
						<tr class="nopadding">
							<td>
								<cfoutput query="getConfig">
								<table width="100%" border="0" cellspacing="0" cellpadding="5">
									<tr>
										<td>Version:<br />#version# </td>
										<td>Signal Server Port:<br />#signalServerPort#</td>
										<td>Local Site:<br />#localSite#</td>
										<td>Primary FEP:<br />#primaryFep#</td>
										<td>Secondary FEP:<br />#secondaryFep#</td>
										<td>Active FEP:<br />#activeFep#</td>
										<td>Am I Primary?<br /><cfif amIPrimary is not 0>YES<cfelse>NO</cfif> </td>
										<td>Stale Alarm Threshold:<br />#staleAlarmInterval# seconds </td>
									</tr>
									<tr>
										<td>HA Address:<br />#ha_address#</td>
										<td>HA Port:<br />#ha_port#</td>
										<td>HA Preferred FEP:<br />#HA_Preferred#</td>
										<td>Modem IP:<br />#modemIP#</td>
										<td>Modem Port:<br />#modemPort#</td>
										<td>Am I Preferred?<br /><cfif amIPreferred is not 0>YES<cfelse>NO</cfif></td>
										<td>My Name:<br />#myName#</td>
										<td>Inactivity threshold:<br />#inactivityInterval# seconds</td>
									</tr>
								</cfoutput>
							</table>
							</td>
						</tr>
						<cfoutput>
						<tr>
							<td>
							<b>There <cfif getSS.recordcount is 1>is<cfelse>are</cfif> #getss.recordcount# signal sources configured</b>&nbsp;
							<cfif showss is 1>
								<a href="index.cfm?fepsite=#fepsite#&showss=0">[hide]</a>
							<cfelse>
								<a href="index.cfm?fepsite=#fepsite#&showss=1">[show]</a>
							</cfif>
							&nbsp;<a href="javascript:addSS('#fepsite#-FEP#i#');">[Add New Signal Source]</a>
							</td>
						</tr>
						</cfoutput>
						<cfif showss is 1>
							<tr>
								<td class="nopadding">
								<table width="100%" border="0" cellspacing="0" cellpadding="5">
									<tr>
										<td align="center" nowrap class="linedrowrightcolumn"><b>Action</b></td>
										<td nowrap class="linedrowrightcolumn"><b>ConfigID</b></td>
										<td nowrap class="linedrowrightcolumn"><b>Priority</b></td>
										<td nowrap class="linedrowrightcolumn"><b>Name</b></td>
										<td nowrap class="linedrowrightcolumn"><b>Site</b></td>
										<td nowrap class="linedrowrightcolumn"><b>Type</b></td>
										<td nowrap class="linedrowrightcolumn"><b>Output Mode </b></td>
										<td nowrap class="linedrowrightcolumn"><b>Path</b></td>
										<td nowrap class="linedrowrightcolumn"><b>HeartBeat</b></td>
										<td nowrap class="linedrowrightcolumn"><b>Keep HB</b></td>
										<td nowrap class="linedrowrightcolumn"><b>Term Char </b></td>
										<td nowrap class="linedrowrightcolumn"><b>Ack Char </b></td>
										<td nowrap class="linedrowrightcolumn"><b>Read Timeout </b></td>
										<td nowrap class="linedrowrightcolumn"><b>SN Connected? </b></td>
										<td nowrap class="linedrowrightcolumn"><b>Is Connected? </b></td>
									</tr>
									<cfoutput query="getSS">
									<tr>
										<td align="center" nowrap class="linedrowrightcolumn">
										<select name="action" onchange="performAction('#fepsite#-FEP#i#','#getss.signalsourceid#',this);" style="width:125px;">
											<option value=""></option>
											<option value="edit">Edit</option>
											<option value="delete">Delete</option>
											<option value="copy">Copy as New</option>
											<option value="reset">Reset Connection</option>
										</select>
										</td>
										<td nowrap class="linedrowrightcolumn">#configid#</td>
										<td nowrap class="linedrowrightcolumn">#priority#</td>
										<td nowrap class="linedrowrightcolumn"><a target="_blank" style="text-decoration:underline;" href="http://www.copalink.com/utilities/fep/viewAlarms.cfm?site=#fepsite#&filter_source=#name#">#name#</a></td>
										<td nowrap class="linedrowrightcolumn">#site#</td>
										<td nowrap class="linedrowrightcolumn">#type#</td>
										<td nowrap class="linedrowrightcolumn">#outputMode#</td>
										<td nowrap class="linedrowrightcolumn">#path#</td>
										<td nowrap class="linedrowrightcolumn">#heartbeat#</td>
										<td nowrap class="linedrowrightcolumn">#keepHeartbeat#</td>
										<td nowrap class="linedrowrightcolumn">#terminatingCharacter#</td>
										<td nowrap class="linedrowrightcolumn">#ackCharacter#</td>
										<td nowrap class="linedrowrightcolumn"><cfif readTimeout is 0>None<cfelse>#readTimeout# seconds</cfif></td>
										<cfif isSafetyNetConnected is 1><cfset bgc = "00FF00"><cfset c = "000000"><cfelse><cfset bgc = "FF0000"><cfset c = "FFFFFF"></cfif>
										<td style="background-color:###bgc#; color:###c#;" nowrap class="linedrowrightcolumn"><cfif isSafetyNetConnected is 1>YES<cfelse>NO</cfif></td>
										<cfif isConnected is 1><cfset bgc = "00FF00"><cfset c = "000000"><cfelse><cfset bgc = "FF0000"><cfset c = "FFFFFF"></cfif>
										<td style="background-color:###bgc#; color:###c#;" nowrap class="linedrowrightcolumn"><cfif isConnected is 1>YES<cfelse>NO</cfif></td>
									</tr>
									</cfoutput>
								</table>
								</td>
							</tr>
						</cfif>
						<tr>
							<td>&nbsp;</td>
						</tr>
					</cfloop>
				</cfif>
			</table>
		</td>
    </tr>
</table>
<br />
<a class="normal" style="text-decoration:underline;" href="/index.cfm">Return to Intranet</a>
</div>

