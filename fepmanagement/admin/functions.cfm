
<cfif isDefined("form.btnAddSignalSource")>
	<cfparam name="form.copyToOtherFep" default="0">
	<cfquery name="saveSS" datasource="#fepsite#-FEP#fep#">
		insert into config_signalSources (signalSourceID, configID, priority, name, site, type, outputMode, path, heartbeat, keepheartbeat, terminatingCharacter, ackCharacter) 
		values (#signalSourceiD#, #configid#, #priority#, '#name#', '#site#', '#type#', '#outputMode#', '#path#', '#heartbeat#', '#keepHeartbeat#', '#terminatingCharacter#', '#ackCharacter#') 
	</cfquery>
	<cfif form.copyToOtherFep is 1>
		<cfif fep is 1><cfset otherFep = 2><cfelse><cfset otherFep = 1></cfif>
		<cfquery name="saveSS" datasource="#fepsite#-FEP#otherFep#">
			insert into config_signalSources (signalSourceID, configID, priority, name, site, type, outputMode, path, heartbeat, keepheartbeat, terminatingCharacter, ackCharacter) 
			values (#signalSourceiD#, #configid#, #priority#, '#name#', '#site#', '#type#', '#outputMode#', '#path#', '#heartbeat#', '#keepHeartbeat#', '#terminatingCharacter#', '#ackCharacter#') 
		</cfquery>
	</cfif>
	<cflocation url="manageFep.cfm?fepsite=#fepsite#&fep=#fep#">
</cfif>

<cfif isDefined("url.delss")>
	<cfquery name="delSS" datasource="#fepsite#-FEP#fep#">
		delete from config_signalSources where signalsourceid = #url.delss# 
	</cfquery>
	<cfif isDefined("url.delfromboth")>
		<cfif fep is 1><cfset otherFep = 2><cfelse><cfset otherFep = 1></cfif>
		<cfquery name="delSS" datasource="#fepsite#-FEP#otherfep#">
			delete from config_signalSources where signalsourceid = #url.delss# 
		</cfquery>
	</cfif>
	<cflocation url="manageFep.cfm?fepsite=#fepsite#&fep=#fep#">
</cfif>

<cfif isDefined("form.btnUpdateSignalSource")>
	<cfparam name="form.copyToOtherFep" default="0">
	<cfquery name="updateSS" datasource="#fepsite#-FEP#fep#">
		update config_signalSources 
		set signalsourceid = #signalsourceid#, configid = #configid#, priority = #priority#, name = '#name#', site = '#site#', type = '#type#', outputMode = '#outputMode#', path = '#path#', heartbeat = '#heartbeat#', keepHeartbeat = '#keepheartbeat#', terminatingcharacter = '#terminatingcharacter#', ackcharacter = '#ackcharacter#' 
		where signalsourceid = #ssid# 
	</cfquery>
	<cfif form.copyToOtherFep is 1>
		<cfif fep is 1><cfset otherFep = 2><cfelse><cfset otherFep = 1></cfif>
		<cfquery name="updateSS" datasource="#fepsite#-FEP#otherFep#">
			update config_signalSources 
			set signalsourceid = #signalsourceid#, configid = #configid#, priority = #priority#, name = '#name#', site = '#site#', type = '#type#', outputMode = '#outputMode#', path = '#path#', heartbeat = '#heartbeat#', keepHeartbeat = '#keepheartbeat#', terminatingcharacter = '#terminatingcharacter#', ackcharacter = '#ackcharacter#' 
			where signalsourceid = #ssid# 
		</cfquery>
	</cfif>
	<cflocation url="manageFep.cfm?fepsite=#fepsite#&fep=#fep#">
</cfif>

<cfif isDefined("form.btnSaveConfig")>
	<cfquery name="saveConfig" datasource="#fepsite#-FEP#fep#">
		update config_main 
		set 
			version = '#form.version#', 
			signalserverport = '#signalserverport#', 
			localsite = '#localsite#', 
			primaryFep = '#primaryFep#', 
			secondaryFep = '#secondaryFep#', 
			activeFep = '#activeFep#', 
			amIPrimary = '#amIPrimary#', 
			staleAlarmInterval = #staleAlarmInterval#, 
			ha_address = '#ha_address#', 
			ha_port = '#ha_port#', 
			ha_preferred = '#ha_preferred#', 
			modemIP = '#modemIP#', 
			modemPort = '#modemPort#', 
			amIPreferred = '#amIpreferred#', 
			myName = '#myname#', 
			inactivityInterval = #inactivityInterval# 
		where configid = #update_configid# 
	</cfquery>
	<cflocation url="manageFep.cfm?fepsite=#fepsite#&fep=#fep#&configupdated=true">
</cfif>

<cfif isDefined("url.restartfep")>
	<cfquery name="markForRestart" datasource="#fepsite#-FEP#fep#">
		update config_main 
		set executeRestart = 1 
	</cfquery>
	<cflocation url="index.cfm?fepsite=#fepsite#">
</cfif>

