<cfsetting showdebugoutput="no">

<link rel="stylesheet" type="text/css" href="/styles.css">

<cfif isDefined("form.btnEditSS")>
	<cfset fepsite = listgetat(fep,1,"-")>
	<cfset fepindex = listgetat(fep,2,"-")>
	<cfset fepindex = replace(fepindex, "FEP", "", "all")>
	<!--- save SS data --->
	<cfparam name="form.copyToOtherFep" default="0">
	<cfquery name="updateSS" datasource="#fepsite#-FEP#fepindex#">
		update config_signalSources 
		set configid = #configid#, priority = #priority#, name = '#ucase(name)#', site = '#site#', type = '#type#', outputMode = '#outputMode#', path = '#path#', heartbeat = '#heartbeat#', keepHeartbeat = '#keepheartbeat#', terminatingcharacter = '#terminatingcharacter#', ackcharacter = '#ackcharacter#', readtimeout='#readtimeout#'
		where signalsourceid = #ssid# 
	</cfquery>
	<cfif form.copyToOtherFep is 1>
		<cfif fepindex is 1><cfset otherFep = 2><cfelse><cfset otherFep = 1></cfif>
		<cfquery name="updateSS" datasource="#fepsite#-FEP#otherFep#">
			update config_signalSources 
			set configid = #configid#, priority = #priority#, name = '#ucase(name)#', site = '#site#', type = '#type#', outputMode = '#outputMode#', path = '#path#', heartbeat = '#heartbeat#', keepHeartbeat = '#keepheartbeat#', terminatingcharacter = '#terminatingcharacter#', ackcharacter = '#ackcharacter#', readtimeout='#readtimeout#'
			where signalsourceid = #ssid# 
		</cfquery>
	</cfif>
	<script type="text/javascript">
		opener.location.reload();
		self.close();
	</script>
</cfif>

<cfquery name="getSSInfo" datasource="#fep#">
	select * from config_signalsources with (nolock) 
	where signalsourceid = #ssid# 
</cfquery>

<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<div align="center">
<form method="post">
<cfoutput>
<input type="hidden" name="fep" value="#fep#" />
<input type="hidden" name="ssid" value="#ssid#" />
</cfoutput>
<table border="0" cellspacing="0" cellpadding="5" height="100%" width="100%">
    <tr>
        <td class="highlightbar"><b>Edit a signal source</b></td>
    </tr>
	<cfoutput query="getSSInfo">
    <tr>
        <td class="greyrowbottomnopadding">
			<table width="600" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td>Config ID <br>
						<input type="text" name="configid" value="#configid#">
					</td>
					<td>Priority<br>
						<input type="text" name="priority" value="#priority#">
					</td>
					<td>Name<br>
						<input type="text" name="name" value="#name#">
					</td>
					<td>Read Timeout<br>
						<input type="text" name="readtimeout" value="#readTimeout#">
					</td>
				</tr>
				<tr>
					<td>Site<br>
						<input type="text" name="site" value="#site#">
					</td>
					<td>Type<br>
						<input type="text" name="type" value="#type#">
					</td>
					<td>Output Mode <br>
						<input type="text" name="outputMode" value="#outputMode#">
					</td>
					<td>Path<br>
						<input type="text" name="path" value="#path#">
					</td>
				</tr>
				<tr>
					<td>Hearbeat<br>
						<input type="text" name="heartbeat" value="#heartbeat#">
					</td>
					<td>Keep Heartbeat <br>
						<select name="keepheartbeat">
							<option <cfif keepHeartbeat is "true">selected</cfif> value="true">true</option>
							<option <cfif keepHeartbeat is "false">selected</cfif> value="false">false</option>
						</select>
					</td>
					<td>Terminating Character <br>
						<input type="text" name="terminatingCharacter" value="#trim(terminatingCharacter)#">
					</td>
					<td>ACK Character <br>
						<input type="text" name="ackCharacter" value="#trim(ackCharacter)#">
					</td>
				</tr>
				<tr>
					<td colspan="4">
					<input type="checkbox" name="copyToOtherFep" value="1" style="vertical-align:middle;" /> Copy to other FEP
					</td>
				</tr>
				<tr>
					<td colspan="4">
					<input type="submit" name="btnEditSS" value="Update Signal Source" />
					<input type="button" value="Cancel" onClick="self.close();" />
					</td>
				</tr>
			</table>
		</td>
    </tr>
	</cfoutput>
</table>
</form>
</div>
</body>
