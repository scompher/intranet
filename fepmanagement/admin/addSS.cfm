<cfsetting showdebugoutput="no">

<cfif isDefined("form.btnAddSS")>
	<cfset fepsite = listgetat(fep,1,"-")>
	<cfset fepindex = listgetat(fep,2,"-")>
	<cfset fepindex = replace(fepindex, "FEP", "", "all")>
	<!--- save SS data --->
	<cfparam name="form.copyToOtherFep" default="0">
	<cfquery name="saveSS" datasource="#fepsite#-FEP#fepindex#">
		insert into config_signalSources (signalSourceID, configID, priority, name, site, type, outputMode, path, heartbeat, keepheartbeat, terminatingCharacter, ackCharacter, readtimeout) 
		values (#signalSourceiD#, #configid#, #priority#, '#ucase(name)#', '#site#', '#type#', '#outputMode#', '#path#', '#heartbeat#', '#keepHeartbeat#', '#terminatingCharacter#', '#ackCharacter#','#readtimeout#') 
	</cfquery>
	<cfif form.copyToOtherFep is 1>
		<cfif fepindex is 1><cfset otherFep = 2><cfelse><cfset otherFep = 1></cfif>
		<cfquery name="saveSS" datasource="#fepsite#-FEP#otherFep#">
			insert into config_signalSources (signalSourceID, configID, priority, name, site, type, outputMode, path, heartbeat, keepheartbeat, terminatingCharacter, ackCharacter,readtimeout) 
			values (#signalSourceiD#, #configid#, #priority#, '#ucase(name)#', '#site#', '#type#', '#outputMode#', '#path#', '#heartbeat#', '#keepHeartbeat#', '#terminatingCharacter#', '#ackCharacter#','#readtimeout#') 
		</cfquery>
	</cfif>
	<script type="text/javascript">
		opener.location.reload();
		self.close();
	</script>
	<!--- close window and refresh calling page --->
</cfif>

<link rel="stylesheet" type="text/css" href="/styles.css">

<cfquery name="getNextLargestSSID" datasource="#fep#">
	select max(signalsourceid) as maxid from config_signalsources 
</cfquery>
<cfset ssid = getNextLargestSSID.maxid + 1>

<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<div align="center">
<form method="post">
<cfoutput>
<input type="hidden" name="fep" value="#fep#" />
</cfoutput>
<table border="0" cellspacing="0" cellpadding="5" height="100%" width="100%">
    <tr>
        <td class="highlightbar"><b>Add a new signal source</b></td>
    </tr>
    <tr>
        <td class="greyrowbottomnopadding">
			<table width="600" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<cfoutput>
					<td>Signal Source ID <br>
						<input type="text" name="signalsourceid" value="#ssid#">
					</td>
					<td>Config ID <br>
						<input type="text" name="configid" value="#ssid#">
					</td>
					<td>Priority<br>
						<input type="text" name="priority" value="#ssid#">
					</td>
					</cfoutput>
					<td>Name<br>
						<input type="text" name="name">
					</td>
				</tr>
				<tr>
					<td>Site<br>
						<input type="text" name="site">
					</td>
					<td>Type<br>
						<input type="text" name="type">
					</td>
					<td>Output Mode <br>
						<input type="text" name="outputMode">
					</td>
					<td>Path<br>
						<input type="text" name="path">
					</td>
				</tr>
				<tr>
					<td>Hearbeat<br>
						<input type="text" name="heartbeat">
					</td>
					<td>Keep Heartbeat <br>
						<select name="keepheartbeat">
							<option value="true">true</option>
							<option value="false">false</option>
						</select>
					</td>
					<td>Terminating Character <br>
						<input type="text" name="terminatingCharacter">
					</td>
					<td>ACK Character <br>
						<input type="text" name="ackCharacter">
					</td>
				</tr>
				<tr>
					<td colspan="2">
					<input type="checkbox" name="copyToOtherFep" value="1" style="vertical-align:middle;" /> Copy to other FEP
					</td>
					<td>Read Timeout<br>
					<input type="text" name="readtimeout" value="">
					</td>
				</tr>
				<tr>
					<td colspan="4">
					<input type="submit" name="btnAddSS" value="Add Signal Source" />
					<input type="button" value="Cancel" onClick="self.close();" />
					</td>
				</tr>
			</table>
		</td>
    </tr>
</table>
</form>
</div>
</body>
