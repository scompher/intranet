
<cfparam name="fepsite" default="">
<cfparam name="fep" default="">

<cfinclude template="functions.cfm">

<style type="text/css">
Example
 
table
 {
 border-collapse:collapse;
 }
 table,th, td
 {
 border: 1px solid #EEEEEE;
 }
 td 
 {
 vertical-align:top;
 padding:5px;
 }
</style>

<cfquery name="getConfig" datasource="#fepsite#-fep#fep#">
	select * from config_main 
</cfquery>

<cfquery name="getSS" datasource="#fepsite#-fep#fep#">
	select * from config_signalsources order by signalsourceid asc 
</cfquery>

<link rel="stylesheet" type="text/css" href="/styles.css">

<form method="post" action="manageFep.cfm">
<cfoutput>
<input type="hidden" name="fepsite" value="#fepsite#" />
<input type="hidden" name="fep" value="#fep#" />
</cfoutput>
<div align="center">
<table width="950" border="0" cellspacing="0" cellpadding="5" style="border:none;">
    <tr>
        <td style="border:none;">
			<span class="normal" style="font-size:14px;"><b>Configuration</b></span><br /><br />
			<cfif isDefined("configupdated")>
			<span style="color:#009900; font-weight:bold; font-family:Arial, Helvetica, sans-serif; font-size:12px;">Configuration Values Updated</span><br /><br />
			</cfif>
			<cfoutput query="getConfig">
			<input type="hidden" name="update_configid" value="#getConfig.configid#" />
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td>Version<br>
						<input type="text" name="version" value="#version#">
					</td>
					<td>Signal Server Port <br>
						<input type="text" name="signalServerPort" value="#signalServerPort#">
					</td>
					<td>Local Site <br>
						<input type="text" name="localSite" value="#localSite#">
					</td>
					<td>Primary FEP <br>
						<input type="text" name="primaryFep" value="#primaryFep#">
					</td>
					<td>Seconary FEP<br>
						<input type="text" name="secondaryFep" value="#secondaryFep#">
					</td>
					<td>Active FEP<br>
						<input type="text" name="activeFep" value="#activeFep#">
					</td>
					<td>Am Primary FEP? <br>
						<input type="text" name="amIPrimary" value="#amIPrimary#">
					</td>
					<td>Stale Alarm Threshold<br>
						<input type="text" name="staleAlarmInterval" value="#staleAlarmInterval#">
					</td>
				</tr>
				<tr>
					<td>HA Address<br>
						<input type="text" name="HA_Address" value="#HA_Address#">
					</td>
					<td>HA Port<br>
						<input type="text" name="HA_Port" value="#HA_Port#">
					</td>
					<td>HA Preferred FEP <br>
						<input type="text" name="HA_Preferred" value="#HA_Preferred#">
					</td>
					<td>Modem IP<br>
						<input type="text" name="modemIP" value="#modemIP#">
					</td>
					<td>Modem port<br>
						<input type="text" name="modemPort" value="#modemPort#">
					</td>
					<td>Am I Preferred? <br>
						<input type="text" name="amIPreferred" value="#amIPreferred#">
					</td>
					<td>My Name <br>
						<input type="text" name="myName" value="#myName#">
					</td>
					<td>Inactivity Threshold<br>
						<input type="text" name="inactivityInterval" value="#inactivityInterval#">
					</td>
				</tr>
			</table>
			</cfoutput>
			<input type="submit" name="btnSaveConfig" value="Save Configuration" />
			<br />
			<br />
			<br />
			<span class="normal" style="font-size:14px;"><b>Signal Sources</b></span>&nbsp;
			<cfoutput><a href="manageFep.cfm?fepsite=#fepsite#&fep=#fep#&addNewSS=true">[Add New Signal Source]</a></cfoutput>
			<br />
			<cfoutput query="getSS">
			<br />
			<table width="950" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td>
					<a href="manageFep.cfm?fepsite=#fepsite#&fep=#fep#&editss=#signalsourceid#">[edit]</a>
					<a onclick="return confirm('Are you sure you wish to delete this signal source?');" href="manageFep.cfm?fepsite=#fepsite#&fep=#fep#&delss=#signalsourceid#">[delete]</a>
					<a onclick="return confirm('Are you sure you wish to delete this signal source?');" href="manageFep.cfm?fepsite=#fepsite#&fep=#fep#&delss=#signalsourceid#&delfromboth=yes">[delete from both]</a>
					</td>
					<td colspan="5">&nbsp;</td>
				</tr>
				<tr>
					<td>Signal Source ID <br>
						#signalSourceID#
					</td>
					<td>Config ID <br>
						#configID#
					</td>
					<td>Priority<br>
						#priority#
					</td>
					<td>Name<br>
					   #name#
					</td>
					<td>Site<br>
						#site#
					</td>
					<td>Type<br>
						#type#
					</td>
				</tr>
				<tr>
					<td>Output Mode <br>
						#outputMode#
					</td>
					<td>Path<br>
						#path#
					</td>
					<td>Heartbeat<pre>#heartBeat#</pre>
					</td>
					<td>Keep Heartbeat <br>
						#keepHeartbeat#
					</td>
					<td>Terminating Character <br>
						#terminatingCharacter#
					</td>
					<td>ACK Character <br>
						#ackCharacter#
					</td>
				</tr>
			</table>
			</cfoutput>
			<br />
			<cfif isDefined("url.addNewSS")>
				<b class="normal">Add New Signal Source:</b>
				<table width="950" border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td>Signal Source ID <br>
							<input type="text" name="signalsourceid">
						</td>
						<td>Config ID <br>
							<input type="text" name="configid">
						</td>
						<td>Priority<br>
							<input type="text" name="priority">
						</td>
						<td>Name<br>
							<input type="text" name="name">
						</td>
						<td>Site<br>
							<input type="text" name="site">
						</td>
						<td>Type<br>
							<input type="text" name="type">
						</td>
					</tr>
					<tr>
						<td>Output Mode <br>
							<input type="text" name="outputMode">
						</td>
						<td>Path<br>
							<input type="text" name="path">
						</td>
						<td>Hearbeat<br>
							<input type="text" name="heartbeat">
						</td>
						<td>Keep Heartbeat <br>
							<input type="text" name="keepheartbeat">
						</td>
						<td>Terminating Character <br>
							<input type="text" name="terminatingCharacter">
						</td>
						<td>ACK Character <br>
							<input type="text" name="ackCharacter">
						</td>
					</tr>
					<tr>
						<td colspan="6">
						<input type="checkbox" name="copyToOtherFep" value="1" /> Copy to other FEP
						</td>
					</tr>
				</table>
				<input type="submit" name="btnAddSignalSource" value="Add Signal Source" />
			</cfif>
			<cfif isDefined("url.editss")>
				<cfoutput>
				<input type="hidden" name="ssid" value="#url.editss#" />
				</cfoutput>
				<cfquery name="getSSInfo" datasource="#fepsite#-fep#fep#">
					select * from config_signalsources where signalsourceid = #editss# 
				</cfquery>
				<b class="normal">Edit Signal Source:</b>
				<cfoutput query="getSSInfo">
				<table width="950" border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td>Signal Source ID <br>
							<input type="text" name="signalsourceid" value="#signalsourceid#">
						</td>
						<td>Config ID <br>
							<input type="text" name="configid" value="#configid#">
						</td>
						<td>Priority<br>
							<input type="text" name="priority" value="#priority#">
						</td>
						<td>Name<br>
							<input type="text" name="name" value="#name#">
						</td>
						<td>Site<br>
							<input type="text" name="site" value="#site#">
						</td>
						<td>Type<br>
							<input type="text" name="type" value="#type#">
						</td>
					</tr>
					<tr>
						<td>Output Mode <br>
							<input type="text" name="outputMode" value="#outputMode#">
						</td>
						<td>Path<br>
							<input type="text" name="path" value="#path#">
						</td>
						<td>Hearbeat<br>
							<input type="text" name="heartbeat" value="#heartbeat#">
						</td>
						<td>Keep Heartbeat <br>
							<input type="text" name="keepheartbeat" value="#keepheartbeat#">
						</td>
						<td>Terminating Character <br>
							<input type="text" name="terminatingCharacter" value="#terminatingCharacter#">
						</td>
						<td>ACK Character <br>
							<input type="text" name="ackCharacter" value="#ackCharacter#">
						</td>
					</tr>
					<tr>
						<td colspan="6">
						<input type="checkbox" name="copyToOtherFep" value="1" /> Copy changes to other FEP
						</td>
					</tr>
				</table>
				</cfoutput>
				<input type="submit" name="btnUpdateSignalSource" value="Update Signal Source" />
			</cfif>
			<br>
		</td>
    </tr>
</table>
<br />
<cfoutput>
<a style="text-decoration:underline;" class="normal" href="index.cfm?fepsite=#fepsite#">Return to previous page</a>
</cfoutput>
<br />
<br />
</div>
</form>
