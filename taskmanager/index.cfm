

<link rel="stylesheet" type="text/css" href="../styles.css">
<style type="text/css">
<!--
.style1 {color: #FFFFFF}
-->
</style>
<div align="center">
<table width="955" border="0" cellspacing="0" cellpadding="5">
    <tr>
        <td class="highlightbar"><b>Personal Task Manager  </b></td>
    </tr>
    <tr>
        <td class="greyrowbottomnopadding">
		<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td class="header">
					Tasks&nbsp;<input name="Submit" type="submit" class="smalltable" value="Add New Task" style="vertical-align:middle;" />
				</td>
			</tr>
			<tr>
			    <td class="nopadding">
					<table width="100%" border="0" cellspacing="0" cellpadding="5">
						<tr>
							<td class="linedrowrightcolumn"><b>Priority</b></td>
						    <td class="linedrowrightcolumn"><b>Task</b></td>
						    <td class="linedrowrightcolumn"><b>Assigned By </b></td>
						    <td class="linedrowrightcolumn"><b>Status</b></td>
						    <td class="linedrowrightcolumn"><b>Due Date </b></td>
						    <td class="linedrow"><b>Notes</b></td>
						    </tr>
						<tr>
						    <td bgcolor="#FF0000" class="linedrowrightcolumn">
						        <select name="select" class="smalltable">
									<option value="1" selected="selected">High</option>
									<option value="2">Medium</option>
									<option value="3">Low</option>
						        </select>
						    </td>
						    <td bgcolor="#FF0000" class="linedrowrightcolumn"><span class="style1">CINTAS Report </span></td>
						    <td bgcolor="#FF0000" class="linedrowrightcolumn"><span class="style1">Don </span></td>
						    <td bgcolor="#FF0000" class="linedrowrightcolumn">
								<select name="select" class="smalltable">
									<option value="1">not started</option>
									<option value="2" selected="selected">in process</option>
									<option value="3">complete</option>
								</select>
							</td>
						    <td bgcolor="#FF0000" class="linedrowrightcolumn"><span class="style1"></span></td>
						    <td bgcolor="#FF0000" class="linedrow"><span class="style1">Waiting on Don and Jim's approval </span></td>
						    </tr>
						<tr>
						    <td bgcolor="#FF0000" class="linedrowrightcolumn">
						        <select name="select" class="smalltable">
									<option value="1" selected="selected">High</option>
									<option value="2">Medium</option>
									<option value="3">Low</option>
						        </select>
						    </td>
						    <td bgcolor="#FF0000" class="linedrowrightcolumn"><span class="style1">Certificate of monitoring date change </span></td>
						    <td bgcolor="#FF0000" class="linedrowrightcolumn"><span class="style1">Heather</span></td>
						    <td bgcolor="#FF0000" class="linedrowrightcolumn">
								<select name="select" class="smalltable">
									<option value="1" selected="selected">not started</option>
									<option value="2">in process</option>
									<option value="3">complete</option>
								</select>							
							</td>
						    <td bgcolor="#FF0000" class="linedrowrightcolumn"><span class="style1"></span></td>
						    <td bgcolor="#FF0000" class="linedrow"><span class="style1"></span></td>
						    </tr>
						<tr>
						    <td bgcolor="#FF8000" class="linedrowrightcolumn">
						        <select name="select" class="smalltable">
									<option value="1">High</option>
									<option value="2" selected="selected">Medium</option>
									<option value="3">Low</option>
						        </select>
						    </td>
						    <td bgcolor="#FF8000" class="linedrowrightcolumn"><span class="style1">Turning on CAL access for a login </span></td>
						    <td bgcolor="#FF8000" class="linedrowrightcolumn"><span class="style1">Heather</span></td>
						    <td bgcolor="#FF8000" class="linedrowrightcolumn">
								<select name="select" class="smalltable">
									<option value="1" selected="selected">not started</option>
									<option value="2">in process</option>
									<option value="3">complete</option>
								</select>
							</td>
						    <td bgcolor="#FF8000" class="linedrowrightcolumn"><span class="style1"></span></td>
						    <td bgcolor="#FF8000" class="linedrow"><span class="style1">dealers have no way of removing CAL access from a login </span></td>
						    </tr>
						<tr>
						    <td bgcolor="#FFFF00" class="linedrowrightcolumn">
						        <select name="select" class="smalltable">
									<option value="1">High</option>
									<option value="2">Medium</option>
									<option value="3" selected="selected">Low</option>
						        </select>
						    </td>
						    <td bgcolor="#FFFF00" class="linedrowrightcolumn">Access alarms private label site </td>
						    <td bgcolor="#FFFF00" class="linedrowrightcolumn">&nbsp;</td>
						    <td bgcolor="#FFFF00" class="linedrowrightcolumn">
								<select name="select" class="smalltable">
									<option value="1" selected="selected">not started</option>
									<option value="2">in process</option>
									<option value="3">complete</option>
								</select>
							</td>
						    <td bgcolor="#FFFF00" class="linedrowrightcolumn">&nbsp;</td>
						    <td bgcolor="#FFFF00" class="linedrow">&nbsp;</td>
						    </tr>
						<tr>
						    <td bgcolor="#FFFF00" class="linedrowrightcolumn">
						        <select name="select" class="smalltable">
									<option value="1">High</option>
									<option value="2">Medium</option>
									<option value="3" selected="selected">Low</option>
						        </select>
						    </td>
						    <td bgcolor="#FFFF00" class="linedrowrightcolumn">Dealer 4933 private label site </td>
						    <td bgcolor="#FFFF00" class="linedrowrightcolumn">&nbsp;</td>
						    <td bgcolor="#FFFF00" class="linedrowrightcolumn">
								<select name="select" class="smalltable">
									<option value="1" selected="selected">not started</option>
									<option value="2">in process</option>
									<option value="3">complete</option>
								</select>
							</td>
						    <td bgcolor="#FFFF00" class="linedrowrightcolumn">&nbsp;</td>
						    <td bgcolor="#FFFF00" class="linedrow">&nbsp;</td>
						    </tr>
					</table>
				</td>
			</tr>
			<tr>
			    <td class="header">
					Projects&nbsp;<input name="Submit2" type="submit" class="smalltable" value="Add New Project" style="vertical-align:middle;" />
			    </td>
			    </tr>
			<tr>
			    <td class="nopadding">
			        <table width="100%" border="0" cellspacing="0" cellpadding="5">
                        <tr>
                            <td class="linedrowrightcolumn"><b>Priority</b></td>
                            <td class="linedrowrightcolumn"><b>Task</b></td>
                            <td class="linedrowrightcolumn"><b>Assigned By </b></td>
                            <td class="linedrowrightcolumn"><b>Status</b></td>
                            <td class="linedrowrightcolumn"><b>Due Date </b></td>
                            <td class="linedrow"><b>Notes</b></td>
                        </tr>
                        <tr>
                            <td colspan="6" class="smallheader">ASN&nbsp;<span class="header">
                                <input name="Submit22" type="submit" class="smalltable" value="Add Project Task" style="vertical-align:middle;" />
                            </span></td>
                            </tr>
                        <tr>
						    <td bgcolor="#FF0000" class="linedrowrightcolumn">
						        <select name="select" class="smalltable">
									<option value="1" selected="selected">High</option>
									<option value="2">Medium</option>
									<option value="3">Low</option>
						        </select>
						    </td>
                            <td bgcolor="#FF0000" class="linedrowrightcolumn style1">Test with Nick and Troy </td>
                            <td bgcolor="#FF0000" class="linedrowrightcolumn style1">&nbsp;</td>
                            <td bgcolor="#FF0000" class="linedrowrightcolumn style1">
								<select name="select" class="smalltable">
									<option value="1">not started</option>
									<option value="2" selected="selected">in process</option>
									<option value="3">complete</option>
								</select>
							</td>
                            <td bgcolor="#FF0000" class="linedrowrightcolumn style1">3/9/2012</td>
                            <td bgcolor="#FF0000" class="linedrow style1">Scheduled to test at 10:30 AM </td>
                        </tr>
                        <tr>
						    <td bgcolor="#FF8000" class="linedrowrightcolumn">
						        <select name="select" class="smalltable">
									<option value="1">High</option>
									<option value="2" selected="selected">Medium</option>
									<option value="3">Low</option>
						        </select>
						    </td>
                            <td bgcolor="#FF8000" class="linedrowrightcolumn style1">Schedule monthly tests with Nick and Adam </td>
                            <td bgcolor="#FF8000" class="linedrowrightcolumn style1">&nbsp;</td>
                            <td bgcolor="#FF8000" class="linedrowrightcolumn style1">
								<select name="select" class="smalltable">
									<option value="1" selected="selected">not started</option>
									<option value="2">in process</option>
									<option value="3">complete</option>
								</select>
							</td>
                            <td bgcolor="#FF8000" class="linedrowrightcolumn style1">3/15/2012</td>
                            <td bgcolor="#FF8000" class="linedrow style1">Call nick and adam to set up and document </td>
                        </tr>
                        <tr>
                            <td colspan="6" class="smallheader">Testing Center <span class="header">
                                <input name="Submit222" type="submit" class="smalltable" value="Add Project Task" style="vertical-align:middle;" />
                            </span></td>
                            </tr>
                        <tr>
						    <td bgcolor="#FF0000" class="linedrowrightcolumn">
						        <select name="select" class="smalltable">
									<option value="1" selected="selected">High</option>
									<option value="2">Medium</option>
									<option value="3">Low</option>
						        </select>
						    </td>
                            <td bgcolor="#FF0000" class="linedrowrightcolumn style1">Tie in jump links around site </td>
                            <td bgcolor="#FF0000" class="linedrowrightcolumn style1">&nbsp;</td>
                            <td bgcolor="#FF0000" class="linedrowrightcolumn style1">
								<select name="select" class="smalltable">
									<option value="1" selected="selected">not started</option>
									<option value="2">in process</option>
									<option value="3">complete</option>
								</select>
							</td>
                            <td bgcolor="#FF0000" class="linedrowrightcolumn style1">&nbsp;</td>
                            <td bgcolor="#FF0000" class="linedrow style1">Work with dave smith </td>
                        </tr>
                    </table>
			    </td>
			</tr>
		</table>
		</td>
    </tr>
</table>
</div>

