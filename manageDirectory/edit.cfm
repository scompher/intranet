

<link rel="stylesheet" type="text/css" href="/styles.css">

<cfparam name="employeeid" default="0">
<cfparam name="photoImage" default="">
<cfparam name="name" default="">
<cfparam name="title" default="">
<cfparam name="ext" default="">
<cfparam name="email" default="">
<cfparam name="action" default="">
<cfparam name="showOnMpower" default="0">
<cfparam name="showOnWWW" default="0">
<cfparam name="did" default="0">

<cfset tempImageDir = "e:\websites\intranet\manageDirectory\images">
<cfset finalImageDir = "e:\websites\copalink\images\directoryimages">

<cfif action is "uploadimage">
	<cffile action="upload" filefield="photoImage" destination="#tempImageDir#" nameconflict="overwrite">
	<cfset uploadedFile = file.ServerFile>
	<cfset photoImage = uploadedFile>
</cfif>

<cfif action is "deleteimage">
	<cffile action="delete" file="#tempImageDir#\#photoImage#">
	<cfset photoImage = "">
</cfif>

<cfif isDefined("form.btnSavePerson")>
	
<cfquery name="saveperson" datasource="copalink">
	update directory_main
	set name='#name#',
	title='#title#',
	extension='#ext#',
	email='#email#',
	showonmpower=#showonmpower#,
	showonwww=#showonwww#,
	photo='#photoimage#'
	where directoryid=#did#
</cfquery>
	
	<cflocation url="index.cfm">
</cfif>

<cfquery name="getPerson" datasource="copalink">
	select * from directory_main 
	where directoryID = #did# 
</cfquery>
<cfif getPerson.recordcount gt 0>
	<cfset photoImage = getPerson.photo>
	<cfset name = getPerson.name>
	<cfset title = getPerson.title>
	<cfset ext = getPerson.extension>
	<cfset email = getPerson.email>
	<cfset showOnMpower = getPerson.showOnMPower>
	<cfset showOnWWW = getPerson.showOnWWW>
	<cffile action="copy" source="#finalImageDir#\#photoImage#" destination="#tempImageDir#\#photoImage#">
</cfif>

<cfform method="post" action="edit.cfm" enctype="multipart/form-data" name="directoryform">
<cfinput type="hidden" name="action" value="">
<cfinput type="hidden" name="did" value="#did#">
<div align="center">
<table border="0" cellspacing="0" cellpadding="5">
    <tr>
        <td class="highlightbar"><b>Edit a Directory Entry</b></td>
    </tr>
    <tr>
        <td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td width="12%" nowrap>Name:</td>
					<td width="88%">
					    <cfinput type="text" name="name" style="width:300px;" value="#name#">
					</td>
				</tr>
				<tr>
				    <td nowrap>Title:</td>
				    <td>
				        <cfinput type="text" name="title" style="width:300px;" value="#title#">
				    </td>
				    </tr>
				<tr>
				    <td nowrap>Extension:</td>
				    <td>
				        <cfinput type="text" name="ext" style="width:300px;" value="#ext#">
				    </td>
				    </tr>
				<tr>
				    <td nowrap>Email:</td>
				    <td>
				        <cfinput type="text" name="email" style="width:300px;" value="#email#">
				    </td>
				    </tr>
				<tr>
				    <td nowrap valign="top">Photo:</td>
					<cfif trim(photoImage) is not "">
						<cfinput type="hidden" name="photoImage" value="#photoImage#">
						<td class="nopadding" valign="top">
							<table border="0" cellspacing="0" cellpadding="5" class="greyrownopadding">
								<tr>
									<td><img src="/manageDirectory/images/<cfoutput>#photoImage#</cfoutput>" border="1" /></td>
									<td valign="top">
									<a href="javascript:document.directoryform.action.value='deleteimage'; document.directoryform.submit();">Delete</a><br />
									</td>
								</tr>
							</table>
						</td>
					<cfelse>
						<td>
							<cfinput type="file" name="photoImage" onChange="this.form.action.value='uploadimage'; this.form.submit();">
						</td>
					</cfif>
			    </tr>
				<tr>
				    <td colspan="2">
				        <input <cfif showOnMpower is 1>checked</cfif> type="checkbox" name="showOnMpower" value="1" style="vertical-align:middle;"> Show on MPower </td>
				    </tr>
				<tr>
				    <td colspan="2">
				        <input <cfif showOnWWW is 1>checked</cfif> type="checkbox" name="showOnWWW" value="1" style="vertical-align:middle;"> Show on WWW Site </td>
			    </tr>
				<tr>
				    <td colspan="2">
				        <input type="submit" name="btnSavePerson" value="Update Person" />
						<input type="button" name="btnCancel" value="Cancel" onclick="document.location='index.cfm';" />
				    </td>
				    </tr>
			</table>
		</td>
    </tr>
</table>
</div>
</cfform>
<div class="normal" align="center">
<br />
<a class="normal" style="text-decoration:none;" href="index.cfm">Return to Previous Page</a>
</div>

