
<link rel="stylesheet" type="text/css" href="/styles.css">

<cfparam name="employeeid" default="0">
<cfparam name="photoImage" default="">
<cfparam name="name" default="">
<cfparam name="title" default="">
<cfparam name="ext" default="">
<cfparam name="email" default="">
<cfparam name="action" default="">
<cfparam name="showOnMpower" default="1">
<cfparam name="showOnWWW" default="1">

<cfset tempImageDir = "e:\websites\intranet\manageDirectory\images">
<cfset finalImageDir = "e:\websites\copalink\images\directoryimages">

<cfif action is "uploadimage">
	<cffile action="upload" filefield="photoImage" destination="#tempImageDir#" nameconflict="overwrite">
	<cfset uploadedFile = file.ServerFile>
	<cfset photoImage = uploadedFile>
</cfif>

<cfif action is "deleteimage">
	<cffile action="delete" file="#tempImageDir#\#photoImage#">
	<cfset photoImage = "">
</cfif>

<cfif isDefined("form.btnSavePerson")>
	<cfquery name="" datasource="copalink">
		insert into directory_main (name, title, extension, email, photo, showOnMpower, showOnWWW, position, visible) 
		values ('#name#', '#title#', '#ext#', '#email#', '#photoImage#', #showOnMpower#, #showOnWWW#, 1, 1)  
	</cfquery>
	<!--- copy file to final location --->
	<cffile action="copy" source="#tempImageDir#\#photoImage#" destination="#finalImageDir#\#photoImage#" nameconflict="overwrite">
	<cflocation url="index.cfm">
</cfif>

<cfform method="post" action="add.cfm" enctype="multipart/form-data" name="directoryform">
<cfinput type="hidden" name="action" value="">
<div align="center">
<table border="0" cellspacing="0" cellpadding="5">
    <tr>
        <td class="highlightbar"><b>Add a Directory Entry</b></td>
    </tr>
    <tr>
        <td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td width="12%" nowrap>Name:</td>
					<td width="88%">
					    <cfinput type="text" name="name" style="width:300px;" value="#name#">
					</td>
				</tr>
				<tr>
				    <td nowrap>Title:</td>
				    <td>
				        <cfinput type="text" name="title" style="width:300px;" value="#title#">
				    </td>
				    </tr>
				<tr>
				    <td nowrap>Extension:</td>
				    <td>
				        <cfinput type="text" name="ext" style="width:300px;" value="#ext#">
				    </td>
				    </tr>
				<tr>
				    <td nowrap>Email:</td>
				    <td>
				        <cfinput type="text" name="email" style="width:300px;" value="#email#">
				    </td>
				    </tr>
				<tr>
				    <td nowrap valign="top">Photo:</td>
					<cfif trim(photoImage) is not "">
						<cfinput type="hidden" name="photoImage" value="#photoImage#">
						<td class="nopadding" valign="top">
							<table border="0" cellspacing="0" cellpadding="5" class="greyrownopadding">
								<tr>
									<td><img src="/manageDirectory/images/<cfoutput>#photoImage#</cfoutput>" border="1" /></td>
									<td valign="top">
									<a href="javascript:document.directoryform.action.value='deleteimage'; document.directoryform.submit();">Delete</a><br />
									</td>
								</tr>
							</table>
						</td>
					<cfelse>
						<td>
							<cfinput type="file" name="photoImage" onChange="this.form.action.value='uploadimage'; this.form.submit();">
						</td>
					</cfif>
			    </tr>
				<tr>
				    <td colspan="2">
				        <input <cfif showOnMpower is 1>checked</cfif> type="checkbox" name="showOnMpower" value="1" style="vertical-align:middle;"> Show on MPower </td>
				    </tr>
				<tr>
				    <td colspan="2">
				        <input <cfif showOnWWW is 1>checked</cfif> type="checkbox" name="showOnWWW" value="1" style="vertical-align:middle;"> Show on WWW Site </td>
			    </tr>
				<tr>
				    <td colspan="2">
				        <input type="submit" name="btnSavePerson" value="Add Person" />
						<input type="button" name="btnCancel" value="Cancel" onclick="document.location='index.cfm';" />
				    </td>
				    </tr>
			</table>
		</td>
    </tr>
</table>
</div>
</cfform>
<div class="normal" align="center">
<br />
<a class="normal" style="text-decoration:none;" href="index.cfm">Return to Previous Page</a>
</div>