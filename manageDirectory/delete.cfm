
<cfset finalImageDir = "e:\websites\copalink\images\directoryimages">

<cfparam name="did" default="0">

<!--- get photo file name --->
<cfquery name="getPhotoImage" datasource="copalink">
	select photo from directory_main 
	where directoryid = #did#
</cfquery>
<cfset photoImage = getPhotoImage.photo>

<cfquery name="delPerson" datasource="copalink">
	update directory_main 
	set visible = 0 
	where directoryid = #did# 
</cfquery>

<!--- delete file --->
<cfif fileExists("#finalImageDir#\#photoImage#")>
	<cffile action="delete" file="#finalImageDir#\#photoImage#">
</cfif>

<cflocation url="index.cfm">
