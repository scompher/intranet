<cfif isdefined("form.btnsaveposition")>
	<cfset sortlistlen=listlen(sortorder)>
		<cfloop from="1" to="#sortlistlen#" index="i">
			<cfset did=listgetat(sortorder,i)>
			<cfquery name="savepositions" datasource="copalink">
				update directory_main
				set position=#i#
				where directoryid=#did#		
			</cfquery>
		</cfloop>
		<div align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold;">Saving Positions...</div>
		<meta http-equiv="refresh" content="2;index.cfm" />
		<cfabort>
</cfif>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/themes/smoothness/jquery-ui.css" />
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js"></script>
<cfset imageDir = "https://copalink.copsmonitoring.com/images/directoryimages">
 <script>
  $(function() {
  		
  $("#sort").on("sortupdate", function (event,ui) {
 	 var sortedIDs = $("#sort").sortable("toArray",{attribute: "value"});
 	 document.directoryform.sortorder.value = sortedIDs;
 });
  
	$("#sort").sortable().disableSelection();
	
	
  });
  </script>
<cfquery name="getUsers" datasource="copalink">
	select * 
	from directory_main 
	where visible = 1 
	order by position asc 
</cfquery>

<form name="directoryform" method="post" action="index.cfm">
<input type="hidden" name="sortorder" value="" />
<div align="center">
<table width="465" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="nopadding">
			<table width="100%" border="0" cellpadding="5" cellspacing="0" class="greyBorder">
				<tr>
					<td>&nbsp;</td>
					<td >
						<input type="button" value="Add a Person" onclick="document.location='add.cfm';" />
					</td>
					<td >
						<input type="submit" name="btnsaveposition" value="Save Positions" />
					</td>
				</tr>
				<tr>
					<td></td>
					<td colspan="2">
					<strong>Drag and Drop to rearrange the order (then click save positions) </strong>
					</td>
				</tr>
<!---				<tr valign="middle">
					<td>&nbsp;</td>
					<td align="center" class="greyRow"><img src="http://www.copalink.com/images/directoryimages/copsLogo.jpg" width="50" height="75"></td>
					<td class="greyRow"><span class="name">Sales</span><br>
					Extension 1256<br>
					<a href="mailto:sales@copsmonitoring.com">sales@copsmonitoring.com</a>
					</td>
				</tr>
				<tr valign="middle">
					<td>&nbsp;</td>
					<td align="center" class="greyRow"><img src="http://www.copalink.com/images/directoryimages/copsLogo.jpg" width="50" height="75"></td>
					<td class="greyRow"><span class="name">Dealer Support</span><br>
						Extension 1802 <br>
						<a href="mailto:dsupport@copsmonitoring.com">dsupport@copsmonitoring.com</a>
					</td>
				</tr>
				<tr valign="middle">
					<td>&nbsp;</td>
					<td align="center" class="greyRow"><img src="http://www.copalink.com/images/directoryimages/copsLogo.jpg" width="50" height="75"></td>
					<td class="greyRow"><span class="name">Data Entry</span><br>
						Extension 1803<br>
						<a href="mailto:dataentry@copsmonitoring.com">dataentry@copsmonitoring.com</a>
					</td>
				</tr>
				<tr valign="middle">
					<td>&nbsp;</td>
					<td align="center" class="greyRow"><img src="http://www.copalink.com/images/directoryimages/copsLogo.jpg" width="50" height="75"></td>
					<td class="greyRow"><span class="name">Accounting</span><br>
					Extension 1804<br>
					<a href="mailto:accounting@copsmonitoring.com">accounting@copsmonitoring.com</a>
					</td>
				</tr>--->
				</table>
				
				<ul id="sort">
				<cfoutput query="getUsers">
				<li value="#directoryid#" style="list-style-type:none">
				<table width="465" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td align="center" valign="middle" width="40">
						<a onclick="return confirm('Are you sure you wish to delete this person?');" href="delete.cfm?did=#directoryid#"><img src="icons/no.png" alt="Delete" border="0" /></a>&nbsp;
						<a href="edit.cfm?did=#directoryid#"><img src="icons/pencil.png" alt="Edit" border="0" /></a>
					</td>
					<td width="50" valign="top" align="center" class="greyRow"><img src="#imageDir#/#photo#" width="50" height="75" border="1"></td>
					<td valign="top" class="greyRow">
						<span class="name">#name#</span><br>
						<span class="title">
						#title#</span><br>
						Extension #extension#<br>
						<a href="mailto:#email#">#email#</a><br />
						<i>
						<cfif showOnMpower is not 0>This will show on MPower</cfif><br />
						<cfif showOnWWW is not 0>This will show on www.copsmonitoring.com</cfif><br />
						</i>
						</span>
					</td>
				</tr>
				</table>
				</li>
				</cfoutput>
				</ul>
			</table>
		</td>
	</tr>
</table>
<br />
<a href="/index.cfm" class="normal" style="text-decoration:underline;">Return to Intranet</a>
</div>
</form>