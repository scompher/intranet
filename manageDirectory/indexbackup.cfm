
<cfset imageDir = "http://192.168.107.7/images/directoryimages">

<cfquery name="getUsers" datasource="copalink">
	select * 
	from directory_main 
	where visible = 1 
	order by position asc 
</cfquery>

<form method="post" action="index.cfm">
<div align="center">
<table width="465" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="nopadding">
			<table width="100%" border="0" cellpadding="5" cellspacing="0" class="greyBorder">
				<tr>
					<td>&nbsp;</td>
					<td colspan="2">
						<input type="button" value="Add a Person" onclick="document.location='add.cfm';" />
					</td>
				</tr>
				<tr valign="middle">
					<td>&nbsp;</td>
					<td align="center" class="greyRow"><img src="http://www.copalink.com/images/directoryimages/copsLogo.jpg" width="50" height="75"></td>
					<td class="greyRow"><span class="name">Sales</span><br>
					Extension 1256<br>
					<a href="mailto:sales@copsmonitoring.com">sales@copsmonitoring.com</a>
					</td>
				</tr>
				<tr valign="middle">
					<td>&nbsp;</td>
					<td align="center" class="greyRow"><img src="http://www.copalink.com/images/directoryimages/copsLogo.jpg" width="50" height="75"></td>
					<td class="greyRow"><span class="name">Dealer Support</span><br>
						Extension 1802 <br>
						<a href="mailto:dsupport@copsmonitoring.com">dsupport@copsmonitoring.com</a>
					</td>
				</tr>
				<tr valign="middle">
					<td>&nbsp;</td>
					<td align="center" class="greyRow"><img src="http://www.copalink.com/images/directoryimages/copsLogo.jpg" width="50" height="75"></td>
					<td class="greyRow"><span class="name">Data Entry</span><br>
						Extension 1803<br>
						<a href="mailto:dataentry@copsmonitoring.com">dataentry@copsmonitoring.com</a>
					</td>
				</tr>
				<tr valign="middle">
					<td>&nbsp;</td>
					<td align="center" class="greyRow"><img src="http://www.copalink.com/images/directoryimages/copsLogo.jpg" width="50" height="75"></td>
					<td class="greyRow"><span class="name">Accounting</span><br>
					Extension 1804<br>
					<a href="mailto:accounting@copsmonitoring.com">accounting@copsmonitoring.com</a>
					</td>
				</tr>
				<cfoutput query="getUsers">
				<tr>
					<td align="center" valign="middle" width="40">
						<a onclick="return confirm('Are you sure you wish to delete this person?');" href="delete.cfm?did=#directoryid#"><img src="icons/no.png" alt="Delete" border="0" /></a>&nbsp;
						<a href="edit.cfm?did=#directoryid#"><img src="icons/pencil.png" alt="Edit" border="0" /></a>
					</td>
					<td width="50" valign="top" align="center" class="greyRow"><img src="#imageDir#/#photo#" width="50" height="75" border="1"></td>
					<td valign="top" class="greyRow">
						<span class="name">#name#</span><br>
						<span class="title">
						#title#</span><br>
						Extension #extension#<br>
						<a href="mailto:#email#">#email#</a><br />
						<cfif showOnMpower is not 0>Show on MPower</cfif><br />
						<cfif showOnWWW is not 0>Show on WWW</cfif><br />
						</span>
					</td>
				</tr>
				</cfoutput>
				<tr>
					<td>&nbsp;</td>
					<td colspan="2">
						<input type="button" value="Add a Person" onclick="document.location='add.cfm';" />
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<br />
<a href="/index.cfm" class="normal" style="text-decoration:underline;">Return to Intranet</a>
</div>
</form>