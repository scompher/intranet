
<cfset filedir = "#request.DirectPath#\newsletter\files">

<!--- delete email files --->
<cfdirectory action="list" directory="#filedir#" name="getfiles" filter="#id#-*" sort="asc">
<cfloop query="getfiles">
	<cffile action="delete" file="#filedir#/#getfiles.name#">
</cfloop>

<!--- delete newsletter --->
<cfquery name="delnewsletter" datasource="#ds#">
	delete from newsletter_main
	where newsletterid = #id#
</cfquery>

<cflocation url="index.cfm">