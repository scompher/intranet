
<style type="text/css">
	TABLE {font-family:Arial, Helvetica, sans-serif;font-size:12px; background-color:#FFFFFF;}
	.normal {font-family:Arial, Helvetica, sans-serif;font-size:12px}
	.heading {font-family:Arial, Helvetica, sans-serif;font-size:16px}
	.titlebar {font-family: Arial, Helvetica, sans-serif;	font-size: 12px;font-weight: bold;color: #FFFFFF;}
	.small {font-family:Verdana, Arial, Helvetica, sans-serif;font-size:10px}
	a:link {color:#000000; text-decoration: underline;}
	a:visited {text-decoration: underline; color: #000000;}
	a:hover {text-decoration: underline; color: #0000FF;}
	a:active {text-decoration: underline; color: #000000;}
	.box {border: 1px solid #000000;}
	.header {font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #FFFFFF; background-color: #000066;}
</style>

<cfset filedir = "#request.DirectPath#\newsletter\files">
<cffile action="read" file="#filedir#\#url.file#" variable="fileContents">

<div align="center">
<table width="725" border="1" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center" bgcolor="000066"><b class="titlebar">View File Contents</b></td>
	</tr>
	
	<tr>
		<td><cfoutput>#replace(fileContents, "#chr(13)#", "<br>", "all")#</cfoutput></td>
	</tr>
</table>
</div>
