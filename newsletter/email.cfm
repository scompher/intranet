<style type="text/css">
	TABLE {font-family:Arial, Helvetica, sans-serif;font-size:12px; background-color:#FFFFFF;}
	.normal {font-family:Arial, Helvetica, sans-serif;font-size:12px}
	.heading {font-family:Arial, Helvetica, sans-serif;font-size:16px}
	.titlebar {font-family: Arial, Helvetica, sans-serif;	font-size: 12px;font-weight: bold;color: #FFFFFF;}
	.small {font-family:Verdana, Arial, Helvetica, sans-serif;font-size:10px}
	a:link {color:#000000; text-decoration: underline;}
	a:visited {text-decoration: underline; color: #000000;}
	a:hover {text-decoration: underline; color: #0000FF;}
	a:active {text-decoration: underline; color: #000000;}
	.box {border: 1px solid #000000;}
	.header {font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #FFFFFF; background-color: #000066;}
</style>

<cfparam name="id" default="0">
<cfset filedir = "#request.DirectPath#\newsletter\files">

<cfif isdefined("form.btnUploadFile") and trim(form.emailfile) IS NOT "">
	<cffile action="upload" filefield="emailfile" destination="#filedir#" nameconflict="makeunique">
	<cfset uploadedfile = file.ClientFile>
	<cfset savedfile = file.ServerFile>
	<cfset newname = "#id#-#uploadedfile#">
	<cffile action="rename" source="#filedir#/#savedfile#" destination="#filedir#/#newname#">
</cfif>

<cfif isDefined("form.btnCancel")>
	<cflocation url="index.cfm">
</cfif>

<cfif isDefined("form.btnProcess")>

<cfabort>
</cfif>

<cfdirectory action="list" directory="#filedir#" name="getfiles" filter="#id#-*" sort="asc">

<div align="center">
<form action="email.cfm" method="post" enctype="multipart/form-data">
<cfoutput>
<input type="hidden" name="id" value="#id#">
</cfoutput>
<table width="650" border="1" cellpadding="5" cellspacing="0" class="normal">
	<tr>
		<td width="624" colspan="2" align="center" bgcolor="000066" class="titlebar">Newsletter Administration - Upload Email Adress Files </td>
	</tr>
	
	<tr>
		<td colspan="2" nowrap><table border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td><b>Upload Email File:</b> </td>
				<td width="7">&nbsp;</td>
				<td><input type="file" name="emailfile"></td>
				<td><input type="submit" name="btnUploadFile" value="Upload File" class="normal"></td>
			</tr>
		</table></td>
	</tr>
	<tr>
		<td colspan="2" nowrap><b>Existing Files:</b> <br>
		<cfif getfiles.recordcount GT 0>
		<cfoutput query="getfiles">
			<table border="0" cellpadding="2" cellspacing="0" class="normal">
				<tr>
					<td>#replace(getfiles.name, "#id#-", "", "all")#</td>
					<td width="7">&nbsp;</td>
					<td class="small"><a target="_blank" href="viewfile.cfm?file=#getfiles.name#&id=#id#">view</a> | <a href="removefile.cfm?file=#getfiles.name#&id=#id#">remove</a></td>
				</tr>
			</table>
		</cfoutput>
		<cfelse>
		No files have been uploaded
		</cfif>
		</td>
	</tr>
	<tr>
		<td colspan="2" nowrap>
		<input type="submit" name="btnProcess" value="Process files and Send Newsletter">&nbsp;
		<input type="submit" name="btnCancel" value="Cancel Process">
		</td>
	</tr>
</table>
</form>
</div>
