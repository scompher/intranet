
<style type="text/css">
	TABLE {font-family:Arial, Helvetica, sans-serif;font-size:12px; background-color:#FFFFFF;}
	.normal {font-family:Arial, Helvetica, sans-serif;font-size:12px}
	.heading {font-family:Arial, Helvetica, sans-serif;font-size:16px}
	.titlebar {font-family: Arial, Helvetica, sans-serif;	font-size: 12px;font-weight: bold;color: #FFFFFF;}
	.small {font-family:Verdana, Arial, Helvetica, sans-serif;font-size:10px}
	a:link {color:#000000; text-decoration: underline;}
	a:visited {text-decoration: underline; color: #000000;}
	a:hover {text-decoration: underline; color: #0000FF;}
	a:active {text-decoration: underline; color: #000000;}
	.box {border: 1px solid #000000;}
	.header {font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #FFFFFF; background-color: #000066;}
</style>

<cfquery name="getnewsletters" datasource="#ds#">
	select * from newsletter_main
	order by datecreated DESC, sent asc
</cfquery>

<div align="center" class="normal">
<table width="725" border="1" cellspacing="0" cellpadding="5">
	<tr>
		<td colspan="5" align="center" bgcolor="#000066" class="titlebar">Newsletter Administration </td>
	</tr>
	<cfif getnewsletters.recordcount GT 0>
	<tr>
		<td width="75" align="center" nowrap="nowrap"><b>Action</b></td>
		<td width="423"><b>Newsletter Subject </b></td>
		<td width="77" align="center" nowrap="nowrap"><b>Date Created </b></td>
		<td width="26" align="center" nowrap="nowrap"><b>Sent</b></td>
		<td width="62" align="center" nowrap="nowrap"><b>Date Sent </b></td>
	</tr>
	<cfoutput query="getnewsletters">
	<tr>
		<td align="left" valign="top" nowrap="nowrap">
		<a target="_blank" href="viewnewsletter.cfm?id=#getnewsletters.newsletterid#">View</a><br>
		<a href="newsletter.cfm?id=#getnewsletters.newsletterid#&edit=yes">Edit</a><br>
		<a href="javascript: if (confirm('Are you sure you wish to delete this item?')) {document.location='deletenewsletter.cfm?id=#getnewsletters.newsletterid#';}">Delete</a><br>
		<a href="##">Send Out</a>
		</td>
		<td valign="top">#getnewsletters.title#</td>
		<td align="center" valign="top" nowrap="nowrap">#dateformat(getnewsletters.dateCreated,'mm/dd/yyyy')#</td>
		<td align="center" valign="top" nowrap="nowrap"><cfif getnewsletters.sent IS 1>Yes<cfelse>No</cfif></td>
		<td align="center" valign="top" nowrap="nowrap">#dateformat(getnewsletters.dateSent,'mm/dd/yyyy')#&nbsp;</td>
	</tr>
	</cfoutput>
	<tr>
		<td align="center" valign="top" nowrap="nowrap"><b><a href="newsletter.cfm">Create  New</a></b></td>
		<td colspan="4" valign="top">&nbsp;</td>
	</tr>
	<cfelse>
	<tr>
		<td colspan="5" valign="top">There are no newsletters on file</td>
	</tr>
	<tr>
		<td colspan="5" align="left" valign="top" nowrap="nowrap"><b><a href="newsletter.cfm">Create  New</a></b></td>
	</tr>
	</cfif>
</table>
<br />
<a href="/index.cfm">Return to Intranet</a>
</div>
