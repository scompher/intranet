<style type="text/css">
	TABLE {font-family:Arial, Helvetica, sans-serif;font-size:12px; background-color:#FFFFFF;}
	.normal {font-family:Arial, Helvetica, sans-serif;font-size:12px}
	.heading {font-family:Arial, Helvetica, sans-serif;font-size:16px}
	.titlebar {font-family: Arial, Helvetica, sans-serif;	font-size: 12px;font-weight: bold;color: #FFFFFF;}
	.small {font-family:Verdana, Arial, Helvetica, sans-serif;font-size:10px}
	a:link {color:#000000; text-decoration: underline;}
	a:visited {text-decoration: underline; color: #000000;}
	a:hover {text-decoration: underline; color: #0000FF;}
	a:active {text-decoration: underline; color: #000000;}
	.box {border: 1px solid #000000;}
	.header {font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #FFFFFF; background-color: #000066;}
</style>

<cfif isdefined("form.btnsave")>

	<cfif newsletterid is 0>
		<cfinclude template="add.cfm">
	<cfelse>
		<cfinclude template="edit.cfm">
	</cfif>
	<!---<cflocation url="email.cfm?newsletterid=#id#">--->
	<cflocation url="index.cfm">
</cfif>

<cfset thisDir = "http://" & CGI.HTTP_HOST & Replace(CGI.SCRIPT_NAME, "newsletter.cfm","")>

<cfparam name="body" default="">
<cfparam name="subject" default="">
<cfparam name="newsletterid" default="0">

<cfif isdefined("url.edit")>
	<cfquery name="getnewsletter" datasource="#ds#">
		select * from newsletter_main
		where newsletterid = #id#
	</cfquery>
	<cfset body = getnewsletter.body>
	<cfset subject = getnewsletter.title>
	<cfset newsletterid = id>
</cfif>

<div align="center">
<form action="proof.cfm" method="post" enctype="multipart/form-data">
	<cfoutput>
	<input type="hidden" name="newsletterid" value="#newsletterid#" />
	</cfoutput>
	<p class="normal"><a href="index.cfm">Return to Newsletter Administration</a></p>
	<p class="normal"><a target="_blank" href="/activedit/docs/users_guide/index.htm">Click here for documentation</a></p>
	<table border="1" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center" bgcolor="#000066" class="titlebar">Create A Newsletter</td>
	</tr>
	<tr>
		<td>
		<b>Subject:</b><br />
		<cfoutput>
		<input name="subject" type="text" class="normal" style="width:650px" value="#subject#" />
		</cfoutput>
		</td>
	</tr>
	<tr>
		<td valign="top">
		<b>Body:</b><br />
		<cf_activedit name="body"
			toolbar="font,bold,italic,underline,|,outdent,indent,|,justifyleft,justifycenter,justifyright,bullets,|,table,image,hyperlink,|,subscript,superscript,strike,highlight,removeformat,replace,||,quickfont,quickfontsize,|,cut,copy,paste,redo,undo"
			imagepath="#request.DirectPath#images\newsletter\"
			imageurl="http://192.168.3.236/images/newsletter/"
			inc="/activedit/inc/"
			defaultfont="10pt Arial"
			tabview="true"
			width="650" 
			height="600" 
			breakonenter="yes"
			><cfoutput>#body#</cfoutput></cf_activedit>
		</td>
	</tr>
	<tr>
		<td valign="top">
		<input type="submit" name="Submit" value="Proof Newsletter"/>
		</td>
	</tr>
</table>
</form>
</div>
