
<cfsetting showdebugoutput="no">

<cfparam name="aid" default="0">

<cfquery name="getMain" datasource="copalink">
	select * from employmentApp_main 
	where applicationid = #aid# 
</cfquery>
<cfquery name="getGeneral" datasource="copalink">
	select * from employmentApp_general
	where applicationid = #aid# 
</cfquery>
<cfquery name="getPersonal" datasource="copalink">
	select * from employmentApp_personal
	where applicationid = #aid# 
</cfquery>
<cfquery name="getEducation" datasource="copalink">
	select * from employmentApp_education 
	where applicationid = #aid# 
</cfquery>
<cfquery name="getEmploymentHistory" datasource="copalink">
	select * from employmentApp_employmentHistory 
	where applicationid = #aid# 
</cfquery>
<cfquery name="getEmploymentInfo" datasource="copalink">
	select * from employmentApp_employmentInfo 
	where applicationid = #aid# 
</cfquery>
<cfquery name="getReferences" datasource="copalink">
	select * from employmentApp_references 
	where applicationid = #aid# 
</cfquery>

<!--- mark read --->
<cfif getMain.isRead is 0>
	<cfquery name="getSec" datasource="#ds#">
		select admin_users.*, admin_users_departments_lookup.*
		from admin_users 
		inner join admin_users_departments_lookup on admin_users.adminuserid = admin_users_departments_lookup.adminuserid
		where admin_users.adminuserid = #cookie.adminlogin# 
	</cfquery>
	<cfset readBy = "Viewed by #getSec.firstname# #getSec.lastname# at #dateformat(now(),'mm/dd/yyyy')# #timeformat(now(),'hh:mm tt')#">
	<cfquery name="updateRead" datasource="copalink">
		update employmentApp_main 
		set isRead = 1, openedBy = '#readBy#' 
		where applicationid = #aid# 
	</cfquery>
</cfif>

<cfdocument format="pdf">

<style>
	.normal {
		font-family:Arial, Helvetica, sans-serif; 
		font-size:12px; 
		color:#000000;
	}
	TABLE {
		font-family:Arial, Helvetica, sans-serif; 
		font-size:12px; 
		color:#000000;
	}
</style>

<br />

<div align="center" class="normal">

<img src="/images/cops-logo.jpg" />

<br />
<br />

<table width="600" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td>

			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td style="background-color:#000000; color:#FFFFFF; font-weight:bold;" align="center">APPLICANT INFORMATION</td>
				</tr>
			</table>
			
			<br />
			<cfoutput>
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td width="60%"><b>Reference ##</b>: <br /> #getMain.referenceNum#</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td width="60%"><b>Full Name:</b> <br /> 
						#getpersonal.firstname# #getpersonal.lastname#</td>
					<td><b>Date:</b> <br /> #dateformat(getmain.datetimecreated, 'mm/dd/yyyy')#</td>
				</tr>
			</table>
			
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td width="60%"><b>Address:</b> <br /> 
						#getpersonal.address1#, #getpersonal.city#, #getpersonal.state#  #getpersonal.zip#</td>
					<td><b>Apartment/Unit ##:</b> <br /> #getpersonal.address2#</td>
				</tr>
			</table>
			
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td><b>Phone:</b> <br /> 
						Day: #getpersonal.dayphone# Eve: #getpersonal.evePhone#</td>
					<td><b>E-Mail Address:</b> <br /> 
						#getpersonal.email#</td>
				</tr>
			</table>
			
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td>
					<b>Bilingual:</b> <br />
					<cfif getpersonal.bilingual is 1>YES<cfelse>NO</cfif>
					</td>
				</tr>
				<cfif getpersonal.bilingual is 1>
				<tr>
					<td>
					<b>Other Languages:</b> <br />
					#getpersonal.otherlanguages#
					</td>
				</tr>
				</cfif>
			</table>
			
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td><b>How did you hear about us?</b> <br /> 
						#getpersonal.hearAboutUs#
					</td>
					<td>
					<b>Hear about other/reference ## :</b> <br /> 
					<cfif trim(getpersonal.hearAboutUsOther) is not "">
						#getpersonal.hearAboutUsOther#
					<cfelse>
						#getpersonal.referenceNumber#
					</cfif>
					</td>
				</tr>
			</table>
			
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td width="33%"><b>Date Available: </b> <br /> 
						#getemploymentinfo.dateavailable#</td>
					<td width="33%"><b>Social Security No: </b> <br /> 
						#getpersonal.social#</td>
					<td width="33%"><b>Desired Salary:</b> <br /> 
						#getemploymentinfo.desiredSalary#</td>
				</tr>
			</table>

			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td width="50%"><b>Position Applied For: </b> <br /> 
						#getemploymentinfo.position#</td>
					<td width="50%"><b>Shift Availability: </b> <br /> 
						#getemploymentinfo.shiftdesired# #getemploymentinfo.hoursdesired#</td>
				</tr>
			</table>

			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td width="60%"><b>Are you a citizen of the U.S.?</b> <br /> <cfif getpersonal.uscitizen is 1>YES<cfelse>NO</cfif> </td>
					<td>
					<cfif getpersonal.uscitizen is 0>
						<b>Are you eligible to work in the U.S.?</b> <br /> <cfif getpersonal.authorizedToWork is 1>YES<cfelse>NO</cfif>
					<cfelse>
						&nbsp;
					</cfif>
					</td>
				</tr>
			</table>

			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td width="60%"><b>Have you ever worked for this company? </b> <br /> <cfif getemploymentinfo.workedBefore is 1>YES<cfelse>NO</cfif></td>
					<td><b>If so, when? </b> <br /> #getemploymentinfo.workedWhen#</td>
				</tr>
			</table>
<!---
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td width="60%"><b>Have you ever been convicted of a felony?</b> <br /> <cfif getgeneral.felonCharge is 1>YES<cfelse>NO</cfif></td>
					<td><b>If yes, explain: </b> <br /> #getgeneral.felonwhen# #getgeneral.felonWhere#</td>
				</tr>
			</table>
--->
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td width="60%"><b>Do you know anyone who works here?</b> <br /> <cfif getemploymentinfo.knowEmployee is 1>YES<cfelse>NO</cfif></td>
					<td><b>If so, whom? </b> <br /> #getemploymentinfo.knowEmployeeWho#</td>
				</tr>
			</table>
			</cfoutput>
			<br />

			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td style="background-color:#000000; color:#FFFFFF; font-weight:bold;" align="center">EDUCATION</td>
				</tr>
			</table>

			<br />

			<cfoutput query="getEducation">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td width="60%"><b>#getEducation.typeSchool#:</b> <br /> 
							#getEducation.schoolName#
						</td>
						<td><b>City: </b> <br /> #getEducation.schoolCity#</td>
					</tr>
				</table>
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td width="15%"><b>From:</b> <br /> 
							#getEducation.schoolFrom#</td>
						<td width="15%"><b>To:</b> <br /> 
							#getEducation.schoolTo#</td>
						<td width="30%"><b>Did you graduate? </b> <br /> 
							<cfif getEducation.graduate is 1>YES<cfelse>NO</cfif>
						</td>
						<td><b>Degree:</b> <br /> #getEducation.degree#</td>
					</tr>
				</table>
				<cfif getEducation.currentRow lt getEducation.recordCount>
				<hr />
				</cfif>
			</cfoutput>

			<br />

			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td style="background-color:#000000; color:#FFFFFF; font-weight:bold;" align="center">REFERENCES</td>
				</tr>
			</table>
			
			<br />
			
			<cfif getReferences.recordcount gt 0>
				<cfoutput query="getReferences">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td width="60%"><b>Full Name: </b> <br /> 
							#getReferences.fullName#</td>
						<td><b>Relationship: </b> <br /> 
							#getReferences.relationship#</td>
					</tr>
				</table>
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td width="60%"><b>Company:</b> <br /> 
							#getReferences.occupation#</td>
						<td><b>Phone:</b> <br /> #getReferences.phone#</td>
					</tr>
				</table>
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td><b>Address:</b> <br /> #getReferences.address#</td>
					</tr>
				</table>
				<cfif getReferences.currentRow LT getReferences.recordcount>
				<hr />
				</cfif>
				</cfoutput>
			<cfelse>
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td>No references were supplied</td>
					</tr>
				</table>
			</cfif>
			
			<br />

			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td style="background-color:#000000; color:#FFFFFF; font-weight:bold;" align="center">PREVIOUS EMPLOYMENT</td>
				</tr>
			</table>
			
			<br />

			<cfif getEmploymentHistory.recordcount gt 0>
				<cfoutput query="getEmploymentHistory">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td><b>Company:</b> <br /> #getEmploymentHistory.company#</td>
						<td><b>Phone: </b> <br /> #getEmploymentHistory.companyPhone#</td>
					</tr>
				</table>
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td><b>Address:</b> <br /> #getEmploymentHistory.companyAddress#</td>
						<td><b>Supervisor:</b> <br /> #getEmploymentHistory.supervisor#</td>
					</tr>
				</table>
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td><b>Job Title: </b> <br /> #getEmploymentHistory.title#</td>
						<td><b>Starting Salary:</b> <br /> #getEmploymentHistory.startingSalary#</td>
						<td><b>Ending Salary:</b> <br /> #getEmploymentHistory.endingSalary#</td>
					</tr>
				</table>
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td><b>From:</b> <br /> #getEmploymentHistory.employmentFrom#</td>
						<td><b>To:</b> <br /> #getEmploymentHistory.employmentTo#</td>
						<td><b>Reason for leaving:</b> <br /> #getEmploymentHistory.reasonForLeaving#</td>
					</tr>
				</table>
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td><b>May we contact your previous supervisor for a reference?</b> <br /> <cfif getEmploymentHistory.contactForReference is 1>YES<cfelse>NO</cfif></td>
					</tr>
				</table>
				<cfif getEmploymentHistory.currentRow lt getEmploymentHistory.recordcount>
				<hr />
				</cfif>
				</cfoutput>
			<cfelse>
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td>No employment history was supplied</td>
					</tr>
				</table>
			</cfif>

			<br />

			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td style="background-color:#000000; color:#FFFFFF; font-weight:bold;" align="center">MILITARY SERVICE</td>
				</tr>
			</table>
			
			<br />

			<cfoutput query="getGeneral">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td><b>Branch:</b> <br /> #getGeneral.militaryBranch#</td>
					<td><b>From:</b> <br /> #getGeneral.militaryFrom#</td>
					<td><b>To:</b> <br /> #getGeneral.militaryTo#</td>
				</tr>
			</table>
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td><b>Rank at discharge:</b> <br /> #getGeneral.dischargeRank#</td>
					<td><b>Type of discharge:</b> <br /> #getGeneral.dischargeType#</td>
				</tr>
			</table>
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td>
					<b>If other than honorable, explain:</b> <br />
					#getGeneral.dischargeDetail#
					</td>
				</tr>
			</table>
			</cfoutput>
			
<!---			<br />
			<cfdocumentitem type="pagebreak"></cfdocumentitem> 
			
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td colspan="2"><b>ATTENTION APPLICANT: PLEASE READ STATEMENT BELOW BEFORE SIGNING:</b></td>
				</tr>
				<tr>
					<td colspan="2">
					I certify that the facts contained in this application are true and complete to the best of my knowledge and I understand that, if employed, falsified statements on this application are grounds for dismissal. 
					I authorize investigation of all statements contained herein and otherwise, and release the company from all liability for damage that may result from utilization of such information.
					In consideration of my employment, I agree to follow the rules and regulations of C.O.P.S. Monitoring, Inc. 
					I understand that my employment and compensation can be terminated with or without cause and without prior notice at any time by the company or my own option. 
					I also understand and agree that no representative of the Company has any authority to enter into any agreement for employment for any specified amount of time, or make any agreement contrary to the foregoing, unless it is in writing and signed by an authorized company representative.					</td>
				</tr>
				<tr><td colspan="2">&nbsp;</td></tr>
				<tr><td colspan="2">&nbsp;</td></tr>
				<tr>
					<td>Applicant's signature: ___________________________________________</td>
					<td>Date: ___/___/______</td>
				</tr>
			</table>
			
			<br />
			<br />

			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td colspan="2"><b>IMPORTANT INFORMATION - DRUG TESTING POLICY</b></td>
				</tr>
				<tr>
					<td colspan="2">
					In the interest of maintaining a drug-free work environment and protecting it's customers, C.O.P.S. Monitoring conducts both ongoing random and suspicious illegal drug tests on (1) All applicants being considered for employment and (2) existing employees of the company. 
					THE RESULT OF THE DRUG TEST MAY DISQUALIFY YOU FROM EMPLOYMENT WITH C.O.P.S.
					Initial employment with C.O.P.S. Monitoring is based partially on the applicant submitting a negative drug test result from a scheduled test as designated by C.O.P.S. Monitoring. 
					Anyone who fails to submit this negative test result to C.O.P.S. Monitoring will not be considered for employment. 
					If a positive result is reported at a later testing date during employment, the status of the employee's employment will be determined by the company's written drug test policy as outlined.
					</td>
				</tr>
				<tr><td colspan="2">&nbsp;</td></tr>
				<tr>
					<td colspan="2">
					<b>I have read and understand the above</b>
					</td>
				</tr>
				<tr><td colspan="2">&nbsp;</td></tr>
				<tr><td colspan="2">&nbsp;</td></tr>
				<tr>
					<td>Applicant's signature: ___________________________________________</td>
					<td>Date: ___/___/______</td>
				</tr>
			</table>
		
			<br />
			<br />

			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td colspan="2"><b>IMPORTANT INFORMATION - CRIMINAL BACKGROUND CHECK</b></td>
				</tr>
				<tr>
					<td colspan="2">
					In the interest of our customers, C.O.P.S. Monitoring has adopted the policy of conducting criminal background checks on all employees hired effective September 1, 1997.  
					All applicants or employees of C.O.P.S. Monitoring are required to sign a 212B form (see attached 212B form) required by the New Jersey State Police in order to execute our request. 
					The facts contained within the report may disqualify you from employment with our company. 
					You will be given the opportunity to complete and/or challenge the accuracy of the information contained in the Criminal History Record. 
					You shall be afforded a reasonable period of time to correct and complete this record.  
					A person is not presumed guilty of any charges or arrests for which there are no final dispositions indicated on the record.  
					The use of this information will be limited to authorized personnel only and all records will be destroyed after they have served their intended and authorized purpose. 
					Anyone who does not comply with this policy will not be considered for employment with our company or shall be terminated from employment. 
					</td>
				</tr>
				<tr><td colspan="2">&nbsp;</td></tr>
				<tr>
					<td colspan="2">
					<b>I have read and understand the above</b>
					</td>
				</tr>
				<tr><td colspan="2">&nbsp;</td></tr>
				<tr><td colspan="2">&nbsp;</td></tr>
				<tr>
					<td>Applicant's signature: ___________________________________________</td>
					<td>Date: ___/___/______</td>
				</tr>
			</table>
--->
		</td>
	</tr>
</table>


</div>

 </cfdocument> 
