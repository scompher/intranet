
<link rel="stylesheet" type="text/css" href="/styles.css">

<cfparam name="location" default="">
<cfparam name="name" default="">
<cfparam name="dateSubmitted" default="">
<cfparam name="refCode" default="">
<cfparam name="dateSubmittedLater" default="">
<!--- add search by department applied for --->
<cfparam name="departmentappliedfor" default="">

<cfif isDate(dateSubmitted)>
	<cfset dateSubmittedLater = dateadd("d",1,dateSubmitted)>
	<cfset dateSubmittedLater = dateformat(dateSubmittedLater,'mm/dd/yyyy')>
</cfif>

<cfquery name="GetApplications" datasource="copalink">
	select 
		employmentApp_main.*, 
		employmentApp_personal.*, 
		employmentApp_employmentInfo.location,
		employmentApp_employmentInfo.position 
	from employmentApp_main 
		left join employmentApp_personal on employmentApp_main.applicationID = employmentApp_personal.applicationID 
		left join employmentApp_employmentInfo on employmentApp_main.applicationID = employmentApp_employmentInfo.applicationID 
	where employmentApp_main.deleted = 0 and active = 1
	<cfif trim(location) is not "">
		and employmentApp_employmentInfo.location = '#location#' 
	</cfif>
	<cfif trim(name) is not "">
		and employmentApp_personal.lastname like '%#trim(name)#%' 
	</cfif>
	<cfif trim(dateSubmitted) is not "">
		and (dateTimeCreated >= '#dateSubmitted#' and dateTimeCreated < '#dateSubmittedLater#')
	</cfif>
	<cfif trim(refCode) is not "">
		and employmentApp_Main.referenceNum = '#trim(refCode)#'
	</cfif>
	<!--- add search by department applied for --->
	<cfif trim(departmentappliedfor) is not "">
		and employmentApp_employmentInfo.position like '%#trim(departmentappliedfor)#%'
	</cfif>
	order by employmentApp_main.dateTimeCreated desc 
</cfquery>

<br />

<div align="center" class="normal">
<table width="600" border="0" cellspacing="0" cellpadding="5">
    <tr>
        <td class="highlightbar"><b>Search Filter</b></td>
    </tr>
    <tr>
        <td class="greyrowbottomnopadding">
			<cfform method="post" action="index.cfm">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td width="20%" nowrap="nowrap"><b>Location:</b></td>
					<td width="80%">
						<select name="location">
							<option <cfif location is "">selected</cfif> value="">All</option>
							<option <cfif location is "AZ">selected</cfif> value="AZ">AZ</option>
							<option <cfif location is "NJ">selected</cfif> value="NJ">NJ</option>
							<option <cfif location is "FL">selected</cfif> value="FL">FL</option>
							<option <cfif location is "TX">selected</cfif> value="TX">TX</option>
							<option <cfif location is "MD">selected</cfif> value="MD">MD</option>
							<option <cfif location is "TN">selected</cfif> value="TN">TN</option>
						</select>
					</td>
				</tr>
				<tr>
					<td nowrap="nowrap"><b>Last Name:</b></td>
					<td>
						<cfinput type="text" name="name" style="width:300px;" value="#name#" />
					</td>
				</tr>
				<tr>
					<td nowrap="nowrap"><b>Date Submitted:</b> </td>
					<td>
						<cfinput type="datefield" name="dateSubmitted" style="width:75px;" value="#dateSubmitted#" />
					</td>
				</tr>
				<tr>
					<td nowrap="nowrap"><b>Reference Code:</b> </td>
					<td>
						<cfinput type="text" name="refCode" style="width:200px;" value="#refCode#" />
					</td>
				</tr>
				<!--- add search by department applied for --->
				<tr>
					<td nowrap="nowrap"><b>Position Applying For:</b></td>
					<td><cfinput type="text" name="departmentappliedfor" style="width:300px;" value="#departmentappliedfor#" /></td>
				</tr>
				<tr>
					<td colspan="2" nowrap="nowrap">
						<input type="submit" name="btnSearch" value="Search" />
						<input type="button" name="btnReset" value="Clear Search" onclick="document.location='index.cfm';" />
					</td>
					</tr>
			</table>
			</cfform>
		</td>
    </tr>
</table>
</div>

<br />

<div align="center" class="normal">
<table width="950" border="0" cellspacing="0" cellpadding="5">
    <tr>
        <td class="highlightbar" style="font-weight:bold;">Employment Application Management</td>
    </tr>
    <tr>
        <td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td width="75" align="center" class="linedrowrightcolumn"><b>Action</b></td>
					<td width="350" nowrap="nowrap" class="linedrowrightcolumn"><b>Name</b></td>
					<td width="100" align="center" class="linedrowrightcolumn"><b>Submitted</b></td>
					<td width="75" align="center" class="linedrowrightcolumn"><b>Location</b></td>
					<td align="center" class="linedrow"><b>Opened By</b></td>
				</tr>
				<cfoutput query="getApplications">
				<tr>
					<td width="75" align="center" class="linedrowrightcolumn">
						<a target="_blank" href="view.cfm?aid=#getApplications.applicationID#">
						<cfif getApplications.isRead is 0>
							<img src="/images/unread.gif" border="0" align="View Application" />
						<cfelse>
							<img src="/images/read.gif" border="0" align="View Application" />
						</cfif>
						</a>
						&nbsp;
						<a onclick="return confirm('Are you sure you want to delete this item?');" href="delete.cfm?aid=#getApplications.applicationID#"><img src="/images/delete.gif" border="0" alt="Delete Application" /></a>
						<!--- 
						&nbsp;
						<a href="##"><img src="/images/edit.gif" border="0" alt="Add Note" /></a>
						 --->
					</td>
					<td nowrap="nowrap" class="linedrowrightcolumn">#getApplications.firstName# #getApplications.lastName#</td>
					<td align="center" class="linedrowrightcolumn">#dateformat(getApplications.dateTimeCreated,'mm/dd/yyyy')#</td>
					<td align="center" class="linedrowrightcolumn">#getApplications.location#</td>
					<td nowrap="nowrap" align="center" class="linedrow">#getApplications.openedBy#&nbsp;</td>
				</tr>
				</cfoutput>
			</table>
		</td>
    </tr>
</table>
<br>
<a href="/index.cfm" style="text-decoration:underline;">Return to Intranet</a>
</div>
