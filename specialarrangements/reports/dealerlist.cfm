
<style type="text/css">
	TABLE {font-family:Arial, Helvetica, sans-serif;font-size:12px; background-color:#FFFFFF;}
	.normal {font-family:Arial, Helvetica, sans-serif;font-size:12px}
	.heading {font-family:Arial, Helvetica, sans-serif;font-size:16px}
	.titlebar {font-family: Arial, Helvetica, sans-serif;	font-size: 12px;font-weight: bold;color: #FFFFFF;}
	.small {font-family:Verdana, Arial, Helvetica, sans-serif;font-size:10px}
	a:link {color:#000000; text-decoration: underline;}
	a:visited {text-decoration: underline; color: #000000;}
	a:hover {text-decoration: underline; color: #0000FF;}
	a:active {text-decoration: underline; color: #000000;}
	.box {border: 1px solid #000000;}
</style>

<cfquery name="getdealers" datasource="#ds#">
	select * from specialarrangements_dealers
	where active = 1
	order by dealername asc
</cfquery>

<cfset today = dateformat(now(),'mm/dd/yyyy')>

<cfset last4start = createodbcdate(dateadd("d",-30,today))>
<cfset next4end = createodbcdate(dateadd("d",30,today))>

<div align="center">
<table width="725" border="1" cellspacing="0" cellpadding="5">
	<tr bgcolor="FFFFCC">
		<td colspan="6" align="center" nowrap><b>List of Dealers on Special Arrangements </b></td>
	</tr>
	<tr>
		<td align="center" nowrap bgcolor="DDDDDD"><b>Dealer</b></td>
		<td nowrap bgcolor="DDDDDD"><b>Payment Terms</b></td>
		<td align="center" bgcolor="DDDDDD"><b>Payments Expected<br>in Last 30 Days</b></td>
		<td align="center" bgcolor="DDDDDD"><b>Payments Made <br>in Last 30 Days</b></td>
		<td align="center" nowrap bgcolor="DDDDDD"><b>Invoice Amt</b></td>
	    <td align="center" nowrap bgcolor="DDDDDD"><b>Current Ratio</b></td>
	</tr>
	<cfloop query="getdealers">
		<cfswitch expression="#getdealers.paymentterms#">
			<cfcase value="Weekly"><cfset terms = 7></cfcase>
			<cfcase value="Bi-Monthly"><cfset terms = 15></cfcase>
			<cfcase value="Monthly"><cfset terms = 30></cfcase>
		</cfswitch>
		<cfset startdate = dateadd("d",-30,now())>
		<cfset enddate = "#dateformat(now(),'mm')#/#dateformat(getdealers.firstpaymentdue,'dd')#/#dateformat(now(),'yyyy')#">
		<cfquery name="getnext4" datasource="#ds#">
			select * from specialarrangements_payments
			where dealerid = #getdealers.dealerid# and paymentdate >= #last4start# and paymentdate <= #next4end#
		</cfquery>
		<cfoutput>
		<tr>
			<td align="left" valign="top" nowrap>Dealer ###getdealers.dealernumber# : <a href="##">#getdealers.dealername#</a></td>
			<td valign="top">#getdealers.paymentterms#</td>
			<td valign="top" nowrap>
			<cfset currdate = startdate>
			<cfloop condition="currdate LTE enddate">
				#dateformat(currdate,'mm/dd')# - #getdealers.paymentamount#<br>
				<cfset currdate = dateadd("d",terms,currdate)>
			</cfloop>
			&nbsp;
			</td>
			<td valign="top" nowrap>
			<cfset paymenttotal = 0>
			<cfloop query="getnext4">
				#dateformat(getnext4.paymentdate,'mm/dd')# - #getnext4.paymentamount#<br>
				<cfset tmp = getnext4.paymentamount>
				<cfset tmp = replace(tmp,"$","","all")>
				<cfset tmp = replace(tmp,",","","all")>
				<cfset tmp = trim(tmp)>
				<cfset paymenttotal = paymenttotal + tmp>
			</cfloop>
			&nbsp;
			</td>
			<td valign="top">#getdealers.lastinvoiceamount#</td>
			<td valign="top">
			<cfset tmp = getdealers.lastinvoiceamount>
			<cfset tmp = replace(tmp,"$","","all")>
			<cfset tmp = replace(tmp,",","","all")>
			<cfset tmp = trim(tmp)>
			#numberformat(evaluate(paymenttotal/tmp),'00')#%
			</td>
		</tr>
		</cfoutput>
	</cfloop>
</table>
<br>
<a href="index.cfm" class="normal">Return to Reports Menu</a>
</div>


