
<style type="text/css">
	TABLE {font-family:Arial, Helvetica, sans-serif;font-size:12px; background-color:#FFFFFF;}
	.normal {font-family:Arial, Helvetica, sans-serif;font-size:12px}
	.heading {font-family:Arial, Helvetica, sans-serif;font-size:16px}
	.titlebar {font-family: Arial, Helvetica, sans-serif;	font-size: 12px;font-weight: bold;color: #FFFFFF;}
	.small {font-family:Verdana, Arial, Helvetica, sans-serif;font-size:10px}
	a:link {color:#000000; text-decoration: underline;}
	a:visited {text-decoration: underline; color: #000000;}
	a:hover {text-decoration: underline; color: #0000FF;}
	a:active {text-decoration: underline; color: #000000;}
	.box {border: 1px solid #000000;}
</style>

<cfquery name="getdealers" datasource="#ds#">
	select * from specialarrangements_dealers
	where active = 1
	order by dealername asc
</cfquery>

<div align="center" class="normal">
<table width="725"  border="1" cellspacing="0" cellpadding="5">
	<tr bgcolor="dddddd">
		<td width="70" align="center" nowrap><b>Action</b></td>
		<td width="65" align="center" nowrap><b>Dealer # </b></td>
		<td width="590" align="center" nowrap><b>Dealer Name </b></td>
	</tr>
	<cfoutput query="getdealers">
	<tr>
		<td align="center">
		<table  border="0" cellspacing="0" cellpadding="5">
        	<tr align="center">
        		<td><a href="edit.cfm?id=#getdealers.dealerid#"><img src="/images/edit.gif" alt="Edit Dealer" width="16" height="16" border="0"></a></td>
        		<td><a onClick="return confirm('Are you sure you wish to delete this dealer?');" href="delete.cfm?id=#getdealers.dealerid#"><img src="/images/delete.gif" alt="Delete Dealer" width="16" height="16" border="0"></a></td>
       		</tr>
        </table>
		</td>
		<td align="center">#getdealers.dealernumber#</td>
		<td>#getdealers.dealername#</td>
	</tr>
	</cfoutput>
	<tr>
		<td><input name="button" type="submit" class="normal" value="Add New" onClick="document.location='add.cfm'"></td>
		<td colspan="2">&nbsp;</td>
	</tr>
</table>
<br>
<a href="../index.cfm">Return to Special Arrangements Menu</a>
</div>
