
<style type="text/css">
	TABLE {font-family:Arial, Helvetica, sans-serif;font-size:12px; background-color:#FFFFFF;}
	.normal {font-family:Arial, Helvetica, sans-serif;font-size:12px}
	.heading {font-family:Arial, Helvetica, sans-serif;font-size:16px}
	.titlebar {font-family: Arial, Helvetica, sans-serif;	font-size: 12px;font-weight: bold;color: #FFFFFF;}
	.small {font-family:Verdana, Arial, Helvetica, sans-serif;font-size:10px}
	a:link {color:#000000; text-decoration: underline;}
	a:visited {text-decoration: underline; color: #000000;}
	a:hover {text-decoration: underline; color: #0000FF;}
	a:active {text-decoration: underline; color: #000000;}
	.box {border: 1px solid #000000;}
</style>

<cfif isdefined("form.btnEditDealer")>

	<cfset variables.firstpaymentdue = createodbcdate(form.firstpaymentdue)>
	<cfif trim(form.lastinvoicedate) IS NOT "">
		<cfset variables.lastinvoicedate = createodbcdate(form.lastinvoicedate)>
	<cfelse>
		<cfset variables.lastinvoicedate = "NULL">
	</cfif>

	<!--- save info --->
	<cfquery name="savedealer" datasource="#ds#">
		update specialarrangements_dealers 
		set dealernumber = '#form.dealernumber#', dealername = '#form.dealername#', paymentamount = '#form.paymentamount#', paymentterms = '#form.paymentterms#', firstpaymentdue = #variables.firstpaymentdue#, lastinvoiceamount = '#form.lastinvoiceamount#', email = '#form.email#', description = '#form.description#', lastinvoicedate = #variables.lastinvoicedate#, acctsbilled = '#form.acctsbilled#'
		where dealerid = #form.dealerid#
	</cfquery>

	<cflocation url="index.cfm">

<cfelse>

	<cfquery name="getdealer" datasource="#ds#">
		select * from specialarrangements_dealers
		where dealerid = #id#
	</cfquery>

	<script language="javascript" src="../cal2.js">
	/*
	Xin's Popup calendar script-  Xin Yang (http://www.yxscripts.com/)
	Script featured on/available at http://www.dynamicdrive.com/
	This notice must stay intact for use
	*/
	</script>
	<script language="javascript" src="../cal_conf2.js"></script>

	<script language="JavaScript" type="text/JavaScript">
	function checkform(frm) {
		if (frm.dealernumber.value == "") {alert('The dealer number is required.'); frm.dealernumber.focus(); return false;}
		if (frm.dealername.value == "") {alert('The dealer name is required.'); frm.dealername.focus(); return false;}
		if (frm.paymentamount.value == "") {alert('The payment amount is required.'); frm.paymentamount.focus(); return false;}
		if (!frm.paymentterms[0].checked && !frm.paymentterms[1].checked && !frm.paymentterms[2].checked) {alert('The payment terms are required.'); return false;}
		if (frm.firstpaymentdue.value == "") {alert('The date the first payment is due is required.'); frm.firpaymentdue.focus(); return false;}
		if (frm.lastinvoiceamount.value == "") {alert('The last invoice amount is required.'); frm.lastinvoiceamount.focus(); return false;}
		if (frm.description.value == "") {alert('The description is required.'); frm.description.focus(); return false;}
		
		frm.submit();
	}
	</script>
	
	<div align="center">
	<form method="post" action="edit.cfm">
	<cfoutput>
	<input type="hidden" name="dealerid" value="#id#">
	</cfoutput>
	<table  border="1" cellspacing="0" cellpadding="5">
		<tr align="center" bgcolor="ffffcc">
			<td colspan="2"><b>Edit a Dealer</b></td>
		</tr>
		<cfoutput query="getdealer">
		<tr>
			<td width="154">Dealer Number </td>
			<td><input name="dealernumber" type="text" class="normal" maxlength="4" size="5" value="#getdealer.dealernumber#"></td>
		</tr>
		<tr>
			<td>Dealer Name </td>
			<td><input name="dealername" type="text" class="normal" maxlength="255" style="width:400px" value="#getdealer.dealername#"></td>
		</tr>
		<tr>
			<td>Payment Amount </td>
			<td><input name="paymentamount" type="text" class="normal" maxlength="25" value="#getdealer.paymentamount#"></td>
		</tr>
		<tr>
			<td>Payment Terms </td>
			<td>
			<input name="paymentterms" type="radio" <cfif trim(getdealer.paymentterms) IS "Weekly">checked</cfif> value="Weekly"> Weekly&nbsp;&nbsp;&nbsp;
			<input name="paymentterms" type="radio" <cfif trim(getdealer.paymentterms) IS "Bi-Monthly">checked</cfif> value="Bi-Monthly"> Bi-Monthly&nbsp;&nbsp;&nbsp;
			<input name="paymentterms" type="radio" <cfif trim(getdealer.paymentterms) IS "Monthly">checked</cfif> value="Monthly"> Monthly
			</td>
		</tr>
		<tr>
			<td>Date First Payment is Due </td>
			<td>
			<input name="firstpaymentdue" type="text" class="normal" maxlength="15" value="#dateformat(getdealer.firstpaymentdue,'mm/dd/yyyy')#">
			<a style="text-decoration:none;" href="javascript:showCal('EditDealerFirstPaymentDue');"><img align="absmiddle" border="0" src="/images/calicon.gif"></a>
			</td>
		</tr>
		<tr>
			<td>Last Invoice Date </td>
			<td>
			<input name="lastinvoicedate" type="text" class="normal" maxlength="15" value="#dateformat(getdealer.lastinvoicedate,'mm/dd/yyyy')#">
            <a style="text-decoration:none;" href="javascript:showCal('EditDealerLastInvoiceDate');"><img align="absmiddle" border="0" src="/images/calicon.gif"></a></td>
		</tr>
		<tr>
			<td>Last Invoice Amount </td>
			<td><input name="lastinvoiceamount" type="text" class="normal" maxlength="25" value="#getdealer.lastinvoiceamount#"></td>
		</tr>
		<tr>
			<td>Number of Accts Billed </td>
			<td><input name="acctsbilled" type="text" class="normal" maxlength="10" size="10" value="#getdealer.acctsbilled#"></td>
		</tr>
		<tr>
			<td>Dealer E-Mail Address </td>
			<td><input name="email" type="text" class="normal" maxlength="255" style="width:400px" value="#getdealer.email#"></td>
		</tr>
		<tr valign="top">
			<td>Description</td>
			<td><textarea name="description" rows="5" class="normal" style="width:400px ">#getdealer.description#</textarea></td>
		</tr>
		</cfoutput>
		<tr>
			<td colspan="2">
			<input type="hidden" name="btnEditDealer" value="1">
			<input type="button" value="Update Dealer" onClick="checkform(this.form);">
			<input type="reset" name="Reset" value="Reset Fields">
			</td>
		</tr>
	</table>
	</form>
	<a href="index.cfm" class="normal">Return to Dealer Menu</a>
	</div>

</cfif>
