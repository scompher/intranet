
<style type="text/css">
	TABLE {font-family:Arial, Helvetica, sans-serif;font-size:12px; background-color:#FFFFFF;}
	.normal {font-family:Arial, Helvetica, sans-serif;font-size:12px}
	.heading {font-family:Arial, Helvetica, sans-serif;font-size:16px}
	.titlebar {font-family: Arial, Helvetica, sans-serif;	font-size: 12px;font-weight: bold;color: #FFFFFF;}
	.small {font-family:Verdana, Arial, Helvetica, sans-serif;font-size:10px}
	a:link {color:#000000; text-decoration: underline;}
	a:visited {text-decoration: underline; color: #000000;}
	a:hover {text-decoration: underline; color: #0000FF;}
	a:active {text-decoration: underline; color: #000000;}
	.box {border: 1px solid #000000;}
</style>

<cfif isdefined("form.btnAddPayment")>

	<cfset variables.paymentdate = createodbcdate(form.paymentdate)>
	
	<!--- save payment --->
	<cfquery name="savepayment" datasource="#ds#">
		insert into specialarrangements_payments (dealerid, paymentdate, paymentamount, description)
		values (#form.dealerid#, #variables.paymentdate#, '#form.paymentamount#', '#form.description#')
	</cfquery>
	
	<cflocation url="index.cfm">

<cfelse>
	
	<script language="JavaScript" type="text/JavaScript">
	function checkform(frm) {
		if (frm.dealerid.selectedIndex == 0) {alert('The dealer is required.'); frm.dealerid.focus(); return false;}
		if (frm.paymentdate.value == "") {alert('The payment date is required'); frm.paymentdate.focus(); return false;}
		var currDate = new Date();
		var backDate = new Date(currDate.getFullYear(), currDate.getMonth(), currDate.getDate() - 15);
		var paydate = new Date(frm.paymentdate.value);
		// if (paydate < backDate || paydate > currDate) {alert('The payment date may not be greater than today or more than 15 days prior.'); return false;}
		if (frm.paymentamount.value == "$") {alert('The payment amount is required'); frm.paymentamount.focus(); return false;}
		if (frm.description.value == "") {alert('The description is required'); frm.description.focus(); return false;}
				
		frm.submit();
	}
	</script>
	
	<script language="javascript" src="../cal2.js">
	/*
	Xin's Popup calendar script-  Xin Yang (http://www.yxscripts.com/)
	Script featured on/available at http://www.dynamicdrive.com/
	This notice must stay intact for use
	*/
	</script>
	<script language="javascript" src="../cal_conf2.js"></script>
	
	<cfquery name="getdealers" datasource="#ds#">
		select * from specialarrangements_dealers
		where active = 1
		order by dealername asc
	</cfquery>
	
	<div align="center">
	<form method="post" action="add.cfm">
	<table border="1" cellspacing="0" cellpadding="5">
		<tr align="center" bgcolor="ffffcc">
			<td colspan="2"><b>Add a payment </b></td>
		</tr>
		<tr>
			<td>Dealer</td>
			<td>
			<select name="dealerid" class="normal">
				<option></option>
			<cfoutput query="getdealers">
				<option value="#getdealers.dealerid#">#getdealers.dealername#</option>
			</cfoutput>
			</select>
			</td>
		</tr>
		<tr>
			<td>Payment Date </td>
			<td><input name="paymentdate" type="text" class="normal" id="paymentdate" maxlength="15">
				<a style="text-decoration:none;" href="javascript:showCal('AddPaymentDate');"><img align="absmiddle" border="0" src="/images/calicon.gif"></a> </td>
		</tr>
		<tr>
			<td>Amount</td>
			<td><input name="paymentamount" type="text" class="normal" id="paymentamount" value="$" maxlength="15"></td>
		</tr>
		<tr valign="top">
			<td>Description</td>
			<td><textarea name="description" rows="5" class="normal" style="width:400px "></textarea></td>
		</tr>
		<tr>
			<td colspan="2">
			<input type="hidden" name="btnAddPayment" value="1">
			<input type="button" value="Add Payment" onClick="checkform(this.form);">
			<input type="reset" name="Reset" value="Clear Fields">
			</td>
		</tr>
	</table>
	</form>
	<a href="index.cfm" class="normal">Return to Payment Menu</a>
	</div>

</cfif>
