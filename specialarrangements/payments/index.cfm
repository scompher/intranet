
<style type="text/css">
	TABLE {font-family:Arial, Helvetica, sans-serif;font-size:12px; background-color:#FFFFFF;}
	.normal {font-family:Arial, Helvetica, sans-serif;font-size:12px}
	.heading {font-family:Arial, Helvetica, sans-serif;font-size:16px}
	.titlebar {font-family: Arial, Helvetica, sans-serif;	font-size: 12px;font-weight: bold;color: #FFFFFF;}
	.small {font-family:Verdana, Arial, Helvetica, sans-serif;font-size:10px}
	a:link {color:#000000; text-decoration: underline;}
	a:visited {text-decoration: underline; color: #000000;}
	a:hover {text-decoration: underline; color: #0000FF;}
	a:active {text-decoration: underline; color: #000000;}
	.box {border: 1px solid #000000;}
</style>

<cfquery name="getpayments" datasource="#ds#">
	select specialarrangements_payments.*, specialarrangements_dealers.dealername
	from specialarrangements_payments
	inner join specialarrangements_dealers on specialarrangements_payments.dealerid = specialarrangements_dealers.dealerid
	where specialarrangements_payments.active = 1
	order by paymentdate desc
</cfquery>

<div align="center" class="normal">
<table width="725" border="1" cellspacing="0" cellpadding="5">
	<tr>
		<td width="45" align="center" nowrap bgcolor="DDDDDD"><b>Action</b></td>
		<td bgcolor="DDDDDD"><b>Dealer Name</b></td>
		<td width="65" align="center" nowrap bgcolor="DDDDDD"><b>Date</b></td>
		<td width="65" align="center" nowrap bgcolor="DDDDDD"><b>Amount</b></td>
	</tr>
	<cfoutput query="getpayments">
	<tr>
		<td align="center" nowrap>
		<a href="edit.cfm?id=#getpayments.paymentid#"><img border="0" src="../../images/edit.gif" width="16" height="16" alt="Edit Payment"></a>&nbsp;
		<a onClick="return confirm('Are you sure you wish to delete this payment?');" href="delete.cfm?id=#getpayments.paymentid#"><img border="0" src="../../images/delete.gif" width="16" height="16" alt="Delete Payment"></a>
		</td>
		<td>#getpayments.dealername#</td>
		<td align="center" nowrap>#dateformat(getpayments.paymentdate,'mm/dd/yyyy')#</td>
		<td align="right" nowrap>#getpayments.paymentamount#</td>
	</tr>
	</cfoutput>
	<tr>
		<td colspan="4"><input type="button" onClick="document.location='add.cfm';" value="New Payment"></td>
	</tr>
</table>
<br>
<a href="../index.cfm">Return to Special Arrangements Menu</a>
</div>
