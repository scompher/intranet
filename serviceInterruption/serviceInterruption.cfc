<cfcomponent>
	<!--- Add New Incident Data --->
	<cffunction name="addIntDetails" access="public" returntype="string">
		<cfargument name="ds" type="string" required="yes">
		<cfargument name="interruptionDate" type="string" required="yes">
		<cfargument name="interruptionTime" type="string" required="yes">
		<cfargument name="interruptionSiteName" type="string" required="yes">
		<cfargument name="interruptionEmpname" type="string" required="yes">
		<cfargument name="interruptionDept" type="string" required="yes">
		<cfargument name="interruptionDetails" type="string" required="yes">

		<cfset interruptionDateTimeScr = createodbcdatetime(interruptionDate & " " & interruptionTime)>
		<cfset dateTimeCreated = createodbcdatetime(now())>
		
		<cfquery name="saveInterruptionDetails" datasource="#ds#">
			INSERT INTO serviceInterruption_main(interruptionDateTime,interruptionSiteName,interruptionEmpname,interruptionDept,interruptionDetails,dateTimeCreated)
				VALUES (#interruptionDateTimeScr#,
					'#interruptionSiteName#',
					'#interruptionEmpname#',
					'#interruptionDept#',
					'#interruptionDetails#',
					#dateTimeCreated#)
		</cfquery>
		<cfquery name="GetId" datasource="#ds#" >
				SELECT MAX(incidentid) as incidentid FROM serviceInterruption_main
				WHERE interruptionSiteName = '#interruptionSiteName#' and
					  interruptionEmpname = '#interruptionEmpname#' and
					  interruptionDept = '#interruptionDept#' and
					  dateTimeCreated = #dateTimeCreated#
		</cfquery>
		<cfset holdIncidentid = GetId.incidentid>
	<cfreturn holdIncidentid>
	</cffunction>
	
	<!--- Add Action Taken Data --->
	<cffunction name="addNotificationDetails" access="public" returntype="boolean">
		<cfargument name="ds" type="string" required="yes">
		<cfargument name="incidentid" type="string" required="yes">
		<cfargument name="actionDate" type="string" required="yes">
		<cfargument name="actionTime" type="string" required="yes">
		<cfargument name="actionSiteName" type="string" required="yes">
		<cfargument name="actionEmployeeNotified" type="string" required="yes">
		<cfargument name="actionDepartment" type="string" required="yes">
		<cfargument name="methodOfNotification" type="string" required="yes">
		
		<cfset actionDateTimeScr = createodbcdatetime(actionDate & " " & actionTime)>
		
		<cfquery name="saveNotificationDetails" datasource="#ds#">
			UPDATE serviceInterruption_main
			SET actionDateTime = #actionDateTimeScr#,
				actionSiteName = '#actionSiteName#',
				actionEmployeeNotified = '#actionEmployeeNotified#',
				actionDepartment = '#actionDepartment#',
				methodOfNotification = '#methodOfNotification#'
			WHERE incidentid = #incidentid#
		</cfquery>
	
		<cfreturn true>
	</cffunction>

	<!--- Add New Incident Technology Data --->
	<cffunction name="addTechDetails" access="public" returntype="boolean">
		<cfargument name="ds" type="string" required="yes">
		<cfargument name="incidentid" type="string" required="yes">
		<cfargument name="techPriority" type="string" required="yes">
		<cfargument name="techAffectedSubscriber" type="string" required="yes">
		<cfargument name="techAffectedDatabase" type="string" required="yes">
		<cfargument name="techIsolationOfCause" type="string" required="yes">
		<cfargument name="techServiceRestored" type="string" required="yes">
		<cfargument name="techDateRestored" type="string" required="yes">
		<cfargument name="techTimeRestored" type="string" required="yes">
		<cfargument name="RCAICAFollowUp" type="string" required="yes">
		<cfargument name="techAdditionalNotifications" type="string" required="yes">
		
		<cfif trim(techDateRestored) is not "" and trim(techTimeRestored) is not "">
			<cfset techDateTimeRestored = createodbcdatetime(techDateRestored & " " & techTimeRestored)>
		<cfelse>
			<cfset techDateTimeRestored = "">
		</cfif>
		
		<cfquery name="saveTechInfo" datasource="#ds#">
			UPDATE serviceInterruption_main 
			SET techPriority = '#techPriority#',
				techAffectedSubscriber = '#techAffectedSubscriber#',
				techAffectedDatabase = '#techAffectedDatabase#',
				techIsolationOfCause = '#techIsolationOfCause#',
				techServiceRestored = '#techServiceRestored#',
				<cfif trim(techDateTimeRestored) is not "">
				techDateTimeRestored = #techDateTimeRestored#,
				</cfif>
				RCAICAFollowUp = #RCAICAFollowUp#,
				techAdditionalNotifications = '#techAdditionalNotifications#'
			WHERE incidentid = #incidentid#
		</cfquery>
	
		<cfreturn true>
	</cffunction>

	<!--- Add Escalation Notification Data --->
	<cffunction name="addEscalation" access="public" returntype="boolean">
		<cfargument name="ds" type="string" required="yes">
		<cfargument name="incidentid" type="string" required="yes">
		<cfargument name="notifyLogNotify" type="string" required="yes">
		<cfargument name="notifyLogEmpname" type="string" required="yes">
		<cfargument name="notifyLogDateNotified" type="string" required="yes">
		<cfargument name="notifyLogTimeNotified" type="string" required="yes">
		<cfargument name="notifyLogAppdealer" type="string" required="yes">
		<cfargument name="notifyLogDealerNotified" type="string" required="yes">
		<cfargument name="notifyLogDealerDate" type="string" required="yes">
		<cfargument name="notifyLogDealerTime" type="string" required="yes">
		 
		<cfset notifyLogDateTimeScr = createodbcdatetime(notifyLogDateNotified & " " & notifyLogTimeNotified)>
		<cfif trim(notifyLogDealerDate) is not "" and trim(notifyLogDealerTime) is not "">
			<cfset notifyLogDealerDateTimeScr = createodbcdatetime(notifyLogDealerDate & " " & notifyLogDealerTime)>
		<cfelse>
			<cfset notifyLogDealerDateTimeScr = "">
		</cfif>
		
		<cfquery name="saveEscalationLog" datasource="#ds#">
			UPDATE serviceInterruption_main
			SET notifyLogDateTimeNotified = #notifyLogDateTimeScr#,
				notifyLogNotify = '#notifyLogNotify#',
				notifyLogEmpname = '#notifyLogEmpname#',
				notifyLogAppdealer = '#notifyLogAppdealer#', 
				<cfif trim(notifyLogDealerDateTimeScr) is not "">
				notifyLogDealerDateTime = #notifyLogDealerDateTimeScr#, 
				</cfif>
		 		notifyLogDealerNotified = '#notifyLogDealerNotified#'
			WHERE incidentid = #incidentid#
		</cfquery>
	
		<cfreturn true>
	</cffunction>
	
	<!--- Additional Escalation Notification Data --->

	<cffunction name="addMoreEscalation" access="public" returntype="string">
	
		<cfargument name="ds" type="string" required="yes">
		<cfargument name="incidentid" type="string" required="yes">
		<cfargument name="AdditionalNotifyDate" type="string" required="yes">
		<cfargument name="AdditionalNotifyTime" type="string" required="yes">
		<cfargument name="AdditionalNotifyEmpname" type="string" required="yes">
		<cfargument name="AdditionalNotifyDealer" type="string" required="yes">
		 
		<cfset additionalNotifyDateTimeScr = createodbcdatetime(AdditionalNotifyDate & " " & AdditionalNotifyTime)>
		<cfset dateTimeCreatedAddNotify = createodbcdatetime(now())>
		
		<cfquery name="saveNotification" datasource="#ds#">
		INSERT INTO serviceInterruption_notifications(AdditionalNotifyDateTime,AdditionalNotifyEmpname,AdditionalNotifyDealer,incidentid)
				VALUES (#additionalNotifyDateTimeScr#,
					'#AdditionalNotifyEmpname#',
					'#AdditionalNotifyDealer#',
					'#incidentid#')
		</cfquery>
		<cfreturn true>
	</cffunction>

	<cffunction name="sendTechEmailNotification" access="public" returntype="string">
		<cfargument name="techName" required="yes" type="string">
		<cfargument name="ds" required="yes" type="string">
		<cfargument name="incidentid" required="yes" type="string">
		
		<cfset firstname = trim(listgetat(techName,1," "))>
		<cfset lastname = trim(listgetat(techName,2," "))>
		<cfquery name="getEmail" datasource="#ds#">
			select * from admin_users where firstname = '#firstname#' and lastname = '#lastname#'
		</cfquery>
	
<cfmail from="system.info@copsmonitoring.com" to="#getEmail.email#" subject="Tech Notification Service Interruption" username="system" password="V01c3">
#firstname# #lastname# this is to notify you of a service interruption that needs to be completed by you:

You can view the details here: http://192.168.107.10/serviceInterruption/viewDetail.cfm?incidentid=#incidentid#

</cfmail>
	
		<cfreturn "">
	</cffunction>
		
</cfcomponent>