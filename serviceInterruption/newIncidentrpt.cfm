<cfparam name="form.interruptionDate" default="">
<cfparam name="form.interruptionTimeHH" default="">
<cfparam name="form.interruptionTimeMM" default="">
<cfparam name="form.interruptionTimeTT" default="">
<cfparam name="form.interruptionSiteName" default="New Jersey">
<cfparam name="form.interruptionEmpname" default="">
<cfparam name="form.interruptionDept" default="">
<cfparam name="form.interruptionDetails" default="">
<cfparam name="form.actionDate" default="">
<cfparam name="form.actionTimeHH" default="">
<cfparam name="form.actionTimeMM" default="">
<cfparam name="form.actionTimeTT" default="">
<cfparam name="form.actionSiteName" default="New Jersey">
<cfparam name="form.actionEmployeeNotified" default="">
<cfparam name="form.actionDepartment" default="">
<cfparam name="form.methodOfNotification" default="">
<cfparam name="incidentid" default="0">

<!--- when save info button pressed --->
<cfif isDefined("form.btnSave")>
	<cfif trim(form.interruptionTimeHH) is not "" and trim(form.interruptionTimeMM) is not "">
		<cfset interruptionTime = "#form.interruptionTimeHH#:#form.interruptionTimeMM# #form.interruptionTimeTT#">
	<cfelse>
		<cfset interruptionTime = "">
	</cfif>
	<!--- call save info component method --->
	<cfinvoke 
		component="serviceInterruption.serviceInterruption"
		method="addIntDetails"
		returnvariable="incidentid">
		<cfinvokeargument name="ds" value="#ds#"/>
		<cfinvokeargument name="interruptionDate" value="#interruptionDate#"/>
		<cfinvokeargument name="interruptionTime" value="#interruptionTime#"/>
		<cfinvokeargument name="interruptionSiteName" value="#interruptionSiteName#"/>
		<cfinvokeargument name="interruptionEmpname" value="#interruptionEmpname#"/>
		<cfinvokeargument name="interruptionDept" value="#interruptionDept#"/>
		<cfinvokeargument name="interruptionDetails" value="#interruptionDetails#"/>
	</cfinvoke>	
	<cflocation url="newIncidentrpt.cfm?incidentid=#incidentid#">
</cfif>

<!--- when save action button pressed --->
<cfif isDefined("form.btnSaveAction")>
	<cfif trim(form.actionTimeHH) is not "" and trim(form.actionTimeMM) is not "">
		<cfset actionTime = "#form.actionTimeHH#:#form.actionTimeMM# #form.actionTimeTT#">
	<cfelse>
		<cfset actionTime = "">
	</cfif>
	<cfinvoke 
		 component="serviceInterruption.serviceInterruption"
		 method="addNotificationDetails"
		 returnvariable="SuccessfulAdd">
		 <cfinvokeargument name="ds" value="#ds#"/>
		 <cfinvokeargument name="incidentid" value="#incidentid#"/>
		 <cfinvokeargument name="actionDate" value="#actionDate#"/>
		 <cfinvokeargument name="actionTime" value="#actionTime#"/>
		 <cfinvokeargument name="actionSiteName" value="#actionSiteName#"/>
		 <cfinvokeargument name="actionEmployeeNotified" value="#actionEmployeeNotified#"/>
		 <cfinvokeargument name="actionDepartment" value="#actionDepartment#"/>
		 <cfinvokeargument name="methodOfNotification" value="#methodOfNotification#"/>
	</cfinvoke>	
	
	<!--- send email notification out --->
	<cfinvoke 
		component="serviceInterruption.serviceInterruption"
		method="sendTechEmailNotification"
		returnvariable="resultOfSend">
		<cfinvokeargument name="techName" value="#actionEmployeeNotified#"/>
		<cfinvokeargument name="ds" value="#ds#"/>
		<cfinvokeargument name="incidentid" value="#incidentid#"/>
	</cfinvoke>

	<cflocation url="index.cfm">
</cfif>

<!---if Reset button is entered, erase data --->
<cfif isDefined("form.btnReset")>
	<cfset form.interruptionDate = "">
	<cfset form.interruptionTimeHH = "">
	<cfset form.interruptionTimeMM = "">
	<cfset form.interruptionTimeTT = "">
	<cfset form.interruptionSiteName = "">
	<cfset form.interruptionEmpname = "">
	<cfset form.interruptionDept = "">
	<cfset form.interruptionDetails = "">
</cfif>

<link rel="stylesheet" type="text/css" href="../styles.css">

<br />

<cfif trim(incidentid) is not "">
	<cfquery name="getInfo" datasource="#ds#">
		select * from serviceInterruption_main
		where incidentid = #incidentid# 
	</cfquery>
	<cfif getInfo.recordcount gt 0>
		<cfset form.interruptionDate = dateformat(getinfo.interruptionDateTime,'mm/dd/yyyy')>
		<cfset form.interruptionTimeHH = timeformat(getinfo.interruptionDateTime,'HH')>
		<cfset form.interruptionTimeMM = timeformat(getinfo.interruptionDateTime,'MM')>
		<cfset form.interruptionTimeTT = timeformat(getinfo.interruptionDateTime,'TT')>
		<cfset form.interruptionSiteName = getinfo.interruptionSiteName>
		<cfset form.interruptionEmpname = getinfo.interruptionEmpname>
		<cfset form.interruptionDept = getinfo.interruptionDept>
		<cfset form.interruptionDetails = getinfo.interruptionDetails>
		<cfset form.actionDate = dateformat(getinfo.actionDateTime,'mm/dd/yyyy')>
		<cfset form.actionTimeHH = timeformat(getinfo.actionDateTime,'HH')>
		<cfset form.actionTimeMM = timeformat(getinfo.actionDateTime,'MM')>
		<cfset form.actionTimeTT = timeformat(getinfo.actionDateTime,'TT')>
		<cfset form.actionSiteName = getinfo.actionSiteName>
		<cfset form.actionEmployeeNotified = getinfo.actionEmployeeNotified>
		<cfset form.actionDepartment = getinfo.actionDepartment>
		<cfset form.methodOfNotification = getinfo.methodOfNotification>
	</cfif>
</cfif>

<cfquery name="getDepartments" datasource="#ds#">
	select * from admin_users_departments 
	order by department asc 
</cfquery>

<cfquery name="getEmployees" datasource="#ds#">
	select adminuserid, firstname + ' ' + lastname as employeeName 
	from admin_users 
	where active = 1
	order by firstname, lastname 
</cfquery>

<div align="center" class="normal">
<cfform method="post" name="mainform" action="newIncidentrpt.cfm">
<cfinput type="hidden" name="incidentid" value="#incidentid#" />
    <table border="0" cellspacing="0" cellpadding="5">
        <tr>
            <td class="highlightbar"><b>Service Interruption Report - New Incident</b></td>
        </tr>
        <tr>
            <td class="greyrowbottomnopadding">
                <table width="100%" border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td class="alert"><b>IDENTIFIER OF SERVICE INTERRUPTION TO PROVIDE THE FOLLOWING INFORMATION:</b> </td>
					</tr>
                    <tr>
                        <td class="nopadding">
                            <table border="0" cellspacing="0" cellpadding="5">
                                <tr>
                                    <td><b>Date:</b></td>
                                    <td>
										<cfinput type="datefield" name="interruptionDate" style="width:75px" value="#form.interruptionDate#" required="yes" message="Date is required.">       
             						</td>
                                    <td><b>Time:</b></td>
                                    <td>
                                        <cfinput type="text" name="interruptionTimeHH" style="width:20px; vertical-align:middle;" value="#form.interruptionTimeHH#" required="yes" message="Hour is required."> : 
										<cfinput type="text" name="interruptionTimeMM" style="width:20px; vertical-align:middle;" value="#form.interruptionTimeMM#" required="yes" message="Minutes are required.">
										<cfselect style="vertical-align:middle;" name="interruptionTimeTT" required="yes" message="AM or PM is required.">
											<option <cfif ucase(form.interruptionTimeTT) is "AM"> selected </cfif> value="AM">AM</option>
											<option <cfif ucase(form.interruptionTimeTT) is "PM"> selected </cfif> value="PM">PM</option>
										</cfselect>
                                    </td>
                                    <td><b>Site:</b></td>
                                    <td>
                                        <cfselect name="interruptionSiteName" required="yes" message="Site is required.">	
                                            <option <cfif ucase(form.interruptionSiteName) is "NJ"> selected </cfif> value="NJ">New Jersey</option>
                                            <option <cfif ucase(form.interruptionSiteName) is "FL"> selected </cfif> value="FL">Florida</option>
                                            <option <cfif ucase(form.interruptionSiteName) is "AZ"> selected </cfif> value="AZ">Arizona</option>
                                            <option <cfif ucase(form.interruptionSiteName) is "TN"> selected </cfif> value="TN">Tennessee</option>
                                        </cfselect>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="nopadding">
                            <table border="0" cellspacing="0" cellpadding="5">
                                <tr>
                                    <td><b>Employee Name:</b></td>
								   <td>
                                        <!--- <cfinput type="text" name="interruptionEmpName" style="width:175px" value="#form.interruptionEmpname#" required="yes" message="Employee Name is required."> --->
										<select  name="interruptionEmpName">
											<option value=""></option>
											<cfoutput query="getEmployees">
												<option <cfif getEmployees.employeeName is form.interruptionEmpname>selected</cfif> value="#employeeName#">#employeeName#</option>
											</cfoutput>
										</select>
                                    </td>
                                    <td><b>Department:</b></td>
                                    <td>
										<!--- <cfselect name="interruptionDept" query="getDepartments" value="department" display="department" required="yes" message="Department is required." selected="#form.interruptionDept#"></cfselect> --->
										<select name="interruptionDept">
											<option value=""></option>
											<cfoutput query="getDepartments">
												<option <cfif getDepartments.department is form.interruptionDept>selected</cfif> value="#department#">#department#</option>
											</cfoutput>
										</select>
                                        <!--- <cfinput type="text" name="interruptionDept" style="width:175px" value="#form.interruptionDept#" required="yes" message="Department is required."> --->
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="alert"><b>Service has been interrupted due to the following reason: </b></td>
                    </tr>
                    
                    <tr>
                    	<td>
                    		<cftextarea style="width:600px;" rows="5" name="interruptionDetails" required="yes" message="Reason is required."><cfoutput>#form.interruptionDetails#</cfoutput></cftextarea>
                    	</td>
                   	</tr>
					<cfif incidentid is 0>
						<tr>
							<td>
							<cfinput type="submit" name="btnSave" id="Saveinfo" value="Save Info"> &nbsp;
							<cfinput name="btnReset" type="reset" value="Clear form" />
							</td>
						</tr>
					</cfif>
                </table>	
            </td>
        </tr>
    </table>
    <br>
	<cfif incidentid is not 0>
		<table width="610" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td class="highlightbar"><b>Action Taken</b></td>
			</tr>
			<tr>
				<td class="greyrowbottomnopadding">
					<table width="100%" border="0" cellspacing="0" cellpadding="5">
						<tr>
							<td class="nopadding">
								<table border="0" cellspacing="0" cellpadding="5">
									<tr>
										<td><b>Date:</b></td>
										<td>
											<cfinput type="datefield" name="actionDate" style="width:75px" value="#form.actionDate#" required="yes" message="Date is required.">
										</td>
										<td><b>Time:</b></td>
										<td>
											<cfinput type="text" name="actionTimeHH" style="width:20px; vertical-align:middle;" value="#form.actionTimeHH#" required="yes" message="Hour is required."> : 
											<cfinput type="text" name="actionTimeMM" style="width:20px; vertical-align:middle;" value="#form.actionTimeMM#" required="yes" message="Minutes are required.">
											<cfselect name="actionTimeTT" style="vertical-align:middle;" required="yes" message="AM or PM is required."> 
												<option <cfif form.actionTimeTT is "AM">selected</cfif> value="AM">AM</option>
												<option <cfif form.actionTimeTT is "PM">selected</cfif> value="PM">PM</option>
											</cfselect>
										</td>
										<td><b>Site:</b></td>
										<td>
											<cfselect name="actionSiteName" id="actionSiteName" required="yes" message="Site is required.">
												<option <cfif form.actionSiteName is "NJ"> selected </cfif> value="NJ">New Jersey</option>
												<option <cfif form.actionSiteName is "FL"> selected </cfif> value="FL">Florida</option>
												<option <cfif form.actionSiteName is "AZ"> selected </cfif> value="AZ">Arizona</option>
												<option <cfif form.actionSiteName is "TN"> selected </cfif> value="TN">Tennessee</option>
											</cfselect>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td class="nopadding">
								<table border="0" cellpadding="5" cellspacing="0">
									<tr>
										<td><b>Employee Notified:</b></td>
										<td>
											<!--- <cfinput type="text" name="actionEmployeeNotified" style="width:175px" value="#form.actionEmployeeNotified#" required="yes" message="Employee Notified is required."> --->
											<select  name="actionEmployeeNotified">
												<option value=""></option>
												<cfoutput query="getEmployees">
													<option <cfif getEmployees.employeeName is form.actionEmployeeNotified>selected</cfif> value="#employeeName#">#employeeName#</option>
												</cfoutput>
											</select>
										</td>
										<td><b>Department:</b></td>
										<td>
											<!--- <cfinput type="text" name="actionDepartment" style="width:175px" value="#form.actionDepartment#" required="yes" message="Department Notified is required."> --->
											<select name="actionDepartment">
												<option value=""></option>
												<cfoutput query="getDepartments">
													<option <cfif getDepartments.department is form.actionDepartment>selected</cfif> value="#department#">#department#</option>
												</cfoutput>
											</select>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td class="nopadding">
								<table border="0" cellpadding="5" cellspacing="0">
									<tr>
										<td nowrap="nowrap"><b>Method of Notification:</b></td>
										<td>
											<cfif listfindnocase(form.methodOfNotification, "employeeOnSite") is not 0>
												<cfinput style="vertical-align:middle" type="checkbox" checked name="methodOfNotification" id="methodOfNotification" value="employeeOnSite" required="yes" message="Method of Notification is required."/>Employee On Site<br />
											<cfelse>
												<cfinput style="vertical-align:middle" type="checkbox" name="methodOfNotification" id="methodOfNotification" value="employeeOnSite" required="yes" message="Method of Notification is required."/>Employee On Site<br />
											</cfif>
										</td>
										<td>
											<cfif listfindnocase(form.methodOfNotification, "phone") is not 0>
												<cfinput style="vertical-align:middle" type="checkbox" checked name="methodOfNotification" id="methodOfNotification" value="phone" required="yes" message="Method of Notification is required." />Phone<br /> 
											<cfelse>
												<cfinput style="vertical-align:middle" type="checkbox" name="methodOfNotification" id="methodOfNotification" value="phone" required="yes" message="Method of Notification is required."/>Phone<br /> 
											</cfif>
										</td>
										<td>
											<cfif listfindnocase(form.methodOfNotification, "email") is not 0> 
												<cfinput style="vertical-align:middle" type="checkbox" checked name="methodOfNotification" id="methodOfNotification" value="email" required="yes" message="Method of Notification is required."/>Email
											<cfelse>
												<cfinput style="vertical-align:middle" type="checkbox" name="methodOfNotification" id="methodOfNotification" value="email" required="yes" message="Method of Notification is required."/>Email
											</cfif>	
										</td>	
									</tr>
								</table>
							</td>
						</tr>
						<tr>
						<td>
							<cfinput type="submit" name="btnSaveAction" id="SaveAction" value="Save Action">&nbsp;
						</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</cfif>
	
</cfform>

<br>
<a style="text-decoration:underline;" href="index.cfm">Return to Incident List</a>
<br />
</div>