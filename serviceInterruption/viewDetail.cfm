
<link rel="stylesheet" type="text/css" href="/styles.css">

<br />

<cfquery name="getInterruptionDetails" datasource="#ds#">
	select * from serviceInterruption_main 
	where incidentid = #incidentid# 
</cfquery>

<cfquery name="getAdditionalNotifications" datasource="#ds#">
	select * from serviceInterruption_notifications 
	where incidentid = #incidentid# 
	order by notificationID asc 
</cfquery>

<div align="center" class="normal">

	<cfoutput query="getInterruptionDetails">
    <table width="610" border="0" cellspacing="0" cellpadding="5">
        <tr>
            <td class="highlightbar"><b>Service Interruption Report - New Incident</b></td>
        </tr>
        <tr>
            <td class="greyrowbottomnopadding">
                <table width="100%" border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td class="alert"><b>IDENTIFIER OF SERVICE INTERRUPTION TO PROVIDE THE FOLLOWING INFORMATION:</b> </td>
					</tr>
                    <tr>
                        <td class="nopadding">
                            <table border="0" cellspacing="0" cellpadding="5">
                                <tr>
                                    <td><b>Date:</b></td>
                                    <td>
										#dateformat(getInterruptionDetails.interruptionDateTime,'mm/dd/yyyy')#
             						</td>
                                    <td><b>Time:</b></td>
                                    <td>
										#timeformat(getInterruptionDetails.interruptionDateTime,'hh:mm tt')#
                                    </td>
                                    <td><b>Site:</b></td>
                                    <td>
										#getInterruptionDetails.interruptionSiteName#
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="nopadding">
                            <table border="0" cellspacing="0" cellpadding="5">
                                <tr>
                                    <td><b>Employee Name:</b></td>
								   <td>
								   		#getInterruptionDetails.interruptionEmpName#
                                    </td>
                                    <td><b>Department:</b></td>
                                    <td>
                                        #getInterruptionDetails.interruptionDept# 
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="alert"><b>Service has been interrupted due to the following reason: </b></td>
                    </tr>
                    
                    <tr>
                    	<td>
                    		#replace(getInterruptionDetails.interruptionDetails, "#chr(13)#", "<br />", "all")#
                    	</td>
                   	</tr>
                </table>	
            </td>
        </tr>
    </table>
    <br>
	<table width="610" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td class="highlightbar"><b>Action Taken</b></td>
		</tr>
		<tr>
			<td class="greyrowbottomnopadding">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td class="nopadding">
							<table border="0" cellspacing="0" cellpadding="5">
								<tr>
									<td><b>Date:</b></td>
									<td>
										#dateformat(getInterruptionDetails.actionDateTime,'mm/dd/yyyy')#
									</td>
									<td><b>Time:</b></td>
									<td>
										#timeformat(getInterruptionDetails.actionDateTime,'hh:mm tt')#
									</td>
									<td><b>Site:</b></td>
									<td>
										#getInterruptionDetails.actionSiteName#
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td class="nopadding">
							<table border="0" cellpadding="5" cellspacing="0">
								<tr>
									<td><b>Employee Notified:</b></td>
									<td>
										#getInterruptionDetails.actionEmployeeNotified#
									</td>
									<td><b>Department:</b></td>
									<td>
										#getInterruptionDetails.actionDepartment#
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td class="nopadding">
							<table border="0" cellpadding="5" cellspacing="0">
								<tr>
									<td nowrap="nowrap"><b>Method of Notification:</b></td>
									<td>
										#getInterruptionDetails.methodOfNotification#
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	
	<br />
	
	<table width="610" border="0" cellspacing="0" cellpadding="5">
		<tr>
		<td class="highlightbar"><b>Technology to Provide Following Information:</b></td>
		</tr>
		<tr>
		<td class="greyrowbottomnopadding">
		<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr>
			<td><b>Priority:</b></td>
			</tr>
			<tr>
			<td class="nopadding">
			<table border="0" cellspacing="0" cellpadding="5">
				<tr>
				<td>
					#getInterruptionDetails.techPriority#
				</td>
				</tr>
			</table>
			</td>
			</tr>
		</table>
		</td>
		</tr>
	
		<tr>
		<td class="greyrowbottomnopadding">
		<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr>
			<td><b>Affected Subscribers:</b></td>
			</tr>
			<tr>
			<td class="nopadding">
			<table border="0" cellspacing="0" cellpadding="5">
				<tr>
				<td>
				#getInterruptionDetails.techAffectedSubscriber#
				</td>
				</tr>
			</table>
			</td>
			</tr>
	
			<tr>
			<td><span class="alert"><b>Please define, if possible the affected data base below: </b></span></td>
			</tr>
			<tr>
			<td>
			#getInterruptionDetails.techAffectedDatabase#
			</td>
			</tr>
			<tr>
			<td>
				<b>Isolation of Cause: </b>&nbsp;#getInterruptionDetails.techIsolationOfCause#
			</td>
			</tr>
		</table>
		</td>
		</tr>
		<cfif getInterruptionDetails.techServiceRestored is 1>
		<tr>
		<td class="greyrowbottomnopadding">
		<table border="0" cellspacing="0" cellpadding="5">
			<tr>
			<td nowrap="nowrap">
			<cfif getInterruptionDetails.techServiceRestored is 1>
				Service Fully Restored on #dateformat(techDateTimeRestored,'mm/dd/yyyy')# #timeformat(techDateTimeRestored,'hh:mm tt')#
			</cfif>
			</td>
			</tr>	
		</table>
		</td>
		</tr>
		</cfif>
		<cfif getInterruptionDetails.RCAICAFollowUp is 1>
		<tr>
		<td class="greyrowbottomnopadding">
		<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr>
			<td width="4%">
				<cfif getInterruptionDetails.RCAICAFollowUp is 1>RCA/ICA Follow Up</cfif>
			</td>
			</tr>
		</table>
		</td>
		</tr>
		</cfif>
	</table>
	<br />
	
    <table width="610" border="0" cellspacing="0" cellpadding="5">
        <tr>
            <td width="100%" class="highlightbar"><b>Escalation Notification Log for Service Interruption</b></td>
        </tr>
        <tr>
            <td class="greyrowbottomnopadding">
                <table width="100%" border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td width="86%" class="alert"><b>INITIAL NOTIFICATION:</b> </td>
					</tr>
					<tr>
					 <td class="nopadding">
                       <table border="0" cellspacing="0" cellpadding="5">
					   <cfif getInterruptionDetails.notifyLogNotify is 1>
					   <tr>
						<td>
							<b>COPS Employee Notified:</b>	
						</td>
						<td>
							#getInterruptionDetails.notifyLogEmpname#
						<td><b>Date:</b></td>
						<td>
							#dateformat(getInterruptionDetails.notifyLogDateTimeNotified,'mm/dd/yyyy')#
						<td><b>Time:</b></td>
						<td>
							#timeformat(getInterruptionDetails.notifyLogDateTimeNotified,'hh:mm tt')#
						</td>
						</tr>
						</cfif>
					<tr>
					<td colspan="3">
						<cfif getInterruptionDetails.notifyLogAppdealer is 1>
							<b>Applicable Dealers Notified:</b>	
						</cfif>
					</td>
					</tr>
					<tr>
					<td> <b>Dealer/Employee Notified:</b></td>
					<td>
						#getInterruptionDetails.notifyLogDealerNotified#
					</td>
					<td><b>Date:</b></td>	
					<td>
						#dateformat(getInterruptionDetails.notifyLogDealerDateTime,'mm/dd/yyyy')#
					</td>
					<td><b>Time:</b></td>
					<td>
						#timeformat(getInterruptionDetails.notifyLogDealerDateTime,'hh:mm tt')#
					</td>
					</tr>
					</table>		
					</td>
					</tr>
				</table>
		</table>
	</cfoutput>
	
	<br />
	
	<cfoutput query="getAdditionalNotifications">
	   <table width="610" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td width="100%" class="highlightbar"><b>Additional Notification</b></td>
			</tr>
			<tr>
				<td class="greyrowbottomnopadding">
					<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<tr>
						 <td class="nopadding">
							<table border="0" cellspacing="0" cellpadding="5">
								<tr>
									<td align="right"><b>Date:</b></td>
									<td>
									#dateformat(getAdditionalNotifications.AdditionalNotifyDateTime,'mm/dd/yyyy')#
									</td>
									<td><b>Time:</b></td>
									<td>
									#timeformat(getAdditionalNotifications.AdditionalNotifyDateTime,'hh:mm tt')#
									</td>
								</tr>
								<tr>
									<td><b>COPS Employee Notified:</b></td>
									<td colspan="3">#getAdditionalNotifications.AdditionalNotifyEmpname#</td>
								</tr>
								<tr>
									<td><b>Dealer/Employee Notified:</b></td>
									<td colspan="3">
									#getAdditionalNotifications.AdditionalNotifyDealer#
									</td>	
								</tr>
							</table>
						</td>
					</tr>
				</table>
				</td></tr>
		</table>
		<br />
	</cfoutput>

<br />

<a href="index.cfm" style="text-decoration:underline;">Return to Incident List</a>
</div>