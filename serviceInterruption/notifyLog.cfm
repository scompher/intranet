<cfparam name="form.notifyLogNotify" default="">
<cfparam name="form.notifyLogEmpname" default="">
<cfparam name="form.notifyLogDateNotified" default="">
<cfparam name="form.notifyLogTimeNotifiedHH" default="">
<cfparam name="form.notifyLogTimeNotifiedMM" default="">
<cfparam name="form.notifyLogTimeNotifiedTT" default="">
<cfparam name="form.notifyLogAppdealer" default="0">
<cfparam name="form.notifyLogDealerNotified" default="">
<cfparam name="form.notifyLogDealerDate" default="">
<cfparam name="form.notifyLogDealerTimeHH" default="">
<cfparam name="form.notifyLogDealerTimeMM" default="">
<cfparam name="form.notifyLogDealerTimeTT" default="">
<cfparam name="form.additionalNotifyDate" default="">
<cfparam name="form.additionalNotifyTimeHH" default="">
<cfparam name="form.additionalNotifyTimeMM" default="">
<cfparam name="form.additionalNotifyTimeTT" default="">
<cfparam name="form.additionalNotifyEmpname" default="">
<cfparam name="form.additionalNotifyDealer" default="">

<!--- when Additional Notification button pressed --->
<cfif isDefined("form.btnSaveEscalation") or isDefined("form.btnSaveEscalationAddMore")>

	<cfif trim(form.notifyLogTimeNotifiedHH) is not "" and trim(form.notifyLogTimeNotifiedMM) is not "">
		<cfset notifyLogTimeNotified = "#notifyLogTimeNotifiedHH#:#notifyLogTimeNotifiedMM# #notifyLogTimeNotifiedTT#">
	<cfelse>
		<cfset notifyLogTimeNotified = "">
	</cfif>
	<cfif trim(form.notifyLogDealerTimeHH) is not "" and trim(form.notifyLogDealerTimeMM) is not "">
		<cfset notifyLogDealerTime = "#notifyLogDealerTimeHH#:#notifyLogDealerTimeMM# #notifyLogDealerTimeTT#">
	<cfelse>
		<cfset notifyLogDealerTime = "">
	</cfif>
	
	<cfinvoke 
		 component="serviceInterruption.serviceInterruption"
		 method="addEscalation"
		 returnvariable="SuccessfulAdd">
		 <cfinvokeargument name="ds" value="#ds#"/>
		 <cfinvokeargument name="incidentid" value="#incidentid#"/>
		 <cfinvokeargument name="notifyLogNotify" value="#notifyLogNotify#"/>
		 <cfinvokeargument name="notifyLogEmpname" value="#notifyLogEmpname#"/>
		 <cfinvokeargument name="notifyLogDateNotified" value="#notifyLogDateNotified#"/>
		 <cfinvokeargument name="notifyLogTimeNotified" value="#notifyLogTimeNotified#"/>
		 <cfinvokeargument name="notifyLogAppdealer" value="#notifyLogAppdealer#"/>
		 <cfinvokeargument name="notifyLogDealerNotified" value="#notifyLogDealerNotified#"/>
		 <cfinvokeargument name="notifyLogDealerDate" value="#notifyLogDealerDate#"/>
		 <cfinvokeargument name="notifyLogDealerTime" value="#notifyLogDealerTime#"/>	
	</cfinvoke>
	<cfif isDefined("form.btnSaveEscalationAddMore")>
		<cflocation url="notifyLog.cfm?incidentid=#incidentid#&more=true">
	<cfelse>
		<cflocation url="index.cfm">
	</cfif>
</cfif>

<!--- when Save Additional Notification button pressed --->
<cfif isDefined("form.btnSaveNotificationAddMore") or isDefined("form.btnSaveNotification")>
	<cfif trim(form.AdditionalNotifyTimeHH) is not "" and trim(form.AdditionalNotifyTimeMM) is not "">
		<cfset AdditionalNotifyTime = "#form.AdditionalNotifyTimeHH#:#form.AdditionalNotifyTimeMM# #form.AdditionalNotifyTimeTT#">
	<cfelse>
		<cfset AdditionalNotifyTime = "">
	</cfif>
	<cfinvoke 
		 component="serviceInterruption.serviceInterruption"
		 method="addMoreEscalation"
		 returnvariable="SuccessfulAdd">
		 <cfinvokeargument name="ds" value="#ds#"/>
		 <cfinvokeargument name="incidentid" value="#incidentid#"/>
		 <cfinvokeargument name="AdditionalNotifyDate" value="#AdditionalNotifyDate#"/>
		 <cfinvokeargument name="AdditionalNotifyTime" value="#AdditionalNotifyTime#"/>
		 <cfinvokeargument name="AdditionalNotifyEmpname" value="#AdditionalNotifyEmpname#"/>
		 <cfinvokeargument name="AdditionalNotifyDealer" value="#AdditionalNotifyDealer#"/>
	</cfinvoke>	 
	<cfif isDefined("form.btnSaveNotificationAddMore")>
		<cflocation url="notifyLog.cfm?incidentid=#incidentid#&more=true">
	<cfelse>
		<cflocation url="index.cfm">
	</cfif>
</cfif>

<!--- get escalation details --->
<cfquery name="getEsc" datasource="#ds#">
	select * from serviceInterruption_main 
	where incidentid = #incidentid# 
</cfquery>

<cfset form.notifyLogNotify = getEsc.notifyLogNotify>
<cfset form.notifyLogEmpname = getEsc.notifyLogEmpName>
<cfset form.notifyLogDateNotified = dateformat(getEsc.notifyLogDateTimeNotified,'mm/dd/yyyy')>
<cfset form.notifyLogTimeNotifiedHH = timeformat(getEsc.notifyLogDateTimeNotified,'hh')>
<cfset form.notifyLogTimeNotifiedMM = timeformat(getEsc.notifyLogDateTimeNotified,'MM')>
<cfset form.notifyLogTimeNotifiedTT = timeformat(getEsc.notifyLogDateTimeNotified,'TT')>
<cfset form.notifyLogAppdealer = getEsc.notifyLogAppdealer>
<cfset form.notifyLogDealerNotified = getEsc.notifyLogDealerNotified>
<cfset form.notifyLogDealerDate = dateformat(getEsc.notifyLogDealerDateTime,'mm/dd/yyyy')>
<cfset form.notifyLogDealerTimeHH = timeformat(getEsc.notifyLogDealerDateTime,'hh')>
<cfset form.notifyLogDealerTimeMM = timeformat(getEsc.notifyLogDealerDateTime,'MM')>
<cfset form.notifyLogDealerTimeTT = timeformat(getEsc.notifyLogDealerDateTime,'TT')>

<cfquery name="getAdditionalNotifications" datasource="#ds#">
	select * from serviceInterruption_notifications 
	where incidentid = #incidentid# 
	order by notificationID asc 
</cfquery>

<cfquery name="getEmployees" datasource="#ds#">
	select adminuserid, firstname + ' ' + lastname as employeeName 
	from admin_users 
	where active = 1
	order by firstname, lastname 
</cfquery>

<link rel="stylesheet" type="text/css" href="../styles.css">

<br />

<div align="center" class="normal">

<cfform method="post" action="notifyLog.cfm">
	<cfinput type="hidden" name="incidentid" value="#incidentid#">
    <table width="783" border="0" cellspacing="0" cellpadding="5">
        <tr>
            <td width="773" class="highlightbar"><b>Escalation Notification Log for Service Interruption</b></td>
        </tr>
        <tr>
            <td class="greyrowbottomnopadding">
                <table width="100%" border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td width="86%" class="alert"> <b>INITIAL NOTIFICATION:</b> </td>
					</tr>
					<tr>
					 <td class="nopadding">
                       <table border="0" cellspacing="0" cellpadding="5">
					   <tr>
						<td>
						<cfif form.notifyLogNotify is 1>
							<cfinput style="vertical-align:middle" type="checkbox" checked name="notifyLogNotify" id="LogNotify" value="1" required="yes" message="COPS Employee Notified is required."/><b>COPS Employee Notified:</b><br />
						<cfelse>
							<cfinput style="vertical-align:middle" type="checkbox" name="notifyLogNotify" id="LogNotify" value="1" required="yes" message="COPS Employee Notified is required."/><b>COPS Employee Notified:</b><br />
						</cfif>
						<!---	<input type="checkbox" <cfif form.notifyLogNotify is 1>checked</cfif> name="notifyLogNotify" id="LogNotify" value="1" /><b>COPS Employee Notified:</b>	--->
						</td>
						<td>
							<select  name="notifyLogEmpname">
								<option value=""></option>
								<cfoutput query="getEmployees">
									<option <cfif getEmployees.employeeName is form.notifyLogEmpname>selected</cfif> value="#employeeName#">#employeeName#</option>
								</cfoutput>
							</select>
						</td>
						<td><b>Date:</b></td>
						<td>
							<cfinput type="datefield" name="notifyLogDateNotified" style="width:75px" value="#form.notifyLogDateNotified#" required="yes" message="Date is required."></td>
						<td><b>Time:</b></td>
						<td>
							<cfinput type="text" name="notifyLogTimeNotifiedHH" style="width:25px; vertical-align:middle;" value="#form.notifyLogTimeNotifiedHH#" required="yes" message="Hour is required."> : 
							<cfinput type="text" name="notifyLogTimeNotifiedMM" style="width:25px; vertical-align:middle;" value="#form.notifyLogTimeNotifiedMM#" required="yes" message="Minutes are required.">
							<cfselect name="notifyLogTimeNotifiedTT" style="vertical-align:middle;" required="yes" message="AM or PM is required.">
								<option <cfif form.notifyLogTimeNotifiedTT is "AM">selected</cfif> value="AM">AM</option>
								<option <cfif form.notifyLogTimeNotifiedTT is "PM">selected</cfif> value="PM">PM</option>
							</cfselect>
						</td>
						</tr>
					<tr>
					<td colspan="3">
					<cfif form.notifyLogAppdealer is 1>
						<cfinput type="checkbox" checked name="notifyLogAppdealer" value="1" required="yes" message="Applicable Dealers Notified is required."/><b>Applicable Dealers Notified:</b>	
					<cfelse>
						<cfinput type="checkbox" name="notifyLogAppdealer" value="1"/><b>Applicable Dealers Notified:</b>	
					</cfif>
					</td>
					</tr>
					<tr>
					<td> <b>Dealer/Employee Notified:</b></td>
					<td>
					    <cfinput type="text" name="notifyLogDealerNotified" style="width:175px" value="#form.notifyLogDealerNotified#">
					</td>
					<td><b>Date:</b></td>	
					<td>
					    <cfinput type="datefield" name="notifyLogDealerDate" style="width:75px" value="#form.notifyLogDealerDate#">
					</td>
					<td><b>Time:</b></td>
					<td>
					    <cfinput type="text" name="notifyLogDealerTimeHH" style="width:25px; vertical-align:middle;" value="#form.notifyLogDealerTimeHH#" > : 
						<cfinput type="text" name="notifyLogDealerTimeMM" style="width:25px; vertical-align:middle;" value="#form.notifyLogDealerTimeMM#" >
						<cfselect name="notifyLogDealerTimeTT" style="vertical-align:middle;">
							<option <cfif ucase(form.notifyLogDealerTimeTT) is "AM"> selected </cfif> value="AM">AM</option>
							<option <cfif ucase(form.notifyLogDealerTimeTT) is "PM"> selected </cfif> value="PM">PM</option>
						</cfselect>
					</td>
					</tr>
					</table>		
					</td>
					</tr>
					<tr>
					<td>
						<cfinput type="submit" name="btnSaveEscalation" value="Save and Complete">&nbsp;
						<cfinput type="submit" name="btnSaveEscalationAddMore" value="Save and Add Additional Notifications">
					</td>
					</tr>
				</table>
		</table>
</cfform>

</br>

<cfoutput query="getAdditionalNotifications">
   <table width="783" border="0" cellspacing="0" cellpadding="5">
		<tr>
            <td width="773" class="highlightbar"><b>Additional Notification</b></td>
        </tr>
		<tr>
			<td class="greyrowbottomnopadding">
                <table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					 <td class="nopadding">
						<table border="0" cellspacing="0" cellpadding="5">
							<tr>
								<td align="right"><b>Date:</b></td>
								<td>
								#dateformat(getAdditionalNotifications.AdditionalNotifyDateTime,'mm/dd/yyyy')#
								</td>
								<td><b>Time:</b></td>
								<td>
								#timeformat(getAdditionalNotifications.AdditionalNotifyDateTime,'hh:mm tt')#
								</td>
							</tr>
							<tr>
								<td><b>COPS Employee Notified:</b></td>
								<td colspan="3">#getAdditionalNotifications.AdditionalNotifyEmpname#</td>
							</tr>
							<tr>
								<td><b>Dealer/Employee Notified:</b></td>
								<td colspan="3">
								#getAdditionalNotifications.AdditionalNotifyDealer#
								</td>	
							</tr>
						</table>
					</td>
				</tr>
			</table>
			</td></tr>
	</table>
	<br />
</cfoutput>

<!--- if Additional Notifications button is pushed, display more info --->
<cfif isDefined("url.more")>
<cfform method="post" action="notifyLog.cfm">
	<cfinput type="hidden" name="incidentid" value="#incidentid#">
   <table width="783" border="0" cellspacing="0" cellpadding="5">
		<tr>
            <td width="773" class="highlightbar"><b>Additional Notifications As Required:</b></td>
        </tr>
		<tr>
			<td class="greyrowbottomnopadding">
                <table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					 <td class="nopadding">
                       <table border="0" cellspacing="0" cellpadding="5">
					   <tr>
					   <td align="right"><b>Date:</b></td>
						<td>
						    <cfinput type="datefield" name="AdditionalNotifyDate" style="width:75px" value="#form.AdditionalNotifyDate#" required="yes" message="Additional Notification Date is required."></td>
						<td><b>Time:</b></td>
						<td>
							<cfinput type="text" name="AdditionalNotifyTimeHH" style="width:25px; vertical-align:middle;" value="#form.AdditionalNotifyTimeHH#" required="yes" message="Additional Notification Hour is required."> : 
							<cfinput type="text" name="AdditionalNotifyTimeMM" style="width:25px; vertical-align:middle;" value="#form.AdditionalNotifyTimeMM#" required="yes" message="Additional Notification Minutes are required.">
							<cfselect name="AdditionalNotifyTimeTT" style="vertical-align:middle;" required="yes" message="AM or PM is required.">
								<option <cfif form.AdditionalNotifyTimeTT is "AM">selected</cfif> value="AM">AM</option>
								<option <cfif form.AdditionalNotifyTimeTT is "PM">selected</cfif> value="PM">PM</option>
							</cfselect>
						 </td>
					   </tr>
					   <tr>
						<td><b>COPS Employee Notified:</b></td>
						<td colspan="3">
						<select  name="AdditionalNotifyEmpname">
							<option value=""></option>
							<cfoutput query="getEmployees">
								<option <cfif getEmployees.employeeName is form.AdditionalNotifyEmpname>selected</cfif> value="#employeeName#">#employeeName#</option>
							</cfoutput>
						</select>
						</td>
					   </tr>
					   <tr>
					   <td><b>Dealer/Employee Notified:</b></td>
						<td colspan="3"><cfinput type="text" name="AdditionalNotifyDealer" style="width:250px" value="#form.AdditionalNotifyDealer#"></td>	
					   </tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<cfinput type="submit" name="btnSaveNotification" value="Save and Complete">
						&nbsp;&nbsp;&nbsp;
						<cfinput type="submit" name="btnSaveNotificationAddMore" value="Save and Add Another Notification">
					</td>
				</tr>
			</table>
			</td></tr>

	</table>
</cfform>
</cfif>
<br />
<a href="index.cfm" style="text-decoration:underline;">Return to Incident List</a>
</div> 