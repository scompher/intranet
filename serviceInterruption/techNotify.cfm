
<cfparam name="form.techPriority" default="">
<cfparam name="form.techAffectedSubscriber" default="">
<cfparam name="form.techAffectedDatabase" default="">
<cfparam name="form.techIsolationOfCause" default="">
<cfparam name="form.techServiceRestored" default="0">
<cfparam name="form.techDateRestored" default="">
<cfparam name="form.techTimeRestoredHH" default="">
<cfparam name="form.techTimeRestoredMM" default="">
<cfparam name="form.techTimeRestoredTT" default="">
<cfparam name="form.RCAICAFollowUp" default="0">
<cfparam name="form.techAdditionalNotifications" default="0">

<cfparam name="err" default="">

<!--- when save info button pressed --->
<cfif isDefined("form.btnSaveInfo")>

	<!--- validate form here --->
	<cfif form.techAffectedSubscriber is "databaseSegment" and trim(form.techAffectedDatabase) is "">
		<cfset err = listappend(err,"The affected database needs to be filled out.")>
	</cfif>

	<cfif trim(err) is "">
		<cfif trim(form.techTimeRestoredHH) is not "" and trim(form.techTimeRestoredMM) is not "">
			<cfset techTimeRestored = "#form.techTimeRestoredHH#:#form.techTimeRestoredMM# #form.techTimeRestoredTT#">
		<cfelse>
			<cfset techTimeRestored = "">
		</cfif>
		<!--- call save tech info component method --->
		<cfinvoke 
			 component="serviceInterruption.serviceInterruption"
			 method="addTechDetails"
			 returnvariable="SuccessfulAdd">
			 <cfinvokeargument name="ds" value="#ds#"/>
			 <cfinvokeargument name="incidentid" value="#incidentid#"/>
			 <cfinvokeargument name="techPriority" value="#techPriority#"/>
			 <cfinvokeargument name="techAffectedSubscriber" value="#techAffectedSubscriber#"/>
			 <cfinvokeargument name="techAffectedDatabase" value="#techAffectedDatabase#"/>
			 <cfinvokeargument name="techIsolationOfCause" value="#techIsolationOfCause#"/>
			 <cfinvokeargument name="techServiceRestored" value="#techServiceRestored#"/>
			 <cfinvokeargument name="techDateRestored" value="#techDateRestored#"/>
			 <cfinvokeargument name="techTimeRestored" value="#techTimeRestored#"/>
			 <cfinvokeargument name="RCAICAFollowUp" value="#RCAICAFollowUp#"/>
			 <cfinvokeargument name="techAdditionalNotifications" value="#techAdditionalNotifications#"/>
		</cfinvoke>
		<cfif form.techAdditionalNotifications is 1>
			<cflocation url="notifyLog.cfm?incidentid=#incidentid#">
		<cfelse>
			<cflocation url="index.cfm">
		</cfif>
	</cfif>
</cfif>

<cfif trim(err) is "">
	<cfquery name="getTechNotifyInfo" datasource="#ds#">
		select * from serviceInterruption_main 
		where incidentid = #incidentid# 
	</cfquery>
	<cfif getTechNotifyInfo.recordcount is not 0>
		<cfif getTechNotifyInfo.pickedUp is 0>
			<cfquery name="setToPickedUp" datasource="#ds#">
				update serviceInterruption_main 
				set pickedUp = 1, pickedUpBy = #cookie.adminlogin#, pickedUpDateTime = #createodbcdatetime(now())# 
				where incidentid = #incidentid#
			</cfquery>
		</cfif>
		<cfset form.techPriority = getTechNotifyInfo.techPriority>
		<cfset form.techAffectedSubscriber = getTechNotifyInfo.techAffectedSubscriber>
		<cfset form.techAffectedDatabase = getTechNotifyInfo.techAffectedDatabase>
		<cfset form.techIsolationOfCause = getTechNotifyInfo.techIsolationOfCause>
		<cfset form.techServiceRestored = getTechNotifyInfo.techServiceRestored>
		<cfset form.techDateRestored = dateformat(getTechNotifyInfo.techDateTimeRestored,'mm/dd/yyyy')>
		<cfset form.techTimeRestoredHH = timeformat(getTechNotifyInfo.techDateTimeRestored,'HH')>
		<cfset form.techTimeRestoredMM = timeformat(getTechNotifyInfo.techDateTimeRestored,'MM')>
		<cfset form.techTimeRestoredTT = timeformat(getTechNotifyInfo.techDateTimeRestored,'TT')>
		<cfset form.RCAICAFollowUp = getTechNotifyInfo.RCAICAFollowUp>
		<cfset form.techAdditionalNotifications = getTechNotifyInfo.techAdditionalNotifications>
	</cfif>
</cfif>

<link rel="stylesheet" type="text/css" href="../styles.css">

<br />

<div align="center" class="normal">

<cfform method="post" action="techNotify.cfm">
<cfinput type = "hidden" name="incidentid" value="#incidentid#" />

<!--- if error msg needs to be displayed, display it --->
<table width="500" border="0" cellspacing="0" cellpadding="5" class="alert">
	<cfloop list="#err#" index="errorMsg">
	<cfoutput>
	<tr>
		<td><li><b>#errorMsg#</b></td>
	</tr>
	</cfoutput>
	</cfloop>
</table>

<table width="750" border="0" cellspacing="0" cellpadding="5">
	<tr>
	<td class="highlightbar"><b>Technology to Provide Following Information:</b></td>
	</tr>
	<tr>
	<td class="greyrowbottomnopadding">
	<table width="100%" border="0" cellspacing="0" cellpadding="5">
		<tr>
		<td><b>Priority:</b></td>
		</tr>
		<tr>
		<td class="nopadding">
		<table border="0" cellspacing="0" cellpadding="5">
			<tr>
			<td>
				<cfif form.techPriority is "priorityCritical">
					<cfinput type="radio" checked name="techPriority" value="priorityCritical" required="yes" message="Priority is required." /> 
				<cfelse>
					<cfinput type="radio" name="techPriority" value="priorityCritical" required="yes" message="Priority is required."/> 
				</cfif>
			</td>
			<td>Critical</td>
			<td><b>*Alarm handling impeded</b></td>
			</tr>
			<tr>
			<td>
				<cfif form.techPriority is "priorityHigh">
					<cfinput type="radio" checked name="techPriority" value="priorityHigh" required="yes" message="Priority is required." /> 
				<cfelse>
					<cfinput type="radio" name="techPriority" value="priorityHigh" required="yes" message="Priority is required." /> 
				</cfif>
			</td>
			<td>High</td>
			<td><b>*Temporary resolution allows for continuation of alarm handling</b> </td>
			</tr>
		</table>
		</td>
		</tr>
	</table>
	</td>
	</tr>

	<tr>
	<td class="greyrowbottomnopadding">
	<table width="100%" border="0" cellspacing="0" cellpadding="5">
		<tr>
		<td><b>Affected Subscribers:</b></td>
		</tr>
		<tr>
		<td class="nopadding">
		<table border="0" cellspacing="0" cellpadding="5">
			<tr>
			<td>
			<cfif form.techAffectedSubscriber is "entireSubscriber" > 
				<cfinput type="radio" checked name="techAffectedSubscriber" value="entireSubscriber" required="yes" message="Affected Subscribers is required."/> 
			<cfelse>
				<cfinput type="radio" name="techAffectedSubscriber" value="entireSubscriber" required="yes" message="Affected Subscribers is required."/> 
			</cfif>
			</td>
			<td>Entire Subscriber Base</td>
			<td>
			<cfif form.techAffectedSubscriber is "databaseSegment" > 
				<cfinput type="radio" checked name="techAffectedSubscriber" value="databaseSegment" required="yes" message="Affected Subscribers is required."/> 
			<cfelse>
				<cfinput type="radio" name="techAffectedSubscriber" value="databaseSegment" required="yes" message="Affected Subscribers is required."/> 
			</cfif>
			</td>
			<td>Data Base Segment</td>
			</tr>
		</table>
		</td>
		</tr>

		<tr>
		<td><span class="alert"><b>Please define, if possible the affected data base below: </b></span></td>
		</tr>
		<tr>
		<td>
			<cftextarea style="width:740px;" rows="5" name="techAffectedDatabase"><cfoutput>#form.techAffectedDatabase#</cfoutput></cftextarea>
		</td>
		</tr>
		<tr>
		<td>
			<b>Isolation of Cause: </b>&nbsp;
			<cfselect style="vertical-align:middle;" name="techIsolationOfCause" id="techIsolationOfCause" required="yes" message="Isolation of Cause is required.">
				<option <cfif form.techIsolationOfCause is "web"> selected </cfif> value="web">Web Services</option>
				<option <cfif form.techIsolationOfCause is "automation"> selected </cfif> value="automation">Automation</option>
				<option <cfif form.techIsolationOfCause is "telco"> selected </cfif> value="telco">Telco</option>
				<option <cfif form.techIsolationOfCause is "isp"> selected </cfif> value="isp">ISP</option>
				<option <cfif form.techIsolationOfCause is "other"> selected </cfif> value="other">Other</option>
			</cfselect>
		</td>
		</tr>
	</table>
	</td>
	</tr>

	<tr>
	<td class="greyrowbottomnopadding">
	<table border="0" cellspacing="0" cellpadding="5">
		<tr>
		<td nowrap="nowrap">
		<input type="checkbox" <cfif form.techServiceRestored is 1>checked</cfif> name="techServiceRestored" value="1" />Service Fully Restored:	
		</td>
		<td>Date:</td>
		<td>
		<cfinput type="datefield" name="techDateRestored" style="width:75px" value="#form.techDateRestored#" />
		</td>
		<td>Time:</td>
		<td>
		<cfinput type="text" name="techTimeRestoredHH" style="width:25px; vertical-align:middle;" value="#form.techTimeRestoredHH#" > : 
		<cfinput type="text" name="techTimeRestoredMM" style="width:25px; vertical-align:middle;" value="#form.techTimeRestoredMM#" >
		<cfselect style="vertical-align:middle;" name="techTimeRestoredTT">
			<option <cfif form.techTimeRestoredTT is "AM">selected</cfif> value="AM">AM</option>
			<option <cfif form.techTimeRestoredTT is "PM">selected</cfif> value="PM">PM</option>
		</cfselect>						
		</td>
		</tr>	
	</table>
	</td>
	</tr>

	<tr>
	<td class="greyrowbottomnopadding">
	<table width="100%" border="0" cellspacing="0" cellpadding="5">
		<tr>
		<td width="4%">
			<input type="checkbox" name="RCAICAFollowUp" <cfif form.RCAICAFollowUp is 1>checked</cfif> value="1" />RCA/ICA Follow Up
		</td>
		</tr>
		<tr>
		<td width="4%">
			<input type="checkbox" name="techAdditionalNotifications" <cfif form.techAdditionalNotifications is 1>checked</cfif> value="1" />Escalation Notifications Needed
		</td>
		</tr>
		<tr>
		<td colspan="2">
			<cfinput type="submit" name="btnSaveInfo" id="SaveInfo" value="Save Info"> &nbsp;
		</td>
		</tr>
	</table>
	</td>
	</tr>
</table>

</cfform>

<br>
<a style="text-decoration:underline;" href="index.cfm">Return to Incidents</a>

</div>

