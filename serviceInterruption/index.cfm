<!--- Service Interruption Log Index Page --->
<link rel="stylesheet" type="text/css" href="/styles.css">

<cfparam name="err" default="">

<!--- pagination variables --->
<cfset maxrows = 50>
<cfparam name="p" default="1">
<cfparam name="pages" default="1">
<!--- pagination variables --->

<!--- cal cstart page --->
<cfset start = (maxrows * p) - (maxrows - 1)>
<!--- cal cstart page --->

<cfquery name="getInterruptions" datasource="#ds#">
	select serviceInterruption_main.*, admin_users.firstname + ' ' + admin_users.lastname as pickedUpName
	from serviceInterruption_main 
	left join admin_users on serviceInterruption_main.pickedUpBy = admin_users.adminuserid 
</cfquery>

<br />

<div align="center" class="normal" style="position:relative;z-index:0">
<table width="955" border="0" cellpadding="5" cellspacing="0">
	<form method="post" action="newIncidentrpt.cfm">
	<tr>
		<td><input type="submit" value="Create New Incident" /></td>
	</tr>
	</form>
</table>
<table width="955" border="0" cellpadding="5" cellspacing="0">
		<tr>
			<td class="highlightbar"><b>Service Interruption Log</b></td>
		</tr>
		<tr>
			<td class="greyrowbottomnopadding">
				<table width="100%" border="0" cellpadding="5" cellspacing="0">
					<tr>
						<td width="1%" class="linedrowrightcolumn">&nbsp;</td>
						<td width="15" class="linedrowrightcolumn">&nbsp;</td>
						<td width="15" class="linedrowrightcolumn">&nbsp;</td>
						<td width="150" nowrap="nowrap" class="linedrowrightcolumn"><b>Date/Time Created</b></td>
						<td width="150" class="linedrowrightcolumn"><b>Date/Time Picked Up</b></td>
						<td width="150" class="linedrowrightcolumn"><b>Picked Up By</b></td>
						<td class="linedrow"><b>Interruption Reason</b></td>
					</tr>
					<cfoutput query="getInterruptions">
					<tr>
						<td class="linedrowrightcolumn">#getInterruptions.currentRow#.</td>
						<td class="linedrowrightcolumn" align="center">
						<cfif getInterruptions.pickedUp is 0>
							<a href="techNotify.cfm?incidentid=#getInterruptions.incidentid#"><img border="0" src="/images/unread.gif" /></a>
						<cfelseif getInterruptions.pickedUp is 1>
							<a href="techNotify.cfm?incidentid=#getInterruptions.incidentid#"><img border="0" src="/images/read.gif" /></a>
						</cfif>
						</td>
						<td class="linedrowrightcolumn">
						<a href="viewDetail.cfm?incidentid=#getInterruptions.incidentid#"><img border="0" src="/images/manifyingglass.gif" width="15" height="15" /></a>
						</td>
						<td class="linedrowrightcolumn">
						#dateformat(getInterruptions.interruptionDateTime,'mm/dd/yyyy')# 
						#timeformat(getInterruptions.interruptionDateTime,'HH:mm tt')# 
						</td>
						<td class="linedrowrightcolumn">
						<cfif trim(getInterruptions.pickedUpDateTime) is not "">
						#dateformat(getInterruptions.pickedUpDateTime,'mm/dd/yyyy')# 
						#timeformat(getInterruptions.pickedUpDateTime,'HH:mm tt')# 
						<cfelse>
						&nbsp;
						</cfif>
						</td>
						<td class="linedrowrightcolumn" nowrap="nowrap">#pickedUpName#&nbsp;</td>
						<td class="linedRow">
						#left(getInterruptions.interruptionDetails,100)#
						<cfif len(getInterruptions.interruptionDetails) gt 100>...</cfif>&nbsp;
						</td>
					</tr>
					</cfoutput>
				</table>
			</td>	
		</tr>
</table>

<br>
<a style="text-decoration:underline;" href="/index.cfm">Return to Intranet</a>
</div>