<cfset ds = "copalink">
<cfset webserviceURL = "http://192.168.1.7">

<cfif dn is "9999">
	<cfset dn = "C094">
	<cfset pc = "XXXXX">
</cfif>

<cfif isDefined("form.btnViewAlarms")>
	<cfinvoke 
	 webservice="#webserviceurl#/remoting/alarmlist.cfc?wsdl"
	 method="sayHello"
	 returnvariable="isLoggedIn">
		<cfinvokeargument name="dn" value="#dn#"/>
		<cfinvokeargument name="pc" value="#pc#"/>
	</cfinvoke>
	<cfif isLoggedIn is 2>
	<link rel="stylesheet" type="text/css" href="/mobile/styles.css">
	<body leftmargin="0" topmargin="0" rightmargin="0" bottommargin="0">
	<table width="100%" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td class="headerbar">Scrolling List of Alarms</td>
		</tr>
		<tr>
			<td>You are currently logged into the scrolling list of alarms.  Please try again in a few minutes.</td>
		</tr>
	</table>
	</body>
	<cfabort>
	</cfif>
</cfif>

<cfparam name="refresh" default="30">
<cfparam name="clear" default="no">
<cfparam name="type" default="1,2,3">

<cfset type = replace(type, ",", "|", "all")>

<cfset urlString = "scroller.cfm?type=#type#&dn=#dn#&pc=#pc#">

<cfif refresh is not 0>
	<cfoutput>
	<meta http-equiv="refresh" content="#refresh#;url=scroller.cfm?type=#type#&refresh=#refresh#&dn=#dn#&pc=#pc#">
	</cfoutput>
</cfif>

<cfif clear is "yes">
	<cfquery name="clearAlarms" datasource="#ds#">
		delete from scrolling_alarms_mobile 
		where dealernumber = '#dn#' and passcode = '#pc#' 
	</cfquery>
</cfif>

<cfinvoke 
webservice="#webserviceurl#/remoting/alarmlist.cfc?wsdl"
method="getAlarms"
returnvariable="newAlarms">
	<cfinvokeargument name="dn" value="#dn#"/>
	<cfinvokeargument name="pc" value="#pc#"/>
	<cfinvokeargument name="types" value="#type#"/>
</cfinvoke>

<cfquery name="AddToHistory" datasource="#ds#">
	begin
		update scrolling_alarms_mobile 
		set newItem = 0 
		where dealernumber = '#dn#' and passcode = '#pc#' 
	end
	<cfif newAlarms.recordcount gt 0>
	<cfloop query="newAlarms">
		begin
			insert into scrolling_alarms_mobile (incidentNumber, accountNumber, name, dateTimeReceived, timeZone, alarmCode, alarmZone, condition, dispatcherHandled, status, dateTimeProcessed, address, priority, dealernumber, passcode, dealerLoginID, newItem)
			values ('#incidentNumber#', '#accountNumber#', '#name#', '#dateTimeReceived#', '#timeZone#', '#alarmCode#', '#alarmZone#', '#condition#', '#dispatcherHandled#', '#status#', '#dateTimeProcessed#', '#address#', '#priority#', '#dn#', '#pc#', #cookie.adminlogin#, 1)
		end
	</cfloop>
	</cfif>
</cfquery>

<cfquery name="getAlarms" datasource="#ds#">
	select * from scrolling_alarms_mobile with (nolock) 
	where dealerNumber like '%#dn#%' and passcode = '#pc#' 
	order by dateTimeReceived desc 
</cfquery>

<link rel="stylesheet" type="text/css" href="styles.css">
<body leftmargin="0" topmargin="0" rightmargin="0" bottommargin="0">
<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="headerbar">Scrolling List of Alarms </td>
	</tr>
	<tr>
		<td class="greyrow">
		<b>
		Auto Refresh 
		<cfif refresh is not 0>
		(every <cfoutput>#refresh#</cfoutput> seconds)
		<cfelse>
		(currently OFF)
		</cfif>
		<br>
		</td>
	</tr>
	<cfoutput>
	<tr>
		<td class="linedrowblack" style="width:100%;">
		<b>
		<cfif refresh is 0>
			OFF | 
		<cfelse>
			<a href="#urlString#&refresh=0">OFF</a> | 
		</cfif>
		<cfif refresh is 15>
			15 | 
		<cfelse>
			<a href="#urlString#&refresh=15">15</a> | 
		</cfif>
		<cfif refresh is 30>
			30 | 
		<cfelse>
			<a href="#urlString#&refresh=30">30</a> | 
		</cfif>
		<a href="#urlString#&refresh=#refresh#">Refresh Now</a> 
		</b>
		</td>
	</tr>
	<cfoutput>
	<tr>
		<td style="width:100%;" class="linedrowblack"><a href="#urlString#&refresh=#refresh#&clear=yes"><b>Clear List</b></a></td>
	</tr>
	</cfoutput>
	<tr>
		<td style="width:100%;" class="linedrowblack"><b>There are #newAlarms.recordcount# new items</b></td>
	</tr>
	</cfoutput>
	<cfoutput query="getAlarms">
	<cfset fc = "##000000">
	<cfif getAlarms.currentRow mod 2 is not 0>
		<cfset bgc ="##EEEEEE">
	<cfelse>
		<cfset bgc = "##FFFFFF">
	</cfif>
	<cfif dispatcherHandled is "Y">
		<cfset bgc = "##E81212">
		<cfset fc = "##FFFFFF">
	</cfif>
	<cfif status is "6">
		<cfset bgc = "##0C5E0E">
		<cfset fc = "##FFFFFF">
	</cfif>
	<tr bgcolor="#bgc#">
		<td bgcolor="#bgc#" style="width:100%;">
			<cfif newItem is 1>
			<b>
			<span style="color:#fc#">
			<a style="color:#fc#" href="detail.cfm?id=#alarmid#&type=#type#&refresh=#refresh#&dn=#dn#&pc=#pc#">
			#dateformat(dateTimeReceived,'mm/dd/yyyy')# #timeformat(dateTimeReceived,'hh:mm tt')#
			<br>
			#name#
			</a><br>
			Code: #alarmCode#<br>
			Zone: #alarmZone#<br>
			Condition:<br>
			#condition#
			</span>
			</b>
			<cfelse>
			<span style="color:#fc#">
			<a style="color:#fc#" href="detail.cfm?id=#alarmid#&type=#type#&refresh=#refresh#&dn=#dn#&pc=#pc#">
			#dateformat(dateTimeReceived,'mm/dd/yyyy')# #timeformat(dateTimeReceived,'hh:mm tt')#
			<br>
			#name#
			</a><br>
			Code: #alarmCode#<br>
			Zone: #alarmZone#<br>
			Condition:<br>
			#condition#
			</span>
			</cfif>
		</td>
	</tr>
	</cfoutput>
</table>
<br>
<a href="index.cfm" style="text-decoration:underline;">Return to Scroller Menu</a>
</body>
