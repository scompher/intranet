<cfset ds = "copalink">

<cfquery name="getAlarm" datasource="#ds#">
	select * from scrolling_alarms_mobile with (nolock) 
	where alarmid = #id# 
</cfquery>

<link rel="stylesheet" type="text/css" href="styles.css">
<body leftmargin="0" topmargin="0" rightmargin="0" bottommargin="0">

<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="headerbar">Alarm Detail </td>
	</tr>
	<cfoutput query="getAlarm">
	<cfset rec = listgetat(accountnumber,1,"-")>
	<cfset acct = listgetat(accountnumber,2,"-")>
	<tr>
		<td class="greyrow"><b>Account ##: </b></td>
	</tr>
	<tr>
		<td>#accountnumber#</td>
	</tr>
	<tr>
		<td class="greyrow"><b>Name:</b></td>
	</tr>
	<tr>
		<td>#name#</td>
	</tr>
	<tr>
		<td class="greyrow"><b>Address:</b></td>
	</tr>
	<tr>
		<td class="linedrow">#address#</td>
	</tr>
	<tr>
		<td class="greyrow"><b>Date/Time Received: </b></td>
	</tr>
	<tr>
		<td>#dateformat(dateTimeReceived,'mm/dd/yyyy')# #timeformat(dateTimeReceived,'hh:mm tt')#</td>
	</tr>
	<tr>
		<td class="greyrow"><b>Time Zone: </b></td>
	</tr>
	<tr>
		<td>#timeZone#</td>
	</tr>
	<tr>
		<td class="greyrow"><b>Code:</b></td>
	</tr>
	<tr>
		<td>#alarmCode#</td>
	</tr>
	<tr>
		<td class="greyrow"><b>Zone:</b></td>
	</tr>
	<tr>
		<td>#alarmZone#</td>
	</tr>
	<tr>
		<td class="greyrow"><b>Condition:</b></td>
	</tr>
	<tr>
		<td>#condition#</td>
	</tr>
	</cfoutput>
</table>
<br>
<div align="center">
<cfoutput>
<a href="scroller.cfm?type=#type#&refresh=#refresh#&dn=#dn#&pc=#pc#">Return to Signals</a>
</cfoutput>
</div>
</body>