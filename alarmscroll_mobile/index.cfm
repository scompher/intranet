<link rel="stylesheet" type="text/css" href="styles.css">
<body leftmargin="0" topmargin="0" rightmargin="0" bottommargin="0">

<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="headerbar">Scrolling List of Alarms </td>
	</tr>
	<form method="post" action="scroller.cfm">
	<tr>
		<td>
		<b>Dealer Number: </b>
		</td>
	</tr>
	<tr>
		<td class="greyrow">
		<input type="text" name="dn" size="4" maxlength="4" value="">
		</td>
	</tr>
	<tr>
		<td>
		<b>Passcode: </b>
		</td>
	</tr>
	<tr>
		<td class="greyrow">
		<input type="text" name="pc" size="5" maxlength="5" value="">
		</td>
	</tr>
	<tr>
		<td><b>Alarm Types to Show </b></td>
	</tr>
	<tr>
		<td class="greyrow">
		<input type="checkbox" name="type" checked="checked" value="1">
		<span style="color:#FFFFFF; background-color:#E81212"><b>Dispatcher Handled</b></span><br>
		<input type="checkbox" name="type" checked="checked" value="2">
		<b>Logged In Alarms</b><br>
		<input type="checkbox" name="type" checked="checked" value="3">
		<span style="color:#FFFFFF; background-color:#0C5E0E"><b>Test Results</b></span>
		</td>
	</tr>
	<tr>
		<td>
			<b>
			<input class="button" type="submit" name="btnViewAlarms" value=" View Alarms ">
			</b>
		</td>
	</tr>
	</form>
</table>
<br>
<br>
<a href="/index.cfm" style="text-decoration:underline;">Return to Intranet Menu</a>
</body>
