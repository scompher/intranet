<!---
http://192.168.107.10/recordings/playrecording.cfm?r=00001032781491001439&format=a&startedat=20170331-18:00:32&endedat=20170331-19:04:32
http://192.168.107.81/recordings/playrecording.cfm?r=00001023471491240833&format=a&startedat=20170403-14:32:50&endedat=20170403-15:02:50
--->

<cfsetting showdebugoutput="no">

<link rel="stylesheet" type="text/css" href="../styles.css">

<cfif isDefined("url.startedat")>
	<cfset sd = listgetat(url.startedat,1,"-")>
	<cfset st = listgetat(url.startedat,2,"-")>
	<cfset startDate = right(sd,2) & "/"  & mid(sd,5,2) & "/" & left(sd,4)>
	<cfset startTime = st>
<cfelse>
	<cfset startDate = dateformat(now(),'dd/mm/yyyy')>
	<cfset startTime = timeformat(now(),'HH:mm:ss')>
</cfif>

<cfif isDefined("url.endedat")>
	<cfset ed = listgetat(url.endedat,1,"-")>
	<cfset et = listgetat(url.endedat,2,"-")>
	<cfset endDate = right(ed,2) & "/"  & mid(ed,5,2) & "/" & left(ed,4)>
	<cfset endTime = et>
<cfelse>
	<cfset endDate = dateformat(now(),'dd/mm/yyyy')>
	<cfset endTime = timeformat(now(),'HH:mm:ss')>
</cfif>

<cfif not isDefined("url.r")>
	<br>
	<br>
	<div align="center">
	<table border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td class="highlightbar"><b>Recording Playback</b></td>
		</tr>
		<tr>
			<td class="greyrowbottomnopadding">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td>
						The recording was not found 
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	</div>
	<cfabort>
</cfif>

<!--- API URL --->
<cfset apiurl = "http://192.168.28.51:8080/searchapi">
<cfset apiuser = "apiuser">
<cfset apipassword = "gocops">
<cfset recordingDir = "e:\websites\intranet\recordings\temp\#dateformat(now(),'yyyymmdd')#">
<cfset switchcallid = "#url.r#">
<cfset recordingName = "#switchcallid#.mp3">
<cfset tempName = "#switchcallid#.wav">
<cfset playbackDir = "#dateformat(now(),'yyyymmdd')#">

<!---
command => 'search',
layout => 'searchapi',
operator_startedat => '9',
param1_startedat => $sdate,
param2_startedat => $stime,
param3_startedat => $edate,
param4_startedat => $etime,
operator_otherparties => '6',
param1_otherparties => $extension
--->

<!--- <cfoutput>
#startDate# #startTime#<br>
#endDate# #endTime#<br>
#switchcallid#<br>
</cfoutput>
<cfabort> --->

<cfhttp url="#apiurl#" method="get" result="getresults" username="#apiuser#" password="#apipassword#" >
	<cfhttpparam type="url" name="command" value="search" >
	<cfhttpparam type="url" name="layout" value="searchapi" >
	<cfhttpparam type="url" name="operator_startedat" value="9" >
	<cfhttpparam type="url" name="param1_startedat" value="#startDate#" >
	<cfhttpparam type="url" name="param2_startedat" value="#startTime#" >
	<cfhttpparam type="url" name="param3_startedat" value="#endDate#" >
	<cfhttpparam type="url" name="param4_startedat" value="#endTime#" >
	<cfhttpparam type="url" name="operator_switchcallid" value="6" >
	<cfhttpparam type="url" name="param1_switchcallid" value="#switchcallid#" >
</cfhttp>

<cfset xResult = xmlparse(getresults.Filecontent)>

<cfif not isdefined("xResult.results.result")>
	<br>
	<br>
	<div align="center">
	<table border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td class="highlightbar"><b>Recording Playback</b></td>
		</tr>
		<tr>
			<td class="greyrowbottomnopadding">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td>
						The recording was not found 
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	</div>
	<cfabort>
</cfif>

<cfset binList = "">
<cfset results = xResult.results>
<cfset recordingNum = arraylen(results.result)>
<cfset outputcontent = "">
<cfset idlist = "">
<cfset wavlist = "">

<cfif arraylen(results.result) gte 1>

	<!--- create temp directory --->
	<cfif not directoryExists("#recordingDir#")>
		<cfdirectory action="create" directory="#recordingDir#">
	</cfif>
	
	<!--- get files from recorder --->
	<cfloop from="1" to="#arraylen(results.result)#" index="i">
		<cfset result = results.result[i]>
		<cfset id = result.xmlAttributes.inum>
		<cfset idlist = listappend(idlist,id)>
		<cfset wavlist = listappend(wavlist,"#recordingDir#\#id#.wav")>
		<cfhttp url="#apiurl#" method="get" getasbinary="auto" file="#id#.wav" path="#recordingDir#" result="getrecording" username="#apiuser#" password="#apipassword#" >
			<cfhttpparam type="url" name="command" value="replay" >
			<cfhttpparam type="url" name="id" value="#id#" >
		</cfhttp>
	</cfloop>
	
	<!--- concatinate all wav files into one recording --->
	<cfx_concatenateWavFiles files="#wavlist#" destination="#recordingDir#\#tempName#">
	
	<!--- convert recording to mp3 --->
	<cfx_wav2mp3 source="#recordingDir#\#tempName#" destination="#recordingDir#\#recordingName#" samplerate="8000"> 
	
	<br>
	<br>
	
	<div align="center">
	<table border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td class="highlightbar"><b>Recording Playback</b></td>
		</tr>
		<tr>
			<td class="greyrowbottomnopadding">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td>
						<audio controls>
							<cfoutput>
								<source src="temp/#playbackDir#/#recordingName#" type="audio/mpeg">
							</cfoutput>	
							<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=8,0,0,0" width="315" height="100" id="soundplayer" align="middle">
								<cfoutput>
									<param name="allowScriptAccess" value="sameDomain" />
									<param name="movie" value="soundplayer.swf?soundFile=temp/#playbackDir#/#recordingName#" />
									<param name="quality" value="high" />
									<param name="bgcolor" value="##F7F7F7" />
									<embed src="soundplayer.swf?soundFile=temp/#playbackDir#/#recordingName#" quality="high" bgcolor="##F7F7F7" width="315" height="100" name="soundplayer" align="middle" allowscriptaccess="sameDomain" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" />
								</cfoutput>
							</object>
						</audio>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	</div>
	
<cfelse>
	
	<br>
	<br>
	<div align="center">
	<table border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td class="highlightbar"><b>Recording Playback</b></td>
		</tr>
		<tr>
			<td class="greyrowbottomnopadding">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td>
						The recording was not found 
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	</div>
	
</cfif>

<!--- clean up temp files --->
<cfset wavlist = listappend(wavlist,"#recordingDir#\#tempName#")>
<cfloop list="#wavlist#" index="wavFile">
	<cfif fileExists("#wavFile#")>
		<cffile action="delete" file="#wavFile#">
	</cfif>
</cfloop>

