
<cfsetting showdebugoutput="no">

<link rel="stylesheet" type="text/css" href="../styles.css">

<cfparam name="url.r" default="">

<cfif trim(url.r) is "" or findnocase("_",url.r) is 0>

	The recording name is empty

<cfelse>
	
	<cfset recordingDir = listgetat(url.r,2,"_")>
	<cfset recordingName = listgetat(url.r,listlen(url.r,"\"),"\")>
	
	<cfset sourceFile = "\\192.168.9.234\recordings\#recordingDir#\#recordingName#">
	<cfif not directoryExists("e:\websites\intranet\recordings\temp\#recordingDir#")>
		<cfdirectory action="create" directory="e:\websites\intranet\recordings\temp\#recordingDir#">
	</cfif>
	<cfset destinationFile = "e:\websites\intranet\recordings\temp\#recordingDir#\#replace(recordingName,".wav",".mp3","all")#">
	
	<!--- convert recording from wav to mp3 and place on web server --->
	<cfif fileExists("#sourceFile#")>
		<cfx_wav2mp3 source="#sourceFile#" destination="#destinationFile#" samplerate="8000"> 
	<cfelse>
		<br>
		<br>
		<div align="center">
		<table border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td class="highlightbar"><b>Recording Playback</b></td>
			</tr>
			<tr>
				<td class="greyrowbottomnopadding">
					<table width="100%" border="0" cellspacing="0" cellpadding="5">
						<tr>
							<td>
							The recording was not found 
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		</div>
		<cfabort>
	</cfif>
	
	<cfset recordingName = replace(recordingName,".wav",".mp3","all")>
	
	<br>
	<br>
	
	<div align="center">
	<table border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td class="highlightbar"><b>Recording Playback</b></td>
		</tr>
		<tr>
			<td class="greyrowbottomnopadding">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td>
							<audio controls>
								<cfoutput>
									<source src="temp/#recordingDir#/#recordingName#" type="audio/mpeg">
								</cfoutput>	
								<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=8,0,0,0" width="315" height="100" id="soundplayer" align="middle">
									<cfoutput>
										<param name="allowScriptAccess" value="sameDomain" />
										<param name="movie" value="soundplayer.swf?soundFile=temp/#recordingDir#/#recordingName#" />
										<param name="quality" value="high" />
										<param name="bgcolor" value="##F7F7F7" />
										<embed src="soundplayer.swf?soundFile=temp/#recordingDir#/#recordingName#" quality="high" bgcolor="##F7F7F7" width="315" height="100" name="soundplayer" align="middle" allowscriptaccess="sameDomain" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" />
									</cfoutput>
								</object>
							</audio>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	</div>

</cfif>
