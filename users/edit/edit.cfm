
<script language="JavaScript" type="text/JavaScript">
function checkform(frm) {
	if (frm.firstname.value == '') {
		alert('Please enter the first name.');
		frm.firstname.focus();
		return false;
	}
	if (frm.lastname.value == '') {
		alert('Please enter the last name.');
		frm.lastname.focus();
		return false;
	}
	if (frm.email.value == '') {
		alert('Please enter the email address.');
		frm.email.focus();
		return false;
	}
	if (frm.username.value == '') {
		alert('Please enter the username.');
		frm.username.focus();
		return false;
	}
	if (frm.password.value != '' || frm.confirmpassword.value != '') {
		if (frm.password.value != frm.confirmpassword.value) {
			alert('The password and confirming password must match.');
			frm.password.focus();
			return false;
		}
	}
	if (frm.defaultdeptid.selectedIndex == 0) {
		alert('Please choose a primary department.');
		frm.defaultdeptid.focus();
		return false;
	}
	return true;
}
</script>

<!--- get user --->
<cfquery name="getuser" datasource="#request.odbc_datasource#">
	select * from Admin_Users where adminuserid = #url.uid#
</cfquery>

<!--- get user departments --->
<cfquery name="getuserdept" datasource="#ds#">
	select * from Admin_Users_Departments_Lookup
	where adminuserid = #url.uid#
</cfquery>
<cfset deptlist = valuelist(getuserdept.departmentid)>

<!--- get departments --->
<cfquery name="getdepartments" datasource="#ds#">
	select * from Admin_Users_Departments
	order by department ASC
</cfquery>

<!--- get access levels --->
<cfquery name="getsecuritylevels" datasource="#ds#">
	select * from Admin_Users_Security_Levels
	where seclevelid > 1
	order by seclevel asc
</cfquery>

<div align="center">
	<form method="post" action="save.cfm">
	<cfoutput>
	<input type="hidden" name="adminuserid" value="#url.uid#">
	</cfoutput>
    <table border="1" cellpadding="5" cellspacing="0">
		<tr><td colspan="2" align="center" bgcolor="DDDDDD"><b>Edit an administrative user</b></td></tr>
		<cfoutput query="getuser">
        <tr>
            <td><b>First Name </b></td>
            <td><input name="firstname" type="text" class="normal" size="50" maxlength="50" value="#getuser.firstname#"></td>
        </tr>
        <tr>
            <td><b>Last Name </b></td>
            <td><input name="lastname" type="text" class="normal" size="50" maxlength="50" value="#getuser.lastname#"></td>
        </tr>
        <tr>
            <td><b>Email</b></td>
            <td><input name="email" type="text" class="normal" size="50" maxlength="100" value="#getuser.email#"></td>
        </tr>
        <tr>
            <td><b>Username</b></td>
            <td><input name="username" type="text" class="normal" size="50" maxlength="50" value="#getuser.username#"></td>
        </tr>
		</cfoutput>
        <tr>
            <td><b>Password</b></td>
            <td><input name="password" type="password" class="normal" size="50" maxlength="50"></td>
        </tr>
        <tr>
            <td><b>Confirm Password</b></td>
            <td><input name="confirmpassword" type="password" class="normal" size="50" maxlength="50"></td>
        </tr>
		<cfif getsec.seclevelid IS 1>
		<tr>
			<td><b>Security Level</b></td>
			<td>
			<select name="seclevelid" class="normal">
			<cfoutput query="getsecuritylevels">
				<option value="#getsecuritylevels.seclevelid#" <cfif getsecuritylevels.seclevelid IS getuser.seclevelid>selected</cfif> >#getsecuritylevels.seclevel#</option>
			</cfoutput>
			</select>
			</td>
		</tr>
		<cfelse>
		<input type="hidden" name="seclevelid" value="#getuser.seclevelid#">
		</cfif>
        <tr>
            <td valign="top"><b>Choose Departments</b></td>
            <td>
			<select name="departmentid" multiple size="5">
			<cfoutput query="getdepartments">
				<option value="#getdepartments.departmentid#" <cfif listfind(deptlist, getdepartments.departmentid) IS NOT 0>selected</cfif> >#getdepartments.department#</option>
			</cfoutput>
			</select><br>
			<span class="small" style="color:ff0000;">Hold CTRL or SHIFT to select multiples</span>
			</td>
        </tr>
		<tr>
			<td><b>Primary Department:</b></td>
			<td>
			<select name="defaultdeptid">
				<option value="0"></option>
			<cfoutput query="getdepartments">
			<!--- <cfif listfind(deptlist, getdepartments.departmentid) IS NOT 0> --->
				<option value="#getdepartments.departmentid#" <cfif getuser.defaultdeptid IS getdepartments.departmentid>selected</cfif> >#getdepartments.department#</option>
			<!--- </cfif> --->
			</cfoutput>
			</select>
			</td>
		</tr>
        <tr>
            <td colspan="2" align="right">
			<input onClick="return checkform(this.form);" name="btnUpdateUser" type="submit" class="normal" value="Update User">
			<input type="reset" class="normal" value="Reset Form">
			</td>
            </tr>
    </table>
	<br>
	<table border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td align="center"><a href="/index.cfm">Return to Administrative Menu</a></td>
	</tr>
	</table>
	</form>
</div>
