
<cfparam name="deptid" default="0">

<cfquery name="getDepts" datasource="#ds#">
	select * from 
	admin_users_departments
	order by department asc
</cfquery>

<cfquery name="getusers" datasource="#request.odbc_datasource#">
	select admin_users.*, admin_users_departments.department
	from admin_users
	left join Admin_Users_Departments_Lookup on admin_users.adminuserid = Admin_Users_Departments_Lookup.adminuserid
	left join admin_users_departments on Admin_Users_Departments_Lookup.departmentid = admin_users_departments.departmentid
	where 
	<cfif deptid is not 0>
		admin_users_departments.departmentid = #deptid# and 
	</cfif>
	<!--- admin_users.seclevelid <> 1 and  ---> active <> 0 
	order by admin_users.lastname asc, admin_users.firstname asc, admin_users_departments.department asc
</cfquery>

<div align="center">
    <table width="600" border="1" cellpadding="5" cellspacing="0">
		<tr>
			<td colspan="3" style="padding:0px">
			<table border="0" cellspacing="0" cellpadding="3">
				<tr>
					<td><img src="/images/edit.gif" border="0" alt="Edit User"></td>
					<td><span class="small"> = Edit a Users Information</span></td>
				</tr>
				<tr>
					<td><img src="/images/delete.gif" border="0" alt="Delete User"></td>
					<td><span class="small"> = Delete a User from the Intranet</span></td>
				</tr>
			</table>
			</td>
		</tr>
		
		<form method="post" action="index.cfm">
		<tr>
			<td colspan="3" style="padding:0px;">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td width="34%" nowrap="nowrap">Filter by department: </td>
						<td width="66%">
							<select name="deptid" onchange="this.form.submit();">
								<option <cfif deptid is 0>selected</cfif> value="0">All Departments</option>
								<cfoutput query="getDepts">
									<option <cfif deptid is getdepts.departmentid>selected</cfif> value="#departmentid#">#department#</option>
								</cfoutput>
							</select>
						</td>
					</tr>

				</table>
			</td>
		</tr>
		</form>
		<tr>
			<td colspan="3" nowrap="nowrap">
				<input type="button" name="Button" value="Add New User" onclick="document.location='/users/add/';" />
			</td>
		</tr>
        <tr bgcolor="#FFFFCC">
            <td width="15%" align="center"><b>Action</b></td>
            <td><b>User</b></td>
            <td><b>Department</b></td>
        </tr>
		<cfset currRow = 1>
		<cfoutput query="getusers" group="adminuserid">
		<tr>
            <td valign="top" align="center" nowrap="nowrap">
			<a href="edit.cfm?uid=#adminuserid#"><img src="/images/edit.gif" border="0" alt="Edit User"></a>&nbsp;
			<a onclick="return confirm('Are you sure you wish to delete #getusers.firstname# #getusers.lastname#?');" href="delete.cfm?uid=#adminuserid#&deptid=#deptid#"><img src="/images/delete.gif" border="0" alt="Delete User"></a>
			</td>
            <td valign="top">#getusers.lastname#, #getusers.firstname#</td>
			<td valign="top"><cfoutput>#department#<br></cfoutput></td>
        </tr>
		</cfoutput>
    </table>
	<br>
	<table border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td align="center"><a href="/index.cfm">Return to Administrative Menu</a></td>
	</tr>
	</table>
</div>
