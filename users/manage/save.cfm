
<!--- update user info --->
<cfquery name="adduser" datasource="#ds#">
	update Admin_Users
	set defaultdeptid = #form.defaultdeptid#, seclevelid = #form.seclevelid#, firstname = '#form.firstname#', lastname = '#form.lastname#', email = '#form.email#'<cfif trim(form.username) IS NOT "">, username = '#form.username#'</cfif><cfif trim(form.password) IS NOT "">, password = '#form.password#'</cfif>
	where adminuserid = #adminuserid#
</cfquery>

<!--- update dept. lookup --->
<cfquery name="updateDept" datasource="#ds#">
begin
	delete from Admin_Users_Departments_Lookup
	where adminuserid = #adminuserid#
end
<cfloop index="item" list="#form.departmentid#">
begin
	insert into Admin_Users_Departments_Lookup (adminuserid, departmentid)
	values (#adminuserid#, #item#)
end
</cfloop>
</cfquery>

<div align="center">
    <table border="1" cellpadding="5" cellspacing="0">
		<tr><td align="center" bgcolor="DDDDDD"><b>Edit an administrative user</b></td></tr>
		<tr>
			<td align="center"><b>User Successfully Update</b></td>
		</tr>
    </table>
	<br>
	<table border="0" cellpadding="5" cellspacing="0">
	<tr>
	    <td align="center"><a href="/users/security/copyaccess.cfm">Copy Access</a> </td>
	    </tr>
	<tr>
		<td align="center"><a href="index.cfm">Edit another user</a></td>
	</tr>
	<tr>
		<td align="center"><a href="/index.cfm">Return to Administrative Menu</a></td>
	</tr>
	</table>
</div>

