<link rel="stylesheet" type="text/css" href="../styles.css">

<cfif isDefined("form.btnChangePW")>

	<!--- updatepw --->
	<cfquery name="updatepw" datasource="#ds#">
		update admin_users
		set password = '#form.newpw#'
		where adminuserid = #cookie.adminlogin#
	</cfquery>

	<cfset msg = "Your password has been successfully changed">

	<div align="center">
		<table width="400" border="0" cellpadding="5" cellspacing="0">
			<tr>
				<td colspan="2" align="center" bgcolor="FFFFCC" class="highlightbar"><b>Password Change </b></td>
			</tr>
			<cfoutput>
			<tr>
				<td colspan="2" align="center" class="greyrowbottom">#msg#</td>
			</tr>
			</cfoutput>
			<tr>
				<td align="center" colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td align="center" colspan="2"><a href="/index.cfm">Return to Main Menu</a></td>
			</tr>
		</table>
	</div>

<cfelse>

	<script language="JavaScript">
	<!--
	function checkform(frm) {
		if (frm.newpw.value == "") {alert('The new password is required'); frm.newpw.focus(); return false;}
		if (frm.confirmnewpw.value == "") {alert('You must confirm the new password'); frm.confirmnewpw.focus(); return false;}
		if (frm.newpw.value != frm.confirmnewpw.value) {alert('The new password and confirmation passwords do not match'); frm.newpw.focus(); return false;}
		return true;
	}
	//-->
	</script> 

	<div align="center">
		<form method="post" action="changepw.cfm">
		<table width="400" border="0" cellpadding="5" cellspacing="0">
			<tr>
				<td colspan="2" align="center" bgcolor="FFFFCC" class="highlightbar"><b>Password Change </b></td>
			</tr>
			<tr>
				<td colspan="2" class="greyrowbottom">
					<table width="100%" border="0" cellpadding="5" cellspacing="0" class="grey">
						<tr>
							<td width="39%" nowrap="nowrap">New Password: </td>
							<td width="61%">
								<input name="newpw" type="password" class="normal" size="20" maxlength="20" />
							</td>
						</tr>
						<tr>
							<td nowrap="nowrap">Confirm New Password: </td>
							<td>
								<input name="confirmnewpw" type="password" class="normal" size="20" maxlength="20" />
							</td>
						</tr>
						<tr align="center">
							<td colspan="2">
								<input onclick="return checkform(this.form);" name="btnChangePW" type="submit" class="sidebar" value="Change Password" />
							</td>
						</tr>
					</table>
				</td>
				</tr>
			
			<tr>
				<td align="center" colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td align="center" colspan="2"><a href="/index.cfm" class="obviousLink">Return to Main Menu</a></td>
			</tr>
		</table>
		</form>
	</div>

</cfif>
