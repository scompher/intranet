
<link rel="stylesheet" type="text/css" href="/styles.css">


<cfif isDefined("form.btnCopyAccess")>
	
	<!--- copy access --->
	<cfif copyToUserID is not 0>
		<cfquery name="removeCurrentAcess" datasource="#ds#">
			delete from Admin_Security_Lookup
			where adminuserid = #copyToUserID# 
		</cfquery>
		<cfquery name="copyaccess" datasource="#ds#">
			insert into Admin_Security_Lookup (adminuserid, itemid) 
			select #copyToUserID#, itemid from Admin_Security_Lookup
			where adminuserid = #copyFromUserID# 
		</cfquery>
	</cfif>
	
	<!--- get access user 1 --->
	<cfquery name="getAccess1" datasource="#ds#">
		select Admin_Security_Items.* 
		from Admin_Security_Items 
		inner join Admin_Security_Lookup on Admin_Security_Items.itemID = Admin_Security_Lookup.itemID
		inner join Admin_Users on Admin_Users.adminuserid = Admin_Security_Lookup.adminuserid 
		where Admin_Users.adminuserid = #copyFromUserID# 
		order by Admin_Security_Items.itemName ASC
	</cfquery>
	
	<!--- get new access user 2 --->
	<cfquery name="getAccess2" datasource="#ds#">
		select Admin_Security_Items.* 
		from Admin_Security_Items 
		inner join Admin_Security_Lookup on Admin_Security_Items.itemID = Admin_Security_Lookup.itemID
		inner join Admin_Users on Admin_Users.adminuserid = Admin_Security_Lookup.adminuserid 
		where Admin_Users.adminuserid = #copyToUserID# 
		order by Admin_Security_Items.itemName ASC
	</cfquery>
	
	<div align="center">
	<table border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td class="highlightbar"><b>Copy User Access</b></td>
		</tr>
		<tr>
			<td class="greyrowbottomnopadding">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td>User 1 access</td>
						<td>User 2 access</td>
					</tr>
					<tr>
						<td>
						<cfoutput query="getAccess1">
							#itemName#<br>
						</cfoutput>
						</td>
						<td>
						<cfoutput query="getAccess2">
							#itemName#<br>
						</cfoutput>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<br>
	<a class="normal" style="text-decoration:underline;" href="/index.cfm">Return to Intranet</a>
	</div>
	
<cfelse>
	
	<cfquery name="getusers" datasource="#ds#">
		select admin_users.*
		from admin_users
		where admin_users.active = 1
		order by admin_users.lastname asc, admin_users.firstname asc
	</cfquery>
	
	<div align="center">
	<table border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td class="highlightbar"><b>Copy User Access</b></td>
		</tr>
		<tr>
			<td class="greyrowbottomnopadding">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<form method="post" action="copyaccess.cfm">
					<tr>
						<td width="50%">User to copy from  </td>
						<td width="50%">User to copy to </td>
					</tr>
					<tr>
						<td>
							<select name="copyFromUserID">
								<option value="0">Select User</option>
								<cfoutput query="getUsers">
									<option value="#adminuserid#">#lastname#, #firstname#</option>
								</cfoutput>
							</select>
							</td>
						<td>
							<select name="copyToUserID">
								<option value="0">Select User</option>
								<cfoutput query="getUsers">
									<option value="#adminuserid#">#lastname#, #firstname#</option>
								</cfoutput>
							</select>
						</td>
						</tr>
					<tr>
						<td colspan="2" align="center">
							<input type="submit" name="btnCopyAccess" value="Copy Access">
						</td>
					</tr>
					</form>
				</table>
			</td>
		</tr>
	</table>
	<br>
	<a class="normal" style="text-decoration:underline;" href="/index.cfm">Return to Intranet</a>
	</div>
	
</cfif>