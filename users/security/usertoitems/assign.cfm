
<script type="text/javascript">
function checkUncheckAll(frm) {
	var theItem = frm.itemid;
	var state = frm.state.value;
	if (state == 'true') {
		var newState = false;
	} else {
		var newState = true;
	}
	if (theItem.length > 1) {
		for (var x = 0; x < theItem.length; x++) {
			theItem[x].checked = newState;
		}
	} else {
		theItem.checked = newState;
	}
	frm.state.value = newState;
}
</script>

<div align="center">
	<cfif NOT isDefined("adminuserid")>
		<cfparam name="filterid" default="0">
		<cfif filterid is 0>
			<cfquery name="getusers" datasource="#ds#">
				select * 
				from admin_users 
				where active = 1 
				order by lastname asc, firstname asc
			</cfquery>
		<cfelse>
			<cfquery name="getusers" datasource="#ds#">
				select admin_users.*
				from admin_users
				inner join Admin_Users_Departments_Lookup on admin_users.adminuserid = Admin_Users_Departments_Lookup.adminuserid
				where Admin_Users_Departments_Lookup.departmentid = #filterid# and admin_users.active = 1
				order by admin_users.lastname asc, admin_users.firstname asc
			</cfquery>
		</cfif>
		<cfquery name="getdepts" datasource="#ds#">
			select * from Admin_Users_Departments
			order by department asc
		</cfquery>
		<form method="post" action="assign.cfm">
		<table border="1" cellpadding="5" cellspacing="0">
		<tr bgcolor="#DDDDDD">
			<td align="center"><b>Please Select a User</b></td>
		</tr>
		<tr>
			<td  valign="top">
			<cfoutput>
			Filter By Department<br /><br />
			<select name="departmentid" class="normal" onchange="document.location='assign.cfm?filterid=' + this.options[this.selectedIndex].value;">
			</cfoutput>
				<option value="0" <cfif filterid IS 0>selected</cfif> >Show All Departments</option>
			<cfoutput query="getdepts">
				<option value="#departmentid#" <cfif filterid IS getdepts.departmentid>selected</cfif> >#department#</option>
			</cfoutput>
			</select>
			<br />
			<br />
			<select name="adminuserid" size="25" class="normal" style="width:400px;">
			<cfoutput query="getusers">
				<option value="#adminuserid#">#lastname#, #firstname#</option>
			</cfoutput>
			</select>			</td>
		</tr>
		<tr>
			<td align="center">
			<input type="submit" value="Select User" class="normal">
			</td>
		</tr>
		</table>
		<br>
		<table border="0" cellpadding="5" cellspacing="0">
		<tr>
			<td align="center"><a href="/index.cfm">Return to Administrative Menu</a></td>
		</tr>
		</table>
		</form>
	<cfelse>
	
		<cfquery name="getitems" datasource="#request.odbc_datasource#">
			select Admin_Security_Sections.*, Admin_Security_Items.*
			from Admin_Security_Sections, Admin_Security_Items
			where Admin_Security_Sections.sectionid = Admin_Security_Items.sectionid
			order by Admin_Security_Sections.sectionName ASC, Admin_Security_Items.itemName ASC
		</cfquery>
		
		<cfquery name="getseclevel" datasource="#request.odbc_datasource#">
			select * from Admin_Security_Lookup
			where adminuserid = #adminuserid#
		</cfquery>
		
		<cfif getseclevel.recordcount IS NOT 0>
			<cfset seclist = valuelist(getseclevel.itemID)>
		<cfelse>
			<cfset seclist = 0>
		</cfif>
		
		<cfquery name="getsec" dbtype="query">
			select * from getitems where itemid IN (#seclist#)
			order by sectionName ASC, itemName ASC
		</cfquery>
		

		
		<form method="post" action="assignupdate.cfm">
		<cfoutput>
		<input type="hidden" name="adminuserid" value="#adminuserid#">
		</cfoutput>
		<input type="hidden" name="assign" value="0" />
		<input type="hidden" name="state" value="false" />
		<table width="750" border="1" cellpadding="5" cellspacing="0">
			<tr bgcolor="dddddd">
				<td align="center"><b>Existing Security Levels</b></td>
				<td align="center"><b>Currently Assigned Security Levels</b></td>
				</td>
			</tr>
			<tr>
				<td width="50%" align="center" valign="top">
				<select ondblclick="this.form.assign.value = 1; this.form.submit();" name="itemid" size="50" multiple class="normal">
				<cfoutput query="getitems">
					<cfif listfindnocase(seclist, getitems.itemid) IS 0>
					<option value="#getitems.itemid#">#getitems.sectionName# - #getitems.itemName#</option>
					</cfif>
				</cfoutput>
				</select><br>
				<span style="color:ff0000">Hold CTRL or SHIFT to select multiples</span>
				</td>
				<td width="50%" align="center" valign="top">
				<table width="90%" border="0" cellpadding="0" cellspacing="0">
					<tr><td><a href="javascript:checkUncheckAll(document.forms[0]);">[Check/Uncheck All]</a></td></tr>
					<tr>
						<td>
						<cfoutput query="getsec">
						<input type="checkbox" name="itemid" value="#itemid#"> #getsec.sectionName# - #getsec.itemname#<br>					
						</cfoutput>
						<br>
						<span style="color:ff0000">Select single or multiple items and click Remove</span>
						</td>
					</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top"><input name="btnAssign" type="submit" class="normal" value="Assign Access"></td>
				<td align="center" valign="top"><input name="btnUpdate" type="submit" class="normal" value="Remove Access"></td>
			</tr>
		</table>
		<br>
		<table border="0" cellpadding="5" cellspacing="0">
		<tr>
			<td align="center"><a href="assign.cfm">Choose another user</a></td>
		</tr>
		<tr>
			<td align="center"><a href="/index.cfm">Return to Administrative Menu</a></td>
		</tr>
		</table>
		</form>
	</cfif>
</div>
