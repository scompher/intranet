
<cfif isDefined("form.btnAssign") or form.assign is 1>
	<!--- assign selected security levels --->
	<cfif isDefined("form.itemid")>
	<cfloop index="item" list="#itemid#">
		<cfquery name="assignsecurity" datasource="#request.odbc_datasource#">
			if exists (select itemID from Admin_Security_Lookup where adminuserid = #adminuserid# and itemid = #item#)
				print 'exists'
			else
				begin
					insert into Admin_Security_Lookup (adminuserid, itemid)
					values (#adminuserid#, #item#)
				end
		</cfquery>
	</cfloop>
	</cfif>
	<cflocation url="assign.cfm?adminuserid=#adminuserid#">
<cfelseif isDefined("form.btnUpdate")>
	<!--- assign selected security levels --->
	<cfif isDefined("form.itemid")>
	<cfquery name="updatesecurity" datasource="#request.odbc_datasource#">
		delete from Admin_Security_Lookup
		where adminuserid = #adminuserid# and itemid IN (#itemid#)
	</cfquery>
	</cfif>
	<cflocation url="assign.cfm?adminuserid=#adminuserid#">
</cfif>

