<script type="text/javascript">
function checkUncheckAll(frm) {
	var theItem = frm.adminuserid;
	var state = frm.state.value;
	if (state == 'true') {
		var newState = false;
	} else {
		var newState = true;
	}
	if (theItem.length > 1) {
		for (var x = 0; x < theItem.length; x++) {
			theItem[x].checked = newState;
		}
	} else {
		theItem.checked = newState;
	}
	frm.state.value = newState;
}
</script>

<div align="center">
	<cfif NOT isDefined("itemid")>
		<cfquery name="getitems" datasource="#request.odbc_datasource#">
			select Admin_Security_Sections.*, Admin_Security_Items.*
			from Admin_Security_Sections, Admin_Security_Items
			where Admin_Security_Sections.sectionid = Admin_Security_Items.sectionid
			order by Admin_Security_Sections.sectionName ASC, Admin_Security_Items.itemName ASC
		</cfquery>
		<form method="post" action="assign.cfm">
		<table border="1" cellpadding="5" cellspacing="0">
		<tr bgcolor="#DDDDDD">
			<td align="center"><b>Please Select an Item</b></td>
		</tr>
		<tr>
			<td  valign="top">
			<select name="itemid" size="30" class="normal">
			<cfoutput query="getitems">
				<option value="#itemid#">#getitems.sectionName# - #getitems.itemName#</option>
			</cfoutput>
			</select>
			</td>
		</tr>
		<tr>
			<td align="center">
			<input type="submit" value="Select Item" class="normal">
			</td>
		</tr>
		</table>
		<br>
		<table border="0" cellpadding="5" cellspacing="0">
		<tr>
			<td align="center"><a href="/index.cfm">Return to Administrative Menu</a></td>
		</tr>
		</table>
		</form>
	<cfelse>

		<cfparam name="filterid" default="0">

		<cfquery name="getallusers" datasource="#ds#">
			select * 
			from admin_users where active <> 0
			order by lastname asc, firstname asc
		</cfquery>
		
		<cfif filterid is 0>
			<cfquery name="getusers" datasource="#ds#">
				select * 
				from admin_users where active <> 0
				order by lastname asc, firstname asc
			</cfquery>
		<cfelse>
			<cfquery name="getusers" datasource="#ds#">
				select admin_users.*
				from admin_users
				inner join Admin_Users_Departments_Lookup on admin_users.adminuserid = Admin_Users_Departments_Lookup.adminuserid
				where Admin_Users_Departments_Lookup.departmentid = #filterid# and admin_users.active <> 0 
				order by admin_users.lastname asc, admin_users.firstname asc
			</cfquery>
		</cfif>
		
		<cfquery name="getseclevel" datasource="#request.odbc_datasource#">
			select * from Admin_Security_Lookup
			where itemid = #itemid#
		</cfquery>
		
		<cfif getseclevel.recordcount IS NOT 0>
			<cfset seclist = valuelist(getseclevel.adminuserid)>
		<cfelse>
			<cfset seclist = 0>
		</cfif>
		
		<cfquery name="getsec" dbtype="query">
			select * from getallusers where adminuserid IN (#seclist#)
			order by lastname asc
		</cfquery>
		
		<cfquery name="getdepts" datasource="#ds#">
			select * from Admin_Users_Departments
			order by department asc
		</cfquery>
		
		<form method="post" action="assignupdate.cfm">
		<cfoutput>
		<input type="hidden" name="itemid" value="#itemid#">
		<input type="hidden" name="filterid" value="#filterid#">
		<input type="hidden" name="state" value="false" />
		</cfoutput>
		<table width="750" border="1" cellpadding="5" cellspacing="0">
			<tr bgcolor="dddddd">
				<td align="center"><b>Existing Security Levels</b></td>
				<td align="center"><b>Currently Assigned Security Levels</b></td>
				</td>
			</tr>
			<tr>
				<td width="50%" align="center" valign="top">
				<cfoutput>
				<br />
				Filter By Department<br /><br />
				<select name="departmentid" class="normal" onchange="document.location='assign.cfm?itemid=#itemid#&filterid=' + this.options[this.selectedIndex].value;">
				</cfoutput>
					<option value="0" <cfif filterid IS 0>selected</cfif> >Show All Departments</option>
				<cfoutput query="getdepts">
					<option value="#departmentid#" <cfif filterid IS getdepts.departmentid>selected</cfif> >#department#</option>
				</cfoutput>
				</select>
				<br /><br />
				<select name="adminusers" size="15" multiple class="normal">
				<cfoutput query="getusers">
					<cfif listfindnocase(seclist, getusers.adminuserid) IS 0>
					<option value="#getusers.adminuserid#">#getusers.lastname#, #getusers.firstname#</option>
					</cfif>
				</cfoutput>
				</select><br>
				<span style="color:ff0000">Hold CTRL or SHIFT to select multiples</span>
				</td>
				<td width="50%" align="center" valign="top">
				<table width="90%" border="0" cellpadding="0" cellspacing="0">
					<tr><td><a href="javascript:checkUncheckAll(document.forms[0]);">[Check/Uncheck All]</a></td></tr>
					<tr>
						<td>
						<cfoutput query="getsec">
						<input type="checkbox" name="adminuserid" value="#adminuserid#"> #getsec.lastname#, #getsec.firstname#<br>					
						</cfoutput>
						<br>
						<span style="color:ff0000">Select single or multiple items and click Remove</span>
						</td>
					</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td align="center" valign="top"><input name="btnAssign" type="submit" class="normal" value="Assign Access"></td>
				<td align="center" valign="top"><input name="btnUpdate" type="submit" class="normal" value="Remove Access"></td>
			</tr>
		</table>
		<br>
		<table border="0" cellpadding="5" cellspacing="0">
		<tr>
			<td align="center"><a href="assign.cfm">Choose another item</a></td>
		</tr>
		<tr>
			<td align="center"><a href="/index.cfm">Return to Administrative Menu</a></td>
		</tr>
		</table>
		</form>
	</cfif>
</div>
