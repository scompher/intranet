

<cfif isDefined("form.btnAssign")>
	<!--- assign selected security levels --->
	<cfif isDefined("form.adminusers")>
	<cfloop index="adminid" list="#adminusers#">
		<cfquery name="assignsecurity" datasource="#request.odbc_datasource#">
			if exists (select itemID from Admin_Security_Lookup where adminuserid = #adminid# and itemid = #itemid#)
				print 'exists'
			else
				begin
					insert into Admin_Security_Lookup (adminuserid, itemid)
					values (#adminid#, #itemid#)
				end
		</cfquery>
	</cfloop>
	</cfif>
	<cflocation url="assign.cfm?itemid=#itemid#&filterid=#filterid#">
<cfelseif isDefined("form.btnUpdate")>
	<!--- assign selected security levels --->
	<cfif isDefined("form.itemid")>
	<cfquery name="updatesecurity" datasource="#request.odbc_datasource#">
		delete from Admin_Security_Lookup
		where itemid = #itemid# and adminuserid IN (#adminuserid#)
	</cfquery>
	</cfif>
	<cflocation url="assign.cfm?itemid=#itemid#&filterid=#filterid#">
</cfif>


