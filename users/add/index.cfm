
<script language="JavaScript" type="text/JavaScript">
function checkform(frm) {
	if (frm.firstname.value == '') {
		alert('Please enter the first name.');
		frm.firstname.focus();
		return false;
	}
	if (frm.lastname.value == '') {
		alert('Please enter the last name.');
		frm.lastname.focus();
		return false;
	}
	if (frm.email.value == '') {
		alert('Please enter the email address.');
		frm.email.focus();
		return false;
	}
	if (frm.username.value == '') {
		alert('Please enter the username.');
		frm.username.focus();
		return false;
	}
	if (frm.password.value == '') {
		alert('Please enter the password.');
		frm.password.focus();
		return false;
	}
	if (frm.confirmpassword.value == '') {
		alert('Please enter the confirmation password.');
		frm.confirmpassword.focus();
		return false;
	}
	if (frm.password.value != frm.confirmpassword.value) {
		alert('The password and confirming password must match.');
		frm.password.focus();
		return false;
	}
	if (frm.seclevelid.selectedIndex == 0) {
		alert('The security level is required.');
		frm.seclevelid.focus();
		return false;
	}
	return true;
}
function autocomplete(frm) {
	frm.email.value = frm.firstname.value.substr(0,1) + frm.lastname.value + "@copsmonitoring.com";
	frm.email.value = frm.email.value.toLowerCase();
	frm.username.value = frm.firstname.value.substr(0,1) + frm.lastname.value;
	frm.username.value = frm.username.value.toLowerCase();
	frm.password.focus();
}
</script>

<!--- get departments --->
<cfquery name="getdepartments" datasource="#ds#">
	select * from Admin_Users_Departments
	order by department ASC
</cfquery>

<!--- get access levels --->
<cfquery name="getsecuritylevels" datasource="#ds#">
	select * from Admin_Users_Security_Levels
	where seclevelid > 1
	order by seclevel asc
</cfquery>
<body onLoad="document.forms[0].firstname.focus();">
<div align="center">
	<form method="post" action="save.cfm">
    <table border="1" cellpadding="5" cellspacing="0">
		<tr><td colspan="2" align="center" bgcolor="DDDDDD"><b>Add an administrative user</b></td>
		</tr>
        <tr>
            <td><b>First Name </b></td>
            <td><input name="firstname" type="text" class="normal" size="50" maxlength="50"></td>
        </tr>
        <tr>
            <td><b>Last Name </b></td>
            <td><input name="lastname" type="text" class="normal" size="50" maxlength="50" onChange="autocomplete(this.form);"></td>
        </tr>
        <tr>
            <td><b>Email</b></td>
            <td><input name="email" type="text" class="normal" size="50" maxlength="100"></td>
        </tr>
        <tr>
            <td><b>Username</b></td>
            <td><input name="username" type="text" class="normal" size="50" maxlength="50"></td>
        </tr>
        <tr>
            <td><b>Password</b></td>
            <td><input name="password" type="password" class="normal" size="50" maxlength="50"></td>
        </tr>
        <tr>
            <td><b>Confirm Password</b></td>
            <td><input name="confirmpassword" type="password" class="normal" size="50" maxlength="50"></td>
        </tr>
		<tr>
			<td><b>Security Level</b></td>
			<td>
			<select name="seclevelid" class="normal">
				<option value="0"></option>
			<cfoutput query="getsecuritylevels">
				<option value="#getsecuritylevels.seclevelid#">#getsecuritylevels.seclevel#</option>
			</cfoutput>
			</select>
			</td>
		</tr>
        <tr>
            <td valign="top"><b>Choose Departments</b></td>
            <td>
			<select name="departmentid" multiple size="5">
			<cfoutput query="getdepartments">
				<option value="#getdepartments.departmentid#">#getdepartments.department#</option>
			</cfoutput>
			</select><br>
			<span class="small" style="color:ff0000;">Hold CTRL or SHIFT to select multiples</span>
			</td>
        </tr>
		<tr>
			<td><b>Primary Department:</b></td>
			<td>
			<select name="defaultdeptid">
				<option value="0"></option>
			<cfoutput query="getdepartments">
			<!--- <cfif listfind(departmentlist, getdepartments.departmentid) IS NOT 0> --->
				<option value="#getdepartments.departmentid#">#getdepartments.department#</option>
			<!--- </cfif> --->
			</cfoutput>
			</select>
			</td>
		</tr>
        <tr>
            <td colspan="2" align="center">
			<input onClick="return checkform(this.form);" type="submit" class="normal" value="Add User">
			<input type="reset" class="normal" value="Clear Form">
			</td>
            </tr>
    </table>
	<br>
	<table border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td align="center"><a href="/index.cfm">Return to Administrative Menu</a></td>
	</tr>
	</table>
	</form>
</div>
</body>
