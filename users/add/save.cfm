
<cftransaction>
	<cfquery name="adduser" datasource="#request.odbc_datasource#">
		insert into Admin_Users (firstname, lastname, email, username, password, active, seclevelid, defaultdeptid)
		values ('#form.firstname#', '#form.lastname#', '#form.email#', '#form.username#', '#form.password#', 1, #form.seclevelid#, #form.defaultdeptid#)
	</cfquery>
	<cfquery name="getID" datasource="#ds#">
		select max(adminuserid) as id from Admin_Users
	</cfquery>
	<cfquery name="insertdeptlookup" datasource="#ds#">
		<cfloop index="item" list="#departmentid#">
		begin
			insert into Admin_Users_Departments_Lookup (adminuserid, departmentid)
			values (#getid.id#, #item#)
		end
		</cfloop>
	</cfquery>
</cftransaction>

<!--- email user --->
<cfmail from="pgregory@copsmonitoring.com" to="#form.email#" subject="COPS Monitoring Intranet New User Notification" username="copalink@copsmonitoring.com" password="copsmoncal">
Hello #form.firstname#,

You have been added as a new user to the COPS Monitorng Intranet.  

Here is the URL of the intranet: #request.appurl#

Your username is : #form.username#
Your password is : #form.password#

After you log in, you have the option to change your password if you wish.  Should you ever forget your password, there is a link on the login page to have your password emailed to you.

</cfmail>

<div align="center">
	<form method="post" action="save.cfm">
    <table border="1" cellpadding="5" cellspacing="0">
		<tr><td align="center" bgcolor="DDDDDD"><b>Add an administrative user</b></td></tr>
		<tr>
			<td align="center"><b>User Successfully Added</b></td>
		</tr>
    </table>
	<br>
	<table border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td align="center"><a href="index.cfm">Add another user</a></td>
	</tr>
	<cfoutput>
	<tr>
		<td align="center"><a href="/users/security/usertoitems/assign.cfm?adminuserid=#getid.id#">Assign user access</a></td>
	</tr>
	<tr>
	    <td align="center"><a href="/users/security/copyaccess.cfm">Copy Access</a> </td>
    </tr>
	<cfif form.seclevelid LTE 2>
	<tr>
		<td align="center"><a href="/users/security/notifyoptions/notifyoptions.cfm?adminuserid=#getid.id#">Set managment notification access</a></td>
	</tr>
	</cfif>
	</cfoutput>
	<tr>
		<td align="center"><a href="/index.cfm">Return to Administrative Menu</a></td>
	</tr>
	</table>
	</form>
</div>
