
<cfquery name="getusers" datasource="#request.odbc_datasource#">
	select admin_users.*, admin_users_departments.department
	from admin_users
	inner join Admin_Users_Departments_Lookup on admin_users.adminuserid = Admin_Users_Departments_Lookup.adminuserid
	inner join admin_users_departments on Admin_Users_Departments_Lookup.departmentid = admin_users_departments.departmentid
	order by admin_users.lastname asc, admin_users_departments.department asc
</cfquery>

<div align="center">
    <table border="1" cellpadding="5" cellspacing="0">
        <tr bgcolor="#FFFFCC">
            <td width="15%" align="center">&nbsp;</td>
            <td><b>User</b></td>
            <td><b>Department</b></td>
        </tr>
		<cfoutput query="getusers" group="adminuserid">
		<tr>
            <td align="center" valign="top"><a onClick="return confirm('Are you sure you wish to delete #getusers.firstname# #getusers.lastname#?');" href="delete.cfm?uid=#adminuserid#"><img src="/images/delete.gif" border="0" alt="Delete User"></a></td>
            <td valign="top">#getusers.lastname#, #getusers.firstname#</td>
			<td valign="top"><cfoutput>#department#<br></cfoutput></td>
        </tr>
		</cfoutput>
    </table>
	<br>
	<table border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td align="center"><a href="/index.cfm">Return to Administrative Menu</a></td>
	</tr>
	</table>
</div>
