
<cftransaction>
	<!--- delete security --->
	<cfquery name="delsec" datasource="#request.odbc_datasource#">
		delete from Admin_Security_Lookup
		where adminuserid = #url.uid#
	</cfquery>
	
	<!--- del lookup --->
	<cfquery name="delLookup" datasource="#ds#">
		delete from Admin_Users_Departments_Lookup
		where adminuserid = #url.uid#
	</cfquery>
	
	<!--- delete user --->
	<cfquery name="deluser" datasource="#request.odbc_datasource#">
		delete from Admin_Users
		where adminuserid = #url.uid#
	</cfquery>
</cftransaction>

<cflocation url="index.cfm">
