
<!--- toList is the list of recipients the uptime report is emailed --->
<cfset toList = "dmaden@copsmonitoring.com,MSteigerwalt@copsmonitoring.com,hsparks@copsmonitoring.com,pgregory@copsmonitoring.com">

<cfset backdate = dateadd("d",-7,now())>

<cfset filename = "uptime report #dateformat(backdate, 'yyyymmdd')# to #dateformat(now(), 'yyyymmdd')#.pdf">

<cfset pdfFilename = "attachments/#filename#">

<cfset sd = createodbcdate(backdate)>
<cfset ed = createodbcdate(now())>

<cfdocument format="pdf" orientation="portrait" scale="75" filename="attachments/#filename#" overwrite="yes">
<cfinclude template="/uptime/outtages/reports/generateReport.cfm">
</cfdocument>

<cfmail from="pgregory@copsmonitoring.com" to="#toList#" subject="Weekly equipment uptime report from #dateformat(backdate, 'mm/dd/yyyy')# - #dateformat(now(), 'mm/dd/yyy')#" type="html" username="copalink@copsmonitoring.com" password="copsmoncal">
<style type="text/css">
.normal {font-family:Arial, Helvetica, sans-serif; font-size:12px}
</style>
<cfmailparam file="attachments\#filename#">
<div class="normal">
The weekly equipment uptime report from #dateformat(backdate, 'mm/dd/yyyy')# - #dateformat(now(), 'mm/dd/yyyy')# is attached.<br>

</div>
</cfmail>

process complete.

<!--- removed 6/9/2011 PG 
<cfdocument format="pdf" orientation="portrait" scale="75" filename="attachments/#filename#">

<cfset sd = createodbcdate(backdate)>
<cfset ed = createodbcdate(now())>

<cfset toDate = ed>
<cfset ed = dateadd("d",1,ed)>

<cfif dateformat(now(),'yyyy') gt 2007>
	<cfset startyear = dateformat(now(),'yyyy')>
	<cfset start = "1/1/" & startyear>
<cfelse>
	<cfset start = "3/12/2007">
</cfif>
<cfset present = now()>

<cfset totalYearlyHours = datediff("h", start, present)>

<cfset totalHours = datediff("h", sd, ed)>

<cfquery name="getInfo" datasource="#ds#">
	select 
	uptime_equipment_categories.equipmentCategory, 
	uptime_equipment.equipmentID, 
	uptime_equipment.equipmentName, 
	uptime_outtages.* 
	from uptime_equipment 
	left join uptime_outtages on uptime_equipment.equipmentid = uptime_outtages.equipmentid
	left join uptime_equipment_categories on uptime_equipment.equipmentCategoryID = uptime_equipment_categories.equipmentCategoryid
	order by equipmentCategory, equipmentName, startTime
</cfquery>

<cfquery name="getAfterHoursCalls" datasource="#ds#">
	select count(incidentid) as totalIncidents
	from callform_incidents
	where opened >= #sd# and opened < #ed#
</cfquery>

<cfquery name="gettime" datasource="#ds#">
	select timesheet_main.entryDate, timesheet_main.starttime, timesheet_main.hours, admin_users.adminuserid, admin_users.firstname, admin_users.lastname, timesheet_actions.actionName
	from timesheet_main
	inner join admin_users on timesheet_main.adminuserid = admin_users.adminuserid
	inner join timesheet_actions on timesheet_main.actionid = timesheet_actions.actionid
	where 
	timesheet_main.adminuserid IN (16,17) and 
	(timesheet_main.entryDate >= #sd# and timesheet_main.entryDate < #ed#) and 
	timesheet_main.actionid IN (106,107)
	order by lastname, firstname, entrydate, starttime
</cfquery>

<cfset callHrs = 0.0>
<cfset emailHrs = 0.0>

<cfset totalCalls = 0>
<cfset totalEmails = 0>

<div align="center">
<table width="550" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td colspan="3" align="center"><b>C.O.P.S. MONITORING EQUIPMENT UPTIME REPORT</b></td>
	</tr>
	<tr><td colspan="3">&nbsp;</td></tr>
	<cfoutput>
	<tr>
		<td colspan="3" align="center"><b>Uptime report from #dateformat(sd, 'mm/dd/yyyy')# to #dateformat(toDate, 'mm/dd/yyyy')#</b></td>
	</tr>
	</cfoutput>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<cfoutput>
	<tr>
		<td colspan="3"><b>Total after hours calls between #dateformat(sd, 'mm/dd/yyyy')# and #dateformat(toDate, 'mm/dd/yyyy')#: #getAfterHoursCalls.totalIncidents#</b></td>
	</tr>
	</cfoutput>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<cfif gettime.recordcount gt 0>
		<tr>
			<td colspan="3">
			<cfoutput><b>Support Information between #dateformat(sd,'mm/dd/yyyy')# and #dateformat(toDate, 'mm/dd/yyyy')# for:</b><br /></cfoutput>
			<br />
			<cfoutput query="gettime" group="adminuserid">
				<b>#firstname# #lastname#</b><br />
				<cfoutput>
					<cfif actionName is "Support Call">
						<cfset callHrs = callHrs + hours>
						<cfset totalCalls = totalCalls + 1>
					<cfelseif actionName is "Support Email">
						<cfset emailHrs = emailHrs + hours>
						<cfset totalEmails = totalEmails + 1>
					</cfif>
				</cfoutput>
				Total Support Calls: #totalCalls#<br />
				Total Support Call Time: #numberFormat(callHrs,"00.00")# Hours<br />
				<br />
				Total Support Emails: #totalEmails#<br />
				Total Support Email Time: #numberFormat(emailHrs,"00.00")# Hours<br />
				<br />
			</cfoutput>
			</td>
		</tr>
		<tr><td colspan="3">&nbsp;</td></tr>
	</cfif>
	<tr>
		<td><b>System</b></td>
		<td align="center"><b>Total Hours Down</b></td>
		<td align="center"><b>Period %</b></td>
		<td align="center"><b>Yearly %</b></td>
	</tr>
	<cfoutput query="getinfo" group="equipmentCategory">
	<tr>
		<td colspan="4"><br /><b>#equipmentCategory#</b><hr /></td>
	</tr>
	<cfoutput group="equipmentid">
	<cfset yearlyTotalHoursDown = 0.0>
	<cfset totalHoursDown = 0.0>
	<cfoutput>
		<cfif hrsUnavailable is not "">
			<cfif (startTime gte #sd# and endTime lt #ed#)>
				<cfset totalHoursDown = totalHoursDown + hrsUnavailable>
			</cfif>
			<cfset yearlyTotalHoursDown = yearlyTotalHoursDown + hrsUnavailable>
		</cfif>
	</cfoutput>
	<cfset totalUptime = totalHours - totalHoursDown>
	<cfset totalYearlyUptime = totalYearlyHours - yearlyTotalHoursDown>
	<cfset uptime = (totalUptime / totalHours) * 100>
	<cfset yearlyUptime = (totalYearlyUptime / totalYearlyHours) * 100>
	<tr>
		<td>#equipmentName#</td>
		<td align="center">#numberformat(totalHoursDown, "0.00")#</td>
		<td align="center">#decimalformat(uptime)#%</td>
		<td align="center">#decimalformat(yearlyUptime)#%</td>
	</tr>
	</cfoutput>
	</cfoutput>
	<!--- include equipment report --->
	<!--- <cfinclude template="/troubletickets/reports/calcUptime.cfm"> --->
	<!--- include equipment report --->
</table>
</div>

</cfdocument>
--->
