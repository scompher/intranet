
<cfset clientIP = attributes.clientIP>
<cfset clientPort = attributes.clientPort>
<cfset serviceToRestart = attributes.serviceToRestart>

<cfset clientSocket = createobject("java","java.net.Socket")>
<cfset inStream = createobject("java","java.io.BufferedReader")>
<cfset outStream = createobject("java","java.io.PrintWriter")>
<cfset iReader = createobject("java","java.io.InputStreamReader")>
<cfset temp = clientSocket.init(clientIP,clientPort)>
<cfset temp = iReader.init(clientSocket.getInputStream())>
<cfset temp = outStream.init(clientSocket.getOutputStream(), true)>
<cfset temp = inStream.init(iReader)>
<cfset temp = outStream.println("#serviceToRestart#")>
<cfset output = "">
<cfset endflag = false>

<cftry>
	<cfset temp = outStream.close()>
	<cfset temp = inStream.close()>
	<cfset temp = clientSocket.close()>
	<cfcatch type="any"><!--- do nothing ---></cfcatch>
</cftry>

<cfset clientSocket = "">
<cfset inStream = "">
<cfset outStream = "">
<cfset iReader = "">
