
<cfset pageList = "8563414570@vtext.com,8563414572@vtext.com,8563047855@vtext.com,8567250978@vtext.com">

<cfset msgCenterAlertFlag = false>
<cfset callProcessorAlertFlag = false>

<cfset ds = "nCall">

<cftry>

<cfquery name="getLastContacted" datasource="#ds#">
	select * from watch_dog
</cfquery>
<cfdump var="#getLastContacted#">
<cfset currTime = now()>

<cfoutput query="getLastContacted">
	<cfswitch expression="#module#">
		<cfcase value="MsgCentre">
			<cfset msgCenterLastHit = datediff("s",barked_at,currTime)>
		</cfcase>
		<cfcase value="CallProcessor">
			<cfset callProcessorLastHit = datediff("s",barked_at,currTime)>
		</cfcase>
	</cfswitch>
</cfoutput>

<cfif msgCenterLastHit gt 120>
	<cfset msgCenterAlertFlag = true>
</cfif>
<cfif callProcessorLastHit gt 40>
	<cfset callProcessorAlertFlag = true>
</cfif>

<cfoutput>
Message Center Last Check (in seconds): #msgCenterLastHit#<br>
Call Processor Last Check (in seconds): #callProcessorLastHit#<br>
</cfoutput>
<br>
<br>

<cfif msgCenterAlertFlag is false and callProcessorAlertFlag is false>

	<cflog application="no" date="yes" time="yes" file="nCallMonitorLog" text="nCall process check OK : callProcessorLastHit = #callProcessorLastHit#, msgCenterLastHit = #msgCenterLastHit#">

</cfif>

<!--- send restart command of msgCenter --->
<cfif msgCenterAlertFlag is true>
	<cfmail from="system.info@copsmonitoring.com" to="#pageList#" subject="" username="system" password="V01c3">Restarted nCall MsgCentre at #dateformat(currTime,'mm/dd/yyyy')# #timeformat(currTime, 'HH:mm:ss')# : Last Check (in seconds) #msgCenterLastHit#</cfmail>
	<cf_restartService clientIP="192.168.3.37" clientPort="4444" serviceToRestart="nCall MsgCentre">
	<cflog application="no" date="yes" time="yes" file="nCallMonitorLog" text="Restarted nCall MsgCentre service : Last Check (in seconds) #msgCenterLastHit#">
</cfif>

<!--- send restart command of callProcessor --->
<cfif callProcessorAlertFlag is true>
	<cfmail from="system.info@copsmonitoring.com" to="#pageList#" subject="" username="system" password="V01c3">Restarted nCall CallProcessor at #dateformat(currTime,'mm/dd/yyyy')# #timeformat(currTime, 'HH:mm:ss')# : Last Check (in seconds) #callProcessorLastHit#</cfmail>
	<cf_restartService clientIP="192.168.3.37" clientPort="4444" serviceToRestart="nCall Call Processor">
	<cflog application="no" date="yes" time="yes" file="nCallMonitorLog" text="Restarted nCall CallProcessor service : Last Check (in seconds) #callProcessorLastHit#">
</cfif>

<cfcatch>
	<!--- <cfmail from="system.info@copsmonitoring.com" to="#pageList#" subject="" username="system" password="V01c3">Error connecting to nCall database at #dateformat(currTime,'mm/dd/yyyy')# #timeformat(currTime, 'HH:mm:ss')#!</cfmail> --->
	<cflog application="no" date="yes" time="yes" file="nCallMonitorLog" text="ERROR OCCURRED ON PAGE #CFCATCH.Message# #CFCATCH.Detail#">
</cfcatch>
</cftry>