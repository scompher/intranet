
<cfset dn = "3612">
<cfset recipientList = "centralreports@atkins-systems.com">
<cfset sinceDate = dateAdd("d",-1,now())>

<cfset outputFilePath = "e:\websites\intranet\scheduled\attachments">
<cfset outputFileName = "Changes#dn#_#dateformat(sinceDate,'yyyymmdd')#.xls">

<cfset headerlist = "Date,Time,Process,DlrNum,Type,Account Number,Description,From,To">
<cfset colList = "changeDate,changeTime,processID,dealerNumber,changeType,accountNumber,attributeDescription,singleBeforeChange,singleAfterChange">
<cfset reportHeader = "">

<cfset attributeBlockList = "'Time change','Operator','Date Changed'">

<cfquery name="getChanges" datasource="changeLog">
	select 
	CONVERT(nvarchar(10),changeDateTime,101) as changeDate, 
	right(CONVERT(nvarchar(25),changeDateTime,0),7) as changeTime, 
	processID, 
	dealerNumber, 
	changeType, 
	accountNumber, 
	attributeDescription, 
	singleBeforeChange, 
	singleAfterChange 
	from changeLog with (nolock) 
	where 
	attributeDescription NOT IN (#preserveSingleQuotes(attributeBlockList)#) and 
	dealerNumber = '#dn#' and changeDatetime >= #createodbcdate(sincedate)# 
</cfquery>

<cfx_query2excel 
	file="#outputfilepath#\#outputfilename#" 
	headings="#headerlist#" 
	queryFields="#colList#" 
	query="#getChanges#" 
	format="excel" 
	reportHeader="#reportHeader#">

<cfmail from="reports@copsmonitoring.com" to="#recipientList#" subject="Dealer #dn# Daily Change Report for #dateformat(sinceDate,'mm/dd/yyyy')#" username="copalink@copsmonitoring.com" password="copsmoncal">
Change Report for #dateformat(sinceDate,'mm/dd/yyyy')#

<cfmailparam file="attachments\#outputfilename#">
</cfmail>
