<cfset theDate = now()>
<cfset recipientList = "ktierno@copsmonitoring.com">
<cfset ds = "sai">
<cfset outputfilepath = "e:\websites\intranet\scheduled\attachments">
<cfset outputfilename = "daily_oos_report_#dateformat(theDate,'mm-dd-yy')#.xls">
<cfset reportHeader = "">

<cfset headerlist = "inst_no,cs_no,out_of_service_date">
<cfset colList = "inst_no,cs_no,out_of_service_date">

<cfquery name="getData" datasource="#ds#">
	select 
	sai.sai_installer.inst_no, 
	sai.SAI_Out_of_Service.cs_no, 
	CONVERT(VARCHAR(10), sai.sai_out_of_service.oos_date, 101) AS out_of_service_date 
	from sai.SAI_Out_of_Service
	left join sai.sai_account on sai.SAI_Out_of_Service.cs_no = sai.sai_account.cs_no 
	left join sai.sai_installer on sai.sai_account.inst_no = sai.sai_installer.inst_no 
	order by sai.SAI_Out_of_Service.cs_no asc
</cfquery>

<cfx_query2excel 
	file="#outputfilepath#\#outputfilename#" 
	headings="#headerlist#" 
	queryFields="#colList#" 
	query="#getData#" 
	format="excel" 
	reportHeader="#reportHeader#">

<cfmail from="reports@copsmonitoring.com" to="#recipientList#" subject="Daily SAI OOS Report " username="copalink@copsmonitoring.com" password="copsmoncal">
<cfmailparam file="attachments\#outputfilename#">
</cfmail>

Done!