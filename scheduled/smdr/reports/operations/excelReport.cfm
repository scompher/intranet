
<!--- <cfset toEmail = "pgregory@copsmonitoring.com"> --->
<cfset reportDate = dateadd("d",-1,now())>
<cfset XLSFileName = "Operations_Phone_Report_#dateformat(reportDate,'mm-dd-yyyy')#.xls">
<cfset outputfilepath = "#request.directpath#\scheduled\smdr\reports\operations\attachments">

<cfset startDate = "#dateadd('d',-2,now())#">
<cfset startTime = "23:00">
<cfset startDateTime = dateformat(startDate,'mm/dd/yyyy') & " " & startTime>
<cfset endDate = "#dateadd('d',-1,now())#">
<cfset endTime = "23:00">
<cfset endDateTime = dateformat(endDate,'mm/dd/yyyy') & " " & endTime>

<cfquery name="getCalls" datasource="smdr">
	select *  
	from smdrData with (nolock) 
	where (dateTimeOccurred >= #createodbcdatetime(startDateTime)# and dateTimeOccurred < #createodbcdatetime(endDateTime)#) 
	and dnis IN (select distinct opsDNIS from ops_dnis_lookup) 
</cfquery>

<cfset index = 0>
<cfset keepLooping = true>
<cfset currDateTime = startDateTime>

<cfset colList = "timePeriod,totalCalls,avgAnswerSpeed,tier1,tier2,tier3,tier4,abandoned,avgAbandonedTime,abandonedGT30">
<cfset headerList = "Time Period,Total Calls,Average Answer Speed (in seconds),15 sec or less,16-30 sec,31-60 sec, greater than 60 sec,number abandoned,Avg Abandoned Time (in seconds),Abandoned greather than 30 seconds">
<cfset typeList = "VarChar,VarChar,VarChar,VarChar,VarChar,VarChar,VarChar,VarChar,VarChar,VarChar">

<cfset qCalls = querynew(colList,typeList)>

<cfoutput>
<cfloop condition="#keepLooping#">
	<cfset currDateTime = dateadd("n",15,currDateTime)>
	<cfif ("#dateformat(currDateTime,'mm/dd/yyyy')# #timeformat(currDateTime,'HH:mm')#" is endDateTime)>
		<cfset keepLooping = false>
	</cfif>

	<cfquery name="getAlarms" dbtype="query">
		select *  
		from getCalls 
		where (dateTimeOccurred >= #createodbcdatetime(dateadd("n",-15,currDateTime))# and dateTimeOccurred < #createodbcdatetime(currDateTime)#) 
	</cfquery>
	
	<!--- 
	tier1 : 15 seconds or less
	tier2 : 16-30 seconds
	tier3 : 31-60 seconds
	--->

	<cfset numOfCalls = 0>
	<cfset totalAnswerSpeed = 0>
	<cfset totalAnswered = 0>
	<cfset avgAnswerSpeed = 0>
	<cfset tier1 = 0>
	<cfset tier2 = 0>
	<cfset tier3 = 0>
	<cfset tier4 = 0>
	<cfset abandoned = 0>
	<cfset avgAbandonedTime = 0>
	<cfset totalAbandonedTime = 0>
	<cfset abandonedGT30 = 0>
	<cfset totalCalls = 0>
	
	<cfloop query="getAlarms">
		<cfif conditionCode is not "G" and conditionCode is not "H" and conditionCode is not "I">
			<cfset totalCalls = totalCalls + 1>
		</cfif>
		<cfset totalDuration = 0>
		<cfset durationHrs = mid(duration,1,1)>
		<cfset durationMins = mid(duration,2,2)>
		<cfset durationSec = mid(duration,4,2)>
		<cfset totalDuration = totalDuration + durationSec + (durationMins * 60) + (durationHrs * 3600)>
		<cfif conditionCode is "G"> <!--- answered tallies --->
			<cfset totalAnswered = totalAnswered + 1>
			<cfset totalAnswerSpeed = totalAnswerSpeed + totalDuration>
			<cfif totalDuration lte 15>
				<cfset tier1 = tier1 + 1>
			<cfelseif totalDuration gt 15 and totalDuration lte 30>
				<cfset tier2 = tier2 + 1>
			<cfelseif totalDuration gt 30 and totalDuration lte 60>
				<cfset tier3 = tier3 + 1>
			<cfelse>
				<cfset tier4 = tier4 + 1>
			</cfif>
		<cfelseif conditionCode is "H"> <!--- abandoned tallies --->
			<cfset abandoned = abandoned + 1>
			<cfset totalAbandonedTime = totalAbandonedTime + totalDuration>
			<cfif totalDuration gt 30>
				<cfset abandonedGT30 = abandonedGT30 + 1>
			</cfif>
		</cfif>
	</cfloop>

	
	<cfif totalAnswered is not 0>
		<cfset avgAnswerSpeed = totalAnswerSpeed / totalAnswered> 
	<cfelse>
		<cfset avgAnswerSpeed = 0> 
	</cfif>
	<cfif abandoned is not 0>
		<cfset avgAbandonedTime = totalAbandonedTime / abandoned>
	<cfelse>
		<cfset avgAbandonedTime = 0>
	</cfif>
	<cfset bd = dateformat(dateadd("n",-15,currDateTime),'mm/dd')>
	<cfset bt = timeformat(dateadd("n",-15,currDateTime),'HH:mm')>
	<cfset ed = dateformat(currDateTime,'mm/dd')>
	<cfset et = timeformat(currDateTime,'HH:mm')>
	
	<cfset x = queryaddrow(qCalls)>
	<cfset x = querysetcell(qCalls,"timePeriod","#bd# #bt# - #ed# #et#")>
	<cfset x = querysetcell(qCalls,"totalCalls","#numberformat(totalCalls)#")>
	<cfset x = querysetcell(qCalls,"avgAnswerSpeed","#numberformat(avgAnswerSpeed)#")>
	<cfset x = querysetcell(qCalls,"tier1","#numberformat(tier1)#")>
	<cfset x = querysetcell(qCalls,"tier2","#numberformat(tier2)#")>
	<cfset x = querysetcell(qCalls,"tier3","#numberformat(tier3)#")>
	<cfset x = querysetcell(qCalls,"tier4","#numberformat(tier4)#")>
	<cfset x = querysetcell(qCalls,"abandoned","#numberformat(abandoned)#")>
	<cfset x = querysetcell(qCalls,"avgAbandonedTime","#numberformat(avgAbandonedTime)#")>
	<cfset x = querysetcell(qCalls,"abandonedGT30","#numberformat(abandonedGT30)#")>
	
</cfloop>
</cfoutput>

<cfx_query2excel 
	file="#outputfilepath#\#XLSFileName#" 
	headings="#headerList#" 
	queryFields="#colList#" 
	format="excel" 
	query="#qCalls#">

Done.  

