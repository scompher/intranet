
<cfset toEmail = "SScrivana@copsmonitoring.com,hsparks@copsmonitoring.com">
<cfset reportDate = dateadd("d",-1,now())>
<cfset PDFFileName = "Operations_Phone_Report_#dateformat(reportDate,'mm-dd-yyyy')#.pdf">
<cfset outputfilepath = "#request.directpath#\scheduled\smdr\reports\operations\attachments">

<cfsetting showdebugoutput="no" requesttimeout="300">

<cftry>

<cfdocument format="pdf" filename="#outputfilepath#\#PDFFileName#" orientation="portrait" scale="75" overwrite="yes">

<cfloop from="1" to="3" index="i">
	<cfswitch expression="#i#">
		<cfcase value="1">
			<cfset startDate = "#dateadd('d',-2,now())#">
			<cfset startTime = "23:00">
			<cfset startDateTime = dateformat(startDate,'mm/dd/yyyy') & " " & startTime>
			<cfset endDate = "#dateadd('d',-1,now())#">
			<cfset endTime = "07:00">
			<cfset endDateTime = dateformat(endDate,'mm/dd/yyyy') & " " & endTime>
			<cfinclude template="page.cfm">
			<cfdocumentitem type="pagebreak"></cfdocumentitem>
		</cfcase>
		<cfcase value="2">
			<cfset startDate = "#dateadd('d',-1,now())#">
			<cfset startTime = "07:00">
			<cfset startDateTime = dateformat(startDate,'mm/dd/yyyy') & " " & startTime>
			<cfset endDate = "#dateadd('d',-1,now())#">
			<cfset endTime = "15:00">
			<cfset endDateTime = dateformat(endDate,'mm/dd/yyyy') & " " & endTime>
			<cfinclude template="page.cfm">
			<cfdocumentitem type="pagebreak"></cfdocumentitem>
		</cfcase>
		<cfcase value="3">
			<cfset startDate = "#dateadd('d',-1,now())#">
			<cfset startTime = "15:00">
			<cfset startDateTime = dateformat(startDate,'mm/dd/yyyy') & " " & startTime>
			<cfset endDate = "#dateadd('d',-1,now())#">
			<cfset endTime = "23:00">
			<cfset endDateTime = dateformat(endDate,'mm/dd/yyyy') & " " & endTime>
			<cfinclude template="page.cfm">
			<cfdocumentitem type="pagebreak"></cfdocumentitem>
		</cfcase>
	</cfswitch>
</cfloop>

</cfdocument>

<cfinclude template="excelReport.cfm">

<cfmail from="system.info@copsmonitoring.com" to="#toEmail#" subject="Operations_Phone_Report_#dateformat(reportDate,'mm-dd-yyyy')#" username="system" password="V01c3">
Report attached
<cfmailparam file="attachments\#PDFFileName#">
<cfmailparam file="attachments\#XLSFileName#">
</cfmail>

done.
<cfcatch type="any">
<cfmail from="system.info@copsmonitoring.com" to="#toEmail#" subject="ERROR REPORT FAILED: Operations_Phone_Report_#dateformat(reportDate,'mm-dd-yyyy')#" username="system" password="V01c3">
Operations SMDR Report failed! 

#cfcatch.Detail#
#cfcatch.Message#
</cfmail>
<cfoutput>
#cfcatch.Detail#<br />
#cfcatch.Message#<br />
</cfoutput>
</cfcatch>
</cftry>

