
<cfset runtime = CreateObject("java","java.lang.Runtime").getRuntime()>
<cfset freeMemory = runtime.freeMemory() / 1024 / 1024>
<cfset totalMemory = runtime.totalMemory() / 1024 / 1024>
<cfset maxMemory = runtime.maxMemory() / 1024 / 1024>
<cfset memoryInUse = Round(totalMemory-freeMemory)>

<cfoutput>
    Free Allocated Memory: #numberformat(Round(freeMemory))# MB<br>
    Total Memory Allocated: #numberformat(Round(totalMemory))# MB<br>
    Max Memory Available to JVM: #numberformat(Round(maxMemory))# MB<br>
    Memory In Use: #numberformat(memoryInUse)# MB<br>
</cfoutput>


<cfif round(memoryInUse) gte 1800>

<cfmail from="pgregory@copsmonitoring.com" to="pgregory@copsmonitoring.com" subject="Server low on memory alert - restarting server" username="copalink@copsmonitoring.com" password="copsmoncal">
INTRANET 
Restarting server - memory usage too high:

Free Allocated Memory: #numberformat(Round(freeMemory))# MB
Total Memory Allocated: #numberformat(Round(totalMemory))# MB
Max Memory Available to JVM: #numberformat(Round(maxMemory))# MB
Memory In Use: #numberformat(memoryInUse)# MB

http://192.168.107.10/utilities/restartlivecfmx.cfm
</cfmail>

<cfexecute name="c:\java\restart.bat" outputfile="d:\java\restartlog.txt" timeout="10"></cfexecute>

</cfif>
