<!--- 
<cfset ed = "5/24/2007">
<cfset sd = createodbcdate(dateadd("d", -7, ed))>
--->
<cfset sd = createodbcdate(sd)>
<cfset ed = dateadd("d",1,ed)>
<cfset ed = createodbcdate(ed)>

<cfquery name="getEmployees" datasource="#ds#">
	select * 
	from admin_users
	inner join Admin_Users_Departments_Lookup on admin_users.adminuserid = Admin_Users_Departments_Lookup.adminuserid
	where Admin_Users_Departments_Lookup.departmentid = 1 and 
	<cfif adminuserid is not 0>admin_users.adminuserid = #adminuserid# and </cfif>
	1=1
</cfquery>

<!--- define tab char --->
<cfset tab = chr(9)>

<!--- create headers --->
<cfset headers = "Employee Name" & tab & "Date" & tab & "Time Span" & tab & "Hours" & tab & "Category" & tab & "Task" & tab & "Project" & tab &  "Status" & tab & "Department" & tab & "Requestor" & tab & "Description">

<!--- create content --->
<cfset content = "">
<cfloop query="getEmployees">
	<cfset empName = getEmployees.firstname & " " & getEmployees.lastname>
	<cfquery name="getTime" datasource="#ds#">
		select 
		timesheet_main.*, 
		timesheet_categories.category, 
		timesheet_actions.actionName,
		timesheet_systems.systemName,
		timesheet_projects.projectName,
		timesheet_status.statusName,
		Admin_Users_Departments.department
		from timesheet_main
		left join timesheet_categories on timesheet_main.categoryid = timesheet_categories.categoryid 
		left join timesheet_actions on timesheet_main.actionid = timesheet_actions.actionid
		left join timesheet_systems on timesheet_main.systemid = timesheet_systems.systemid
		left join timesheet_projects on timesheet_main.projectid = timesheet_projects.projectid
		left join timesheet_status on timesheet_main.statusid = timesheet_status.statusid
		left join Admin_Users_Departments on timesheet_main.departmentid = Admin_Users_Departments.departmentid
		where timesheet_main.entrydate >= #sd# and timesheet_main.entrydate < #ed# and timesheet_main.adminuserid = #getEmployees.adminuserid#
		order by timesheet_main.entryDate asc, timesheet_main.startTime asc
	</cfquery>
	<cfset emp = "">
	<cfif gettime.recordcount gt 0>
		<cfset totalTime = 0.00>
		<cfoutput query="getTime">
			<cfset totalTime = totalTime + hours>
			<cfset emp = emp & empName & tab & dateformat(entryDate, 'mm/dd/yyyy') & tab & "#timeformat(startTime, 'hh:mm tt')# - #timeformat(stopTime, 'hh:mm tt')#" & tab & decimalformat(hours) & tab>
			<cfif trim(category) IS NOT ""><cfset cat = category><cfelse><cfset cat = "None"></cfif>
			<cfset emp = emp & cat & tab>
			<cfif trim(actionName) IS NOT ""><cfset actName = actionName><cfelse><cfset actName = "None"></cfif>
			<cfset emp = emp & actName & tab>
			<cfif trim(projectName) IS NOT ""><cfset pname = projectName><cfelse><cfset pname = "None"></cfif>
			<cfset emp = emp & pname & tab>
			<cfif trim(statusName) IS NOT ""><cfset sname = statusName><cfelse><cfset sname = "None"></cfif>
			<cfset emp = emp & sname & tab>
			<cfif trim(department) IS NOT ""><cfset dept = department><cfelse><cfset dept = "None"></cfif>
			<cfset emp = emp & dept & tab>
			<cfset emp = emp & requestor & tab>
			<cfset desc = description>
			<cfset desc = replace(desc, "#chr(13)#", "", "all")>
			<cfset desc = replace(desc, "#chr(10)#", "", "all")>
			<cfset desc = replace(desc, "#chr(9)#", "", "all")>
			<cfset emp = emp & trim(desc) & chr(13)>
		</cfoutput>
	<cfset content = content & emp & chr(13)>
	</cfif>
</cfloop>

<!--- create file --->
<cfset fileContent = headers & chr(13) & content>


