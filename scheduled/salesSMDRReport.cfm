
<cfset beginDate = dateadd("d",-7,now())>
<cfset endDate = now()>
<cfset startDateTime = "#dateformat(beginDate,'mm/dd/yyyy')# 00:00:00">
<cfset endDateTime = "#dateformat(endDate,'mm/dd/yyyy')# 23:59:59">

<cfset salesList = "1254,1256,1257,1261">
<cfset fileLocation = "#request.directpath#\scheduled\attachments">
<cfset fileName = "#dateformat(endDate, 'yyyymmdd')#_smdrSalesReport.pdf">

<cfdocument format="pdf" scale="100" filename="#fileLocation#\#fileName#" overwrite="yes">
<link rel="stylesheet" type="text/css" href="/styles.css">

<cffunction name="formatPhone" returntype="string">
	<cfargument name="inPhone" required="yes">
	<cfset formattedPhone = inPhone>
	<cfif len(formattedPhone) is 11>
		<cfset formattedPhone = mid(formattedPhone,1,1) & "-" & mid(formattedPhone,2,3) & "-" & mid(formattedPhone,5,3) & "-" & mid(formattedPhone,8,4)>
	<cfelseif len(formattedPhone) is 10>
		<cfset formattedPhone = mid(formattedPhone,1,3) & "-" & mid(formattedPhone,4,3) & "-" & mid(formattedPhone,7,4)>	
	</cfif>
	<cfreturn formattedPhone>
</cffunction>

<cffunction name="getIncomingCalls" access="private" returntype="query">
	<cfargument name="extension" type="string" required="yes">
	
	<cfquery name="getIncoming" datasource="smdr">
		select dbo.smdrData.itemid, left(duration,1) + ':' + substring(duration,2,2) + ':' + right(duration,2) as callduration, duration, dbo.smdrData.dateTimeOccurred, dbo.smdrData.dialedNumber, dbo.smdrData.callingNumber, dbo.smdrData.accessCodeDialed 
		from smdrData with (nolock) 
		where 
		dateTimeOccurred >= #createOdbcDateTime(startDateTime)# and dateTimeOccurred <= #createOdbcDateTime(endDateTime)# and 
		dialedNumber = '#extension#' and 
		1=1 
		order by dialedNumber, dateTimeOccurred 
	</cfquery>
	
	<cfreturn getIncoming>
</cffunction>

<cffunction name="getOutgoingCalls" access="private" returntype="query">
	<cfargument name="extension" type="string" required="yes">
	
	<cfquery name="getOutgoing" datasource="smdr">
		select dbo.smdrData.itemid, left(duration,1) + ':' + substring(duration,2,2) + ':' + right(duration,2) as callduration, duration, dbo.smdrData.dateTimeOccurred, dbo.smdrData.dialedNumber, dbo.smdrData.callingNumber, dbo.smdrData.accessCodeDialed 
		from smdrData with (nolock) 
		where 
		dateTimeOccurred >= #createOdbcDateTime(startDateTime)# and dateTimeOccurred <= #createOdbcDateTime(endDateTime)# and 
		callingNumber = '#extension#' and 
		1=1 
		order by callingNumber, dateTimeOccurred 
	</cfquery>
	
	<cfreturn getOutgoing>
</cffunction>

<cffunction name="outputResults" access="private" returntype="void" output="true">
	<cfargument name="getResults" type="query" required="yes">

	<cfoutput query="getResults">
	<cfset callDurationInSeconds = 0>
	<cfset callDurationInSeconds = callDurationInSeconds + (left(getResults.duration,1) * 3600)>
	<cfset callDurationInSeconds = callDurationInSeconds + (mid(getResults.duration,2,2) * 60)>
	<cfset callDurationInSeconds = callDurationInSeconds + right(getResults.duration,2)>
	<cfif getResults.currentRow mod 2 is 0><cfset bgc = "##EEEEEE"><cfelse><cfset bgc = "##FFFFFF"></cfif>
	<tr bgcolor="#bgc#">
		<td class="linedrow">#getResults.currentRow#.</td>
		<td class="linedrow">
		<cfif accessCodeDialed is not "">
		Outgoing
		<cfelse>
		Incoming
		</cfif>
		</td>
		<td class="linedrow">#dateformat(datetimeoccurred,'mm/dd/yyyy')# #timeformat(datetimeoccurred,'HH:mm')#&nbsp;</td>
		<td class="linedrow">
		#callDuration#&nbsp;
		</td>
		<td class="linedrow">
		#callDurationInSeconds#&nbsp;
		</td>
		<td class="linedrow">
		#formatPhone(dialedNumber)#
		</td>
		<td class="linedrow">
		#formatPhone(callingNumber)#&nbsp;
		</td>
	</tr>
	</cfoutput>
</cffunction>

<table width="725" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>SMDR SALES REPORT FROM <cfoutput>#dateformat(beginDate, 'mm/dd/yyyy')# to #dateformat(endDate, 'mm/dd/yyyy')#</cfoutput></b></td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td valign="bottom" class="linedrow">&nbsp;</td>
					<td valign="bottom" class="linedrow"><b>Type</b></td>
					<td valign="bottom" class="linedrow"><b>Date/Time</b></td>
					<td valign="bottom" class="linedrow"><b>Duration</b><br />(hh:mm:ss)</td>
					<td valign="bottom" class="linedrow"><b>Duration</b><br />(in seconds)</td>
					<td valign="bottom" class="linedrow"><b>Dialed #</b></td>
					<td valign="bottom" class="linedrow"><b>Calling #</b></td>
				</tr>
				<cfloop list="#salesList#" index="extension">
				<tr>
					<td colspan="7" class="linedrow"><b>Incoming Calls for <cfoutput>#extension#</cfoutput></b></td>
				</tr>
				<cfset getIncoming = getIncomingCalls(extension)>
				<cfoutput>#outputResults(getIncoming)#</cfoutput>
				<tr>
					<td colspan="7" class="linedrow"><b>Outgoing Calls for <cfoutput>#extension#</cfoutput></b></td>
				</tr>
				<cfset getOutgoing = getOutGoingCalls(extension)>
				<cfoutput>#outputResults(getOutgoing)#</cfoutput>
				<tr><td colspan="7" class="linedrow">&nbsp;</td></tr>
				</cfloop>
			</table>
		</td>
	</tr>
</table>
</cfdocument>

<cfset tolist = "JHausler@copsmonitoring.com">

<cfmail from="pgregory@copsmonitoring.com" to="#tolist#" subject="Daily SMDR Sales Report from #dateformat(beginDate, 'mm/dd/yyyy')# to #dateformat(endDate, 'mm/dd/yyyy')#">
<cfmailparam file="attachments/#fileName#">
</cfmail>

fini.