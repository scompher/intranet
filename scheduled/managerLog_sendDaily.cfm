<!--- Managers Log Send Daily Email Report --->

<cfset today = now()>
<cfset yesterday = dateadd("d",-1,now())>

<cfquery name="GetLog" datasource="#ds#" >
SELECT managerLog_logEntries.*
	FROM managerLog_logEntries 
	WHERE dateTimeEntered >= #createodbcdate(yesterday)# and dateTimeEntered < #createodbcdate(today)#
</cfquery>

<cfquery name="GetEmailAddr" datasource="#ds#" >
SELECT managerLog_emailNotifications.emailAddress
	FROM managerLog_emailNotifications
</cfquery>

<cfloop query="GetEmailAddr">
<cfmail from="reports@copsmonitoring.com" to="#GetEmailAddr.emailAddress#" subject="Managers Log #dateformat(yesterday,'mm/dd/yyyy')#" type="html" username="copalink@copsmonitoring.com" password="copsmoncal">
<style type="text/css">
.normal {
	font-family:Arial, Helvetica, sans-serif;
	font-size: 12px;
}
</style>
<div class="normal">

<cfloop query="GetLog">

<cfquery name="GetAppendLog" datasource="#ds#">
	SELECT managerLog_appendedLogEntries.*, Admin_Users.firstname, Admin_Users.lastname
	FROM managerLog_appendedLogEntries
	LEFT JOIN Admin_Users on managerLog_appendedLogEntries.adminUserID = Admin_Users.adminuserid
	WHERE managerLog_appendedLogEntries.logID = #GetLog.logid#
</cfquery>

<cfoutput>
<b>Entry Information: </b>
<br />
Created #trim(dateformat(GetLog.dateTimeEntered,'mm/dd/yyyy'))# #TimeFormat(GetLog.dateTimeEntered,'hh:mm tt')# by #GetLog.firstname# #GetLog.lastname#
<br />
<cfif trim(GetLog.receiver) is not "" or trim(GetLog.location) is not "">
		Ref Account: #GetLog.receiver#-#GetLog.location#
	<br />	
</cfif>

<cfif #GetLog.subject# is "" or #GetLog.subject# is "Other">
	Subject: #GetLog.othersubject#
<cfelse>
	Subject: #GetLog.subject#
</cfif>
<br />
Note: 
#replace(GetLog.note, chr(13), "<br />", "all")#

</cfoutput>
<br />
<cfif GetAppendLog.recordcount gt 0>
 	<br />
	<b>Additional Information: </b>
<!---<cfoutput query="GetAppendLog">--->
<cfloop query="GetAppendLog">
<br />
	<cfif GetAppendLog.currentrow gt 1>
		<br />
	</cfif>
Created #trim(DateFormat(GetAppendLog.entryDateTime,'mm/dd/yyyy'))# #TimeFormat(GetAppendLog.entryDateTime,'hh:mm tt')# by: #GetAppendLog.firstname# #GetappendLog.lastname# 
<br />
#replace(GetAppendLog.entryText, chr(13), "<br />", "all")#
<!---</cfoutput>--->
</cfloop>
<br />
</cfif>
------------------------------------------------------------------------------------
<br />
</cfloop>

</div>
</cfmail>
</cfloop>