

<cfset today = createodbcdate(now())>
<cfset compareDate = dateadd("h",-48,today)>

<cfquery name="getOverdueTickets" datasource="#ds#">
	select 
		troubleTicket_tickets.*, 
		troubleTicket_sites.siteLabel, 
		troubleTicket_departments.departmentLabel, 
		troubleTicket_equipment.equipmentLabel,
		troubleTicket_equipment.serviceTagNumber, 
		troubleTicket_components.componentid, 
		troubleTicket_components.componentLabel, 
		troubleTicket_status.statusLabel, 
		reportedUsers.firstname + ' ' + reportedUsers.lastname as reportedUser, 
		troubleTicket_components.componentCategoryID 
	from troubleTicket_tickets 
	left join admin_users as reportedUsers on troubleTicket_tickets.whoReportedID = reportedUsers.adminuserid 
	left join troubleTicket_departments on troubleTicket_tickets.departmentid = troubleTicket_departments.departmentid 
	left join troubleTicket_sites on troubleTicket_departments.siteid = troubleTicket_sites.siteid 
	left join troubleTicket_equipment on troubleTicket_tickets.equipmentid = troubleTicket_equipment.equipmentid 
	left join troubleTicket_components on troubleTicket_tickets.componentid = troubleTicket_components.componentid 
	left join troubleTicket_status on troubleTicket_tickets.currentStatusID = troubleTicket_status.statusid 
	where 
	troubleTicket_tickets.currentStatusID = 0 and troubleTicket_tickets.dateTimeReported <= #createodbcdatetime(compareDate)#
</cfquery>

<cfloop query="getOverdueTickets">
<cfquery name="getDepartmentEmail" datasource="#ds#">
	select * from troubleTicket_notificationEmails 
	where departmentID = #getOverdueTickets.departmentid# 
</cfquery>
<cfif trim(getDepartmentEmail.uponOverdue) is not "">
<cfmail priority="2" from="system.info@copsmonitoring.com" to="#getDepartmentEmail.uponOverdue#" subject="A trouble ticket created for #getOverdueTickets.siteLabel# - #getOverdueTickets.departmentLabel# is now overdue for pickup.">
A trouble ticket created on #dateformat(getOverdueTickets.dateTimeReported,'mm/dd/yyyy')# #timeformat(getOverdueTickets.dateTimeReported,'hh:mm tt')# is now overdue for pickup. 

Department: #getOverdueTickets.siteLabel# - #getOverdueTickets.departmentLabel#

Equipment: #getOverdueTickets.equipmentLabel#

Component: #getOverdueTickets.componentLabel# 

Problem Description: 
#getOverdueTickets.problemDescription# 

</cfmail>
</cfif>
</cfloop>
