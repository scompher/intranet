
<!---
1/3/2013 : PG : this script removes temporary played recordings from the previous day then removes the empty directory 
--->

<cfset yesterday = dateadd("d",-1,now())>

<cfdirectory action="list" directory="#request.directpath#\recordings\temp\" name="getDirs" type="dir">

<cfloop query="getDirs">
	<cfset dirName = getDirs.name>
	<cfdirectory action="list" directory="#request.directpath#\recordings\temp\#dirName#" name="getFiles" type="file">
	<cfloop query="getFiles">
		<cfset fileName = getFiles.name>
		<cfif getFiles.dateLastModified lte dateformat(yesterday,'mm/dd/yyyy')>
			<cffile action="delete" file="#request.directpath#\recordings\temp\#dirName#\#fileName#">
		</cfif>
	</cfloop>
	<cfdirectory action="list" directory="#request.directpath#\recordings\temp\#dirName#" name="checkForEmpty" type="file">
	<cfif checkForEmpty.recordcount is 0>
		<cfdirectory action="delete" directory="#request.directpath#\recordings\temp\#dirName#">
	</cfif>
</cfloop>

Done
