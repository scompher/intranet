
<cfset today = createodbcdate(now())>

<cfquery name="getTasks" datasource="#ds#">
	select tasktracker_tasks.*, admin_users.email
	from tasktracker_tasks
	inner join admin_users on tasktracker_tasks.originatorID = admin_users.adminuserid
	inner join tasktracker_task_user_lookup on tasktracker_tasks.taskid = tasktracker_task_user_lookup.taskid
	where tasktracker_tasks.active <> 0 and tasktracker_task_user_lookup.statusid NOT IN (1,5) and tasktracker_tasks.duedate < #today#
</cfquery>

<cfloop query="gettasks">
	<cfquery name="getusers" datasource="#ds#">
		select admin_users.firstname, admin_users.lastname
		from tasktracker_task_user_lookup
		inner join admin_users on tasktracker_task_user_lookup.adminuserid = admin_users.adminuserid
		where taskid = #gettasks.taskid#
	</cfquery>
<cfmail from="pgregory@copsmonitoring.com" to="#gettasks.email#" subject="Overdue Task Notification" username="copalink@copsmonitoring.com" password="copsmoncal">
The following task is overdue:

#gettasks.title#
Due Date: #dateformat(gettasks.duedate,'mm/dd/yyyy')#

Users assigned to this task are:
<cfloop query="getusers">
#getusers.firstname# #getusers.lastname#
</cfloop>

View this task : #request.appurl#tasktracker/tasks/view.cfm?id=#gettasks.taskid#

</cfmail>
</cfloop>

