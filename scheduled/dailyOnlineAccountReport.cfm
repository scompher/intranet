
<cfsetting requesttimeout="60">

<cfset theDate = now()>
<cfset prevDay = dateAdd("d",-1,theDate)>


<cfset recipientList = "pgregory@copsmonitoring.com">
<cfset ds = "sai">
<cfset outputfilepath = "e:\websites\intranet\scheduled\attachments">
<cfset outputfilename = "daily_online_acct_report_#dateformat(theDate,'yyyymmdd')#.txt">
<cfset reportHeader = "">

<cfset headerlist = "cs_no,sec_cs_no,name,addr1,inst_no">
<cfset colList = "cs_no,sec_cs_no,name,addr1,inst_no">
<!--- 
<cfstoredproc datasource="#ds#" procedure="sp_GETACTIVEACCOUNTS">
	<cfprocresult name="getData">
</cfstoredproc>
 --->

<cfquery name="getData" datasource="#ds#">
	select sai.sai_account.cs_no, sai.sai_account.sec_cs_no, sai.sai_account.name, sai.sai_account.addr1, sai.sai_account.inst_no
	from sai.sai_account
	where sai.sai_account.cs_no not in (select sai.SAI_Out_of_Service.cs_no from sai.SAI_Out_of_Service)
	order by sai.sai_account.inst_no, sai.sai_account.cs_no 
</cfquery>

<cfx_query2excel 
	file="#outputfilepath#\#outputfilename#" 
	headings="#headerlist#" 
	queryFields="#colList#" 
	query="#getData#" 
	format="excel" 
	reportHeader="#reportHeader#">

<cfset un = "root">
<cfset pw = "deviler13">
<cfset svr = "192.168.3.10">

<cfftp action="putfile" localfile="#outputfilepath#\#outputfilename#" remotefile="/cpm/sai/compare/#outputfilename#" username="#un#" password="#pw#" server="#svr#">

<!--- 
<cfmail from="reports@copsmonitoring.com" to="#recipientList#" subject="Daily SAI Online Accounts Report " username="copalink@copsmonitoring.com" password="copsmoncal">
<cfmailparam file="attachments\#outputfilename#">
</cfmail>
--->

Done!