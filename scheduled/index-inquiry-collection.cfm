
<cfquery name="getInquiries" datasource="#ds#">
	select 
	inquiries_main.inquiryid, 
	inquiries_main.inquiryText, 
	inquiries_main.subscriberName, 
	inquiries_main.dealerName, 
	inquiries_main.personInquiring, 
	inquiries_main.otherCategory, 
	inquiries_research.researchText, 
	inquiries_conclusion.conclusionText, 
	inquiries_resolution.resolutionText, 
	inquiries_categories.category 
	from inquiries_main
	left join inquiries_research on inquiries_main.inquiryid = inquiries_research.inquiryid
	left join inquiries_conclusion on inquiries_main.inquiryid = inquiries_conclusion.inquiryid 
	left join inquiries_resolution on inquiries_main.inquiryid = inquiries_resolution.inquiryid
	left join inquiries_categories on inquiries_main.categoryid = inquiries_categories.categoryid 
	where inquiries_main.dateTimeCreated >= '01/01/2012' 
</cfquery>

<cfindex 
	collection="intranet_inquiries" 
	action="REFRESH" 
	query="getInquiries" 
	key="inquiryid" 
	body="subscriberName, dealerName, personInquiring, inquiryText, researchText, conclusionText, resolutionText, category, otherCategory" 
	custom1="/inquiry/openInquiry.cfm?i=">

fini