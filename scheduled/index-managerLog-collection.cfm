
<cfsetting showdebugoutput="no" requesttimeout="600">

<cfquery name="getLogEntries" datasource="#ds#">
	select managerLog_logEntries.logID, managerLog_logEntries.note, managerLog_logEntries.subject, managerLog_appendedLogEntries.entryText, managerLog_logEntries.firstname, managerLog_logEntries.lastname 
	from managerLog_logEntries 
	left join managerLog_appendedLogEntries on managerLog_logEntries.adminuserid = managerLog_appendedLogEntries.adminuserid 
</cfquery>

<cfindex 
	collection="intranet_managerLog" 
	action="REFRESH" 
	query="getLogEntries" 
	key="logid" 
	body="subject, note, entryText, firstname, lastname" 
	custom1="/managerLog/viewEntry.cfm?logid=">

fini
