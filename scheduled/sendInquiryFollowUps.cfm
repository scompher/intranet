
<cfset today = createodbcdate(now())>

<!--- get follow-ups --->
<cfquery name="getFollowUPs" datasource="#ds#">
	select 
	inquiries_followUp.*, 
	admin_users.*,  
	inquiries_main.inquiryNumber
	from inquiries_followUp
	inner join admin_users on inquiries_followUp.adminuserid = admin_users.adminuserid
	inner join inquiries_main on inquiries_followUp.inquiryid = inquiries_main.inquiryid
	where inquiries_followUp.followUpDate = #today# and inquiries_followUp.sent = 0
</cfquery>

<cfif getFollowUps.recordcount gt 0>

<cfset sentlist = "">
<cfloop query="getFollowUps">

<cfmail from="pgregory@copsmonitoring.com" to="#email#" subject="Inquiry Follow Up Reminder">
#firstname#,

This is to remind you of your scheduled follow up on inquiry ###inquiryNumber#.

Your follow-up notes:

#followUpNote#

</cfmail>

<cfset sentlist = listappend(sentlist,followUpID)>

</cfloop>

<cfquery name="updateSent" datasource="#ds#">
	update inquiries_followUp
	set sent = 1
	where followUpID in (#sentlist#)
</cfquery>

</cfif>

<cfoutput>
Done : #getFollowUps.recordcount# follow-ups sent out.
</cfoutput>
