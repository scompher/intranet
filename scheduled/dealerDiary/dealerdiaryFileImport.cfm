<cfsetting requesttimeout="3000">

<!---
should receive in file:
dealer number
company name
phone 1
phone 2
acct manager
contact person
invoice information (if any)
--->

<cfinclude template="getFile.cfm">

<cfset abortProcess = false>

<!--- get accounting dept id --->
<cfquery name="getaccountingid" datasource="#ds#">
	select departmentid from admin_users_departments
	where department = 'Accounting'
</cfquery>

<cfset filepath = "#request.directpath#\dealerdiary\ftp\LocalUser\dealerdiary">

<cfif not fileExists("#filepath#\diary.ftp")>
	<cfmail from="pgregory@copsmonitoring.com" to="pgregory@copsmonitoring.com" subject="dealerdiary ftp error - ftp file not found">
		The ftp file was not found, process aborted.
	</cfmail>
	<cfset abortProcess = true>
</cfif>

<cfif abortProcess is false>
	
	<cffile action="read" file="#filepath#\diary.ftp" variable="contents">
	
	<cfoutput>
	<cfloop index="line" list="#contents#" delimiters="#chr(13)#">
	<cfset line = trim(line)>
	<cfif line is not "">
		<cfset dealernumber = trim(replace(listgetat(line,1,"|"),"#chr(34)#","","all"))>
		<cfset dealername = trim(replace(listgetat(line,2,"|"),"#chr(34)#","","all"))>
		<cfset primaryphone = trim(replace(listgetat(line,3,"|"),"#chr(34)#","","all"))>
		<cfset secondaryphone = trim(replace(listgetat(line,4,"|"),"#chr(34)#","","all"))>
		<cfset acctmanager = trim(replace(listgetat(line,5,"|"),"#chr(34)#","","all"))>
		<cfset contact = trim(replace(listgetat(line,6,"|"),"#chr(34)#","","all"))>
		<cfset relation = trim(replace(listgetat(line,7,"|"),"#chr(34)#","","all"))>
		<cfset numaccts = trim(replace(listgetat(line,8,"|"),"#chr(34)#","","all"))>
		<cfset masterdealer = trim(replace(listgetat(line,9,"|"),"#chr(34)#","","all"))>
		<cfif listlen(line,"|") gte 10>
			<cfset invoice = trim(replace(listgetat(line,10,"|"),"#chr(34)#","","all"))>
			<cfif listlen(invoice,"^") gte 3>
				<cfset subject = listgetat(invoice,3,"^")>
				<cfset subject = subject & "<br>" & listgetat(invoice,2,"^")>
				<cfset subject = subject & "<br>" & listgetat(invoice,1,"^")>
				<cfset invoice =  replace(invoice, "^", "<br />", "all")>
				<cfset invoice = "<pre>" & invoice & "</pre>">
			<cfelse>
				<cfset subject = "">
				<cfset invoice = "">
			</cfif>
		<cfelse>
			<cfset invoice = "">
		</cfif>
	
		<cfset created = createodbcdatetime(now())>
	
		<!--- if dealer is in system, update info. If not, add --->
		<cfquery name="updatedealerinfo" datasource="#ds#">
			if not exists (select dealerid from dealerdiary_dealers where dealernumber = '#dealernumber#')
				begin
					insert into dealerdiary_dealers (dealernumber, dealername, primaryphone, secondaryphone, acctmanager, contact, masterDealer, numAccounts, managerEmail)
					values ('#dealernumber#', '#dealername#', '#primaryphone#', '#secondaryphone#', '#acctmanager#', '#contact#', '#masterdealer#','#numaccts#','#relation#')
				end
			else
				begin
					update dealerdiary_dealers
					set dealernumber = '#dealernumber#', dealername = '#dealername#', primaryphone = '#primaryphone#', secondaryphone = '#secondaryphone#', acctmanager = '#acctmanager#', contact = '#contact#', masterDealer = '#masterDealer#', numAccounts = '#numaccts#', managerEmail = '#relation#'
					where dealernumber = '#dealernumber#'
				end
			<cfif trim(invoice) is not "">
				begin
					insert into dealerdiary_entries (creatorID, dealernumber, created, content, subject, departmentid, parentid)
					values (0, '#dealernumber#', #created#, '#variables.invoice#', '#variables.subject#', #getaccountingid.departmentid#, 0)
				end
			</cfif>
		</cfquery>
	</cfif>
	</cfloop>
	</cfoutput>

	<cfif fileExists("#filepath#\diary.ftp")>
		<cffile action="delete" file="#filepath#\diary.ftp">
	</cfif>
	
	finished update

</cfif>
