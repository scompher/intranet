
<cfset toList = "MSteigerwalt@copsmonitoring.com,jmcmullen@copsmonitoring.com">
<!--- <cfset toList = "pgregory@copsmonitoring.com"> --->

<!--- employee percentage daily --->
<cfset dailyPercentageUrlAddress = "#request.AppURL#timesheets/reports/employeePercentageDailyXLS.cfm">
<cfset dailyPercentageFilename = "#dateformat(now(),'yyyymmdd')#DailyPercentage.xls">

<cfset ed = dateformat(now(), 'mm/dd/yyyy')>

<cfhttp method="post" url="#dailyPercentageUrlAddress#" throwonerror="yes">
	<cfhttpparam name="ed" type="formfield" value="#ed#">
</cfhttp>

<cffile action="write" file="#request.DirectPath#\scheduled\attachments\#dailyPercentageFilename#" output="#cfhttp.FileContent#">
<!--- employee percentage daily --->

<!--- daily timesheet detail --->
<cfset dailyDetailUrlAddress = "#request.AppURL#timesheets/reports/showAll.cfm">
<cfset dailyDetailFilename = "#dateformat(now(),'yyyymmdd')#DailyTimesheetDetail.xls">

<!--- 
<cfhttp method="post" url="#dailyDetailUrlAddress#" throwonerror="yes">
	<cfhttpparam name="sd" type="formfield" value="#dateformat(dateadd("d",-1,now()), 'mm/dd/yyyy')#">
	<cfhttpparam name="ed" type="formfield" value="#dateformat(dateadd("d",-1,now()), 'mm/dd/yyyy')#">
	<cfhttpparam name="adminuserid" type="formfield" value="0">
</cfhttp>
 --->

<cfset sd = "#dateformat(now(), 'mm/dd/yyyy')#">
<cfset ed = "#dateformat(now(), 'mm/dd/yyyy')#">
<cfset adminuserid = 0>

<cfinclude template="showAll.cfm">

<cffile action="write" file="#request.DirectPath#\scheduled\attachments\#dailyDetailFilename#" output="#FileContent#">
<!--- daily timesheet detail --->

<!--- email reports --->
<cfmail from="pgregory@copsmonitoring.com" to="#toList#" subject="Timesheet Reports for #dateformat(now(), 'mm/dd/yyyy')#" username="copalink@copsmonitoring.com" password="copsmoncal">
<cfmailparam file="attachments\#dailyPercentageFilename#">
<cfmailparam file="attachments\#dailyDetailFilename#">

The daily timesheet percentage report  and daily timesheet detail report attached.

</cfmail>

<!--- email reports --->
<!--- 
<cfif fileExists("#request.DirectPath#\scheduled\attachments\#dailyPercentageFilename#")>
	<cffile action="delete" file="#request.DirectPath#\scheduled\attachments\#dailyPercentageFilename#">
</cfif>

<cfif fileExists("#request.DirectPath#\scheduled\attachments\#dailyDetailFilename#")>
	<cffile action="delete" file="#request.DirectPath#\scheduled\attachments\#dailyDetailFilename#">
</cfif> 
--->

done
