﻿using System;
using System.Collections.Generic;
using System.Deployment.Internal.CodeSigning;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Intranet
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            /*West Service*/
            ServicePointManager.Expect100Continue = true;            
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

            CryptoConfig.AddAlgorithm(typeof(RSAPKCS1SHA256SignatureDescription), "http://www.w3.org/2001/04/xmldsig-more#rsa-sha256");
            //ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

            AreaRegistration.RegisterAllAreas();
            //Important - the order of these lines matter.  WebApiConfig.Register needs to go after RegisterAllAreas
            //https://stackoverflow.com/questions/20621825/asp-net-mvc-webapi-404-error
            GlobalConfiguration.Configure(WebApiConfig.Register);

            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            
        }
    }
}
