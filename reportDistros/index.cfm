
<cfparam name="form.newemail" default="">
<cfparam name="form.reportid" default="0">
<cfparam name="form.action" default="">
<cfparam name="form.actionID" default="0">

<cfif isDefined("url.action")><cfset form.action = url.action></cfif>
<cfif isDefined("url.reportid")><cfset form.reportid = url.reportid></cfif>
<cfif isDefined("url.actionID")><cfset form.actionID = url.actionID></cfif>

<link rel="stylesheet" type="text/css" href="../styles.css">
<div align="center">

<cfinclude template="actions.cfm">

<form method="post" action="index.cfm">

<!--- report selection --->
<table width="600" border="0" cellspacing="0" cellpadding="5">
    <tr>
        <td class="highlightbar"><b>Report Distro List Maintenance </b></td>
    </tr>
    <tr>
        <td class="greyrowbottomnopadding">
            <table width="100%" border="0" cellspacing="0" cellpadding="5">
                <tr>
                    <td width="24%" nowrap>Please select a report: </td>
                    <td width="64%">
                        <select name="reportID" style="width:375px;">
							<option value="0">Select a Report</option>
							<cfoutput query="getReports">
								<option <cfif form.reportID is getReports.reportid>selected</cfif> value="#getReports.reportID#">#getReports.reportName#</option>
							</cfoutput>
                        </select>
                    </td>
                    <td width="12%" align="center">
                        <input name="btnSelectReport" type="submit" class="sidebar" id="btnSelectReport" value="Select">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<!--- report selection --->

<p></p>

<cfif form.reportID is not 0>
	<!--- distro maintenance --->
	<table width="600" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td class="highlightbar"><b>Distro Maintenance for: <cfoutput>#getReportName.reportName#</cfoutput></b></td>
		</tr>
		<tr>
			<td class="greyrowbottomnopadding">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td align="center" class="linedrowrightcolumn">&nbsp;</td>
						<td class="linedrow">
							<textarea name="newEmail" style="width:300px; vertical-align:top;" rows="3"></textarea>
							<!--- <input type="text" name="newEmail" style="width:300px; vertical-align:middle" value="" /> --->
							<input type="submit" name="btnAddNewEmail" style="vertical-align:top;" value="Add New E-Mail(s)" class="sidebar" />
							<br />
							<span class="smallred">Separate multiple E-Mail addresses with a comma</span>
						</td>
					</tr>
					<tr>
						<td width="9%" align="center" class="linedrowrightcolumn"><b>Action</b></td>
						<td width="91%" class="linedrow"><b>E-Mail Address </b></td>
					</tr>
					<cfoutput query="getEmails">
					<cfif form.action is "EDIT" and form.actionID is getEmails.reportEmailID>
						<input type="hidden" name="actionID" value="#form.actionID#" />
						<tr>
							<td align="center" class="linedrowrightcolumn">Editing</td>
							<td class="linedrow">
								<input type="text" name="updatedEmail" style="width:300px; vertical-align:middle" value="#getEmails.emailAddress#">
								<input name="btnUpdateEmail" type="submit" class="sidebarsmall" value="Update" style="vertical-align:middle;">
								<input name="btnCancel" type="submit" class="sidebarsmall" value="Cancel" style="vertical-align:middle;">
							</td>
						</tr>
					<cfelse>
						<tr>
							<td align="center" class="linedrowrightcolumn">
							<a href="index.cfm?action=EDIT&actionID=#getEmails.reportEmailID#&reportid=#form.reportid#">[E]</a>
							<a onclick="return confirm('Are you sure you wish to remove this E-Mail address?');" href="index.cfm?action=DELETE&actionID=#getEmails.reportEmailID#&reportid=#form.reportid#">[D]</a>
							</td>
							<td class="linedrow">#emailAddress#</td>
						</tr>
					</cfif>
					</cfoutput>
				</table>
			</td>
		</tr>
	</table>
	<!--- distro maintenance --->
</cfif>

</form>

<br />
<a class="normal" style="text-decoration:underline;" href="/index.cfm">Return to Intranet</a>

</div>

