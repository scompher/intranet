
<cfif isDefined("form.btnAddNewEmail")>
	<cfloop list="#form.newEmail#" index="currentEmail" delimiters=",">
		<cfquery name="addEmail" datasource="#ds#">
			if not exists (select * from report_distro_emails where reportid = #form.reportid# and emailAddress = '#trim(currentEmail)#')
			begin
				insert into report_distro_emails (reportID, emailAddress) 
				values (#form.reportid#, '#trim(currentEmail)#')
			end
		</cfquery>
	</cfloop>
</cfif>

<cfif isDefined("form.btnUpdateEmail")>
	<cfquery name="updateEmail" datasource="#ds#">
		update report_distro_emails 
		set emailAddress = '#trim(form.updatedEmail)#'
		where reportEmailID = #form.actionid# 
	</cfquery>
</cfif>

<cfif form.action is "DELETE">
	<cfquery name="delEmail" datasource="#ds#">
		delete from report_distro_emails 
		where reportEmailID = #form.actionid# 
	</cfquery>
</cfif>

<cfquery name="getReports" datasource="#ds#">
	select * 
	from report_distro_main 
	order by reportName asc 
</cfquery>

<cfquery name="getReportName" datasource="#ds#">
	select * 
	from report_distro_main 
	where reportID = #form.reportID# 
</cfquery>

<cfquery name="getEmails" datasource="#ds#">
	select * 
	from report_distro_emails 
	where reportid = #form.reportid# 
	order by emailAddress asc 
</cfquery>

