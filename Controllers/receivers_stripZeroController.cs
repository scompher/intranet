﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Intranet.Models;

namespace Intranet.Controllers
{
    public class receivers_stripZeroController : Controller
    {
        private copalink db = new copalink();

        // GET: receivers_stripZero
        public ActionResult Index()
        {
            return View(db.receivers_stripZero.ToList().OrderBy(i => i.receiver));
        }

        // GET: receivers_stripZero/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            receivers_stripZero receivers_stripZero = db.receivers_stripZero.Find(id);
            if (receivers_stripZero == null)
            {
                return HttpNotFound();
            }
            return View(receivers_stripZero);
        }

        // GET: receivers_stripZero/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: receivers_stripZero/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "receiverID,receiver")] receivers_stripZero receivers_stripZero)
        {
            if (ModelState.IsValid)
            {
                db.receivers_stripZero.Add(receivers_stripZero);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(receivers_stripZero);
        }

        // GET: receivers_stripZero/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            receivers_stripZero receivers_stripZero = db.receivers_stripZero.Find(id);
            if (receivers_stripZero == null)
            {
                return HttpNotFound();
            }
            return View(receivers_stripZero);
        }

        // POST: receivers_stripZero/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "receiverID,receiver")] receivers_stripZero receivers_stripZero)
        {
            if (ModelState.IsValid)
            {
                db.Entry(receivers_stripZero).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(receivers_stripZero);
        }

        // GET: receivers_stripZero/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            receivers_stripZero receivers_stripZero = db.receivers_stripZero.Find(id);
            if (receivers_stripZero == null)
            {
                return HttpNotFound();
            }
            return View(receivers_stripZero);
        }

        // POST: receivers_stripZero/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            receivers_stripZero receivers_stripZero = db.receivers_stripZero.Find(id);
            db.receivers_stripZero.Remove(receivers_stripZero);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
