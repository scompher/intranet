﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Text;
using Intranet.Models;

namespace Intranet.Controllers
{
    public class UCC2Controller : Controller
    {
        // GET: UCC2
        public ActionResult Index()
        {
            return View();
        }



        public ActionResult DownloadUCCAccounts()
        {
            
            string q = Request.QueryString["Increment"];
            bool downloadIncremenets = false;
            Boolean.TryParse(q, out downloadIncremenets);

            q = Request.QueryString["StartDate"];            
            DateTime dt;
            if (DateTime.TryParse(q, out dt))
            {
                DownloadUCCAccountsFile(downloadIncremenets, dt);
                return null;
            }
            
            return View(new UCCDownloadAccounts());
        }

        [HttpPost]
        public ActionResult DownloadUCCAccounts(UCCDownloadAccounts acc)
        {
            if (ModelState.IsValid)
            {
                DownloadUCCAccountsFile(false, acc.StartDate, acc.NoBillingID);
                return null;
            }
            return View(acc);
        }


        /// <summary>
        /// Returns a CSV file
        /// </summary>
        /// <returns></returns>        
        public void DownloadUCCAccountsFile(bool increments, DateTime startDate, bool noBillingID = false)
        {  

            bool downloadIncremenets = increments;
            int recStart = 1;
            int recInc = 49999;
            string pdt = startDate.ToString("yyyy-MM-dd");

            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Disposition", "attachment; filename=" + "uccbilling.csv"); 

            string connectionString208 = ConfigurationManager.ConnectionStrings["LydiaBilling208"].ConnectionString;
            using (SqlConnection connection = new SqlConnection(connectionString208))
            {
                connection.Open();

                while (Response.IsClientConnected)
                {
                    string sql = "";
                    if (downloadIncremenets)
                    {
                        sql = String.Format("exec csp_UCCAccounts @StartDate='{0}', @NullBillID={1}, @StartRec={2}, @Increment={3}", pdt, (noBillingID == false) ? 0 : 1, recStart, recInc);
                    }
                    else
                    {
                        sql = String.Format("exec csp_UCCAccounts @StartDate='{0}', @NullBillID={1}", pdt, (noBillingID == false) ? 0 : 1);
                    }

                        
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        int recsRead = 0;
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                recsRead++;
                                string buf = "";
                                //buf += recsRead + ",";  //Debug RowCount
                                buf += FormatForCSV(reader.GetValue(reader.GetOrdinal("sitenum"))) + ",";
                                buf += FormatForCSV(reader.GetValue(reader.GetOrdinal("sitetype"))) + ",";
                                buf += FormatForCSV(reader.GetValue(reader.GetOrdinal("sitename"))) + ",";
                                buf += FormatForCSV(reader.GetValue(reader.GetOrdinal("sitegroupnum"))) + ",";
                                buf += FormatForCSV(reader.GetValue(reader.GetOrdinal("siteid"))) + ",";
                                buf += FormatForCSV(reader.GetValue(reader.GetOrdinal("ContractItem"))) + ",";
                                buf += FormatForCSV(reader.GetValue(reader.GetOrdinal("OOSStartDate"))) + ",";
                                //buf += FormatForCSV(reader.GetValue(reader.GetOrdinal("SiteID2"))) + ",";    //Not used in original code
                                buf += FormatForCSV(reader.GetValue(reader.GetOrdinal("OOSCat"))) + ",";
                                buf += FormatForCSV(reader.GetValue(reader.GetOrdinal("sitegrouptype"))) + ",";
                                buf += FormatForCSV(reader.GetValue(reader.GetOrdinal("billingid")));
                                buf += Environment.NewLine;
                                Response.Write(buf);
                                
                                if (recsRead % 5000 == 0)
                                    Response.Flush();
                                
                            }
                        }

                        //Response.Flush();

                        if (downloadIncremenets == false)
                            return;

                        if (recsRead < recInc + 1)
                            return;

                        
                        recStart = recStart + recInc + 1;
                        
                            
                    } //command
                } //client
            } //connection
            return;
        }



        private string FormatForCSV(object obj)
        {
            try
            {
                string val = obj.ToString();
                val = val.Replace(',', ' ');
                val = val.Replace(Environment.NewLine, "");
                val = val.Replace((char)10, ' ');
                val = val.Replace((char)13, ' ');
                return val.Trim();
            }
            catch (Exception)
            {
                return "";
            }
        }



    } //class
} //ns
    