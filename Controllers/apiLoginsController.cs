﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Intranet.Models;

namespace Intranet.Controllers
{
    public class apiLoginsController : Controller
    {
        private copalink db = new copalink();

        // GET: apiLogins
        public ActionResult Index()
        {
            return View(db.apiLogins.ToList());
        }

        // GET: apiLogins/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            apiLogin apiLogin = db.apiLogins.Find(id);
            if (apiLogin == null)
            {
                return HttpNotFound();
            }
            return View(apiLogin);
        }

        // GET: apiLogins/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: apiLogins/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,login,password,active,apidoc,receiveAlarm,emailAddress,QADealerNum")] apiLogin apiLogin)
        {
            if (ModelState.IsValid)
            {
                db.apiLogins.Add(apiLogin);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(apiLogin);
        }

        // GET: apiLogins/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            apiLogin apiLogin = db.apiLogins.Find(id);
            if (apiLogin == null)
            {
                return HttpNotFound();
            }
            return View(apiLogin);
        }

        // POST: apiLogins/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,login,password,active,apidoc,receiveAlarm,emailAddress,QADealerNum")] apiLogin apiLogin)
        {
            if (ModelState.IsValid)
            {
                db.Entry(apiLogin).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(apiLogin);
        }

        // GET: apiLogins/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            apiLogin apiLogin = db.apiLogins.Find(id);
            if (apiLogin == null)
            {
                return HttpNotFound();
            }
            return View(apiLogin);
        }

        // POST: apiLogins/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            apiLogin apiLogin = db.apiLogins.Find(id);
            db.apiLogins.Remove(apiLogin);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
