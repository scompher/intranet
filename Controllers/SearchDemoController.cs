﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using System.Configuration;

namespace Intranet.Controllers
{
    public class SearchDemoController : Controller
    {
        
        public ActionResult Index()
        {
            return View(new SearchDemoRecord());
        }

        [HttpPost]
        public ActionResult Index(SearchDemoRecord query)
        {

            if (!string.IsNullOrWhiteSpace(query.Search))
            {
                var results = BasicSearch(query.Search);
                return View("Results", results);

            }
            else
            {
                var results = AdvancedSearch(query);
                return View("Results", results);
            }
        }




        public ActionResult Results(SearchDemoRecord query)
        {
            
            return View();
        }



        private List<SearchDemoRecord> AdvancedSearch(SearchDemoRecord query)
        {
            List<SearchDemoRecord> results = new List<SearchDemoRecord>();
            string sql = "with cte as (";
            sql += string.Format("SELECT COUNT(0) OVER() AS CT, ROW_NUMBER() OVER (ORDER BY SearchID) AS RN, * FROM [Search_Main] WHERE ");

            List<string> where = new List<string>();

            if (!String.IsNullOrWhiteSpace(query.Account))
                where.Add(String.Concat("Account='", query.Account.Replace("'", "''"), "'"));

            if (!String.IsNullOrWhiteSpace(query.Address)) 
                where.Add(String.Concat("Address='", query.Address.Replace("'", "''"), "'"));

            if (!String.IsNullOrWhiteSpace(query.City))
                where.Add(String.Concat("City='", query.City.Replace("'", "''"), "'"));

            if (!String.IsNullOrWhiteSpace(query.State))
                where.Add(String.Concat("State='", query.State.Replace("'", "''"), "'"));

            if (!String.IsNullOrWhiteSpace(query.Zip))
                where.Add(String.Concat("Zip='", query.Zip.Replace("'", "''"), "'"));

            if (!String.IsNullOrWhiteSpace(query.DealerNumber))
                where.Add(String.Concat("DealerNumber='", query.Zip.Replace("'", "''"), "'"));

            if (!String.IsNullOrWhiteSpace(query.Name))
                where.Add(String.Concat("Name='", query.Name.Replace("'", "''"), "'"));

            if (where.Count == 0)
                return results;

            string whereClause = string.Join(" AND ", where.ToArray<string>());

            sql += whereClause +  ") SELECT TOP 100 * FROM cte";

            return DoSearch(sql);
        }


        private List<SearchDemoRecord> BasicSearch(string searchValue)
        {
            string sql = "with cte as (";
            sql += string.Format ("SELECT COUNT(0) OVER() as CT, ROW_NUMBER() OVER (ORDER BY SearchID) AS RN, * FROM [Search_Main] WHERE Address LIKE '%{0}%' OR CITY LIKE '%{0}%' OR [Name] LIKE '%{0}%'", searchValue.Replace("'", "''"));
            sql += ") SELECT TOP 100 * FROM cte";

            return DoSearch(sql);
        }

        private List<SearchDemoRecord> DoSearch(string sql)
        {
            List<SearchDemoRecord> results = new List<SearchDemoRecord>();

            ViewBag.sql = sql;

            string connectionString = ConfigurationManager.ConnectionStrings["copalink"].ConnectionString;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                using (SqlCommand cmd = new SqlCommand(sql, connection))
                {
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            if (ViewBag.Total == null)
                                ViewBag.Total = reader.GetInt32(reader.GetOrdinal("CT"));

                            SearchDemoRecord rec = new SearchDemoRecord();
                            rec.RowNumber = reader.GetInt64(reader.GetOrdinal("RN"));
                            rec.SearchID = reader.GetInt32(reader.GetOrdinal("SearchID"));
                            rec.Account = reader.GetString(reader.GetOrdinal("Account"));
                            rec.Address = reader.GetString(reader.GetOrdinal("Address"));
                            rec.City = reader.GetString(reader.GetOrdinal("City"));
                            rec.State = reader.GetString(reader.GetOrdinal("State"));
                            rec.Zip = reader.GetString(reader.GetOrdinal("Zip"));
                            rec.Permit = reader.GetString(reader.GetOrdinal("Permit"));
                            rec.DealerNumber = reader.GetString(reader.GetOrdinal("DealerNumber"));
                            rec.VirtualAccount = reader.GetString(reader.GetOrdinal("VirtualAccount"));
                            rec.BillingID = reader.GetString(reader.GetOrdinal("BillingID"));
                            rec.Name = reader.GetString(reader.GetOrdinal("Name"));
                            results.Add(rec);
                        }
                    }
                }
            }
            return results;
        }




    } //class


    
    
    public class SearchDemoRecord
    {
        public int SearchID { get; set; }
        public Int64 RowNumber{ get; set; }
        public string Account { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Permit { get; set; }
        public string DealerNumber { get; set; }
        public string VirtualAccount { get; set; }
        public string BillingID { get; set; }
        public string Name { get; set; }
        public string Search { get; set; }
    }



}