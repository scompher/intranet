﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Net.Http.Headers;
using Cops.D3Communications;
using System.Configuration;


namespace Intranet.Controllers
{
    public class AccountsController : ApiController
    {

        string _commBridgeIP = ConfigurationManager.AppSettings["CommBridgeAddress"];
        int _commBridgePort = 5000;  //default - value is set in constructor

        public AccountsController() : base()
        {
            Int32.TryParse(ConfigurationManager.AppSettings["CommBridgePort"], out _commBridgePort);
        }        


        //Testing only, this should be passed in via http headers or OAuth (better) 
        private const string DEALER_NUMBER = "9999";
        private const string DEALER_PASSCODE = "44455";
        

        [HttpGet]
        public IHttpActionResult BasicInformation(string id)
        {
            D3Communications d3 = new D3Communications(_commBridgeIP, _commBridgePort, DEALER_NUMBER, DEALER_PASSCODE);
            d3.Login();
            AccountInformationSections sections = new AccountInformationSections();
            sections.AddSelectedArea(AccountInformationSections.Basic);
            Account acc = d3.GetAccountInformation(id, sections);
            return new RawJsonActionResult(acc.BasicInformation.ToJson());

            
        }
    }

    public class RawJsonActionResult : IHttpActionResult
    {
        private readonly string _jsonString;

        public RawJsonActionResult(string jsonString)
        {
            _jsonString = jsonString;
        }

        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            var content = new StringContent(_jsonString);
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            var response = new HttpResponseMessage(HttpStatusCode.OK) { Content = content };
            return Task.FromResult(response);
        }
    }


}
