﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Intranet.Models;

namespace Intranet.Controllers
{
    public class requestsController : Controller
    {
        private intranet db = new intranet();

        // GET: requests
        public ActionResult Index()
        {

            //var viewModel = db.Requests.Where(a => a.active == true);
            var viewModel = from r in db.Requests
                            where r.active == true
                            join r2 in db.Admin_Users
                            on r.requestor equals r2.adminuserid
                            join r4 in db.Admin_Users
                            on r.currentTech equals r4.adminuserid
                            join r3 in db.Admin_Users_Departments
                            on r.department equals r3.departmentID
                            select new requestsView {
                                request = r,
                                Admin_Users = r2,
                                Admin_Users_Departments = r3,
                                Admin_Users2 = r4
                            };
                            
            return View(viewModel);
        }

        // GET: requests/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            request request = db.Requests.Find(id);
            if (request == null)
            {
                return HttpNotFound();
            }
            return View(request);
        }

        // GET: requests/Create
        public ActionResult Create()
        {
            int adminlogin = Convert.ToInt32(Request.Cookies["adminlogin"].Value);
            var userinfo = db.Admin_Users.Where(a => a.adminuserid == adminlogin).FirstOrDefault();


            requestsCreate employeeName = new requestsCreate
            {

                employeeName = userinfo.firstname +" "+ userinfo.lastname
            };
      
            return View(employeeName);
        }

        // POST: requests/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "requestText")] request request)
        {
            if (ModelState.IsValid)
            {

                request.requestor = Convert.ToInt32(Request.Cookies["adminlogin"].Value);
                var getDepartment = db.Admin_Users.Where(a => a.adminuserid == request.requestor).FirstOrDefault().defaultdeptid;
                request.department = getDepartment;
                request.active =true;
                request.dateTimeCreated = DateTime.Now;
                request.lastUpdated = DateTime.Now;
                request.opened = false;
                request.currentTech = null;
                db.Requests.Add(request);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
     
            return View(request);
        }

        // GET: requests/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            request request = db.Requests.Find(id);
            if (request == null)
            {
                return HttpNotFound();
            }
            if (request.opened == false) {
                request.opened = true;
                db.Entry(request).State = EntityState.Modified;
                db.SaveChanges();
            }
            
            return View(request);
        }

        // POST: requests/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,dateTimeCreated,department,requestor,currentTech,lastUpdated,active,requestText")] request request)
        {
            if (ModelState.IsValid)
            {
                // int loggedin = Convert.ToInt32(Request.Cookies["adminlogin"].Value);
                //var loggedinuser = db.Admin_Users.Where(a => a.adminuserid == loggedin).FirstOrDefault();
                request.currentTech = Convert.ToInt32(Request.Cookies["adminlogin"].Value);
                request.opened = true;
                request.lastUpdated = DateTime.Now;
                db.Entry(request).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(request);
        }

        // GET: requests/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            request request = db.Requests.Find(id);
            if (request == null)
            {
                return HttpNotFound();
            }
            return View(request);
        }

        // POST: requests/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            request request = db.Requests.Find(id);
            db.Requests.Remove(request);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }

}
