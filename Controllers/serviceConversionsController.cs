﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Intranet.Models;

namespace Intranet.Controllers
{
    public class serviceConversionsController : Controller
    {
        private lydiaBilling db = new lydiaBilling();

        // GET: serviceConversions
        public ActionResult Index()
        {
            return View(db.serviceConversions.ToList());
        }

        // GET: serviceConversions/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            serviceConversion serviceConversion = db.serviceConversions.Find(id);
            if (serviceConversion == null)
            {
                return HttpNotFound();
            }
            return View(serviceConversion);
        }

        // GET: serviceConversions/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: serviceConversions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,vendor,item,description,serviceCode,Ignore,vendorID")] serviceConversion serviceConversion)
        {
            if (ModelState.IsValid)
            {
                db.serviceConversions.Add(serviceConversion);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(serviceConversion);
        }

        // GET: serviceConversions/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            serviceConversion serviceConversion = db.serviceConversions.Find(id);
            if (serviceConversion == null)
            {
                return HttpNotFound();
            }
            return View(serviceConversion);
        }

        // POST: serviceConversions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,vendor,item,description,serviceCode,Ignore,vendorID")] serviceConversion serviceConversion)
        {
            if (ModelState.IsValid)
            {
                db.Entry(serviceConversion).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(serviceConversion);
        }

        // GET: serviceConversions/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            serviceConversion serviceConversion = db.serviceConversions.Find(id);
            if (serviceConversion == null)
            {
                return HttpNotFound();
            }
            return View(serviceConversion);
        }

        // POST: serviceConversions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            serviceConversion serviceConversion = db.serviceConversions.Find(id);
            db.serviceConversions.Remove(serviceConversion);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
