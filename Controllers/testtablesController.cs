﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Intranet.Models;

namespace Intranet.Controllers
{
    public class testtablesController : Controller
    {
        private intranet db = new intranet();

        // GET: testtables
        public ActionResult Index()
        {
            return View(db.testtables.ToList());
        }

        // GET: testtables/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            testtable testtable = db.testtables.Find(id);
            if (testtable == null)
            {
                return HttpNotFound();
            }
            return View(testtable);
        }

        // GET: testtables/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: testtables/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,name,address,city")] testtable testtable)
        {
            if (ModelState.IsValid)
            {
                db.testtables.Add(testtable);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(testtable);
        }

        // GET: testtables/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            testtable testtable = db.testtables.Find(id);
            if (testtable == null)
            {
                return HttpNotFound();
            }
            return View(testtable);
        }

        // POST: testtables/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,name,address,city")] testtable testtable)
        {
            if (ModelState.IsValid)
            {
                db.Entry(testtable).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(testtable);
        }

        // GET: testtables/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            testtable testtable = db.testtables.Find(id);
            if (testtable == null)
            {
                return HttpNotFound();
            }
            return View(testtable);
        }

        // POST: testtables/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            testtable testtable = db.testtables.Find(id);
            db.testtables.Remove(testtable);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
