﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Net;
using System.IO;
using Intranet.Models;
using System.Security.Cryptography;
using System.Deployment.Internal.CodeSigning;
using System.Security.Cryptography.X509Certificates;

namespace Intranet.Controllers
{
    public class westTestController : Controller
    {

        const string CERT_PATH = "~/APP_DATA/West/COPSMONITOR_VUI_ILAB_2017.p12";
        const string CERT_PASS = "Hr6FW2LWQRKoqcSJn387";
        const string SERVICE_USER = "216007";

        const string WEST_THUMB_PRINT = "6a7b0712cfebd53678eb5a63b19a7185aea1da80";


        public ActionResult Index()
        {
            return View(new WestAddress());
        }

        [HttpPost]
        public ActionResult Index(WestAddress address)
        {
            if (ModelState.IsValid)
            {
                TempData["lookup"] = address;
                return RedirectToAction("LookUp");
            }
            else
            {
                return View(address);
            }

        }


        public ActionResult GeodeticTest(WestAddress address)
        {

            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
            CryptoConfig.AddAlgorithm(typeof(RSAPKCS1SHA256SignatureDescription), "http://www.w3.org/2001/04/xmldsig-more#rsa-sha256");


            if (address == null)
                return View(new WestResult());

            Intranet.West.geodeticQueryRequest req = new Intranet.West.geodeticQueryRequest();
            req.transactionID = Guid.NewGuid().ToString();
            req.geolocation = new Intranet.West.geolocation();


            req.geolocation.Items = new string[] {
                "", address.LatitudeDegrees.ToString(), address.LatitudeMinutes.ToString(), address.LatitudeSeconds.ToString(),
                "", address.LongitudeDegrees.ToString(), address.LongitudeMinutes.ToString(), address.LongitudeSeconds.ToString()
            };

            req.geolocation.ItemsElementName = new Intranet.West.ItemsChoiceType[] {
                Intranet.West.ItemsChoiceType.latitude, Intranet.West.ItemsChoiceType.latitudeDegrees, Intranet.West.ItemsChoiceType.latitudeMinutes,Intranet.West.ItemsChoiceType.latitudeSeconds,
                Intranet.West.ItemsChoiceType.longitude, Intranet.West.ItemsChoiceType.longitudeDegrees, Intranet.West.ItemsChoiceType.longitudeMinutes, Intranet.West.ItemsChoiceType.longitudeSeconds
            };

            Intranet.West.frlResourceClient frlResource = new Intranet.West.frlResourceClient();

            /*
            X509Store certStore = new X509Store(StoreName.My, StoreLocation.LocalMachine);
            certStore.Open(OpenFlags.ReadOnly);
            X509Certificate2Collection certCollection = certStore.Certificates.Find(X509FindType.FindByThumbprint, WEST_THUMB_PRINT, false);
            certStore.Close();
            var originalCert = certCollection[0];
            */
            

            var originalCert = new X509Certificate2(Server.MapPath(CERT_PATH), CERT_PASS, X509KeyStorageFlags.Exportable);
            var certBytes = originalCert.Export(X509ContentType.Cert);
            var cert = new X509Certificate2(certBytes);

            frlResource.ClientCredentials.ClientCertificate.Certificate = cert;
            frlResource.ClientCredentials.UserName.UserName = SERVICE_USER;

            try
            {
                var info = frlResource.geodeticQuery(req);
                return View(new WestResult(address, info));

            }
            catch (WebException ex)
            {
                Console.Out.WriteLine(ex.Message);
            }

            return View(new WestResult());
        }


        public ActionResult LookUp()
        {

            WestAddress address = TempData["lookup"] as WestAddress;
            if (address == null)
                address = new WestAddress();

            Intranet.West.responderInfoRequest req = new Intranet.West.responderInfoRequest();

            req.transactionID = Guid.NewGuid().ToString();
            Intranet.West.address adr = new Intranet.West.address();

            adr.HouseNum = address.HouseNumber ?? string.Empty;
            adr.PrefixDirectional = address.PrefixDirectional ?? string.Empty;
            adr.StreetName = address.StreetName ?? string.Empty;
            adr.PostalCommunity = address.City ?? string.Empty;
            adr.StreetSuffix = address.StreetSuffix ?? string.Empty;
            adr.StateProvince = address.State ?? string.Empty;
            adr.PostalZipCode = address.PostalCode ?? string.Empty;
            adr.PostDirectional = address.PostDirectional ?? string.Empty;
            adr.UnitType = address.UnityType ?? string.Empty;
            adr.UnitNumber = address.UnitNumber ?? string.Empty;
            req.address = adr;

            Intranet.West.frlResourceClient frlResource = new Intranet.West.frlResourceClient();

            var originalCert = new X509Certificate2(Server.MapPath(CERT_PATH), CERT_PASS, X509KeyStorageFlags.Exportable);
            var certBytes = originalCert.Export(X509ContentType.Cert);
            var cert = new X509Certificate2(certBytes);

            frlResource.ClientCredentials.ClientCertificate.Certificate = cert;
            frlResource.ClientCredentials.UserName.UserName = SERVICE_USER;

            try
            {
                var info = frlResource.responderInfo(req);

                if (info.addressMatchCode.overallMatch == Intranet.West.overallMatchCodeValues.Failure)
                {
                    //try again
                    req.transactionID = Guid.NewGuid().ToString();
                    info = frlResource.responderInfo(req);
                }


                WestResult result = new WestResult(address, info);
                return View(result);
            }
            catch (WebException ex)
            {
                Console.Out.WriteLine(ex.Message);
            }

            return View(new WestResult());

        }




        private static XmlDocument CreateSoapEnvelope()
        {
            XmlDocument soapEnvelop = new XmlDocument();
            soapEnvelop.LoadXml(@"<SOAP-ENV:Envelope xmlns:SOAP-ENV=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/1999/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/1999/XMLSchema""><SOAP-ENV:Body><HelloWorld xmlns=""http://tempuri.org/"" SOAP-ENV:encodingStyle=""http://schemas.xmlsoap.org/soap/encoding/""><int1 xsi:type=""xsd:integer"">12</int1><int2 xsi:type=""xsd:integer"">32</int2></HelloWorld></SOAP-ENV:Body></SOAP-ENV:Envelope>");
            return soapEnvelop;
        }

    }

}