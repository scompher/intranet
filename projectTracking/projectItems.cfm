
<cfif isDefined("btnUpdateProjectStatus")>
	<cfquery name="updateProjectStatus" datasource="#ds#">
		update projectTracking_projects 
		set statusid = #form.statusid# 
		where projectid = #pid# 
	</cfquery>
	<cflocation url="index.cfm?userid=#userid#&hd=#hd#&filter_statusid=#filter_statusid#&hp=#hp#">
</cfif>

<cfquery name="getProjects" datasource="#ds#">
	select projectTracking_projects.*, projectTracking_status.status 
	from projectTracking_projects 
	left join projectTracking_assignments on projectTracking_projects.projectid = projectTracking_assignments.projectID
	left join projectTracking_status on projectTracking_projects.statusid = projectTracking_status.statusid 
	where 
		projectTracking_assignments.adminUserID = #userid# and (projectTracking_projects.statusid IN (#filter_statusid#))
</cfquery>

<table width="955" border="0" cellpadding="5" cellspacing="0">
	<cfoutput>
    <tr>
        <td class="highlightbar">
		<b>Project Items</b> 
		<cfif hp is 0><a href="index.cfm?userid=#userid#&hd=#hd#&filter_statusid=#filter_statusid#&hp=1">[hide]</a><cfelse><a href="index.cfm?userid=#userid#&hd=#hd#&filter_statusid=#filter_statusid#&hp=0">[show]</a></cfif>
		&nbsp;
		<a href="create/newProject.cfm?userid=#userid#">[Add Project]</a>
		</td>
    </tr>
	</cfoutput>
	<cfif hp is 0>
    <tr>
        <td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td bgcolor="#EEEEEE" colspan="2" style="font-size:13px;" nowrap="NOWRAP" class="linedrowrightcolumn"><b>Project Name</b></td>
					<td bgcolor="#EEEEEE" width="100" align="center" nowrap="NOWRAP" class="linedrowrightcolumn"><b>Start Date</b></td>
					<td bgcolor="#EEEEEE" width="100" align="center" nowrap="NOWRAP" class="linedrowrightcolumn"><b>Due Date</b></td>
					<td bgcolor="#EEEEEE" width="150" align="center" nowrap="nowrap" class="linedrow"><b>Item Status</b></td>
				</tr>
				<cfloop query="getProjects">
					<cfquery name="getMilestones" datasource="#ds#">
						select * 
						from projectTracking_milestones
						where projectid = #getProjects.projectid# 
						order by dueDate, milestoneid asc
					</cfquery>
					<cfoutput>
					<tr>
						<td colspan="2" style="font-size:12px;" nowrap="NOWRAP" class="linedrowrightcolumn"><b><i>#getProjects.projectName#</i></b></td>
						<td width="100" align="center" nowrap="NOWRAP" class="linedrowrightcolumn"><b>#dateformat(getProjects.dateStarted,'mm/dd/yyyy')#</b></td>
						<td width="100" align="center" nowrap="NOWRAP" class="linedrowrightcolumn"><b>#dateformat(getProjects.dueDate,'mm/dd/yyyy')#</b></td>
						<td width="150" align="center" nowrap="nowrap" class="linedrow"><b>#getprojects.status#</b></td>
					</tr>
					<tr>
						<td colspan="2" valign="top" nowrap="NOWRAP" class="linedrowrightcolumn">
							<a href="manageMilestones.cfm?pid=#getProjects.projectid#&userid=#userid#&hd=#hd#&filter_statusid=#filter_statusid#&hp=#hp#">[manage milestones]</a> 
							<a href="index.cfm?pid=#getProjects.projectid#&userid=#userid#&hd=#hd#&filter_statusid=#filter_statusid#&hp=#hp#&action=changeprojectstatus">[change project status]</a>
							<a href="deleteProject.cfm?pid=#getProjects.projectid#&userid=#userid#&hd=#hd#&filter_statusid=#filter_statusid#&hp=#hp#" onclick="return confirm('Are you sure you wish to delete this project?');">[Delete Project]</a>
						</td>
						<td width="100" valign="top" align="center" nowrap="NOWRAP" class="linedrowrightcolumn">&nbsp;</td>
						<td width="100" valign="top" align="center" nowrap="NOWRAP" class="linedrowrightcolumn">#dateformat(getProjects.dueDate,'mm/dd/yyyy')#</td>
						<td width="150" valign="top" align="center" nowrap="nowrap" class="linedrow">
						<cfif action is "changeprojectstatus" and pid is getProjects.projectid>
							<cfinput type="hidden" name="pid" value="#getProjects.projectid#" />
							<cfselect name="statusid" style="vertical-align:middle;">
								<option value=""></option>
								<cfloop query="getStatuses">
									<option <cfif getStatuses.statusid is getProjects.statusid>selected</cfif> value="#getStatuses.statusid#">#getStatuses.status#</option>
								</cfloop>
							</cfselect>
							<cfinput type="submit" name="btnUpdateProjectStatus" value="Apply" class="sidebar" style="vertical-align:middle;">
						<cfelse>
						#getProjects.status#&nbsp;
						</cfif>
						</td>
					</tr>
					<cfloop query="getMilestones">
						<tr>
							<td width="141" align="center" nowrap="NOWRAP" class="linedrowrightcolumn">
								<a href="newProjectTask.cfm?userid=#userid#&hd=#hd#&filter_statusid=#filter_statusid#&hp=#hp#&pid=#getMilestones.projectid#&mid=#getMilestones.milestoneid#">[add item to milestone]</a>
							</td>
							<td class="linedrowrightcolumn"><b>#getMilestones.milestone#</b></td>
							<td align="center" nowrap="nowrap" class="linedrowrightcolumn">&nbsp;</td>
							<td align="center" nowrap="nowrap" class="linedrowrightcolumn"><b>#dateformat(getMilestones.dueDate,'mm/dd/yyyy')#</b></td>
							<td nowrap="nowrap" class="linedrow">&nbsp;</td>
						</tr>
						<cfquery name="getProjectItems" datasource="#ds#">
							select projectTracking_items.*, projectTracking_status.status
							from projectTracking_items 
							left join projectTracking_item_assignments on projectTracking_items.itemID = projectTracking_item_assignments.itemid
							left join projectTracking_status on projectTracking_items.statusID = projectTracking_status.statusid 
							where projectTracking_items.milestoneID = #getMilestones.milestoneID# and projectTracking_item_assignments.adminuserid = #userid# 
							and (projectTracking_items.statusid IN (#filter_statusid#) or projectTracking_items.lastUpdated >= #createodbcdate(now())#)
							order by projectTracking_items.dueDate, projectTracking_items.itemid asc 
						</cfquery>
						<cfloop query="getProjectItems">
							<tr>
								<td align="center" nowrap="NOWRAP" class="linedrowrightcolumn">
									<a href="editProjectTask.cfm?itemid=#getProjectItems.itemID#&userid=#userid#&hd=#hd#&filter_statusid=#filter_statusid#&hp=#hp#">[edit]</a>
									<a onclick="return confirm('Are you sure you wish to delete this item?');" href="deleteitem.cfm?itemid=#getProjectItems.itemid#&userid=#userid#&hd=#hd#&filter_statusid=#filter_statusid#&hp=#hp#">[delete]</a>
									<a href="index.cfm?action=addProjectNoteItem&iid=#itemid#&userid=#userid#&hd=#hd#&filter_statusid=#filter_statusid#&hp=#hp#">[add note]</a>
								</td>
								<td class="linedrowrightcolumn">&bull; #itemDescription#</td>
								<td align="center" nowrap="nowrap" class="linedrowrightcolumn">#dateformat(startDate,'mm/dd/yyyy')#&nbsp;</td>
								<td align="center" nowrap="nowrap" class="linedrowrightcolumn">#dateformat(dueDate,'mm/dd/yyyy')#&nbsp;</td>
								<td nowrap="nowrap" align="center" class="linedrow">#status#&nbsp;</td>
							</tr>
							<cfquery name="getNotes" datasource="#ds#">
								select * from projectTracking_notes where itemid = #getProjectItems.itemid# order by dateTimeCreated asc
							</cfquery>
							<cfloop query="getNotes">
								<cfif trim(getNotes.note) is not "">
								<tr>
									<td align="center" valign="top" nowrap="NOWRAP" class="linedrowrightcolumn">
									<a href="index.cfm?action=deletenote&noteid=#noteid#&userid=#userid#&hd=#hd#&filter_statusid=#filter_statusid#&hp=#hp#">[delete note]</a>
									</td>
									<td colspan="5" valign="top" class="linedrowrightcolumn"><i>#dateformat(dateTimeCreated,'mm/dd/yyyy')# - #note#</i></td>
								</tr>
								</cfif>
							</cfloop>
							<cfif action is "addProjectNoteItem" and iid is getProjectItems.itemid>
								<cfinput type="hidden" name="itemid" value="#itemid#">
								<tr>
									<tr>
										<td align="center" valign="top" nowrap="NOWRAP" class="linedrowrightcolumn">&nbsp;
										
										</td>
										<td colspan="5" valign="top">
										<cftextarea name="note" style="width:750px;" rows="3"></cftextarea>
										</td>
									</tr>
									<tr>
										<td align="center" valign="top" nowrap="NOWRAP" class="linedrowrightcolumn">&nbsp;</td>
										<td colspan="5" valign="top" class="linedrowrightcolumn">
										<cfinput type="submit" name="btnSaveProjectItemNote" value="Add Note" class="sidebar">
										<cfinput onClick="document.location='index.cfm?userid=#userid#&hd=#hd#&filter_statusid=#filter_statusid#&hp=#hp#';" type="button" name="btnCancelNote" value="Cancel" class="sidebar">
										</td>
									</tr>
								</tr>
							</cfif>
						</cfloop>
					</cfloop>
					</cfoutput>
					<tr>
						<td colspan="5" align="right" valign="top" nowrap="nowrap" class="linedrowrightcolumn">&nbsp;</td>
					</tr>
				</cfloop>
			</table>
		</td>
    </tr>
	</cfif>
</table>
