
<cfquery name="getTasks" datasource="#ds#">
	select projectTracking_status.status, projectTracking_items.itemDescription, convert(nvarchar(12),projectTracking_items.dueDate,101) as dueDate, admin_users.firstname + ' ' + admin_users.lastname as assignedTo 
	from projectTracking_items 
	left join projectTracking_item_assignments on projectTracking_items.itemID = projectTracking_item_assignments.itemid
	left join admin_users on projectTracking_item_assignments.adminuserid = admin_users.adminuserid 
	left join projectTracking_status on projectTracking_items.statusID = projectTracking_status.statusID 
	where projectTracking_items.statusID <> 4 and projectTracking_items.milestoneid = 0 
	order by admin_users.lastname, admin_users.firstname, projectTracking_items.duedate desc 
</cfquery>

<cfx_query2excel 
	file="e:\websites\intranet\projectTracking\reporting\attachments\#dateformat(now(),'yyyymmdd')#_taskreport.xls" 
	headings="Status,Description,Due Date,Assigned To" 
	queryFields="status,itemDescription,dueDate,assignedTo" 
	format="excel" 
	query="#getTasks#">

<cfoutput>
<a href="/projectTracking/reporting/attachments/#dateformat(now(),'yyyymmdd')#_taskreport.xls">Download</a>
</cfoutput>
