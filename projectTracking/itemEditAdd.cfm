
<cfif action is "edititem">
	<cfquery name="getItemInfo" datasource="#ds#">
		select * from projectTracking_items 
		where itemid = #itemid# 
	</cfquery>
	<cfoutput query="getItemInfo">
		<cfset itemDescription = getItemInfo.itemDescription>
		<cfset dueDate = dateformat(getItemInfo.dueDate,'mm/dd/yyyy')>
		<cfset catid = getItemInfo.categoryid>
		<cfset sid = getItemInfo.statusid>
		<cfset notes = "">
	</cfoutput>
<cfelse>
	<cfset itemDescription = "">
	<cfset dueDate = "">
	<cfset catid = 0>
	<cfset sid = 0>
	<cfset notes = "">
</cfif>

<cfinput type="hidden" name="iid" value="#iid#">

<cfif action is not "addnote">
	<tr>
		<td width="10%" align="center" nowrap="nowrap" class="linedrowrightcolumn">&nbsp;</td>
		<td width="45%" class="linedrowrightcolumn">Item Description: <br />
			<cfinput type="text" name="itemDescription" style="width:380px;" value="#itemDescription#" required="yes" message="The description is required." />
		</td>
		<td width="16%" nowrap="nowrap" class="linedrowrightcolumn">
			Due Date: (mm/dd/yyyy)<br />
			<cfinput type="datefield" name="dueDate" style="width:75px;" value="#dueDate#" validate="date" required="yes" message="A valid date in the format mm/dd/yyyy is required." />
		</td>
		<td width="21%" nowrap="nowrap" class="linedrowrightcolumn">
			Category:<br />
			<cfselect name="categoryid" required="yes" message="The category is required.">
				<option value="0"></option>
				<cfoutput query="getCategories">
					<option <cfif catid  is getCategories.categoryid>selected</cfif> value="#getCategories.categoryID#">#getCategories.category#</option>
				</cfoutput>
			</cfselect>
		</td>
		<td width="16%" nowrap="nowrap" class="linedrow">
			Status:<br />
			<cfselect name="statusid" required="yes" message="The status is required.">
				<option value="0"></option>
				<cfoutput query="getStatuses">
					<option <cfif sid is getStatuses.statusid>selected</cfif> value="#getStatuses.statusid#">#getStatuses.status#</option>
				</cfoutput>
			</cfselect>
		</td>
	</tr>
<cfelse>
	<cfoutput>
	<tr>
		<td align="center" nowrap="nowrap" class="linedrowrightcolumn">
		<a href="index.cfm?action=edititem&itemid=#itemid#">[edit]</a>
		<a onclick="return confirm('Are you sure you wish to delete this item?');" href="deleteitem.cfm?itemid=#getDailyItems.itemid#">[delete]</a>
		<a href="index.cfm?action=addnote&itemid=#itemid#">[Add Note]</a>
		</td>
		<td class="linedrowrightcolumn"><b>#getDailyItems.itemDescription#</b></td>
		<td nowrap="nowrap" class="linedrowrightcolumn">#dateformat(getDailyItems.dueDate,'mm/dd/yyyy')#</td>
		<td nowrap="nowrap" class="linedrowrightcolumn">#getDailyItems.category#</td>
		<td nowrap="nowrap" class="linedrow">#getDailyItems.status#</td>
	</tr>
	</cfoutput>
</cfif>
<cfif action is not "edititem">
	<tr>
		<td align="center" nowrap="nowrap" class="linedrowrightcolumn">&nbsp;</td>
		<td colspan="4" class="linedrow">
			Notes:<br />
			<cftextarea name="note" style="width:800px" rows="3"><cfoutput>#notes#</cfoutput></cftextarea>
		</td>
	</tr>
</cfif>
<tr>
	<td align="center" nowrap="nowrap" class="linedrowrightcolumn">&nbsp;</td>
	<td colspan="4" class="linedrowrightcolumn">
		<cfif action is "edititem">
			<cfinput name="btnUpdateDailyItem" type="submit" class="sidebar" value="Update Item" />
		<cfelseif action is "addnote">
			<cfinput name="btnAddNote" type="submit" class="sidebar" value="Save Note" />
		<cfelse>
			<cfinput name="btnAddDailyItem" type="submit" class="sidebar" value="Add Item" />
		</cfif>
		<cfinput onClick="document.location='index.cfm?userid=#userid#&hd=#hd#&filter_statusid=#filter_statusid#&hp=#hp#';" name="btnCancel" type="button" class="sidebar" value="Cancel" />
	</td>
</tr>

