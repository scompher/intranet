
<script type="text/javascript">
	function moveToProject(iid) {
		var pWin = window.open('movetoproject.cfm?iid=' + iid,'pWindow','width=520,height=400,resizable=yes,resizable=1');
	}
</script>

<cfquery name="getDailyItems" datasource="#ds#">
	select 
		projectTracking_items.itemid, 
		projectTracking_items.itemDescription, 
		projectTracking_items.milestoneID, 
		projectTracking_items.type, 
		projectTracking_items.categoryID, 
		projectTracking_items.dueDate, 
		projectTracking_items.statusID, 
		projectTracking_items.lastUpdated, 
		projectTracking_status.status, 
		projectTracking_categories.category 
	from projectTracking_items 
	left join projectTracking_item_assignments on projectTracking_items.itemid = projectTracking_item_assignments.itemid
	left join projectTracking_status on projectTracking_items.statusid = projectTracking_status.statusid 
	left join projectTracking_categories on projectTracking_items.categoryid = projectTracking_categories.categoryid 
	where milestoneID = 0 and projectTracking_item_assignments.adminuserid = #userid# and (projectTracking_items.statusid IN (#filter_statusid#) or projectTracking_items.lastUpdated >= #createodbcdate(now())#)
	order by projectTracking_items.dueDate, projectTracking_items.itemid asc 
</cfquery>

<table width="955" border="0" cellspacing="0" cellpadding="5">
	<cfoutput>
    <tr>
        <td class="highlightbar"><b>Daily Items </b> <cfif hd is 0><a href="index.cfm?userid=#userid#&hd=1&filter_statusid=#filter_statusid#&hp=#hp#">[hide]</a><cfelse><a href="index.cfm?userid=#userid#&hd=0&filter_statusid=#filter_statusid#&hp=#hp#">[show]</a></cfif></td>
    </tr>
	</cfoutput>
	<cfif hd is 0>
    <tr>
        <td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
				    <td width="10%" align="center" nowrap="nowrap" class="linedrowrightcolumn"><b>Action</b></td>
					<td width="45%" class="linedrowrightcolumn"><b>Item</b></td>
				    <td width="16%" nowrap="nowrap" align="center" class="linedrowrightcolumn"><b>Date Due </b></td>
				    <td width="21%" nowrap="nowrap" class="linedrowrightcolumn"><b>Category</b></td>
				    <td align="center" width="16%" nowrap="nowrap" class="linedrow"><b>Item Status</b></td>
			    </tr>
				<cfif action is "addDailyItem">
					<cfinclude template="itemEditAdd.cfm">
				</cfif>
				<cfif getDailyItems.recordcount is 0>
					<tr>
						<td class="linedrow" colspan="5">There are no daily items created</td>
					</tr>
				<cfelse>
					<cfloop query="GetDailyItems">
					<cfif (action is "edititem" or action is "addnote") and iid is getDailyItems.itemid>
						<cfinclude template="itemEditAdd.cfm">
					<cfelse>
						<cfquery name="getNotes" datasource="#ds#">
							select * from projectTracking_notes
							where itemid = #getDailyItems.itemid#
							order by dateTimeCreated ASC 
						</cfquery>
						<cfoutput>
						<tr>
							<td align="center" nowrap="nowrap" class="linedrowrightcolumn">
							<a href="index.cfm?action=edititem&iid=#itemid#&userid=#userid#&hd=#hd#&filter_statusid=#filter_statusid#&hp=#hp#">[edit]</a>
							<a onclick="return confirm('Are you sure you wish to delete this item?');" href="deleteitem.cfm?itemid=#getDailyItems.itemid#&userid=#userid#&hd=#hd#&filter_statusid=#filter_statusid#&hp=#hp#">[delete]</a>
							<a href="index.cfm?action=addnote&iid=#itemid#&userid=#userid#&hd=#hd#&filter_statusid=#filter_statusid#&hp=#hp#">[Add Note]</a>
							<br />
							<a href="javascript:moveToProject(#itemid#);">[Move to A Project]</a>
							</td>
							<td class="linedrowrightcolumn"><b>#getDailyItems.itemDescription#</b></td>
							<td nowrap="nowrap" align="center" class="linedrowrightcolumn">#dateformat(getDailyItems.dueDate,'mm/dd/yyyy')#</td>
							<td nowrap="nowrap" class="linedrowrightcolumn">#getDailyItems.category#&nbsp;</td>
							<td nowrap="nowrap" class="linedrow">#getDailyItems.status#&nbsp;</td>
						</tr>
						</cfoutput>
						<cfloop query="getNotes">
							<cfoutput>
							<tr>
								<td class="linedrowrightcolumn" align="right"><a href="index.cfm?action=deletenote&noteid=#noteid#&userid=#userid#&hd=#hd#&filter_statusid=#filter_statusid#&hp=#hp#">[delete note]</a></td>
								<td colspan="4" class="linedrow">
								#dateformat(dateTimeCreated,'mm/dd/yyyy')# - #note#
								</td>
							</tr>
							</cfoutput>
						</cfloop>
					</cfif>
					<tr><td colspan="5" class="linedrow">&nbsp;</td></tr>
					</cfloop>
				</cfif>
				<tr>
				    <td align="center" nowrap="nowrap" class="linedrowrightcolumn"><cfoutput><a href="index.cfm?action=addDailyItem&userid=#userid#&hd=#hd#&filter_statusid=#filter_statusid#&hp=#hp#">[Add Daily Item]</a></cfoutput></td>
				    <td class="linedrow" colspan="4">&nbsp;</td>
				</tr>
			</table>
		</td>
    </tr>
	</cfif>
</table>
