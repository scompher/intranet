
<cfparam name="userid" default="#cookie.adminlogin#">
<cfparam name="action" default="">
<cfparam name="iid" default="0">
<cfparam name="filter_statusid" default="1,2,3">
<cfparam name="hd" default="0">
<cfparam name="hp" default="0">
<cfparam name="whatToView" default="tasks,projects">

<cfif isDefined("btnSaveProjectItemNote")>	
	<cfif trim(form.note) is not "">
		<cfquery name="AddNote" datasource="#ds#">
			insert into projectTracking_notes (note, dateTimeCreated, creatorid, itemid) 
			values ('#form.note#', GETDATE(), #userid#, #itemid#) 
		</cfquery>
	</cfif>
	<cflocation url="index.cfm?userid=#userid#&hd=#hd#&filter_statusid=#filter_statusid#&hp=#hp#">
</cfif>

<cfif action is "deletenote">
	<cfquery name="deleteNote" datasource="#ds#">
		delete from projectTracking_notes 
		where noteid = #noteid# 
	</cfquery>
	<cflocation url="index.cfm?userid=#userid#&hd=#hd#&filter_statusid=#filter_statusid#&hp=#hp#">
</cfif>

<cfif isDefined("form.btnAddNote")>
	<cfif trim(form.note) is not "">
		<cfquery name="AddNote" datasource="#ds#">
			insert into projectTracking_notes (note, dateTimeCreated, creatorid, itemid) 
			values ('#form.note#', GETDATE(), #userid#, #iid#) 
		</cfquery>
	</cfif>
	<cflocation url="index.cfm?userid=#userid#&hd=#hd#&filter_statusid=#filter_statusid#&hp=#hp#">
</cfif>

<cfif isDefined("form.btnUpdateDailyItem")>
	<cfset pid = 0>
	<cfset mid = 0>
	<cfquery name="updateTaskItem" datasource="#ds#">
		update projectTracking_items 
		set itemDescription = '#form.itemDescription#', milestoneid = #mid#, statusid = #form.statusid#, categoryid = #form.categoryid#, dueDate = '#form.dueDate#', lastUpdated = GETDATE() 
		where itemid = #iid# 
	</cfquery>
	<cflocation url="index.cfm?userid=#userid#&hd=#hd#&filter_statusid=#filter_statusid#&hp=#hp#">
</cfif>

<cfif isDefined("form.btnAddDailyItem")>
	<cfset pid = 0>
	<cfset mid = 0>
	<cfset dateCreated = dateformat(now(),'mm/dd/yyyy')>
	<cfquery name="addTaskItem" datasource="#ds#">
		insert into projectTracking_items (itemDescription, milestoneID, type, categoryID, dueDate, statusID, lastUpdated, dateCreated) 
		values ('#form.itemDescription#', #mid#, 0, #form.categoryid#, '#form.dueDate#', #form.statusid#, GETDATE(), '#dateCreated#') 
	</cfquery>
	<cfquery name="getItemID" datasource="#ds#">
		select max(itemid) as iid from projectTracking_items where itemDescription = '#form.itemDescription#' and milestoneID = #mid# 
	</cfquery>
	<cfloop list="#userid#" index="uid">
		<cfquery name="assignEmployees" datasource="#ds#">
			if not exists (select adminuserid from projectTracking_item_assignments where itemid = #getItemID.iid# and adminuserid = #uid#)
				begin
					insert into projectTracking_item_assignments (itemid, adminuserid) values (#getItemID.iid#, #uid#) 
				end
		</cfquery>
	</cfloop>
	<cfif trim(form.note) is not "">
		<cfquery name="AddNote" datasource="#ds#">
			insert into projectTracking_notes (note, dateTimeCreated, creatorid, itemid) 
			values ('#form.note#', GETDATE(), #userid#, #getItemID.iid#) 
		</cfquery>
	</cfif>
	<cflocation url="index.cfm?userid=#userid#&hd=#hd#&filter_statusid=#filter_statusid#&hp=#hp#">
</cfif>

<cfquery name="getCategories" datasource="#ds#">
	select * 
	from projectTracking_categories
	order by category asc 
</cfquery>

<cfquery name="getStatuses" datasource="#ds#">
	select * from projectTracking_status
	order by status asc 
</cfquery>

<link rel="stylesheet" type="text/css" href="/styles.css">
<div align="center">

<cfform method="post" action="index.cfm">

<cfinput type="hidden" name="hd" value="#hd#">
<cfinput type="hidden" name="hp" value="#hp#">

<cfquery name="getEmployees" datasource="#ds#">
	select Admin_Users.*  
	from Admin_Users
	left join Admin_Users_Departments_Lookup on admin_users.adminuserid = Admin_Users_Departments_Lookup.adminuserid
	where Admin_Users_Departments_Lookup.departmentid = #getsec.defaultdeptid# and active = 1  and Admin_Users.adminuserid <> 170
	order by lastname, firstname 
</cfquery>

<table width="955" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Filters</b></td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
		<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<cfif getsec.seclevelid lte 2>
				<tr>
					<td width="61" nowrap="nowrap">Employee:</td>
					<td width="41">
					<cfselect name="userid">
						<option value="0">Select Employee</option>
						<cfoutput query="getEmployees">
							<option <cfif userid is getEmployees.adminuserid>selected</cfif> value="#adminuserid#">#firstname# #lastname#</option>
						</cfoutput>
					</cfselect>
					</td>
				</tr>
				<tr>
					<td width="61" nowrap="nowrap">View:</td>
					<td valign="middle">
						<input <cfif listfindnocase(whatToView,"tasks") is not 0>checked</cfif> type="checkbox" name="whatToView" style="vertical-align:middle;" value="tasks" /> Tasks &nbsp; 
						<input <cfif listfindnocase(whatToView,"projects") is not 0>checked</cfif> type="checkbox" name="whatToView" style="vertical-align:middle;" value="projects" /> Projects 
					</td>
				</tr>
			<cfelse>
				<cfinput type="hidden" name="userid" value="#cookie.adminlogin#">
			</cfif>
			<tr>
				<td align="left" width="5">Status:</td>
				<td align="left">
				<cfoutput query="getStatuses">
					<input <cfif listfind(filter_statusid,getStatuses.statusid) is not 0>checked</cfif> style="vertical-align:middle;" type="checkbox" name="filter_statusid" value="#getStatuses.statusid#" />#getStatuses.status#&nbsp;&nbsp;
				</cfoutput>
				</td>
			</tr>
			<tr>
				<td align="left">
				<cfinput type="submit" name="btnApplyFilter" value="Apply" class="sidebar">
				</td>				
			</tr>
		</table>
		</td>
	</tr>
</table>


<cfif listfindnocase(whatToView,"tasks")>
	<br />
	<cfinclude template="dailyItems.cfm">
</cfif>

<cfif listfindnocase(whatToView,"projects")>
	<br>
	<cfinclude template="projectItems.cfm">
</cfif>

</cfform>
<br />
<a style="text-decoration:underline;" class="normal" href="/index.cfm">Return to Intranet</a>
<br />
<br />
</div>


