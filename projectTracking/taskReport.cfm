
<link rel="stylesheet" type="text/css" href="/styles.css">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

<cfquery name="getTasks" datasource="#ds#">
	select projectTracking_items.*, admin_users.firstname, admin_users.lastname, projectTracking_status.status 
	from projectTracking_items 
	left join projectTracking_item_assignments on projectTracking_items.itemID = projectTracking_item_assignments.itemid
	left join admin_users on projectTracking_item_assignments.adminuserid = admin_users.adminuserid 
	left join projectTracking_status on projectTracking_items.statusID = projectTracking_status.statusID 
	where projectTracking_items.statusID <> 4 and projectTracking_items.milestoneid = 0 
	order by admin_users.lastname, admin_users.firstname, projectTracking_items.duedate desc 
</cfquery>

<cfquery name="getEmployees" datasource="#ds#">
	select Admin_Users.*  
	from Admin_Users
	left join Admin_Users_Departments_Lookup on admin_users.adminuserid = Admin_Users_Departments_Lookup.adminuserid
	where Admin_Users_Departments_Lookup.departmentid = #getsec.defaultdeptid# and active = 1  and Admin_Users.adminuserid <> 170
	order by lastname, firstname 
</cfquery>

<style>
	INPUT, SELECT {vertical-align:middle;}
</style>

<script>
$(document).ready(function(){
	$("button#togglenotes").click(function(){
	  $("tr#notes").toggle();
	});
});
</script>

<div align="center">
<table width="950" border="0" cellspacing="0" cellpadding="5">
    <tr>
        <td class="highlightbar"><b>Manage Tasks</b></td>
    </tr>
    <tr>
        <td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td class="linedrow" colspan="5">
					<button id="togglenotes">show/hide all notes</button>
					&nbsp;
					<button id="downloadExcel">Download Report</button>
					</td>
				</tr>
				<!--- 
				<tr>
				    <td class="linedrow" colspan="5">
						<cfform method="post" action="taskReport.cfm" name="addTaskForm">
						<table border="0" cellspacing="0" cellpadding="2">
							<tr>
								<td>New Task Description:</td>
								<td>Assign To:</td>
								<td>Due Date:</td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td><input type="text" name="taskDescription" style="width:500px;" /></td>
								<td>
								<select name="assignedTo">
									<option value="0">Assign To</option>
									<cfoutput query="getEmployees">
										<option value="#adminuserid#">#firstname# #lastname#</option>
									</cfoutput>
								</select>
								</td>
								<td><cfinput type="datefield" name="dueDate" style="width:75px;"></td>
								<td><cfinput type="submit" name="btnAddTask" value="Add Task Item" /></td>
							</tr>
						</table>
						</cfform>
					</td>
			    </tr>
				 --->
				<tr>
					<td align="center" class="linedrow">&nbsp;</td>
					<td class="linedrow"><b>Task description</b></td>
					<td align="center" class="linedrow"><b>Due Date</b></td>
					<td class="linedrow"><b>Status</b></td>
				    <td class="linedrow"><b>Assigned To</b> </td>
				</tr>
				<cfoutput query="getTasks">
					<cfquery name="getNotes" datasource="#ds#">
						select * from projectTracking_notes where itemid = #getTasks.itemid# order by dateTimeCreated asc
					</cfquery>
					<tr>
						<td align="center" valign="middle" class="linedrow"><input type="checkbox" value=""></td>
						<td valign="top" class="linedrow">#itemDescription#</td>
						<td align="center" valign="top" nowrap="nowrap" class="linedrow">#dateformat(dueDate,'mm/dd/yyyy')#</td>
						<td valign="top" nowrap="nowrap" class="linedrow">#status#</td>
						<td valign="top" nowrap="nowrap" class="linedrow">#firstname# #lastname#</td>
					</tr>
					<cfloop query="getNotes">
						<cfif trim(getNotes.note) is not "">
						<tr id="notes">
							<td class="linedrow">&nbsp;</td>
							<td colspan="4" valign="top" class="linedrow"><i>#dateformat(dateTimeCreated,'mm/dd/yyyy')# - #note#</i></td>
						</tr>
						</cfif>
					</cfloop>
				</cfoutput>
			</table>
		</td>
    </tr>
</table>
</div>
