<link rel="stylesheet" type="text/css" href="/styles.css">

<cfparam name="form.milestone" default="">
<cfparam name="form.dueDate" default="">
<cfparam name="mid" default="0">

<cfif isDefined("form.btnSaveMilestone")>
	<cfquery name="saveMilestone" datasource="#ds#">
		insert into projectTracking_milestones (projectid, milestone, dueDate, statusid) 
		values (#pid#, '#form.milestone#', '#dueDate#', 0) 
	</cfquery>
	<cflocation url="manageMilestones.cfm?pid=#pid#&userid=#userid#&hd=#hd#&filter_statusid=#filter_statusid#&hp=#hp#">
</cfif>

<cfif isDefined("form.btnUpdateMilestone")>
	<cfquery name="updateMilestone" datasource="#ds#">
		update projectTracking_milestones 
		set milestone = '#form.milestone#', dueDate = '#form.dueDate#' 
		where milestoneID = #mid# 
	</cfquery>
	<cflocation url="manageMilestones.cfm?pid=#pid#&userid=#userid#&hd=#hd#&filter_statusid=#filter_statusid#&hp=#hp#">
</cfif>

<cfif isDefined("url.delete")>
	<cfquery name="deleteMilestone" datasource="#ds#">
		delete from projectTracking_milestones 
		where milestoneid = #mid# 
	</cfquery>
	<cflocation url="manageMilestones.cfm?pid=#pid#&userid=#userid#&hd=#hd#&filter_statusid=#filter_statusid#&hp=#hp#">
</cfif>

<cfif isDefined("url.edit")>
	<cfquery name="getMilestone" datasource="#ds#">
		select * from projectTracking_milestones 
		where milestoneid = #mid# 
	</cfquery>
	<cfset form.milestone = getMilestone.milestone>
	<cfset form.dueDate = dateformat(getMilestone.dueDate,'mm/dd/yyyy')>
</cfif>

<cfquery name="getProjectName" datasource="#ds#">
	select * from projectTracking_projects 
	where projectid = #pid# 
</cfquery>

<cfquery name="getMilestones" datasource="#ds#">
	select * from projectTracking_milestones
	where projectid = #pid# 
	order by dueDate asc 
</cfquery>

<div align="center">
<br />
<br />
<cfform method="post" action="manageMilestones.cfm">

<cfinput type="hidden" name="pid" value="#pid#">
<cfinput type="hidden" name="mid" value="#mid#">
<cfinput type="hidden" name="hd" value="#hd#">
<cfinput type="hidden" name="hp" value="#hp#">
<cfinput type="hidden" name="filter_statusid" value="#filter_statusid#">
<cfinput type="hidden" name="userid" value="#userid#">



<table width="425" border="0" cellpadding="5" cellspacing="0">
    <tr>
        <td class="highlightbar"><b>Manage Milestones</b></td>
    </tr>
    <tr>
        <td class="greyrowbottomnopadding">
            <table width="100%" border="0" cellspacing="0" cellpadding="5">
                <tr>
                    <td width="15%" nowrap="nowrap">Project:</td>
                    <td width="85%">
						<cfoutput>#getProjectName.projectName#</cfoutput>
                    </td>
                </tr>
                <tr>
                    <td nowrap="nowrap">Milestone name: </td>
                    <td>
                        <cfinput type="text" name="milestone" style="width:300px;" value="#form.milestone#" required="yes" message="The milestone name is required" />
                    </td>
                </tr>
                <tr>
                    <td nowrap="nowrap">Due Date: </td>
                    <td>
                        <cfinput type="datefield" name="dueDate" style="width:75px;" value="#form.dueDate#" validate="date" required="yes" message="A valid due date is required" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
					<cfif isDefined("url.edit")>
						<cfinput type="submit" name="btnUpdateMilestone" class="sidebar" value="Update Milestone">
					<cfelse>
						<cfinput type="submit" name="btnSaveMilestone" class="sidebar" value="Add Milestone">
					</cfif>
					<cfinput name="btnClear" type="reset" class="sidebar" value="Clear">
					</td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</cfform>
<br />
<table width="425" border="0" cellpadding="5" cellspacing="0">
    <tr>
        <td class="highlightbar" align="center"><b>Current Milestones </b></td>
    </tr>
	<tr>
		<td class="greyrowbottomnopadding">
		<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<cfif getMilestones.recordCount gt 0>
				<cfoutput query="getMilestones">
				<tr>
					<td width="0" align="center" nowrap="nowrap">
						<a href="manageMilestones.cfm?pid=#pid#&mid=#getMilestones.milestoneid#&edit=true&userid=#userid#&hd=#hd#&filter_statusid=#filter_statusid#&hp=#hp#">[edit]</a>
						<a onclick="return confirm('Are you sure you wish to delete this milestone?');" href="manageMilestones.cfm?pid=#pid#&mid=#getMilestones.milestoneid#&delete=true&userid=#userid#&hd=#hd#&filter_statusid=#filter_statusid#&hp=#hp#">[delete]</a>
					</td>
					<td width="200">#getMilestones.currentRow#. #milestone#</td>
					<td width="100" nowrap="nowrap">Due: #dateformat(dueDate,'mm/dd/yyyy')#</td>
				</tr>
				</cfoutput>
			<cfelse>
				<tr><td>There are no milestones currently attached to this project</td></tr>
			</cfif>
		</table>
		</td>
	</tr>
</table>
<br />
<cfoutput>
<a style="text-decoration:underline;" class="normal" href="index.cfm?userid=#userid#&hd=#hd#&filter_statusid=#filter_statusid#&hp=#hp#">Return to Project Tracker</a>
</cfoutput>
</div>


