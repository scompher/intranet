
<link rel="stylesheet" type="text/css" href="/styles.css">

<cfparam name="pid" default="0">
<cfparam name="mid" default="0">
<cfparam name="form.categoryid" default="0">
<cfparam name="form.statusid" default="0">
<cfparam name="type" default="project">
<cfparam name="dueDate" default="">
<cfparam name="startDate" default="">
<cfparam name="userid" default="#cookie.adminlogin#">

<cfif isDefined("form.btnSaveTask")>
	<cfif pid is 0><cfset mid = 0></cfif>
	<cfquery name="addTaskItem" datasource="#ds#">
			insert into projectTracking_items (itemDescription, milestoneID, type, categoryID, startDate, dueDate, statusID, lastUpdated, dateCreated) 
			values ('#form.itemDescription#', #mid#, 0, #form.categoryid#, '#form.startDate#', '#form.dueDate#', #form.statusid#, GETDATE(), GETDATE()) 
	</cfquery>
	<cfquery name="getItemID" datasource="#ds#">
		select max(itemid) as iid from projectTracking_items where itemDescription = '#form.itemDescription#' and milestoneID = #mid# 
	</cfquery>
	<cfloop list="#form.adminuserid#" index="uid">
		<cfquery name="assignEmployees" datasource="#ds#">
			if not exists (select adminuserid from projectTracking_item_assignments where itemid = #getItemID.iid# and adminuserid = #uid#)
				begin
					insert into projectTracking_item_assignments (itemid, adminuserid) values (#getItemID.iid#, #uid#) 
				end
		</cfquery>
	</cfloop>
	<cfif trim(form.notes) is not "">
		<cfquery name="saveNote" datasource="#ds#">
			insert into projectTracking_notes (note, dateTimeCreated, creatorid, itemid) 
			values ('#form.notes#', GETDATE(), #cookie.adminLogin#, #getItemID.iid#)
		</cfquery>
	</cfif>
	<cflocation url="index.cfm?userid=#userid#&hd=#hd#&filter_statusid=#filter_statusid#&hp=#hp#">
</cfif>

<cfquery name="getProjects" datasource="#ds#">
	select * from projectTracking_projects
	order by projectName asc 
</cfquery>

<cfquery name="getMilestones" datasource="#ds#">
	select * from projectTracking_milestones
	where projectid = #pid#  
</cfquery>

<cfquery name="getCategories" datasource="#ds#">
	select * 
	from projectTracking_categories
	order by category asc 
</cfquery>

<cfquery name="getStatuses" datasource="#ds#">
	select * from projectTracking_status
	order by status asc 
</cfquery>

<cfif pid is not 0>
	<cfquery name="getEmployees" datasource="#ds#">
		select * from admin_users 
		where adminuserid in (select adminuserid from projectTracking_assignments where projectid = #pid#) and active = 1 
		order by lastname, firstname 
	</cfquery>
<cfelse>
	<cfquery name="getEmployees" datasource="#ds#">
		select * from admin_users 
		where adminuserid = #cookie.adminLogin# and active = 1 
		order by lastname, firstname 
	</cfquery>
</cfif>

<div align="center">
<br />
<cfform method="post" action="newProjectTask.cfm">

<cfinput type="hidden" name="type" value="#type#">
<cfinput type="hidden" name="userid" value="#userid#">
<cfinput type="hidden" name="hd" value="#hd#">
<cfinput type="hidden" name="hp" value="#hp#">
<cfinput type="hidden" name="filter_statusid" value="#filter_statusid#">

<table width="600" border="0" cellspacing="0" cellpadding="5">
    <tr>
        <td class="highlightbar"><b>New Task</b></td>
    </tr>
    <tr>
        <td class="greyrowbottomnopadding">
            <table width="100%" border="0" cellspacing="0" cellpadding="5">
				<cfif type is not "daily">
					<tr>
						<td width="19%" nowrap>Project:</td>
						<td width="81%">
							<cfselect name="pid" onChange="this.form.submit();">
								<option value="0">No Project</option>
								<cfoutput query="getProjects">
									<option <cfif pid is getProjects.projectid>selected</cfif> value="#getProjects.projectID#">#getProjects.projectName#</option>
								</cfoutput>
							</cfselect>
						</td>
					</tr>
				</cfif>
				<cfif pid is not 0>
				<tr>
                    <td nowrap>Milestone:</td>
                    <td>
						<cfselect name="mid">
							<option value="0">No Milestone</option>
							<cfoutput query="getMilestones">
								<option <cfif mid is getMilestones.milestoneid>selected</cfif> value="#milestoneid#">#milestone#</option>
							</cfoutput>
						</cfselect>
					</td>
                </tr>
				</cfif>
                <tr>
                    <td nowrap>Item Description: </td>
                    <td>
                        <cfinput type="text" name="itemDescription" style="width:300px;">
                    </td>
                </tr>
				<tr>
					<td nowrap="nowrap">Status:</td>
					<td>
						<cfselect name="statusid">
							<option value="0"></option>
							<cfoutput query="getStatuses">
								<option value="#getStatuses.statusid#">#getStatuses.status#</option>
							</cfoutput>
						</cfselect>
					</td>
				</tr>
				<cfif pid is 0>
					<tr>
						<td nowrap>Category:</td>
						<td>
							<cfselect name="categoryid">
								<option value="0"></option>
								<cfoutput query="getCategories">
									<option value="#getCategories.categoryID#">#getCategories.category#</option>
								</cfoutput>
							</cfselect>
						</td>
					</tr>
					<tr>
						<td nowrap>Status:</td>
						<td>
							<cfselect name="statusid">
								<option value="0"></option>
								<cfoutput query="getStatuses">
									<option value="#getStatuses.statusid#">#getStatuses.status#</option>
								</cfoutput>
							</cfselect>
						</td>
					</tr>
				</cfif>
                <tr>
                    <td nowrap>Start Date: </td>
                    <td>
                        <cfinput type="datefield" name="startDate" style="width:75px;" validate="date" required="yes" message="A valid start date is required">
                    </td>
                </tr>
                <tr>
                    <td nowrap>Due Date: </td>
                    <td>
                        <cfinput type="datefield" name="dueDate" style="width:75px;" validate="date" required="yes" message="A valid due date is required">
                    </td>
                </tr>
                <tr>
                    <td valign="top" nowrap>Notes:</td>
                    <td valign="top">
                        <cftextarea name="notes" style="width:300px;" rows="5"></cftextarea>
                    </td>
                </tr>
				<cfif pid is 0 or getsec.seclevelid gt 2>
					<cfinput type="hidden" name="adminUserID" value="#cookie.adminLogin#">
				<cfelse>
					<tr>
						<td valign="top" nowrap>Assigned To: </td>
						<td valign="top">
							<cfselect name="adminUserID" multiple="yes" style="width:300px;" size="10">
								<cfoutput query="getEmployees">
									<option value="#adminuserid#">#lastname#, #firstname#</option>
								</cfoutput>
							</cfselect><br />
							<span class="alert">Hold SHIFT or CTRL to select multiples</span>
						</td>
					</tr>
				</cfif>
                <tr>
                    <td colspan="2" nowrap>
                        <input name="btnSaveTask" type="submit" class="sidebar" id="btnSaveTask" value="Save Task">
                        <input name="btnClearInfo" type="reset" class="sidebar" id="btnClearInfo" value="Clear Form">
						<cfoutput>
						<input type="button" onclick="document.location='index.cfm?userid=#userid#&hd=#hd#&filter_statusid=#filter_statusid#&hp=#hp#';" class="sidebar" id="btnCancel" value="Cancel">
						</cfoutput>
                    </td>
                    </tr>
            </table>
        </td>
    </tr>
</table>
</cfform>
</div>