
<cfparam name="pid" default="0">
<cfset milestoneList = "">
<cfset itemList = "">

<cfquery name="getMilestones" datasource="#ds#">
	select milestoneid from projectTracking_milestones where projectid = #pid# 
</cfquery>
<cfset milestoneList = valuelist(getMilestones.milestoneid)>
<cfif trim(milestoneList) is not "">
	<cfquery name="getItems" datasource="#ds#">
		select itemid from projectTracking_items 
		where milestoneid IN (#milestoneList#) 
	</cfquery>
	<cfset itemList = valuelist(getItems.itemid)>
</cfif>
<cfif trim(itemlist) is not "">
	<cfquery name="deleteItems" datasource="#ds#">
		delete from projectTracking_items where itemid IN (#itemList#)
	</cfquery>
	<cfquery name="deleteItemAssignments" datasource="#ds#">
		delete from projectTracking_item_assignments where itemid IN (#itemList#) 
	</cfquery>
</cfif>
<cfif trim(milestoneList) is not "">
	<cfquery name="deleteMilestones" datasource="#ds#">
		delete from projectTracking_milestones where milestoneID IN (#milestoneList#) 
	</cfquery>
</cfif>
<cfquery name="deleteProjects" datasource="#ds#">
	delete from projectTracking_projects where projectid = #pid# 
</cfquery>
<cfquery name="deleteProjectAssignments" datasource="#ds#">
	delete from projectTracking_assignments where projectid = #pid# 
</cfquery>

<cflocation url="index.cfm?hd=#hd#&filter_statusid=#filter_statusid#&hp=#hp#"> 

