
<link rel="stylesheet" type="text/css" href="/styles.css">

<cfparam name="pid" default="0">
<cfparam name="mid" default="0">
<cfparam name="iid" default="0">

<cfif isDefined("form.btnMove")>
	<cfquery name="moveItem" datasource="#ds#">
		update projectTracking_items 
		set milestoneid = #mid#
		where itemid = #iid# 
	</cfquery>
	<script type="text/javascript">
		window.opener.location = window.opener.location;
		self.close();
	</script>
	<cfabort>
</cfif>

<cfquery name="getProjects" datasource="#ds#">
	select * 
	from projectTracking_projects 
	where projectid in (select projectid from projectTracking_assignments where adminuserid = #cookie.adminlogin#) and statusid <> 4 
	order by projectname asc 
</cfquery>

<cfif pid is not 0>
	<cfquery name="getMilestones" datasource="#ds#">
		select * from projectTracking_milestones where projectid = #pid# 
		order by milestone asc 
	</cfquery>
</cfif>

<cfform method="post" action="movetoproject.cfm">
<cfinput type="hidden" name="iid" value="#iid#">
<div align="center">
    <table width="400" border="0" cellspacing="0" cellpadding="5">
        <tr>
            <td class="highlightbar"><b>Move to a Project </b></td>
        </tr>
        <tr>
            <td class="greyrowbottomnopadding">
                <table width="100%" border="0" cellspacing="0" cellpadding="5">
                    <tr>
                        <td width="17%" nowrap>Project:</td>
                        <td width="83%">
                            <cfselect name="pid" onChange="this.form.submit();">
								<option value=""></option>
								<cfoutput query="getProjects">
									<option value="#projectid#" <cfif getProjects.projectid is pid>selected</cfif>>#projectName#</option>
								</cfoutput>
                            </cfselect>
                        </td>
                    </tr>
					<cfif pid is not 0>
                    <tr>
                        <td nowrap>Milestone:</td>
                        <td>
                            <cfselect name="mid">
								<option value=""></option>
								<cfoutput query="getMilestones">
									<option value="#milestoneid#">#milestone#</option>
								</cfoutput>
                            </cfselect>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <cfinput name="btnMove" type="submit" class="sidebar" id="btnMove" value="Move">
                            <cfinput name="btnCancel" type="submit" class="sidebar" id="btnCancel" value="Cancel">
                        </td>
                    </tr>
					</cfif>
                </table>
            </td>
        </tr>
    </table>
</div>
</cfform>


