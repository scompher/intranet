
<cfset today = dateformat(now(),'mm/dd/yyyy')>
<cfset startDate = dateadd("d",-7,today)>
<cfset toList = "pgregory@copsmonitoring.com">

<style type="text/css">
	TABLE {font-family:Arial, Helvetica, sans-serif; font-size:12px;} 
	.larger {font-size:14px;} 
</style>

<link rel="stylesheet" type="text/css" href="/styles.css">

<cfquery name="getUsers" datasource="#ds#">
	select Admin_Users.*  
	from Admin_Users	
	left join Admin_Users_Departments_Lookup on admin_users.adminuserid = Admin_Users_Departments_Lookup.adminuserid
	where Admin_Users_Departments_Lookup.departmentid = 1 and active = 1 and Admin_Users.adminuserid <> 170 
	order by lastname, firstname 
</cfquery>

<cfloop query="getUsers">

<cfquery name="getDailyItems" datasource="#ds#">
	select 
		projectTracking_items.itemid, 
		projectTracking_items.itemDescription, 
		projectTracking_items.milestoneID, 
		projectTracking_items.type, 
		projectTracking_items.categoryID, 
		projectTracking_items.dueDate, 
		projectTracking_items.statusID, 
		projectTracking_items.lastUpdated, 
		projectTracking_status.status, 
		projectTracking_categories.category 
	from projectTracking_items 
	left join projectTracking_item_assignments on projectTracking_items.itemid = projectTracking_item_assignments.itemid
	left join projectTracking_status on projectTracking_items.statusid = projectTracking_status.statusid 
	left join projectTracking_categories on projectTracking_items.categoryid = projectTracking_categories.categoryid 
	where milestoneID = 0 and projectTracking_item_assignments.adminuserid = #adminuserid# and (projectTracking_items.lastUpdated >= #createodbcdatetime(startDate)# or projectTracking_items.statusID <> 4) 
	order by dueDate asc 
</cfquery>

<cfquery name="getProjects" datasource="#ds#">
	select * 
	from projectTracking_projects 
	left join projectTracking_assignments on projectTracking_projects.projectid = projectTracking_assignments.projectID
	where projectTracking_assignments.adminUserID = #adminuserid# and statusid <> 4 
</cfquery>

<div align="center">
<cfoutput>
<table width="700" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="larger"><b>#getusers.firstname# #getusers.lastname# from #dateformat(startDate,'mm/dd/yyyy')# to #dateformat(now(),'mm/dd/yyyy')#</b></td>
	</tr>
</table>
</cfoutput>
<table width="700" border="0" cellspacing="0" cellpadding="5">
    <tr>
        <td class="larger" style="border-bottom:solid 1px #D7D7D7;"><b>Daily Items for the week</b></td>
    </tr>
    <tr>
        <td>
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td width="45%"><b>Item</b></td>
				    <td width="16%" nowrap="nowrap"><b>Date Due </b></td>
				    <td width="21%" nowrap="nowrap"><b>Category</b></td>
				    <td width="16%" align="center" nowrap="nowrap"><b>Status</b></td>
			    </tr>
				<cfif getDailyItems.recordcount is 0>
					<tr>
						<td colspan="4">There are no daily items created for this user</td>
					</tr>
				<cfelse>
					<cfloop query="GetDailyItems">
						<cfquery name="getNotes" datasource="#ds#">
							select * from projectTracking_notes
							where itemid = #getDailyItems.itemid# 
							order by dateTimeCreated ASC
						</cfquery>
						<cfoutput>
						<tr>
							<td>#getDailyItems.currentRow#. #getDailyItems.itemDescription#</td>
							<td nowrap="nowrap">#dateformat(getDailyItems.dueDate,'mm/dd/yyyy')#</td>
							<td nowrap="nowrap">#getDailyItems.category#&nbsp;</td>
							<td align="center" nowrap="nowrap">#getDailyItems.status#</td>
						</tr>
						</cfoutput>
						<cfset notecount = 0>
						<cfloop query="getNotes">
							<cfif trim(getNotes.note) is not "">
							<cfoutput>
							<tr>
								<td colspan="4">
								<i>#dateformat(dateTimeCreated,'mm/dd/yyyy')# - #note#</i>
								</td>
							</tr>
							<cfset notecount = notecount + 1>
							</cfoutput>
							</cfif>
						</cfloop>
						<tr>
							<td colspan="4" class="linedtop"><img src="/images/spacer.gif" height="1" /></td>
						</tr>
					</cfloop>
				</cfif>
			</table>
		</td>
    </tr>
</table>
<cfif getProjects.recordcount is not 0>
<br />
<br />
<table width="700" border="0" cellpadding="5" cellspacing="0">
    <tr>
        <td class="larger" style="border-bottom:solid 1px #D7D7D7;"><b>Project Items</b></td>
    </tr>
    <tr>
        <td>
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<cfloop query="getProjects">
					<cfquery name="getMilestones" datasource="#ds#">
						select * 
						from projectTracking_milestones
						where projectid = #getProjects.projectid# order by dueDate asc, milestoneid asc 
					</cfquery>
					<cfoutput>
					<tr>
						<td nowrap="NOWRAP" class="larger"><b>#getProjects.projectName#</b></td>
						<td width="100" align="center" nowrap="NOWRAP"><b>Start Date</b></td>
						<td width="100" align="center" nowrap="NOWRAP"><b>Due Date</b></td>
						<td width="125" align="center" nowrap="nowrap"><b>Item Status</b></td>
					</tr>
					<cfset mcount = 1>
					<cfloop query="getMilestones">
						<tr>
							<td>#mcount#. #getMilestones.milestone#</td>
							<td align="center" nowrap="nowrap">&nbsp;</td>
							<td align="center" nowrap="nowrap">#dateformat(getMilestones.dueDate,'mm/dd/yyyy')#</td>
							<td align="center" nowrap="nowrap">&nbsp;</td>
						</tr>
						<cfquery name="getProjectItems" datasource="#ds#">
							select projectTracking_items.*, projectTracking_status.status
							from projectTracking_items 
							left join projectTracking_item_assignments on projectTracking_items.itemID = projectTracking_item_assignments.itemid
							left join projectTracking_status on projectTracking_items.statusID = projectTracking_status.statusid 
							where projectTracking_items.milestoneID = #getMilestones.milestoneID# and projectTracking_item_assignments.adminuserid = #getusers.adminuserid#
							order by projectTracking_items.dueDate, projectTracking_items.itemid asc 
						</cfquery>
						<cfloop query="getProjectItems">
							<tr>
								<td style="padding-left:20px;">&bull; #itemDescription#</td>
								<td align="center" nowrap="nowrap">#dateformat(startDate,'mm/dd/yyyy')#&nbsp;</td>
								<td align="center" nowrap="nowrap">#dateformat(dueDate,'mm/dd/yyyy')#&nbsp;</td>
								<td align="center" nowrap="nowrap">#status#&nbsp;</td>
							</tr>
							<cfquery name="getNotes" datasource="#ds#">
								select * from projectTracking_notes where itemid = #getProjectItems.itemid# and creatorid = #getusers.adminuserid# order by dateTimeCreated asc
							</cfquery>
							<cfloop query="getNotes">
								<tr>
									<td style="padding-left:20px;" colspan="4" valign="top"><i>#dateformat(dateTimeCreated,'mm/dd/yyyy')# - #note#</i></td>
								</tr>
							</cfloop>
						</cfloop>
						<cfset mcount = mcount + 1>
					</cfloop>
					</cfoutput>
					<tr>
						<td colspan="4" align="right" valign="top" style="border-top:solid 1px #D7D7D7;" nowrap="nowrap">&nbsp;</td>
					</tr>
				</cfloop>
			</table>
		</td>
    </tr>
</table>
</cfif>
</div>

</cfloop>

