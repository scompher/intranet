<link rel="stylesheet" type="text/css" href="/styles.css">

<style type="text/css">
.alert-box {
    color:#555;
    border-radius:10px;
    font-family:Tahoma,Geneva,Arial,sans-serif;font-size:11px;
    padding:10px 10px 10px 36px;
    margin:10px; 
	text-align:left;
}
.alert-box span {
    font-weight:bold;
    text-transform:uppercase;
}
.error {
    background:#ffecec url('images/error.png') no-repeat 10px 50%;
    border:1px solid #f5aca6;
}
</style>

<cfparam name="form.milestone" default="">
<cfparam name="form.dueDate" default="">
<cfparam name="mid" default="0">
<cfparam name="err" default="">

<cfif isDefined("form.btnSaveMilestone")>
	<cfquery name="saveMilestone" datasource="#ds#">
		insert into projectTracking_milestones (projectid, milestone, dueDate, statusid) 
		values (#pid#, '#form.milestone#', '#dueDate#', 0) 
	</cfquery>
	<cflocation url="manageMilestones.cfm?pid=#pid#">
</cfif>

<cfif isDefined("form.btnUpdateMilestone")>
	<cfquery name="updateMilestone" datasource="#ds#">
		update projectTracking_milestones 
		set milestone = '#form.milestone#', dueDate = '#form.dueDate#' 
		where milestoneID = #mid# 
	</cfquery>
	<cflocation url="manageMilestones.cfm?pid=#pid#">
</cfif>

<cfif isDefined("url.delete")>
	<cfquery name="deleteMilestone" datasource="#ds#">
		delete from projectTracking_milestones 
		where milestoneid = #mid# 
	</cfquery>
	<cflocation url="manageMilestones.cfm?pid=#pid#">
</cfif>

<cfif isDefined("url.edit")>
	<cfquery name="getMilestone" datasource="#ds#">
		select * from projectTracking_milestones 
		where milestoneid = #mid# 
	</cfquery>
	<cfset form.milestone = getMilestone.milestone>
	<cfset form.dueDate = dateformat(getMilestone.dueDate,'mm/dd/yyyy')>
</cfif>

<cfquery name="getProjectName" datasource="#ds#">
	select * from projectTracking_projects 
	where projectid = #pid# 
</cfquery>

<cfquery name="getMilestones" datasource="#ds#">
	select * from projectTracking_milestones
	where projectid = #pid# 
	order by dueDate asc 
</cfquery>

<div align="center">
<br />
<br />
<cfform method="post" action="manageMilestones.cfm">
<cfinput type="hidden" name="pid" value="#pid#">
<cfinput type="hidden" name="mid" value="#mid#">

<cfif trim(err) is not "">
	<cfloop list="#err#" delimiters="~" index="e">
		<div class="alert-box error" style="width:425px;">
		<cfoutput>#e#</cfoutput>
		</div>
	</cfloop>
</cfif>

<table width="425" border="0" cellpadding="5" cellspacing="0">
    <tr>
        <td class="highlightbar"><b>Manage Milestones</b></td>
    </tr>
    <tr>
        <td class="greyrowbottomnopadding">
            <table width="100%" border="0" cellspacing="0" cellpadding="5">
                <tr>
                    <td width="15%" nowrap="nowrap">Project:</td>
                    <td width="85%">
						<cfoutput>#getProjectName.projectName#</cfoutput>
                    </td>
                </tr>
                <tr>
                    <td nowrap="nowrap">Milestone name: </td>
                    <td>
                        <cfinput type="text" name="milestone" style="width:300px;" value="#form.milestone#" required="yes" message="The milestone name is required" />
                    </td>
                </tr>
                <tr>
                    <td nowrap="nowrap">Due Date: </td>
                    <td>
                        <cfinput type="datefield" name="dueDate" style="width:75px;" value="#form.dueDate#" validate="date" required="yes" message="A valid due date is required" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
					<cfif isDefined("url.edit")>
						<cfinput type="submit" name="btnUpdateMilestone" class="sidebar" value="Update Milestone">
					<cfelse>
						<cfinput type="submit" name="btnSaveMilestone" class="sidebar" value="Add Milestone">
					</cfif>
					<cfinput name="btnClear" type="reset" class="sidebar" value="Clear">
					</td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</cfform>
<br />
<table width="425" border="0" cellpadding="5" cellspacing="0">
    <tr>
        <td class="highlightbar" align="center"><b>Current Milestones </b></td>
    </tr>
	<tr>
		<td class="greyrowbottomnopadding">
		<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<cfif getMilestones.recordCount gt 0>
				<cfoutput query="getMilestones">
				<tr>
					<td width="0" align="center" nowrap="nowrap">
						<a href="manageMilestones.cfm?pid=#pid#&mid=#getMilestones.milestoneid#&edit=true">[edit]</a>
						<a onclick="return confirm('Are you sure you wish to delete this milestone?');" href="manageMilestones.cfm?pid=#pid#&mid=#getMilestones.milestoneid#&delete=true">[delete]</a>
					</td>
					<td width="200">#getMilestones.currentRow#. #milestone#</td>
					<td width="100" nowrap="nowrap">Due: #dateformat(dueDate,'mm/dd/yyyy')#</td>
				</tr>
				</cfoutput>
			<cfelse>
				<tr><td>There are no milestones currently attached to this project</td></tr>
			</cfif>
		</table>
		</td>
	</tr>
</table>
<br />
<cfoutput>
<a class="normal" style="text-decoration:underline;" href="manageTasks.cfm?pid=#pid#">Continue to Manage Tasks</a>
 | 
<a class="normal" style="text-decoration:underline;" href="/projectTracking/index.cfm">Return to My Tasks and Projects</a>

</cfoutput>
</div>


