
<cfquery name="getProjects" datasource="#ds#">
	select projectTracking_projects.*, admin_users.firstname, admin_users.lastname 
	from projectTracking_projects 
	left join projectTracking_assignments on projectTracking_projects.projectid = projectTracking_assignments.projectid
	left join admin_users on projectTracking_assignments.adminuserid = admin_users.adminuserid 
	order by dueDate, lastname, firstname 
</cfquery>

<link rel="stylesheet" type="text/css" href="/styles.css">
<div align="center">
<table width="755" border="0" cellspacing="0" cellpadding="5">
    <tr>
        <td class="highlightbar"><b>Project Menu</b> (click project to view details)</td>
    </tr>
    <tr>
        <td class="greyrowbottomnopadding">
            <table width="100%" border="0" cellspacing="0" cellpadding="5">
                <tr>
                    <td width="13%" align="center" class="linedrowrightcolumn"><b>Action</b></td>
                    <td width="52%" class="linedrowrightcolumn"><b>Project</b></td>
                    <td width="14%" align="center" class="linedrowrightcolumn"><b>Due Date </b></td>
                    <td width="21%" align="center" class="linedrow"><b>Assigned To </b></td>
                </tr>
				<cfoutput query="getProjects" group="projectid">
                <tr>
                    <td align="center" valign="top" nowrap="nowrap" class="linedrowrightcolumn">
					<a href="editProject.cfm?pid=#projectid#">[edit]</a>
					<a onclick="return confirm('Deleting this project will remove all milestones and tasks associated with it.  Continue?');" href="deleteProject.cfm?pid=#projectid#">[delete]</a>					</td>
                    <td valign="top" nowrap="nowrap" class="linedrowrightcolumn">#projectName#</td>
                    <td align="center" valign="top" nowrap="NOWRAP" class="linedrowrightcolumn">#dateformat(dueDate,'mm/dd/yyyy')#</td>
                    <td align="left" valign="top" nowrap="NOWRAP" class="linedrow">
					<cfoutput>
					#firstname# #lastname#<br />
					</cfoutput>
					</td>
                </tr>
				</cfoutput>
                <tr>
                    <td align="center" class="linedrowrightcolumn"><a href="newProject.cfm">[New Project]</a></td>
                    <td class="linedrow">&nbsp;</td>
                    <td align="center" class="linedrow">&nbsp;</td>
                    <td align="center" class="linedrow">&nbsp;</td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<br />
<a href="/index.cfm" class="normal" style="text-decoration:underline;">Return to Intranet</a>
</div>
