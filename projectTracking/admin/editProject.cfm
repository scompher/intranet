<link rel="stylesheet" type="text/css" href="/styles.css">

<cfparam name="pid" default="0">

<cfif isDefined("form.btnSaveProject")>
	<cfquery name="saveProject" datasource="#ds#">
		update projectTracking_projects 
		set projectName = '#projectName#', description = '#projectDescription#', dueDate = '#dueDate#', dateStarted = '#dateStarted#', comments = '#comments#' 
		where projectid = #pid# 
	</cfquery>
	<cftransaction>
		<cfquery name="DelAssignments" datasource="#ds#">
			delete from projectTracking_assignments 
			where projectid = #pid# 
		</cfquery>	
		<cfloop list="#form.adminUserID#" index="uid">
			<cfquery name="assignResource" datasource="#ds#">
				insert into projectTracking_assignments (adminUserID, projectID) 
				values (#uid#, #pid#) 
			</cfquery>
		</cfloop>
	</cftransaction>
	<cfoutput>
		<meta http-equiv="refresh" content="0;url=manageMilestones.cfm?pid=#pid#" />
	</cfoutput>
	<cfabort>
</cfif>

<cfquery name="getEmployees" datasource="#ds#">
	select * from admin_users 
	where adminuserid in (select adminuserid from Admin_Users_Departments_Lookup where departmentid = #getsec.defaultdeptid#) and active = 1 
	order by lastname, firstname 
</cfquery>

<cfquery name="getProject" datasource="#ds#">
	select * from projectTracking_projects where projectid = #pid# 
</cfquery>

<cfquery name="getAssignments" datasource="#ds#">
	select * from projectTracking_assignments where projectid = #pid# 
</cfquery>
<cfset assignedUserList = valuelist(getAssignments.adminuserid)>

<div align="center">
<br>
<cfform method="post" action="editProject.cfm">
<cfinput type="hidden" name="pid" value="#pid#">
<table border="0" cellspacing="0" cellpadding="5">
    <tr>
        <td class="highlightbar"><b>Edit  Project </b></td>
    </tr>
    <tr>
        <td class="greyrowbottomnopadding">
            <table width="100%" border="0" cellspacing="0" cellpadding="5">
				<cfoutput query="getProject">
                <tr>
                    <td width="16%" nowrap>Project Name: </td>
                    <td width="84%">
                        <cfinput type="text" name="projectName" style="width:300px;" value="#projectName#">
                    </td>
                </tr>
                <tr>
                    <td nowrap>Description:</td>
                    <td>
                        <cfinput type="text" name="projectDescription" style="width:300px;" value="#description#">
                    </td>
                </tr>
                <tr>
                    <td nowrap>Date Started: </td>
                    <td>
                        <cfinput type="datefield" name="dateStarted" style="width:75px;" value="#dateformat(dateStarted,'mm/dd/yyyy')#"></td>
                </tr>
                <tr>
                    <td nowrap>Due Date: </td>
                    <td>
                        <cfinput type="datefield" name="dueDate" style="width:75px;" value="#dateformat(dueDate,'mm/dd/yyyy')#"></td>
                </tr>
                <tr>
                    <td valign="top" nowrap>Notes/Comments:</td>
                    <td valign="top">
                        <cftextarea name="comments" style="width:300px;" rows="5">#comments#</cftextarea></td>
                </tr>
				</cfoutput>
                <tr>
                    <td valign="top" nowrap="nowrap">Assign Resources: </td>
                    <td valign="top">
						<cfselect name="adminUserID" multiple="yes" style="width:300px;" size="10">
							<cfoutput query="getEmployees">
								<option <cfif listfindnocase(assignedUserList,getEmployees.adminuserid) is not 0>selected</cfif> value="#adminuserid#">#lastname#, #firstname#</option>
							</cfoutput>
						</cfselect><br />
						<span class="alert">Hold SHIFT or CTRL to select multiples</span>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <cfinput name="btnSaveProject" type="submit" class="sidebar" value="Update Project and Manage Milestones">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</cfform>
<br />
<a href="index.cfm" class="normal" style="text-decoration:underline;">Return to project list</a>
</div>

