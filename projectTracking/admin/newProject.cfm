
<cfif isDefined("form.btnSaveProject")>
	<cfquery name="saveProject" datasource="#ds#">
		insert into projectTracking_projects (projectName, description, dueDate, statusID, comments, active, deleted, creatorID, dateTimeCreated, dateStarted) 
		values ('#form.projectName#', '#form.projectDescription#', '#dueDate#', 1, '#form.comments#', 1, 0, #cookie.adminLogin#, GETDATE(), '#dateStarted#') 
	</cfquery>
	<cfquery name="getPID" datasource="#ds#">
		select max(projectid) as pid from projectTracking_projects where creatorID = #cookie.adminLogin# 
	</cfquery>
	<cfloop list="#form.adminUserID#" index="uid">
		<cfquery name="assignResource" datasource="#ds#">
			insert into projectTracking_assignments (adminUserID, projectID) 
			values (#uid#, #getPID.pid#) 
		</cfquery>
	</cfloop>
	<cfoutput>
		<meta http-equiv="refresh" content="0;url=manageMilestones.cfm?pid=#getPID.pid#" />
	</cfoutput>
</cfif>

<link rel="stylesheet" type="text/css" href="/styles.css">

<cfquery name="getEmployees" datasource="#ds#">
	select * from admin_users 
	where adminuserid in (select adminuserid from Admin_Users_Departments_Lookup where departmentid = #getsec.defaultdeptid#) and active = 1 
	order by lastname, firstname 
</cfquery>

<cfform method="post" action="newProject.cfm">
<div align="center">
<br>
<table border="0" cellspacing="0" cellpadding="5">
    <tr>
        <td class="highlightbar"><b>Enter New Project </b></td>
    </tr>
    <tr>
        <td class="greyrowbottomnopadding">
            <table width="100%" border="0" cellspacing="0" cellpadding="5">
                <tr>
                    <td width="16%" nowrap>Project Name: </td>
                    <td width="84%">
                        <cfinput type="text" name="projectName" style="width:300px;">
                    </td>
                </tr>
                <tr>
                    <td nowrap>Description:</td>
                    <td>
                        <cfinput type="text" name="projectDescription" style="width:300px;">
                    </td>
                </tr>
                <tr>
                    <td nowrap>Date Started: </td>
                    <td style="z-index:1; position:relative;">
                        <cfinput type="datefield" name="dateStarted" style="width:75px;"></td>
                </tr>
                <tr>
                    <td nowrap>Due Date: </td>
                    <td>
                        <cfinput type="datefield" name="dueDate" style="width:75px;"></td>
                </tr>
                <tr>
                    <td valign="top" nowrap>Notes/Comments:</td>
                    <td valign="top">
                        <cftextarea name="comments" style="width:300px;" rows="5"></cftextarea></td>
                </tr>
                <tr>
                    <td valign="top" nowrap="nowrap">Assign Resources: </td>
                    <td valign="top">
						<cfselect name="adminUserID" multiple="yes" style="width:300px;" size="10">
							<cfoutput query="getEmployees">
								<option value="#adminuserid#">#lastname#, #firstname#</option>
							</cfoutput>
						</cfselect><br />
						<span class="alert">Hold SHIFT or CTRL to select multiples</span>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <cfinput name="btnSaveProject" type="submit" class="sidebar" value="Create Project and Manage Milestones">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<br />
<a href="index.cfm" class="normal" style="text-decoration:underline;">Return to project list</a>
</div>
</cfform>
