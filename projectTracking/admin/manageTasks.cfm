
<link rel="stylesheet" type="text/css" href="/styles.css">

<cfparam name="pid" default="0">
<cfparam name="mid" default="0">

<cfif isDefined("form.btnAddTask")>
	<cflocation url="newTask.cfm?mid=#mid#&pid=#pid#&type=project">
	<cfabort>
</cfif>

<cfquery name="getProjects" datasource="#ds#">
	select * from projectTracking_projects
	<cfif pid is not 0>
		where projectid = #pid# 
	</cfif>
	order by projectName asc 
</cfquery>

<cfquery name="getMilestones" datasource="#ds#">
	select * from projectTracking_milestones
	<cfif pid is not 0>
		where projectid = #pid# 
	</cfif>
</cfquery>

<cfquery name="getTasks" datasource="#ds#">
	select 
		projectTracking_items.itemID, 
		projectTracking_items.itemDescription, 
		projectTracking_items.type, 
		projectTracking_items.categoryID, 
		projectTracking_items.dueDate as itemDueDate, 
		projectTracking_items.statusID, 
		projectTracking_items.lastUpdated, 
		projectTracking_projects.projectName, 
		projectTracking_milestones.milestone, 
		admin_users.firstname, 
		admin_users.lastname 
	from projectTracking_items 
	left join projectTracking_milestones on projectTracking_items.milestoneID = projectTracking_milestones.milestoneid
	left join projectTracking_projects on projectTracking_projects.projectid = projectTracking_milestones.projectid 
	left join projectTracking_item_assignments on projectTracking_items.itemid = projectTracking_item_assignments.itemid 
	left join Admin_Users on projectTracking_item_assignments.adminUserID = admin_users.adminuserid 
	where 
	<cfif pid is not 0>
		projectTracking_projects.projectid = #pid# and 
	</cfif>
	<cfif mid is not 0>
		projectTracking_items.milestoneID = #mid# and 
	</cfif>
	1=1
	order by projectTracking_items.dueDate, lastname, firstname 
</cfquery>

<div align="center">
<cfform method="post" action="manageTasks.cfm">
<table width="600" border="0" cellspacing="0" cellpadding="5">
    <tr>
        <td class="highlightbar"><b>Manage Tasks </b></td>
    </tr>
    <tr>
        <td class="greyrowbottomnopadding">
            <table width="100%" border="0" cellspacing="0" cellpadding="5">
                <tr>
                    <td width="11%" nowrap>Project:</td>
                    <td width="89%">
                        <cfselect name="pid" style="vertical-align:middle;">
							<option value="0">All</option>
							<cfoutput query="getProjects">
							<option <cfif pid is projectid>selected</cfif> value="#projectid#">#projectName#</option>
							</cfoutput>
						</cfselect>
                    </td>
                </tr>
                <tr>
                    <td nowrap>Milestone:</td>
                    <td>
                        <cfselect name="mid" style="vertical-align:middle;">
							<option value="0">All</option>
							<cfoutput query="getMilestones">
							<option <cfif mid is milestoneid>selected</cfif> value="#milestoneid#">#milestone#</option>
							</cfoutput>
						</cfselect>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <input name="Submit" type="submit" class="sidebar" value="Apply">
                    </td>
                    </tr>
            </table>
        </td>
    </tr>
</table>
<br>
<table width="600" border="0" cellspacing="0" cellpadding="5">
    <tr>
        <td class="highlightbar"><b>Existing Tasks </b></td>
    </tr>
    <tr>
        <td class="greyrowbottomnopadding">
            <table width="100%" border="0" cellspacing="0" cellpadding="5">
                <tr>
					<td><b>Action</b></td>
                    <td><b>Project</b></td>
                    <td><b>Milestone</b></td>
                    <td><b>Task</b></td>
					<td><b>Assigned To</b></td>
                    <td><b>Due Date </b></td>
                </tr>
				<cfif getTasks.recordcount gt 0>
					<cfoutput query="getTasks" group="itemid">
					<tr>
						<td valign="top"><a onclick="return confirm('Are you sure you wish to delete this task?');" href="deleteTask.cfm?pid=#pid#&tid=#itemid#&mid=#mid#">[delete]</a></td>
						<td valign="top">#getTasks.projectName#</td>
						<td valign="top">#getTasks.milestone#</td>
						<td valign="top">#getTasks.itemDescription#</td>
						<td valign="top">
						    <cfoutput>#getTasks.firstname# #getTasks.lastname#<br /></cfoutput></td>
						<td valign="top">#dateformat(getTasks.itemDueDate,'mm/dd/yyyy')#</td>
					</tr>
					</cfoutput>
				<cfelse>
					<tr>
						<td colspan="4">There are no associated tasks</td>
					</tr>
				</cfif>
				<tr>
					<td colspan="2"><cfinput type="submit" name="btnAddTask" class="sidebar" value="Add New Task">
					</td>
				</tr>
            </table>
        </td>
    </tr>
</table>
</cfform>
<br />
<cfoutput>
<a class="normal" style="text-decoration:underline;" href="manageMilestones.cfm?pid=#pid#">Return to Milestones</a>
</cfoutput>
</div>
