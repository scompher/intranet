<cffunction name="saveItem" returntype="boolean" access="private">
	<cfargument name="form" type="struct" required="yes">

	<cfset result = false>

	<cfquery name="saveItem" datasource="#ds#">
		insert into All_Lines (phoneNumber, usedFor, description, singleCo, whoOwns, whoPays, DNIS, connectionType, linkedTo, linkMethod, HUNT, trunk, extLinkedTo, dlrNumber, dlrName, comments, active, location, orderPending, orderDetails, carrier, account, circuitID, trunkGroup, [c-24-profile], [3-digit], [pad-with], payPhoneBlock, flatRate, lastModified, lastModifiedBy, dealerLetter, accountRanges, primacy, twoWay, private, orderDate, orderDueDate, altDnis, altTrunkGroup, altExtLink, alternateSwitchedTerm, alternateLinkMethod, recDnis)  
		values ('#form.phoneNumber#', '#form.usedFor#', '#form.description#', '#form.singleCo#', '#form.whoOwns#', '#form.whoPays#', '#form.DNIS#', '#form.connectionType#', '#form.linkedTo#', '#form.getLinkMethod#', '#form.huntGroup#', '#form.trunk#', '#form.extLink#', '#form.dealerNumber#', '#form.dealerName#', '#form.comments#', '#form.active#', '#form.location#', '#form.orderPending#', '#form.orderDetails#', '#form.carrier#', '#form.account#', '#form.circuitID#', '#form.trunkGroup#', '#form.c24Profile#', '#form.threeDigit#', '#form.padWith#', '#form.payPhoneBlock#', '#form.whoPaysRate#', #createodbcdatetime(now())#, #cookie.adminlogin#, '#form.dealerLetter#', '#form.accountRanges#', #primacy#, #twoWay#, #private#, 
		<cfif trim(form.orderDate) is not "">
			'#form.orderDate#', 
		<cfelse>
			NULL, 
		</cfif> 
		<cfif trim(form.orderDueDate) is not "">
			'#form.orderDueDate#', 
		<cfelse>
			NULL,  
		</cfif>
		'#form.altDnis#', '#form.altTrunkGroup#', '#form.altExtLink#', '#form.alternateSwitchedTerm#', '#form.alternateLinkMethod#', '#form.recDNIS#'
		)
	</cfquery>

	<cfquery name="getID" datasource="#ds#">
		select max(id) as newid from all_lines 
		where phoneNumber = '#form.phoneNumber#'
	</cfquery>
	<cfset form.id = getID.newid>

	<cfquery name="saveReceiverLookup" datasource="#ds#">
		begin
			delete from receiver_lookup where lineid = #form.id# 
		end
		<cfloop list="#form.associatedReceivers#" index="rid">
		<cfif trim(rid) is not 0>
			begin
				insert into receiver_lookup (lineid, receiverid) 
				values (#form.id#, #rid#)
			end
		</cfif>
		</cfloop>
	</cfquery>

	<cfquery name="saveDealerLookup" datasource="#ds#">
		begin
			delete from dealer_lookup where lineid = #form.id# 
		end
		<cfloop list="#form.associatedDealers#" index="did">
		<cfif trim(did) is not 0>
			begin
				insert into dealer_lookup (lineid, dealerid) 
				values (#form.id#, #did#)
			end
		</cfif>
		</cfloop>
	</cfquery>


	<cfset result = true>

	<cfreturn result>
</cffunction>

<cffunction name="updateItem" returntype="boolean" access="private">
	<cfargument name="form" type="struct" required="yes">

	<cfset result = false>
<!--- 
	<cfif trim(form.orderDate) is "">
		<cfset form.orderDate = "">
	<cfelse>
		<cfset form.orderDate = createodbcdate(form.orderDate)>
	</cfif>

	<cfif trim(form.orderDueDate) is "">
		<cfset form.orderDueDate = "">
	<cfelse>
		<cfset form.orderDueDate = createodbcdate(form.orderDueDate)>
	</cfif>
 --->

	<cfquery name="updateItem" datasource="#ds#">
		update All_Lines 
		set 
		phoneNumber = '#form.phoneNumber#', 
		<!--- receiver = '#form.receiver#',  --->
		usedFor = '#form.usedFor#', 
		description = '#form.description#', 
		singleCo = '#form.singleCo#', 
		whoOwns = '#form.whoOwns#', 
		whoPays = '#form.whoPays#', 
		DNIS = '#form.DNIS#', 
		connectionType = '#form.connectionType#', 
		linkedTo = '#form.linkedTo#', 
		linkMethod = '#form.getLinkMethod#', 
		HUNT = '#form.huntGroup#', 
		trunk = '#form.trunk#', 
		extLinkedTo = '#form.extLink#', 
		dlrNumber = '#form.dealerNumber#', 
		dlrName = '#form.dealerName#', 
		comments = '#form.comments#', 
		active = '#form.active#', 
		location = '#form.location#', 
		orderPending = '#form.orderPending#', 
		orderDetails = '#form.orderDetails#', 
		carrier = '#form.carrier#', 
		account = '#form.account#', 
		circuitID = '#form.circuitID#', 
		trunkGroup = '#form.trunkGroup#', 
		[c-24-profile] = '#form.c24Profile#', 
		[3-digit] = '#form.threeDigit#', 
		[pad-with] = '#form.padWith#', 
		payPhoneBlock = '#form.payPhoneBlock#', 
		flatRate = '#form.whoPaysRate#', 
		lastModified = #createodbcdatetime(now())#, 
		lastModifiedBy = #cookie.adminlogin#, 
		dealerLetter = '#form.dealerLetter#', 
		accountRanges = '#form.accountRanges#', 
		primacy = #form.primacy#, 
		twoWay = #form.twoWay#, 
		private = #form.private#, 
		<cfif trim(form.orderDate) is not "">
			orderDate = '#form.orderDate#', 
		<cfelse>
			orderDate = NULL, 
		</cfif>
		<cfif trim(form.orderDueDate) is not "">
			orderDueDate = '#form.orderDueDate#', 
		<cfelse>
			orderDueDate = NULL, 
		</cfif>
		altDnis = '#form.altDnis#', 
		altTrunkGroup = '#form.altTrunkGroup#', 
		altExtLink = '#form.altExtLink#', 
		alternateSwitchedTerm = '#alternateSwitchedTerm#', 
		alternateLinkMethod = '#alternateLinkMethod#', 
		recDnis = '#form.recDNIS#' 
		where id = #form.id#
	</cfquery>

	<cfquery name="saveReceiverLookup" datasource="#ds#">
		begin
			delete from receiver_lookup where lineid = #form.id# 
		end
		<cfloop list="#form.associatedReceivers#" index="rid">
		<cfif trim(rid) is not 0>
			begin
				insert into receiver_lookup (lineid, receiverid) 
				values (#form.id#, #rid#)
			end
		</cfif>
		</cfloop>
	</cfquery>

	<cfquery name="saveDealerLookup" datasource="#ds#">
		begin
			delete from dealer_lookup where lineid = #form.id# 
		end
		<cfloop list="#form.associatedDealers#" index="did">
		<cfif trim(did) is not 0>
			begin
				insert into dealer_lookup (lineid, dealerid) 
				values (#form.id#, #did#)
			end
		</cfif>
		</cfloop>
	</cfquery>

	<cfset result = true>

	<cfreturn result>
</cffunction>

<cffunction name="validateInfo" returntype="string" access="private">
	<cfargument name="form" type="struct" required="yes">
		<cfset err = "">
		<cfif trim(form.phoneNumber) is "">
			<cfset err = listappend(err,"[phoneNumber]~The phone number is required.")>
		</cfif>
		<cfif trim(connectionType) is "">
			<cfset err = listappend(err,"[connectionType]~The connection type is required.")>
		</cfif>
		<cfif trim(usedFor) is "">
			<cfset err = listappend(err,"[usedFor]~Used For is required.")>
		</cfif>
		<cfif trim(carrier) is "">
			<cfset err = listappend(err,"[carrier]~The carrier is required.")>
		</cfif>
		<cfif trim(location) is "">
			<cfset err = listappend(err,"[location]~The location is required.")>
		</cfif>
		<cfif trim(whoOwns) is "">
			<cfset err = listappend(err,"[whoOwns]~Who Owns is required.")>
		</cfif>
		<cfif trim(whoPays) is "">
			<cfset err = listappend(err,"[whoPays]~Who Pays is required.")>
		</cfif>
		<cfif trim(twoWay) is "">
			<cfset err = listappend(err,"[twoWay]~Two Way is required.")>
		</cfif>
	<cfreturn err>
</cffunction>

<cffunction name="SearchItems" access="private" returntype="query">
	<cfargument name="form" type="struct" required="yes">
	
	<cfif trim(form.sortResults) is "">
		<cfset form.sortResults = "dealerNumberASC">
	</cfif>
	
	<cfswitch expression="#form.sortResults#">
		<cfcase value="phoneASC"><cfset orderby = "phoneNumber asc, primacy asc"></cfcase>
		<cfcase value="phoneDESC"><cfset orderby = "phoneNumber desc, primacy asc"></cfcase>
		<cfcase value="dnisASC"><cfset orderby = "dnis asc, primacy asc"></cfcase>
		<cfcase value="dnisDESC"><cfset orderby = "dnis desc, primacy asc"></cfcase>
		<cfcase value="receiverASC"><cfset orderby = "receiver asc, primacy asc"></cfcase>
		<cfcase value="receiverDESC"><cfset orderby = "receiver desc, primacy asc"></cfcase>
		<cfcase value="dealerNumberASC"><cfset orderby = "dlrNumber asc, primacy asc"></cfcase>
		<cfcase value="dealerNumberDESC"><cfset orderby = "dlrNumber desc, primacy asc"></cfcase>
	</cfswitch>
	
	<cfquery name="getItems" datasource="#ds#">
		select distinct 
			All_Lines.ID, 
			All_Lines.phoneNumber, 
			All_Lines.DNIS, 
			All_Lines.ExtLinkedTo, 
			All_Lines.LinkedTo, 
			receivers.receiver, 
			dealers.dealerNumber as dlrNumber, 
			dealers.dealerName as dlrName, 
			dealers.isSubDealer, 
			All_Lines.usedFor, 
			All_Lines.active, 
			All_Lines.orderPending, 
			All_lines.private, 
			All_lines.primacy, 
			All_lines.recDnis   
		from All_Lines 
		left join receiver_lookup on all_lines.ID = Receiver_Lookup.lineID
		left join Receivers on Receiver_Lookup.receiverID = receivers.receiveriD 
		left join Dealer_Lookup on all_lines.ID = Dealer_Lookup.lineID 
		left join dealers on Dealer_Lookup.DealerID = dealers.dealerID 
		where ID IN (
			select distinct all_lines.ID 
			from all_lines 
			left join receiver_lookup on all_lines.ID = Receiver_Lookup.lineID
			left join Receivers on Receiver_Lookup.receiverID = receivers.receiveriD 
			left join Dealer_Lookup on all_lines.ID = Dealer_Lookup.lineID 
			left join dealers on Dealer_Lookup.DealerID = dealers.dealerID 
			where 
			<cfif trim(form.searchPhoneNumber) is not "">
				All_Lines.phoneNumber like '#trim(replace(form.searchPhoneNumber, "*", "%", "all"))#' and 
			</cfif>
			<cfif trim(form.searchDnis) is not "">
				(All_Lines.dnis like '#trim(replace(form.searchDnis, "*", "%", "all"))#' OR All_Lines.ExtLinkedTo like '#trim(replace(form.searchDnis, "*", "%", "all"))#' OR All_Lines.recDNIS like '#trim(replace(form.searchDnis, "*", "%", "all"))#') and 
			</cfif>
			<cfif trim(form.searchReceiver) is not ""> 
				receivers.receiver like '#trim(replace(form.searchReceiver, "*", "%", "all"))#' and 
			</cfif>
			<cfif trim(form.searchDealerNumber) is not ""> 
				dealers.dealerNumber like '#trim(replace(form.searchDealerNumber, "*", "%", "all"))#' and 
			</cfif>
			<cfif trim(form.searchUsedFor) is not ""> 
				All_Lines.usedFor like '#trim(form.searchUsedFor)#' and 
			</cfif>
			1=1 
		)
		<cfif trim(form.sortResults) is not "">
			order by #orderby#
		</cfif>
	</cfquery>
	<cfreturn getItems>
</cffunction>

<cffunction name="SearchSMDRItems" access="private" returntype="query">
	<cfargument name="form" type="struct" required="yes">
	
	<cfswitch expression="#form.sortResults#">
		<cfcase value="phoneASC"><cfset orderby = "phoneNumber asc"></cfcase>
		<cfcase value="phoneDESC"><cfset orderby = "phoneNumber desc"></cfcase>
		<cfcase value="dnisASC"><cfset orderby = "dnis asc"></cfcase>
		<cfcase value="dnisDESC"><cfset orderby = "dnis desc"></cfcase>
		<cfcase value="receiverASC"><cfset orderby = "receiver asc"></cfcase>
		<cfcase value="receiverDESC"><cfset orderby = "receiver desc"></cfcase>
		<cfcase value="dealerNumberASC"><cfset orderby = "dlrNumber asc"></cfcase>
		<cfcase value="dealerNumberDESC"><cfset orderby = "dlrNumber desc"></cfcase>
	</cfswitch>
	
	<cfquery name="getItems" datasource="#ds#">
		select * from all_lines 
		where 
		<cfif trim(form.searchPhoneNumber) is not "">
			phoneNumber like '#trim(replace(form.searchPhoneNumber, "*", "%", "all"))#' and 
		</cfif>
		<cfif trim(form.searchDnis) is not "">
			extlinkedto like '#trim(replace(form.searchDnis, "*", "%", "all"))#' and 
		</cfif>
		<cfif trim(form.searchReceiver) is not ""> 
			receiver like '#trim(replace(form.searchReceiver, "*", "%", "all"))#' and 
		</cfif>
		<cfif trim(form.searchDealerNumber) is not ""> 
			dlrNumber like '#trim(replace(form.searchDealerNumber, "*", "%", "all"))#' and 
		</cfif>
		<cfif trim(form.searchUsedFor) is not ""> 
			usedFor like '#trim(form.searchUsedFor)#' and 
		</cfif>
		1=1 
		<cfif trim(form.sortResults) is not "">
			order by #orderby#
		</cfif>
	</cfquery>
	<cfreturn getItems>
</cffunction>

<cffunction name="validateReportForm" access="private" returntype="string">
	<cfargument type="struct" name="form" required="yes">
	<cfset err = "">
	
	<cfif not isDefined("form.includeColumn")>
		<cfset err = listappend(err,"Please select which fields to include on the report.")>
	</cfif>
		
	<cfreturn err>
</cffunction>

<cffunction name="validateUpdateForm" access="private" returntype="string">
	<cfargument type="struct" name="form" required="yes">
	<cfset err = "">
	
	<cfif trim(form.setStatement) is "">
		<cfset err = listappend(err,"Please select which fields to update on the report.")>
	</cfif>
		
	<cfreturn err>
</cffunction>

<cffunction name="deleteItems" access="private" returntype="numeric">
	<cfargument type="string" name="IDList" required="yes">
		
		<cfquery name="delItems" datasource="#ds#">
			delete from all_lines 
			where ID IN (#IDList#)
		</cfquery>
		
	<cfreturn listlen(idlist)>
</cffunction>
