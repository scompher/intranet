<script type="text/javascript">
	function resizeMe(t) {
		if (t == 'dealer') {
			window.resizeTo(450,280);
		} else {
			window.resizeTo(450,200);
		}
	}
</script>

<cfset ds = "allLines">

<link rel="stylesheet" type="text/css" href="../styles.css">

<cfif isDefined("form.btnUpdate")>
	<cfswitch expression="#type#">
		<cfcase value="connectionType">
			<cfquery name="updateItem" datasource="#ds#">
				update connection 
				set type = '#form.item#' 
				where id = #form.itemid#
			</cfquery>
		</cfcase>
		<cfcase value="usedFor">
			<cfquery name="updateItem" datasource="#ds#">
				update UsedFor 
				set UsedFor = '#form.item#' 
				where id = #form.itemid#
			</cfquery>
		</cfcase>
		<cfcase value="linkMethod">
			<cfquery name="updateItem" datasource="#ds#">
				update linkMethod 
				set method = '#form.item#' 
				where id = #form.itemid#
			</cfquery>
		</cfcase>
		<cfcase value="carrier">
			<cfquery name="updateItem" datasource="#ds#">
				update carrier 
				set name = '#form.item#' 
				where id = #form.itemid#
			</cfquery>
		</cfcase>
		<cfcase value="accountNumber">
			<cfquery name="updateItem" datasource="#ds#">
				update accounts 
				set account = '#form.item#' 
				where id = #form.itemid#
			</cfquery>
		</cfcase>
		<cfcase value="trunkGroup">
			<cfquery name="updateItem" datasource="#ds#">
				update TrunkGroup 
				set TrunkGrp = '#form.item#' 
				where id = #form.itemid#
			</cfquery>
		</cfcase>
		<cfcase value="location">
			<cfquery name="updateItem" datasource="#ds#">
				update location 
				set location = '#form.item#' 
				where id = #form.itemid#
			</cfquery>
		</cfcase>
		<cfcase value="whoOwns">
			<cfquery name="updateItem" datasource="#ds#">
				update WhoOwns 
				set owner = '#form.item#' 
				where id = #form.itemid#
			</cfquery>
		</cfcase>
		<cfcase value="whoPays">
			<cfquery name="updateItem" datasource="#ds#">
				update WhoPays 
				set WhoPays = '#form.item#' 
				where id = #form.itemid#
			</cfquery>
		</cfcase>
		<cfcase value="whoPaysRate">
			<cfquery name="getItems" datasource="#ds#">
				update FlatRate 
				set Rate = '#form.item#' 
				where id = #form.itemid#
			</cfquery>
		</cfcase>
		<cfcase value="receiver">
			<cfquery name="getItems" datasource="#ds#">
				update receivers 
				set receiver = '#form.item#' 
				where receiverid = #form.itemid#
			</cfquery>
		</cfcase>
		<cfcase value="dealer">
			<cfparam name="form.isSubDealer" default="0">
			<cfquery name="getItems" datasource="#ds#">
				update dealers 
				set dealerNumber = '#form.dealerNumber#', dealerName = '#form.dealerName#', isSubDealer = #form.isSubDealer# 
				where dealerID = #form.itemid#
			</cfquery>
		</cfcase>
	</cfswitch>
	<script type="text/javascript" language="javascript">
		opener.document.forms[0].submit();
		window.close();
	</script>
	<cfabort>
</cfif>

<cfif isDefined("form.btnSave")>
	<cfswitch expression="#type#">
		<cfcase value="connectionType">
			<cfquery name="saveItem" datasource="#ds#">
				insert into connection (type) 
				values ('#form.item#')
			</cfquery>
		</cfcase>
		<cfcase value="usedFor">
			<cfquery name="saveItem" datasource="#ds#">
				insert into UsedFor (UsedFor) 
				values ('#form.item#')
			</cfquery>
		</cfcase>
		<cfcase value="linkMethod">
			<cfquery name="saveItem" datasource="#ds#">
				insert into linkMethod (method) 
				values ('#form.item#')
			</cfquery>
		</cfcase>
		<cfcase value="carrier">
			<cfquery name="saveItem" datasource="#ds#">
				insert into carrier (name) 
				values ('#form.item#')
			</cfquery>
		</cfcase>
		<cfcase value="accountNumber">
			<cfquery name="saveItem" datasource="#ds#">
				insert into accounts (account) 
				values ('#form.item#')
			</cfquery>
		</cfcase>
		<cfcase value="trunkGroup">
			<cfquery name="saveItem" datasource="#ds#">
				insert into TrunkGroup (TrunkGrp) 
				values ('#form.item#')
			</cfquery>
		</cfcase>
		<cfcase value="location">
			<cfquery name="saveItem" datasource="#ds#">
				insert into location (location) 
				values ('#form.item#')
			</cfquery>
		</cfcase>
		<cfcase value="whoOwns">
			<cfquery name="saveItem" datasource="#ds#">
				insert into WhoOwns (owner) 
				values ('#form.item#')
			</cfquery>
		</cfcase>
		<cfcase value="whoPays">
			<cfquery name="saveItem" datasource="#ds#">
				insert into WhoPays (WhoPays) 
				values ('#form.item#')
			</cfquery>
		</cfcase>
		<cfcase value="whoPaysRate">
			<cfquery name="saveItem" datasource="#ds#">
				insert into FlatRate (Rate) 
				values ('#form.item#')
			</cfquery>
		</cfcase>
		<cfcase value="receiver">
			<cfquery name="saveItem" datasource="#ds#">
				insert into receivers (receiver) 
				values ('#form.item#')
			</cfquery>
		</cfcase>
		<cfcase value="dealer">
			<cfparam name="isSubDealer" default="0">
			<cfquery name="saveItem" datasource="#ds#">
				insert into dealers (dealerNumber, dealerName, isSubDealer) 
				values ('#form.dealerNumber#', '#form.dealerName#', #isSubDealer#) 
			</cfquery>
		</cfcase>
	</cfswitch>
	<script type="text/javascript" language="javascript">
		opener.document.forms[0].submit();
		window.close();
	</script>
	<cfabort>
</cfif>

<cfif isDefined("form.removeItem")>
	<cfif form.removeItem is 1>
		<cfswitch expression="#type#">
			<cfcase value="connectionType">
				<cfquery name="delItem" datasource="#ds#">
					delete from connection 
					where id = #form.itemid# 
				</cfquery>
			</cfcase>
			<cfcase value="usedFor">
				<cfquery name="delItem" datasource="#ds#">
					delete from UsedFor 
					where id = #form.itemid# 
				</cfquery>
			</cfcase>
			<cfcase value="linkMethod">
				<cfquery name="delItem" datasource="#ds#">
					delete from linkMethod 
					where id = #form.itemid# 
				</cfquery>
			</cfcase>
			<cfcase value="carrier">
				<cfquery name="delItem" datasource="#ds#">
					delete from carrier 
					where id = #form.itemid# 
				</cfquery>
			</cfcase>
			<cfcase value="accountNumber">
				<cfquery name="delItem" datasource="#ds#">
					delete from accounts  
					where id = #form.itemid# 
				</cfquery>
			</cfcase>
			<cfcase value="trunkGroup">
				<cfquery name="delItem" datasource="#ds#">
					delete from TrunkGroup  
					where id = #form.itemid# 
				</cfquery>
			</cfcase>
			<cfcase value="location">
				<cfquery name="delItem" datasource="#ds#">
					delete from location  
					where id = #form.itemid# 
				</cfquery>
			</cfcase>
			<cfcase value="whoOwns">
				<cfquery name="delItem" datasource="#ds#">
					delete from WhoOwns  
					where id = #form.itemid# 
				</cfquery>
			</cfcase>
			<cfcase value="whoPays">
				<cfquery name="delItem" datasource="#ds#">
					delete from WhoPays 
					where id = #form.itemid# 
				</cfquery>
			</cfcase>
			<cfcase value="whoPaysrate">
				<cfquery name="delItem" datasource="#ds#">
					delete from FlatRate 
					where id = #form.itemid# 
				</cfquery>
			</cfcase>
			<cfcase value="receiver">
				<cfquery name="delItem" datasource="#ds#">
					delete from receivers 
					where receiverid = #form.itemid# 
				</cfquery>
				<cfquery name="delLookups" datasource="#ds#">
					delete from receiver_lookup  
					where receiverid = #form.itemid# 
				</cfquery>
			</cfcase>
			<cfcase value="dealer">
				<cfquery name="delItem" datasource="#ds#">
					delete from dealers
					where dealerid = #form.itemid# 
				</cfquery>
				<cfquery name="delLookups" datasource="#ds#">
					delete from dealer_lookup 
					where dealerID = #form.itemid# 
				</cfquery>
			</cfcase>
		</cfswitch>
		<script type="text/javascript" language="javascript">
			opener.document.forms[0].submit();
			window.close();
		</script>
		<cfabort>
	</cfif>
</cfif>

<cfif isDefined("form.btnCancel")>
	<cflocation url="editData.cfm?type=#type#">
</cfif>

<cfswitch expression="#type#">
	<cfcase value="connectionType">
		<cfquery name="getItems" datasource="#ds#">
			select id, type as item 
			from connection 
			<cfif isDefined("form.itemid")>
				where id = #form.itemid#
			</cfif>
			order by type asc 
		</cfquery>
	</cfcase>
	<cfcase value="usedFor">
		<cfquery name="getItems" datasource="#ds#">
			select id, UsedFor as item 
			from UsedFor 
			<cfif isDefined("form.itemid")>
				where id = #form.itemid#
			</cfif>
			order by UsedFor asc 
		</cfquery>
	</cfcase>
	<cfcase value="linkMethod">
		<cfquery name="getItems" datasource="#ds#">
			select id, method as item 
			from linkMethod 
			<cfif isDefined("form.itemid")>
				where id = #form.itemid#
			</cfif>
			order by method asc 
		</cfquery>
	</cfcase>
	<cfcase value="carrier">
		<cfquery name="getItems" datasource="#ds#">
			select id, name as item 
			from carrier 
			<cfif isDefined("form.itemid")>
				where id = #form.itemid#
			</cfif>
			order by name asc 
		</cfquery>
	</cfcase>
	<cfcase value="accountNumber">
		<cfquery name="getItems" datasource="#ds#">
			select id, account as item 
			from accounts 
			<cfif isDefined("form.itemid")>
				where id = #form.itemid#
			</cfif>
			order by account asc 
		</cfquery>
	</cfcase>
	<cfcase value="trunkGroup">
		<cfquery name="getItems" datasource="#ds#">
			select id, TrunkGrp as item 
			from TrunkGroup  
			<cfif isDefined("form.itemid")>
				where id = #form.itemid#
			</cfif>
			order by TrunkGrp asc 
		</cfquery>
	</cfcase>
	<cfcase value="location">
		<cfquery name="getItems" datasource="#ds#">
			select id, location as item 
			from location  
			<cfif isDefined("form.itemid")>
				where id = #form.itemid#
			</cfif>
			order by location asc 
		</cfquery>
	</cfcase>
	<cfcase value="whoOwns">
		<cfquery name="getItems" datasource="#ds#">
			select id, owner as item 
			from WhoOwns
			<cfif isDefined("form.itemid")>
				where id = #form.itemid#
			</cfif>
			order by owner asc 
		</cfquery>
	</cfcase>
	<cfcase value="whoPays">
		<cfquery name="getItems" datasource="#ds#">
			select id, WhoPays as item 
			from WhoPays
			<cfif isDefined("form.itemid")>
				where id = #form.itemid#
			</cfif>
			order by WhoPays asc 
		</cfquery>
	</cfcase>
	<cfcase value="whoPaysRate">
		<cfquery name="getItems" datasource="#ds#">
			select id, Rate as item 
			from FlatRate
			<cfif isDefined("form.itemid")>
				where id = #form.itemid#
			</cfif>
			order by Rate asc 
		</cfquery>
	</cfcase>
	<cfcase value="receiver">
		<cfquery name="getItems" datasource="#ds#">
			select receiverID as id, receiver as item 
			from receivers 
			<cfif isDefined("form.itemid")>
				where receiverID = #form.itemid#
			</cfif>
			order by receiver asc 
		</cfquery>
	</cfcase>
	<cfcase value="dealer">
		<cfquery name="getItems" datasource="#ds#">
			select dealerID as id, dealernumber + ' - ' + dealername as item, dealerNumber, dealerName, isSubDealer 
			from dealers 
			<cfif isDefined("form.itemid")>
				where dealerID = #form.itemid#
			</cfif>
			order by dealernumber asc 
		</cfquery>
	</cfcase>
</cfswitch>

<cfif isDefined("form.btnAdd")>
	<body onLoad="resizeMe('<cfoutput>#form.type#</cfoutput>');">
	<div align="center">
	<table width="400" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td class="highlightbar"><b>Add Data</b> </td>
		</tr>
		<tr>
			<td class="greyrowbottomnopadding">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<form method="post" action="editData.cfm">
					<cfoutput>
					<input type="hidden" name="type" value="#form.type#" />
					</cfoutput>
					<cfif type is "dealer">
					<tr>
						<td valign="middle">
							Dealer Number:<br /><input style="vertical-align:middle;" type="text" style="width:50px" name="dealerNumber" value="" />
						</td>
					</tr>
					<tr>
						<td valign="middle">
							Dealer Name:<br /><input style="vertical-align:middle;" type="text" style="width:390px" name="dealerName" value="" />
						</td>
					</tr>
					<tr>
						<td valign="middle">
							Is this a sub dealer? <input style="vertical-align:middle;" type="checkbox" value="1" name="isSubDealer" />
						</td>
					</tr>
					<cfelse>
					<tr>
						<td>
							<input type="text" style="width:390px" name="item" value="" />
						</td>
					</tr>
					</cfif>
					<tr>
						<td>
							<input name="btnSave" type="submit" class="sidebar" value="Save" />
							<input name="btnCancel" type="submit" class="sidebar" value="Cancel" />
						</td>
					</tr>
					</form>
				</table>
			</td>
		</tr>
	</table>
	</div>
	</body>
	<cfabort>
</cfif>

<cfif isDefined("form.btnEdit")>
	<body onLoad="resizeMe('<cfoutput>#form.type#</cfoutput>');">
	<div align="center">
	<table width="400" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td class="highlightbar"><b>Edit Data</b> </td>
		</tr>
		<tr>
			<td class="greyrowbottomnopadding">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<form method="post" action="editData.cfm">
					<cfoutput query="getItems">
					<input type="hidden" name="itemID" value="#form.itemid#" />
					<input type="hidden" name="type" value="#form.type#" />
					<cfif type is "dealer">
					<tr>
						<td>
							Dealer Number: <input type="text" style="width:100px" name="dealerNumber" value="#dealerNumber#" />
						</td>
					</tr>
					<tr>
						<td>
							Dealer Name: <input type="text" style="width:390px" name="dealerName" value="#dealerName#" />
						</td>
					</tr>
					<tr>
						<td>
							Is this a sub dealer? <input type="checkbox" value="1" name="isSubDealer" <cfif getItems.isSubDealer is 1>checked</cfif> />
						</td>
					</tr>
					<cfelse>
					<tr>
						<td>
							<input type="text" style="width:390px" name="item" value="<cfif form.type is "whopaysrate">#numberformat(item,'0.00')#<cfelse>#item#</cfif>" />
						</td>
					</tr>
					</cfif>
					</cfoutput>
					<tr>
						<td>
							<input name="btnUpdate" type="submit" class="sidebar" value="Update" />
							<input name="btnCancel" type="submit" class="sidebar" value="Cancel" />
						</td>
					</tr>
					</form>
				</table>
			</td>
		</tr>
	</table>
	</div>
	</body>
	<cfabort>
</cfif>

<body onLoad="resizeMe('');">
<div align="center">
<table width="400" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Edit Data</b> </td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<form method="post" action="editData.cfm">
				<cfoutput>
				<input type="hidden" name="type" value="#type#" />
				</cfoutput>
				<input type="hidden" name="removeItem" value="0" />
				<tr>
					<td>
					<select name="itemid" style="width:390px">
						<option value="0"></option>
						<cfoutput query="getItems">
							<option value="#id#"><cfif type is "whoPaysRate">#dollarformat(item)#<cfelse>#item#</cfif></option>
						</cfoutput>
					</select>
					</td>
				</tr>
				<tr>
					<td>
						<input name="btnAdd" type="submit" class="sidebar" value="Add New Item" />
						<input name="btnEdit" type="submit" class="sidebar" value="Edit Selected" />
						<input name="btnRemove" type="button" onClick="if (confirm('Are you sure you wish to remove this item?')) {this.form.removeItem.value = 1; this.form.submit();}" class="sidebar" value="Remove Selected" />
					</td>
				</tr>
				</form>
			</table>
		</td>
	</tr>
</table>
</div>
</body>
