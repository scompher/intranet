<cfsetting showdebugoutput="yes">


<link rel="stylesheet" type="text/css" href="../styles.css">

<script language="javascript" type="text/javascript">
function manageItems(type) {
	var windowWidth = 420;
	var windowHeight = 125;
	var left = (screen.width / 2) - (windowWidth / 2);
	var top = (screen.height / 2) - (windowHeight/ 2) - 200;
	DetailWin = window.open("editData.cfm?type=" + type,"EditData","top=" + top + ",left=" + left + ",height=" + windowHeight + ",width=" + windowWidth + ",resizable=1,status=0,titlebar=0,toolbar=0");
}
</script>

<script language="javascript" src="cal2.js">
/*
Xin's Popup calendar script-  Xin Yang (http://www.yxscripts.com/)
Script featured on/available at http://www.dynamicdrive.com/
This notice must stay intact for use
*/
</script>
<script language="javascript" src="cal_conf2.js"></script>

<cfinclude template="functions.cfm">

<cfparam name="err" default="">

<cfparam name="form.active" default="No">
<cfparam name="form.singleCo" default="No">
<cfparam name="form.payPhoneBlock" default="No">
<cfparam name="form.orderPending" default="No">
<cfparam name="form.account" default="">
<cfparam name="form.C24Profile" default="">
<cfparam name="form.carrier" default="">
<cfparam name="form.circuitID" default="">
<cfparam name="form.comments" default="">
<cfparam name="form.connectionType" default="">
<cfparam name="form.dealerName" default="">
<cfparam name="form.dealerNumber" default="">
<cfparam name="form.description" default="">
<cfparam name="form.dnis" default="">
<cfparam name="form.extLink" default="">
<cfparam name="form.getLinkMethod" default="">
<cfparam name="form.huntGroup" default="">
<cfparam name="form.linkedTo" default="">
<cfparam name="form.location" default="">
<cfparam name="form.orderDate" default="">
<cfparam name="form.orderDueDate" default="">
<cfparam name="form.orderDetails" default="">
<cfparam name="form.padWith" default="">
<cfparam name="form.phoneNumber" default="">
<cfparam name="form.receiver" default="">
<cfparam name="form.threeDigit" default="">
<cfparam name="form.trunk" default="">
<cfparam name="form.trunkGroup" default="">
<cfparam name="form.usedFor" default="">
<cfparam name="form.whoOwns" default="">
<cfparam name="form.whoPays" default="">
<cfparam name="form.whoPaysRate" default="">
<cfparam name="form.lastModified" default="">
<cfparam name="form.lastModifiedBy" default="">
<cfparam name="form.dealerLetter" default="">
<cfparam name="form.accountRanges" default="">
<cfparam name="form.primacy" default="">
<cfparam name="form.twoWay" default="0">
<cfparam name="form.private" default="0">
<cfparam name="form.remove_receiverID" default="0">
<cfparam name="form.remove_dealerID" default="0">
<cfparam name="form.altDnis" default="">
<cfparam name="form.altTrunkGroup" default="">
<cfparam name="form.altExtLink" default="">
<cfparam name="form.alternateSwitchedTerm" default="">
<cfparam name="form.alternateLinkMethod" default="">
<cfparam name="form.recDNIS" default="">
<cfparam name="form.searchphoneNumber" default="">
<cfparam name="form.searchdnis" default="">
<cfparam name="form.searchreceiver" default="">
<cfparam name="form.searchdealerNumber" default="">
<cfparam name="form.searchusedFor" default="">
<cfparam name="form.sortResults" default="">
<cfparam name="form.associatedReceivers" default="0">
<cfparam name="form.associatedDealers" default="0">
<cfparam name="form.receiverID" default="0">

<cfset ds = "allLines">

<cfif isDefined("form.btnRemoveReceiver")>
	<cfset index = listfindnocase(form.associatedReceivers,form.remove_receiverID)>
	<cfset form.associatedReceivers = listdeleteat(form.associatedReceivers,index)>
	<cfif trim(form.associatedReceivers) is ""><cfset form.associatedReceivers = 0></cfif>
</cfif>

<cfif isDefined("form.btnAssociateReceiver")>
	<cfif listfindnocase(form.associatedReceivers,form.receiverID) is 0>
		<cfset form.associatedReceivers = listappend(form.associatedReceivers,form.receiverID)>
	</cfif>
</cfif>

<cfif isDefined("form.btnRemoveDealer")>
	<cfset index = listfindnocase(form.associatedDealers,form.remove_dealerID)>
	<cfset form.associatedDealers = listdeleteat(form.associatedDealers,index)>
	<cfif trim(form.associatedDealers) is ""><cfset form.associatedDealers = 0></cfif>
</cfif>

<cfif isDefined("form.btnAssociateDealer")>
	<cfif listfindnocase(form.associatedDealers,form.dealerID) is 0>
		<cfset form.associatedDealers = listappend(form.associatedDealers,form.dealerID)>
	</cfif>
</cfif>

<cfif isDefined("form.btnSaveInfo")>
	<cfset err = validateInfo(form)>
	<cfquery name="checkDup" datasource="#ds#">
		select id from all_lines
		where phonenumber = '#form.phonenumber#'
	</cfquery>
	<cfif checkDup.recordcount gt 0>
		<cfset err = listappend(err,"[phoneNumber]~The phone number is already in the database.")>
	</cfif>
	<cfif trim(err) is "">
		<cfif saveItem(form)>
			<cfset savedItem = form.phonenumber>
			<cfif page is "new.cfm">
				<cfif cookie.adminlogin is 18>
					<cfset form.phoneNumber = "">
				<cfelse>
					<cfset form.active = "">
					<cfset form.singleCo = "No">
					<cfset form.payPhoneBlock = "No">
					<cfset form.orderPending = "No">
					<cfset form.account = "">
					<cfset form.C24Profile = "">
					<cfset form.carrier = "">
					<cfset form.circuitID = "">
					<cfset form.comments = "">
					<cfset form.connectionType = "">
					<cfset form.dealerName = "">
					<cfset form.dealerNumber = "">
					<cfset form.description = "">
					<cfset form.dnis = "">
					<cfset form.extLink = "">
					<cfset form.getLinkMethod = "">
					<cfset form.huntGroup = "">
					<cfset form.linkedTo = "">
					<cfset form.location = "">
					<cfset form.orderDate = "">
					<cfset form.orderDueDate = "">
					<cfset form.orderDetails = "">
					<cfset form.padWith = "">
					<cfset form.phoneNumber = "">
					<cfset form.receiver = "">
					<cfset form.threeDigit = "">
					<cfset form.trunk = "">
					<cfset form.trunkGroup = "">
					<cfset form.usedFor = "">
					<cfset form.whoOwns = "">
					<cfset form.whoPays = "">
					<cfset form.whoPaysRate = "">
					<cfset form.dealerLetter = "">
					<cfset form.accountRanges = "">
					<cfset form.primacy = 1>
					<cfset form.twoWay = 0>
					<cfset form.private = 0>
					<cfset form.associatedReceivers = "0">
					<cfset form.recDNIS = "">
				</cfif>
				<!--- Keep form loaded
				<cflocation url="/index.cfm">
				--->
				<cfset err = "*~#savedItem# Information Saved">
			<cfelse>
				<script type="text/javascript" language="javascript">
					window.close();
				</script>
			</cfif>
		</cfif>
	</cfif>
</cfif>

<cfif isDefined("form.btnUpdateInfo")>
	<cfset err = validateInfo(form)>
	<cfif trim(err) is "">
		<cfif updateItem(form)>
			<cfif page is "new.cfm">
				<cflocation url="/index.cfm">
			<cfelse>
				<script type="text/javascript" language="javascript">
					if (opener.document.forms[0].btnSearch) 
						opener.document.forms[0].btnSearch.click();
					else 
						opener.document.location.reload(true);
					window.close();
				</script>
			</cfif>
		</cfif>
	</cfif>
</cfif>

<cfif isDefined("form.btnCancel")>
	<cfif page is "new.cfm">
		<cflocation url="/index.cfm">
	<cfelse>
		<script type="text/javascript" language="javascript">
			window.close();
		</script>
	</cfif>
</cfif>

<cfif isDefined("url.getItem")>
	<cfquery name="GetInfo" datasource="#ds#">
		select *, [c-24-profile] as c24profile, [pad-with] as padWith, [3-digit] as threeDigit 
		from All_Lines 
		where id = #id# 
	</cfquery>
	<cfif getInfo.recordcount gt 0>
		<cfset form.active = getInfo.active>
		<cfset form.singleCo = getInfo.singleCo>
		<cfset form.payPhoneBlock = getInfo.payPhoneBlock>
		<cfset form.orderPending = getInfo.orderPending>
		<cfset form.account = getInfo.account>
		<cfset form.C24Profile = getinfo.c24profile>
		<cfset form.carrier = getinfo.carrier>
		<cfset form.circuitID = getinfo.circuitID>
		<cfset form.comments = getinfo.comments>
		<cfset form.connectionType = getinfo.connectionType>
		<cfset form.dealerName = getinfo.dlrName>
		<cfset form.dealerNumber = getInfo.dlrNumber>
		<cfset form.description = getinfo.description>
		<cfset form.dnis = getinfo.dnis>
		<cfset form.extLink = getinfo.EXTLINKEDTO>
		<cfset form.getLinkMethod = getinfo.linkMethod>
		<cfset form.huntGroup = getinfo.hunt>
		<cfset form.linkedTo = getinfo.linkedTo>
		<cfset form.location = getinfo.location>
		<cfset form.orderDate = getinfo.orderDate>
		<cfset form.orderDueDate = getinfo.orderDueDate>
		<cfset form.orderDetails = getinfo.orderDetails>
		<cfset form.padWith = getInfo.padWith>
		<cfset form.phoneNumber = getInfo.phoneNumber>
		<cfset form.receiver = getInfo.receiver>
		<cfset form.threeDigit = getInfo.threeDigit>
		<cfset form.trunk = getInfo.trunk>
		<cfset form.trunkGroup = getInfo.trunkGroup>
		<cfset form.usedFor = getInfo.usedFor>
		<cfset form.whoOwns = getInfo.whoOwns>
		<cfset form.whoPays = getInfo.whoPays>
		<cfset form.whoPaysRate = getInfo.FlatRate>
		<cfset form.lastModified = getInfo.lastModified>
		<cfset form.lastModifiedBy = getInfo.lastModifiedBy>
		<cfset form.dealerLetter = getInfo.dealerLetter>
		<cfset form.accountRanges = getInfo.accountRanges>
		<cfset form.primacy = getInfo.primacy>
		<cfset form.twoWay = getInfo.twoWay>
		<cfset form.private = getInfo.private>
		<cfset form.recDNIS = getInfo.recDnis>
		<cfset form.altDnis = getInfo.altDnis>
		<cfset form.altTrunkGroup = getInfo.altTrunkGroup>
		<cfset form.altExtLink = getInfo.altExtLink>

		<cfif trim(form.lastModifiedBy) is not "">
			<cfquery name="getLastModifiedBy" datasource="intranet">
				select * from admin_users 
				where adminuserid = #form.lastModifiedBy#
			</cfquery>
			<cfset form.lastModifiedBy = "#getLastModifiedBy.firstname# #getLastModifiedBy.lastname#">
		</cfif>
		<cfquery name="getReceiverList" datasource="#ds#">
			select * from receiver_lookup 
			where lineid = #id# 
		</cfquery>
		<cfif getReceiverList.recordcount is 0>
			<cfset form.associatedReceivers = 0>
		<cfelse>
			<cfset form.associatedReceivers = valuelist(getReceiverList.receiverID)>
		</cfif>

		<cfquery name="getDealerList" datasource="#ds#">
			select * from dealer_lookup 
			where lineid = #id# 
		</cfquery>
		<cfif getDealerList.recordcount is 0>
			<cfset form.associatedDealers = 0>
		<cfelse>
			<cfset form.associatedDealers = valuelist(getDealerList.dealerID)>
		</cfif>
	</cfif>
</cfif>

<cfquery name="getConnTypes" datasource="#ds#">
	select * from Connection
	order by type asc 
</cfquery>

<cfquery name="getUsedFor" datasource="#ds#">
	select * from usedFor
	order by usedFor asc 
</cfquery>

<cfquery name="getLinkedTo" datasource="#ds#">
	select phoneNumber 
	from All_Lines 
	where phonenumber <> ''
	order by phoneNumber asc 
</cfquery>

<cfquery name="getLinkMethod" datasource="#ds#">
	select method from LinkMethod
	order by method asc 
</cfquery>

<cfquery name="getCarrier" datasource="#ds#">
	select name from carrier
	order by name asc 
</cfquery>

<cfquery name="getAccounts" datasource="#ds#">
	select account from accounts
	order by account asc 
</cfquery>

<cfquery name="getTrunkGroup" datasource="#ds#">
	select trunkGrp 
	from trunkGroup 
	order by trunkGrp ASC 
</cfquery>

<cfquery name="getLocations" datasource="#ds#">
	select Location from Location 
	order by Location asc 
</cfquery>

<cfquery name="getWhoOwns" datasource="#ds#">
	select owner from whoOwns
	order by owner asc 
</cfquery>

<cfquery name="getWhoPays" datasource="#ds#">
	select WhoPays from WhoPays 
	order by WhoPays asc 
</cfquery>

<cfquery name="getWhoPaysRate" datasource="#ds#">
	select rate from FlatRate
	order by rate asc 
</cfquery>

<cfquery name="getReceivers" datasource="#ds#">
	select receiverID, receiver from receivers 
	order by receiver asc 
</cfquery>

<cfif trim(form.associatedReceivers) is ""><cfset form.associatedReceivers = 0></cfif>
<cfquery name="getAssociatedReceivers" datasource="#ds#">
	select receiverID, receiver from receivers 
	where receiverID IN (#form.associatedReceivers#) 
	order by receiver asc 
</cfquery>

<cfquery name="getDealers" datasource="#ds#">
	select * 
	from dealers 
	order by dealerNumber asc 
</cfquery>

<cfif trim(form.associatedDealers) is ""><cfset form.associatedDealers = 0></cfif>
<cfquery name="getAssociatedDealers" datasource="#ds#">
	select *  
	from dealers 
	where dealerID IN (#form.associatedDealers#) 
	order by dealerNumber asc 
</cfquery>

<div align="center">
<body onLoad="document.forms[0].phoneNumber.focus();">
<table border="0" cellspacing="0" cellpadding="5">
	<cfloop list="#err#" index="e">
	<tr>
		<td class="alert"><cfoutput>#listgetat(e,2,"~")#</cfoutput></td>
	</tr>
	</cfloop>
	<tr>
		<td><a href="http://192.168.107.104/alllines/import/import.cfm" style="text-decoration:underline">Import data from spreadsheet template</a></td>
	</tr>
	<tr>
		<td class="highlightbar"><b>All Lines Input Form</b></td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
			<table border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td class="nopadding">
						<form method="post" action="<cfoutput>#page#</cfoutput>" name="inputform">
						<input type="hidden" name="remove_receiverID" value="0">
						<input type="hidden" name="remove_dealerID" value="0">
						<input type="hidden" name="orderDetails" value="" />
						<input type="hidden" name="lastModified" value="<cfoutput>#form.lastModified#</cfoutput>" />
						<input type="hidden" name="lastModifiedBy" value="<cfoutput>#form.lastModifiedBy#</cfoutput>" />
						<input type="hidden" name="associatedReceivers" value="<cfoutput>#form.associatedReceivers#</cfoutput>">
						<input type="hidden" name="associatedDealers" value="<cfoutput>#form.associatedDealers#</cfoutput>">
						<cfoutput>
						<cfif isDefined("getItem")>
							<input type="hidden" name="getItem" value="#getItem#" />
							<input type="hidden" name="id" value="#id#" />
						</cfif>
						</cfoutput>
						<table border="0" cellspacing="0" cellpadding="3">
							<cfif trim(form.lastModified) is not "" and trim(form.lastModifiedBy) is not "">
							<cfoutput>
							<tr>
								<td colspan="5">Last modified by #form.lastModifiedBy# on #dateformat(form.lastModified,'mm/dd/yyyy')# at #timeformat(form.lastModified,'hh:mm tt')#</td>
							</tr>
							</cfoutput>
							</cfif>
							<tr>
								<td>Phone # or IP Address: <span class="smallred">*</span> </td>
								<td>Connection Type: <span class="smallred">*</span> <a href="javascript: manageItems('connectionType');">[+]</a> </td>
								<td>DNIS:</td>
								<td>Receiver: <a href="javascript: manageItems('receiver');">[+]</a> </td>
								<td>Used For: <span class="smallred">*</span> <a href="javascript: manageItems('usedFor');">[+]</a> </td>
							</tr>
							<tr>
								<td>
									<input name="phoneNumber" type="text" value="<cfoutput>#form.phoneNumber#</cfoutput>" <cfif findnocase("[phoneNumber]",err) is not 0>style="background-color:#FFCCCC;"</cfif> />
								</td>
								<td>
									<select name="connectionType" <cfif findnocase("[connectionType]",err) is not 0>style="background-color:#FFCCCC;"</cfif> >
											<option value=""></option>
										<cfoutput query="getConnTypes">
											<option <cfif form.connectionType is type>selected</cfif> value="#type#">#type#</option>
										</cfoutput>
									</select>
								</td>
								<td>
									<input type="text" name="dnis" value="<cfoutput>#form.dnis#</cfoutput>" />
								</td>
								<td valign="middle">
									<select name="receiverID" style="vertical-align:middle;">
										<option value=""></option>
										<cfoutput query="getReceivers">
											<option value="#getReceivers.receiverID#">#getReceivers.receiver#</option>
										</cfoutput>
									</select>
									<input style="vertical-align:middle;" name="btnAssociateReceiver" type="submit" class="smalltable" value="Add">
									<!--- <input name="receiver" type="text" value="<cfoutput>#form.receiver#</cfoutput>" /> --->
								</td>
								<td>
									<select name="usedFor" <cfif findnocase("[usedFor]",err) is not 0>style="background-color:#FFCCCC;"</cfif> >
										<option value=""></option>
										<cfoutput query="getUsedFor">
											<option <cfif form.usedFor is getUsedFor.usedFor>selected</cfif> value="#usedFor#">#usedFor#</option>
										</cfoutput>
									</select>
								</td>
							</tr>
							<tr>
							    <td>&nbsp;</td>
							    <td>&nbsp;</td>
							    <td>&nbsp;</td>
							    <td valign="top" class="nopadding">
									<table border="0" cellspacing="0" cellpadding="2">
										<tr><td colspan="2" nowrap="nowrap"><b>Currently Associated:</b></td></tr>
										<cfoutput query="getAssociatedReceivers">
										<tr><td>#getAssociatedReceivers.receiver#</td><td><input type="submit" name="btnRemoveReceiver" onClick="this.form.remove_receiverID.value = #getAssociatedReceivers.receiverID#" class="smalltable" value="del"></td></tr>
										</cfoutput>
									</table>
							    </td>
							    <td>&nbsp;</td>
							    </tr>
							<tr>
							    <td>&nbsp;</td>
							    <td>&nbsp;</td>
							    <td>&nbsp;</td>
							    <td>&nbsp;</td>
							    <td>&nbsp;</td>
							    </tr>
							<tr>
								<td>Number Linked To: </td>
								<td>Link Method: <a href="javascript: manageItems('linkMethod');">[+]</a></td>
								<td>HUNT Group: </td>
								<td>Trunk:</td>
								<td>Ext Link: </td>
							</tr>
							<tr>
								<td>
									<select name="linkedTo">
										<option value=""></option>
										<cfoutput query="getLinkedTo">
											<option <cfif form.linkedTo is getLinkedTo.phoneNumber>selected</cfif> value="#phoneNumber#">#phoneNumber#</option>
										</cfoutput>
									</select>
								</td>
								<td>
									<select name="getLinkMethod">
										<option value=""></option>
										<cfoutput query="getLinkMethod">
											<option <cfif form.getLinkMethod is getLinkMethod.method>selected</cfif> value="#method#">#method#</option>
										</cfoutput>
									</select>
								</td>
								<td>
									<input type="text" name="huntGroup" value="<cfoutput>#form.huntGroup#</cfoutput>" />
								</td>
								<td>
									<input type="text" name="trunk" value="<cfoutput>#form.trunk#</cfoutput>" />
								</td>
								<td>
									<input type="text" name="extLink" value="<cfoutput>#form.extLink#</cfoutput>" />
								</td>
							</tr>
							<tr>
								<td colspan="2">&nbsp;</td>
								<td>&nbsp;</td>
								<td>Rec DNIS: </td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td colspan="2">&nbsp;</td>
								<td>&nbsp;</td>
								<td>
									<input type="text" name="recDNIS" value="<cfoutput>#form.recDNIS#</cfoutput>" />
								</td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td colspan="2">Description:</td>
								<td colspan="3">Associated Dealer(s): <a href="javascript: manageItems('dealer');">[+]</a></td>
								</tr>
							<tr>
								<td colspan="2">
									<input name="description" type="text" value="<cfoutput>#form.description#</cfoutput>" style="width:200px" />
								</td>
								<td colspan="3" valign="middle">
								<select name="dealerID" style="vertical-align:middle;">
									<option value="0"></option>
									<cfoutput query="getDealers">
										<option value="#getDealers.dealerID#">#getDealers.dealerNumber# - #getDealers.dealerName#</option>
									</cfoutput>
								</select>
								<input type="submit" name="btnAssociateDealer" class="smalltable" style="vertical-align:middle;" value="Add">
								</td>
							</tr>
							<tr>
							    <td colspan="2" valign="top">&nbsp;</td>
							    <td colspan="3" valign="top" class="nopadding">
							        <table border="0" cellspacing="0" cellpadding="2">
                                        <tr>
                                            <td><b>Currently Associated:</b> </td>
                                        </tr>
										<cfoutput query="getAssociatedDealers">
                                        <tr>
                                            <td>#dealerNumber# <cfif getAssociatedDealers.isSubDealer is not 0>(sub)</cfif> - #dealerName#</td>
											<td><input type="submit" name="btnRemoveDealer" onClick="this.form.remove_dealerID.value = #getAssociatedDealers.dealerID#" class="smalltable" value="del"></td>
                                        </tr>
										</cfoutput>
                                    </table>
							    </td>
							    </tr>
							<tr>
							    <td colspan="2">&nbsp;</td>
							    <td valign="top">&nbsp;</td>
							    <td colspan="2" valign="top">&nbsp;</td>
							    </tr>
							<tr>
							    <td colspan="2">Alternate DNIS:</td>
							    <td valign="top">Alternate Trunk Group: <a href="javascript: manageItems('trunkGroup');">[+]</a> </td>
							    <td colspan="2" valign="top">Alternate Ext Link: </td>
							    </tr>
							<tr>
							    <td colspan="2">
							        <input name="altDnis" type="text" id="altDnis" value="<cfoutput>#form.altDnis#</cfoutput>" />
							    </td>
							    <td valign="top">
							        <select name="altTrunkGroup" id="altTrunkGroup">
                                        <option value=""></option>
                                        <cfoutput query="getTrunkGroup">
                                        <option <cfif form.altTrunkGroup is getTrunkGroup.trunkGrp>selected</cfif> value="#trunkGrp#">#trunkGrp#</option>
                                        </cfoutput>
                                    </select>
							    </td>
							    <td colspan="2" valign="top">
							        <input name="altExtLink" type="text" id="altExtLink" value="<cfoutput>#form.altExtLink#</cfoutput>" />
							    </td>
							    </tr>
							<tr>
							    <td colspan="2">Alternate Link Method: </td>
							    <td valign="top">Alternate Switched Term: </td>
							    <td colspan="2" valign="top">&nbsp;</td>
							    </tr>
							<tr>
							    <td colspan="2">
							        <select name="alternateLinkMethod">
                                        <option value=""></option>
                                        <cfoutput query="getLinkMethod">
                                        <option <cfif form.alternateLinkMethod is getLinkMethod.method>selected</cfif> value="#method#">#method#</option>
                                        </cfoutput>
                                    </select>
							    </td>
							    <td valign="top">
							        <select name="alternateSwitchedTerm">
                                        <option value=""></option>
                                        <cfoutput query="getLinkedTo">
                                        <option <cfif form.alternateSwitchedTerm is getLinkedTo.phoneNumber>selected</cfif> value="#phoneNumber#">#phoneNumber#</option>
                                        </cfoutput>
                                    </select>
							    </td>
							    <td colspan="2" valign="top">&nbsp;</td>
							    </tr>
							<tr>
							    <td colspan="2">&nbsp;</td>
							    <td valign="top">&nbsp;</td>
							    <td colspan="2" valign="top">&nbsp;</td>
							    </tr>
							<tr>
								<td colspan="2">Account Ranges: </td>
								<td valign="top">Primacy: </td>
								<td colspan="2" valign="top">&nbsp;</td>
							</tr>
							<tr>
								<td colspan="2">
									<input name="accountRanges" type="text" value="<cfoutput>#form.accountRanges#</cfoutput>" maxlength="40" size="20" />
								</td>
								<td valign="top">
								<select name="primacy">
									<cfloop from="1" to="10" index="i">
										<cfoutput>
										<option <cfif form.primacy is i>selected</cfif> value="#i#">#i#</option>
										</cfoutput>
									</cfloop>
								</select>
								</td>
								<td colspan="2" valign="top">&nbsp;</td>
							</tr>
							<tr>
								<td colspan="5">Comments:</td>
								</tr>
							<tr>
								<td colspan="5">
									<textarea name="comments" rows="4" style="width:500px"><cfoutput>#trim(comments)#</cfoutput></textarea>
								</td>
							</tr>
							<tr>
								<td>Carrier: <span class="smallred">*</span> <a href="javascript:  manageItems('carrier');">[+]</a></td>
								<td>Account Number: <a href="javascript: manageItems('accountNumber');">[+]</a></td>
								<td>Circuit ID: </td>
								<td valign="top">Trunk Group: <a href="javascript: manageItems('trunkGroup');">[+]</a></td>
								<td valign="top">&nbsp;</td>
							</tr>
							<tr>
								<td>
									<select name="carrier" <cfif findnocase("[carrier]",err) is not 0>style="background-color:#FFCCCC;"</cfif> >
										<option value=""></option>
										<cfoutput query="getCarrier">
											<option <cfif form.carrier is getcarrier.name>selected</cfif> value="#name#">#name#</option>
										</cfoutput>
									</select>
								</td>
								<td>
									<select name="account">
										<option value=""></option>
										<cfoutput query="getAccounts">
											<option <cfif form.account is getAccounts.account>selected</cfif> value="#account#">#account#</option>
										</cfoutput>
									</select>
								</td>
								<td>
									<input name="circuitID" type="text" />
								</td>
								<td colspan="2" valign="top">
									<select name="trunkGroup">
										<option value=""></option>
										<cfoutput query="getTrunkGroup">
											<option <cfif form.trunkGroup is getTrunkGroup.trunkGrp>selected</cfif> value="#trunkGrp#">#trunkGrp#</option>
										</cfoutput>
									</select>
								</td>
								</tr>
							<tr>
								<td>C24 Profile: </td>
								<td>3-Digit:</td>
								<td>Pad With: </td>
								<td valign="top">Release in Letter: </td>
								<td valign="top">Two Way: </td>
							</tr>
							<tr>
								<td>
									<input name="c24Profile" type="text" value="<cfoutput>#form.c24Profile#</cfoutput>" />
								</td>
								<td>
									<select name="threeDigit">
										<option value=""></option>
										<option <cfif form.threeDigit is "Yes">selected</cfif> value="Yes">Yes</option>
										<option <cfif form.threeDigit is "No">selected</cfif> value="No">No</option>
									</select>
								</td>
								<td>
									<input name="padWith" type="text" style="width:30px" maxlength="3" value="<cfoutput>#form.padWith#</cfoutput>" />
								</td>
								<td valign="top">
								<select name="dealerLetter">
									<option value=""></option>
									<option <cfif form.dealerLetter is "YES">selected</cfif> value="YES">YES</option>
									<option <cfif form.dealerLetter is "NO">selected</cfif> value="NO">NO</option>
									<option <cfif form.dealerLetter is "CHECK FILE">selected</cfif> value="CHECK FILE">CHECK FILE</option>
								</select>
								</td>
								<td valign="top">
									<select name="twoWay">
										<option value=""></option>
										<option <cfif form.twoWay is 1>selected</cfif> value="1">Yes</option>
										<option <cfif form.twoWay is 0>selected</cfif> value="0">No</option>
									</select>
								</td>
							</tr>
							<tr>
								<td>Location: <span class="smallred">*</span> <a href="javascript: manageItems('location');">[+]</a></td>
								<td>Who Owns: <span class="smallred">*</span> <a href="javascript: manageItems('whoOwns');">[+]</a></td>
								<td>Who Pays: <span class="smallred">*</span> <a href="javascript: manageItems('whoPays');">[+]</a></td>
								<td valign="top">&nbsp;</td>
								<td valign="top">&nbsp;</td>
							</tr>
							<tr>
								<td>
									<select name="location" <cfif findnocase("[location]",err) is not 0>style="background-color:#FFCCCC;"</cfif> >
										<option value=""></option>
										<cfoutput query="getLocations">
											<option <cfif form.location is getLocations.location>selected</cfif>  value="#location#">#location#</option>
										</cfoutput>
									</select>
								</td>
								<td>
									<select name="whoOwns" <cfif findnocase("[whoOwns]",err) is not 0>style="background-color:#FFCCCC;"</cfif> >
										<option value=""></option>
										<cfoutput query="getWhoOwns">
											<option <cfif form.whoOwns is getWhoOwns.owner>selected</cfif> value="#owner#">#owner#</option>
										</cfoutput>
									</select>
								</td>
								<td colspan="3">
									<select name="whoPays" style="vertical-align:middle" <cfif findnocase("[whoPays]",err) is not 0>style="background-color:#FFCCCC;"</cfif> >
										<option value=""></option>
										<cfoutput query="getWhoPays">
											<option <cfif form.whoPays is getWhoPays.whoPays>selected</cfif> value="#whoPays#">#whoPays#</option>
										</cfoutput>
									</select> 
									@ 
									<select name="whoPaysRate" style="vertical-align:middle">
										<option value=""></option>
										<cfoutput query="getWhoPaysRate">
											<option <cfif form.whoPaysRate is getWhoPaysRate.rate>selected</cfif> value="#rate#">#dollarformat(rate)#</option>
										</cfoutput>
									</select>
									&nbsp;<a href="javascript: manageItems('whoPaysRate');">[+]</a>								</td>
								</tr>
							<tr>
								<td colspan="5" class="nopadding">
									<table border="0" cellspacing="0" cellpadding="5">
										<tr>
											<td style="padding-right:0px">
												<input type="checkbox" name="active" <cfif form.active is "Yes">checked</cfif> value="Yes" />
											</td>
											<td>Active&nbsp;&nbsp;</td>
											<td style="padding-right:0px">
												<input type="checkbox" name="singleCo" <cfif form.singleCo is "Yes">checked</cfif> value="Yes" />
											</td>
											<td>Single Co.&nbsp;&nbsp; </td>
											<td style="padding-right:0px">
												<input type="checkbox" name="payPhoneBlock" <cfif form.payPhoneBlock is "Yes">checked</cfif> value="Yes" />
											</td>
											<td>Pay Phone Block&nbsp;&nbsp; </td>
											<td style="padding-right:0px">
												<input onClick="this.form.submit();" type="checkbox" name="orderPending" <cfif form.orderPending is "Yes">checked</cfif> value="Yes" />
											</td>
											<td nowrap="nowrap">Order Pending</td>
											<td style="padding-right:0px">
												<input style="vertical-align:middle;" type="checkbox" name="private" value="1" <cfif form.private is 1>checked</cfif> >
											</td>
											<td nowrap="nowrap">Private</td>
										</tr>
									</table>
								</td>
							</tr>
							<cfif orderPending is 1>
							<tr>
								<td colspan="5" class="nopadding">
									<table border="0" cellspacing="0" cellpadding="5">
										<tr>
											<td>
											Order Date: 
											<cfif trim(form.orderDate) is not "">
												<input type="text" name="orderDate" style="width:75px; vertical-align:middle;" value="<cfoutput>#dateformat(form.orderDate,'mm/dd/yyyy')#</cfoutput>">
											<cfelse>
												<input type="text" name="orderDate" style="width:75px; vertical-align:middle;" value="">
											</cfif>
											<a style="text-decoration:none;" href="javascript:showCal('order_date');"><img border="0" src="/images/calicon.gif" width="20" height="20" align="absmiddle"></a>											</td>
											<td>&nbsp;</td>
											<td>
											Order Due Date:
											<cfif trim(form.orderDueDate) is not "">
												<input type="text" name="orderDueDate" style="width:75px; vertical-align:middle;" value="<cfoutput>#dateformat(form.orderDueDate,'mm/dd/yyyy')#</cfoutput>">
											<cfelse>
												<input type="text" name="orderDueDate" style="width:75px; vertical-align:middle;" value="">
											</cfif>
											<a style="text-decoration:none;" href="javascript:showCal('order_due_date');"><img border="0" src="/images/calicon.gif" width="20" height="20" align="absmiddle"></a>											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td colspan="5">
								Pending Order Details:								</td>
							</tr>
							<tr>
								<td colspan="5" >
								<textarea name="orderDetails" rows="4" style="width:500px"><cfoutput>#form.orderDetails#</cfoutput></textarea>
								</td>
							</tr>
							<cfelse>
							<input type="hidden" name="orderDetails" value="<cfoutput>#form.orderDetails#</cfoutput>">
							</cfif>
							<cfif page is not "new.cfm">
							<tr>
								<td colspan="5">
								<cfquery name="getLinkedTo" datasource="#ds#">
									select PhoneNumber, WhoOwns 
									from all_lines 
									where LinkedTo = '#form.phonenumber#'								</cfquery>
								<b>Linked to this number:</b><br />
								<cfoutput query="getLinkedto">
								#phoneNumber# owned by #whoowns#<br />
								</cfoutput>
								</td>
							</tr>
							</cfif>
							<tr>
								<td colspan="5">
									<cfif isDefined("getItem")>
										<input name="btnUpdateInfo" type="submit" class="sidebar" value="Update Information" />
									<cfelse>
										<input name="btnSaveInfo" type="submit" class="sidebar" value="Save Information" />
									</cfif>
									<input name="btnCancel" type="submit" class="sidebar" value="Cancel" />
								</td>
							</tr>
						</table>
						</form>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</body>
<cfif page is "new.cfm">
<br>
<a class="normal" style="text-decoration:underline;" href="/index.cfm">Return to Intranet</a>
</cfif>
</div>
