
<cfsetting showdebugoutput="no">

<link rel="stylesheet" type="text/css" href="../styles.css">

<cfset err = "">

<cfset ds = "allLines">

<cfif isDefined("url.id")>
	<cfquery name="GetInfo" datasource="#ds#">
		select *, [c-24-profile] as c24profile, [pad-with] as padWith, [3-digit] as threeDigit 
		from All_Lines 
		where id = #id# 
	</cfquery>

	<cfquery name="getLinkedTo" datasource="#ds#">
		select PhoneNumber, WhoOwns 
		from all_lines 
		where LinkedTo = '#getInfo.phonenumber#'
	</cfquery>
	<cfif getInfo.recordcount gt 0>
		<cfset form.active = getInfo.active>
		<cfset form.singleCo = getInfo.singleCo>
		<cfset form.payPhoneBlock = getInfo.payPhoneBlock>
		<cfset form.orderPending = getInfo.orderPending>
		<cfset form.account = getInfo.account>
		<cfset form.C24Profile = getinfo.c24profile>
		<cfset form.carrier = getinfo.carrier>
		<cfset form.circuitID = getinfo.circuitID>
		<cfset form.comments = getinfo.comments>
		<cfset form.connectionType = getinfo.connectionType>
		<cfset form.dealerName = getinfo.dlrName>
		<cfset form.dealerNumber = getInfo.dlrNumber>
		<cfset form.description = getinfo.description>
		<cfset form.dnis = getinfo.dnis>
		<cfset form.extLink = getinfo.EXTLINKEDTO>
		<cfset form.getLinkMethod = getinfo.linkMethod>
		<cfset form.huntGroup = getinfo.hunt>
		<cfset form.linkedTo = getinfo.linkedTo>
		<cfset form.location = getinfo.location>
		<cfset form.orderDate = getinfo.orderDate>
		<cfset form.orderDueDate = getinfo.orderDueDate>
		<cfset form.orderDetails = getinfo.orderDetails>
		<cfset form.padWith = getInfo.padWith>
		<cfset form.phoneNumber = getInfo.phoneNumber>
		<cfset form.receiver = getInfo.receiver>
		<cfset form.threeDigit = getInfo.threeDigit>
		<cfset form.trunk = getInfo.trunk>
		<cfset form.trunkGroup = getInfo.trunkGroup>
		<cfset form.usedFor = getInfo.usedFor>
		<cfset form.whoOwns = getInfo.whoOwns>
		<cfset form.whoPays = getInfo.whoPays>
		<cfset form.whoPaysRate = getInfo.FlatRate>
		<cfset form.lastModified = getInfo.lastModified>
		<cfset form.lastModifiedBy = getInfo.lastModifiedBy>
		<cfset form.dealerLetter = getInfo.dealerLetter>
		<cfset form.accountRanges = getInfo.accountRanges>
		<cfset form.primacy = getInfo.primacy>
		<cfset form.twoWay = getInfo.twoWay>
		<cfset form.altDnis = getInfo.altDnis>
		<cfset form.altTrunkGroup = getInfo.altTrunkGroup>
		<cfset form.altExtLink = getInfo.altExtLink>
		<cfset form.alternateSwitchedTerm = getInfo.alternateSwitchedTerm>
		<cfset form.alternateLinkMethod = getInfo.alternateLinkMethod>

		<cfquery name="getReceiverList" datasource="#ds#">
			select * 
			from receivers 
			inner join receiver_lookup on receivers.receiverID = receiver_lookup.receiverID  
			where lineid = #id# 
			order by receiver asc 
		</cfquery>
		<cfset form.associatedReceivers = valuelist(getReceiverList.receiver)>

		<cfquery name="getDealerList" datasource="#ds#"> 
			select dealerNumber + ' - ' + dealerName + ':' + convert(nvarchar(1),isSubDealer) as theDealer  
			from dealers  
			inner join dealer_lookup on dealers.dealerID = dealer_lookup.dealerID 
			where lineid = #id# 
			order by dealernumber asc 
		</cfquery>
		<cfset form.associatedDealers = valuelist(getDealerList.theDealer,"~")>

		<cfif trim(form.lastModifiedBy) is not "">
			<cfquery name="getLastModifiedBy" datasource="intranet">
				select * from admin_users 
				where adminuserid = #form.lastModifiedBy#
			</cfquery>
			<cfset form.lastModifiedBy = "#getLastModifiedBy.firstname# #getLastModifiedBy.lastname#">
		</cfif>
	</cfif>
</cfif>
<div align="center">
<table width="700" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar" style="padding:0px">
		<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td width="50%"><b>All Lines Detail View</b></td>
				<td width="50%" align="right"><a href="javascript:window.close();"><b>Close This Window</b></a> <a href="javascript:window.close();"><img src="/images/closeWin.gif" alt="Close Window" width="21" height="21" border="0" align="absmiddle" /></a></td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
			<table border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td class="nopadding">
						<table border="0" cellspacing="0" cellpadding="5">
							<cfif trim(form.lastModified) is not "" and trim(form.lastModifiedBy) is not "">
							<cfoutput>
							<tr>
								<td colspan="5">Last modified by #form.lastModifiedBy# on #dateformat(form.lastModified,'mm/dd/yyyy')# at #timeformat(form.lastModified,'hh:mm tt')#</td>
							</tr>
							</cfoutput>
							</cfif>
							<tr>
								<td><b>Phone Number: </b></td>
								<td><b>Connection Type:</b></td>
								<td><b>DNIS:</b></td>
								<td><b>Receiver(s):</b></td>
								<td><b>Used For: </b></td>
							</tr>
							<tr>
								<td valign="top">
									<cfoutput>#form.phoneNumber#</cfoutput>
								</td>
								<td valign="top">
									<cfoutput>#form.connectionType#</cfoutput>
								</td>
								<td valign="top">
									<cfoutput>#form.dnis#</cfoutput>
								</td>
								<td valign="top" class="nopadding">
									<cfloop list="#form.associatedReceivers#" index="rcvr" delimiters=",">
									<cfoutput>
									#rcvr#<br />
									</cfoutput>
									</cfloop>
									<!--- <cfoutput>#form.receiver#</cfoutput> --->
								</td>
								<td valign="top">
									<cfoutput>#form.usedFor#</cfoutput>
								</td>
							</tr>
							<tr>
								<td><b>Number Linked To: </b></td>
								<td><b>Link Method:</b></td>
								<td><b>HUNT Group:</b></td>
								<td><b>Trunk:</b></td>
								<td><b>Ext Link: </b></td>
							</tr>
							<tr>
								<td>
									<cfoutput>#form.linkedTo#</cfoutput>								</td>
								<td>
									<cfoutput>#form.getLinkMethod#</cfoutput>								</td>
								<td>
									<cfoutput>#form.huntGroup#</cfoutput>								</td>
								<td>
									<cfoutput>#form.trunk#</cfoutput>								</td>
								<td>
									<cfoutput>#form.extLink#</cfoutput>								</td>
							</tr>
							<tr>
								<td colspan="2"><b>Description:</b></td>
								<td colspan="3"><b>Associated Dealer(s):</b> </td>
							</tr>
							<tr>
								<td colspan="2" valign="top">
									<cfoutput>#form.description#</cfoutput>
								</td>
								<td colspan="3" valign="top" class="nopadding">
									<table width="100%" border="0" cellspacing="0" cellpadding="5">
										<cfloop list="#form.associatedDealers#" index="d" delimiters="~">
										<cfset dlr = listgetat(d,1,":")>
										<cfset isSub = listgetat(d,2,":")>
										<cfoutput>
										<tr>
											<td  nowrap="nowrap">
											#dlr# <cfif isSub is not 0>(sub)</cfif>
											</td>
										</tr>
										</cfoutput>
										</cfloop>
									</table>
								</td>
							</tr>
							<tr>
							    <td colspan="2"><b>Alt Dnis: </b></td>
							    <td><b>Alt Trunk Group: </b></td>
							    <td><b>Alt Ext Link: </b></td>
							    <td>&nbsp;</td>
						    </tr>
							<cfoutput>
							<tr>
							    <td colspan="2">#form.altDnis#</td>
							    <td>#form.altTrunkGroup#</td>
							    <td>#form.altExtLink#</td>
							    <td>&nbsp;</td>
						    </tr>
							</cfoutput>
							<tr>
							    <td colspan="2" nowrap="nowrap"><b>Alternate Link Method: </b></td>
							    <td nowrap="nowrap"><b>Alternate Switched Term: </b></td>
							    <td nowrap="nowrap">&nbsp;</td>
							    <td nowrap="nowrap">&nbsp;</td>
						    </tr>
							<cfoutput>
							<tr>
							    <td colspan="2">#form.alternateLinkMethod#</td>
							    <td>#form.alternateSwitchedTerm#</td>
							    <td>&nbsp;</td>
							    <td>&nbsp;</td>
						    </tr>
							</cfoutput>
							<tr>
								<td colspan="2"><b>Account Ranges:</b> </td>
								<td><b>Primacy:</b></td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td colspan="2"><cfoutput>#form.accountRanges#</cfoutput></td>
								<td><cfoutput>#form.primacy#</cfoutput></td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td colspan="5"><b>Comments:</b></td>
							</tr>
							<tr>
								<td colspan="5">
									<cfoutput>#replace(trim(comments),"#chr(10)#","<br />","all")#</cfoutput>
								</td>
							</tr>
							<tr>
								<td><b>Carrier:</b></td>
								<td><b>Account Number:</b></td>
								<td><b>Circuit ID:</b></td>
								<td valign="top"><b>Trunk Group:</b></td>
								<td valign="top">&nbsp;</td>
							</tr>
							<tr>
								<td>
									<cfoutput>#form.carrier#</cfoutput>								</td>
								<td>
									<cfoutput>#form.account#</cfoutput>								</td>
								<td>
									<cfoutput>#form.circuitID#</cfoutput>								</td>
								<td colspan="2" valign="top">
									<cfoutput>#form.trunkGroup#</cfoutput>								</td>
							</tr>
							<tr>
								<td><b>C24 Profile: </b></td>
								<td><b>3-Digit:</b></td>
								<td><b>Pad With: </b></td>
								<td valign="top"><b>Release in Letter:</b> </td>
								<td valign="top"><b>Two Way:</b></td>
							</tr>
							<tr>
								<td>
									<cfoutput>#form.c24Profile#</cfoutput>								</td>
								<td>
									<cfoutput>#form.threeDigit#</cfoutput>								</td>
								<td>
									<cfoutput>#form.padWith#</cfoutput>								</td>
								<td valign="top"><cfoutput>#form.dealerLetter#</cfoutput></td>
								<td valign="top">
								<cfif form.twoWay is 1>Yes<cfelse>No</cfif>								</td>
							</tr>
							<tr>
								<td><b>Location:</b></td>
								<td><b>Who Owns:</b></td>
								<td><b>Who Pays:</b></td>
								<td valign="top">&nbsp;</td>
								<td valign="top">&nbsp;</td>
							</tr>
							<tr>
								<td>
									<cfoutput>#form.location#</cfoutput>								</td>
								<td>
									<cfoutput>#form.whoOwns#</cfoutput>								</td>
								<td colspan="3">
									<cfoutput>#form.whoPays#</cfoutput>
									<cfif trim(form.whoPaysRate) is not "">@ 
									<cfoutput>#dollarformat(form.whoPaysRate)#</cfoutput>
									</cfif>									</td>
							</tr>
							<tr>
								<td colspan="5" class="nopadding">
									<table border="0" cellspacing="0" cellpadding="5">
										<tr>
											<td style="padding-right:0px">
												<input type="checkbox" name="active" <cfif form.active is 1>checked</cfif> value="1" />											</td>
											<td>Active&nbsp;&nbsp;</td>
											<td style="padding-right:0px">
												<input type="checkbox" name="singleCo" <cfif form.singleCo is 1>checked</cfif> value="1" />											</td>
											<td>Single Co.&nbsp;&nbsp; </td>
											<td style="padding-right:0px">
												<input type="checkbox" name="payPhoneBlock" <cfif form.payPhoneBlock is 1>checked</cfif> value="1" />											</td>
											<td>Pay Phone Block&nbsp;&nbsp; </td>
											<td style="padding-right:0px">
												<input type="checkbox" name="orderPending" <cfif form.orderPending is 1>checked</cfif> value="1" />											</td>
											<td nowrap="nowrap">Order Pending</td>
										</tr>
									</table>								</td>
							</tr>
							<cfif orderPending is 1>
							<tr>
								<td colspan="5" class="nopadding">
									<table border="0" cellspacing="0" cellpadding="5">
										<tr>
											<td>
											<b>Order Date:</b>
											<cfoutput>#dateformat(form.orderDate,'mm/dd/yyyy')#</cfoutput>											</td>
											<td>&nbsp;</td>
											<td>
											<b>Order Due Date:</b>
											<cfoutput>#dateformat(form.orderDueDate,'mm/dd/yyyy')#</cfoutput>											</td>
										</tr>
									</table>								</td>
							</tr>
							<tr>
								<td colspan="5" >
								<b>Pending Order Details:</b><br />
								<cfoutput>#replace(orderDetails, chr(13), "<br />", "all")#</cfoutput>
								</td>
							</tr>
							</cfif>
							<tr>
								<td colspan="5" >
								<b>Linked to this number:</b><br />
								<cfoutput query="getLinkedto">
								#phoneNumber# owned by #whoowns#<br />
								</cfoutput>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>