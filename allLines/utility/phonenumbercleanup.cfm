<cfsetting showdebugoutput="no">

<cffunction name="formatPhone" access="private" returntype="string">
	<cfargument name="inPhone" type="string" required="yes">
	<cfset outPhone = "">
	
		<cfif findnocase(".",inPhone) is not 0>
			<cfset outPhone = trim(inPhone)>
		<cfelse>
			<cfset tempPhone = trim(inPhone)>
			<cfset tempPhone = replace(tempPhone,"(","","all")>
			<cfset tempPhone = replace(tempPhone,")","","all")>
			<cfset tempPhone = replace(tempPhone,"-","","all")>
			<cfset tempPhone = replace(tempPhone," ","","all")>
			<cfif isNumeric(tempPhone) and len(tempPhone) is 10>
				<cfset outPhone = left(tempPhone,3) & "-" & mid(tempPhone,4,3) & "-" & right(tempPhone,4)>
			<cfelse>
				<cfset outPhone = inPhone>
			</cfif>
		</cfif>
	<!--- <cfif outPhone is not inPhone><cfset outPhone = outPhone & " changed!"></cfif> --->
	<cfreturn outPhone>
</cffunction>

<cfquery name="getNumbers" datasource="allLines">
	select id, phoneNumber 
	from all_lines 
	order by id asc 
</cfquery>

<cfoutput query="getNumbers">
<cfset newPhone = formatPhone(phoneNumber)>
<cfquery name="updateitem" datasource="allLines">
	update all_lines 
	set phoneNumber = '#newPhone#' 
	where id = #id# 
</cfquery>
#id#. #phoneNumber# cleaned up to #newPhone#<br />
</cfoutput>

