<link rel="stylesheet" type="text/css" href="/styles.css">
<cfparam name="form.dealerNumber" default="">
<cfoutput>
<cfif isDefined("form.btnSubmit")>
	<cfif form.DealerNumber is not "">
		<cfquery datasource="#DS#" name="updateAllLine">
		update all_lines
		set pendingDeletion = 1
		where dlrNumber = "#form.dealerNumber#"
		</cfquery>
	</cfif>
</cfif>
</cfoutput>
<div align="center">
<table border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="highlightbar"><b>Update a dealer in all lines</b> </td>
	</tr>
	<tr>
		<td class="greyrowbottomnopadding">
		<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td><b>To update the dealer, enter the dealer number</b></td>
			</tr>
			<form method="post" action="pendingDeletion.cfm">
			<tr>
				<td class="nopadding">
				<table border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td>Please Enter the Dealer Number: </td>
						<td>
						<input type="text" name="dealerNumber" style="width:50px; vertical-align:middle;" value="<cfoutput>#form.dealerNumber#</cfoutput>">
						<input name="btnSubmit" type="submit" class="sidebarsmall" style="vertical-align:middle;" value="Update">
						</td>
					</tr>
				</table>
				</td>
			</tr>
			</form>
		</table>
		</td>
	</tr>
</table>