<cfsetting showdebugoutput="no">

<cfset qString = urlDecode(qString)>
<cfwddx action="wddx2cfml" input="#qString#" output="qGetItems">

<cfset PDFFileName = "#dateformat(now(),'mmddyyyy')#_#TimeFormat(now(),'hhmmss')#.pdf">
<cfset outputfilepath = "#request.DirectPath#\allLines\reports\files">
<cfset downloadURL = "/allLines/reports/files">

<cfdocument format="pdf" scale="80" filename="#outputfilepath#\#PDFFileName#">
<link rel="stylesheet" type="text/css" href="../styles.css">
<div align="center">
<p class="normal" style="font-size:18px">
<b>Search Results Report</b>
</p>
<br>
<table border="0" cellspacing="0" cellpadding="3" width="100%">
<tr>
	<td nowrap="nowrap" class="linedrow">&nbsp;</td>
	<td nowrap="nowrap" class="linedrow"><b>Phone Number </b></td>
	<td nowrap="nowrap" class="linedrow"><b>DNIS</b></td>
	<td nowrap="nowrap" class="linedrow"><b>Receiver</b></td>
	<td nowrap="nowrap" class="linedrow"><b>Dealer #</b></td>
	<td nowrap="nowrap" class="linedrow"><b>Active</b></td>
</tr>
<cfoutput query="qGetItems">
<cfif qGetItems.currentRow mod 2 is 0><cfset bgc = "##EEEEEE"><cfelse><cfset bgc = "##FFFFFF"></cfif>
<cfif qGetItems.active is "Yes">
<tr bgcolor="#bgc#">
	<td>#qGetItems.currentRow#.</td>
	<td>#phoneNumber#</td>
	<td>#DNIS#</td>
	<td>#receiver#</td>
	<td>#dlrNumber#</td>
	<td>#active#</td>
</tr>
</cfif>
</cfoutput>
</table>
</div>
</cfdocument>

<div align="center">
<br>
<br>
<cfoutput>
<a class="normal" href="#downloadURL#/#PDFFileName#">Report complete - right click here and choose save-as to download</a>
</cfoutput>
<br>
<br>
<a class="normal" href="javascript: history.go(-1);">Return to Previous Page</a>
</div>