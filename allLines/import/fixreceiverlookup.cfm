<cfset ds = "allLines">

<cfquery name="getDups" datasource="allLines">
	SELECT receiver, 
	 COUNT(receiver) AS NumOccurrences
	FROM receivers
	GROUP BY receiver
	HAVING ( COUNT(receiver) > 1 )
	ORDER BY RECEIVER ASC 
</cfquery>

<cfloop query="getDups">
	<cfset currentItem = getDups.receiver>
	
	<cfquery name="getIDs" datasource="#ds#">
		select receiverid from receivers where receiver = '#currentItem#' order by receiverid asc 
	</cfquery>
	<cfset idlist = valuelist(getIDs.receiverid)> 
	<cfset rid = listgetat(idlist,1)>
	<cfset deleteList = listdeleteat(idlist,1)>
	
	<cfquery name="updateLookup" datasource="#ds#">
		update receiver_lookup 
		set receiverid = #rid# 
		where receiverid IN (#idlist#) 
	</cfquery>
	
	<cfquery name="deleteOther" datasource="#ds#">
		delete from receivers where receiverid IN (#deleteList#) 
	</cfquery>
	
	<cfoutput>
	#currentItem# done<br>
	</cfoutput>
	<br>
</cfloop>

<!--- clean up receiver lookup --->
<cfquery name="GetDupLookups" datasource="#ds#">
	SELECT lineid, receiverid, count(*) as qty 
	FROM receiver_lookup  
	GROUP BY lineid, receiverid HAVING count(*)> 1
</cfquery>

<cfloop query="GetDupLookups">
	<cfquery name="getLookupIDs" datasource="#ds#">
		select * from receiver_lookup where receiverid = #GetDupLookups.receiverid# and lineid = #GetDupLookups.lineid#  
	</cfquery>
	<cfset lookupList = valuelist(getLookupIDs.receiverLookupID)>
	<cfset lookupID = listgetat(lookupList,1)>
	<cfset lookupDeleteList = listdeleteat(lookupList,1)>
	<cfquery name="deleteOtherLookups" datasource="#ds#">
		delete from receiver_lookup where receiverLookupID IN (#lookupDeleteList#)
	</cfquery>
</cfloop>

<br>
all done. 
