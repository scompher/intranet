
<cfset ds = "allLines">

<cfquery name="getDups" datasource="allLines">
	SELECT dealerNumber, COUNT(dealerNumber) AS NumOccurrences
	FROM dealers
	GROUP BY dealerNumber
	HAVING ( COUNT(dealerNumber) > 1 )
	ORDER BY dealerNumber ASC  
</cfquery>

<cfloop query="getDups">
	<cfset currentItem = getDups.dealerNumber>
	
	<cfquery name="getIDs" datasource="#ds#">
		select dealerid from dealers where dealerNumber = '#currentItem#' order by dealerid asc 
	</cfquery>
	<cfset idlist = valuelist(getIDs.dealerid)> 
	<cfset did = listgetat(idlist,1)>
	<cfset deleteList = listdeleteat(idlist,1)>
	
	<cfquery name="updateLookup" datasource="#ds#">
		update dealer_lookup 
		set dealerid = #did# 
		where dealerid IN (#idlist#) 
	</cfquery>
	
	<cfquery name="deleteOther" datasource="#ds#">
		delete from dealers where dealerid IN (#deleteList#) 
	</cfquery>

	<cfoutput>
	#currentItem# done<br>
	</cfoutput>
	<br>
</cfloop>

<!--- clean up dealer lookup --->
<cfquery name="GetDupLookups" datasource="#ds#">
	SELECT lineid, dealerid, count(*) as qty 
	FROM dealer_lookup  
	GROUP BY lineid, dealerid HAVING count(*)> 1
</cfquery>

<cfloop query="GetDupLookups">
	<cfquery name="getLookupIDs" datasource="#ds#">
		select * from dealer_lookup where dealerid = #GetDupLookups.dealerid# and lineid = #GetDupLookups.lineid#  
	</cfquery>
	<cfset lookupList = valuelist(getLookupIDs.dealerLookupID)>
	<cfset lookupID = listgetat(lookupList,1)>
	<cfset lookupDeleteList = listdeleteat(lookupList,1)>
	<cfquery name="deleteOtherLookups" datasource="#ds#">
		delete from dealer_lookup where dealerLookupID IN (#lookupDeleteList#)
	</cfquery>
</cfloop>


<br>
all done. 
