
<link rel="stylesheet" type="text/css" href="/styles.css">
<cfif isDefined("form.btnProcess")>

	<cffile action="upload" filefield="importfile" destination="#request.directpath#\allLines\import\import.xls" nameconflict="overwrite">
	<cfquery name="getStuff" datasource="alllinesimport">
		select phoneNumber, connectionType, dnis, receiver, usedFor, description, trunkGroup, active, dlrNumber, dlrName, singleCo, carrier, account, location, whoOwns, whoPays, hunt, comments, linkedTo, linkMethod, extLinkedTo, flatRate, circuitID, [pad-with] as padWith, payPhoneBlock, [c-24-profile] as c24Profile, [3-digit] as 3digit, trunk, orderPending, orderDate, orderDueDate, orderDetails, altDnis, altTrunkGroup, altExtLink 
		from "All Lines Import Template$"
		where PHONENUMBER <> ''
	</cfquery>
	
	<div align="center">
	<table border="0" cellspacing="0" cellpadding="5" width="400">
		<tr>
			<td class="highlightbar"><b>All Lines Import Utility </b></td>
		</tr>
		<tr>
			<td class="greyrowbottomnopadding">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<cfset counter = 0>
					<cfloop query="getstuff">
						<cfset counter = counter + 1>
						<cfif counter mod 2 is 0><cfset bgc = "##EEEEEE"><cfelse><cfset bgc = "##FFFFFF"></cfif>
						<cfquery name="checkExists" datasource="allLines">
							select phonenumber from all_lines 
							where phonenumber = '#getstuff.phonenumber#'
						</cfquery>
						<cfoutput>
						<cfif checkExists.recordcount gt 0>
							<cfif form.importType is "newOnly">
								<tr>
									<td bgcolor="#bgc#" style="color:##FF0000">#getStuff.currentrow#. #getstuff.phonenumber# already exists, item not saved.</td>
								</tr>
							<cfelse>
								<cfquery name="insertIntoAllLines" datasource="allLines">
									update all_lines
									set 
										phoneNumber = '#getStuff.phoneNumber#', 
										connectionType = '#getStuff.connectionType#', 
										dnis = '#getStuff.dnis#', 
										receiver = '#getStuff.receiver#', 
										usedFor = '#getStuff.usedFor#', 
										description = '#getStuff.description#', 
										trunkGroup = '#getStuff.trunkGroup#', 
										active = '#getStuff.active#', 
										dlrNumber = '#getStuff.dlrNumber#', 
										dlrName = '#getStuff.dlrName#', 
										singleCo = '#getStuff.singleCo#', 
										carrier = '#getStuff.carrier#', 
										account = '#getStuff.account#', 
										location = '#getStuff.location#', 
										whoOwns = '#getStuff.whoOwns#', 
										whoPays = '#getStuff.whoPays#', 
										hunt = '#getStuff.hunt#', 
										comments = '#getStuff.comments#', 
										linkedTo = '#getStuff.linkedTo#', 
										linkMethod = '#getStuff.linkMethod#', 
										extLinkedTo = '#getStuff.extLinkedTo#', 
										flatRate = '#getStuff.flatRate#', 
										circuitID = '#getStuff.circuitID#', 
										[pad-with] = '#getStuff.padwith#', 
										payPhoneBlock = '#getStuff.payPhoneBlock#', 
										[c-24-profile] = '#getStuff.c24profile#', 
										[3-digit] = '#getStuff.3digit#', 
										trunk = '#getStuff.trunk#', 
										orderPending = '#getStuff.orderPending#', 
										orderDate = '#getStuff.orderDate#', 
										orderDueDate = '#getStuff.orderDueDate#', 
										orderDetails = '#getStuff.orderDetails#', 
										altDnis = '#getStuff.altDnis#', 
										altTrunkGroup = '#getStuff.altTrunkGroup#', 
										altExtLink = '#getStuff.altExtLink#'  
									where phonenumber = '#getstuff.phonenumber#' 
								</cfquery>
								<tr>
									<td bgcolor="#bgc#" style="color:##0033FF">#getStuff.currentrow#. #getstuff.phonenumber# has been updated.</td>
								</tr>
							</cfif>
						<cfelse>
							<cfquery name="insertIntoAllLines" datasource="allLines">
								insert into all_lines (phoneNumber, connectionType, dnis, receiver, usedFor, description, trunkGroup, active, dlrNumber, dlrName, singleCo, carrier, account, location, whoOwns, whoPays, hunt, comments, linkedTo, linkMethod, extLinkedTo, flatRate, circuitID, [pad-with], payPhoneBlock, [c-24-profile], [3-digit], trunk, orderPending, orderDate, orderDueDate, orderDetails, altDnis, altTrunkGroup, altExtLink) 
								values ('#getStuff.phoneNumber#', '#getStuff.connectionType#', '#getStuff.dnis#', '#getStuff.receiver#', '#getStuff.usedFor#', '#getStuff.description#', '#getStuff.trunkGroup#', '#getStuff.active#', '#getStuff.dlrNumber#', '#getStuff.dlrName#', '#getStuff.singleCo#', '#getStuff.carrier#', '#getStuff.account#', '#getStuff.location#', '#getStuff.whoOwns#', '#getStuff.whoPays#', '#getStuff.hunt#', '#getStuff.comments#', '#getStuff.linkedTo#', '#getStuff.linkMethod#', '#getStuff.extLinkedTo#', '#getStuff.flatRate#', '#getStuff.circuitID#', '#getStuff.padwith#', '#getStuff.payPhoneBlock#', '#getStuff.c24profile#', '#getStuff.3digit#', '#getStuff.trunk#', '#getStuff.orderPending#', '#getStuff.orderDate#', '#getStuff.orderDueDate#', '#getStuff.orderDetails#', '#getStuff.altDnis#', '#getStuff.altTrunkGroup#', '#getStuff.altExtLink#') 
							</cfquery>
							<tr>
								<td bgcolor="#bgc#" style="color:##009900">#getStuff.currentrow#.  #getstuff.phonenumber# added.</td>
							</tr>
						</cfif>
						</cfoutput>
					</cfloop>
				</table>
			</td>
		</tr>
	</table>
	<br>
	<a class="normal" style="text-decoration:underline;" href="/index.cfm">Return to Intranet</a> | 
	<a class="normal" style="text-decoration:underline;" href="import.cfm">Return to Import Utility</a>
	</div>

<cfelse>

	<div align="center">
	<table border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td class="highlightbar"><b>All Lines Import Utility </b></td>
		</tr>
		<tr>
			<td class="greyrowbottomnopadding">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td><b>Select File to Import:</b> </td>
					</tr>
					<form method="post" action="import.cfm" enctype="multipart/form-data">
					<tr>
						<td>
							<input type="file" name="importfile" style="width:300px">
						</td>
					</tr>
					<tr>
						<td>
							<b>Import Type:</b> &nbsp;
							<select name="importType" style="vertical-align:middle">
								<option value="newOnly">Add New Only</option>
								<option value="update">Update Existing and Add New</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>
							<input name="btnProcess" type="submit" class="sidebar" value="Import File">
						</td>
					</tr>
					</form>
				</table>
			</td>
		</tr>
	</table>
	<br>
	<a class="normal" style="text-decoration:underline;" href="/index.cfm">Return to Intranet</a>
	</div>

</cfif>