<!--- columnnames = "phoneNumber, connectionType, dnis, receiver, usedFor, description, trunkGroup, active, dlrNumber, dlrName, singleCo, carrier, account, location, whoOwns, whoPays, hunt, comments, linkedTo, linkMethod, extLinkedTo, flatRate, circuitID, padWith, payPhoneBlock, c24Profile, 3digit,trunk,orderPending,orderDate,orderDueDate,orderDetails,altDnis,altTrunkGroup,altExtLink,recDNIS"  --->

<cfsetting showdebugoutput="no">

<cftry>

<cffunction name="formatPhone" access="private" returntype="string">
	<cfargument name="inPhone" type="string" required="yes">
	<cfset outPhone = "">
		
		<cfif trim(inPhone) is not "">
			<cfif findnocase(".",inPhone) is not 0>
				<cfset outPhone = trim(inPhone)>
			<cfelse>
				<cfset tempPhone = trim(inPhone)>
				<cfset tempPhone = replace(tempPhone,"(","","all")>
				<cfset tempPhone = replace(tempPhone,")","","all")>
				<cfset tempPhone = replace(tempPhone,"-","","all")>
				<cfset tempPhone = replace(tempPhone," ","","all")>
				<cfif isNumeric(tempPhone) and len(tempPhone) is 10>
					<cfset outPhone = left(tempPhone,3) & "-" & mid(tempPhone,4,3) & "-" & right(tempPhone,4)>
				<cfelse>
					<cfset outPhone = inPhone>
				</cfif>
			</cfif>
		</cfif>
		
		<cfset outPhone = trim(outPhone)>
	<cfreturn outPhone>
</cffunction>

<link rel="stylesheet" type="text/css" href="/styles.css">
<cfif isDefined("form.btnProcess")>

	<cffile action="upload" filefield="importfile" destination="#request.directpath#\allLines\import\import.xls" nameconflict="overwrite">
	
	<cfspreadsheet  
		action="read" 
		src = "#request.directpath#\allLines\import\import.xls" 
		headerrow = "1" 
		query = "readSheet"> 

		<cfquery name="getStuff" dbtype="query">
			select * from readSheet 
			where phoneNumber <> '' 
		</cfquery>
<!--- 
<cftry>
<cfoutput query="getStuff"> #currentrow#) 
<cfloop index="col" list="#columnlist#">
	#col#=#getStuff[col][currentRow]#=#len(getStuff[col][currentRow])#<br />
</cfloop> <p/> 
</cfoutput>
<cfcatch type="any">
<cfoutput>
#cfcatch.Message#<br />
#cfcatch.Detail#<br />
</cfoutput>
</cfcatch>
</cftry>
<cfabort>
 --->
 
	<div align="center">
	<table border="0" cellspacing="0" cellpadding="5" width="400">
		<tr>
			<td class="highlightbar"><b>All Lines Import Utility </b></td>
		</tr>
		<tr>
			<td class="greyrowbottomnopadding">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<cfset counter = 0>
					<cfloop query="getstuff">
						<cfset updateInsert = true>
						<cfset lineID = 0>
						<cfset unformattedPhone = getStuff.phoneNumber>
						<cfset thePhoneNumber = formatPhone(getStuff.phoneNumber)>
						<cfset counter = counter + 1>
						<cfif counter gt 1>
							<cfif counter mod 2 is 0><cfset bgc = "##EEEEEE"><cfelse><cfset bgc = "##FFFFFF"></cfif>
							<cfquery name="checkExists" datasource="allLines">
								select phonenumber, recDnis from all_lines 
								where phonenumber = '#thePhoneNumber#'
							</cfquery>
							
							<cfoutput>
							<cfif checkExists.recordcount gt 0>
							
								<cfif form.importType is "newOnly">
									<tr>
										<td bgcolor="#bgc#" style="color:##FF0000">#getStuff.currentrow#. #thePhoneNumber# already exists, item not saved.</td>
									</tr>
									<cfset updateInsert = false>
								<cfelse>
									<cfquery name="getLineID" datasource="allLines">
										select max(id) as maxid from all_lines 
										where phonenumber = '#trim(thePhoneNumber)#' 
									</cfquery>
									<cfset lineID = getLineID.maxid>
									
									<!--- this is what makes it special - if the DNIS is empty, update it.  Otherwise leave it alone --->
									<cfif trim(checkExists.recDnis) is "">
										<cfset updateInsert = true>
									<cfelse>
										<cfset updateInsert = false>
									</cfif>
									
									
									<cfif updateInsert is true>
										<cfquery name="insertIntoAllLines" datasource="allLines">
											update all_lines
											set 
												phoneNumber = '#trim(thePhoneNumber)#', 
												connectionType = '#trim(getStuff.connectionType)#', 
												dnis = '#trim(getStuff.dnis)#', 
												receiver = '#trim(getStuff.receiver)#', 
												usedFor = '#trim(getStuff.usedFor)#', 
												description = '#trim(getStuff.description)#', 
												trunkGroup = '#trim(getStuff.trunkGroup)#', 
												active = '#trim(getStuff.active)#', 
												dlrNumber = '#trim(getStuff.dlrNumber)#', 
												dlrName = '#trim(getStuff.dlrName)#', 
												singleCo = '#trim(getStuff.singleCo)#', 
												carrier = '#trim(getStuff.carrier)#', 
												account = '#trim(getStuff.account)#', 
												location = '#trim(getStuff.location)#', 
												whoOwns = '#trim(getStuff.whoOwns)#', 
												whoPays = '#trim(getStuff.whoPays)#', 
												hunt = '#trim(getStuff.hunt)#', 
												comments = '#trim(getStuff.comments)#', 
												linkedTo = '#trim(formatPhone(getStuff.linkedTo))#', 
												linkMethod = '#trim(getStuff.linkMethod)#', 
												extLinkedTo = '#trim(getStuff.extLinkedTo)#', 
												flatRate = '#trim(getStuff.flatRate)#', 
												circuitID = '#trim(getStuff.circuitID)#', 
												[pad-with] = '#trim(getStuff.padwith)#', 
												payPhoneBlock = '#trim(getStuff.payPhoneBlock)#', 
												[c-24-profile] = '#trim(getStuff.c24profile)#', 
												[3-digit] = '#trim(getStuff.3digit)#', 
												trunk = '#trim(getStuff.trunk)#', 
												orderPending = '#trim(getStuff.orderPending)#', 
												orderDate = '#trim(getStuff.orderDate)#', 
												orderDueDate = '#trim(getStuff.orderDueDate)#', 
												orderDetails = '#trim(getStuff.orderDetails)#', 
												altDnis = '#trim(getStuff.altDnis)#', 
												altTrunkGroup = '#trim(getStuff.altTrunkGroup)#', 
												altExtLink = '#trim(getStuff.altExtLink)#', 
												recDNIS = '#trim(getStuff.recDNIS)#'   
											where id = #trim(lineID)# 
										</cfquery>

										<tr>
											<td bgcolor="#bgc#" style="color:##0033FF">#evaluate(getStuff.currentrow - 1)#. #thePhoneNumber# has been updated.</td>
										</tr>
										
									<cfelse>
									
										<tr>
											<td bgcolor="#bgc#" style="color:##0033FF">#evaluate(getStuff.currentrow - 1)#. #thePhoneNumber# update skipped.</td>
										</tr>
										
									</cfif>
									
								</cfif>
								
							<cfelse>
							
								<cfquery name="insertIntoAllLines" datasource="allLines">
									insert into all_lines (phoneNumber, connectionType, dnis, receiver, usedFor, description, trunkGroup, active, dlrNumber, dlrName, singleCo, carrier, account, location, whoOwns, whoPays, hunt, comments, linkedTo, linkMethod, extLinkedTo, flatRate, circuitID, [pad-with], payPhoneBlock, [c-24-profile], [3-digit], trunk, orderPending, orderDate, orderDueDate, orderDetails, altDnis, altTrunkGroup, altExtLink, recDNIS) 
									values ('#thePhoneNumber#', '#getStuff.connectionType#', '#getStuff.dnis#', '#getStuff.receiver#', '#getStuff.usedFor#', '#getStuff.description#', '#getStuff.trunkGroup#', '#getStuff.active#', '#getStuff.dlrNumber#', '#getStuff.dlrName#', '#getStuff.singleCo#', '#getStuff.carrier#', '#getStuff.account#', '#getStuff.location#', '#getStuff.whoOwns#', '#getStuff.whoPays#', '#getStuff.hunt#', '#getStuff.comments#', '#getStuff.linkedTo#', '#getStuff.linkMethod#', '#getStuff.extLinkedTo#', '#getStuff.flatRate#', '#getStuff.circuitID#', '#getStuff.padwith#', '#getStuff.payPhoneBlock#', '#getStuff.c24profile#', '#getStuff.3digit#', '#getStuff.trunk#', '#getStuff.orderPending#', '#getStuff.orderDate#', '#getStuff.orderDueDate#', '#getStuff.orderDetails#', '#getStuff.altDnis#', '#getStuff.altTrunkGroup#', '#getStuff.altExtLink#', '#getStuff.recDNIS#') 
								</cfquery>
								<!--- get line ID --->
								<cfquery name="getLineID" datasource="allLines">
									select max(id) as maxid from all_lines 
									where phoneNumber = '#trim(thePhoneNumber)#' 
								</cfquery>
								<cfset lineID = getLineID.maxid>
								<tr>
									<td bgcolor="#bgc#" style="color:##009900">#getStuff.currentrow#.  #thePhoneNumber# added.</td>
								</tr>
							
							</cfif>
							
							<cfif updateInsert is true>
								<cfif trim(getStuff.receiver) is not "">
									<!--- if receiver exists, update lookup. If not, create and then insert into lookup --->
									<cfquery name="checkReceiver" datasource="allLines">
										select max(receiverID) as maxid from receivers where receiver = '#trim(getStuff.receiver)#'
									</cfquery>
									<cfif trim(checkReceiver.maxid) is not "">
										<cfset RID = checkReceiver.maxid>
										<cfquery name="deleteLookup" datasource="allLines">
											delete from Receiver_Lookup where lineID = #lineid# and receiverID = #RID# 
										</cfquery>
<									<cfelse>
										<!--- insert receiver --->
										<cfquery name="addReceiver" datasource="allLines">
											insert into receivers (receiver) values ('#trim(receiver)#') 
										</cfquery>
									</cfif>
									<cfquery name="getRID" datasource="allLines">
										select max(receiverID) as maxid from receivers where receiver = '#trim(getStuff.receiver)#'
									</cfquery>
									<cfset RID = getRID.maxid>
									<cfquery name="insertLookup" datasource="allLines">
										insert into Receiver_Lookup (lineID, receiverID) values (#lineid#, #RID#) 
									</cfquery>
								</cfif>
								
								<cfset RID = 0>
								
								<cfif trim(getStuff.dlrNumber) is not "">
									<!--- if dealer exists, update lookup. If not, create and then insert into lookup --->
									<cfquery name="checkDealer" datasource="allLines">
										select max(dealerID) as maxid from dealers where dealerNumber = '#trim(getStuff.dlrNumber)#'
									</cfquery>
									<cfif trim(checkDealer.maxid) is not "">
										<cfset DID = checkDealer.maxid>
										<cfquery name="deleteLookup" datasource="allLines">
											delete from dealer_lookup where lineID = #lineid# and dealerID = #DID# 
										</cfquery>
									<cfelse>
										<!--- insert dealer --->
										<cfquery name="addDealer" datasource="allLines">
											insert into dealers (dealerNumber, dealerName) values ('#trim(getStuff.dlrNumber)#', '#trim(getStuff.dlrName)#') 
										</cfquery>
									</cfif>									
									<cfquery name="getDID" datasource="allLines">
										select max(dealerID) as maxid from dealers where dealerNumber = '#trim(getStuff.dlrNumber)#'
									</cfquery>
									<cfset DID = getDID.maxid>
									<cfquery name="insertLookup" datasource="allLines">
										insert into dealer_lookup (lineID, dealerID) values (#lineid#, #DID#) 
									</cfquery>
								</cfif>
								
								<cfset DID = 0>
								
							</cfif>
							
							</cfoutput>
						</cfif>
					</cfloop>
				</table>
			</td>
		</tr>
	</table>
	<br>
	<a class="normal" style="text-decoration:underline;" href="/index.cfm">Return to Intranet</a> | 
	<a class="normal" style="text-decoration:underline;" href="production_import.cfm">Return to Import Utility</a>
	</div>

<cfelse>

	<div align="center">
	<table border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td class="highlightbar"><b>All Lines Import Utility </b></td>
		</tr>
		<tr>
			<td class="greyrowbottomnopadding">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td><b>Select File to Import:</b> </td>
					</tr>
					<form method="post" action="special_production_import.cfm" enctype="multipart/form-data">
					<tr>
						<td>
							<input type="file" name="importfile" style="width:300px">
						</td>
					</tr>
					<tr>
						<td>
							<b>Import Type:</b> &nbsp;
							<select name="importType" style="vertical-align:middle">
								<option value="newOnly">Add New Only</option>
								<option value="update">Update Existing and Add New</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>
							<input name="btnProcess" type="submit" class="sidebar" value="Import File">
						</td>
					</tr>
					</form>
				</table>
			</td>
		</tr>
	</table>
	<br>
	<a class="normal" style="text-decoration:underline;" href="http://192.168.1.10/index.cfm">Return to Intranet</a>
	</div>

</cfif>

<cfcatch type="any">
<cfoutput>
<cfquery name="getItem" dbtype="query">
	select * from getStuff where phoneNumber = '#unformattedPhone#' 
</cfquery>
<cfdump var="#getItem#">
#thePhoneNumber#<br />
#cfcatch.Message#<br />
#cfcatch.Detail#<br />
</cfoutput>
</cfcatch>

</cftry>