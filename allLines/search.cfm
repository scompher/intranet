<link rel="stylesheet" type="text/css" href="../styles.css">

<cfparam name="departmentList" default="0">

<script language="javascript" type="text/javascript">
function OpenDetailWin(id) {
	var windowWidth = 750;
	var windowHeight = 650;
	var left = (screen.width / 2) - (windowWidth / 2);
	var top = (screen.height / 2) - (windowHeight/ 2) - 150;
	DetailWin = window.open("detailview.cfm?id=" + id,"DetailWin","top=" + top + ",left=" + left + ",height=" + windowHeight + ",width=" + windowWidth + ",resizable=1,scrollbars=1,status=0,titlebar=0,toolbar=0");
}

function OpenEditWin(id) {
	var windowWidth = 900;
	var windowHeight = 850;
	var left = (screen.width / 2) - (windowWidth / 2);
	var top = (screen.height / 2) - (windowHeight/ 2) - 150;
	EditWin = window.open("edit.cfm?getItem=1&id=" + id,"EditWin","top=" + top + ",left=" + left + ",height=" + windowHeight + ",width=" + windowWidth + ",resizable=1,scrollbars=1,status=0,titlebar=0,toolbar=0");
}

function checkUncheck(frm) {
	if (frm.checkStatus.value == 1) {
		var checkStatus = false;
		frm.checkStatus.value = 0;
	} else {
		var checkStatus = true;
		frm.checkStatus.value = 1;
	}
	if (frm.delItemID.length > 1) {
		for (var i=0; i < frm.delItemID.length; i++) {
			frm.delItemID[i].checked = checkStatus;
		}
	} else {
		frm.delItemID.checked = checkStatus;
	}
}

function deleteChecked(frm) {
	if (confirm('Are you sure you wish to delete all selected items?')) {
		frm.deleteConfirmed.value = 1;
		frm.submit();
	}
}
</script>

<cfinclude template="functions.cfm">

<cfparam name="form.searchphoneNumber" default="">
<cfparam name="form.searchdnis" default="">
<cfparam name="form.searchreceiver" default="">
<cfparam name="form.searchdealerNumber" default="">
<cfparam name="form.searchusedFor" default="">
<cfparam name="form.sortResults" default="">
<cfparam name="form.deleteConfirmed" default="">
<cfparam name="message" default="">
<cfparam name="form.delItemID" default="0">

<cfset ds = "allLines">

<cfif form.deleteConfirmed is 1>
	<cfif form.delItemID is not 0>
		<cfset result = deleteItems(form.delItemID)>
		<cfset message = "#result# items deleted successfully">
	</cfif>
</cfif>

<cfif isDefined("form.btnSearch")>
	<cfset qGetItems = searchItems(form)>
	<cfwddx action="cfml2wddx" input="#qGetItems#" output="qString">
</cfif>

<cfquery name="getUsedFor" datasource="#ds#">
	select * from usedFor
	order by usedFor asc 
</cfquery>

<body onLoad="document.forms[0].searchPhoneNumber.focus();">
<div align="center">
	<table border="0" cellpadding="5" cellspacing="0">
		<tr>
			<td class="highlightbar"><b>Lookup Form</b> </td>
		</tr>
		<tr>
			<td class="greyrowbottom">
				<form method="post" action="search.cfm">
				<table width="100%" border="0" cellpadding="3" cellspacing="0">
					<tr>
						<td colspan="5"><b>Search Items By</b> (Use * as a wildcard) - Phone # must be in format xxx-xxx-xxxx</td>
					</tr>
					<tr>
						<td nowrap="nowrap">Phone Number:</td>
						<td>DNIS/Ext Link:</td>
						<td>Receiver:</td>
						<td>Dealer Number: </td>
						<td>Used For: </td>
					</tr>
					<tr>
						<td nowrap="nowrap">
							<input onFocus="this.select();" name="searchPhoneNumber" type="text" value="<cfoutput>#form.searchphoneNumber#</cfoutput>" />
						</td>
						<td>
							<input name="searchdnis" type="text" value="<cfoutput>#form.searchdnis#</cfoutput>" />
						</td>
						<td>
							<input name="searchreceiver" type="text" value="<cfoutput>#form.searchreceiver#</cfoutput>" />
						</td>
						<td>
							<input name="searchDealerNumber" type="text" value="<cfoutput>#form.searchdealerNumber#</cfoutput>" />
						</td>
						<td>
							<select name="searchUsedFor">
								<option value=""></option>
								<cfoutput query="getUsedFor">
									<option <cfif form.searchUsedFor is getUsedFor.usedFor>selected</cfif> value="#usedFor#">#usedFor#</option>
								</cfoutput>
							</select>
						</td>
					</tr>
					<tr>
						<td colspan="5">Sort Results: </td>
					</tr>
					<tr>
						<td colspan="5">
						<select name="sortResults">
							<option value=""></option>
							<option <cfif form.sortResults is "phoneASC">selected</cfif> value="phoneASC">Phone Number Low to High</option>
							<option <cfif form.sortResults is "phoneDESC">selected</cfif> value="phoneDESC">Phone Number High to Low</option>
							<option <cfif form.sortResults is "dnisASC">selected</cfif> value="dnisASC">DNIS Low to High</option>
							<option <cfif form.sortResults is "dnisDESC">selected</cfif> value="dnisDESC">DNIS High to Low</option>
							<option <cfif form.sortResults is "receiverASC">selected</cfif> value="receiverASC">Receiver Low to High</option>
							<option <cfif form.sortResults is "receiverDESC">selected</cfif> value="receiverDESC">Receiver High to Low</option>
							<option <cfif form.sortResults is "dealerNumberASC">selected</cfif> value="dealerNumberASC">Dealer Number Low to High</option>
							<option <cfif form.sortResults is "dealerNumberDESC">selected</cfif> value="dealerNumberDESC">Dealer Number High to Low</option>
						</select>
						</td>
					</tr>
					<tr>
						<td colspan="5" nowrap="nowrap">
							<input name="btnSearch" type="submit" class="sidebar" value="Search Now" />
							<input type="button" class="sidebar" value="Clear Form" onClick="this.form.searchPhoneNumber.value = ''; this.form.searchdnis.value = ''; this.form.searchreceiver.value = ''; this.form.searchDealerNumber.value = ''; this.form.searchUsedFor.selectedIndex = 0; this.form.sortResults.selectedIndex = 0;" />
							<input type="button" name="btnNewSearch" value="New Search" class="sidebar" onClick="document.location='search.cfm';" />
						</td>
					</tr>
				</table>
				</form>
			</td>
		</tr>
	</table>
	<cfif trim(message) is not "">
	<br />
	<br />
	<span class="alert">
	<cfoutput><b>#message#</b></cfoutput>
	</span>
	<br />
	<br />
	</cfif>
	<cfif isDefined("qGetItems")>
		<br />
		<span class="normal">
		<b style="font-size:18px">Search Results</b>
		<br /><br />
		<cfoutput>#numberFormat(qGetItems.recordcount)# items found</cfoutput>
		</span>
		<form method="post" action="viewPDF.cfm" name="resultform">
		<input type="hidden" name="qString" value="<cfoutput>#urlEncodedFormat(qString)#</cfoutput>" />
		<a class="normal" href="javascript:document.resultform.submit();">
		<img src="../images/pdfIconWhite.gif" align="absmiddle" border="0" />
		Click here to download PDF
		</a>
		</form>
		<br />
		<table border="0" cellspacing="0" cellpadding="3" width="90%">
		<tr>
			<td colspan="15">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td colspan="2"><b>Legend:</b></td>
				</tr>
				<tr>
					<td width="2%" bgcolor="#FFFF66">&nbsp;&nbsp;</td>
					<td width="98%">Order Pending</td>
				</tr>
				<tr>
					<td bgcolor="#FF5555">&nbsp;&nbsp;</td>
					<td>Inactive</td>
				</tr>
				<tr>
					<td bgcolor="#B9B9FF">&nbsp;&nbsp;</td>
					<td>Private</td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td colspan="3" nowrap="nowrap" class="linedrow">
			<cfif listfind(departmentlist,1) is not 0>
				<a href="javascript: checkUncheck(document.outputForm);"><img alt="Check All/Uncheck All" src="../images/checkBox.gif" border="0" /></a>
				<a href="javascript: deleteChecked(document.outputForm);"><img alt="Delete Selected Items" src="../images/trash.gif" border="0" /></a>
			<cfelse>
				&nbsp;
			</cfif>
			</td>
			<td nowrap="nowrap" class="linedrow"><b>Phone Number </b></td>
			<td nowrap="nowrap" class="linedrow"><b>DNIS</b></td>
			<td nowrap="nowrap" class="linedrow"><b>Rec DNIS</b></td>
			<td nowrap="nowrap" class="linedrow"><b>Ext Link</b></td>
			<td nowrap="nowrap" class="linedrow"><b>Linked To</b></td>
			<td nowrap="nowrap" class="linedrow"><b>Receiver</b></td>
			<td nowrap="nowrap" class="linedrow"><b>Dealer #</b></td>
			<td nowrap="nowrap" class="linedrow"><b>Dealer Name</b></td>
			<td nowrap="nowrap" class="linedrow"><b>Used For</b></td>
			<td nowrap="nowrap" class="linedrow"><b>Active</b></td>
		</tr>
		<form method="post" action="search.cfm" name="outputForm">
		<input type="hidden" name="checkStatus" value="0" />
		<input type="hidden" name="deleteConfirmed" value="0" />
		<cfoutput query="qGetItems">
		<cfif qGetItems.currentRow mod 2 is 0><cfset bgc = "##EEEEEE"><cfelse><cfset bgc = "##FFFFFF"></cfif>
		
		<cfif qGetItems.orderPending is 1>
			<cfset bgc = "##FFFF66">
		<cfelseif qGetItems.active is 0>
			<cfset bgc = "##FF5555">
		<cfelseif qGetItems.private is 1>
			<cfset bgc = "##B9B9FF">
		</cfif>

		<tr bgcolor="#bgc#">
			<td>
			<cfif listfind(departmentlist,1) is not 0>
				<input type="checkbox" name="delItemID" value="#ID#" />
			<cfelse>
				&nbsp;
			</cfif>
			</td>
			<td>#qGetItems.currentRow#.</td>
			<td nowrap="nowrap">
				<a href="javascript: OpenDetailWin(#qGetItems.ID#);">[view details]</a>
				<cfif listfind(departmentlist,1) is not 0>
					<a href="javascript: OpenEditWin(#qGetItems.ID#);">[edit item]</a>
				</cfif>
			</td>
			<td>#phoneNumber#&nbsp;</td>
			<td>#DNIS#&nbsp;</td>
			<td>#recDnis#&nbsp;</td>
			<td>#ExtLinkedTo#&nbsp;</td>
			<td>#LinkedTo#&nbsp;</td>
			<td>#receiver#&nbsp;</td>
			<td>#dlrNumber#&nbsp; <cfif qGetItems.isSubDealer is not "0">(sub)</cfif></td>
			<td>#dlrName#&nbsp;</td>
			<td>#usedFor#&nbsp;</td>
			<td>#active#&nbsp;</td>
		</tr>
		</cfoutput>
		</form>
	</table>
</cfif>
<cfif isDefined("cookie.adminLogin")>
<br />
<a class="normal" href="/index.cfm" style="text-decoration:underline">Return to Intranet Menu</a>
</cfif>
</div>
</body>
