
<cfset XLSFileName = "#dateformat(now(),'mmddyyyy')#_#TimeFormat(now(),'hhmmss')#.xls">
<cfset outputfilepath = "#request.DirectPath#\allLines\reports\files">
<cfset downloadURL = "/allLines/reports/files">

<cftry>

<cfquery name="getData" datasource="allLines">
	select 
		PhoneNumber, 
		UsedFor, 
		[DESCRIPTION], 
		SingleCo, 
		WhoOwns, 
		WhoPays, 
		DNIS, 
		ConnectionType, 
		LinkedTo, 
		LinkMethod, 
		HUNT, 
		Trunk, 
		ExtLinkedTo, 

		STUFF
		(
		(SELECT ',' + dealerNumber 
		from dealers  
		left join dealer_lookup on dealers.DealerID = dealer_lookup.dealerID
		WHERE all_lines.ID = dealer_lookup.lineID  and isSubDealer = 0  
		FOR XML PATH('')),1,1,'') AS dlrNumber, 

		Comments, 
		Active, 
		Location, 
		[GL comments], 
		OrderPending, 
		OrderDetails, 
		Carrier, 
		Account, 
		CircuitID, 
		TrunkGroup, 
		Lock, 
		OrderDate, 
		OrderDueDate, 
		[C-24-profile], 
		[3-Digit], 
		[Pad-with], 
		PayphoneBlock, 
		FlatRate, 
		lastModified, 
		lastModifiedBy, 
		dealerLetter, 
		accountRanges, 
		manitou, 
		primacy, 
		validated, 
		twoWay, 
		[private]
	from all_lines
	where PhoneNumber not like '%.%' 
	order by PhoneNumber asc 
</cfquery>


<cfloop query="getData">
	<cfset comm = getData.Comments>
	<cfset comm = replace(comm, "#chr(13)##chr(10)#", "; ", "all")>
	<cfset getData.Comments = comm>
</cfloop>

<cfset headerlist = getData.columnList>

<cfx_query2excel 
	file="#outputFilePath#\#XLSFileName#" 
	headings="#headerlist#" 
	queryFields="#headerlist#" 
	query="#getData#" 
	format="excel">

<div align="center">
<br>
<br>
<cfoutput>
<a class="normal" href="#downloadURL#/#XLSFileName#">Report complete - right click here and choose save-as to download</a>
</cfoutput>
<br>
<br>
<a class="normal" href="/index.cfm">Return to Intranet Menu</a>
</div>

<cfcatch type="any">
<cfoutput>
#cfcatch.Message#<br />
#cfcatch.Detail#<br />
</cfoutput>
</cfcatch>
</cftry>
