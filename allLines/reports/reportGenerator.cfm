<link rel="stylesheet" type="text/css" href="../../styles.css">

<cfinclude template="../functions.cfm">

<script type="text/javascript" language="javascript">
function validateWhere(frm) {
	if (frm.whereField.selectedIndex == 0) {alert('The criteria field is required.'); return false;}
	if (frm.operator.selectedIndex == 0) {alert('The operator is required.'); return false;}
	return true;
}

function validateSort(frm) {
	if (frm.sortField.selectedIndex == 0) {alert('The sort field is required.'); return false;}
	if (frm.sortorder.selectedIndex == 0) {alert('The sort order is required.'); return false;}
	return true;
}

function checkAllFields(frm) {
	for (var i=0; i < frm.includeColumn.length; i++) {
		frm.includeColumn[i].checked = true;
	}
}

function UnCheckAllFields(frm) {
	for (var i=0; i < frm.includeColumn.length; i++) {
		frm.includeColumn[i].checked = false;
	}
}

function OpenEditWin(id) {
	var windowWidth = 900;
	var windowHeight = 600;
	var left = (screen.width / 2) - (windowWidth / 2);
	var top = (screen.height / 2) - (windowHeight/ 2) - 150;
	EditWin = window.open("/allLines/edit.cfm?getItem=1&id=" + id,"EditWin","top=" + top + ",left=" + left + ",height=" + windowHeight + ",width=" + windowWidth + ",resizable=1,status=0,titlebar=0,toolbar=0");
}

function checkUncheck(frm) {
	if (frm.checkStatus.value == 1) {
		var checkStatus = false;
		frm.checkStatus.value = 0;
	} else {
		var checkStatus = true;
		frm.checkStatus.value = 1;
	}
	if (frm.delItemID.length > 1) {
		for (var i=0; i < frm.delItemID.length; i++) {
			frm.delItemID[i].checked = checkStatus;
		}
	} else {
		frm.delItemID.checked = checkStatus;
	}
}

function deleteChecked(frm) {
	if (confirm('Are you sure you wish to delete all selected items?')) {
		frm.deleteConfirmed.value = 1;
		frm.submit();
	}
}
</script>

<cfset ds = "allLines">

<cfparam name="includeColumn" default="">
<cfparam name="whereField" default="">
<cfparam name="operator" default="">
<cfparam name="value" default="">
<cfparam name="sortField" default="">
<cfparam name="sortOrder" default="">
<cfparam name="removeWhere_index" default="0">
<cfparam name="removeSort_index" default="0">
<cfparam name="m" default="">
<cfparam name="form.delItemID" default="0">
<cfparam name="form.deleteConfirmed" default="0">
<cfparam name="departmentList" default="">
<cfparam name="whereStatement" default="">
<cfparam name="sortStatement" default="">

<cfif form.deleteConfirmed is 1>
	<cfif form.delItemID is not 0>
		<cfset result = deleteItems(form.delItemID)>
	</cfif>
</cfif>

<cfif isDefined("url.delReport")>
	<cfif url.reportid is ""><cfset url.reportid = 0></cfif>
	<cfquery name="delReport" datasource="#ds#">
		delete from savedReports
		where savedReportid = #url.reportid#
	</cfquery>
</cfif>

<cfif isDefined("form.btnSaveReport")>
	<cfquery name="savereport" datasource="#ds#">
		insert into savedReports (savedReportName, savedQuery, savedDateTime)
		values ('#form.savedReportName#','#form.searchQuery#',#createodbcdatetime(now())#)
	</cfquery>
	<cflocation url="reportGenerator.cfm">
</cfif>

<cfif isDefined("form.btnAddWhere")>
	<cfset value = replace(value, "*", "%", "all")>
	<cfif operator is "IS NULL" or operator is "IS NOT NULL">
		<cfif listlen(trim(whereField),".") is 1>
			<cfset whereField = "[#trim(whereField)#]">
		<cfelse>
			<cfset whereField = listgetat(trim(whereField),1,".") & "." & "[#listgetat(trim(whereField),2,".")#]">
		</cfif>
		<cfset whereStatement = listappend(whereStatement, "#whereField# #operator#", "~")>
	<cfelse>
		<cfset whereStatement = listappend(whereStatement, "#whereField# #operator# '#value#'", "~")>
	</cfif>
</cfif>

<cfif isDefined("form.btnRemoveWhere")>
	<cfset whereStatement = listdeleteat(whereStatement, form.removeWhere_index, "~")>
</cfif>

<cfif isDefined("form.btnAddSort")>
	<cfset sortStatement = listappend(sortStatement, "#sortField# #sortOrder#")>
</cfif>

<cfif isDefined("form.btnRemoveSort")>
	<cfset sortStatement = listdeleteat(sortStatement, form.removeSort_index)>
</cfif>

<cfif isDefined("url.runreport") and isDefined("url.reportid")>
	<cfif url.reportid is ""><cfset url.reportid = 0></cfif>
	<cfquery name="getReport" datasource="#ds#">
		select * from savedReports
		where savedReportid = #url.reportid#
	</cfquery>
	<cfset form.btnGenerateReport = 1>
	<cfset searchQuery = getReport.savedQuery>
</cfif>

<cfif isDefined("form.btnGenerateReport")>

	<cfif form.btnGenerateReport is not 1>
		<cfset err = validateReportForm(form)>
		<cfif trim(err) is "">
			<cfset colList = "">
			<cfloop list="#includeColumn#" index="col">
				<cfif listlen(trim(col),".") is 1>
					<cfset colList = listappend(colList, "all_lines.[#trim(col)#]")>
				<cfelse>
					<cfset colList = listappend(colList, "#trim(col)#")>
				</cfif>
			</cfloop>
			<cfset searchQuery = "select #colList# from all_lines left join receiver_lookup on all_lines.id = receiver_lookup.lineid left join receivers on receiver_lookup.receiverID = receivers.receiverID left join dealer_lookup on all_lines.id = dealer_lookup.lineID left join dealers on dealer_lookup.dealerID = dealers.dealerID ">
			<cfif trim(whereStatement) is not "">
				<cfset searchQuery = searchQuery & "where ">
				<cfloop list="#whereStatement#" delimiters="~" index="item">
					<cfset searchQuery = searchQuery & "#item# and ">
				</cfloop>
				<cfset searchQuery = searchQuery & "1=1 "> 
			</cfif>
			<cfif trim(sortStatement) is not "">
				<cfset searchQuery = searchQuery & "order by #sortStatement# ">
			</cfif>
		<cfelse>
		<div align="center">
		<table width="400" border="0" cellspacing="0" cellpadding="5">
			<cfloop list="#err#" index="e">
			<tr>
				<td class="alert"><b><cfoutput>#e#</cfoutput></b></td>
			</tr>
			</cfloop>
			<tr>
				<td>
				<a class="normal" style="text-decoration:underline" href="javascript: history.go(-1);">Return to previous page to correct</a>
				</td>
			</tr>
		</table>
		</div>
		<cfabort>
		</cfif>
		<cfset searchQuery = replace(searchQuery, "select ", "select id as itemid,")>
	</cfif>

	<cfquery name="getReport" datasource="#ds#">
		#preserveSingleQuotes(searchQuery)#
	</cfquery>

	<cfif isDefined("url.exceldownload")>
		<cfset colList = getReport.columnList>
		<cfset outputfilename = "#cookie.adminlogin##TimeFormat(now(),'hhmmss')#.xls">
		<cfset outputfilepath = "#request.DirectPath#\allLines\reports\downloads">
		<cfset downloadURL = "/allLines/reports/downloads">
		<cfx_query2excel 
			file="#outputfilepath#\#outputfilename#" 
			headings="#colList#" 
			queryFields="#colList#" 
			format="excel" 
			query="#getReport#">
		
		<cfheader name="content-disposition" value="attachment;filename=#outputfilename#"> 
		<cfcontent type="application/unknown" file="#outputfilepath#\#outputfilename#"> 
		<br />
		<br />
		If your download does not begin automatically, <a class="link" href="<cfoutput>#downloadurl#/#outputfilename#</cfoutput>">right click here </a> and choose &quot;Save As&quot; to download manually.
		<br />
		<br />
		<cfabort>
	</cfif>

	<div align="center">
	<a class="normal" style="text-decoration:underline" href="javascript: history.go(-1);">Return to previous page</a>
	<br />
	<br />
	<cfif not isDefined("url.runreport")>
	<table border="0" cellspacing="0" cellpadding="3">
	<form method="post" action="reportGenerator.cfm">
	<input type="hidden" name="searchQuery" value="<cfoutput>#searchQuery#</cfoutput>" />
	<tr>
		<td align="center" colspan="<cfoutput>#listlen(getReport.columnList)#</cfoutput>">
			<table border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td colspan="2"><b>Save Report As:</b></td>
				</tr>
				<tr>
					<td><input type="text" name="savedReportName" maxlength="50" size="30" /></td>
					<td><input name="btnSaveReport" type="submit" class="sidebar" value="Save Report" /></td>
				</tr>
			</table>
		</td>
	</tr>
	</table>
	</form>
	</cfif>
	<table width="900" border="0" cellspacing="0" cellpadding="3">
	<!--- output report --->
	<form method="post" action="reportGenerator.cfm" name="outputForm">
	<input type="hidden" name="deleteConfirmed" value="0" />
	<input type="hidden" name="checkStatus" value="0" />
	<cfoutput>
	<input type="hidden" name="searchQuery" value="#searchQuery#" />
	</cfoutput>
	<input type="hidden" name="btnGenerateReport" value="1" />
	<tr>
		<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td nowrap="nowrap" class="linedrow">
				<cfif listfind(departmentlist,1) is not 0>
					<a href="javascript: checkUncheck(document.outputForm);"><img alt="Check All/Uncheck All" src="../../images/checkBox.gif" border="0" /></a>
					<a href="javascript: deleteChecked(document.outputForm);"><img alt="Delete Selected Items" src="../../images/trash.gif" border="0" /></a>
				<cfelse>
					&nbsp;
				</cfif>
				</td>
				<cfloop list="#getReport.columnList#" index="col">
					<cfif col is not "itemid">
						<cfoutput>
						<td nowrap="nowrap" class="linedrow"><b>#col#</b></td>
						</cfoutput>
					</cfif>
				</cfloop>
			</tr>
			<cfloop from="1" to="#getReport.RecordCount#" index="i">
			<cfif i mod 2 is 0><cfset bgc = "##EEEEEE"><cfelse><cfset bgc = "##FFFFFF"></cfif>
			<tr bgcolor="<cfoutput>#bgc#</cfoutput>">
				<cfoutput>
				<td nowrap="nowrap" class="linedrowrightcolumn" style="border-left:##D7D7D7 solid 1px" width="120" valign="top">
					<cfif listfind(departmentlist,1) is not 0>
						<input type="checkbox" name="delItemID" value="#getReport['itemid'][i]#" />
						&nbsp;
						<a href="javascript: OpenEditWin(#getReport['itemid'][i]#);">[edit item]</a>
					<cfelse>
						&nbsp;
					</cfif>
				</td>
				</cfoutput>
				<cfloop list="#getReport.columnList#" index="col">
					<cfif col is not "itemid">
						<cfoutput>
						<td class="linedrowrightcolumn" valign="top">
						#getReport["#evaluate("col")#"][i]#&nbsp;
						</td>
						</cfoutput>
					</cfif>
				</cfloop>
			</tr>
			</cfloop>
		</table>
		</td>
	</tr>
	</form>
	</table>
	<br />
	<a class="normal" style="text-decoration:underline" href="javascript: history.go(-1);">Return to previous page</a>
	</div>
	<cfabort>
	<!--- output report --->
</cfif>

<cfset columnList = "ID, PhoneNumber, receivers.Receiver, UsedFor, DESCRIPTION, SingleCo, WhoOwns, WhoPays, DNIS, ConnectionType, LinkedTo, LinkMethod, HUNT, Trunk, ExtLinkedTo, dealers.dealerNumber, dealers.dealerName, Comments, Active, Location, GL comments, OrderPending, OrderDetails, Carrier, Account, CircuitID, TrunkGroup, Lock, OrderDate, OrderDueDate, C-24-profile, 3-Digit, Pad-with, PayphoneBlock, Phil, FlatRate, lastModified, lastModifiedBy, dealerLetter, accountRanges, manitou, primacy, validated, twoWay, private, altDnis, altTrunkGroup, altExtLink">
<cfset columnList = listsort(columnList,"text","asc")>

<cfquery name="getFields" datasource="#ds#">
	select * 
	from all_lines 
	where id = 0
</cfquery>

<cfquery name="getSavedReports" datasource="#ds#">
	select * 
	from savedReports
	order by savedReportName asc 
</cfquery>

<div align="center">
<a class="normal" style="text-decoration:underline" href="/index.cfm">Return to Intranet</a>
<br />
<span class="normal">
<br />
<b style="font-size:18px">Report Generator</b>
<br /><br />
</span>
<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<form method="post" action="reportGenerator.cfm" name="mainform">
	<input type="hidden" name="removeWhere_index" value="0" />
	<input type="hidden" name="removeSort_index" value="0" />
	<cfoutput>
	<input type="hidden" name="whereStatement" value="#whereStatement#" />
	<input type="hidden" name="sortStatement" value="#sortStatement#" />
	</cfoutput>
	<cfif trim(m) is not "">
	<tr>
		<td colspan="3" align="center" class="alert"><cfoutput><b>#m#</b></cfoutput><br /><br /></td>
	</tr>
	</cfif>
	<tr>
		<td colspan="3" align="center">
			<input name="btnGenerateReport" type="submit" class="sidebar" value="Run Generated Report" style="width:400px; padding:10px" />
		</td>
	</tr>
	<cfif getSavedReports.recordCount gt 0>
	<tr>
		<td colspan="3" align="center">
		<table width="725" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td colspan="3"><b>Previously Saved Reports</b></td>
			</tr>
			<cfoutput query="getSavedReports">
			<cfif getSavedReports.currentRow mod 2 is 0><cfset bgc = "##EEEEEE"><cfelse><cfset bgc = "##FFFFFF"></cfif>
			<tr bgcolor="#bgc#">
				<td>#savedReportName#</td>
				<td>#dateformat(savedDateTime,'mm/dd/yyyy')#</td>
				<td>
				<a href="reportGenerator.cfm?runreport=1&reportid=#savedReportID#">[run report]</a>
				&nbsp;
				<a onclick="return confirm('Are you sure you wish to delete this report?')" href="reportGenerator.cfm?delReport=1&reportid=#savedReportID#">[delete report]</a>
				&nbsp;
				<a href="reportGenerator.cfm?runreport=1&reportid=#savedReportID#&exceldownload=true">[download report as xls]</a>
				</td>
			</tr>
			</cfoutput>
		</table>
		</td>
	</tr>
	</cfif>
	<tr>
		<td><a href="javascript:checkAllFields(document.mainform);">[Check All]</a></td>
		<td valign="top" class="nopadding">&nbsp;</td>
		<td valign="top" class="nopadding">&nbsp;</td>
	</tr>
	<tr>
		<td><a href="javascript:UnCheckAllFields(document.mainform);">[Un-Check All]</a></td>
		<td valign="top" class="nopadding">&nbsp;</td>
		<td valign="top" class="nopadding">&nbsp;</td>
	</tr>
	<tr>
		<td valign="top">
		<table border="0" cellspacing="0" cellpadding="1">
			<tr>
				<td colspan="2"><b>Include Field</b></td>
			</tr>
			<cfloop list="#variables.columnList#" index="column">
			<cfoutput>
			<tr>
				<td><input  <cfif listfindnocase(includeColumn,column) gt 0>checked</cfif> type="checkbox" name="includeColumn" value="#column#"></td>
				<td width="150">#column#</td>
			</tr>
			</cfoutput>
			</cfloop>
		</table>
		</td>
		<td valign="top" class="nopadding">
			<table border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td><b>Criteria</b></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>Field</td>
					<td>Operator</td>
					<td>Value</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>
						<select name="whereField">
							<option value=""></option>
						<cfloop list="#variables.columnList#" index="column">
							<cfoutput>
							<option value="#trim(column)#">#trim(column)#</option>
							</cfoutput>
						</cfloop>
						</select>
					</td>
					<td>
						<select name="operator">
							<option value=""></option>
							<option value="=">=</option>
							<option value="LIKE">LIKE</option>
							<option value="NOT LIKE">NOT LIKE</option>
							<option value="<">Less than</option>
							<option value=">">Greater than</option>
							<option value="<>">Not Equal To</option>
							<option value="IS NULL">IS NULL</option>
							<option value="IS NOT NULL">IS NOT NULL</option>
						</select>
					</td>
					<td>
						<input type="text" name="value" value="">
					</td>
					<td>
						<input  onclick="return validateWhere(this.form);" name="btnAddWhere" type="submit" class="sidebarsmall" value="Add">
					</td>
				</tr>
				<cfset whereindex = 0>
				<cfloop list="#whereStatement#" index="item" delimiters="~">
				<cfset whereindex = whereindex + 1>
				<cfset whereField = listgetat(item,1," ")>
				<cfset tmp = replace(item,"#whereField# ","")>
				<cfif trim(tmp) is "IS NOT NULL">
					<cfset operator = "IS NOT">
					<cfset value = "NULL">
				<cfelse>
					<cfset operator = listgetat(item,2," ")>
					<cfset value = listgetat(item,3," ")>
				</cfif>
				<cfoutput>
				<tr>
					<td>
					<cfset whereField = replace(whereField,"[","","all")>
					<cfset whereField = replace(whereField,"]","","all")>
					#whereField#
					</td>
					<td>
					<cfif operator is "<">
					Less Than
					<cfelseif operator is ">">
					Greater Than
					<cfelseif operator is "<>">
					Not Equal To
					<cfelse>
					#operator#
					</cfif>
					</td>
					<td>
					<cfset value = replace(value, "%", "*", "all")>
					#value#
					</td>
					<td>
					<input name="btnRemoveWhere" type="submit" onclick="this.form.removeWhere_index.value = #whereindex#;" class="sidebarsmall" value="Remove">
					</td>
				</tr>
				</cfoutput>
				</cfloop>
			</table>
		</td>
		<td valign="top" class="nopadding">
			<table border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td><b>Sort By</b></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>Field</td>
					<td>Sort Order</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>
						<select name="sortField">
							<option value=""></option>
						<cfloop list="#variables.columnList#" index="column">
							<cfoutput>
							<option value="#column#">#column#</option>
							</cfoutput>
						</cfloop>
						</select>
					</td>
					<td>
						<select name="sortorder">
							<option value=""></option>
							<option value="ASC">Ascending</option>
							<option value="DESC">Descending</option>
						</select>
					</td>
					<td>
						<input onclick="return validateSort(this.form);" name="btnAddSort" type="submit" class="sidebarsmall" value="Add">
					</td>
				</tr>
				<cfset sortindex = 0>
				<cfloop list="#sortStatement#" index="sortItem">
				<cfset sortindex = sortindex + 1>
				<cfset sortField = listgetat(sortItem,1," ")>
				<cfset sortOrder = listgetat(sortItem,2," ")>
				<cfoutput>
				<tr>
					<td>#sortField#</td>
					<td>#sortOrder#</td>
					<td>
					<input name="btnRemoveSort" type="submit" onclick="this.form.removeSort_index.value = #sortIndex#;" class="sidebarsmall" value="Remove">
					</td>
				</tr>
				</cfoutput>
				</cfloop>
			</table>
		</td>
	</tr>
	</form>
</table>
</div>
