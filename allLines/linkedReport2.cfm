
<cfsetting showdebugoutput="no">

<style type="text/css">
	TABLE {font-family:Verdana, Arial, Helvetica, sans-serif; font-size:11px; color:#000000;}
	.red {font-family:Verdana, Arial, Helvetica, sans-serif; font-size:11px; color:#FF0000;}
	.normal {font-family:Verdana, Arial, Helvetica, sans-serif; font-size:11px; color:#000000;}
</style>

<cfquery name="getlinked" datasource="alllines">
	select phoneNumber, extLinkedto from all_lines 
	where phoneNumber IN (select distinct linkedTo from all_lines where linkedTo <> '') 
	and extLinkedTo <> '' 
	order by phoneNumber asc 
</cfquery>

<div class="normal">
<table border="1" cellspacing="0" cellpadding="5">
	<tr>
		<td><b>Phone Number</b></td>
		<td><b>Ext Linked To</b></td>
	</tr>
	<cfloop query="getLinked">
		<cfquery name="getSubs" datasource="alllines">
			select ID, phoneNumber, extLinkedTo from all_lines 
			where linkedto = '#getLinked.phoneNumber#' and connectiontype = 'rcf' and whoowns = 'cops'
			order by phoneNumber asc 
		</cfquery>
		<cfif getsubs.recordcount gt 0>
			<cfoutput>
			<tr>
				<td><b>#getLinked.phoneNumber#</b>&nbsp;</td>
				<td><b>#getLinked.extLinkedto#</b>&nbsp;</td>
			</tr>
			</cfoutput>
			<cfloop query="getsubs">
				<cfif trim(getsubs.extLinkedTo) is not trim(getLinked.extLinkedTo)>
					<cfset c = "red">
					<!--- 
					<cfquery name="updateLink" datasource="allLines">
						update all_lines 
						set extLinkedTo = '#getLinked.extLinkedto#'
						where ID = #getsubs.id# 
					</cfquery>
					--->
				<cfelse>
					<cfset c = "normal">
				</cfif>
				<cfoutput>
				<tr>
					<td class="#c#">#getsubs.phoneNumber#&nbsp;</td>
					<td class="#c#">#getsubs.extlinkedto#&nbsp;</td>
				</tr>			
				</cfoutput>
			</cfloop>
			<tr><td colspan="2">&nbsp;</td></tr>
		</cfif>
	</cfloop>
</table>
</div>
